/**
 * @vendor      Kurufootwear
 * @module      Magento_Swatches
 * @copyright   Copyright (c) 2017 Scandiweb. All rights reserved.
 * @license     http://opensource.org/licenses/afl-3.0.php Academic Free License (AFL 3.0)
 */

define([
    'jquery',
    'underscore',
    'jquery/ui',
    'jquery/jquery.parsequery',
    'selectric',
    'arrive',
    'mage/validation'
], function ($, _) {
    'use strict';

    /**
     * Render tooltips by attributes (only to up).
     * Required element attributes:
     *  - option-type (integer, 0-3)
     *  - option-label (string)
     *  - option-tooltip-thumb
     *  - option-tooltip-value
     */
    $.widget('mage.SwatchRendererTooltip', {
        options: {
            delay: 200,                             //how much ms before tooltip to show
            tooltipClass: 'swatch-option-tooltip'  //configurable, but remember about css
        },

        /**
         * @private
         */
        _init: function () {
            var $widget = this,
                $this = this.element,
                $element = $('.' + $widget.options.tooltipClass),
                timer,
                type = parseInt($this.attr('option-type'), 10),
                label = $this.attr('option-label'),
                thumb = $this.attr('option-tooltip-thumb'),
                value = $this.attr('option-tooltip-value'),
                $image,
                $title,
                $corner;

            if (!$element.size()) {
                $element = $('<div class="' +
                    $widget.options.tooltipClass +
                    '"><div class="image"></div><div class="title"></div><div class="corner"></div></div>'
                );
                $('body').append($element);
            }

            $image = $element.find('.image');
            $title = $element.find('.title');
            $corner = $element.find('.corner');

            $this.hover(function () {
                if (!$this.hasClass('disabled')) {
                    timer = setTimeout(
                        function () {
                            var leftOpt = null,
                                leftCorner = 0,
                                left,
                                $window;

                            if (type === 2) {
                                // Image
                                $image.css({
                                    'background': 'url("' + thumb + '") no-repeat center', //Background case
                                    'background-size': 'initial'
                                });
                                $image.show();
                            } else if (type === 1) {
                                // Color
                                $image.css({
                                    background: value
                                });
                                $image.show();
                            } else if (type === 0 || type === 3) {
                                // Default
                                $image.hide();
                            }

                            $title.text(label);

                            leftOpt = $this.offset().left;
                            left = leftOpt + $this.width() / 2 - $element.width() / 2;
                            $window = $(window);

                            // the numbers (5 and 5) is magick constants for offset from left or right page
                            if (left < 0) {
                                left = 5;
                            } else if (left + $element.width() > $window.width()) {
                                left = $window.width() - $element.width() - 5;
                            }

                            // the numbers (6,  3 and 18) is magick constants for offset tooltip
                            leftCorner = 0;

                            if ($element.width() < $this.width()) {
                                leftCorner = $element.width() / 2 - 3;
                            } else {
                                leftCorner = (leftOpt > left ? leftOpt - left : left - leftOpt) + $this.width() / 2 - 6;
                            }

                            $corner.css({
                                left: leftCorner
                            });
                            $element.css({
                                left: left,
                                top: $this.offset().top - $element.height() - $corner.height() - 18
                            }).show();
                        },
                        $widget.options.delay
                    );
                }
            }, function () {
                $element.hide();
                clearTimeout(timer);
            });

            $(document).on('tap', function () {
                $element.hide();
                clearTimeout(timer);
            });

            $this.on('tap', function (event) {
                event.stopPropagation();
            });
        }
    });

    /**
     * Render swatch controls with options and use tooltips.
     * Required two json:
     *  - jsonConfig (magento's option config)
     *  - jsonSwatchConfig (swatch's option config)
     *
     *  Tuning:
     *  - numberToShow (show "more" button if options are more)
     *  - onlySwatches (hide selectboxes)
     *  - moreButtonText (text for "more" button)
     *  - selectorProduct (selector for product container)
     *  - selectorProductPrice (selector for change price)
     */
    $.widget('mage.SwatchRenderer', {
        options: {
            classes: {
                attributeClass: 'swatch-attribute',
                attributeLabelClass: 'swatch-attribute-label',
                attributeSelectedOptionLabelClass: 'swatch-attribute-selected-option',
                attributeOptionsWrapper: 'swatch-attribute-options',
                attributeInput: 'swatch-input',
                optionClass: 'swatch-option',
                selectClass: 'swatch-select',
                moreButton: 'swatch-more',
                loader: 'swatch-option-loading',
                attributeWrapperClass: 'product-options-wrapper',
                sizeInfoClass: 'size-info-wrapper'
            },

            /**
             * Options for query parameter manipulation
             */
            queryParams: {
                ignored: [
                    'v'
                ],
                forbidden: [
                    'size',
                    'womens_size',
                    'socks_size'
                ]
            },

            // option's json config
            jsonConfig: {},

            // swatch's json config
            jsonSwatchConfig: {},

            // selector of parental block of prices and swatches (need to know where to seek for price block)
            selectorProduct: '.product-info-main',

            // selector of price wrapper (need to know where set price)
            selectorProductPrice: '[data-role=priceBox]',

            //selector of product images gallery wrapper
            mediaGallerySelector: '[data-gallery-role=gallery-placeholder]',

            // selector of category product tile wrapper
            selectorProductTile: '.product-item',

            // number of controls to show (false or zero = show all)
            numberToShow: false,

            // show only swatch controls
            onlySwatches: false,

            // enable label for control
            enableControlLabel: true,

            // flag to check first base image update
            initialBaseImageUpdate: true,

            // text for more button
            moreButtonText: 'More',

            // Callback url for media
            mediaCallback: '',

            // Local media cache
            mediaCache: {},

            // Cache for BaseProduct images. Needed when option unset
            mediaGalleryInitial: [{}],

            // whether swatches are rendered in product list or on product page
            inProductList: false,

            /**
             * Defines the mechanism of how images of a gallery should be
             * updated when user switches between configurations of a product.
             *
             * As for now value of this option can be either 'replace' or 'prepend'.
             *
             * @type {String}
             */
            gallerySwitchStrategy: 'replace',

            // sly-old-price block selector
            slyOldPriceSelector: '.sly-old-price',

            // Document title
            documentTitle: document.title,

            // Position of color in title
            colorPosition: 0,

            // Title suffix
            titleSuffix: '',

            // Stock message selector
            stockMessageSelector: '.stock-message-wrapper .stock-message'
        },

        /**
         * Get chosen product
         *
         * @returns array
         */
        getProduct: function () {
            return this._CalcProducts().shift();
        },

        /**
         * @private
         */
        _init: function () {
            if (this.options.jsonConfig !== '' && this.options.jsonSwatchConfig !== '') {
                this.isInitialising = true;
                this._forceUpdateConfig($.proxy(function() {
                    this._sortAttributes();
                    this._RenderControls();
                    this._removeInitialisationClass();
                    this.isInitialising = false;
                }, this));
            } else {
                console.log('SwatchRenderer: No input data received');
            }
        },

        /**
         * Remove initialisation class from price boxes
         *
         * @private
         */
        _removeInitialisationClass: function() {
            var initialisationClass = "initialising";
            var $priceBox = $(this.options.selectorProductPrice);

            $priceBox
                .find("." + initialisationClass)
                .add($priceBox)
                .removeClass(initialisationClass);
        },

        _forceUpdateConfig: function(callback) {
            var swatchRenderer = this;
            var swatchAjaxUrl = this.options.jsonConfigAjaxUrl;
            var productId = this.options.productId;

            if (swatchAjaxUrl) {
                $.ajax({
                    url: swatchAjaxUrl,
                    type: 'POST',
                    dataType: 'JSON',
                    data: { id: productId },
                    complete: function (response) {
                        var data = response.responseJSON;
                        if (data.success && data.jsonConfig) {
                            swatchRenderer.options.jsonConfig = data.jsonConfig;
                        } else {
                            console.warn('Updating latest swatch configurations failed.');
                        }
                        callback();
                    }
                });
            } else {
                callback();
            }
        },

        /**
         * @private
         */
        _sortAttributes: function () {
            this.options.jsonConfig.attributes = _.sortBy(this.options.jsonConfig.attributes, function (attribute) {
                return attribute.position;
            });
        },

        /**
         * Color preselection if none is selected yet
         * @TODO - This should be done in the PHP level
         */
        checkSelectedOption: function () {
            var colorSwatchesSelector = '.swatch-attribute.color .swatch-option';

            if (!$(colorSwatchesSelector).hasClass('selected')) {
                $(colorSwatchesSelector).first().trigger('click');
            }
        },

        /**
         * @private
         */
        _create: function () {
            var options = this.options,
                gallery = $('[data-gallery-role=gallery-placeholder]', '.column.main'),
                isProductViewExist = $('body.catalog-product-view').size() > 0,
                $main = isProductViewExist ?
                    this.element.parents('.column.main') :
                    this.element.parents('.product-item-info');

            if (isProductViewExist) {
                gallery.data('gallery') ?
                    this._onGalleryLoaded(gallery) :
                    gallery.on('gallery:loaded', this._onGalleryLoaded.bind(this, gallery));
            } else {
                options.mediaGalleryInitial = [{
                    'img': $main.find('.product-image-photo').attr('src')
                }];
            }

            this.productForm = this.element.parents(this.options.selectorProductTile).find('form:first');
            this.inProductList = this.productForm.length > 0;
        },

        /**
         * Render controls
         *
         * @private
         */
        _RenderControls: function () {
            var $widget = this,
                container = this.element,
                classes = this.options.classes,
                chooseText = this.options.jsonConfig.chooseText;

            $widget.optionsMap = {};

            $.each(this.options.jsonConfig.attributes, function () {
                var item = this,
                    // These options are now rendered differently
                    options = $widget._RenderSwatchOptions(item),
                    select = $widget._RenderSwatchSelect(item, chooseText),
                    input = $widget._RenderFormInput(item),
                    label = '';

                // Show only swatch controls
                if ($widget.options.onlySwatches && !$widget.options.jsonSwatchConfig.hasOwnProperty(item.id)) {
                    return;
                }

                if ($widget.options.enableControlLabel) {
                    label +=
                        '<span class="' + classes.attributeLabelClass + '">' + item.label + '</span>' +
                        '<span class="' + classes.attributeSelectedOptionLabelClass + '"></span>';
                }

                if ($widget.inProductList) {
                    $widget.productForm.append(input);
                    input = '';
                }

                // Create new control
                container.append(
                    '<div class="' + classes.attributeClass + ' ' + item.code +
                    '" attribute-code="' + item.code +
                    '" attribute-id="' + item.id + '">' +
                    label +
                    '<div class="' + classes.attributeOptionsWrapper + ' clearfix">' +
                    options + select +
                    '</div>' + input +
                    '</div>'
                );

                $widget.optionsMap[item.id] = {};

                // Aggregate options array to hash (key => value)
                $.each(item.options, function () {
                    if (this.products.length > 0) {
                        $widget.optionsMap[item.id][this.id] = {
                            price: parseInt(
                                $widget.options.jsonConfig.optionPrices[this.products[0]].finalPrice.amount,
                                10
                            ),
                            products: this.products
                        };
                    }
                });
            });

            // Hide all elements below more button
            $('.' + classes.moreButton).nextAll().hide();

            // Handle events like click or change
            $widget._EventListener();

            // Rewind options
            $widget._Rewind(container);
    
            /**
             * Emulate click on all swatches from Request
             *
             * JS preselection must be disabled, because it's AJAX and it can mismatch w/
             * Varnish-cached PHP preselection and will result in price glitching (KURU2-1559)
             *
             * Solution dry-preselect - on init don't change prices, only handle proper rendering
             */
            $widget._EmulatePreselection($.parseQuery());
    
            this.checkSelectedOption();

            // Hax to move width selector! https://youtu.be/fVz1VVWcF5U?t=1m23s
            // @TODO Remove HAX when products ir merged
            // <!--- Hax Starts -->
            $widget.moveSwatchBlockElements();
            // <!--- Hax Ends -->
        },

        /**
         * Move swatch block elements
         */
        moveSwatchBlockElements: function () {
            var $widget = this;

            $('.swatch-opt').ready(function () {
                var widthAttribute = '.product.attribute.width',
                    womensSizeAttribute = '.' + $widget.options.classes.attributeClass + '.womens_size',
                    sizeAttribute = '.' + $widget.options.classes.attributeClass + '.size',
                    socksSizeAttribute = '.' + $widget.options.classes.attributeClass + '.socks_size',
                    womensSizeLabel = womensSizeAttribute + ' .' + $widget.options.classes.attributeSelectedOptionLabelClass,
                    sizeLabel = sizeAttribute + ' .' + $widget.options.classes.attributeSelectedOptionLabelClass,
                    socksSizeLabel = socksSizeAttribute + ' .' + $widget.options.classes.attributeSelectedOptionLabelClass,
                    sizeInfoSelector = '.' + $widget.options.classes.sizeInfoClass,
                    attributeWrapperSelector = '.' + $widget.options.classes.attributeWrapperClass;

                $(widthAttribute).insertBefore(sizeAttribute);
                $(widthAttribute).insertBefore(womensSizeAttribute);
                $(sizeInfoSelector).insertBefore(sizeLabel);
                $(sizeInfoSelector).insertBefore(womensSizeLabel);
                $(sizeInfoSelector).insertBefore(socksSizeLabel);
                $(attributeWrapperSelector).removeClass('initializing');
            });
        },

        /**
         * Render swatch options by part of config
         *
         * @param {Object} config
         * @returns {String}
         * @private
         */
        _RenderSwatchOptions: function (config) {
            /**
             * Render womens_size, size and socks_size as buttons
             * Temporary solution till switch to text swatch
             */
            var sizeCode = config.code.match(/size/gi) ? config.code : null,
                optionConfig = this.options.jsonSwatchConfig[config.id],
                optionClass = this.options.classes.optionClass,
                moreLimit = parseInt(this.options.numberToShow, 10),
                moreClass = this.options.classes.moreButton,
                moreText = this.options.moreButtonText,
                countAttributes = 0,
                html = '',
                // Fixer browser specific bug that displays images diffrently
                isIE = (navigator.appName === 'Microsoft Internet Explorer' || !!(navigator.userAgent.match(/Trident/) || navigator.userAgent.match(/rv:11/)) || (typeof $.browser !== 'undefined' && $.browser.msie === 1)),
                browserSelector = isIE ? 'background-size: contain;' : 'object-fit: contain; background-size: contain;';

            // Don't render buttons, if not womens_size/size/socks_size
            if (!sizeCode && !this.options.jsonSwatchConfig.hasOwnProperty(config.id)) {
                return '';
            }

            // Size ascending order
            if (config.code.indexOf('size') !== -1) {
                config.options.sort(function(a, b) {
                    return parseFloat(a.label) - parseFloat(b.label);
                });
            }

            $.each(config.options, function () {
                var id,
                    type,
                    value,
                    thumb,
                    label,
                    attr;

                if (!sizeCode && !optionConfig.hasOwnProperty(this.id)) {
                    return '';
                }

                if (sizeCode) {
                    // Parse and generate size/womens_size differently than normal buttons
                    id = this.id;
                    type = 0;
                    optionClass = 'swatch-option';
                    label = this.label;
                    attr =
                        ' value="' + id + '"' +
                        ' option-value="' + label + '"' +
                        ' option-id="' + id + '"';

                    if (!this.hasOwnProperty('products') || this.products.length <= 0) {
                        attr += ' option-empty="true"';
                    }
                } else {
                    // Add more button
                    if (moreLimit === countAttributes++) {
                        html += '<a href="#" class="' + moreClass + '">' + moreText + '</a>';
                    }

                    id = this.id;
                    type = parseInt(optionConfig[id].type, 10);
                    value = optionConfig[id].hasOwnProperty('value') ? optionConfig[id].value : '';
                    thumb = optionConfig[id].hasOwnProperty('thumb') ? optionConfig[id].thumb : '';
                    label = this.label ? this.label : '';
                    attr =
                        ' option-type="' + type + '"' +
                        ' option-id="' + id + '"' +
                        ' option-label="' + label + '"' +
                        ' option-tooltip-thumb="' + thumb + '"' +
                        ' option-tooltip-value="' + value + '"';

                    if ($('body').hasClass('catalog-product-view')) {
                        attr += ' option-tooltip-media="false"';
                    }

                    if (!this.hasOwnProperty('products') || this.products.length <= 0) {
                        attr += ' option-empty="true"';
                    }
                }

                if (type === 0) {
                    // Text
                    html += '<div class="' + optionClass + ' text button secondary-button" ' + attr + '>' + (value ? value : label) +
                        '</div>';
                } else if (type === 1) {
                    // Color
                    html += '<div class="' + optionClass + ' color" ' + attr +
                        '" style="background: ' + value +
                        ' no-repeat center; background-size: contain;">' + '' +
                        '</div>';
                } else if (type === 2) {
                    // Image
                    html += '<div class="' + optionClass + ' image" ' + attr +
                        '" style="background: url(' + value + ') no-repeat center;  background-size: initial; ' + browserSelector + '">' + '' +
                        '</div>';
                } else if (type === 3) {
                    // Clear
                    html += '<div class="' + optionClass + '" ' + attr + '></div>';
                } else if (type === 4) {
                    // Gradient
                    html += '<div class="' + optionClass + ' color" ' + attr +
                        '" style="background-image: ' + value + ';"></div>';
                } else {
                    // Defaualt
                    html += '<div class="' + optionClass + '" ' + attr + '>' + label + '</div>';
                }
            });

            return html;
        },

        /**
         * Render select by part of config
         *
         * @param {Object} config
         * @param {String} chooseText
         * @returns {String}
         * @private
         */
        _RenderSwatchSelect: function (config, chooseText) {
            /**
             * Skip and don't render womens_size, size and socks_size as dropdowns
             * Temporary solution till switch to text swatch
             */
            var sizeCode = config.code.match(/size/gi) ? config.code : null,
                html;

            if (this.options.jsonSwatchConfig.hasOwnProperty(config.id) || sizeCode) {
                return '';
            }

            html =
                '<select class="' + this.options.classes.selectClass + ' ' + config.code + '">' +
                '<option value="0" option-id="0">' + chooseText + '</option>';

            $.each(config.options, function () {
                var label = this.label,
                    attr = ' value="' + this.id + '" option-id="' + this.id + '"';

                if (!this.hasOwnProperty('products') || this.products.length <= 0) {
                    attr += ' option-empty="true"';
                }

                html += '<option ' + attr + '>' + label + '</option>';
            });

            html += '</select>';

            return html;
        },

        /**
         * Input for submit form.
         * This control shouldn't have "type=hidden", "display: none" for validation work :(
         *
         * @param {Object} config
         * @private
         */
        _RenderFormInput: function (config) {
            return '<input class="' + this.options.classes.attributeInput + ' super-attribute-select" ' +
                'name="super_attribute[' + config.id + ']" ' +
                'type="text" ' +
                'value="" ' +
                'data-selector="super_attribute[' + config.id + ']" ' +
                'data-validate="{required:true}" ' +
                'aria-required="true" ' +
                'data-msg-required="Please select a ' + config.label.toLowerCase() + '."' +
                'aria-invalid="true" ' +
                'style="visibility: hidden; position:absolute; left:-1000px">';
        },

        /**
         * Event listener
         *
         * @private
         */
        _EventListener: function () {

            var $widget = this;

            $widget.element.on('click', '.' + this.options.classes.optionClass, function () {
                return $widget._OnClick($(this), $widget);
            });

            $widget.element.on('change', '.' + this.options.classes.selectClass, function () {
                return $widget._OnChange($(this), $widget);
            });

            $widget.element.on('click', '.' + this.options.classes.moreButton, function (e) {
                e.preventDefault();

                return $widget._OnMoreClick($(this));
            });
        },

        /**
         * Event for swatch options
         *
         * @param {Object} $this
         * @param {Object} $widget
         * @private
         */
        _OnClick: function ($this, $widget) {
            var $parent = $this.parents('.' + $widget.options.classes.attributeClass),
                $label = $parent.find('.' + $widget.options.classes.attributeSelectedOptionLabelClass),
                attributeId = $parent.attr('attribute-id'),
                attributeCode = $parent.attr('attribute-code'),
                $input = $parent.find('.' + $widget.options.classes.attributeInput);

            if ($widget.inProductList) {
                $input = $widget.productForm.find(
                    '.' + $widget.options.classes.attributeInput + '[name="super_attribute[' + attributeId + ']"]'
                );
            }

            if ($this.hasClass('disabled')) {
                return;
            }

            if (!$this.hasClass('selected')) {
                $parent.attr('option-selected', $this.attr('option-id')).find('.selected').removeClass('selected');
                $label.text($this.attr('option-label'));
                $input.val($this.attr('option-id'));
                $this.addClass('selected');
            }

            if ($this.attr('option-label')) {
                var attributeValue = $this.attr('option-label');
            } else {
                var attributeValue = $this.attr('option-value');
            }

            $widget._updateUrlParameter(attributeCode, attributeValue);
            $widget._Rebuild();

            if ($widget.element.parents($widget.options.selectorProduct)
                    .find(this.options.selectorProductPrice).is(':data(mage-priceBox)')
            ) {
                $widget._UpdatePrice();
            }

            // Do not trigger product media load if size attribute clicked
            if (attributeCode.indexOf('size') === -1) {
                $widget._LoadProductMedia();
            }

            $input.trigger('change');
            this.dataLayerPush($this, 'click');
            $widget._Validate();
        },

        /**
         * Updates url parameter with pushstate
         *
         * @param param
         * @param value
         * @private
         */
        _updateUrlParameter: function (param, value) {
            var parameters = $.parseQuery(),
                forbidden = this.options.queryParams.forbidden.indexOf(param) !== -1

            delete parameters[''];
            parameters[param] = value;

            if (typeof (history.replaceState) !== 'undefined' && !forbidden) {
                window.history.replaceState(
                    null,
                    null,
                    window.location.href.split('?')[0] + '?' + $.param(parameters)
                );
            }

            if (param === 'color') {
                this.options.colorPosition = this.options.documentTitle.indexOf(value) !== -1 ?
                    this.options.documentTitle.indexOf(value) : this.options.documentTitle.indexOf(this.options.titleSuffix);

                this.options.documentTitle = this.options.documentTitle.replace(value, '');

                document.title = this.options.documentTitle.slice(0, this.options.colorPosition) + value
                    + ' ' + this.options.documentTitle.slice(this.options.colorPosition)
            }
        },

        /**
         * Event for select
         *
         * @param {Object} $this
         * @param {Object} $widget
         * @private
         */
        _OnChange: function ($this, $widget) {
            var $parent = $this.parents('.' + $widget.options.classes.attributeClass),
                attributeId = $parent.attr('attribute-id'),
                attributeCode = $parent.attr('attribute-code'),
                $input = $parent.find('.' + $widget.options.classes.attributeInput),
                sizeText = this.element.find('[attribute-code="' + attributeCode + '"] select option').filter(function (index) {
                    return $(this).val() === $this.val();
                }).text();

            if ($widget.inProductList) {
                $input = $widget.productForm.find(
                    '.' + $widget.options.classes.attributeInput + '[name="super_attribute[' + attributeId + ']"]'
                );
            }

            if ($this.val() > 0) {
                $parent.attr('option-selected', $this.val()).addClass('selected');
                $input.val($this.val());
            } else {
                $parent.removeAttr('option-selected').removeClass('selected');
                $input.val('');
            }

            $widget._updateUrlParameter(attributeCode, sizeText);
            $widget._Rebuild();
            $widget._UpdatePrice();
            $widget._LoadProductMedia();

            $input.trigger('change');
            this.dataLayerPush($this, 'change');
            $widget._Validate();
        },

        /**
         * Revalidate the inputs in case they're now correct
         */
        _Validate: function() {
            var $submitForm = $('#product_addtocart_form');

            $submitForm.validation();
            if ($submitForm.validate().numberOfInvalids() > 0) {
                $submitForm.validation('isValid');
            }
        },

        /**
         * Event for more switcher
         *
         * @param {Object} $this
         * @private
         */
        _OnMoreClick: function ($this) {
            $this.nextAll().show();
            $this.blur().remove();
        },

        /**
         * Rewind options for controls
         *
         * @private
         */
        _Rewind: function (controls) {
            controls.find('div[option-id], option[option-id]').removeClass('disabled').removeAttr('disabled');
            controls.find('div[option-empty], option[option-empty]').attr('disabled', true).addClass('disabled');
        },

        /**
         * Rebuild container
         *
         * @private
         */
        _Rebuild: function () {
            var $widget = this,
                controls = $widget.element.find('.' + $widget.options.classes.attributeClass + '[attribute-id]'),
                selected = controls.filter('[option-selected]'),
                colorAttribute = 'color',
                size = selected.filter("[class$='size']").find('select').val();

            // Enable all options
            $widget._Rewind(controls);

            // done if nothing selected
            if (selected.size() <= 0) { // If array size of selected swatches is under zero
                return;
            }

            // Disable not available options
            controls.each(function () {
                var $this = $(this),
                    id = $this.attr('attribute-id'),
                    products = $widget._CalcProducts(id);

                if (selected.size() === 1 && selected.first().attr('attribute-id') === id) {
                    return;
                }

                $this.find('[option-id]').each(function () {
                    var $element = $(this),
                        option = $element.attr('option-id'),
                        attribute = $element.closest('.swatch-attribute').attr('attribute-code'),
                        attributeId = $element.closest('.swatch-attribute').attr('attribute-id'),
                        swatchInputSelector = '.swatch-input[name="super_attribute[' + attributeId + ']"]',
                        optionMapItem = $widget.optionsMap[id][option];

                    /**
                     * Removes select if item is out of stock
                     */
                    if ((typeof optionMapItem !== 'undefined')
                        && (_.intersection(products, optionMapItem.products).length <= 0)
                        && attribute.includes('size')
                        && (size === option)) {
                        $element.parent('select').prop('selectedIndex', 0).selectric('refresh');
                    }

                    if (!$widget.optionsMap.hasOwnProperty(id) || !$widget.optionsMap[id].hasOwnProperty(option) ||
                        // $element.hasClass('selected') || (Temporarily solution for size buttons till switch to text swatch)
                        $element.is(':selected')) {
                        return;
                    }

                    if ((_.intersection(products, $widget.optionsMap[id][option].products).length <= 0) && (attribute !== colorAttribute)) {
                        // Remove selected class for not available option
                        if ($element.hasClass('selected')) {
                            $element.parents('.swatch-attribute').removeAttr('option-selected');
                            $element.removeClass('selected');
                        }

                        // If not available options selected, remove super attribute value
                        if ($(swatchInputSelector).val() === $element.attr('value')) {
                            $(swatchInputSelector).val('');
                        }

                        $($widget.options.stockMessageSelector).text('');

                        $element.attr('disabled', true).addClass('disabled');
                    } else if (attribute === colorAttribute) {
                        $element.removeAttr('disabled');
                    }
                });
                $('select').selectric('refresh');
            });
        },

        /**
         * Get selected product list
         * - In Kuru case: All available size-variation of current color
         * - Excludes products if the attribute isn't selected (in theory proper functionality)
         *
         * @returns {Array}
         * @private
         */
        _CalcProducts: function ($skipAttributeId) {
            var $widget = this,
                products = [];

            // Generate intersection of products
            $widget.element.find('.' + $widget.options.classes.attributeClass + '[option-selected]').each(function () {
                var id = $(this).attr('attribute-id'),
                    option = $(this).attr('option-selected');

                if ($skipAttributeId !== undefined && $skipAttributeId === id) {
                    return;
                }

                if (!$widget.optionsMap.hasOwnProperty(id) || !$widget.optionsMap[id].hasOwnProperty(option)) {
                    return;
                }

                if (products.length === 0) {
                    products = $widget.optionsMap[id][option].products;
                } else {
                    products = _.intersection(products, $widget.optionsMap[id][option].products);
                }
            });

            return products;
        },

        /**
         * Update total price
         *
         * @private
         */
        _UpdatePrice: function () {
            var $widget = this,
                $product = $widget.element.parents($widget.options.selectorProduct),
                $productPrice = $product.find(this.options.selectorProductPrice),
                options = _.object(_.keys($widget.optionsMap), {}),
                result;

            $widget.element.find('.' + $widget.options.classes.attributeClass + '[option-selected]').each(function () {
                var attributeId = $(this).attr('attribute-id');

                options[attributeId] = $(this).attr('option-selected');
            });

            var productId = _.findKey($widget.options.jsonConfig.index, options);

            if (productId !== undefined) {
                $($widget.options.stockMessageSelector).text($widget.options.jsonConfig.optionStock[productId]);
            }

            // KURU2-1559 Price is cached, avoid flicker on initialisation
            if ($widget.isInitialising) {
                return false;
            }

            result = $widget.options.jsonConfig.optionPrices[productId];

            /**
             * Ticket [KURU2-1062]
             *   If price can't be updated
             *   (Because all options aren't selected yet)
             *   Try setting price from the first size of current color
             *   (Implemented as set price from first matching configuration)
             **/
            if (typeof (result) === 'undefined') {
                /**
                 * configurationPrices is cloned so we can freely and mutably
                 * modify, filter the configurations and not delete anything
                 * from the original configuration source
                 **/
                var configurations = $widget.options.jsonConfig.index,
                    configurationPrices = $.extend({}, $widget.options.jsonConfig.optionPrices);

                /**
                 * Filter out configurations with matching options
                 */
                Object.keys(options).forEach(function(optionId) {
                    var option = options[optionId];

                    if (typeof(option) !== 'undefined') {
                        Object.keys(configurations).forEach(function (configId) {
                            var config = configurations[configId];

                            if (config[optionId] !== option) {
                                delete configurationPrices[configId];
                            }
                        });
                    }
                });

                /**
                 * Pick first matching option
                 */
                var configurationPriceValues = Object.keys(configurationPrices).map(function(key) {
                    return configurationPrices[key]
                });
                result = configurationPriceValues[0];
            }

            $productPrice.trigger(
                'updatePrice',
                {
                    'prices': $widget._getPrices(result, $productPrice.priceBox('option').prices)
                }
            );

            if (typeof result !== 'undefined' && result.oldPrice.amount !== result.finalPrice.amount) {
                $(this.options.slyOldPriceSelector).show();
            } else {
                $(this.options.slyOldPriceSelector).hide();
            }
        },

        /**
         * Get prices
         *
         * @param {Object} newPrices
         * @param {Object} displayPrices
         * @returns {*}
         * @private
         */
        _getPrices: function (newPrices, displayPrices) {
            var $widget = this;

            if (_.isEmpty(newPrices)) {
                newPrices = $widget.options.jsonConfig.prices;
            }

            _.each(displayPrices, function (price, code) {
                if (newPrices[code]) {
                    displayPrices[code].amount = newPrices[code].amount - displayPrices[code].amount;
                }
            });

            return displayPrices;
        },

        /**
         * Gets all product media and change current to the needed one
         *
         * @private
         */
        _LoadProductMedia: function () {
            var $widget = this,
                $this = $widget.element,
                attributes = {},
                additional = {},
                productId = 0,
                mediaCallData,
                mediaCacheKey,

                /**
                 * Processes product media data
                 *
                 * @param {Object} data
                 * @returns void
                 */
                mediaSuccessCallback = function (data) {
                    var mediaCache = $widget.options.mediaCache;
                    var mediaCacheLoaded = mediaCacheKey in mediaCache;

                    if (data.promise && data.promise.responseJSON) {
                        data = data.promise.responseJSON
                    }

                    if (!mediaCacheLoaded ||
                        mediaCacheLoaded && 'promise' in mediaCache[mediaCacheKey]
                    ) {
                        $widget.options.mediaCache[mediaCacheKey] = data;
                    }

                    if ($widget.requiredMediaCacheKey === mediaCacheKey &&
                        $widget.lastMediaCacheLoaded !== mediaCacheKey) {
                        $widget.lastMediaCacheLoaded = mediaCacheKey;
                        $widget._ProductMediaCallback($this, data);
                        $widget._DisableProductMediaLoader($this);
                        $widget.loadMediaData();
                    }
                };

            if (!$widget.options.mediaCallback) {
                return;
            }

            $this.find('[option-selected]').each(function () {
                var $selected = $(this);

                /* Do not add size attribute to media call data  */
                if ($selected.attr('attribute-code').indexOf('size') === -1) {
                    attributes[$selected.attr('attribute-code')] = $selected.attr('option-selected');
                    additional['color'] = $selected.find('.swatch-option.selected').attr('option-label');
                }

            });

            if ($('body.catalog-product-view').size() > 0) {
                //Product Page
                productId = document.getElementsByName('product')[0].value;
            } else {
                //Category View
                productId = $this.parents('.product.details.product-item-details')
                    .find('.price-box.price-final_price').attr('data-product-id');
            }

            mediaCallData = {
                'product_id': productId,
                'attributes': attributes,
                'additional': additional
            };

            mediaCacheKey = JSON.stringify(mediaCallData);
            $widget.requiredMediaCacheKey = mediaCacheKey;

            if (mediaCacheKey in $widget.options.mediaCache) {
                var mediaCache = $widget.options.mediaCache[mediaCacheKey];

                if ('promise' in mediaCache) {
                    // 2nd time of media key request - still loading, bind callback to promise
                    $.when(mediaCache.promise).done(function() {
                        mediaSuccessCallback($widget.options.mediaCache[mediaCacheKey]);
                    }.bind(this))
                } else {
                    // 3rd time of media key request - finished loading, trigger callback
                    mediaSuccessCallback(mediaCache);
                }
            } else {
                // 1st time of media key request - create network request, create promise
                mediaCallData.isAjax = true;
                $widget._EnableProductMediaLoader($this);
                var $mediaPromise = $.ajax({
                    type: 'GET',
                    url: $widget.options.mediaCallback,
                    data: mediaCallData,
                    dataType: 'json',
                    success: mediaSuccessCallback,
                    cache: true
                });
                $widget.options.mediaCache[mediaCacheKey] = {'promise': $mediaPromise};
            }
        },

        /**
         * Load all color swatch media images after page load
         */
        loadMediaData: function () {
            var $widget = this,
                $color,
                colorSwatchesSelector = '.swatch-attribute.color .swatch-option',
                attributes = {},
                additional = {},
                mediaCallData,
                mediaCacheKey,
                mediaSuccessCallback = function (data) {
                    $widget.options.mediaCache[mediaCacheKey] = data;
                };

            $(colorSwatchesSelector).each(function () {
                $color = $(this);

                attributes['color'] = $color.attr('option-id');
                additional['color'] = $color.attr('option-label');

                mediaCallData = {
                    'product_id': document.getElementsByName('product')[0].value,
                    'attributes': attributes,
                    'additional': additional
                };

                mediaCacheKey = JSON.stringify(mediaCallData);
                mediaCallData.isAjax = true;

                if (!(mediaCacheKey in $widget.options.mediaCache)) {
                    var $mediaPromise = $.ajax({
                        type: 'GET',
                        url: $widget.options.mediaCallback,
                        data: mediaCallData,
                        dataType: 'json',
                        success: mediaSuccessCallback,
                        async: true,
                        cache: true
                    });
                    $widget.options.mediaCache[mediaCacheKey] = {'promise': $mediaPromise};
                }
            })
        },

        /**
         * Enable loader
         *
         * @param {Object} $this
         * @private
         */
        _EnableProductMediaLoader: function ($this) {
            var $widget = this;

            if ($('body.catalog-product-view').size() > 0) {
                $this.parents('.column.main').find('.photo.image')
                    .addClass($widget.options.classes.loader);
            } else {
                //Category View
                $this.parents('.product-item-info').find('.product-image-photo')
                    .addClass($widget.options.classes.loader);
            }
        },

        /**
         * Disable loader
         *
         * @param {Object} $this
         * @private
         */
        _DisableProductMediaLoader: function ($this) {
            var $widget = this;

            if ($('body.catalog-product-view').size() > 0) {
                $this.parents('.column.main').find('.photo.image')
                    .removeClass($widget.options.classes.loader);
            } else {
                //Category View
                $this.parents('.product-item-info').find('.product-image-photo')
                    .removeClass($widget.options.classes.loader);
            }
        },

        /**
         * Callback for product media
         *
         * @param {Object} $this
         * @param {String} response
         * @private
         */
        _ProductMediaCallback: function ($this, response) {
            var isProductViewExist = $('body.catalog-product-view').size() > 0,
                $main = isProductViewExist ? $this.parents('.column.main') : $this.parents('.product-item-info'),
                $widget = this,
                images = [],

                /**
                 * Check whether object supported or not
                 *
                 * @param {Object} e
                 * @returns {*|Boolean}
                 */
                support = function (e) {
                    return e.hasOwnProperty('large') && e.hasOwnProperty('medium') && e.hasOwnProperty('small');
                };

            if (_.size($widget) < 1 || !support(response)) {
                this.updateBaseImage(this.options.mediaGalleryInitial, $main, isProductViewExist);

                return;
            }

            images.push({
                full: response.large,
                img: response.medium,
                thumb: response.small,
                isMain: true
            });

            if (response.hasOwnProperty('gallery')) {
                $.each(response.gallery, function () {
                    if (!support(this) || response.large === this.large) {
                        return;
                    }
                    images.push({
                        full: this.large,
                        img: this.medium,
                        thumb: this.small
                    });
                });
            }

            // Remove and add the Amasty label
            if (response.hasOwnProperty('label')) {
                this.removeAmastyLabel(response.label.container);
                var gallery = $('[data-gallery-role=gallery-placeholder]');
                if (!gallery.hasClass("slick-initialized")) {
                    gallery.on('init', function() {
                        $widget.setAmastyLabel(response.label.container, response.label.html);
                    });
                }
                else {
                    this.setAmastyLabel(response.label.container, response.label.html);
                }
            }

            this.updateBaseImage(images, $main, isProductViewExist);

            if (!$widget.initialGalleryRender) {
                $widget.initialGalleryRender = true;

                this.updateBaseImage(images, $main, isProductViewExist);
            }
        },

        /**
         * Appends the Amasty label html given int 'label' to 'container'
         *
         * @param {string} container
         * @param {string} label
         * @private
         */
        setAmastyLabel: function(container, label) {
            $(container).append(label);
        },

        /**
         * Removes the Amasty label
         *
         * @private
         */
        removeAmastyLabel: function(container) {
            var label = $(container).find('.amasty-label-container');
            if (label.length) {
                label.remove();
            }
        },

        /**
         * Check if images to update are initial and set their type
         * @param {Array} images
         */
        _setImageType: function (images) {
            var initial = this.options.mediaGalleryInitial[0].img;

            if (images[0].img === initial) {
                images = $.extend(true, [], this.options.mediaGalleryInitial);
            } else {
                images.map(function (img) {
                    img.type = 'image';
                });
            }

            return images;
        },

        /**
         * Update [gallery-placeholder] or [product-image-photo]
         * @param {Array} images
         * @param {jQuery} context
         * @param {Boolean} isProductViewExist
         */
        updateBaseImage: function (images, context, isProductViewExist) {
            var justAnImage = images[0],
                initialImages = this.options.mediaGalleryInitial,
                gallery = context.find(this.options.mediaGallerySelector).data('gallery'),
                imagesToUpdate,
                isInitial;

            if (isProductViewExist) {
                imagesToUpdate = images.length ? this._setImageType($.extend(true, [], images)) : [];
                isInitial = _.isEqual(imagesToUpdate, initialImages);

                if (this.options.gallerySwitchStrategy === 'prepend' && !isInitial) {
                    imagesToUpdate = imagesToUpdate.concat(initialImages);
                }

                if (gallery && gallery.updateData && (!this.options.initialBaseImageUpdate || !isInitial)) {
                    gallery.updateData(imagesToUpdate);
                }

                if (isInitial) {
                    $(this.options.mediaGallerySelector).AddFotoramaVideoEvents();
                }

                if (gallery && (!this.options.initialBaseImageUpdate || !isInitial)) {
                    gallery.first();
                }

                this.options.initialBaseImageUpdate = false;
            } else if (justAnImage && justAnImage.img) {
                context.find('.product-image-photo').attr('src', justAnImage.img);
            }
        },

        /**
         * Kill doubled AJAX requests
         *
         * @private
         */
        _XhrKiller: function () {
            var $widget = this;

            if ($widget.xhr !== undefined && $widget.xhr !== null) {
                $widget.xhr.abort();
                $widget.xhr = null;
            }
        },

        /**
         * Emulate mouse click on all swatches that should be selected
         * @param {Object} [selectedAttributes]
         * @private
         */
        _EmulatePreselection: function (selectedAttributes) {
            var select, input, elIndex,
                optionSelector, optionIdInText, optionIdInProperty, optionLabelInProperty, option = null;

            delete selectedAttributes[''];

            $.each(selectedAttributes, $.proxy(function (attributeCode, optionId) {
                if (!attributeCode) {
                    return true;
                }

                /**
                 * Find for selectable attribute
                 * 1. option (selected, visible FE selectable element)
                 * 2. input (hidden form input for request sending)
                 */
                var attribute = $('.swatch-attribute.' + attributeCode);
                optionSelector = '[attribute-code="' + attributeCode + '"] select option';
                optionSelector += ', [attribute-code="' + attributeCode + '"] .swatch-attribute-options .swatch-option';
                option = this.element.find(optionSelector).filter(function () {
                    optionIdInText = $(this).text() === optionId;
                    // Type coercion on comparision here is on purpose
                    // The optionId and attribute/content-text types can't be known for certain.
                    optionIdInProperty = $(this).attr('option-id') == optionId;
                    optionLabelInProperty = $(this).attr('option-label') == optionId;

                    return optionIdInText || optionIdInProperty || optionLabelInProperty;
                });

                input = this.element.find('[attribute-code="' + attributeCode + '"] .swatch-input');

                /**
                 * Invalid option - Remove from url and ignore
                 */
                if (option.length === 0) {
                    this._removeURLQuery(attributeCode, optionId);

                    return true; // "true" return value is defined as skip for the '$.each' loop we're in
                }

                /**
                 * Option forbidden in URL - Remove from url and continue
                 */
                if (this.options.queryParams.forbidden.indexOf(attributeCode) !== -1) {
                    this._removeURLQuery(attributeCode, optionId);
                }

                /**
                 * Handle Selectric updates
                 */
                select = this.element.find('[attribute-code="' + attributeCode + '"] select');
                if (select && (option.hasClass('disabled') || option.length === 0)) {
                    // If option disabled, deselect on selectric
                    select.prop('selectedIndex', 0).selectric('refresh');
                } else if (select && option.index() > 0) {
                    // If option available, set on selectric
                    select.prop('selectedIndex', option.index()).selectric('refresh');
                }

                /**
                 * Handle button updates
                 */
                var isAvailable = this._getIsOptionAvailable(attributeCode, option.attr('option-id'))

                if (option.hasClass('swatch-option') && isAvailable) {
                    option.click();
                }
            }, this));
        },

        /**
         * Removes url query
         *
         * @param attribute
         * @param value
         * @private
         */
        _removeURLQuery: function (attribute) {
            var parameters = $.parseQuery(),
                ignoredParameter = this.options.queryParams.ignored.indexOf(attribute) !== -1;

            delete parameters[''];

            if (!ignoredParameter) {
                delete parameters[attribute];
            }

            if (typeof (history.replaceState) !== 'undefined') {
                window.history.replaceState(
                    null,
                    null,
                    window.location.href.split('?')[0] + '?' + $.param(parameters)
                );
            }
        },

        /**
         * Get default options values settings with either URL query parameters
         * @private
         */
        _getSelectedAttributes: function () {
            var hashIndex = window.location.href.indexOf('#'),
                selectedAttributes = {},
                params;

            if (hashIndex !== -1) {
                params = $.parseQuery(window.location.href.substr(hashIndex + 1));

                selectedAttributes = _.invert(_.mapObject(_.invert(params), function (attributeId) {
                    var attribute = this.options.jsonConfig.attributes[attributeId];

                    return attribute ? attribute.code : attributeId;
                }.bind(this)));
            }

            return selectedAttributes;
        },

        /**
         * Returns attributes, which aren't selected in the URL
         * @private
         */
        _getBlankAttributes: function() {
            var selectedAttributes = $.parseQuery();
            var blankAttributes = [];

            delete selectedAttributes[''];

            $.each(this.options.jsonConfig.attributes, $.proxy(function (identifier, attribute) {
                if (!(attribute.code in selectedAttributes)) {
                    blankAttributes[attribute.code] = attribute.options[0].id;
                }
            }, this));

            return blankAttributes;
        },

        // Alias for is not out of stock
        _getIsOptionAvailable: function(attributeCode, optionId) {
            return !this._isOutOfStock(attributeCode, optionId);
        },

        _isOutOfStock: function(attributeCode, optionId) {
            var attributeId = this._getAttributeId(attributeCode);
            var optionMapItem = this.optionsMap[attributeId][optionId];
            var products = this._CalcProducts(attributeId)

            return typeof optionMapItem !== 'undefined' &&
                _.intersection(products, optionMapItem.products).length <= 0 &&
                attributeCode !== 'color';
        },

        _getAttributeId: function(attributeCode) {
            var productAttributes = this.options.jsonConfig.attributes,
                attributeId = 0;

            $.each(productAttributes, function(index, attribute) {
                if (attribute.code === attributeCode) {
                    attributeId = attribute.id;

                    return false; // $.each :: break
                }
            });

            return attributeId;
        },

        /**
         * Callback which fired after gallery gets initialized.
         *
         * @param {HTMLElement} element - DOM element associated with a gallery.
         */
        _onGalleryLoaded: function (element) {
            var galleryObject = element.data('gallery');

            this.options.mediaGalleryInitial = galleryObject.returnCurrentImages();
        },

        /**
         * Perform attribute pushes
         *
         * @param $this
         * @param type
         * @returns {null}
         */
        dataLayerPush: function ($this, type) {
            if (document.readyState !== 'complete') {
                return null
            }
            if (typeof(dataLayer) === 'undefined') {
                return null;
            }
            var productPush = '', updatePush = {}, productUpdated = false;
            dataLayer.forEach(function (element) {
                if ((element['event'] === 'detail') ||
                    (element['event'] === 'productUpdate')) {
                    // element has to be dereferenced or else
                    // dataLayer state won't be immutable
                    productPush = JSON.parse(JSON.stringify(element));
                }
            });
            if (productPush.length <= 0) {
                return null;
            }
            if (!this.checkPushStructure(productPush)) {
                return null;
            }

            if ('detail' in productPush['ecommerce']) {
                var ecommerceDetail = productPush['ecommerce']['detail'];
            } else {
                var ecommerceDetail = productPush['ecommerce']['click'];
            }
            var oldName = ecommerceDetail['products'][0]['name'];
            var oldBrand = ecommerceDetail['products'][0]['brand'];
            var newName = this.buildProductName(oldBrand, $this, type);
            var newPrice = this.getProductPrice();

            if (oldName !== newName) {
                updatePush['event'] = 'productUpdate';
                updatePush['ecommerce'] = { 'click': ecommerceDetail };
                updatePush['ecommerce']['click']['products'][0]['name'] = newName;
                updatePush['ecommerce']['click']['products'][0]['id'] = this.getProductDuplicateSku();
                updatePush['ecommerce']['click']['products'][0]['dimension1'] = this.detectActiveColor();
                updatePush['ecommerce']['click']['products'][0]['list'] = 'PDP_ColorSwatch';
                updatePush['ecommerce']['click']['products'][0]['position'] = this.detectPosition();
                if (newPrice) {
                    updatePush['ecommerce']['click']['products'][0]['price'] = newPrice;
                }

                dataLayer.push(updatePush);

                // The same info is also necessary as a 'detail' event for consistency
                // ^ SEO team comment
                updatePush = JSON.parse(JSON.stringify(updatePush)); // Data must be immutable, so clone
                updatePush['event'] = 'PDP_View';
                updatePush['ecommerce']['detail'] = updatePush['ecommerce']['click'];
                delete updatePush['ecommerce']['click'];
                dataLayer.push(updatePush);
            }
        },

        /**
         * Validate push
         *
         * @param push
         * @returns {boolean}
         */
        checkPushStructure: function (push) {
            // Test deep structure vanilla syntax - https://stackoverflow.com/a/4034468
            var detailValid = ((((((push || {}).ecommerce || {}).detail || {}).products) || {})[0] || {}).name;
            var clickValid = ((((((push || {}).ecommerce || {}).click || {}).products) || {})[0] || {}).name;

            return detailValid || clickValid;
        },


        /**
         * Get price of currently selected product
         */
        getProductPrice: function() {
            var firstConfig = this.getFirstMatchingConfiguration();
            if (firstConfig && firstConfig.finalPrice) {
                var price = firstConfig.finalPrice.amount;

                if (price.indexOf('.') === -1) {
                    price += '.00';
                }

                return price;
            } else {
                return undefined;
            }
        },

        /**
         * Get duplicate sku of current selected product
         */
        getProductDuplicateSku: function() {
            return this.getProductSku().slice(0, 6);
        },

        /**
         * Get child product sku
         */
        getProductSku: function () {
            var firstConfig = this.getFirstMatchingConfiguration();
            if (firstConfig) {
                return firstConfig.productSku;
            } else {
                return '';
            }
        },

        getFirstMatchingConfiguration: function () {
            var $widget = this,
                options = _.object(_.keys($widget.optionsMap), {}),
                result;

            /**
             * Default functionality for fully filled attributes
             */
            $widget.element.find('.' + $widget.options.classes.attributeClass + '[option-selected]').each(function () {
                var attributeId = $(this).attr('attribute-id');

                options[attributeId] = $(this).attr('option-selected');
            });

            /**
             * configurationPrices is cloned so we can freely and mutably
             * modify, filter the configurations and not delete anything
             * from the original configuration source
             **/
            var configurations = $widget.options.jsonConfig.index,
                configurationPrices = $.extend({}, $widget.options.jsonConfig.optionPrices);

            /**
             * Filter out configurations with matching options
             */
            Object.keys(options).forEach(function (optionId) {
                var option = options[optionId];

                if (typeof(option) !== 'undefined') {
                    Object.keys(configurations).forEach(function (configId) {
                        var config = configurations[configId];

                        if (config[optionId] !== option) {
                            delete configurationPrices[configId];
                        }
                    });
                }
            })

            /**
             * Pick first matching option
             */
            var configurationPriceValues = Object.keys(configurationPrices).map(function(key) {
                return configurationPrices[key]
            })

            return configurationPriceValues[0];
        },

        /**
         * Construct product name
         *
         * @param baseName
         * @param $this
         * @param type
         * @returns {string}
         */
        buildProductName: function (baseName, $this, type) {
            var postfix = '';

            switch (type) {
                case 'click':
                    postfix = " - " + $this.attr('option-label');
                    break;
                case 'change':
                    postfix = ' - ' + this.detectActiveColor();
                    break;
            }
            return baseName + postfix;
        },

        /**
         * Return attribute value of active color
         */
        detectActiveColor: function() {
            return jQuery('.swatch-attribute.color .swatch-option.selected').attr('option-label');
        },

        /**
         * Return attribute value of active color
         */
        detectPosition: function() {
            // 1 based indexing (starts with 1 not zero)
            return jQuery('.swatch-attribute.color .swatch-option.selected').index() + 1;
        }

    });
    return $.mage.SwatchRenderer;
});
