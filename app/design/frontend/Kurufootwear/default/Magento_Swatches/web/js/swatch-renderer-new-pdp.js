/**
 * @category     Kurufootwear
 * @package      Kurufootwear/Swatches
 * @author       Aleksejs Baranovs <info@scandiweb.com>
 * @copyright    Copyright (c) 2018 KURU Footwear. All rights reserved.
 */

define([
    'jquery',
    'underscore',
    'mage/translate',
    'jquery/ui',
    'jquery/jquery.parsequery',
    'selectric',
    'arrive',
    'Magento_Swatches/js/swatch-renderer'
], function ($, _, $t) {
    'use strict';

    /**
     * Render swatch controls with options and use tooltips.
     */
    $.widget('mage.SwatchRenderer', $.mage.SwatchRenderer, {
        classes: {
            desktopGalleryPlaceholder: '.desktop-gallery-placeholder',
            swatchAttributeLabel: '.swatch-attribute .swatch-attribute-label'
        },

        // flag to check first base image update
        initialBaseImageUpdate: true,

        /**
         * Update [gallery-placeholder] or [product-image-photo]
         *
         * @param {Array} images
         * @param {jQuery} context
         * @param {Boolean} isProductViewExist
         */
        updateBaseImage: function (images, context, isProductViewExist) {
            if (isProductViewExist) {
                var imagesToUpdate = images.length ? this._setImageType($.extend(true, [], images)) : [];

                if (!this.options.initialBaseImageUpdate) {
                    this.changeDesktopImages(imagesToUpdate);
                }

                this.options.initialBaseImageUpdate = false;
            }

            this._super(images, context, isProductViewExist);
        },

        /**
         * Change desktop images
         *
         * @param images
         */
        changeDesktopImages: function (images) {
            var html = '',
                iterator = 0,
                first;

            $(this.classes.desktopGalleryPlaceholder).fadeOut(200, function() {
                $(this).empty();

                $.each(images, function () {
                    first = iterator === 0 ? 'first' : '';
                    html += '<img src="' + this.img + '" class="desktop-image ' + first + '" href="' + this.full + '">';
                    iterator++;
                });

                $(this).append(html).fadeIn(200);
            });
        },

        /**
         * Render controls
         */
        _RenderControls: function () {
            this._super();
            
            $(this.classes.swatchAttributeLabel).each(function () {
                $(this).text($t('Choose') + ' ' + $(this).text())
            })
        }
    });

    return $.mage.SwatchRenderer;
});