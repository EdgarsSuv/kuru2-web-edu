/**
 * @vendor      Kurufootwear
 * @package     Kurufootwear_Customer
 * @author      Aleksejs Baranovs <info@scandiweb.com>
 * @copyright   Copyright (c) 2018 Scandiweb, Inc (http://scandiweb.com)
 * @license     http://opensource.org/licenses/OSL-3.0 The Open Software License 3.0 (OSL-3.0)
 */

define([
    'jquery',
    'maskInput'
], function ($) {
    'use strict';

    return {
        options: {
            countrySelector: '.form-address-edit [name=country_id]',
            phoneSelector: '.form-address-edit [name=telephone]',
            placeholder: ''
        },

        /**
         * Initialize
         */
        initialize: function () {
            var self = this;

            if ($(self.options.countrySelector).val() === 'US') {
                self.options.placeholder = $(self.options.phoneSelector).attr('placeholder');
                $(self.options.phoneSelector).mask('(000) 000-0000', {placeholder: "(___)___-____"});
            }

            self.triggerCountryChange($(self.options.phoneSelector), $(self.options.countrySelector));
        },

        /**
         * Destruct mask
         *
         * @param el
         */
        destructMask: function (el) {
            var self = this;
            
            el.unmask();
            el.attr('placeholder', self.options.placeholder);
        },

        /**
         * Trigger country change
         *
         * @param el
         * @param country
         */
        triggerCountryChange: function (el, country) {
            var self = this;

            country.change(function () {
                if ($(this).val() !== 'US') {
                    self.destructMask(el);
                } else {
                    el.mask('(000) 000-0000', {placeholder: "(___)___-____"});
                }
            })
        }
    }
});
