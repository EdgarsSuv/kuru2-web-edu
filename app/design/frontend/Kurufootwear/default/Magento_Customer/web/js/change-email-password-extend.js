/**
 * @vendor      Kurufootwear
 * @package     Kurufootwear_Customer
 * @author      Aleksejs Baranovs <info@scandiweb.com>
 * @copyright   Copyright (c) 2018 Scandiweb, Inc (http://scandiweb.com)
 * @license     http://opensource.org/licenses/OSL-3.0 The Open Software License 3.0 (OSL-3.0)
 */

define([
    'jquery',
    'jquery/ui',
    'Magento_Customer/change-email-password'
], function ($) {
    'use strict';

    $.widget('kf.changeEmailPassword', $.mage.changeEmailPassword, {
        /**
         * Show email and password input fields
         */
        _showAll: function () {
            $(this.options.titleSelector).html(this.options.titleChangeEmailAndPassword);

            $(this.options.mainContainerSelector).show();
            $(this.options.emailContainerSelector).show();
            $(this.options.newPasswordContainerSelector).show();
            $(this.options.confirmPasswordContainerSelector).show();

            $(this.options.currentPasswordSelector).attr('data-validate', '{required:true}').prop('disabled', false);
            $(this.options.emailSelector).attr('data-validate', '{required:true}').prop('disabled', false);
            $(this.options.newPasswordSelector).attr(
                'data-validate',
                '{required:true, \'validate-customer-password-custom\':true}'
            ).prop('disabled', false);
            $(this.options.confirmPasswordSelector).attr(
                'data-validate',
                '{required:true, equalTo:"' + this.options.newPasswordSelector + '"}'
            ).prop('disabled', false);
        }
    });

    return $.kf.priceOptions;
});
