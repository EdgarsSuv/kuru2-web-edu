/**
 * @vendor Kurufootwear
 * @module Kurufootwear_Customer
 * @author Krisjanis Veinbahs <info@scandiweb.com>
 * @copyright Copyright (c) 2018 KURU Footwear. All rights reserved.
 */
/**
 * Overridden, because customer-data triggers section-config methods,
 * when the internal data hasn't been initialised yet. So this override
 * creates an API, that provides a promise that's resolved after internal
 * data has been loaded.
 * Couldn't have been a mixin, because x-magento-init didn't trigger
 * the internal data initialisation function on the mixin, only on the
 * original section-config.js
 *
 * Reference - KURU2-1667
 * Reference - https://github.com/magento/magento2/issues/17125
 */
define(['underscore', 'jquery'], function (_, $) {
    'use strict';

    var baseUrls, sections, clientSideSections;

    var canonize = function(url){
        var route = url;
        for (var key in baseUrls) {
            var trailingSlashBaseUrl = baseUrls[key].substr(-1,1) === '/' ?
                baseUrls[key] :
                baseUrls[key] + '/';

            route = url.replace(trailingSlashBaseUrl, '');
            if (route != url) {
                break;
            }
        }
        return route.replace(/^\/?index.php\/?/, '').toLowerCase();
    };

    return {
        getAffectedSections: function (url) {
            var route = canonize(url);
            var actions = _.find(sections, function(val, section) {
                if (section.indexOf('*') >= 0) {
                    section = section.replace(/\*/g, '[^/]+') + '$';
                    var matched = route.match(section);
                    return matched && matched[0] == route;
                }
                return (route.indexOf(section) === 0);
            });

            return _.union(_.toArray(actions), _.toArray(sections['*']));
        },

        filterClientSideSections: function (sections) {
            if (Array.isArray(sections)) {
                return _.difference(sections, clientSideSections);
            }
            return sections;
        },

        isClientSideSection: function (sectionName) {
            return _.contains(clientSideSections, sectionName);
        },

        /**
         * Promises of affected sections
         */
        sectionPromises: [],

        /**
         * Return and store promise of affected sections
         * or return actual affected sections
         *
         * @param url
         * @returns {*}
         */
        getAffectedSectionsPromise: function(url) {
            var deferred = $.Deferred();

            // If sections are already loaded, don't promise, just return
            if (typeof(sections) !== 'undefined') {
                return this.getAffectedSections(url);
            }

            this.sectionPromises.push({
                promise: deferred,
                url: url
            });

            return deferred.promise();
        },

        /**
         * Resolve all stored affected section promises
         *
         * Note: Should be run internally, when all section data is loaded
         */
        resolvePromises: function() {
            var self = this;

            this.sectionPromises.forEach(function(bound) {
                var affectedSections = self.getAffectedSections(bound.url)

                bound.promise.resolve(affectedSections);
            });

            this.sectionPromises = [];
        },

        'Magento_Customer/js/section-config': function(options) {
            baseUrls = options.baseUrls;
            sections = options.sections;
            clientSideSections = options.clientSideSections;

            this.resolvePromises();
        }
    };
});
