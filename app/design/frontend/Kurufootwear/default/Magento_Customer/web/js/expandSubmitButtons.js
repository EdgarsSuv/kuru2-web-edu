/**
 * @vendor      Kurufootwear
 * @package     Kurufootwear_Customer
 * @author      Krisjanis Veinbahs <krisjanisv@scandiweb.com>
 * @copyright   Copyright (c) 2018 Scandiweb, Inc (http://scandiweb.com)
 * @license     http://opensource.org/licenses/OSL-3.0 The Open Software License 3.0 (OSL-3.0)
 */
define([
    'jquery',
    'maskInput'
], function ($) {
    'use strict';

    return {
        /**
         * Bind collapse/expand & form submission
         * functionality to login button
         */
        initializeLoginButton: function() {
            var $registrationDropdown = $('.registration-dropdown'),
                $loginDropdown = $('.login-dropdown'),
                $loginDropdownToggle = $loginDropdown.find('.actions-toolbar'),
                $loginButton = $loginDropdown.find('.login.button'),
                $loginForm = $loginDropdown.closest('.form-login');

            $loginButton.click(function(event) {
                if ($loginDropdown.hasClass('open')) {
                    $loginForm.submit();
                } else {
                    $registrationDropdown.collapsible('deactivate');
                    $loginDropdown.collapsible('activate');
                }

                event.stopImmediatePropagation();
            });

            /**
             * The trigger is bound to the actions-toolbar,
             * although I want it to be bound to sign in (collapsible widget restrictions)
             * Therefore if login button wasn't clicked, ignore the clicks.
             */
            $loginDropdownToggle.click(function(event) {
                if (!$loginButton.is(':focus')) {
                    event.stopImmediatePropagation();
                }
            })
        },

        /**
         * Bind collapse/expand & form submission
         * functionality to login button
         */
        initializeRegisterButton: function() {
            var $loginDropdown = $('.login-dropdown'),
                $registrationDropdown = $('.registration-dropdown'),
                $registrationButton = $registrationDropdown.find('.create.button'),
                $registrationForm = $registrationDropdown.find('.form-create-account');

            $registrationButton.click(function(event) {
                if ($registrationDropdown.hasClass('open')) {
                    $registrationForm.submit();
                } else {
                    $loginDropdown.collapsible('deactivate');
                    $registrationDropdown.collapsible('activate');
                }

                event.stopImmediatePropagation();
            });
        }
    };
});
