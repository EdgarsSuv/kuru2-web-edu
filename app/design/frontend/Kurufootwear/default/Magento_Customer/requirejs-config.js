/**
 * @vendor      Kurufootwear
 * @package     Kurufootwear_Customer
 * @author      Aleksejs Baranovs <info@scandiweb.com>
 * @copyright   Copyright (c) 2018 Scandiweb, Inc (http://scandiweb.com)
 * @license     http://opensource.org/licenses/OSL-3.0 The Open Software License 3.0 (OSL-3.0)
 */

var config = {
    map: {
        '*': {
            changeEmailPassword: 'Magento_Customer/js/change-email-password-extend',
            initMask: 'Magento_Customer/js/initialize-mask',
            expandSubmitButtons: 'Magento_Customer/js/expandSubmitButtons'
        }
    }
};
