/**
 * @vendor      Kurufootwear
 * @theme       Kurufootwear\default
 * @author      Krisjanis Veinbahs <krisjanisv@scandiweb.com>
 * @copyright   Copyright (c) 2018 KURU Footwear. All rights reserved.
 */
define([
    'jquery'
], function ($) {
    'use strict';

    /**
     * ErrorLogger object for logging window errors to GTM
     */
    var ErrorLogger = {
        /**
         * Constants and configurations
         */
        options: {
            gtmLoadEvent: 'gtm_loaded',
            errorEventName: 'javascript-error'
        },

        /**
         * Custom window.dataLayer on-load event
         *
         * @param callback
         */
        onDataLayerLoad: function(callback) {
            if (!!window.google_tag_manager) {
                callback()
            } else {
                window.addEventListener(this.gtmLoadEvent, function() {
                    callback()
                });
            }
        },

        /**
         * Log global window error to GTM dataLayer
         *
         * @param msg
         * @param url
         * @param lineNo
         * @param columnNo
         * @param error
         */
        logError: function(msg, url, lineNo, columnNo, error) {
            var stackTrace;

            try {
                stackTrace = error.stack;
            } catch (exception) {
                // Browser does not support error stack traces
                stackTrace = '';
            }

            var messageEvent = {
                event: this.options.errorEventName,
                message: msg,
                stack: stackTrace,
                url: window.location.href
            }

            window.dataLayer.push(messageEvent)
        },

        /**
         * Logger initialisation
         */
        init: function() {
            var logger = this;

            this.onDataLayerLoad(function () {
                window.onerror = function (msg, url, lineNo, columnNo, error) {
                    try {
                        logger.logError.bind(logger)(msg, url, lineNo, columnNo, error);
                    } catch (error) {
                        /**
                         * Just in case error logging throws an error,
                         * don't fall into a recursion loop
                         */
                        return false;
                    }
                }
            })
        }
    };

    ErrorLogger.init();
});