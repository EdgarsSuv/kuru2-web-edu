/**
 * @category  Kurufootwear
 * @package   Kurufootwear\Catalog
 * @author    Krisjanis Veinbahs <krisjanisv@scandiweb.com>
 * @copyright Copyright (c) 2017 Scandiweb, Inc (http://scandiweb.com)
 * @license   http://opensource.org/licenses/OSL-3.0 The Open Software License 3.0 (OSL-3.0)
 */

define([
    'jquery',
    'jquery/ui',
    'js/vendor/jquery_sticky',
    'arrive'
], function($) {
    function stickFilters(element) {
        var $element = $(element);
        if ($element.find('.items-wrapper').length > 0) {
            var $header = $('.page-header');
            var headerHeight = $header.outerHeight() - 2;

            $element.sticky({
                topSpacing: headerHeight,
                getWidthFrom: 'body',
                zIndex: 2,
                wrapperClassName: 'sticky-filters-wrapper'
            });

            setTimeout(function () {
                $element.sticky('update');
            }, 500);
        }
    }

    $(document).ready(function() {
        var $filters = $('.state-filters-wrapper');
        var $header = $('.page-header');
    
        // Check for the clear filters button to know whether or not to show the filters block
        if ($('.filter-clear').length > 0) {
            $('.state-filter-background').css('height', 'auto').css('visibility', 'visible');
        }
        else {
            $('.state-filter-background').css('visibility', 'visible');
        }

        $header.sticky({
            topSpacing: 0,
            zIndex: 5,
            wrapperClassName: 'sticky-header-wrapper'
        });

        $(document).on('dropdowndialogclose dropdowndialogopen', function() {
            if ($header) {
                $header.sticky('update');
            }
        });

        stickFilters($filters);
        $(document).arrive('.state-filters-wrapper', function(element) {
            stickFilters(element);
        });
        
        $('.filter-actions.up-scroll').click(function() {
            window.scrollTo(0, 0);
            var opened = $('#layered-filter-block .block-title').attr('aria-expanded');
            if (opened === 'false') {
                if (window.innerWidth < 1024) {
                    $('#layered-filter-block').accordion().accordion('activate', []);
                } else {
                    $('#layered-filter-block').accordion().accordion('activate');
                }
            }
        });
    });
});
