/**
 * @vendor Scandiweb
 * @theme Kurufootwear_default
 * @copyright Copyright (c) 2016 Magento. All rights reserved.
 * @license http://opensource.org/licenses/afl-3.0.php Academic Free License (AFL 3.0)
 */
require(['jquery', 'tippy', 'arrive'], function($, tippy, arrive) {
    'use strict';

    /**
     * Tippy.js tooltip implementation
     * https://atomiks.github.io/tippyjs/
     */
    var tooltips = '.swatch-option, .tooltip-toggle, .option-label';

    /**
     * Transform tooltip attributes into
     * title attribute
     *
     * @param $elements
     */
    function tippify($elements) {
        var agent =/Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent),
            body = $('body');

        $.each($elements, function(index, element) {
            var $element = $(element);
            var content = '';

            var colorClass = 'kuru-tooltip-color';
            var imageClass = 'kuru-tooltip-image';
            var textClass = 'kuru-tooltip-text';

            var option = $element.attr('option-type');
            var colorOption = ['1', '4', 'color'];
            var imageOption = ['2', 'image'];
            var media = $element.attr('option-tooltip-value');
            var text = $element.attr('option-label');
            var showMediaAttribute = $element.attr('option-tooltip-media');
            var showMedia = typeof(showMediaAttribute) === 'undefined' ? true : showMediaAttribute;

            if (agent === true || body.hasClass('catalog-product-view')) {
                showMedia = false;
            }

            // Get media
            if (showMedia === true) {
                if (colorOption.indexOf(option) !== -1) {
                    // Get colored background
                    if (media) {
                        var $divTag = $('<div/>', {
                            class: colorClass,
                            style: 'background: ' + media + ';'
                        });
                        content += $divTag.outerHtml();
                    }
                } else if (imageOption.indexOf(option) !== -1) {
                    // Get image
                    if (media) {
                        var $imgTag = $('<img/>', {
                            class: imageClass,
                            src: media
                        });
                        content += $imgTag.outerHtml();
                    }
                }
            }

            // Get text
            if (agent === true  && body.hasClass('catalog-category-view')) {
                content = '';
            } else if (text) {
                var $pTag = $('<p/>', {
                    class: textClass
                }).text(
                    text
                );
                content += $pTag.outerHtml();
            }

            $element.attr('title', content)
                .attr('onClick', '');
        });
    }

    function initializeTooltips($tooltips) {
        tippify($tooltips);
        tippy(tooltips, {
            distance: 20,
            duration: 0,
            theme: 'kuru-tooltip',
            zIndex: 1
        });
    }

    /**
     * Return jquery elements' html
     * including its own tags
     */
    $.fn.outerHtml = function () {
        return $('<div>').append(this.clone()).html();
    };

    /**
     * Initialize tooltips
     */
    $(function() {
        // Initialize immediately
        initializeTooltips($(tooltips));

        // Initialize on async arrival
        $(document).arrive(tooltips, function(newTooltips) {
            initializeTooltips($(newTooltips));
        });
    });
});
