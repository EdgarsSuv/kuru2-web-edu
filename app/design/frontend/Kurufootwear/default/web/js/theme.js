/**
 * @vendor Scandiweb
 * @theme Kurufootwear_default
 * @copyright Copyright (c) 2016 Magento. All rights reserved.
 * @license http://opensource.org/licenses/afl-3.0.php Academic Free License (AFL 3.0)
 */
require(['js/scrollFix', 'jquery', 'jquery/ui', 'selectric', 'arrive', 'slick'], function(scrollFix, $) {
    'use strict';

    function updateSlider(elem) {
        var $elem = $(elem);

        $elem.on('input change update', function(e){
            var min = e.target.min,
                max = e.target.max,
                val = e.target.value;

            $(e.target).css({
                'backgroundSize': (val - min) * 100 / (max - min) + '% 100%'
            });
        }).trigger('input');
    }

    function initSelectric() {
        function init(delay) {
            setTimeout(function() {
                $.each($('select'), function(index, select) {
                    /**
                     * Wait for new select values to load
                     * before doing any calculations
                     */
                    var $select = $(select);
                    if ($select.is('.swatch-select.size')) {
                    }
                    $select.selectric({
                        disableOnMobile: false,
                        nativeOnMobile: false
                    });
                    setPlaceholder($select);
                    $select.selectric('refresh');
                    inheritHide($select);
                });
            }, delay);
        }

        /**
         * Repopulate selectric's on page init, load
         * and self change
         */
        init(0);
        $(window).load(init.bind(null, 500));
        $('select').change(init.bind(null, 20));
    }

    function inheritHide(select) {
        var $select = $(select);
        var $selectric = $select.closest('.selectric-wrapper');
        if ($select.css('display') === 'none') {
            $selectric.hide(0);
        } else {
            $selectric.show(0);
        }
    }

    function setPlaceholder($select) {
        var $placeholderOption = $select
            .find('option')
            .first();
        var placeholder = $select.attr('placeholder');
        var $label = $select
            .closest('.selectric-wrapper')
            .find('.label');

        if ($placeholderOption.attr('value') === '') {
            $placeholderOption.text(placeholder);
        }

        if ($select.prop('selectedIndex') === 0) {
            $label.addClass('placeholder');
        } else {
            $label.removeClass('placeholder');
        }
    }

    function formatStars($stars) {
        $stars.each(function(index, star) {
            var $star = $(star);
            var uncut = $star.text();
            var cut = uncut.match(/[0-9]*/);
            var formatted = '(' + cut[0] + ')';
            if (cut[0] !== '') {
                $star.text(formatted);
            }
        });
    }

    function PopupCenter(url, title, w, h) {
        // Fixes dual-screen position                         Most browsers      Firefox
        var dualScreenLeft = window.screenLeft !== undefined ? window.screenLeft : screen.left;
        var dualScreenTop = window.screenTop !== undefined ? window.screenTop : screen.top;

        var width = window.innerWidth ? window.innerWidth : document.documentElement.clientWidth ? document.documentElement.clientWidth : screen.width;
        var height = window.innerHeight ? window.innerHeight : document.documentElement.clientHeight ? document.documentElement.clientHeight : screen.height;

        var left = ((width / 2) - (w / 2)) + dualScreenLeft;
        var top = ((height / 2) - (h / 2)) + dualScreenTop;
        var newWindow = window.open(url, title, 'scrollbars=yes, width=' + w + ', height=' + h + ', top=' + top + ', left=' + left);

        // Puts focus on the newWindow
        if (window.focus) {
            newWindow.focus();
        }
    }

    $(document).ready(function() {
        initSelectric();
        $(document).arrive('select', function(e) {
            initSelectric();
        });

        var stars = '.text-m:not(.ask-question)';
        formatStars($(stars));
        $(document).arrive(stars, function(e) {
            formatStars($(stars));
        });

        var width = $(window).width();

        $('.logo img').hover(
            function () {
                var src = $(this).attr('src');
                $(this).attr('src', src.replace(/\.png$/i, '.gif'));
            },
            function () {
                var src = $(this).attr('src');
                $(this).attr('src', src.replace(/\.gif$/i, '.png'));
            });

        updateSlider('input[type=range]');
        $(document).arrive('input[type=range]', function(elem) {
            updateSlider(elem);
        });

        /**
         * Set mobile menu overlay as fixed
         * and fix iOS device overflow:hidden bug
         * @type {number}
         */
        var fixInstance = new scrollFix('header-main');
        $('.mobile-menu').click(function() {
            /**
             * Close minicart
             */
            $(document).trigger('minicartClose');

            /**
             * Handle mobile menu
             */
            if (!$(this).hasClass('open')) {
                // Open
                $(this).addClass('open');
                $('.header-main').addClass('opened');

                // Freeze bg
                fixInstance.disableScroll();
                $('html, body').addClass('overlayed');
            } else {
                // Close
                $(this).removeClass('open');
                $('.header-main').removeClass('opened');

                // Unfreeze bg
                $('html, body').removeClass('overlayed');
                fixInstance.enableScroll();
            }
        });

        /**
         * Overlay can also be closed on resize and
         * by minicart overlay appearing
         */
        $(window).on('resize', function() {
            // Close
            $('.mobile-menu').removeClass('open');
            $('.header-main').removeClass('opened');

            // Unfreeze bg
            fixInstance.enableScroll();
        });

        $(document).on('mobileMenuClose', function() {
            // Close
            $('.mobile-menu').removeClass('open');
            $('.header-main').removeClass('opened');

            // Unfreeze bg
            fixInstance.enableScroll();
        });

        $('.hover-show').on('click', function (event) {
            if (width < 1024) {
                event.preventDefault();
                $(this).toggleClass('level1-open').parent().next('.submenu').toggleClass('open').slideToggle(200).end()
                    .parent().parent().siblings().children('.level-top').children('.top-item').removeClass('level1-open')
                    .parent().siblings('.submenu').removeClass('open').slideUp(200);
            }
        });

        $('.submenu-label').click(function(){
            if(width < 1024) {
                $(this).toggleClass('level2-open').next('.submenu').toggleClass('open').slideToggle(200).end()
                    .parent().toggleClass('open').siblings().removeClass('open').children('.submenu-label').removeClass('level2-open')
                    .siblings('.submenu').slideUp(200);
            }
        });

        /** Footer link hide for mobile view */
        $('.has-children').children('p').on('click', function (event) {
            event.preventDefault();
            $(this).toggleClass('submenu-open').next('.sub-menu').slideToggle(200).end()
                .parent('.has-children').siblings('.has-children').children('p')
                .removeClass('submenu-open').next('.sub-menu').slideUp(200);
        });

        $(window).on('resize', function(){
            if ((width <= 1024) && ($(window).width() >= 1024)) {
                $('.sub-menu').css('display', 'table');
                $('.has-children .title').removeClass('submenu-open');
                $('.level1').removeClass('open');
                $('.submenu-label').removeClass('level2-open');
                $('.level1.submenu').css('display', 'block');
                $('.level0.submenu').css('display', 'block');
            }
            else if ((width >= 1024) && ($(window).width() <= 1024)) {
                $('.sub-menu').css('display', 'none');
                $('.level1.submenu').css('display', 'none');
                $('.level0.submenu').css('display', 'none');
            }
            width = $(window).width();
        });

        $('.show-pass-button').mousedown(function() {
            showPass('text', $(this).siblings('.show-password'));
        }).bind('mouseup', function() {
            showPass('password', $(this).siblings('.show-password'));
        });

        function showPass(val, selector) {
            $(selector).prop('type', val);
        }

        $('.new-password').keypress(function() {
            $('.password-strength-meter').show();
        });

        /** @TODO Remove this when hacky CMS pages as Category pages are fixed */
        $('.category-cms div:has(button)').addClass('button-parent');
        $('.category-cms button').addClass('blue-button');

        $('.kuru-slider').slick({
            arrows: false,
            dots: true
        });

        $('.godaddy').click(function(){
            PopupCenter('https://seal.godaddy.com/verifySeal?sealID=MqKAt2u0BS8b3sWvQtBBadCrLFE7VKvT456xktD50hkYo3CbAjLs9oJZjI9R','xtf','600','470');
        });
    });
});
