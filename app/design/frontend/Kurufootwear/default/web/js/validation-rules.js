/**
 * @vendor      Kurufootwear
 * @package     Kurufootwear_default
 * @author      Aleksejs Baranovs <info@scandiweb.com>
 * @copyright   Copyright (c) 2018 Scandiweb, Inc (http://scandiweb.com)
 * @license     http://opensource.org/licenses/OSL-3.0 The Open Software License 3.0 (OSL-3.0)
 */

define([
    'jquery',
    'jquery/validate',
    'mage/translate'
], function ($) {
    'use strict';

    $.validator.addMethod(
        'validate-customer-password-custom', function (v, elm) {
            var validator = this,
                length = 0,
                counter = 0;
            var passwordMinLength = $(elm).data('password-min-length');
            var passwordMinCharacterSets = $(elm).data('password-min-character-sets');
            var pass = $.trim(v);
            var result = pass.length >= passwordMinLength;
            if (result === false) {
                /*eslint-disable max-len*/
                validator.passwordErrorMessage = $.mage.__('Minimum length of this field must be equal or greater than %1 symbols. Leading and trailing spaces will be ignored.').replace('%1', passwordMinLength);
                /*eslint-enable max-len*/

                return result;
            }
            if (pass.match(/\d+/)) {
                counter ++;
            }
            if (pass.match(/[a-z]+/)) {
                counter ++;
            }
            if (pass.match(/[A-Z]+/)) {
                counter ++;
            }
            if (pass.match(/[^a-zA-Z0-9]+/)) {
                counter ++;
            }
            if (counter < passwordMinCharacterSets) {
                result = false;
                /*eslint-disable max-len*/
                validator.passwordErrorMessage = $.mage.__('Please include at least %1 different character types to create a secure password: Lower Case, Upper Case, Number, Special Characters.').replace('%1', passwordMinCharacterSets);
                /*eslint-enable max-len*/
            }

            return result;
        }, function () {
            return this.passwordErrorMessage;
        }
    );

    $.validator.addMethod(
        'validate-customer-password-account', function (v, elm) {
            var validator = this,
                length = 0,
                counter = 0,
                pass = $.trim(v),
                passwordMinLength = pass.length === 0 ? 0 : $(elm).data('password-min-length'),
                passwordMinCharacterSets = pass.length === 0 ? 0 : $(elm).data('password-min-character-sets'),
                result = pass.length >= passwordMinLength;

            if (result === false) {
                validator.passwordErrorMessage = $.mage.__(
                    'Minimum length of this field must be equal or greater than %1 symbols. Leading and trailing spaces will be ignored.'
                ).replace('%1', passwordMinLength);

                return result;
            }

            if (pass.match(/\d+/)) {
                counter ++;
            }

            if (pass.match(/[a-z]+/)) {
                counter ++;
            }

            if (pass.match(/[A-Z]+/)) {
                counter ++;
            }

            if (pass.match(/[^a-zA-Z0-9]+/)) {
                counter ++;
            }

            if (counter < passwordMinCharacterSets) {
                result = false;

                validator.passwordErrorMessage = $.mage.__(
                    'Please include at least %1 different character types to create a secure password: Lower Case, Upper Case, Number, Special Characters.'
                ).replace('%1', passwordMinCharacterSets);
            }

            return result;
        }, function () {
            return this.passwordErrorMessage;
        }
    );
});