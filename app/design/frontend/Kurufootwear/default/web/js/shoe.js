/*
 Shoe script -
 To Initiate simply call:
 var shoe = new Shoe('canvas');
 */
// Used in support-shoes CMS page
function Shoe(canvas)
{
    var canvas = document.getElementById(canvas);
    var ctx = canvas.getContext('2d');
    var images = [];
    var loaded = 0;
    var iid = 0;
    var interval;
    var output = [];
    for(var i = 0; i < 23; i++)
    {
        images[i] = new Image();
        images[i].onload = function()
        {
            loaded++;
            inc();
        }
        images[i].src = data[i];
    }
    function start()
    {
        interval = setInterval(function(){ draw() }, 70);
    }
    function inc()
    {
        if(loaded == 23)
        {
            start();
        }
    }
    function draw()
    {
        ctx.drawImage(images[iid], 0, 0, canvas.width, canvas.height);
        if(iid == 22)
        {
            iid = 0;
        }
        else
        {
            iid++;
        }
    }
}