/**
 * @category  Kurufootwear
 * @package   Kurufootwear\Catalog
 * @author    Krisjanis Veinbahs <krisjanisv@scandiweb.com>
 * @copyright Copyright (c) 2017 Scandiweb, Inc (http://scandiweb.com)
 * @license   http://opensource.org/licenses/OSL-3.0 The Open Software License 3.0 (OSL-3.0)
 */

define([
    'jquery',
    'jquery/ui',
    'arrive',
    'scrollToFixed'
], function($) {
    function stickCheckout(element) {
        var $element = $(element);
        var fixed = false;
        var iDevice = navigator.userAgent.match(/iPhone/i);
        var nonFixedExtraLimit = iDevice ? 0 : 56;

        if ($element.length > 0) {
            $element.scrollToFixed( {
                bottom: 10,
                dontSetWidth: true,
                spacerClass: 'placeholder',
                zIndex: 3,
                limit: function() {
                    var $sibling = $element.prev();
                    var siblingPosition = $sibling.offset().top;
                    var siblingHeight = $sibling.outerHeight();
                    var siblingBottom = siblingPosition + siblingHeight;
                    if (!fixed && !iDevice) {
                        siblingBottom = siblingBottom + nonFixedExtraLimit;
                    }
                    return siblingBottom;
                },
                preFixed: function() {
                    fixed = true;
                    $('.kuru-chat-container').addClass('over-checkout');
                },
                preUnfixed: function() {
                    fixed = false;
                    $('.kuru-chat-container').removeClass('over-checkout');
                },
                maxWidth: 767
            });

        }
    }

    function stickTotal(element) {
        var $element = $(element);

        if ($element.length > 0) {
            $element.scrollToFixed( {
                bottom: 74,
                dontSetWidth: true,
                spacerClass: 'placeholder',
                zIndex: 4,
                limit: function() {
                    var $sibling = $element.prev();
                    var siblingPosition = $sibling.offset().top;
                    var siblingHeight = $sibling.outerHeight();
                    var siblingBottom = siblingPosition + siblingHeight;
                    return siblingBottom;
                },
                preFixed: function() {
                    $('.kuru-chat-container').addClass('over-total');
                },
                preUnfixed: function() {
                    $('.kuru-chat-container').removeClass('over-total');
                },
                maxWidth: 767
            });

        }
    }

    $(document).ready(function() {
        var parent = $('.checkout-methods-items .action.checkout').closest('li');
        stickCheckout(parent);
    });

    $(document).arrive('.checkout-cart-index .grand.totals', function(element) {
        stickTotal(element);
    });
});
