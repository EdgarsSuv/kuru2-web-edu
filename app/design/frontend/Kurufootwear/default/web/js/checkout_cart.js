/**
 * @vendor      Scandiweb
 * @theme       Kurufootwear_default
 * @copyright   Copyright (c) 2017 Magento. All rights reserved.
 * @license     http://opensource.org/licenses/afl-3.0.php Academic Free License (AFL 3.0)
 */
/**
 * Checkout Cart functionality
 */
define([
    'jquery',
    'arrive'
], function ($) {
    'use strict';

    /**
     * Quantity selectors functionality to change quantity with plus and minus buttons
     */
    $(document).arrive('.control .plus, .control .minus', { existing: true }, function(element) {
        $(element).on('click', function() {
            var $inputSelector = $(this).siblings('.input-text');
            var options = {
                currentVal: parseInt($inputSelector.val()),
                lessClass: 'minus',
                moreClass: 'plus',
                min: 1,
                max: 10000,
                step: 1
            };
            var result;

            if ($(this).hasClass(options.lessClass)) {
                result = options.currentVal - options.step;
            }
            if ($(this).hasClass(options.moreClass)) {
                result = options.currentVal + options.step;
            }

            if (result >= options.min && result <= options.max) {
                $inputSelector.val(result);
                showUpdateCartBtn($(this));
            }
        });
    });

    /**
     * When quantity selector value is updated manually, shows the "update cart" button below
     */
    $('.qty .input-text').change(function() {
        showUpdateCartBtn($(this));
    });

    /**
     * Makes the "update cart" button below quantity selector visible
     */
    function showUpdateCartBtn($inputField) {
        if ($inputField) {
            $($inputField).parents('.field.qty').children('.action.update').css('visibility', 'visible');
        }
    }
});