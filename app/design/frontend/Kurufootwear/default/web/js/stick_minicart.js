/**
 * @category  Kurufootwear
 * @package   Kurufootwear\Catalog
 * @author    Krisjanis Veinbahs <krisjanisv@scandiweb.com>
 * @copyright Copyright (c) 2017 Scandiweb, Inc (http://scandiweb.com)
 * @license   http://opensource.org/licenses/OSL-3.0 The Open Software License 3.0 (OSL-3.0)
 */

define([
    "jquery",
    "jquery/ui",
    "js/vendor/jquery_sticky"
], function ($) {
    var MinicartItems = function () {
        this.stuckOnce = false;
        this.stuck = false;
        this.isMobile = function () {
            return ($(window).innerWidth() < 1024);
        };
        this.isCMSvisible = function () {
            var $itemsList = $('.header .block-minicart');
            var listHeight = $itemsList[0].clientHeight;
            var $cmsBlock = $('.header .block-minicart .minicart-cms-block');
            var cmsBlockTop = $cmsBlock.position().top + 20;
            return listHeight >= cmsBlockTop;
        };
        this.isOverflowing = function () {
            // Is scrollable if content without cms block
            // doesn't fit in the item list
            var $itemsList = $('.header .block-minicart');
            var cmsHeight = $('.header .block-minicart .minicart-cms-block')
                .outerHeight();
            return $itemsList[0].scrollHeight - cmsHeight > $itemsList[0].clientHeight;
        };
        this.isEmpty = function () {
            return $('.header .block-minicart .subtotal').length === 0;
        };
        this.mobileStick = function () {
            this.stick();
            var $checkoutBlock = $('.header .minicart-content .sticky-checkout-wrapper .actions');
            var $subtotalBlock = $('.header .minicart-content .sticky-subtotal-wrapper .subtotal');
            $subtotalBlock.css({
                'width': '100%'
            });
            $checkoutBlock.css({
                'width': 'calc(100% - 60px)',
                'margin-left': '30px'
            });
        };
        this.stick = function () {
            // Stick checkout button
            if (this.stuckOnce) {
                var $checkoutBlock = $('.header .minicart-content .unsticky-checkout-wrapper .actions');
                $checkoutBlock.parent().addClass('sticky-checkout-wrapper');
                $checkoutBlock.parent().removeClass('unsticky-checkout-wrapper');
            } else { // Create sticky wrapper
                var $checkoutBlock = $('.header .minicart-content > .actions');
                $checkoutBlock.sticky({
                    bottomSpacing: 0,
                    widthFromWrapper: false
                });

                $checkoutBlock.parent().addClass('sticky-checkout-wrapper');
            }
            $checkoutBlock.css({
                'width': $checkoutBlock.parent().outerWidth() - 60,
                'bottom': '20px'
            });
            var checkoutHeight = $checkoutBlock.outerHeight() + 20;

            // Stick subtotal
            if (this.stuckOnce) {
                var $subtotalBlock = $('.header .minicart-content .unsticky-subtotal-wrapper .subtotal');
                $subtotalBlock.parent().addClass('sticky-subtotal-wrapper');
                $subtotalBlock.parent().removeClass('unsticky-subtotal-wrapper');
            } else { // Create sticky wrapper
                var $subtotalBlock = $('.header .minicart-content > .subtotal');
                $subtotalBlock.sticky({
                    bottomSpacing: 0,
                    widthFromWrapper: false
                });
                $subtotalBlock.parent().addClass('sticky-subtotal-wrapper');
            }
            $subtotalBlock.css({
                'width': $subtotalBlock.parent().outerWidth(),
                'bottom': 0,
                'padding-bottom': checkoutHeight + 20
            });
            if (!this.stuckOnce) {
                this.stuckOnce = true;
            }
        };
        this.unstick = function () {
            // Unstick checkout button
            var $checkoutBlock = $('.header .minicart-content .sticky-checkout-wrapper > .actions');
            $checkoutBlock.css({
                'width': 'auto',
                'bottom': '0',
                'margin-left': '0'
            });
            $checkoutBlock.parent().addClass('unsticky-checkout-wrapper');
            $checkoutBlock.parent().removeClass('sticky-checkout-wrapper');
            var checkoutHeight = $checkoutBlock.outerHeight() + parseInt($checkoutBlock.css('bottom'));
            // Unstick subtotal
            var $subtotalBlock = $('.header .minicart-content .sticky-subtotal-wrapper > .subtotal');
            $subtotalBlock.css({
                'width': 'auto',
                'padding-bottom': '20px'
            });
            $subtotalBlock.parent().addClass('unsticky-subtotal-wrapper');
            $subtotalBlock.parent().removeClass('sticky-subtotal-wrapper');
        };
        this.handleBottomStick = function () {
            if (!this.isEmpty() && this.throttleHandlers()) {
                if (this.isMobile()) {
                    this.unstick();
                    this.mobileStick();
                    this.stuck = true;
                    if (this.stuckOnce) {
                        var $subtotalBlock = $('.header .minicart-content .sticky-wrapper .subtotal');
                        var $checkoutBlock = $('.header .minicart-content .sticky-wrapper .actions');
                        $subtotalBlock.parent().css('height', 64);
                        $checkoutBlock.parent().css('height', $checkoutBlock.outerHeight());
                    }
                } else {
                    var $subtotalBlock = $('.header .minicart-content .sticky-wrapper .subtotal');
                    var $checkoutBlock = $('.header .minicart-content .sticky-wrapper .actions');

                    if ((!this.isOverflowing() || this.isCMSvisible()) && this.stuck) {
                        this.unstick();
                        this.stuck = false;
                    } else if (!this.isCMSvisible() && this.isOverflowing() && !this.stuck) {
                        this.stick();
                        this.stuck = true;
                    }
                    if (this.stuckOnce) {
                        $subtotalBlock.parent().css('height', 67);
                        $checkoutBlock.parent().css('height', $checkoutBlock.outerHeight());
                    }

                    if (this.stuck) {
                        $subtotalBlock.css('width', $subtotalBlock.parent().outerWidth());
                        $checkoutBlock.css('width', $checkoutBlock.parent().outerWidth() - 60)
                    }
                }

                setTimeout(function () {
                    var $checkoutBlock = $('.header .minicart-content .sticky-checkout-wrapper .actions');
                    var $subtotalBlock = $('.header .minicart-content .sticky-subtotal-wrapper .subtotal');
                    $checkoutBlock.hide().show(0);
                    $subtotalBlock.hide().show(0);
                }, 300);
            }
        };
        this.handleTitleStick = function(event) {
            var top = $(this).scrollTop();
            $('.header .minicart-title').css('top', top);
        };
        /**
         * Return true only every 50 ms or longer
         * used to run only for one of all triggered minicart open events
         * @type {number}
         */
        this.lastThrottle = 0;
        this.throttleHandlers = function () {
            var newThrottle = performance.now()
            if (newThrottle - this.lastThrottle > 50) {
                this.lastThrottle = newThrottle;
                return true
            } else {
                return false
            }
        }
    };

    function listenAndStick(items) {
        $('.header .block-minicart').on('scroll', items.handleTitleStick);
        $('.header .showcart').on('click', items.handleBottomStick.bind(items));
        $('.header .block-minicart').on('scroll', items.handleBottomStick.bind(items));
        $('.header.content .minicart-wrapper').on('opening', items.handleBottomStick.bind(items));
        $(window).on('resize', items.handleBottomStick.bind(items));
    }

    // Stick minicart static content to bottom,
    // when cart items fill screen height
    $(document).ready(function () {
        var items = new MinicartItems();

        // If cart is initialized before this script
        listenAndStick(items);

        // If cart is initialized after this script
        $('.header #minicart-content-wrapper')
            .on('DOMNodeInserted', '.minicart-cms-block', function() {
                listenAndStick(items)
            });
    });
});
