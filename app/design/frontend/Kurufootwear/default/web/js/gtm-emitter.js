/**
 * @vendor      Kurufootwear
 * @theme       Kurufootwear\default
 * @author      Krisjanis Veinbahs <krisjanisv@scandiweb.com>
 * @copyright   Copyright (c) 2018 KURU Footwear. All rights reserved.
 */
define([
    'jquery',
    'Magento_Theme/js/view/messages',
    'Kurufootwear_ApplePay/js/applepay/button'
], function ($, Messages, applepayButton) {
    'use strict';

    /**
     * jQuery widget to quickly track and emit info to GTM
     */
    return $.widget('kuru.gtmEmitter', {
        /**
         * Internal widget storage
         */
        $emitter: null,
        messages: null,

        /**
         * Bind element
         * @private
         */
        _create: function() {
            this.$emitter = $(this.element);
            this.messages = new Messages();

            this.bindEmissions();
        },

        /**
         * Bind triggers and actions on element for emission
         */
        bindEmissions: function() {
            Object.keys(this.options).forEach(function(key) {
                var emitMethod = 'emit_' + key;
                var emitData = this.options[key];

                if (typeof this[emitMethod] === 'function') {
                    this[emitMethod](emitData);
                }
            }.bind(this))
        },

        /**
         * Generic click emission handler
         */
        emit_onclick: function(clickData) {
            this.$emitter.on('click', function() {
                Object.keys(clickData).forEach(function(key) {
                    this.messages.pushMessage(clickData[key], key);
                }.bind(this))
            }.bind(this));
        },

        /**
         * isApplePay available emission handler
         */
        emit_applepay: function() {
            var isActive = window.checkoutConfig.isApplepaybuttonActive;
            var isAvailable = applepayButton.deviceSupported();

            var isVisible = isAvailable && isActive;
            var isVisibleBoolText = isVisible ? 'T' : 'F';

            this.messages.pushMessage(isVisibleBoolText, 'ApplePay');
        }
    });
});