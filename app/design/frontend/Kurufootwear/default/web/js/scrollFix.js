/**
 * @vendor Scandiweb
 * @theme Kurufootwear_default
 * @copyright Copyright (c) 2016 Magento. All rights reserved.
 * @license http://opensource.org/licenses/afl-3.0.php Academic Free License (AFL 3.0)
 */
define(['jquery'], function($) {
    'use strict';

    return function (excludedElementClass) {
        var freezeAll = function (e) {
            if (!document.getElementsByClassName(excludedElementClass)[0].contains(e.target)) {
                e.preventDefault();
            }
        };

        var excludeFromFreeze = function (e) {
            var top = this.scrollTop,
                totalScroll = this.scrollHeight,
                currentScroll = top + this.offsetHeight;

            if (top === 0 && currentScroll === totalScroll) {
                e.preventDefault();
            } else if (top === 0) {
                this.scrollTop = 1;
            } else if (currentScroll === totalScroll) {
                this.scrollTop = top - 1;
            }
        };

        var excludedElement = undefined;

        this.disableScroll = function () {
            // Capture and stop all scrolling events
            document.addEventListener('touchmove', freezeAll, false);

            // Exclude an element from scroll ban
            excludedElement = document.getElementsByClassName(excludedElementClass)[0];
            excludedElement.addEventListener("touchmove", excludeFromFreeze, false);
        };

        this.enableScroll = function () {
            if (excludedElement) {
                document.removeEventListener("touchmove", freezeAll);
                excludedElement.removeEventListener("touchmove", excludeFromFreeze);
            }
        };
    };
});