﻿Kurufootwear_default theme
===================

Tools

Bourbon mixin library and Neat grid framework are installed.
To install them globally on your local environment check github pages:
https://github.com/thoughtbot/bourbon
https://github.com/thoughtbot/neat

All css, sprite files are generated automatically from assets folder using gulp build system.

First of all you need to install node js on your computer (there are plenty of tutorials online ;) ).
You will also need sass command line compiler. Can be installed from here - http://sass-lang.com/install

COMPILATION

From root directory run:
`$ npm install` - to install node modules required for gulp tasks
`gulp` - to compile sprites, stylesheets, start browser-sync and watch tasks
______________________________________________________________________________________________________________

CSS

Use Mobile-first approach for styling!
There are 3 main folders for styling:
main - for all global element and mobile design styling
tablet - for only tablet styles
desktop - for only desktop styles

+ components folder with frameworks/libraries installed.

______________________________________________________________________________________________________________

Sprites

We are using image sprites to reduce number of requests of images.
Sprites are generated automatically by gulp.

All you have to do is create 2 images from photoshop designs (from vector object) 
first image is in actual size, second one 2 times bigger than original.

New sprite will be generated and added to /spritesheets/ folder.
Additionally SASS variables will be generated with all information of all images.

For example, we add “icon.png” image to /assets/sprites/ folder, sprite is generated.
Then we open editor and add icon to the element:

.icon {
    display: inline-block;
    @include retina-sprite($sprite-trash-icon-group);
}

That’s it. No need to position sprites manually. It will also add dimensions to the element.
Compiled result would looks something like this:

.icon {
    display: inline-block;
    background-image: url("../images/spritesheets/sprite.png");
    background-position: -100px -35px;
    width: 22px;
    height: 22px;
}

______________________________________________________________________________________________________________

Images

Images that can not be added to sprites can be put inside app/design/frontend/Kurufootwear/default/web/images/ folder.
For example textures that are repeating, ajax loaders etc.
