/**
 * @vendor Kurufootwear
 * @module Mirasvit_Rewards
 * @author Krisjanis Veinbahs <krisjanisv@scandiweb.com>
 * @copyright Copyright (c) 2017 Scandiweb. All rights reserved.
 * @license http://opensource.org/licenses/afl-3.0.php Academic Free License (AFL 3.0)
 */

define(
    [
        'jquery',
        'arrive',
        'ko',
        'uiComponent',
        'mage/storage',
        'Mirasvit_Rewards/js/model/messages',
        'Magento_Checkout/js/action/get-payment-information',
        'Mirasvit_Rewards/js/view/checkout/rewards/points_spend',
        'Mirasvit_Rewards/js/view/checkout/rewards/points_totals',
        'Magento_Checkout/js/model/quote',
        'Magento_Checkout/js/model/url-builder',
        'Magento_Checkout/js/model/totals'
    ],
    function(
        $,
        arrive,
        ko,
        Component,
        storage,
        messageContainer,
        getPaymentInformationAction,
        rewardsSpend,
        rewardsEarn,
        quote,
        urlBuilder,
        totals
    ) {
        'use strict';
        var form = '#reward-points-form';

        var isShowRewards            = ko.observable(window.checkoutConfig.chechoutRewardsIsShow);
        var isRemovePoints           = ko.observable(window.checkoutConfig.chechoutRewardsPointsUsed);
        var rewardsPointsUsed        = ko.observable(window.checkoutConfig.chechoutRewardsPointsUsed);
        var rewardsPointsUsedOrigin  = ko.observable(window.checkoutConfig.chechoutRewardsPointsUsed);
        var chechoutRewardsPointsMax = ko.observable(window.checkoutConfig.chechoutRewardsPointsMax);
        var useMaxPoints             = ko.observable(
            window.checkoutConfig.chechoutRewardsPointsUsed === window.checkoutConfig.chechoutRewardsPointsMax
        );
        var addRequireClass          = false;
        var totalRetries             = ko.observable(0);

        var exchangePointsAvailable     = window.checkoutConfig.exchange_points_available;
        var setAllExchangePoints        = window.checkoutConfig.setAllExchangePoints;
        var rewardsPointsName           = window.checkoutConfig.chechoutRewardsPointsName;
        var rewardsPointsAvailble       = window.checkoutConfig.chechoutRewardsPointsAvailble;
        var ApplayPointsUrl             = window.checkoutConfig.chechoutRewardsApplayPointsUrl;
        var PaymentMethodPointsUrl      = window.checkoutConfig.chechoutRewardsPaymentMethodPointsUrl;

        var isMageplazaOsc = window.checkoutConfig.isMageplazaOsc;
        var initialSet = false;

        /**
         * Force update current rewards point values in widget
         * and trigger listeners dependant on widget values
         * @param self
         */
        function updateWidget(self) {
            var $slider = $('.block-rewards-form .input-slider');
            var $points = $('.block-rewards-form .used-amount .points');

            /**
             * Update slider value and points textbox
             */
            if ($points.text() === '' || !rewardsPointsUsed()) {
                var currentValue = 0;
                $slider.val(currentValue);
                $points.text(currentValue);
            } else {
                var currentValue = rewardsPointsUsed();
                $slider.val(currentValue);
                $points.text(currentValue);
            }

            /**
             * Trigger slider green background refresh
             */
            $slider.trigger('input');
        }

        return Component.extend({
            defaults: {
                template: 'Mirasvit_Rewards/checkout/rewards/usepoints'
            },

            isLoading: ko.observable(false),
            isShowRewards: isShowRewards,
            isRemovePoints: isRemovePoints,
            initialPointsUsed: rewardsPointsUsed(),
            rewardsPointsUsed: rewardsPointsUsed,
            rewardsPointsUsedOrigin: rewardsPointsUsedOrigin,
            useMaxPoints: useMaxPoints,
            addRequireClass: addRequireClass,
            totalRetries: totalRetries,

            setAllExchangePoints: typeof(setAllExchangePoints) !== 'undefined',
            chechoutRewardsPointsMax: chechoutRewardsPointsMax,
            rewardsPointsAvailble: parseInt(rewardsPointsAvailble),
            exchangePointsAvailable: exchangePointsAvailable,
            rewardsPointsName: rewardsPointsName,

            ApplayPointsUrl: ApplayPointsUrl,
            PaymentMethodPointsUrl: PaymentMethodPointsUrl,

            isMageplazaOsc: isMageplazaOsc,
            initialSet: initialSet,

            rewardsFormSubmit: function (isRemove) {
                this.validatePointsAmount();
                this.submit();
            },
            setMaxPoints: function (forcedMax) {
                if (typeof(forcedMax) === 'undefined') {
                    forcedMax = this.useMaxPoints();
                }

                if (this.useMaxPoints() && !forcedMax) {
                    this.useMaxPoints(false);
                    if (this.rewardsPointsUsedOrigin()) {
                        this.rewardsPointsUsed(this.rewardsPointsUsedOrigin());
                    } else {
                        this.rewardsPointsUsed(0);
                    }
                } else {
                    this.rewardsPointsUsedOrigin(this.rewardsPointsUsed());
                    this.useMaxPoints(true);
                    this.rewardsPointsUsed(this.chechoutRewardsPointsMax());
                }

                this.rewardsFormSubmit(0);
                $(form + ' .input-slider').trigger('input');
                return true;
            },
            validatePointsAmount: function () {
                if (parseInt(this.rewardsPointsUsed()) < this.chechoutRewardsPointsMax()) {
                    this.useMaxPoints(false);
                } else {
                    this.useMaxPoints(true);
                    this.rewardsPointsUsed(this.chechoutRewardsPointsMax());
                }
            },
            validate: function() {
                return $(form).validation() && $(form).validation('isValid');
            },
            submit: function () {
                var data = $(form).serialize();
                if (data == '') {
                    if (this.rewardsPointsUsed >= this.chechoutRewardsPointsMax()) {
                        this.rewardsPointsUsed(this.chechoutRewardsPointsMax())
                    }
                    data = 'points_amount=' + this.rewardsPointsUsed();
                }
                this.isLoading(true);
                var self = this;
                $.ajax({
                    url: this.ApplayPointsUrl,
                    type: 'POST',
                    dataType: 'JSON',
                    data: data,
                    complete: function (data) {
                        var deferred = $.Deferred();
                        getPaymentInformationAction(deferred);
                        $.when(deferred).done(function () {
                            $('#ajax-loader3').hide();
                            $('#control_overlay_review').hide();
                            // rewardsSpend().getValue(data.responseJSON.spend_points_formated);
                            if (data.responseJSON.message) {
                                messageContainer.addSuccessMessage({'message': data.responseJSON.message});
                            }

                            if (data.responseJSON) {
                                if (self.isRemovePoints()) {
                                    self.useMaxPoints(false);
                                    rewardsSpend().isDisplayed(0);
                                } else {
                                    rewardsSpend().isDisplayed(1);
                                }
                            }
                            self.isLoading(true);
                        });
                        updateWidget(self);
                        var $slider = $('.block-rewards-form .input-slider');
                        $slider.prop('disabled', false);
                    }
                });
            },
            initialize: function(element, valueAccessor, allBindings) {
                this._super();
                var self = this;
                var serviceUrl = urlBuilder.createUrl('/rewards/mine/update', {});
                var isRegistered = window.checkoutConfig.customerData &&
                    window.checkoutConfig.customerData.id;

                if (!isRegistered) {
                    // Guest customers should stay at the initial earned
                    // point value, no JS necessary, there's no slider for them
                    return false;
                }

                if (!self.rewardsPointsUsed()) {
                    rewardsPointsUsed(0);
                    updateWidget(self);
                }

                if (quote) {
                    quote.totals.subscribe(function (totals) {
                        updateWidget(self);
                        var request = $.Deferred();
                        var data = {};
                        if (quote.shippingMethod()) {
                            data = {
                                shipping_method: quote.shippingMethod()['method_code'],
                                shipping_carrier: quote.shippingMethod()['carrier_code']
                            };
                        }
                        // Mageplaza onespetcheckout override loader, so we should not call our
                        if (!self.isMageplazaOsc) {
                            self.isLoading(true);
                        }
                        storage.post(
                            serviceUrl, JSON.stringify(data), false
                        ).done(
                            function (response) {
                                self.rewardsPointsAvailble = response.chechout_rewards_points_availble;
                                self.chechoutRewardsPointsMax(response.chechout_rewards_points_max);

                                /**
                                 * Don't spend more than max points
                                 */
                                if (self.chechoutRewardsPointsMax() < self.rewardsPointsUsed()) {
                                    self.rewardsPointsUsed(self.chechoutRewardsPointsMax())
                                }

                                rewardsSpend().getValue(response.chechout_rewards_points_spend);
                                rewardsEarn().getValue(response.chechout_rewards_points);

                                request.resolve(response);
                                self.isLoading(false);
                                updateWidget(self);
                                var $slider = $('.block-rewards-form .input-slider');
                                $slider.prop('disabled', false);
                            }
                        ).fail(
                            function (response) {
                                request.reject(response);
                            }
                        ).always(
                            function () {
                            }
                        );

                        if ((totals.grand_total < 0 || totals.base_grand_total < 0) && self.totalRetries() < 3) {
                            self.validatePointsAmount();
                            self.isLoading(1);
                            self.submit();
                            self.totalRetries(self.totalRetries + 1)
                        } else if (self.totalRetries() >= 3) {
                            self.isLoading(0)
                        } else {
                            self.isLoading(0)
                            self.totalRetries(0)
                        }

                        return request;
                    });
                }
            }
        });
    }
);