/**
 * @vendor      Kurufootwear
 * @module      Mirasvit_Rewards
 * @copyright   Copyright (c) 2017 Scandiweb. All rights reserved.
 * @license     http://opensource.org/licenses/afl-3.0.php Academic Free License (AFL 3.0)
 */
define(
    [
        'ko',
        'Magento_Checkout/js/view/summary/abstract-total',
        'Magento_Checkout/js/model/quote'
    ],
    function (ko, Component, quote) {
        return Component.extend({
            totals: quote.getTotals(),
            isDisplayed: ko.observable(!!window.checkoutConfig.chechoutRewardsPointsSpend),
            getValue: ko.observable(parseInt(window.checkoutConfig.chechoutRewardsPointsSpend))
        });
    }
);