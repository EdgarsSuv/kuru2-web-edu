/**
 * @vendor Kurufootwear
 * @module Mirasvit_Rewards
 * @author Kristaps Stalidzans <info@scandiweb.com>
 * @copyright Copyright (c) 2017 Scandiweb. All rights reserved.
 * @license http://opensource.org/licenses/afl-3.0.php Academic Free License (AFL 3.0)
 */
require([
    'jquery',
    'priceUtils',
    'priceBox'
], function ($) {
    $('.price-box').on('updatePrice', function(e, data) {
        if (!data || !data.prices || !data.prices.rewardRules) {
            return;
        }
        var oldPoints = $('.rewards__product-points .price', this).data('points');
        var newPoints = oldPoints + data.prices.rewardRules.amount;
        var text      = $('.rewards__product-points .price', this).data('label');

        $('.rewards__product-points .price', this).data('new-points', newPoints);
        if (typeof(text) !== 'undefined') {
            $('.rewards__product-points .price', this).html(text.replace(oldPoints, newPoints));
        }    
    });

    //bundle
    $('#product_addtocart_form').on('updateProductSummary', function(e, data) {
        var totalPoints = 0;
        $.each(data.config.selected, function(index, values) {
            $.each(values, function(i, value) {
                var rules      = data.config.options[index]['selections'][value]['rewardRules'];
                var finalPrice = data.config.options[index]['selections'][value].prices.finalPrice.amount;
                var productId  = data.config.options[index]['selections'][value]['optionId'];
                var qty        = data.config.options[index]['selections'][value]['qty'];
                
                $.each(rules[productId], function(n, rule) {
                    if (rule.points) {
                        totalPoints += rule.points * qty;
                    } else {
                        var rulePoints = finalPrice * qty / rule.coefficient;
                        if (rule.options.limit && rulePoints > rule.options.limit) {
                            rulePoints = rule.options.limit;
                        }'' +
                        ''
                        totalPoints += rulePoints;
                    }
                });
            })
        });
        if (totalPoints) {
            if (parseInt(data.config.rounding) === 1) {
                totalPoints = Math.floor(totalPoints);
            } else {
                totalPoints = Math.ceil(totalPoints);
            }
            
            $('.price-box.price-configured_price .rewards__product-points .price').html(
                totalPoints + ' ' + data.config.rewardLabel
            );
        }
    });

    //
    $('.input-text.qty').keyup(function() {
        if ($('.page-product-bundle').length) {
            return;
        }
        
        var qty       = $(this).val();
        var el        = $('.rewards__product-points .price', $(this).parents('tr')[0])
        var oldPoints = $(el).data('points');
        var newPoints = oldPoints * qty;
        var text      = $(el).data('label');

        $(el).data('new-points', newPoints).html(text.replace(oldPoints, newPoints));
    });
});
