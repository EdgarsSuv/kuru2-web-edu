/**
 * @vendor      Kurufootwear
 * @module      Mirasvit_Rewards
 * @copyright   Copyright (c) 2017 Scandiweb. All rights reserved.
 * @license     http://opensource.org/licenses/afl-3.0.php Academic Free License (AFL 3.0)
 */
define([
    'ko',
    'Magento_Checkout/js/view/summary/abstract-total',
    'Magento_Checkout/js/model/quote'
], function (ko, Component, quote) {
        if (window.checkoutConfig.is_checkout_page) {
            // Checkout page
            // Points are static and calculated at page init
            var pointsValue = function() { return parseInt(window.checkoutConfig.chechoutRewardsPoints) };
            var pointsVisible = function() { return !!window.checkoutConfig.chechoutRewardsPoints };
        } else {
            // Shopping bag
            // Points are dynamic - can be changed by registered users w/ slider
            var pointsValue = ko.observable(parseInt(window.checkoutConfig.chechoutRewardsPoints));
            var pointsVisible = ko.observable(!!window.checkoutConfig.chechoutRewardsPoints);
        }
        
        return Component.extend({
            totals: quote.getTotals(),
            isDisplayed: pointsVisible,
            getValue: pointsValue
        });
    }
);