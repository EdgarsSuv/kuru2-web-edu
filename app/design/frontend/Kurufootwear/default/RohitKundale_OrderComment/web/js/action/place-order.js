/**
 * @vendor Kurufootwear
 * @module Kurufootwear_OrderComments
 * @author Krišjānis Veinbahs <krisjanisv@scandiweb.com>
 * @copyright Copyright (c) 2017 Scandiweb, Ltd (http://scandiweb.com)
 * @license http://opensource.org/licenses/afl-3.0.php Academic Free License (AFL 3.0)
 */
define(
    [
        'Magento_Checkout/js/model/quote',
        'Magento_Checkout/js/model/url-builder',
        'Magento_Customer/js/model/customer',
        'Magento_Checkout/js/model/place-order'
    ],
    function (
        quote,
        urlBuilder,
        customer,
        placeOrderService
    ) {
        'use strict';

        return function (paymentData, messageContainer) {
            var serviceUrl, payload;
            var billingAddressData = quote.billingAddress();

            /**
             * This fixes the issue when auto complete in safari won't trigger set billing address as guest.
             * I added a check in quote data to get flag if shipping address is same as billing address
             * and changes payload to match addresses.
             * As i researched i found main issue was safari auto fill won't trigger an event to set billing
             * address witch possibly relates to knockout version. But after upgrade it made things even worse.
             * This was the best thing i could find as a solution for this problem
             * https://github.com/magento/magento2/issues/6826
             */
            if (!customer.isLoggedIn() && quote.isAddressSameAsShippingGuest) {
                billingAddressData = quote.shippingAddress();
            }

            payload = {
                cartId: quote.getQuoteId(),
                billingAddress:  billingAddressData,
                paymentMethod: paymentData,
                comments: jQuery('[name="comment-code"]').val()
            };

            if (customer.isLoggedIn()) {
                serviceUrl = urlBuilder.createUrl('/carts/mine/payment-information', {});
            } else {
                serviceUrl = urlBuilder.createUrl('/guest-carts/:quoteId/payment-information', {
                    quoteId: quote.getQuoteId()
                });
                payload.email = quote.guestEmail;
            }

            return placeOrderService(serviceUrl, payload, messageContainer);
        };
    }
);
