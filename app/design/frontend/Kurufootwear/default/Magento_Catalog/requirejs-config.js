/**
 * @vendor Kurufootwear
 * @module Kurufootwear_Catalog
 * @author Kristaps Stalidzans <info@scandiweb.com>
 * @copyright Copyright (c) 2017 Scandiweb. All rights reserved.
 * @license http://opensource.org/licenses/afl-3.0.php Academic Free License (AFL 3.0)
 */

var config = {
    map: {
        '*': {
            productSlider: 'Magento_Catalog/js/slider',
            fixedGallery: 'Magento_Catalog/js/fixedGallery',
            fixedPdpDetails: 'Magento_Catalog/js/fixedPdpDetails'
        }
    },
    deps: [
        'Magento_Catalog/js/slider'
    ]
};
