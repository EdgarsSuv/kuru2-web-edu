/**
 * @category     Kurufootwear
 * @package      Kurufootwear/Catalog
 * @copyright    Copyright (c) 2018 Scandiweb, Inc (http://scandiweb.com)
 * @license      http://opensource.org/licenses/OSL-3.0 The Open Software License 3.0 (OSL-3.0)
 */
require([
    'jquery',
    'underscore'
], function ($, _) {
    'use strict';

    $(function(){
        var $root = $('html, body');

        $('a.read-more[href^="#"]').click(function () {
            $root.animate({
                scrollTop: $( $.attr(this, 'href') ).offset().top - ($('.page-header').height() + 20)
            }, 500);

            return false;
        });
    });
});
