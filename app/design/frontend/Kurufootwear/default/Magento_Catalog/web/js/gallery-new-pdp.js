/**
 * @category     Kurufootwear
 * @package      Kurufootwear/Catalog
 * @author       Aleksejs Baranovs <info@scandiweb.com>
 * @copyright    Copyright (c) 2018 KURU Footwear. All rights reserved.
 */

define([
    'jquery',
    'slick',
    'Scandi_MagicZoom/js/gallery'
], function ($, slick, Gallery) {
    'use strict';

    // Override to remove thumb initialization logic
    return Gallery.extend({
        /**
         * Initialize
         *
         * @param config Configuration object
         * @param element Slider wrapper element
         */
        initialize: function (config, element) {
            this.galleryElement = element;

            if (config.data.length <= 1) {
                if (!this.isConfigurable()) {
                    // Simple product has no pictures, no slider init at all
                    return false;
                }
            }

            this.config = $.extend(true, this.defaults, config);
            this.prepareVideoData();
            this.sliderElement = $(element);
            this.initSlider();
            this.initApi();

            /* Bug: https://github.com/magento/magento2/issues/7399 */
            $(document).on('click', '.mz-video-modal', function(e) {
                e.stopImmediatePropagation();
                $('.modals-overlay').trigger('click');
            });
        },

        /**
         * Init slider
         */
        initSlider: function() {
            var self = this,
                imagePopOptions = this.config.imagePopup.options;

            this.sliderElement.removeClass('_block-content-loading');

            this.sliderElement.addClass(this.GALLERY_CLASS);
            this.sliderElement.slick(this.config.sliderSettings.options);

            if (this.config.imagePopup.imagePopupStatus) {
                self.imageModalPopup('click', imagePopOptions);
            }

            // Add slides
            this.addSlides();
        },

        /**
         * Image modal popup
         *
         * @returns {boolean}
         */
        imageModalPopup: function (event, config) {
            var desktopImageClass = '.desktop-gallery-placeholder .desktop-image';

            this._super(event, config);

            $(document).on(event, desktopImageClass, function() {
                var visibleLinks = $(desktopImageClass);
                $.fancybox.open(visibleLinks, config, visibleLinks.index(this));

                return false;
            });
        },

        /**
         *
         */
        addSlides: function () {
            var self = this;
            $.each(this.config.data, function (index, imageData) {
                if (typeof (imageData['mediaType']) === 'undefined') {
                    imageData['mediaType'] = imageData['type'];
                }
                var _className = (self.isVideo(imageData)) ? 'slick-video-slide' : '',
                    _hrefLink = (self.isVideo(imageData)) ? imageData['videoUrl'] : imageData['full'],
                    _fancyBox = (self.config.imagePopup.imagePopupStatus) ? 'href="' + _hrefLink + '"' : '',
                    _slide = '<div class="slick-slide-wrap '+ _className +'" data-data-index="' + index + '" ' + _fancyBox + '><img draggable="false" ondragstart="return false;" alt="' + imageData['product'] + '" data-media-type="' + imageData['mediaType'] + '" data-zoom-image="'+ imageData['full'] +'" src="' + imageData['img'] + '"/></div>';

                self.sliderElement.slick('slickAdd', _slide);
            });
        },

        /**
         * Re-initialize slider
         */
        reInitSlider: function () {
            var slick = this.sliderElement.slick('getSlick'),
                slides = slick.$slides;

            // Remove slides
            while (slides.length !== 0) {
                this.sliderElement.slick('slickRemove', --slides.length);
            }

            if (this.config.data.length <= 1) {
                if (!this.isConfigurable()) {
                    return false;
                }
            }

            this.addSlides();
        }
    })
});