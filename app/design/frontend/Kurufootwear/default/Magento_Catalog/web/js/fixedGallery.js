/**
 * @vendor      Kurufootwear
 * @module      Magento_Catalog
 * @author      Aleksejs Baranovs <info@scandiweb.com>
 * @copyright   Copyright (c) 2018 Scandiweb, Ltd (http://scandiweb.com)
 * @license     http://opensource.org/licenses/afl-3.0.php Academic Free License (AFL 3.0)
 */

define([
    'jquery'
], function ($) {
    'use strict';

    return {
        options: {
            productMediaSelector: '.product.media',
            productMainInfoSelector: '.product-info-main',
            topMargin: 120,
            desktopBreakpoint: 1024
        },

        /**
         * Initialize
         */
        initialize: function () {
            var self = this;

            self.triggerFixedGallery();
            self.triggerWindowResize();
        },

        /**
         * Trigger gallery
         */
        triggerFixedGallery: function () {
            var self = this,
                productMediaTopOffset,
                productMediaHeight,
                productMainInfoTopOffset,
                productMainInfoHeight,
                fixedFlag = true;

            $(document).scroll(function () {
                if ($(window).width() >= self.options.desktopBreakpoint) {
                    productMediaTopOffset = $(self.options.productMediaSelector).offset().top;
                    productMediaHeight = $(self.options.productMediaSelector).height();
                    productMainInfoTopOffset = $(self.options.productMainInfoSelector).offset().top;
                    productMainInfoHeight = $(self.options.productMainInfoSelector).outerHeight();

                    if (productMediaTopOffset + productMediaHeight >= productMainInfoTopOffset + productMainInfoHeight) {
                        if (fixedFlag) {
                            self.options.topMargin = $(self.options.productMediaSelector).offset().top - $(document).scrollTop();
                            fixedFlag = false;
                        }

                        $(self.options.productMediaSelector).css({
                            'position': 'absolute',
                            'bottom': 0
                        });
                    }

                    if ($(document).scrollTop() + productMediaHeight < productMainInfoTopOffset + productMainInfoHeight - self.options.topMargin) {
                        fixedFlag = true;
                        $(self.options.productMediaSelector).css({
                            'position': 'fixed',
                            'bottom': 'auto'
                        });
                    }
                } else {
                    $(self.options.productMediaSelector).css({
                        'position': 'static'
                    });
                }
            });
        },

        /**
         * On window resize set static position to gallery
         */
        triggerWindowResize: function () {
            var self = this;

            $(window).resize(function () {
                if ($(window).width() < self.options.desktopBreakpoint) {
                    $(self.options.productMediaSelector).css({
                        'position': 'static'
                    });
                }
            })
        }
    }
});
