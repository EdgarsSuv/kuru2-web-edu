/**
 * @category     Kurufootwear
 * @package      Kurufootwear/Checkout
 * @author       Krisjanis Veinbahs <info@scandiweb.com>
 * @copyright    Copyright (c) 2018 KURU Footwear
 */
define([
    'jquery'
], function ($) {
    'use strict';

    return {
        options: {
            stickyClass: 'sticky',
            productTopSelector: '.product-info-top',
            productFormSelector: '.product-info-bottom',
            productMainInfoSelector: '.product-info-main',
            headerHeight: 68,
            topMargin: 120,
            desktopBreakpoint: 1024
        },

        /**
         * Initialize
         */
        initialize: function () {
            var self = this;

            self.triggerFixedGallery();
            self.triggerWindowResize();
        },

        /**
         * Trigger gallery
         */
        triggerFixedGallery: function () {
            var self = this,
                productTopTopOffset,
                productTopHeight,
                productFormTopOffset,
                productFormHeight,
                productMainInfoTopOffset,
                productMainInfoHeight,
                fixedFlag = true;

            $(document).scroll(function () {
                if (window.innerWidth >= self.options.desktopBreakpoint) {
                    productTopTopOffset = $(self.options.productTopSelector).offset().top;
                    productTopHeight = $(self.options.productTopSelector).height();
                    productFormTopOffset = $(self.options.productFormSelector).offset().top;
                    productFormHeight = $(self.options.productFormSelector).height();
                    productMainInfoTopOffset = $(self.options.productMainInfoSelector).offset().top;
                    productMainInfoHeight = $(self.options.productMainInfoSelector).outerHeight();

                    if (productFormTopOffset + productFormHeight >= productMainInfoTopOffset + productMainInfoHeight) {
                        // Bottom (main info) reached, should fall back into layout
                        if (fixedFlag) {
                            self.options.topMargin = $(self.options.productFormSelector).offset().top - $(document).scrollTop();
                            fixedFlag = false;
                        }

                        $(self.options.productFormSelector).addClass(self.options.stickyClass)
                            .css({
                                'position': 'absolute',
                                'top': 'auto',
                                'bottom': 0
                            });
                    }

                    if (window.scrollY + self.options.headerHeight <= productTopTopOffset + productTopHeight) {
                        // Top (product title) reached, should fall back into layout
                        $(self.options.productFormSelector).removeClass(self.options.stickyClass)
                            .css({
                                'position': 'relative',
                                'top': 'auto',
                                'bottom': 'auto'
                            });
                    } else if ($(document).scrollTop() + productFormHeight < productMainInfoTopOffset + productMainInfoHeight - self.options.topMargin) {
                        // Above bottom (main info) and below top (header), should be sticky (fixed)
                        fixedFlag = true;
                        $(self.options.productFormSelector).addClass(self.options.stickyClass)
                            .css({
                                'position': 'fixed',
                                'top': self.options.headerHeight + 'px',
                                'bottom': 'auto'
                            });
                    }
                } else {
                    $(self.options.productFormSelector).css({
                        'position': 'static'
                    });
                }
            });
        },

        /**
         * On window resize set static position to gallery
         */
        triggerWindowResize: function () {
            var self = this;

            $(window).resize(function () {
                if (window.innerWidth < self.options.desktopBreakpoint) {
                    $(self.options.productFormSelector).css({
                        'position': 'static'
                    });
                }
            })
        }
    }
});
