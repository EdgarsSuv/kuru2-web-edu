/**
 * @vendor Kurufootwear
 * @module Kurufootwear_Checkout
 * @author Krisjanis Veinbahs <krisjanisv@scandiweb.com>
 * @copyright Copyright (c) 2017 Scandiweb. All rights reserved.
 * @license http://opensource.org/licenses/afl-3.0.php Academic Free License (AFL 3.0)
 */

var config = {
    config: {
        mixins: {
            'Magento_Braintree/js/paypal/button': {
                'Magento_Braintree/js/paypal/button-mixin': true
            },
            'Magento_Braintree/js/view/payment/method-renderer/cc-form': {
                'Magento_Braintree/js/view/payment/method-renderer/cc-form-mixin': true
            }
        }
    }
};
