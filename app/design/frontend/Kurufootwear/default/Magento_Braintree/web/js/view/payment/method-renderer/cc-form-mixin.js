/**
 * @vendor      Kurufootwear
 * @module      Kurufootwear_Braintree
 * @author      Aleksejs Baranovs <info@scandiweb.com>
 * @copyright   Copyright (c) 2018 Scandiweb, Ltd (http://scandiweb.com)
 * @license     http://opensource.org/licenses/afl-3.0.php Academic Free License (AFL 3.0)
 */
define([
    'jquery',
    'Magento_Braintree/js/view/payment/adapter',
    'mage/translate'
], function ($, braintree, $t) {
    'use strict';

    return function (ccFormComponent) {
        return ccFormComponent.extend({
            options: {
                placeOrderButtonSelector: '.action.primary.checkout'
            },

            defaults: {
                /**
                 * Braintree client configuration
                 *
                 * {Object}
                 */
                clientConfig: {
                    /**
                     * Triggers on any Braintree error
                     * Override to enable "Place Order" button on error
                     *
                     * @param {Object} response
                     */
                    onError: function (response) {
                        $(this.options.placeOrderButtonSelector).prop('disabled', false);
                        braintree.showError($t('Payment ' + this.getTitle() + ' can\'t be initialized'));
                        throw response.message;
                    }
                }
            }
        });
    };
});
