/**
 * Copyright © 2013-2017 Magento, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */
define(
    [
        'require',
        'rjsResolver',
        'uiRegistry',
        'uiComponent',
        'underscore',
        'jquery',
        'braintree',
        'domReady!'
    ],
    function (
        require,
        resolver,
        registry,
        Component,
        _,
        $,
        braintree
    ) {
        return function(buttonComponent) {
            return buttonComponent.extend({
                /**
                 * @returns {Object}
                 */
                initComponent: function () {
                    var currentIntegration = registry.get(this.integrationName),
                        $this = $('#' + this.id),
                        self = this,
                        data = {
                            amount: $this.data('amount'),
                            locale: $this.data('locale'),
                            currency: $this.data('currency')
                        },
                        paypalButton = this,
                        initCallback = function () {
                            $this.attr('disabled', 'disabled');
                            registry.remove(this.integrationName);
                            braintree.setup(this.clientToken, 'custom', this.getClientConfig(data));

                            /**
                             * Added updates on quote changes
                             */
                            $(document).ready(function() {
                                require(['Magento_Checkout/js/model/quote'], function(quote) {
                                    quote.totals.subscribe(function(totals) {
                                        data.amount = totals.base_grand_total;
                                        braintree.setup(paypalButton.clientToken, 'custom', paypalButton.getClientConfig(data));
                                    });
                                })
                            })

                            $this.off('click')
                                .on('click', function (event) {
                                    event.preventDefault();

                                    registry.get(self.integrationName, function (integration) {
                                        integration.paypal.initAuthFlow();
                                    });
                                });
                        }.bind(this);

                    currentIntegration ?
                        currentIntegration.teardown(initCallback) :
                        initCallback();

                    return this;
                }
            });
        };
    }
);
