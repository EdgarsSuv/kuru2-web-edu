/**
 * Copyright © 2013-2017 Magento, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */
define([
    'ko',
    'uiClass'
], function (ko, Class) {
    'use strict';

    return Class.extend({
        initialize: function () {
            this._super()
                .initObservable();

            var messageEngine = this;

            // Subscribe to error messages
            this.lastMessageTime = performance.now();
            this.errorMessages.subscribe(function(messages) {
                messages.forEach(function(message) {
                    messageEngine.pushMessage(message, 'red-message');
                })
            })

            return this;
        },

        initObservable: function () {
            this.errorMessages = ko.observableArray([]);
            this.successMessages = ko.observableArray([]);

            return this;
        },

        /**
         * Add  message to list.
         * @param {Object} messageObj
         * @param {Object} type
         * @returns {Boolean}
         */
        add: function (messageObj, type) {
            var expr = /([%])\w+/g,
                message;

            if (!messageObj.hasOwnProperty('parameters')) {
                this.clear();
                type.push(messageObj.message);

                return true;
            }
            message = messageObj.message.replace(expr, function (varName) {
                varName = varName.substr(1);

                if (messageObj.parameters.hasOwnProperty(varName)) {
                    return messageObj.parameters[varName];
                }

                return messageObj.parameters.shift();
            });
            this.clear();
            this.errorMessages.push(message);

            return true;
        },

        addSuccessMessage: function (message) {
            return this.add(message, this.successMessages);
        },

        addErrorMessage: function (message) {
            return this.add(message, this.errorMessages);
        },

        getErrorMessages: function () {
            return this.errorMessages;
        },

        getSuccessMessages: function () {
            return this.successMessages;
        },

        hasMessages: function () {
            return this.errorMessages().length > 0 || this.successMessages().length > 0;
        },

        clear: function () {
            this.errorMessages.removeAll();
            this.successMessages.removeAll();
        },

        pushMessage: function(messageText, messageEventName) {
            messageEventName = messageEventName || 'message';

            if (this.isDuplicateMessagePush(messageText)) {
                // Checkout has a lot of duplicate errors,
                // One push is enough, if the content is the same.
                return false;
            }

            var messageEvent = {
                event: messageEventName,
                message: messageText
            }

            window.dataLayer.push(messageEvent)
        },

        isDuplicateMessagePush: function(messageText) {
            window.dataLayer.forEach(function(event) {
                if (event.message === messageText) {
                    return true;
                }
            })

            return false;
        },

        waitForDatalayer: function(callback) {
            var flag = false;

            // Check if dataLayer is defined
            if (typeof(window.dataLayer) === 'undefined') {
                setTimeout(function() {
                    messageEngine.waitForDatalayer(callback);
                }, 500);

                return false;
            }

            // Check if 'general' event has happened
            window.dataLayer.forEach(function () {
                if (this.event === 'general') {
                    flag = true;
                }
            });

            if (!flag) {
                var messageEngine = this;
                setTimeout(function() {
                    messageEngine.waitForDatalayer(callback);
                }, 500);
                return false;
            }
            callback();
        }
    });
});
