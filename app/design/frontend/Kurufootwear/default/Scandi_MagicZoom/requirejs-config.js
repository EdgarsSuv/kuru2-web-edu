/**
 * @vendor      Kurufootwear
 * @package     Kurufootwear_MagicZoom
 * @author      Aleksejs Baranovs <info@scandiweb.com>
 * @copyright   Copyright (c) 2018 Scandiweb, Inc (http://scandiweb.com)
 * @license     http://opensource.org/licenses/OSL-3.0 The Open Software License 3.0 (OSL-3.0)
 */

var config = {
    map: {
        '*': {
            scandiGallery: 'Scandi_MagicZoom/js/gallery'
        }
    }
};
