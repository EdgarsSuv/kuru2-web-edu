/**
 * @vendor      Kurufootwear
 * @theme       Kurufootwear_Theme
 * @author      Krisjanis Veinbahs <krisjanisv@scandiweb.com>
 * @copyright   Copyright (c) 2018 KURU Footwear. All rights reserved.
 */
var config = {
    map: {
        '*': {
            cookieNotification: 'Magento_Theme/js/view/cookieNotification'
        }
    }
};
