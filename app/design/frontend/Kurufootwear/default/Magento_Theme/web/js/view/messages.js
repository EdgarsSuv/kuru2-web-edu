/**
 * @vendor      Kurufootwear
 * @module      Kurufootwear_Theme
 * @author      Krisjanis Veinbahs <krisjanisv@scandiweb.com>
 * @copyright   Copyright (c) 2018 Scandiweb. All rights reserved.
 * @license     http://opensource.org/licenses/afl-3.0.php Academic Free License (AFL 3.0)
 */
define([
    'jquery',
    'uiComponent',
    'underscore',
    'Magento_Customer/js/customer-data',
    'jquery/jquery-storageapi'
], function ($, Component, _, customerData) {
    'use strict';

    return Component.extend({
        defaults: {
            gtmLoadEvent: 'gtm_loaded',
            cookieMessages: [],
            messages: []
        },

        /** @inheritdoc */
        initialize: function () {
            this._super();
            var messageEngine = this;

            this.cookieMessages = $.cookieStorage.get('mage-messages');
            this.messages = customerData.get('messages').extend({
                disposableCustomerData: 'messages'
            });

            if (!_.isEmpty(this.messages().messages)) {
                customerData.set('messages', {});
            }

            $.cookieStorage.set('mage-messages', '');

            // Bind messages stored in cookies/localstorage
            if (this.cookieMessages && this.cookieMessages.length > 0) {
                messageEngine.waitForDatalayer(function() {
                    messageEngine.cookieMessages.forEach(function(message) {
                        messageEngine.parseAndPushMessage(message);
                    });
                });
            }

            // Bind AJAX/runtime messages
            this.messages.subscribe(function(messageContainer) {
                var messages = messageContainer.messages;

                if (messages.length > 0) {
                    messages.forEach(function (message) {
                        messageEngine.parseAndPushMessage(message);
                    });
                }
            });
        },

        parseAndPushMessage: function(message) {
            switch (message.type) {
                case 'success':
                    break;
                case 'error':
                    // Push only red (error) messages
                    this.pushMessage(message.text, 'red-message')
                    break;
                case 'notice':
                    break;
                default:
                    break;
            }
        },

        pushMessage: function(messageText, messageEventName) {
            function emitMessage() {
                window.dataLayer.push({
                    event: messageEventName || 'message',
                    message: messageText
                });
            }

            if (!window.dataLayer) {
                this.listenForDatalayer(emitMessage())
            } else {
                emitMessage();
            }
        },

        // Kuru specifically has an event added for listening to datalayer 'gtm_loaded'
        listenForDatalayer: function(callback) {
            window.addEventListener(this.gtmLoadEvent, function() {
                callback()
            });
        },

        waitForDatalayer: function(callback) {
            var flag = false;

            // Check if dataLayer is defined
            if (typeof(window.dataLayer) === 'undefined') {
                setTimeout(function() {
                    messageEngine.waitForDatalayer(callback);
                }, 500);

                return false;
            }

            // Check if 'general' event has happened
            window.dataLayer.forEach(function () {
                if (this && this.event === 'general') {
                    flag = true;
                }
            });

            if (!flag) {
                var messageEngine = this;
                setTimeout(function() {
                    messageEngine.waitForDatalayer(callback);
                }, 500);
                return false;
            }
            callback();
        }
    });
});
