/**
 * @vendor      Kurufootwear
 * @theme       Kurufootwear_Theme
 * @author      Krisjanis Veinbahs <krisjanisv@scandiweb.com>
 * @copyright   Copyright (c) 2018 KURU Footwear. All rights reserved.
 */
define([
    'jquery'
], function ($) {
    'use strict';

    $.widget('kuru.cookieNotification', {
        options: {
            cookieAgreementCode: "cookies-accepted",
            cookieAcceptSelector: ".cookie-accept",
            notificationActiveClass: "visible"
        },

        $cookieNotification: undefined,
        $cookieAccept: undefined,

        /**
         * Cookie notification creation
         *
         * @private
         */
        _create: function () {
            var self = this;
            var $cookieNotification = this.$cookieNotification = $(this.element);
            var $cookieAccept = this.$cookieAccept = $cookieNotification.find(this.options.cookieAcceptSelector);
            var cookiesAreAccepted = this.getCookieAgreement();

            if (!cookiesAreAccepted) {
                self.showNotification()

                $cookieAccept.click(function() {
                    self.setCookieAgreement();
                    self.hideNotification();
                })
            }
        },

        /**
         * Show cookie notification block
         */
        showNotification: function() {
            var $cookieNotification = this.$cookieNotification;

            $cookieNotification.addClass(this.options.notificationActiveClass);
        },

        /**
         * Hide cookie notification block
         */
        hideNotification: function() {
            var $cookieNotification = this.$cookieNotification;

            $cookieNotification.removeClass(this.options.notificationActiveClass);
        },

        /**
         * Get the cookie, which confirms agreement to cookies
         */
        getCookieAgreement: function() {
            return $.cookie(this.options.cookieAgreementCode);
        },

        /**
         * Set the cookie, which confirms agreement to cookies
         */
        setCookieAgreement: function(agreementValue) {
            if (agreementValue === undefined) {
                agreementValue = true;
            }

            return $.cookie(this.options.cookieAgreementCode, agreementValue);
        }
    });

    return $.kuru.cookieNotification;
});
