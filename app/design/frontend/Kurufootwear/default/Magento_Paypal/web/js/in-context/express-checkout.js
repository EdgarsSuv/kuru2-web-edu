/**
 * @category     Kurufootwear
 * @package      Kurufootwear/Paypal
 * @author       Aleksejs Baranovs <info@scandiweb.com>
 * @copyright    Copyright (c) 2018 KURU Footwear. All rights reserved.
 */
define(
    [
        'underscore',
        'jquery',
        'uiComponent',
        'paypalInContextExpressCheckout',
        'Magento_Customer/js/customer-data',
        'domReady!'
    ],
    function (
        _,
        $,
        Component,
        paypalExpressCheckout,
        customerData
    ) {
        'use strict';

        return Component.extend({

            defaults: {
                clientConfig: {

                    /**
                     * Override to remove body processStart / processStop triggers
                     *
                     * @param {Object} event
                     */
                    click: function (event) {
                        event.preventDefault();

                        paypalExpressCheckout.checkout.initXO();

                        $.post(
                            this.path,
                            {
                                button: 1
                            }
                        ).done(
                            function (response) {
                                if (response && response.url) {
                                    paypalExpressCheckout.checkout.startFlow(response.url);

                                    return;
                                }

                                paypalExpressCheckout.checkout.closeFlow();
                            }
                        ).fail(
                            function () {
                                paypalExpressCheckout.checkout.closeFlow();
                            }
                        ).always(
                            function () {
                                customerData.invalidate(['cart']);
                            }
                        );
                    }
                }
            },

            /**
             * @returns {Object}
             */
            initialize: function () {
                this._super();

                return this.initClient();
            },

            /**
             * @returns {Object}
             */
            initClient: function () {
                _.each(this.clientConfig, function (fn, name) {
                    if (typeof fn === 'function') {
                        this.clientConfig[name] = fn.bind(this);
                    }
                }, this);

                paypalExpressCheckout.checkout.setup(this.merchantId, this.clientConfig);

                return this;
            }
        });
    }
);
