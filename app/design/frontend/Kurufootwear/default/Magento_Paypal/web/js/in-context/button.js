/**
 * @category     Kurufootwear
 * @package      Kurufootwear/Paypal
 * @author       Krisjanis Veinbahs <krisjanisv@kurufootwear.com>
 * @copyright    Copyright (c) 2018 KURU Footwear. All rights reserved.
 */
define([
    'uiComponent',
    'jquery',
    'domReady!'
], function (
    Component,
    $
) {
    'use strict';

    return Component.extend({

        defaults: {},

        /**
         * @returns {Object}
         */
        initialize: function () {
            this._super();

            return this.initClick();
        },

        /**
         * @returns {Object}
         */
        initClick: function () {
            var self = this;

            $('.action-braintree-paypal-logo').off('click').on('click', function(event) {
                event.preventDefault();
                $('#' + self.paypalButton).click();
            });

            return this;
        }
    });
});
