<?php
/**
 * @vendor Scandiweb
 * @theme Kurufootwear_default
 * @copyright Copyright (c) 2016 Magento. All rights reserved.
 * @license http://opensource.org/licenses/afl-3.0.php Academic Free License (AFL 3.0)
 */
\Magento\Framework\Component\ComponentRegistrar::register(
    \Magento\Framework\Component\ComponentRegistrar::THEME,
    'frontend/Kurufootwear/default',
    __DIR__
);
