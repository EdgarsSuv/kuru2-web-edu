var config = {
    map: {
        '*': {
            theme: 'js/theme',
            selectric: 'js/vendor/jquery_selectric',
            arrive: 'js/vendor/arrive_min',
            maskInput : 'js/vendor/jquery.mask',
            scrollToFixed: 'js/vendor/scrolltofixed',
            customValidation: 'js/validation-rules',
            tippy: 'js/vendor/tippy',
            cycle: 'js/vendor/cycle',
            gtmEmitter: 'js/gtm-emitter'
        }
    },
    deps: [
        'js/error_logger',
        'js/vendor/jquery_sticky',
        'js/stick_header',
        'js/stick_bag',
        'js/tooltips',
        'js/checkout_cart'
    ],
    shim: {
        'js/vendor/arrive_min': {
            deps: ['jquery', 'jquery/ui']
        },
        'js/vendor/scrolltofixed': {
            deps: ['jquery', 'jquery/ui']
        },
        'js/vendor/jquery_selectric': {
            deps: ['jquery', 'jquery/ui']
        },
        'js/vendor/cycle': {
            deps: ['jquery', 'jquery/ui']
        },
        'js/vendor/tippy': {
            deps: ['jquery', 'jquery/ui']
        }
    }
};
