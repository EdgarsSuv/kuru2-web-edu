/**
 * @vendor Kurufootwear
 * @module Kurufootwear_Checkout
 * @author Krisjanis Veinbahs <krisjanisv@scandiweb.com>
 * @copyright Copyright (c) 2017 Scandiweb. All rights reserved.
 * @license http://opensource.org/licenses/afl-3.0.php Academic Free License (AFL 3.0)
 */

var config = {
    map: {
        '*': {
            disableButtonAfterClick: 'Magento_Checkout/js/action/disableButtonAfterClick',
            regionUpdater: 'Magento_Checkout/js/region-updater-custom'
        }
    },

    config: {
        mixins: {
            'Magento_Checkout/js/checkout-data': {
                'Magento_Checkout/js/checkout-data-mixin': true
            },
            'Magento_Checkout/js/model/checkout-data-resolver': {
                'Magento_Checkout/js/model/checkout-data-resolver-mixin': true
            },
            'Magento_Checkout/js/view/shipping': {
                'Magento_Checkout/js/view/shipping-mixin': true
            },
            'Magento_Checkout/js/view/minicart': {
                'Magento_Checkout/js/view/minicart-mixin': true
            },
            'Magento_Checkout/js/view/summary/abstract-total': {
                'Magento_Checkout/js/view/summary/abstract-total-mixin': true
            },
            'Magento_Checkout/js/view/summary/grand-total': {
                'Magento_Checkout/js/view/summary/grand-total-mixin': true
            },
            'Magento_Checkout/js/view/shipping-address/address-renderer/default': {
                'Magento_Checkout/js/view/shipping-address/address-renderer/default-mixin': true
            },
            'Magento_Checkout/js/view/form/element/email': {
                'Magento_Checkout/js/view/form/element/email-mixin': true
            },
            'Magento_Checkout/js/view/payment/list': {
                'Magento_Checkout/js/view/payment/list-mixin': true
            },
            'Magento_Checkout/js/view/payment/default': {
                'Magento_Checkout/js/view/payment/default-mixin': true
            },
            'Magento_Checkout/js/model/step-navigator': {
                'Magento_Checkout/js/model/step-navigator-mixin': true
            }
        }
    }
};
