/**
 * @category     Kurufootwear
 * @package      Kurufootwear/default
 * @copyright    Copyright (c) 2018 Scandiweb, Inc (http://scandiweb.com)
 * @license      http://opensource.org/licenses/OSL-3.0 The Open Software License 3.0 (OSL-3.0)
 */
define([
    'uiComponent'
], function (Component) {
    "use strict";

    return Component.extend({
        /**
         * Default component properties
         */
        defaults: {
            template: 'Magento_Checkout/summary/item/details',
            namesData: window.checkoutConfig.namesData
        },

        /**
         * Get name from checkoutConfig namesData
         *
         * @param quoteItem
         *
         * @return string
         */
        getName: function(quoteItem) {
            return this.namesData[quoteItem.item_id];
        },

        /**
         * Thumbnail alt property name value
         *
         * @param quoteItem
         */
        getValue: function(quoteItem) {
            return this.getName(quoteItem);
        }
    });
});
