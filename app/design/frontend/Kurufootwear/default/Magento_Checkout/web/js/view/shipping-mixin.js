/**
 * @vendor Kurufootwear
 * @module Kurufootwear_Checkout
 * @author Krisjanis Veinbahs <krisjanisv@scandiweb.com>
 * @copyright Copyright (c) 2017 Scandiweb. All rights reserved.
 * @license http://opensource.org/licenses/afl-3.0.php Academic Free License (AFL 3.0)
 */
define(
    [
        'jquery',
        'underscore',
        'Magento_Ui/js/form/form',
        'ko',
        'Magento_Customer/js/model/customer',
        'Magento_Customer/js/model/address-list',
        'Magento_Checkout/js/model/address-converter',
        'Magento_Checkout/js/model/quote',
        'Magento_Checkout/js/action/select-shipping-address',
        'Magento_Checkout/js/model/shipping-rates-validator',
        'Magento_Checkout/js/model/shipping-service',
        'Magento_Checkout/js/model/step-navigator',
        'Magento_Checkout/js/model/checkout-data-resolver',
        'Magento_Checkout/js/checkout-data',
        'mage/translate',
        'Magento_Checkout/js/model/shipping-rate-service'
    ],
    function (
        $,
        _,
        Component,
        ko,
        customer,
        addressList,
        addressConverter,
        quote,
        selectShippingAddress,
        shippingRatesValidator,
        shippingService,
        stepNavigator,
        checkoutDataResolver,
        checkoutData,
        $t
    ) {
        return function(shippingComponent) {
            return shippingComponent.extend({
                isFormInline: ko.observable(addressList().length === 0),

                /**
                 * Initialize and set initial formIsInline
                 */
                initialize: function () {
                    this._super();

                    checkoutData.setIsShippingFormInline(this.isFormInline);

                    return this;
                },

                showInlineForm: function() {
                    this.isFormInline(true);
                    checkoutData.setIsShippingFormInline(this.isFormInline);
                    $('.shipping-address-item.selected-item').addClass('unselected').removeClass('selected-item');
                },

                hideInlineForm: function() {
                    this.isFormInline(false);
                    checkoutData.setIsShippingFormInline(this.isFormInline);
                },

                /**
                 * Get standard or non-standard carrier rates
                 * @param standard
                 */
                carrierRates: function(standard) {
                    var rates = shippingService.getShippingRates();

                    return rates.filter(function(rate) {
                        if (standard) {
                            return rate.method_code === 'bestway';
                        } else {
                            return rate.method_code !== 'bestway';
                        }
                    }).map(function(rate) {
                        if (rate.amount === 0) {
                            rate.updated_carrier = 'FREE ' + rate.carrier_title;
                            rate.updated_method = 'FREE ' + rate.method_title;
                        } else {
                            rate.updated_carrier = rate.carrier_title;
                            rate.updated_method = rate.method_title;
                        }

                        return rate;
                    });
                },

                expeditedRates: function() {
                    return this.carrierRates(false);
                },

                standardRates: function() {
                    return this.carrierRates(true);
                },

                isStandardRateFree: function() {
                    return this.standardRates()[0]['amount'] === 0;
                },

                /**
                 * @return {Boolean}
                 */
                validateShippingInformation: function () {
                    var shippingAddress,
                        addressData,
                        loginFormSelector = 'form[data-role=email-with-possible-login]',
                        emailValidationResult = customer.isLoggedIn();

                    if (!quote.shippingMethod()) {
                        this.errorValidationMessage('Please specify a shipping method.');

                        return false;
                    }

                    if (!customer.isLoggedIn()) {
                        $(loginFormSelector).validation();
                        emailValidationResult = Boolean($(loginFormSelector + ' input[name=username]').valid());
                    }

                    var isFormInline = checkoutData.getIsShippingFormInline();
                    if (isFormInline()) {
                        this.source.set('params.invalid', false);
                        this.source.trigger('shippingAddress.data.validate');

                        if (this.source.get('shippingAddress.custom_attributes')) {
                            this.source.trigger('shippingAddress.custom_attributes.data.validate');
                        }

                        if (this.source.get('params.invalid') ||
                            !quote.shippingMethod().method_code ||
                            !quote.shippingMethod().carrier_code ||
                            !emailValidationResult
                        ) {
                            return false;
                        }

                        shippingAddress = quote.shippingAddress();
                        addressData = addressConverter.formAddressDataToQuoteAddress(
                            this.source.get('shippingAddress')
                        );

                        //Copy form data to quote shipping address object
                        for (var field in addressData) {

                            if (addressData.hasOwnProperty(field) &&
                                shippingAddress.hasOwnProperty(field) &&
                                typeof addressData[field] !== 'function' &&
                                _.isEqual(shippingAddress[field], addressData[field])
                            ) {
                                shippingAddress[field] = addressData[field];
                            } else if (typeof addressData[field] !== 'function' &&
                                !_.isEqual(shippingAddress[field], addressData[field])) {
                                shippingAddress = addressData;
                                break;
                            }
                        }

                        if (customer.isLoggedIn()) {
                            shippingAddress.save_in_address_book = 1;
                        }
                        selectShippingAddress(shippingAddress);
                    }

                    if (!emailValidationResult) {
                        $(loginFormSelector + ' input[name=username]').focus();

                        return false;
                    }

                    return true;
                }
            });
        };
    }
);
