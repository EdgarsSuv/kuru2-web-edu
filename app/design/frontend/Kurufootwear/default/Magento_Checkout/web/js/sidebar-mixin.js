/**
 * @category     Kurufootwear
 * @package      Kurufootwear/default
 * @copyright    Copyright (c) 2017 Scandiweb, Inc (http://scandiweb.com)
 * @license      http://opensource.org/licenses/OSL-3.0 The Open Software License 3.0 (OSL-3.0)
 */
define([
    'jquery',
    'Magento_Customer/js/customer-data'
], function ($, customerData) {
    return function(sidebarWidget) {
        $.widget('mage.sidebar', $.mage.sidebar, {
            /*
             * Update sidebar block.
             */
            update: function () {
                $(this.options.targetElement).trigger('contentUpdated');
                this._calcHeight();
                this._isOverflowed();
            },

            _initContent: function () {
                var self = this,
                    events = {};

                this.element.decorate('list', this.options.isRecursive);

                events['click ' + this.options.button.close] = function (event) {
                    event.stopPropagation();
                    $(self.options.targetElement).dropdownDialog('close');
                };
                events['click ' + this.options.button.checkout] = $.proxy(function () {
                    var cart = customerData.get('cart'),
                        customer = customerData.get('customer');

                    if (!customer().firstname && cart().isGuestCheckoutAllowed === false) {
                        // set URL for redirect on successful login/registration. It's postprocessed on backend.
                        $.cookie('login_redirect', this.options.url.checkout);
                        if (this.options.url.isRedirectRequired) {
                            location.href = this.options.url.loginUrl;
                        } else {
                            authenticationPopup.showModal();
                        }

                        return false;
                    }
                    location.href = this.options.url.checkout;
                }, this);
                events['click ' + this.options.button.remove] =  function (event) {
                    event.stopPropagation();
                    confirm({
                        content: self.options.confirmMessage,
                        actions: {
                            confirm: function () {
                                self._removeItem($(event.currentTarget));
                            },
                            always: function (event) {
                                event.stopImmediatePropagation();
                            }
                        }
                    });
                };
                events['keyup ' + this.options.item.qty] = function (event) {
                    self._showItemButton($(event.target));
                };
                events['click ' + this.options.item.button] = function (event) {
                    event.stopPropagation();
                    self._updateItemQty($(event.currentTarget));
                };
                events['focusout ' + this.options.item.qty] = function (event) {
                    self._validateQty($(event.currentTarget));
                };

                this._on(this.element, events);
                this._calcHeight();
                this._isOverflowed();
            }
        });

        return $.mage.sidebar;
    };
});
