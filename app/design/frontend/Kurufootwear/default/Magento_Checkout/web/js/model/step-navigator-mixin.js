/**
 * @category     Kurufootwear
 * @package      Kurufootwear/Checkout
 * @author       Aleksejs Baranovs <info@scandiweb.com>
 * @copyright    Copyright (c) 2018 KURU Footwear. All rights reserved.
 */

define([
    'jquery',
    'Magento_Checkout/js/model/quote'
], function ($, quote) {
    return function(stepNavigator) {
        /**
         * Handle hash
         * Override to add check for shipping address existence and passed first step
         *
         * @returns {boolean}
         */
        stepNavigator.handleHash = function() {
            var hashString = window.location.hash.replace('#', '');

            if (hashString == '') {
                return false;
            }

            if (-1 == $.inArray(hashString, this.validCodes)) {
                window.location.href = window.checkoutConfig.pageNotFoundUrl;

                return false;
            }

            var isRequestedStepVisible = stepNavigator.steps.sort(this.sortItems).some(function(element) {
                return (element.code == hashString || element.alias == hashString) && element.isVisible();
            });

            // If requested step is visible, then we don't need to load step data from server
            if (isRequestedStepVisible) {
                return false;
            }

            // Override to add check for shipping address existence and passed first step
            if (hashString === 'payment' && quote.shippingAddress().email === undefined
                || hashString === 'payment' && !this.isProcessed('shipping')) {
                navigateTo('shipping');

                return false;
            }

            stepNavigator.steps.sort(this.sortItems).forEach(function(element) {
                if (element.code == hashString || element.alias == hashString) {
                    element.navigate();
                } else {
                    element.isVisible(false);
                }

            });

            return false;
        };

        /**
         * Navigate to necessary step
         *
         * @param code
         */
        var navigateTo = function(code) {
            var sortedItems = stepNavigator.steps.sort(stepNavigator.sortItems),
                bodyElem = $.browser.safari || $.browser.chrome ? $('body') : $('html');

            sortedItems.forEach(function(element) {
                if (element.code === code) {
                    element.isVisible(true);
                    bodyElem.animate({scrollTop: $('#' + code).offset().top}, 0, function () {
                        window.location = window.checkoutConfig.checkoutUrl + "#" + code;
                    });
                } else {
                    element.isVisible(false);
                }
            });
        };

        return stepNavigator;
    };
});
