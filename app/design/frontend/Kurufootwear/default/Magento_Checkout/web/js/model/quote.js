/**
 * @category     Kurufootwear
 * @package      Kurufootwear/default
 * @copyright    Copyright (c) 2017 Scandiweb, Inc (http://scandiweb.com)
 * @license      http://opensource.org/licenses/OSL-3.0 The Open Software License 3.0 (OSL-3.0)
 */
define(
    ['ko'],
    function (ko) {
        'use strict';
        var billingAddress = ko.observable(null);
        var shippingAddress = ko.observable(null);
        var shippingMethod = ko.observable(null);
        var paymentMethod = ko.observable(null);
        var quoteData = window.checkoutConfig ? window.checkoutConfig.quoteData : {};
        var basePriceFormat = window.checkoutConfig ? window.checkoutConfig.basePriceFormat : {};
        var priceFormat = window.checkoutConfig ? window.checkoutConfig.priceFormat : {};
        var storeCode = window.checkoutConfig ? window.checkoutConfig.storeCode : {};
        var totalsData = window.checkoutConfig ? window.checkoutConfig.totalsData : {};
        var totals = ko.observable(totalsData);
        var collectedTotals = ko.observable({});
        var isAddressSameAsShippingGuest = ko.observable(null);
        return {
            totals: totals,
            shippingAddress: shippingAddress,
            shippingMethod: shippingMethod,
            billingAddress: billingAddress,
            paymentMethod: paymentMethod,
            guestEmail: null,

            getQuoteId: function() {
                return quoteData.entity_id;
            },
            isVirtual: function() {
                return !!Number(quoteData.is_virtual);
            },
            getPriceFormat: function() {
                return priceFormat;
            },
            getBasePriceFormat: function() {
                return basePriceFormat;
            },
            getItems: function() {
                return window.checkoutConfig.quoteItemData;
            },
            getTotals: function() {
                return totals;
            },
            setTotals: function(totalsData) {
                if (_.isObject(totalsData) && _.isObject(totalsData.extension_attributes)) {
                    _.each(totalsData.extension_attributes, function(element, index) {
                        totalsData[index] = element;
                    });
                }
                totals(totalsData);
                this.setCollectedTotals('subtotal_with_discount', parseFloat(totalsData.subtotal_with_discount));
            },
            setPaymentMethod: function(paymentMethodCode) {
                paymentMethod(paymentMethodCode);
            },
            getPaymentMethod: function() {
                return paymentMethod;
            },
            getStoreCode: function() {
                return storeCode;
            },
            setCollectedTotals: function(code, value) {
                var totals = collectedTotals();
                totals[code] = value;
                collectedTotals(totals);
            },
            getCalculatedTotal: function() {
                var total = 0.;
                _.each(collectedTotals(), function(value) {
                    total += value;
                });
                return total;
            },
            isAddressSameAsShippingGuest: function () {
                return isAddressSameAsShippingGuest;
            }
        };
    }
);
