/**
 * @category     Kurufootwear
 * @package      Kurufootwear/default
 * @copyright    Copyright (c) 2017 Scandiweb, Inc (http://scandiweb.com)
 * @license      http://opensource.org/licenses/OSL-3.0 The Open Software License 3.0 (OSL-3.0)
 */
define([
    'uiComponent',
    'Magento_Customer/js/customer-data',
    'jquery',
    'ko',
    'underscore',
    'js/scrollFix',
    'sidebar'
], function (Component, customerData, $, ko, _, scrollFix) {
    'use strict';

    /**
     * Redefine private parents' minicart element.
     * On minicart toggle also toggle overlay and
     * close mobile menu & header-main
     */
    var miniCart = $('[data-block=\'minicart\']'),
        fixInstance = new scrollFix('block-minicart'),
        minicartItemsWrapperSelector = '.minicart-items-wrapper',
        minicartOverlaySelector = '.minicart-overlay',
        htmlBodySelector = 'body, html',
        desktopBp = 1024,
        animationSpeed = 600;

    miniCart.on('dropdowndialogopen', function () {
        // Close mobile menu
        $(document).trigger('mobileMenuClose');

        if ($(window).width() >= desktopBp) {
            $(minicartItemsWrapperSelector).css('display', 'none');
            $(minicartItemsWrapperSelector).animate({
                left: 0,
                height: 'toggle'
            }, animationSpeed, 'swing');
        } else {
            $(minicartItemsWrapperSelector).css({
                'display': 'block',
                'left': 0
            });
        }

        // Open minicart
        $(htmlBodySelector).addClass('overlayed');
        $(minicartOverlaySelector).addClass('active');

        // Freeze bg
        $(htmlBodySelector).addClass('overlayed');
        fixInstance.disableScroll();
    }).on('dropdowndialogclose', function () {

        if ($(window).width() >= desktopBp) {
            $(minicartItemsWrapperSelector).animate({
                left: 150,
                height: 'toggle'
            }, animationSpeed, 'swing');
        }

        // Close minicart
        $(htmlBodySelector).removeClass('overlayed');
        $(minicartOverlaySelector).removeClass('active');

        // Unfreeze bg
        $(htmlBodySelector).removeClass('overlayed');
        fixInstance.enableScroll();
    });

    $(document).on('minicartClose', function() {
        // Close minicart
        $(minicartOverlaySelector).removeClass('active');

        // Unfreeze bg
        fixInstance.enableScroll();
    });

    return function (minicartComponent) {
        return minicartComponent.extend({
            /**
             * @override
             */
            initialize: function () {
                var parent = this._super(),
                    self = this,
                    cartData = customerData.get('cart'),
                    previousCart,
                    minicartWrapper;

                this.update(cartData());
                previousCart = cartData();
                minicartWrapper = $('.header.content .minicart-wrapper');

                // Add safari class for browser specific fix in stylesheets
                var isSafari = navigator.userAgent.indexOf('Safari') !== -1 && navigator.userAgent.indexOf('Chrome') === -1;
                if (isSafari) {
                    minicartWrapper.addClass('safari')
                }


                cartData.subscribe(function (updatedCart) {
                    function bindInteractionDetection(callback) {
                        $('.block-minicart').on('hover click', callback)
                    }

                    function unbindInteractionDetection() {
                        $('.block-minicart').off('hover click')
                    }

                    /**
                     * If an item has been added
                     * or quantity has increased
                     * open minicart if it's not yet opened
                     * and close if you haven't hovered or clicked it (after 5s)
                     */
                    var addedProducts = updatedCart.summary_count > previousCart.summary_count;
                    if (addedProducts) {
                        self.openMiniCart();
                        minicartWrapper.trigger('opening');

                        if ($(window).width() >= desktopBp) {
                            var closeTimeoutId = setTimeout(function() {
                                self.closeMiniCart();
                                unbindInteractionDetection();
                            }, 5000);

                            bindInteractionDetection(function() {
                                clearTimeout(closeTimeoutId);
                                unbindInteractionDetection();
                            })
                        }
                    }

                    previousCart = updatedCart;
                }, this);

                return parent;
            },

            closeMiniCart: function () {
                $('[data-block="minicart"]').find('[data-role="dropdownDialog"]').dropdownDialog('close');
            },

            openMiniCart: function () {
                $('[data-block="minicart"]').find('[data-role="dropdownDialog"]').dropdownDialog('open');
            },

            /**
             * Triggers qty update on enter
             * @param data
             * @param event
             * @return {boolean}
             */
            onKeyPress: function (data, event) {
                if (!data) {
                    return true;
                }

                var inputValue = parseInt($('.cart-item-qty' + '[data-cart-item="' + data.item_id + '"]').val());

                if (inputValue && event.keyCode === 13 && inputValue !== data.qty) {
                    $('#update-cart-item-' + data.item_id).trigger('click');
                }

                return true;
            },

            /**
             * Updates qty input value for cart item. Shows update button
             * @param item
             * @param newValue
             */
            updateQty: function (item, newValue) {
                if (!item) {
                    return;
                }

                var qtyElement = $('.cart-item-qty' + '[data-cart-item="' + item.item_id + '"]');
                var qtyElementValue = parseInt(qtyElement.val());

                if (!qtyElementValue) {
                    return;
                }

                var newQty = Math.max(qtyElementValue + newValue, 1);

                qtyElement.val(newQty);

                if (newQty !== item.qty) {
                    $('#update-cart-item-' + item.item_id).show('fade', 300);
                } else {
                    $('#update-cart-item-' + item.item_id).hide('fade', 0);
                }
            }
        });
    };
});
