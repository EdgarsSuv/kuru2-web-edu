/**
 * @category     Kurufootwear
 * @package      Kurufootwear/default
 * @copyright    Copyright (c) 2017 Scandiweb, Inc (http://scandiweb.com)
 * @license      http://opensource.org/licenses/OSL-3.0 The Open Software License 3.0 (OSL-3.0)
 */
define(function () {
    return function(emailComponent) {
        return emailComponent.extend({
            checkDelay: 400,
            emailCheckTimeout: 200
        });
    };
});

