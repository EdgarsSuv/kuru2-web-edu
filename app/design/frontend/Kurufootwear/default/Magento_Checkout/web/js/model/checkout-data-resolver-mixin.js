/**
 * @category     Kurufootwear
 * @package      Kurufootwear/default
 * @copyright    Copyright (c) 2017 Scandiweb, Inc (http://scandiweb.com)
 * @license      http://opensource.org/licenses/OSL-3.0 The Open Software License 3.0 (OSL-3.0)
 */
define(
    [
        'Magento_Checkout/js/model/quote',
        'Magento_Checkout/js/checkout-data',
        'Magento_Checkout/js/action/select-shipping-method',
        'Magento_Checkout/js/action/select-billing-address',
        'underscore'
    ],
    function (
        quote,
        checkoutData,
        selectShippingMethodAction,
        selectBillingAddress,
        _
    ) {
        return function(checkoutDataResolver) {
            checkoutDataResolver.resolveShippingRates = function(ratesData) {
                var selectedShippingRate = checkoutData.getSelectedShippingRate(),
                    availableRate = false;

                if (ratesData.length >= 1) {
                    //preselect shipping rate
                    if(!selectedShippingRate) {
                        selectShippingMethodAction(ratesData[0]);
                    }
                }

                if (quote.shippingMethod()) {
                    availableRate = _.find(ratesData, function (rate) {
                        return rate.carrier_code === quote.shippingMethod().carrier_code &&
                            rate.method_code === quote.shippingMethod().method_code;
                    });
                }

                if (!availableRate && selectedShippingRate) {
                    availableRate = _.find(ratesData, function (rate) {
                        return rate.carrier_code + '_' + rate.method_code === selectedShippingRate;
                    });
                }

                if (!availableRate && window.checkoutConfig.selectedShippingMethod) {
                    availableRate = window.checkoutConfig.selectedShippingMethod;
                    selectShippingMethodAction(window.checkoutConfig.selectedShippingMethod);
                }

                //Unset selected shipping method if not available
                if (!availableRate) {
                    selectShippingMethodAction(null);
                } else {
                    selectShippingMethodAction(availableRate);
                }
            };

            checkoutDataResolver.setBillingToShipping = function() {
                if (quote.billingAddress() && quote.shippingAddress()) {
                    selectBillingAddress(quote.shippingAddress());
                }
            };

            return checkoutDataResolver;
        };
    }
);
