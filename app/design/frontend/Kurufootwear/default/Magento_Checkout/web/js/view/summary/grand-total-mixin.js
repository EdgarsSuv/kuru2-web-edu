/**
 * @category     Kurufootwear
 * @package      Kurufootwear/default
 * @copyright    Copyright (c) 2017 Scandiweb, Inc (http://scandiweb.com)
 * @license      http://opensource.org/licenses/OSL-3.0 The Open Software License 3.0 (OSL-3.0)
 */
define([
    'Magento_Checkout/js/model/quote'
], function (quote) {
    return function (grandTotalComponent) {

        return grandTotalComponent.extend({
            getGrandTotalInclTax: function() {
                var totals = quote.getTotals()(),
                    price;

                if (totals) {
                    price = totals.base_grand_total;
                } else {
                    price = quote.base_grand_total;
                }

                if (price < 0) {
                    price = 0
                }

                return this.getFormattedPrice(price);
            }
        });
    }
});
