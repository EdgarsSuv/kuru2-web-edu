/**
 * @category     Kurufootwear
 * @package      Kurufootwear/Checkout
 * @author       Aleksejs Baranovs <info@scandiweb.com>
 * @copyright    Copyright (c) 2018 KURU Footwear. All rights reserved.
 */

define([
    'jquery',
    'mage/template',
    'jquery/ui',
    'mage/validation',
    'Magento_Checkout/js/region-updater'
], function ($) {
    'use strict';

    $.widget('kuru.customRegionUpdater', $.mage.regionUpdater, {
        /**
         * Update dropdown list based on the country selected
         * Override to update selectric
         *
         * @param {String} country - 2 uppercase letter for country code
         */
        _updateRegion: function (country) {
            this._super(country);

            // Override to update selectric
            if ($().selectric()) {
                $(this.options.regionListId).selectric('refresh');
            }
        }
    });

    return $.kuru.customRegionUpdater;
});
