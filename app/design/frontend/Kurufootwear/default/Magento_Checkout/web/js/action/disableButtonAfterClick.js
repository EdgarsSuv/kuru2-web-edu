/**
 * @vendor      Kurufootwear
 * @module      Kurufootwear_Checkout
 * @author      Aleksejs Baranovs <info@scandiweb.com>
 * @copyright   Copyright (c) 2018 Scandiweb. All rights reserved.
 * @license     http://opensource.org/licenses/afl-3.0.php Academic Free License (AFL 3.0)
 */

define([
    'jquery'
], function ($) {
    'use strict';

    return {
        /**
         * Initialize
         * Disable <a> link button after first click
         */
        initialize: function (buttonClass) {
            var clickCount = 0;

            $(buttonClass).click(function (event) {
                if (clickCount > 0) {
                    event.preventDefault();
                }
                
                $(buttonClass).addClass('disabled');
                clickCount++;
            })
        }
    }
});
