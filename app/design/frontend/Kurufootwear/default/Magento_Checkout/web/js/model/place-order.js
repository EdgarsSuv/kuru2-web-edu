/**
 * @category     Kurufootwear
 * @package      Kurufootwear/default
 * @copyright    Copyright (c) 2017 Scandiweb, Inc (http://scandiweb.com)
 * @license      http://opensource.org/licenses/OSL-3.0 The Open Software License 3.0 (OSL-3.0)
 */
define(
    [
        'mage/storage',
        'Magento_Checkout/js/model/error-processor',
        'Magento_Checkout/js/model/full-screen-loader',
        'Magento_Ui/js/model/messageList'
    ],
    function (storage, errorProcessor, fullScreenLoader, globalMessageList) {
        'use strict';

        return function (serviceUrl, payload, messageContainer) {
            fullScreenLoader.startLoader();

            return storage.post(
                serviceUrl, JSON.stringify(payload)
            ).fail(
                function (response) {
                    messageContainer = globalMessageList;
                    console.log(globalMessageList);
                    errorProcessor.process(response, messageContainer);
                    /**
                     * Jus it cae Hax when it fails again
                     */
                    // var messageData = response.responseJSON.message;
                    // document.getElementById('message-container').innerHTML = '<div data-role="checkout-messages-err" class="messages" data-bind="visible: isVisible(), click: removeAll">\n' +
                    //     '<div class="message message-error error" id="message-error-cc">' +
                    //     '<div data-ui-id="checkout-cart-validationmessages-message-error" data-bind="text: $data">' + messageData + '</div></div></div>';


                    fullScreenLoader.stopLoader();
                }
            );
        };
    }
);
