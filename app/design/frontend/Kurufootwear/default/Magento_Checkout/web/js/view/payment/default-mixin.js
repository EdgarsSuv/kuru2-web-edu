/**
 * @vendor      Kurufootwear
 * @module      Kurufootwear_Checkout
 * @author      Aleksejs Baranovs <info@scandiweb.com>
 * @copyright   Copyright (c) 2018 Scandiweb, Ltd (http://scandiweb.com)
 * @license     http://opensource.org/licenses/afl-3.0.php Academic Free License (AFL 3.0)
 */
define([
    'jquery',
    'Magento_Checkout/js/model/payment/additional-validators',
    'Magento_Checkout/js/action/redirect-on-success'
], function ($, additionalValidators, redirectOnSuccessAction) {
    'use strict';

    return function (paymentMethodDefaultComponent) {
        return paymentMethodDefaultComponent.extend({
            options: {
                placeOrderButtonSelector: '.action.primary.checkout'
            },

            /**
             * Place order.
             * Override to disable "Place Order" button after click
             *
             * @param data
             * @param event
             *
             * @returns {boolean}
             */
            placeOrder: function (data, event) {
                var self = this;

                /* Disable "Place Order" button */
                $(this.options.placeOrderButtonSelector).prop('disabled', true);

                if (event) {
                    event.preventDefault();
                }

                if (this.validate() && additionalValidators.validate()) {
                    this.isPlaceOrderActionAllowed(false);

                    this.getPlaceOrderDeferredObject()
                        .fail(
                            function () {
                                self.isPlaceOrderActionAllowed(true);
                            }
                        ).done(
                            function () {
                                self.afterPlaceOrder();

                                if (self.redirectAfterPlaceOrder) {
                                    redirectOnSuccessAction.execute();
                                }
                            }
                        );

                    return true;
                }

                /* Enable "Place Order" button in case of failed validation */
                $(this.options.placeOrderButtonSelector).prop('disabled', false);

                return false;
            }
        });
    };
});
