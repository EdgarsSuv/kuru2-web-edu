/**
 * @vendor      Kurufootwear
 * @package     Kurufootwear_Checkout
 * @author      Aleksejs Baranovs <info@scandiweb.com>
 * @copyright   Copyright (c) 2018 Scandiweb, Inc (http://scandiweb.com)
 * @license     http://opensource.org/licenses/OSL-3.0 The Open Software License 3.0 (OSL-3.0)
 */

define([
    'jquery',
    'Magento_Ui/js/form/element/abstract',
    'ko',
    'Magento_Ui/js/lib/validation/validator',
    'arrive',
    'maskInput'
], function ($, Element, ko, validator) {
    'use strict';

    return Element.extend({
        elements: {
            countryNameShipping: '.checkout-shipping-address [name=country_id]',
            phoneNameShipping: '.checkout-shipping-address [name=telephone]',
            countryNameBilling: '.checkout-payment-method [name=country_id]',
            phoneNameBilling: '.checkout-payment-method [name=telephone]'
        },

        /**
         * Invokes initialize method of parent class,
         * contains initialization logic
         */
        initialize: function () {
            var self = this;

            _.bindAll(this, 'reset');

            this._super()
                .setInitialValue()
                ._setClasses()
                .initSwitcher();

            $(document).arrive(self.elements.countryNameShipping, function () {
                self.initializeMask($(self.elements.phoneNameShipping), $(self.elements.countryNameShipping));
            });

            $(document).arrive(self.elements.countryNameBilling, function () {
                self.initializeMask($(self.elements.phoneNameBilling), $(self.elements.countryNameBilling));
            });

            return this;
        },

        /**
         * Initialize mask for phone number field
         *
         * @param el
         * @param country
         */
        initializeMask: function (el, country) {
            var self = this;

            if (country.val() === 'US') {
                el.mask('(000) 000-0000', {placeholder: "(___)___-____"});
            }

            self.triggerCountryChange(el, country);
        },

        /**
         * Destruct mask
         *
         * @param el
         */
        destructMask: function (el) {
            el.unmask();
            el.attr('placeholder', this.placeholder);
        },

        /**
         * Trigger country change
         */
        triggerCountryChange: function (el, country) {
            var self = this;

            country.change(function () {
                if ($(this).val() !== 'US') {
                    self.destructMask(el);
                } else {
                    el.mask('(000) 000-0000', {placeholder: "(___)___-____"});
                }
            })
        },

        /**
         * Validates itself by it's validation rules using validator object.
         * If validation of a rule did not pass, writes it's message to
         * 'error' observable property.
         *
         * @returns {Object} Validate information.
         */
        validate: function () {
            var value   = this.value(),
                result  = validator(this.validation, value.replace(/\s+|-|[()]/g, '').replace(/^[+]/, ''), this.validationParams),
                message = !this.disabled() && this.visible() ? result.message : '',
                isValid = this.disabled() || !this.visible() || result.passed;

            this.error(message);
            this.bubble('error', message);

            if (!isValid) {
                this.source.set('params.invalid', true);
            }

            return {
                valid: isValid,
                target: this
            };
        }
    });
});
