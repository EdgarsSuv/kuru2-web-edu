/**
 * @vendor Kurufootwear
 * @module Kurufootwear_Checkout
 * @author Krisjanis Veinbahs <krisjanisv@scandiweb.com>
 * @copyright Copyright (c) 2017 Scandiweb. All rights reserved.
 * @license http://opensource.org/licenses/afl-3.0.php Academic Free License (AFL 3.0)
 */
define([
    'jquery'
], function ($) {
    return function (paymentMethodListComponent) {
        return paymentMethodListComponent.extend({
            initialize: function() {
                this._super();

                var firstMethod = ".payment-methods .payment-group:first-of-type .payment-method:nth-of-type(2) [name='payment[method]']";

                $(firstMethod).click();

                $(document).arrive('.payment-methods', function() {
                    $(firstMethod).click();
                })
            }
        });
    };
});
