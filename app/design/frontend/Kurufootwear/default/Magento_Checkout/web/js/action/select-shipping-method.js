/**
 * @vendor      Kurufootwear
 * @module      Kurufootwear_Checkout
 * @author      Jelena Sorohova <info@scandiweb.com>
 * @copyright   Copyright (c) 2017 Scandiweb. All rights reserved.
 * @license     http://opensource.org/licenses/afl-3.0.php Academic Free License (AFL 3.0)
 */

define([
    '../model/quote',
    'Magento_Checkout/js/action/set-shipping-information'
], function (quote, setShippingAction) {
    return function (shippingMethod) {
        quote.shippingMethod(shippingMethod);

        // Update totals on summary
        setShippingAction([]);
    };
});
