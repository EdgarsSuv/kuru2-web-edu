/**
 * @vendor Kurufootwear
 * @module Kurufootwear_Checkout
 * @author Krisjanis Veinbahs <krisjanisv@scandiweb.com>
 * @copyright Copyright (c) 2017 Scandiweb. All rights reserved.
 * @license http://opensource.org/licenses/afl-3.0.php Academic Free License (AFL 3.0)
 */
define([
    'jquery',
    'Magento_Customer/js/customer-data'
], function ($, storage) {
    return function(checkoutData) {
        /**
         * Redefine private parents cache key
         */
        var cacheKey = 'checkout-data';

        /**
         * Redefine private parent getters and setters
         */
        var getData = function () {
            return storage.get(cacheKey)();
        };

        var saveData = function (data) {
            storage.set(cacheKey, data);
        };

        /**
         * Set initial isShippingFormInline value
         */
        var initData = getData();
        initData['isShippingFormInline'] = null;
        saveData(initData);

        /**
         * Create getter and setter for isShippingFormInline
         * @param data
         */
        checkoutData.setIsShippingFormInline = function(data) {
            var obj = getData();
            obj.isShippingFormInline = data;
            saveData(obj);
        };

        checkoutData.getIsShippingFormInline = function() {
            return getData().isShippingFormInline;
        };

        return checkoutData;
    };
});
