/**
 * @vendor Kurufootwear
 * @module Kurufootwear_Checkout
 * @author Krisjanis Veinbahs <krisjanisv@scandiweb.com>
 * @copyright Copyright (c) 2017 Scandiweb. All rights reserved.
 * @license http://opensource.org/licenses/afl-3.0.php Academic Free License (AFL 3.0)
 */
define([
    'jquery',
    'Magento_Checkout/js/checkout-data',
    'Magento_Checkout/js/action/select-shipping-address'
], function($, checkoutData, selectShippingAddressAction) {
    return function (addressRendererComponent) {
        return addressRendererComponent.extend({
            /** Set selected customer shipping address  */
            selectAddress: function() {
                selectShippingAddressAction(this.address());
                $('.shipping-address-item.unselected').addClass('selected-item').removeClass('unselected');
                checkoutData.setSelectedShippingAddress(this.address().getKey());
                var inlineObserver = checkoutData.getIsShippingFormInline();
                inlineObserver(false);
                checkoutData.setIsShippingFormInline(inlineObserver);
            },

            editAddress: function() {
                var inlineObserver = checkoutData.getIsShippingFormInline();
                inlineObserver(true);
                checkoutData.setIsShippingFormInline(inlineObserver);
            }
        });
    };
});
