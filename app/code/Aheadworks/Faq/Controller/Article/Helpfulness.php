<?php
/**
* Copyright 2016 aheadWorks. All rights reserved.
* See LICENSE.txt for license details.
*/


namespace Aheadworks\Faq\Controller\Article;

use Aheadworks\Faq\Controller\AbstractAction;
use Magento\Framework\Controller\Result\JsonFactory;
use Magento\Framework\Controller\Result\Json;
use Magento\Framework\App\Action\Context;
use Aheadworks\Faq\Api\HelpfulnessManagementInterface;
use Aheadworks\Faq\Model\Config;
use Aheadworks\Faq\Model\Url;
use Magento\Store\Model\StoreManagerInterface;

/**
 * FAQ Helpfulness action
 */
class Helpfulness extends AbstractAction
{
    /**
     * @var JsonFactory
     */
    private $resultJsonFactory;

    /**
     * @var HelpfulnessManagementInterface
     */
    private $helpfulnessManagement;

    /**
     * @var Url
     */
    private $url;

    /**
     * @param Url $url
     * @param Config $config
     * @param Context $context
     * @param JsonFactory $resultJsonFactory
     * @param HelpfulnessManagementInterface $helpfulnessManagement
     * @param StoreManagerInterface $storeManager
     */
    public function __construct(
        Url $url,
        Config $config,
        Context $context,
        JsonFactory $resultJsonFactory,
        HelpfulnessManagementInterface $helpfulnessManagement,
        StoreManagerInterface $storeManager
    ) {
        parent::__construct($context, $storeManager);
        $this->url = $url;
        $this->config = $config;
        $this->resultJsonFactory = $resultJsonFactory;
        $this->helpfulnessManagement = $helpfulnessManagement;
    }

    /**
     * Like\Dislike action
     *
     * @return Json
     */
    public function _execute()
    {
        $data = [];
        $articleId = $this->getRequest()->getParam('articleId');
        $like = $this->getRequest()->getParam('like');
        $dislike = $this->getRequest()->getParam('dislike');

        if (!$like && !$dislike) {
            $this->messageManager->addErrorMessage(__('Something went wrong. Please vote again later.'));
        } else {
            if ($dislike) {
                $this->messageManager->addComplexSuccessMessage(
                    'voteNoSuccessMessage',
                    [
                        'message' => $this->config->getHelpfulnessMessageAfterVotingNo(),
                        'link' => $this->url->getContactUrl(),
                    ]
                );
            } else {
                $this->messageManager->addSuccessMessage(__('Thank you for voting!'));
            }

            $voteResult = $like
                ? $this->helpfulnessManagement->like($articleId)
                : $this->helpfulnessManagement->dislike($articleId);

            $data = [
                'like' => $voteResult->getLikeStatus(),
                'dislike' => $voteResult->getDislikeStatus(),
                'show_rate_after_voting' => $this->config->isHelpfulnessRateAfterVotingEnabled()
            ];
        }

        return $this->resultJsonFactory->create()->setData($data);
    }
}
