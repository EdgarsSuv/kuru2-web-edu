<?php
/**
* Copyright 2016 aheadWorks. All rights reserved.
* See LICENSE.txt for license details.
*/


namespace Aheadworks\Faq\Test\Unit\Model\Article\Source;

use Aheadworks\Faq\Model\Article\Source\IsActive;
use Aheadworks\Faq\Model\Article\Source\IsActiveFilter;
use Magento\Framework\TestFramework\Unit\Helper\ObjectManager;

/**
 * Test for IsActiveFilter
 *
 * @SuppressWarnings(PHPMD.TooManyFields)
 * @SuppressWarnings(PHPMD.CouplingBetweenObjects)
 */
class IsActiveFilterTest extends \PHPUnit_Framework_TestCase
{
    /**
     * @var ObjectManager
     */
    private $objectManager;

    /**
     * @var IsActive|\PHPUnit_Framework_MockObject_MockObject
     */
    private $isActiveMock;

    /**
     * @var IsActiveFilter
     */
    private $isActiveFilterObject;

    /**
     * Initialize model
     */
    public function setUp()
    {
        $this->objectManager = new ObjectManager($this);

        $this->isActiveMock = $this->getMock(IsActive::class, ['toOptionArray'], [], '', false);

        $this->isActiveFilterObject = $this->objectManager->getObject(
            IsActiveFilter::class,
            ['isActive' => $this->isActiveMock]
        );
    }

    /**
     * Return array of options as value-label pairs
     *
     * @covers IsActiveFilter::toOptionArray
     */
    public function testToOptionArray()
    {
        $array = [['label' => 'label1', 'value' => 'value1'], ['label' => 'label2', 'value' => 'value2']];

        $this->isActiveMock
            ->expects($this->once())
            ->method('toOptionArray')
            ->willReturn($array);

        $this->assertEquals($array, $this->isActiveFilterObject->toOptionArray());
    }
}
