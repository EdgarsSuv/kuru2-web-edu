<?php
/**
* Copyright 2016 aheadWorks. All rights reserved.
* See LICENSE.txt for license details.
*/


namespace Aheadworks\Faq\Api\Data;

/**
 * FAQ vote result interface
 *
 * @api
 */
interface VoteResultInterface
{
    /**#@+
     * Constants for keys of data array. Identical to the name of the getter in snake case
     */
    const LIKE_STATUS               = 'like';
    const DISLIKE_STATUS            = 'dislike';

    /**#@-*/

    /**
     * Get like status
     *
     * @return bool
     * @SuppressWarnings(PHPMD.BooleanGetMethodName)
     */
    public function getLikeStatus();

    /**
     * Get dislike status
     *
     * @return bool
     * @SuppressWarnings(PHPMD.BooleanGetMethodName)
     */
    public function getDislikeStatus();

    /**
     * Set like status
     *
     * @param bool $status
     * @return $this
     */
    public function setLikeStatus($status);

    /**
     * Set dislike status
     *
     * @param bool $status
     * @return $this
     */
    public function setDislikeStatus($status);
}
