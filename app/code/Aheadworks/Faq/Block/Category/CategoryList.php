<?php
/**
* Copyright 2016 aheadWorks. All rights reserved.
* See LICENSE.txt for license details.
*/


namespace Aheadworks\Faq\Block\Category;

use Aheadworks\Faq\Block\AbstractTemplate;
use Magento\Framework\Api\SearchCriteriaBuilder;
use Magento\Backend\Block\Widget\Context;
use Magento\Framework\Api\SortOrderBuilder;
use Aheadworks\Faq\Model\Url;
use Aheadworks\Faq\Model\Config;
use Aheadworks\Faq\Model\Category;
use Aheadworks\Faq\Api\Data\CategoryInterface;
use Aheadworks\Faq\Model\Article;
use Aheadworks\Faq\Api\Data\ArticleInterface;
use Aheadworks\Faq\Api\CategoryRepositoryInterface;
use Aheadworks\Faq\Api\ArticleRepositoryInterface;
use Aheadworks\Faq\Api\Data\ArticleSearchResultsInterface;
use Aheadworks\Faq\Api\Data\CategorySearchResultsInterface;

/**
 * FAQ Category list
 */
class CategoryList extends AbstractTemplate
{
    /**
     * @var Url
     */
    private $url;

    /**
     * @var SortOrderBuilder
     */
    private $sortOrderBuilder;

    /**
     * @var SearchCriteriaBuilder
     */
    private $searchCriteriaBuilder;

    /**
     * @var CategoryRepositoryInterface
     */
    private $categoryRepository;

    /**
     * @var ArticleRepositoryInterface
     */
    private $articleRepository;

    /**
     * @var Config
     */
    private $config;

    /**
     * @param Context $context
     * @param SearchCriteriaBuilder $searchCriteriaBuilder
     * @param CategoryRepositoryInterface $categoryRepository
     * @param ArticleRepositoryInterface $articleRepository
     * @param SortOrderBuilder $sortOrderBuilder
     * @param Url $url
     * @param Config $config
     * @param array $data
     */
    public function __construct(
        Context $context,
        SearchCriteriaBuilder $searchCriteriaBuilder,
        CategoryRepositoryInterface $categoryRepository,
        ArticleRepositoryInterface $articleRepository,
        SortOrderBuilder $sortOrderBuilder,
        Url $url,
        Config $config,
        array $data = []
    ) {
        parent::__construct($context, $data);
        $this->url = $url;
        $this->searchCriteriaBuilder = $searchCriteriaBuilder;
        $this->categoryRepository = $categoryRepository;
        $this->articleRepository = $articleRepository;
        $this->sortOrderBuilder = $sortOrderBuilder;
        $this->config = $config;
    }

    /**
     * Retrieve FAQ Title
     *
     * @return string
     */
    public function getTitle()
    {
        return $this->config->getFaqName();
    }

    /**
     * Retrieve categories
     *
     * @return array
     */
    public function getCategories()
    {
        /** \Magento\Framework\Api\SortOrder $sortOrder */
        $sortOrder = $this->sortOrderBuilder
            ->setField(CategoryInterface::SORT_ORDER)
            ->setAscendingDirection()
            ->create();

        /** \Magento\Framework\Api\SearchCriteria $searchCriteria */
        $searchCriteria = $this->searchCriteriaBuilder
            ->addFilter(CategoryInterface::IS_ENABLE, true)
            ->addFilter(CategoryInterface::STORE_IDS, $this->getCurrentStore())
            ->addSortOrder($sortOrder)
            ->create();

        /** @var CategorySearchResultsInterface $articleList */
        $searchResults = $this->categoryRepository->getList($searchCriteria);

        return $searchResults->getItems();
    }

    /**
     * Retrieve articles of one category
     *
     * @param CategoryInterface|Category $category
     * @return array
     */
    public function getCategoryArticles($category)
    {
        $categoryId = $category->getCategoryId();
        /** \Magento\Framework\Api\SortOrder $sortOrder */
        $sortOrder = $this->sortOrderBuilder
            ->setField(ArticleInterface::SORT_ORDER)
            ->setAscendingDirection()
            ->create();
        $sortVotes = $this->sortOrderBuilder
            ->setField(ArticleInterface::VOTES_YES)
            ->setDescendingDirection()
            ->create();
        /** \Magento\Framework\Api\SearchCriteria $searchCriteria */
        $searchCriteria = $this->searchCriteriaBuilder
            ->addFilter('category_id', $categoryId)
            ->addFilter(ArticleInterface::IS_ENABLE, true)
            ->addFilter(ArticleInterface::STORE_IDS, $this->getCurrentStore())
            ->setSortOrders([$sortOrder, $sortVotes])
            ->create();
        /** @var ArticleSearchResultsInterface $articleList */
        $searchResults = $this->articleRepository->getList($searchCriteria);
        return $searchResults->getItems();
    }

    /**
     * Retrieve category icon URL
     *
     * @param CategoryInterface|Category $category
     * @return string
     */
    public function getCategoryIconUrl($category)
    {
        return $this->url->getCategoryIconUrl($category);
    }

    /**
     * Retrieve icon URL for article list
     *
     * @param CategoryInterface|Category $category
     * @return string
     */
    public function getArticleListIconUrl($category)
    {
        return $this->url->getArticleListIconUrl($category);
    }

    /**
     * Retrieve category URL
     *
     * @param CategoryInterface|Category $category
     * @return string
     */
    public function getCategoryUrl($category)
    {
        return $this->url->getCategoryUrl($category);
    }

    /**
     * Retrieve article URL
     *
     * @param ArticleInterface|Article $article
     * @return string
     */
    public function getArticleUrl($article)
    {
        return $this->url->getArticleUrl($article);
    }

    /**
     * Retrieve column count
     *
     * @return int
     */
    public function getDefaultNumberOfColumnsToDisplay()
    {
        return $this->config->getDefaultNumberOfColumnsToDisplay();
    }
}
