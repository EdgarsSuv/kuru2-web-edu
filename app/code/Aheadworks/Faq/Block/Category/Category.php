<?php
/**
* Copyright 2016 aheadWorks. All rights reserved.
* See LICENSE.txt for license details.
*/


namespace Aheadworks\Faq\Block\Category;

use Aheadworks\Faq\Api\Data\CategoryInterface;
use Aheadworks\Faq\Block\AbstractTemplate;
use Magento\Framework\Api\SearchCriteriaBuilder;
use Magento\Framework\View\Element\Template;
use Magento\Backend\Block\Widget\Context;
use Magento\Framework\Api\SortOrderBuilder;
use Aheadworks\Faq\Api\CategoryRepositoryInterface;
use Aheadworks\Faq\Api\ArticleRepositoryInterface;
use Aheadworks\Faq\Model\Config;
use Aheadworks\Faq\Model\Article;
use Aheadworks\Faq\Api\Data\ArticleInterface;
use Aheadworks\Faq\Api\Data\ArticleSearchResultsInterface;
use Aheadworks\Faq\Model\Url;

/**
 * FAQ Category list
 */
class Category extends AbstractTemplate
{
    /**
     * @var Url
     */
    private $url;
    
    /**
     * @var CategoryRepositoryInterface
     */
    private $categoryRepository;

    /**
     * @var SortOrderBuilder
     */
    private $sortOrderBuilder;

    /**
     * @var SearchCriteriaBuilder
     */
    private $searchCriteriaBuilder;

    /**
     * @var ArticleRepositoryInterface
     */
    private $articleRepository;

    /**
     * @var Config
     */
    private $config;

    /**
     * @param Context $context
     * @param CategoryRepositoryInterface $categoryRepository
     * @param ArticleRepositoryInterface $articleRepository
     * @param SearchCriteriaBuilder $searchCriteriaBuilder
     * @param SortOrderBuilder $sortOrderBuilder
     * @param Url $url
     * @param Config $config
     * @param array $data
     */
    public function __construct(
        Context $context,
        CategoryRepositoryInterface $categoryRepository,
        ArticleRepositoryInterface $articleRepository,
        SortOrderBuilder $sortOrderBuilder,
        SearchCriteriaBuilder $searchCriteriaBuilder,
        Config $config,
        Url $url,
        array $data = []
    ) {
        parent::__construct($context, $data);
        $this->url = $url;
        $this->categoryRepository = $categoryRepository;
        $this->searchCriteriaBuilder = $searchCriteriaBuilder;
        $this->articleRepository = $articleRepository;
        $this->sortOrderBuilder = $sortOrderBuilder;
        $this->config = $config;
    }

    /**
     * Retrieve category instance
     *
     * @return string
     */
    public function getCategory()
    {
        $categoryId = $this->getRequest()->getParam('id');
        return $this->categoryRepository->getById($categoryId);
    }

    /**
     * Retrieve category title
     *
     * @return string
     */
    public function getTitle()
    {
        return $this->getCategory()->getName();
    }

    /**
     * Retrieve article list of certain category
     *
     * @return ArticleSearchResultsInterface
     */
    public function getCategoryArticles()
    {
        $categoryId = $this->getRequest()->getParam('id');
        /** \Magento\Framework\Api\SortOrder $sortOrder */
        $sortOrder = $this->sortOrderBuilder
            ->setField(ArticleInterface::SORT_ORDER)
            ->setAscendingDirection()
            ->create();
        $sortVotes = $this->sortOrderBuilder
            ->setField(ArticleInterface::VOTES_YES)
            ->setDescendingDirection()
            ->create();
        /** \Magento\Framework\Api\SearchCriteria $searchCriteria */
        $searchCriteria = $this->searchCriteriaBuilder
            ->addFilter('category_id', $categoryId)
            ->addFilter(ArticleInterface::IS_ENABLE, true)
            ->addFilter(ArticleInterface::STORE_IDS, $this->getCurrentStore())
            ->setSortOrders([$sortOrder, $sortVotes])
            ->create();
        /** @var ArticleSearchResultsInterface $articleList */
        $searchResults = $this->articleRepository->getList($searchCriteria);
        return $searchResults->getItems();
    }

    /**
     * Retrieve article URL
     *
     * @param ArticleInterface|Article $article
     * @return string
     */
    public function getArticleUrl($article)
    {
        return $this->url->getArticleUrl($article);
    }

    /**
     * Retrieve column count
     *
     * @return int
     */
    public function getDefaultNumberOfColumnsToDisplay()
    {
        return $this->config->getDefaultNumberOfColumnsToDisplay();
    }
}
