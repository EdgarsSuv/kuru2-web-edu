<?php
/**
* Copyright 2016 aheadWorks. All rights reserved.
* See LICENSE.txt for license details.
*/


namespace Aheadworks\Faq\Block\Article;

use Magento\Framework\View\Element\Template;
use Magento\Backend\Block\Widget\Context;
use Magento\Customer\Model\Group;
use Aheadworks\Faq\Model\Article as ArticleModel;
use Aheadworks\Faq\Api\ArticleRepositoryInterface;
use Aheadworks\Faq\Api\Data\ArticleInterface;
use Magento\Customer\Model\Session;
use Aheadworks\Faq\Model\Config;
use Aheadworks\Faq\Model\Helpfulness\Manager;

/**
 * FAQ Article
 */
class Voting extends Template
{
    /**
     * @var ArticleRepositoryInterface
     */
    private $articleRepository;

    /**
     * @var Session
     */
    private $customerSession;

    /**
     * @var Config
     */
    private $config;

    /**
     * @var Manager
     */
    private $helpfulnessManager;

    /**
     * @param Config $config
     * @param Context $context
     * @param Session $customerSession
     * @param Manager $helpfulnessManager
     * @param ArticleRepositoryInterface $articleRepository
     * @param array $data
     */
    public function __construct(
        Config $config,
        Context $context,
        Session $customerSession,
        Manager $helpfulnessManager,
        ArticleRepositoryInterface $articleRepository,
        array $data = []
    ) {
        parent::__construct($context, $data);
        $this->articleRepository = $articleRepository;
        $this->customerSession = $customerSession;
        $this->config = $config;
        $this->helpfulnessManager = $helpfulnessManager;
    }
    
    /**
     * Retrieve article instance
     *
     * @return ArticleInterface
     */
    public function getArticle()
    {
        $articleId = $this->getRequest()->getParam('id');

        return $this->articleRepository->getById($articleId);
    }

    /**
     * Check is allowed for helpfulness
     *
     * @return bool
     */
    public function isAllowedHelpfulness()
    {
        $customerGroups = $this->config->getDefaultCustomerGroupsToDisplayHelpfulness();
        if (in_array(Group::CUST_GROUP_ALL, $customerGroups)) {
            return true;
        }

        $currentCustomerGroup = $this->customerSession->getCustomerGroupId();
        if (in_array($currentCustomerGroup, $customerGroups)) {
            return true;
        }

        return false;
    }

    /**
     * Check if user already voted
     *
     * @return bool
     */
    public function isVoted()
    {
        if ($this->getLikeStatus() || $this->getDislikeStatus()) {
            return true;
        }
        return false;
    }

    /**
     * Get like status
     *
     * @return string
     */
    public function getLikeStatus()
    {
        return $this->helpfulnessManager->isSetAction(
            Manager::ACTION_LIKE,
            $this->getArticle()->getArticleId()
        );
    }

    /**
     * Get dislike status
     *
     * @return string
     */
    public function getDislikeStatus()
    {
        return $this->helpfulnessManager->isSetAction(
            Manager::ACTION_DISLIKE,
            $this->getArticle()->getArticleId()
        );
    }

    /**
     * Checks if helpfulness rate is enabled before voting
     *
     * @return bool
     */
    public function isHelpfulnessRateBeforeVotingEnabled()
    {
        return $this->config->isHelpfulnessRateBeforeVotingEnabled();
    }

    /**
     * Checks if helpfulness rate is enabled after voting
     *
     * @return bool
     */
    public function isHelpfulnessRateAfterVotingEnabled()
    {
        return $this->config->isHelpfulnessRateAfterVotingEnabled();
    }

    /**
     * Get helpfulness message after voting No
     *
     * @return mixed
     */
    public function getHelpfulnessMessageAfterVotingNo()
    {
        return $this->config->getHelpfulnessMessageAfterVotingNo();
    }
}
