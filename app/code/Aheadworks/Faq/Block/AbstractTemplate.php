<?php
/**
* Copyright 2016 aheadWorks. All rights reserved.
* See LICENSE.txt for license details.
*/


namespace Aheadworks\Faq\Block;

use Magento\Backend\Block\Template;
use Magento\Store\Model\Store;

/**
 * Class AbstractTemplate
 */
abstract class AbstractTemplate extends Template
{
    /**
     * Get current stores
     *
     * @return int
     */
    protected function getCurrentStore()
    {
        return (int)$this->_storeManager->getStore()->getId();
    }
}
