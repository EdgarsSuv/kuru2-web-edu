<?php
/**
* Copyright 2016 aheadWorks. All rights reserved.
* See LICENSE.txt for license details.
*/


namespace Aheadworks\Faq\Block\Search;

use Magento\Framework\View\Element\Template;
use Magento\Backend\Block\Widget\Context;
use Aheadworks\Faq\Model\Url;
use Aheadworks\Faq\Model\Config;
use Aheadworks\Faq\Model\Article;
use Aheadworks\Faq\Api\Data\ArticleInterface;

/**
 * FAQ search report page content block
 */
class Search extends Template
{
    /**
     * @var Url
     */
    private $url;

    /**
     * @var Config
     */
    private $config;

    /**
     * @param Context $context
     * @param Config $config
     * @param Url $url
     * @param array $data
     */
    public function __construct(
        Context $context,
        Url $url,
        Config $config,
        array $data = []
    ) {
        parent::__construct($context, $data);
        $this->url = $url;
        $this->config = $config;
    }

    /**
     * Retrieve FAQ search results page title
     *
     * @return string
     */
    public function getTitle()
    {
        return $this->config->getFaqName();
    }

    /**
     * Retrieve search query string
     *
     * @return string
     */
    public function getSearchQueryString()
    {
        return $this->getRequest()->getParam($this->getQueryParamName());
    }

    /**
     * Retrieve article URL
     *
     * @param ArticleInterface|Article $article
     * @return string
     */
    public function getArticleUrl($article)
    {
        return $this->url->getArticleUrl($article);
    }

    /**
     * Retrieve FAQ Search results page url
     *
     * @return string
     */
    public function getFaqSearchUrl()
    {
        return $this->url->getSearchResultsPageUrl();
    }

    /**
     * Retrieve FAQ Search query parameter name
     *
     * @return string
     */
    public function getQueryParamName()
    {
        return Url::FAQ_QUERY_PARAM;
    }

    /**
     * Retrieve route name where request came from
     *
     * @return string
     */
    public function getRouteName()
    {
        return substr($this->getRequest()->getPathInfo(), 1);
    }
}
