<?php
/**
* Copyright 2016 aheadWorks. All rights reserved.
* See LICENSE.txt for license details.
*/


namespace Aheadworks\Faq\Model\Category;

use Aheadworks\Faq\Model\ResourceModel\Category\CollectionFactory;
use Aheadworks\Faq\Model\ResourceModel\Category\Collection;
use Aheadworks\Faq\Model\Url;
use Magento\Framework\App\Request\DataPersistorInterface;
use Magento\Ui\DataProvider\AbstractDataProvider;

class DataProvider extends AbstractDataProvider
{
    /**
     * @var Url
     */
    private $url;
    
    /**
     * @var Collection
     */
    protected $collection;

    /**
     * @var DataPersistorInterface
     */
    private $dataPersistor;

    /**
     * @var array
     */
    private $loadedData;

    /**
     * @param string $name
     * @param string $primaryFieldName
     * @param string $requestFieldName
     * @param CollectionFactory $categoryCollectionFactory
     * @param DataPersistorInterface $dataPersistor
     * @param Url $url
     * @param array $meta
     * @param array $data
     */
    public function __construct(
        $name,
        $primaryFieldName,
        $requestFieldName,
        CollectionFactory $categoryCollectionFactory,
        DataPersistorInterface $dataPersistor,
        Url $url,
        array $meta = [],
        array $data = []
    ) {
        $this->collection = $categoryCollectionFactory->create();
        $this->dataPersistor = $dataPersistor;
        $this->meta = $this->prepareMeta($this->meta);
        $this->url = $url;
        parent::__construct($name, $primaryFieldName, $requestFieldName, $meta, $data);
    }

    /**
     * Prepares Meta
     *
     * @param array $meta
     * @return array
     */
    public function prepareMeta(array $meta)
    {
        return $meta;
    }

    /**
     * Get data
     *
     * @return array
     */
    public function getData()
    {
        if (isset($this->loadedData)) {
            return $this->loadedData;
        }
        $items = $this->collection->getItems();

        /** @var \Aheadworks\Faq\Api\Data\CategoryInterface|\Aheadworks\Faq\Model\Category $category */
        foreach ($items as $category) {
            $loadedData = $category->getData();

            if (isset($loadedData['category_icon'])) {
                unset($loadedData['category_icon']);
                $loadedData['category_icon'][0]['name'] = $category->getCategoryIcon();
                $loadedData['category_icon'][0]['url'] = $this->url->getCategoryIconUrl($category);
            }

            if (isset($loadedData['article_list_icon'])) {
                unset($loadedData['article_list_icon']);
                $loadedData['article_list_icon'][0]['name'] = $category->getArticleListIcon();
                $loadedData['article_list_icon'][0]['url'] = $this->url->getArticleListIconUrl($category);
            }

            $this->loadedData[$category->getCategoryId()] = $loadedData;
        }

        $data = $this->dataPersistor->get('faq_category');
        if (!empty($data)) {
            $category = $this->collection->getNewEmptyItem();
            $category->setData($data);
            $this->loadedData[$category->getCategoryId()] = $category->getData();
            $this->dataPersistor->clear('faq_category');
        }

        return $this->loadedData;
    }
}
