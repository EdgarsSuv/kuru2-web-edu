<?php
/**
* Copyright 2016 aheadWorks. All rights reserved.
* See LICENSE.txt for license details.
*/


namespace Aheadworks\Faq\Model\Article;

use \Magento\Framework\Validator\AbstractValidator;
use \Aheadworks\Faq\Model\UrlKeyValidator;
use \Aheadworks\Faq\Model\Article;

/**
 * FAQ Article Validator
 */
class Validator extends AbstractValidator
{
    /**
     * @var UrlKeyValidator
     */
    private $urlKeyValidator;

    /**
     * @param UrlKeyValidator $urlKeyValidator
     */
    public function __construct(UrlKeyValidator $urlKeyValidator)
    {
        $this->urlKeyValidator = $urlKeyValidator;
    }

    /**
     * Validate article data
     *
     * @param Article $article
     * @return bool     Return FALSE if someone item is invalid
     * @SuppressWarnings(PHPMD.CyclomaticComplexity)
     * @SuppressWarnings(PHPMD.NPathComplexity)
     */
    public function isValid($article)
    {
        $errors = [];
        $voteFieldsIsValid = 1;

        if (!\Zend_Validate::is($article->getTitle(), 'NotEmpty')) {
            $errors['title'] = __('Title can\'t be empty.');
        }

        if (!\Zend_Validate::is($article->getUrlKey(), 'NotEmpty')) {
            $errors['url_key'] = __('Url key can\'t be empty.');
        }

        if ($article->getSortOrder() && !\Zend_Validate::is($article->getSortOrder(), 'Digits')) {
            $errors['sort_order'] = __('Sort Order must contain only digits.');
        }

        if ($article->getVotesYes() && !\Zend_Validate::is($article->getVotesYes(), 'Digits')) {
            $errors['votes_yes'] = __('Helpful Votes must contain only digits.');
            $voteFieldsIsValid *= 0;
        }

        if ($article->getTotalVotes() && !\Zend_Validate::is($article->getTotalVotes(), 'Digits')) {
            $errors['total_votes'] = __('Total Votes must contain only digits.');
            $voteFieldsIsValid *= 0;
        }

        if ($voteFieldsIsValid) {
            if ($article->getTotalVotes() < $article->getVotesYes()) {
                $errors['votes_not'] = __('Total Votes can\'t be less Helpful Votes.');
            }
        }

        if (!$this->urlKeyValidator->isValid($article)) {
            $errors = array_merge($errors, $this->urlKeyValidator->getMessages());
        }

        $this->_addMessages($errors);

        return empty($errors);
    }
}
