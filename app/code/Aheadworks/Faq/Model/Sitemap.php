<?php
/**
* Copyright 2016 aheadWorks. All rights reserved.
* See LICENSE.txt for license details.
*/


namespace Aheadworks\Faq\Model;

use Magento\Sitemap\Model\Sitemap as MagentoSitemap;
use Magento\Framework\DataObject;
use Magento\Framework\Model\Context;
use Magento\Framework\Registry;
use Magento\Framework\Escaper;
use Magento\Sitemap\Helper\Data;
use Magento\Framework\Filesystem;
use Magento\Sitemap\Model\ResourceModel\Catalog\CategoryFactory;
use Magento\Sitemap\Model\ResourceModel\Catalog\ProductFactory;
use Magento\Sitemap\Model\ResourceModel\Cms\PageFactory;
use Magento\Framework\App\RequestInterface;
use Magento\Store\Model\StoreManagerInterface;
use Magento\Framework\Stdlib\DateTime;
use Magento\Framework\Stdlib\DateTime\DateTime as ModelDate;
use Magento\Framework\Model\ResourceModel\AbstractResource;
use Magento\Framework\Data\Collection\AbstractDb;
use Magento\Framework\Api\SearchCriteriaBuilder;
use Aheadworks\Faq\Api\CategoryRepositoryInterface;
use Aheadworks\Faq\Api\ArticleRepositoryInterface;
use Aheadworks\Faq\Api\Data\CategoryInterface;
use Aheadworks\Faq\Api\Data\ArticleInterface;
use Aheadworks\Faq\Model\Url;

/**
 * Overide sitemap
 *
 * @SuppressWarnings(PHPMD.CouplingBetweenObjects)
 */
class Sitemap extends MagentoSitemap
{
    /**
     * FAQ Category Change frequency
     */
    const FAQ_CATEGORY_CHANGE_FREQ = 'weekly';

    /**
     * FAQ Article Change frequency
     */
    const FAQ_ARTICLE_CHANGE_FREQ = 'weekly';

    /**
     * FAQ Category Priority
     */
    const FAQ_CATEGORY_PRIORITY = '0.25';

    /**
     * FAQ Article Priority
     */
    const FAQ_ARTICLE_PRIORITY = '0.25';

    /**
     * FAQ Home Page Priority
     */
    const FAQ_HOME_PAGE_PRIORITY = '0.5';

    /**
     * @var CategoryRepositoryInterface
     */
    private $categoryRepository;

    /**
     * @var ArticleRepositoryInterface
     */
    private $articleRepository;

    /**
     * @var SearchCriteriaBuilder
     */
    private $searchCriteriaBuilder;

    /**
     * @var Url
     */
    private $url;

    /**
     * @param SearchCriteriaBuilder $searchCriteriaBuilder
     * @param CategoryRepositoryInterface $categoryRepository
     * @param ArticleRepositoryInterface $articleRepository
     * @param Url $url
     * @param Context $context
     * @param Registry $registry
     * @param Escaper $escaper
     * @param Data $sitemapData
     * @param Filesystem $filesystem
     * @param CategoryFactory $categoryFactory
     * @param ProductFactory $productFactory
     * @param PageFactory $cmsFactory
     * @param ModelDate $modelDate
     * @param StoreManagerInterface $storeManager
     * @param RequestInterface $request
     * @param DateTime $dateTime
     * @param AbstractResource $resource
     * @param AbstractDb $resourceCollection
     * @param array $data
     * @SuppressWarnings(PHPMD.ExcessiveParameterList)
     */
    public function __construct(
        SearchCriteriaBuilder $searchCriteriaBuilder,
        CategoryRepositoryInterface $categoryRepository,
        ArticleRepositoryInterface $articleRepository,
        Url $url,
        Context $context,
        Registry $registry,
        Escaper $escaper,
        Data $sitemapData,
        Filesystem $filesystem,
        CategoryFactory $categoryFactory,
        ProductFactory $productFactory,
        PageFactory $cmsFactory,
        ModelDate $modelDate,
        StoreManagerInterface $storeManager,
        RequestInterface $request,
        DateTime $dateTime,
        AbstractResource $resource = null,
        AbstractDb $resourceCollection = null,
        array $data = []
    ) {
        $this->searchCriteriaBuilder = $searchCriteriaBuilder;
        $this->categoryRepository = $categoryRepository;
        $this->articleRepository = $articleRepository;
        $this->url = $url;
        parent::__construct(
            $context,
            $registry,
            $escaper,
            $sitemapData,
            $filesystem,
            $categoryFactory,
            $productFactory,
            $cmsFactory,
            $modelDate,
            $storeManager,
            $request,
            $dateTime,
            $resource,
            $resourceCollection,
            $data
        );
    }

    /**
     * Initialize sitemap items
     *
     * @return void
     */
    protected function _initSitemapItems()
    {
        parent::_initSitemapItems();
        $this->_sitemapItems[] = $this->getCategoryItems($this->getStoreId());
        $this->_sitemapItems[] = $this->getArticleItems($this->getStoreId());
        $this->_sitemapItems[] = $this->getFaqHomePageItem($this->getStoreId());
    }

    /**
     * Retrieves FAQ homepage sitemap item
     *
     * @param int $storeId
     * @return DataObject
     */
    private function getFaqHomePageItem($storeId)
    {
        $faqHomePage[] = new DataObject(
            [
                'id' => 'faq_home',
                'url' => $this->url->getFaqRoute($storeId),
                'updated_at' => (new \DateTime())->format(DateTime::DATETIME_PHP_FORMAT)
            ]
        );

        return new DataObject(
            [
                'changefreq' => self::FAQ_ARTICLE_CHANGE_FREQ,
                'priority' => self::FAQ_HOME_PAGE_PRIORITY,
                'collection' => $faqHomePage
            ]
        );
    }

    /**
     * Retrieves FAQ category sitemap items
     *
     * @param int $storeId
     * @return DataObject
     */
    private function getCategoryItems($storeId)
    {
        /** \Magento\Framework\Api\SearchCriteria $searchCriteria */
        $searchCriteria = $this->searchCriteriaBuilder
            ->addFilter(CategoryInterface::IS_ENABLE, true)
            ->addFilter(CategoryInterface::STORE_IDS, $storeId)
            ->create();
        $categories = $this->categoryRepository->getList($searchCriteria)->getItems();

        $categoryItems = [];
        foreach ($categories as $category) {
            $categoryItems[$category->getCategoryId()] = new DataObject(
                [
                    'id' => $category->getCategoryId(),
                    'url' => $this->url->getCategoryRoute($category, $storeId),
                    'updated_at' => (new \DateTime())
                        ->format(DateTime::DATETIME_PHP_FORMAT)
                ]
            );
        }

        return new DataObject(
            [
                'changefreq' => self::FAQ_CATEGORY_CHANGE_FREQ,
                'priority' => self::FAQ_CATEGORY_PRIORITY,
                'collection' => $categoryItems
            ]
        );
    }

    /**
     * Retrieves FAQ article sitemap items
     *
     * @param int $storeId
     * @return DataObject
     */
    private function getArticleItems($storeId)
    {
        /** \Magento\Framework\Api\SearchCriteria $searchCriteria */
        $searchCriteria = $this->searchCriteriaBuilder
            ->addFilter(ArticleInterface::IS_ENABLE, true)
            ->addFilter(ArticleInterface::STORE_IDS, $storeId)
            ->create();
        $articles = $this->articleRepository->getList($searchCriteria)->getItems();

        $articleItems = [];
        foreach ($articles as $article) {
            $articleItems[$article->getArticleId()] = new DataObject(
                [
                    'id' => $article->getArticleId(),
                    'url' => $this->url->getArticleRoute($article, $storeId),
                    'updated_at' => (new \DateTime())
                        ->format(DateTime::DATETIME_PHP_FORMAT)
                ]
            );
        }

        return new DataObject(
            [
                'changefreq' => self::FAQ_ARTICLE_CHANGE_FREQ,
                'priority' => self::FAQ_ARTICLE_PRIORITY,
                'collection' => $articleItems
            ]
        );
    }
}
