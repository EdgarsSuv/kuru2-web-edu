<?php
/**
* Copyright 2016 aheadWorks. All rights reserved.
* See LICENSE.txt for license details.
*/


namespace Aheadworks\Faq\Model;

use Magento\Customer\Model\Group;
use Magento\Customer\Model\Session;
use Magento\Store\Api\Data\WebsiteInterface;
use Magento\Store\Model\Store;
use Magento\Store\Model\ScopeInterface;
use Magento\Framework\App\Config\ScopeConfigInterface;
use Magento\Store\Model\StoreManagerInterface;

/**
 * FAQ config model
 */
class Config
{
    /**
     * Default number of columns to display config path
     */
    const XML_PATH_NUMBER_OF_COLUMNS = 'faq/general/number_of_columns';

    /**
     * Default number of columns to display config path
     */
    const XML_PATH_FAQ_SEARCH_ENABLED = 'faq/general/faq_search_enabled';

    /**
     * Show FAQ link in Categories menu config path
     */
    const XML_PATH_NAVIGATION_MENU_LINK_ENABLED = 'faq/general/navigation_menu_link_enabled';

    /**
     * FAQ name path
     */
    const XML_PATH_FAQ_NAME = 'faq/general/faq_name';

    /**
     * Customer groups who have not access to FAQ
     */
    const XML_PATH_GROUPS_WITH_DISABLED_FAQ = 'faq/general/groups_with_disabled_faq';

    /**
     * FAQ route config path
     */
    const XML_PATH_FAQ_ROUTE = 'faq/general/faq_route';

    /**
     * FAQ meta title config path
     */
    const XML_PATH_FAQ_META_TITLE = 'faq/general/meta_title';

    /**
     * FAQ meta description config path
     */
    const XML_PATH_FAQ_META_DESCRIPTION = 'faq/general/meta_description';

    /**
     * Default customer groups to display helpfulness in Articles config path
     */
    const XML_PATH_HELPFULNESS_CUSTOMER_GROUPS = 'faq/helpfulness/helpfulness_customer_groups';

    /**
     * Show FAQ helpfulness rate before voting in Articles config path
     */
    const XML_PATH_HELPFULNESS_RATE_BEFORE_VOTING_ENABLED = 'faq/helpfulness/helpfulness_rate_before_voting';

    /**
     * Show FAQ helpfulness rate after voting in Articles config path
     */
    const XML_PATH_HELPFULNESS_RATE_AFTER_VOTING_ENABLED = 'faq/helpfulness/helpfulness_rate_after_voting';

    /**
     * Default FAQ helpfulness message after voting No in Articles config path
     */
    const XML_PATH_HELPFULNESS_MESSAGE_AFTER_VOTING_NO = 'faq/helpfulness/helpfulness_message_after_voting_no';

    /**
     * Core store config
     *
     * @var ScopeConfigInterface
     */
    private $scopeConfig;

    /**
     * Customer session
     *
     * @var Session
     */
    private $session;

    /**
     * @var StoreManagerInterface
     */
    private $storeManager;

    /**
     * @param ScopeConfigInterface $scopeConfig
     * @param Session $session
     * @param StoreManagerInterface $storeManager
     */
    public function __construct(
        ScopeConfigInterface $scopeConfig,
        Session $session,
        StoreManagerInterface $storeManager
    ) {
        $this->scopeConfig = $scopeConfig;
        $this->session = $session;
        $this->storeManager = $storeManager;
    }

    /**
     * Get faq display column count on FAQ homepage
     *
     * @param null|string|bool|int|Store $store
     * @return int
     */
    public function getDefaultNumberOfColumnsToDisplay($store = null)
    {
        return $this->scopeConfig->getValue(
            self::XML_PATH_NUMBER_OF_COLUMNS,
            ScopeInterface::SCOPE_STORE,
            $store
        );
    }

    /**
     * Get faq Storefront Name
     *
     * @return string
     */
    public function getFaqName()
    {
        return $this->scopeConfig->getValue(
            self::XML_PATH_FAQ_NAME,
            ScopeInterface::SCOPE_STORE
        );
    }

    /**
     * Get faq route
     *
     * @param int $storeId
     * @return string
     * @internal param StoreManagerInterface $store
     * @internal param WebsiteInterface $website
     * @internal param StoreInterface $store
     */
    public function getFaqRoute($storeId = null)
    {
        $websiteCode = null;

        if ($storeId) {
            $websiteId = $this->storeManager->getStore($storeId)->getWebsiteId();
            $websiteCode = $this->storeManager->getWebsite($websiteId)->getCode();
        }

        return $this->scopeConfig->getValue(
            self::XML_PATH_FAQ_ROUTE,
            ScopeInterface::SCOPE_WEBSITE,
            $websiteCode
        );
    }

    /**
     * Get faq Meta Title
     *
     * @return string
     */
    public function getFaqMetaTitle()
    {
        return $this->scopeConfig->getValue(
            self::XML_PATH_FAQ_META_TITLE,
            ScopeInterface::SCOPE_STORE
        );
    }

    /**
     * Get faq Meta Description
     *
     * @return string
     */
    public function getFaqMetaDescription()
    {
        return $this->scopeConfig->getValue(
            self::XML_PATH_FAQ_META_DESCRIPTION,
            ScopeInterface::SCOPE_STORE
        );
    }

    /**
     * Checks if FAQ search is enabled
     *
     * @return bool
     */
    public function isFaqSearchEnabled()
    {
        return $this->scopeConfig->isSetFlag(
            self::XML_PATH_FAQ_SEARCH_ENABLED,
            ScopeInterface::SCOPE_STORE
        );
    }

    /**
     * Checks if FAQ link in Categories is enabled
     *
     * @return bool
     */
    public function isNavigationMenuLinkEnabled()
    {
        return $this->scopeConfig->isSetFlag(
            self::XML_PATH_NAVIGATION_MENU_LINK_ENABLED,
            ScopeInterface::SCOPE_STORE
        );
    }

    /**
     * Get customer groups for FAQ Article helpfulness
     *
     * @return array
     */
    public function getDefaultCustomerGroupsToDisplayHelpfulness()
    {
        $settingsValue = $this->scopeConfig->getValue(
            self::XML_PATH_HELPFULNESS_CUSTOMER_GROUPS,
            ScopeInterface::SCOPE_STORE
        );

        return explode(',', $settingsValue);
    }

    /**
     * Checks if FAQ helpfulness rate before voting in Articles enabled
     *
     * @return bool
     */
    public function isHelpfulnessRateBeforeVotingEnabled()
    {
        return $this->scopeConfig->isSetFlag(
            self::XML_PATH_HELPFULNESS_RATE_BEFORE_VOTING_ENABLED,
            ScopeInterface::SCOPE_STORE
        );
    }

    /**
     * Checks if FAQ helpfulness rate after voting in Articles enabled
     *
     * @return bool
     */
    public function isHelpfulnessRateAfterVotingEnabled()
    {
        return $this->scopeConfig->isSetFlag(
            self::XML_PATH_HELPFULNESS_RATE_AFTER_VOTING_ENABLED,
            ScopeInterface::SCOPE_STORE
        );
    }

    /**
     * Get FAQ helpfulness message after voting in Articles
     *
     * @return string
     */
    public function getHelpfulnessMessageAfterVotingNo()
    {
        return $this->scopeConfig->getValue(
            self::XML_PATH_HELPFULNESS_MESSAGE_AFTER_VOTING_NO,
            ScopeInterface::SCOPE_STORE
        );
    }

    /**
     * Get customer groups with disabled FAQ
     *
     * @return array
     */
    public function getGroupsWithDisabledFaq()
    {
        $settingsValue = $this->scopeConfig->getValue(
            self::XML_PATH_GROUPS_WITH_DISABLED_FAQ,
            ScopeInterface::SCOPE_STORE
        );

        return explode(',', $settingsValue);
    }

    /**
     * Check disabling FAQ for current user
     *
     * @return bool
     */
    public function isDisabledFaqForCurrentCustomer()
    {
        $groups = $this->getGroupsWithDisabledFaq();
        $groupId = (string)$this->session->getCustomerGroupId();
        return in_array($groupId, $groups) || in_array(Group::CUST_GROUP_ALL, $groups);
    }
}
