<?php
/**
* Copyright 2016 aheadWorks. All rights reserved.
* See LICENSE.txt for license details.
*/


namespace Aheadworks\Faq\Model;

use Aheadworks\Faq\Api\Data\ArticleInterface;
use Magento\Framework\DataObject\IdentityInterface;
use Magento\Framework\Model\AbstractModel;
use Aheadworks\Faq\Model\ResourceModel\Article as ArticleResource;

/**
 * FAQ Article model
 *
 */
class Article extends AbstractModel implements ArticleInterface, IdentityInterface
{
    /**
     * FAQ article cache tag
     */
    const CACHE_TAG = 'faq_article';

    /**
     * {@inheritdoc}
     */
    protected function _construct()
    {
        $this->_init(ArticleResource::class);
    }

    /**
     * Return unique ID(s) for each object in system
     *
     * @return array
     */
    public function getArticleId()
    {
        return $this->getData(self::ARTICLE_ID);
    }

    /**
     * Return unique ID(s) for each object in system
     *
     * @return array
     */
    public function getIdentities()
    {
        return [self::CACHE_TAG . '_' . $this->getArticleId()];
    }

    /**
     * Get category id
     *
     * @return int|null
     */
    public function getCategoryId()
    {
        return $this->getData(self::CATEGORY_ID);
    }

    /**
     * Get number of views
     *
     * @return integer|null
     */
    public function getViewCount()
    {
        return $this->getData(self::VIEWS_COUNT);
    }

    /**
     * Get store view
     *
     * @return array of int|null
     */
    public function getStoreIds()
    {
        $ids = $this->getData(self::STORE_IDS);

        if (empty($ids)) {
            return null;
        }

        return array_map('intval', (array)$ids);
    }

    /**
     * Is enable
     *
     * @return bool
     * @SuppressWarnings(PHPMD.BooleanGetMethodName)
     */
    public function getIsEnable()
    {
        return (bool)$this->getData(self::IS_ENABLE);
    }

    /**
     * Get creation time
     *
     * @return string
     */
    public function getCreatedAt()
    {
        return $this->getData(self::CREATED_AT);
    }

    /**
     * Get creation time
     *
     * @return string
     */
    public function getUpdatedAt()
    {
        return $this->getData(self::UPDATED_AT);
    }

    /**
     * Get title
     *
     * @return string
     */
    public function getTitle()
    {
        return $this->getData(self::TITLE);
    }

    /**
     * Get URL-key
     *
     * @return string
     */
    public function getUrlKey()
    {
        return $this->getData(self::URL_KEY);
    }

    /**
     * Get content
     *
     * @return string|null
     */
    public function getContent()
    {
        return $this->getData(self::CONTENT);
    }

    /**
     * Get number of positive votes
     *
     * @return integer|null
     */
    public function getVotesYes()
    {
        return $this->getData(self::VOTES_YES);
    }

    /**
     * Get number of negative votes
     *
     * @return integer|null
     */
    public function getVotesNo()
    {
        return $this->getData(self::VOTES_NO);
    }

    /**
     * Get meta title
     *
     * @return string|null
     */
    public function getMetaTitle()
    {
        return $this->getData(self::META_TITLE);
    }

    /**
     * Get meta description
     *
     * @return string|null
     */
    public function getMetaDescription()
    {
        return $this->getData(self::META_DESCRIPTION);
    }

    /**
     * Get sort order
     *
     * @return integer|null
     */
    public function getSortOrder()
    {
        return $this->getData(self::SORT_ORDER);
    }

    /**
     * Set ID
     *
     * @param int $id
     * @return \Aheadworks\Faq\Api\Data\ArticleInterface
     */
    public function setArticleId($id)
    {
        return $this->setData(self::ARTICLE_ID, $id);
    }

    /**
     * Set title
     *
     * @param string $title
     * @return \Aheadworks\Faq\Api\Data\ArticleInterface
     */
    public function setTitle($title)
    {
        return $this->setData(self::TITLE, $title);
    }

    /**
     * Set URL-key
     *
     * @param string $urlKey
     * @return \Aheadworks\Faq\Api\Data\ArticleInterface
     */
    public function setUrlKey($urlKey)
    {
        return $this->setData(self::URL_KEY, $urlKey);
    }

    /**
     * Set meta title
     *
     * @param string $metaTitle
     * @return \Aheadworks\Faq\Api\Data\ArticleInterface
     */
    public function setMetaTitle($metaTitle)
    {
        return $this->setData(self::META_TITLE, $metaTitle);
    }

    /**
     * Set meta description
     *
     * @param string $metaDescription
     * @return \Aheadworks\Faq\Api\Data\ArticleInterface
     */
    public function setMetaDescription($metaDescription)
    {
        return $this->setData(self::META_DESCRIPTION, $metaDescription);
    }

    /**
     * Set sort order
     *
     * @param integer $sortOrder
     * @return \Aheadworks\Faq\Api\Data\ArticleInterface
     */
    public function setSortOrder($sortOrder)
    {
        return $this->setData(self::SORT_ORDER, $sortOrder);
    }

    /**
     * Set content
     *
     * @param string $content
     * @return \Aheadworks\Faq\Api\Data\ArticleInterface
     */
    public function setContent($content)
    {
        return $this->setData(self::CONTENT, $content);
    }

    /**
     * Set number of positive votes
     *
     * @param integer $votesYes
     * @return \Aheadworks\Faq\Api\Data\ArticleInterface
     */
    public function setVotesYes($votesYes)
    {
        return $this->setData(self::VOTES_YES, $votesYes);
    }

    /**
     * Set number of negative votes
     *
     * @param integer $votesNo
     * @return \Aheadworks\Faq\Api\Data\ArticleInterface
     */
    public function setVotesNo($votesNo)
    {
        return $this->setData(self::VOTES_NO, $votesNo);
    }

    /**
     * Get total votes
     *
     * @return int
     */
    public function getTotalVotes()
    {
        return $this->getVotesNo() + $this->getVotesYes();
    }

    /**
     * Set is enable
     *
     * @param bool $isEnable
     * @return \Aheadworks\Faq\Api\Data\ArticleInterface
     */
    public function setIsEnable($isEnable)
    {
        return $this->setData(self::IS_ENABLE, $isEnable);
    }

    /**
     * Set creation time
     *
     * @param string $creationTime
     * @return \Aheadworks\Faq\Api\Data\ArticleInterface
     */
    public function setCreatedAt($creationTime)
    {
        return $this->setData(self::CREATED_AT, $creationTime);
    }

    /**
     * Set update time
     *
     * @param string $updateTime
     * @return \Aheadworks\Faq\Api\Data\ArticleInterface
     */
    public function setUpdatedAt($updateTime)
    {
        return $this->setData(self::UPDATED_AT, $updateTime);
    }

    /**
     * Set store view
     *
     * @param string $storeIds
     * @return \Aheadworks\Faq\Api\Data\ArticleInterface
     */
    public function setStoreIds($storeIds)
    {
        return $this->setData(self::STORE_IDS, $storeIds);
    }

    /**
     * Set number of views
     * @param integer $viewCount
     * @return \Aheadworks\Faq\Api\Data\ArticleInterface
     */
    public function setViewsCount($viewCount)
    {
        return $this->setData(self::VIEWS_COUNT, $viewCount);
    }

    /**
     * Set store category Ids
     *
     * @param int $categoryId
     * @return \Aheadworks\Faq\Api\Data\ArticleInterface
     */
    public function setCategoryId($categoryId)
    {
        return $this->setData(self::CATEGORY_ID, $categoryId);
    }
}
