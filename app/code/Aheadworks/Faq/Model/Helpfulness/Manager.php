<?php
/**
* Copyright 2016 aheadWorks. All rights reserved.
* See LICENSE.txt for license details.
*/


namespace Aheadworks\Faq\Model\Helpfulness;

use Magento\Customer\Model\Session;
use Magento\Customer\Model\Visitor;
use Aheadworks\Faq\Model\ResourceModel\Votes as VotesResource;

/**
 * FAQ article helpfulness manager
 */
class Manager
{
    const ACTION_LIKE = 'like';
    const ACTION_DISLIKE = 'dislike';

    /**
     * @var Session
     */
    private $customerSession;

    /**
     * @var Visitor
     */
    private $visitor;

    /**
     * @var VotesResource
     */
    private $votesResource;

    /**
     * @var array
     */
    private $actions = [];

    /**
     * @param Session $customerSession
     * @param Visitor $visitor
     * @param VotesResource $votesResource
     */
    public function __construct(
        Session $customerSession,
        Visitor $visitor,
        VotesResource $votesResource
    ) {
        $this->customerSession = $customerSession;
        $this->visitor = $visitor;
        $this->votesResource = $votesResource;
    }

    /**
     * Add vote action
     *
     * @param string $action
     * @param int $articleId
     * @return $this
     */
    public function addAction($action, $articleId)
    {
        $this->cleanActionsCached($action, $articleId);
        if ($this->customerSession->isLoggedIn()) {
            $this->votesResource->addCustomerAction(
                $this->customerSession->getCustomerId(),
                $articleId,
                $action
            );
        } else {
            $this->votesResource->addVisitorAction(
                $this->visitor->getId(),
                $articleId,
                $action
            );
        }

        return $this;
    }

    /**
     * Remove vote action
     *
     * @param string $action
     * @param int $articleId
     * @return $this
     */
    public function removeAction($action, $articleId)
    {
        $this->cleanActionsCached($action, $articleId);
        if ($this->customerSession->isLoggedIn()) {
            $this->votesResource->removeCustomerAction(
                $this->customerSession->getCustomerId(),
                $articleId,
                $action
            );
        } else {
            $this->votesResource->removeVisitorAction(
                $this->visitor->getId(),
                $articleId,
                $action
            );
        }
        return $this;
    }

    /**
     * Check vote status
     *
     * @param string $action
     * @param int $articleId
     * @return bool
     */
    public function isSetAction($action, $articleId)
    {
        $key = $action . '-' . $articleId;
        if (!isset($this->actions[$key])) {
            if ($this->customerSession->isLoggedIn()) {
                $this->actions[$key] = $this->votesResource->isSetCustomerAction(
                    $this->customerSession->getCustomerId(),
                    $articleId,
                    $action
                );
            } else {
                $this->actions[$key] = $this->votesResource->isSetVisitorAction(
                    $this->visitor->getId(),
                    $articleId,
                    $action
                );
            }
        }

        return $this->actions[$key];
    }

    /**
     * Clean actions cached
     *
     * @param string $action
     * @param int $articleId
     * @return $this
     */
    private function cleanActionsCached($action, $articleId)
    {
        $key = $action . '-' . $articleId;
        if (isset($this->actions[$key])) {
            unset($this->actions[$key]);
        }

        return $this;
    }
}
