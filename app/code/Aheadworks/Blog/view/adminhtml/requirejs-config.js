/**
* Copyright 2016 aheadWorks. All rights reserved.
* See LICENSE.txt for license details.
*/

var config = {
    map: {
        '*': {
            blogPostMetaCharCount:  'Aheadworks_Blog/js/post/char-count'
        }
    },
    shim: {
        'jquerytokenize':           ['jquery']
    },
    paths: {
        'jquerytokenize':           'Aheadworks_Blog/js/lib/jquery.tokenize'
    }
};
