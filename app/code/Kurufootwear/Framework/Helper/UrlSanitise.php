<?php
/**
 * @category    Kurufootwear
 * @package     Kurufootwear\Framework
 * @author      Krisjanis Veinbahs <krisjanisv@scandiweb.com>
 * @copyright   Copyright (c) 2018 KURU Footwear. All rights reserved.
 */
namespace Kurufootwear\Framework\Helper;

use Exception;
use Psr\Log\LoggerInterface;
use Zend\Uri\Http;

/**
 * Class UrlSanitise
 *
 * @package Kurufootwear\Helper
 */
class UrlSanitise
{
    /**
     * @var LoggerInterface
     */
    private $logger;

    /**
     * UrlSanitise constructor.
     *
     * @param LoggerInterface $logger
     */
    public function __construct(
        LoggerInterface $logger
    ) {
       $this->logger = $logger;
    }

    /**
     * Remove trailing slashes from url
     *
     * @param $url
     *
     * @return bool|string
     */
    public function sanitiseTrailingSlash($url)
    {
        try {
            $urlStructured = new Http($url);
        } catch (Exception $exception) {
            $this->logger->critical(sprintf(
                __("Can't sanitise trailing slash on URL %s"),
                print_r($url, true)
            ));

            return $url;
        }

        // Remove trailing slash if it exists in URL path
        $urlPath = $urlStructured->getPath();
        if (substr($urlPath, -1) === "/") {
            $urlStructured->setPath(
                substr($urlPath, 0, -1)
            );
        }

        return $urlStructured->toString();
    }
}