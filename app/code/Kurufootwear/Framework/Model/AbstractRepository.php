<?php
/**
 * @category    Kurufootwear
 * @package     Kurufootwear\Framework
 * @author      Jim McGowen <jim@kurufootwear.com>
 * @copyright   Copyright (c) 2018 KURU Footwear. All rights reserved.
 */

namespace Kurufootwear\Framework\Model;

use Kurufootwear\Framework\Api\AbstractRepositoryInterface;
use Magento\Framework\ObjectManagerInterface;
use Magento\Framework\Exception\CouldNotSaveException;
use Magento\Framework\Exception\NoSuchEntityException;
use Magento\Framework\Exception\CouldNotDeleteException;
use Magento\Framework\Model\AbstractModel;
use Magento\Framework\Model\ResourceModel\Db\AbstractDb;
use Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection;
use Magento\Framework\Api\SearchCriteriaInterface;
use Magento\Framework\Api\SearchResultsInterfaceFactory;
use Magento\Framework\Api\SortOrder;

/**
 * Class AbstractRepository
 * @package Kurufootwear\Framework\Model
 */
abstract class AbstractRepository implements AbstractRepositoryInterface
{
    /** @var ObjectManagerInterface  */
    protected $objectManager;
    
    /** @var string */
    protected $modelName;
    
    /** @var string */
    protected $resourceModelName;
    
    /** @var string */
    protected $collectionName;
    
    /** @var AbstractDb  */
    protected $resource;
    
    /** @var SearchResultsInterfaceFactory  */
    protected $searchResultsFactory;
    
    /**
     * AbstractRepository constructor.
     *
     * @param ObjectManagerInterface $objectManager
     * @param SearchResultsInterfaceFactory $searchResultsFactory
     */
    public function __construct(
        ObjectManagerInterface $objectManager,
        SearchResultsInterfaceFactory $searchResultsFactory
    ) {
        $this->objectManager = $objectManager;
        $this->searchResultsFactory = $searchResultsFactory;
        
        $this->_construct();
    }
    
    /**
     * Resource initialization. Concrete classes should implement and call _init.
     */
    abstract protected function _construct();
    
    /**
     * @param string $model
     * @param string $resourceModel
     */
    protected function _init($model, $resourceModel)
    {
        $this->modelName = $model;
        $this->resourceModelName = $resourceModel;
        $this->collectionName = $this->resourceModelName . \Zend_Loader_StandardAutoloader::NS_SEPARATOR . 'Collection';
    }
    
    /**
     * Initializes and returns the resource model.
     * 
     * @return AbstractDb
     */
    public function getResource()
    {
        if (empty($this->resource)) {
            $this->resource = $this->objectManager->create($this->resourceModelName);
        }
        return $this->resource;
    }
    
    /**
     * @inheritdoc
     */
    public function save(AbstractModel $object)
    {
        try {
            $this->getResource()->save($object);
        }
        catch (\Exception $e) {
            throw new CouldNotSaveException(__($e->getMessage()));
        }
        
        return $object;
    }
    
    /**
     * @inheritdoc
     */
    public function getById($id)
    {
        /** @var AbstractModel $object */
        $object = $this->objectManager->create($this->modelName);
        $this->resource->load($object, $id);
        if (!$object->getId()) {
            throw new NoSuchEntityException(__('Object with id "%1" does not exist.', $id));
        }
        
        return $object;
    }
    
    /**
     * @inheritdoc
     */
    public function delete(AbstractModel $object)
    {
        try {
            $this->getResource()->delete($object);
        }
        catch (\Exception $exception) {
            throw new CouldNotDeleteException(__($exception->getMessage()));
        }
        
        return true;
    }
    
    /**
     * @inheritdoc
     */
    public function deleteById($id)
    {
        try {
            $this->getResource()->delete($this->getById($id));
        }
        catch(\Exception $exception) {
            throw new CouldNotDeleteException(__($exception->getMessage()));
        }
        
        return true;
    }
    
    /**
     * @inheritdoc
     */
    public function getList(SearchCriteriaInterface $criteria)
    {
        $searchResults = $this->searchResultsFactory->create();
        $searchResults->setSearchCriteria($criteria);
    
        /** @var AbstractCollection $collection */
        $collection = $this->objectManager->create($this->collectionName);
        foreach ($criteria->getFilterGroups() as $filterGroup) {
            $fields = [];
            $conditions = [];
            foreach ($filterGroup->getFilters() as $filter) {
                $condition = $filter->getConditionType() ? $filter->getConditionType() : 'eq';
                $fields[] = $filter->getField();
                $conditions[] = [$condition => $filter->getValue()];
            }
            if ($fields) {
                $collection->addFieldToFilter($fields, $conditions);
            }
        }
        $searchResults->setTotalCount($collection->getSize());
        $sortOrders = $criteria->getSortOrders();
        if ($sortOrders) {
            /** @var SortOrder $sortOrder */
            foreach ($sortOrders as $sortOrder) {
                $collection->addOrder(
                    $sortOrder->getField(),
                    ($sortOrder->getDirection() == SortOrder::SORT_ASC) ? 'ASC' : 'DESC'
                );
            }
        }
        $collection->setCurPage($criteria->getCurrentPage());
        $collection->setPageSize($criteria->getPageSize());
        $objects = [];
        foreach ($collection as $objectModel) {
            $objects[] = $objectModel;
        }
        $searchResults->setItems($objects);
    
        return $searchResults;
    }
}
