<?php
/**
 * @category    Kurufootwear
 * @package     Kurufootwear\Framework
 * @author      Krisjanis Veinbahs <krisjanisv@scandiweb.com>
 * @copyright   Copyright (c) 2018 KURU Footwear. All rights reserved.
 */
namespace Kurufootwear\Framework\Plugin;

use Kurufootwear\Framework\Helper\UrlSanitise;
use Magento\Framework\App\State;
use Magento\Framework\UrlInterface;

/**
 * Class UrlInterfacePlugin
 *
 * @package Kurufootwear\Framework\Plugin
 */
class UrlInterfacePlugin
{
    /**
     * @var UrlSanitise
     */
    protected $urlSanitise;

    /**
     * @var State
     */
    protected $appState;

    /**
     * UrlInterfacePlugin constructor.
     *
     * @param UrlSanitise $urlSanitise
     * @param State $appState
     */
    public function __construct(
        UrlSanitise $urlSanitise,
        State $appState
    ) {
        $this->urlSanitise = $urlSanitise;
        $this->appState = $appState;
    }

    /**
     * Remove trailing slash from generated URLs
     *
     * @param UrlInterface $subject
     * @param $url
     *
     * @return bool|string
     */
    public function afterGetUrl(
        UrlInterface $subject,
        $url
    ) {
        /**
         * Backend doesn't require trailing slash SEO changes
         * and actually this change conflicts with some URL generation,
         * because some BE URL's expect getUrl to return a trailing slash
         * and probably do some manual URL manipulation
         *
         * e.g. BE does $urlInterface->getUrl('source') . 'file/from/source';
         * which causes a faulty link e.g. kurufootwear.com/sourcefile/from/source
         * as opposed to a good link e.g. kurufootwear.com/source/file/from/source
         */
        try {
            $isBackend = $this->appState->getAreaCode() === 'adminhtml';
        } catch (\Exception $exception) {
            $isBackend = true;
        }

        if (is_string($url) && !$isBackend) {
            $trailingUnslashedUrl = $this->urlSanitise->sanitiseTrailingSlash($url);

            if (is_string($trailingUnslashedUrl)) {
                $url = $trailingUnslashedUrl;
            }
        }

        return $url;
    }
}