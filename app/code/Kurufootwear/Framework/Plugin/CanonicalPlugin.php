<?php
/**
 * @category    Kurufootwear
 * @package     Kurufootwear\Framework
 * @author      Krisjanis Veinbahs <krisjanisv@scandiweb.com>
 * @copyright   Copyright (c) 2018 KURU Footwear. All rights reserved.
 */
namespace Kurufootwear\Framework\Plugin;

use Kurufootwear\Framework\Helper\UrlSanitise;
use MageWorx\SeoBase\Model\Canonical;

/**
 * Class UrlInterfacePlugin
 *
 * Plugin for removing trailing slash from canonical links
 *
 * @package Kurufootwear\Framework\Plugin
 */
class CanonicalPlugin
{
    /**
     * @var UrlSanitise
     */
    protected $urlSanitise;

    /**
     * @var bool
     */
    private $removeSlash;

    /**
     * UrlInterfacePlugin constructor.
     *
     * @param UrlSanitise $urlSanitise
     * @param bool $removeSlash
     */
    public function __construct(
        UrlSanitise $urlSanitise,
        $removeSlash = true
    ) {
        $this->urlSanitise = $urlSanitise;
        $this->removeSlash = $removeSlash;
    }

    /**
     * Remove trailing slash from generated URLs
     *
     * @param Canonical $subject
     * @param $url
     *
     * @return bool|string
     */
    public function afterTrailingSlash(
        Canonical $subject,
        $url
    ) {
        if ($this->removeSlash) {
            $trailingUnslashedUrl = $this->urlSanitise->sanitiseTrailingSlash($url);

            if (is_string($trailingUnslashedUrl)) {
                $url = $trailingUnslashedUrl;
            }
        }

        return $url;
    }
}