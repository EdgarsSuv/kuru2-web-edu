<?php
/**
 * @category    Kurufootwear
 * @package     Kurufootwear\Framework
 * @author      Krisjanis Veinbahs <krisjanisv@scandiweb.com>
 * @copyright   Copyright (c) 2018 KURU Footwear. All rights reserved.
 */
namespace Kurufootwear\Framework\Plugin;

use InvalidArgumentException;
use Magento\Framework\App\Request\Http;
use Magento\Framework\App\State;
use Magento\Framework\Exception\InputException;
use Magento\Framework\Exception\LocalizedException;
use Magento\Framework\Exception\SessionException;
use Magento\Framework\Phrase;
use Magento\Framework\Profiler;
use Magento\Framework\Session\Config\ConfigInterface;
use Magento\Framework\Session\SaveHandlerInterface;
use Magento\Framework\Session\SessionManager;
use Magento\Framework\Session\SessionManager as MagentoSessionManager;
use Magento\Framework\Session\SidResolverInterface;
use Magento\Framework\Session\StorageInterface;
use Magento\Framework\Session\ValidatorInterface;
use Magento\Framework\Stdlib\Cookie\CookieMetadataFactory;
use Magento\Framework\Stdlib\Cookie\CookieSizeLimitReachedException;
use Magento\Framework\Stdlib\Cookie\FailureToSendException;
use Magento\Framework\Stdlib\CookieManagerInterface;

/**
 * Class SessionPersistencePlugin
 *
 * A regular override of the class doesn't work as Magento core developers did not
 * use implements SessionManagerInterface for Session implementation, but instead used
 * extends SessionManager (a specific implementation of the interface), which can't be overridden with di.xml
 *
 * Original fix reference - https://github.com/magento/magento2/pull/14973
 *
 * @package Kurufootwear\Framework\Plugin
 */
class SessionPersistencePlugin
{
    /** @var ValidatorInterface  */
    protected $validator;

    /** @var State  */
    protected $appState;

    /** @var SidResolverInterface  */
    protected $sidResolver;

    /** @var StorageInterface  */
    protected $storage;

    /** @var ConfigInterface  */
    protected $sessionConfig;

    /** @var SaveHandlerInterface  */
    protected $saveHandler;

    /** @var Http  */
    protected $request;

    /** @var CookieMetadataFactory  */
    protected $cookieMetadataFactory;

    /** @var CookieManagerInterface  */
    protected $cookieManager;

    /**
     * SesssionStartPlugin constructor.
     *
     * @param SidResolverInterface $sidResolver
     * @param State $appState
     * @param ValidatorInterface $validator
     * @param StorageInterface $storage
     * @param ConfigInterface $sessionConfig
     * @param SaveHandlerInterface $saveHandler
     * @param Http $request
     * @param CookieMetadataFactory $cookieMetadataFactory
     * @param CookieManagerInterface $cookieManager
     */
    public function __construct(
        SidResolverInterface $sidResolver,
        State $appState,
        ValidatorInterface $validator,
        StorageInterface $storage,
        ConfigInterface $sessionConfig,
        SaveHandlerInterface $saveHandler,
        Http $request,
        CookieMetadataFactory $cookieMetadataFactory,
        CookieManagerInterface $cookieManager
    ) {
        $this->sidResolver = $sidResolver;
        $this->appState = $appState;
        $this->validator = $validator;
        $this->storage = $storage;
        $this->sessionConfig = $sessionConfig;
        $this->saveHandler = $saveHandler;
        $this->request = $request;
        $this->cookieMetadataFactory = $cookieMetadataFactory;
        $this->cookieManager = $cookieManager;
    }

    /**
     * Plugin reimplementation of the start() from the solution
     *
     * @param SessionManager $subject
     * @param callable $proceed
     * @throws CookieSizeLimitReachedException
     * @throws FailureToSendException
     * @throws InputException
     * @throws SessionException
     *
     * @return mixed
     */
    public function aroundStart(SessionManager $subject, callable $proceed)
    {
        if (!$subject->isSessionExists()) {
            Profiler::start('session_start');

            try {
                $this->appState->getAreaCode();
            } catch (LocalizedException $e) {
                throw new SessionException(
                    new Phrase(
                        'Area code not set: Area code must be set before starting a session.'
                    ),
                    $e
                );
            }

            // Need to apply the config options so they can be ready by session_start
            $this->initIniOptions();
            $this->registerSaveHandler();
            if (isset($_SESSION['new_session_id'])) {
                // Not fully expired yet. Could be lost cookie by unstable network.
                session_commit();
                session_id($_SESSION['new_session_id']);
            }
            $sid = $this->sidResolver->getSid($subject);
            // potential custom logic for session id (ex. switching between hosts)
            $subject->setSessionId($this->sidResolver->getSid($subject));
            session_start();
            if (isset($_SESSION['destroyed'])) {
                if ($_SESSION['destroyed'] < time()-300) {
                    $subject->destroy(['clear_storage' => true]);

                }
            }
            $this->validator->validate($subject);
            $this->renewCookie($sid, $subject);

            register_shutdown_function([$subject, 'writeClose']);

            $this->_addHost();
            Profiler::stop('session_start');
        }
        $this->storage->init(isset($_SESSION) ? $_SESSION : []);

        $returnValue = $proceed();
        return $returnValue;
    }

    /**
     * Fixed version of regenerateId() as a plugin based on the fix
     *
     * @param SessionManager $subject
     * @param callable $proceed
     * @throws FailureToSendException
     * @throws InputException
     *
     * @return mixed
     */
    public function aroundRegenerateId(SessionManager $subject, callable $proceed)
    {
        if (headers_sent()) {
            return $proceed();
        }

        if ($subject->isSessionExists()) {
            // Regenerate the session
            session_regenerate_id();
            $new_session_id = session_id();

            $_SESSION['new_session_id'] = $new_session_id;

            // Set destroy timestamp
            $_SESSION['destroyed'] = time();

            // Write and close current session
            session_commit();
            $oldSession = $_SESSION;

            // Start session with the new session id
            session_id($new_session_id);
            ini_set('session.use_strict_mode', 0);
            session_start();
            ini_set('session.use_strict_mode', 1);
            $_SESSION = $oldSession;

            // These are not required in the new session
            unset($_SESSION['destroyed']);
            unset($_SESSION['new_session_id']);
        } else {
            session_start();
        }
        $this->storage->init(isset($_SESSION) ? $_SESSION : []);

        if ($this->sessionConfig->getUseCookies()) {
            $this->clearSubDomainSessionCookie();
        }

        return $proceed();
    }

    /**
     * Renew session cookie to prolong session
     *
     * @param null|string $sid If we have session id we need to use it instead of old cookie value
     * @param SessionManager $pluginSubject
     * @throws FailureToSendException
     * @throws InputException
     * @throws CookieSizeLimitReachedException
     *
     * @return $this
     */
    private function renewCookie($sid, SessionManager $pluginSubject)
    {
        if (!$pluginSubject->getCookieLifetime()) {
            return $this;
        }

        /**
         * When we renew a cookie, we should be sure, that no
         * other session client isn't change a cookie too
         */
        $cookieValue = $sid ?: $this->cookieManager->getCookie($pluginSubject->getName());
        if ($cookieValue) {
            $metadata = $this->cookieMetadataFactory->createPublicCookieMetadata();
            $metadata->setPath($this->sessionConfig->getCookiePath());
            $metadata->setDomain($this->sessionConfig->getCookieDomain());
            $metadata->setDuration($this->sessionConfig->getCookieLifetime());
            $metadata->setSecure($this->sessionConfig->getCookieSecure());
            $metadata->setHttpOnly($this->sessionConfig->getCookieHttpOnly());
            $this->cookieManager->setPublicCookie(
                $pluginSubject->getName(),
                $cookieValue,
                $metadata
            );
        }
        return $this;
    }

    /**
     * Expire the session cookie for sub domains
     *
     * NOTE: Moved due the protected access in the target class
     *
     * @throws InputException
     * @throws FailureToSendException
     *
     * @return void
     */
    protected function clearSubDomainSessionCookie()
    {
        foreach (array_keys($this->_getHosts()) as $host) {
            // Delete cookies with the same name for parent domains
            if (strpos($this->sessionConfig->getCookieDomain(), $host) > 0) {
                $metadata = $this->cookieMetadataFactory->createPublicCookieMetadata();
                $metadata->setPath($this->sessionConfig->getCookiePath());
                $metadata->setDomain($host);
                $metadata->setSecure($this->sessionConfig->getCookieSecure());
                $metadata->setHttpOnly($this->sessionConfig->getCookieHttpOnly());
                $this->cookieManager->deleteCookie($this->getName(), $metadata);
            }
        }
    }

    /**
     * Performs ini_set for all of the config options so they can be read by session_start
     *
     * NOTE: Moved due the protected access in the target class
     *
     * @return void
     */
    private function initIniOptions()
    {
        foreach ($this->sessionConfig->getOptions() as $option => $value) {
            $result = ini_set($option, $value);
            if ($result === false) {
                $error = error_get_last();
                throw new InvalidArgumentException(
                    sprintf('Failed to set ini option "%s" to value "%s". %s', $option, $value, $error['message'])
                );
            }
        }
    }

    /**
     * Register save handler
     *
     * NOTE: Moved due the protected access in the target class
     *
     * @return bool
     */
    protected function registerSaveHandler()
    {
        return session_set_save_handler(
            [$this->saveHandler, 'open'],
            [$this->saveHandler, 'close'],
            [$this->saveHandler, 'read'],
            [$this->saveHandler, 'write'],
            [$this->saveHandler, 'destroy'],
            [$this->saveHandler, 'gc']
        );
    }

    /**
     * Register request host name as used with session
     *
     * NOTE: Moved due the protected access in the target class
     *
     * @return $this
     */
    protected function _addHost()
    {
        $host = $this->request->getHttpHost();
        if (!$host) {
            return $this;
        }

        $hosts = $this->_getHosts();
        $hosts[$host] = true;
        $_SESSION[MagentoSessionManager::HOST_KEY] = $hosts;
        return $this;
    }

    /**
     * Get all host names where session was used
     *
     * NOTE: Moved due the protected access in the target class
     *
     * @return array
     */
    protected function _getHosts()
    {
        return isset($_SESSION[MagentoSessionManager::HOST_KEY]) ? $_SESSION[MagentoSessionManager::HOST_KEY] : [];
    }

}