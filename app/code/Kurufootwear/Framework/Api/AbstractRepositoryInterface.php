<?php
/**
 * @category    Kurufootwear
 * @package     Kurufootwear\Framework
 * @author      Jim McGowen <jim@kurufootwear.com>
 * @copyright   Copyright (c) 2018 KURU Footwear. All rights reserved.
 */

namespace Kurufootwear\Framework\Api;

use Magento\Framework\Api\SearchResultsInterface;
use Magento\Framework\Api\SearchCriteriaInterface;
use Magento\Framework\Model\AbstractModel;

/**
 * Interface AbstractRepositoryInterface
 * @package Kurufootwear\Framework\Api
 */
interface AbstractRepositoryInterface
{
    /**
     * Saves an object.
     * 
     * @param AbstractModel $object
     *
     * @return AbstractModel
     */
    public function save(AbstractModel $object);
    
    /**
     * Retrieves an object by id.
     * 
     * @param int $id
     *
     * @return AbstractModel
     */
    public function getById($id);
    
    /**
     * Retrieves objects matching the specified criteria.
     * 
     * @param SearchCriteriaInterface $criteria
     *
     * @return SearchResultsInterface
     */
    public function getList(SearchCriteriaInterface $criteria);
    
    /**
     * Deletes the object.
     * 
     * @param AbstractModel $object
     * 
     * @return bool
     */
    public function delete(AbstractModel $object);
    
    /**
     * Deletes an object by id.
     * 
     * @param $id
     *
     * @return bool
     */
    public function deleteById($id);
}
