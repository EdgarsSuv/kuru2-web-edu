<?php
/**
 * @category     Kurufootwear
 * @package      Kurufootwear/Ui
 * @author       Krisjanis Veinbahs <info@scandiweb.com>
 * @copyright    Copyright (c) 2018 KURU Footwear. All rights reserved.
 */
namespace Kurufootwear\Ui\Component\MassAction;

use Magento\Framework\Api\FilterBuilder;
use Magento\Framework\App\RequestInterface;
use Magento\Framework\Data\Collection;
use Magento\Framework\Exception\LocalizedException;
use Magento\Framework\View\Element\UiComponentFactory;
use Magento\Ui\Component\MassAction\Filter as MagentoFilter;
use Magento\Framework\View\Element\UiComponent\DataProvider\DataProviderInterface;
use Psr\Log\LoggerInterface;

/**
 * Class Filter
 * Overridden to make the getFilterIds method public
 *
 * @package Kurufootwear\Ui\Component\MassAction
 */
class Filter extends MagentoFilter {
    /**
     * @var DataProviderInterface
     */
    private $dataProvider;

    /**
     * @var LoggerInterface
     */
    protected $logger;

    /**
     * Filter constructor.
     *
     * @param UiComponentFactory $factory
     * @param RequestInterface $request
     * @param FilterBuilder $filterBuilder
     * @param LoggerInterface $logger
     */
    public function __construct(
        UiComponentFactory $factory,
        RequestInterface $request,
        FilterBuilder $filterBuilder,
        LoggerInterface $logger
    ) {
        $this->logger = $logger;

        parent::__construct(
            $factory,
            $request,
            $filterBuilder
        );
    }

    /**
     * Get filter ids as array
     *
     * @return int[]
     */
    public function getFilterIds()
    {
        $ids = [];

        try {
            $this->applySelectionOnTargetProvider();
        } catch (LocalizedException $exception) {
            $this->logger->error($exception->getMessage());

            return $ids;
        }

        $dataProvider = $this->getDataProvider();
        if (empty($dataProvider)) {
            $this->logger->debug(__("Tried to get a filter data provider in admin panel, but failed."));
            return $ids;
        }

        /** @var \Magento\Framework\Api\Search\SearchResultInterface $searchResult */
        $searchResult = $dataProvider->getSearchResult();

        if ($searchResult) {
            if ($searchResult instanceof Collection
                || method_exists($searchResult, 'getAllIds')) {
                $ids = $searchResult->getAllIds();
            } else {
                foreach ($searchResult->getItems() as $document) {
                    $ids[] = $document->getId();
                }
            }
        }

        return $ids;
    }

    /**
     * Get data provider
     *
     * @return DataProviderInterface
     */
    private function getDataProvider()
    {
        if (!$this->dataProvider) {
            try {
                $component = $this->getComponent();
                $this->prepareComponent($component);
                $this->dataProvider = $component->getContext()->getDataProvider();
                $this->dataProvider->setLimit(0, false);
            } catch (LocalizedException $exception) {
                return null;
            }
        }

        return $this->dataProvider;
    }
}