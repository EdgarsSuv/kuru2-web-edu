<?php
/**
 * @category     Kurufootwear
 * @package      Kurufootwear/Ui
 * @author       Krisjanis Veinbahs <info@scandiweb.com>
 * @copyright    Copyright (c) 2018 KURU Footwear. All rights reserved.
 */
\Magento\Framework\Component\ComponentRegistrar::register(
    \Magento\Framework\Component\ComponentRegistrar::MODULE,
    'Kurufootwear_Ui',
    __DIR__
);
