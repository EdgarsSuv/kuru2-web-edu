<?php
/**
 * @category     Kurufootwear
 * @package      Kurufootwear/Maxmind
 * @author       Krisjanis Veinbahs <krisjanisv@kurufootwear.com>
 * @copyright    Copyright (c) 2018 KURU Footwear. All rights reserved.
 */
use Magento\Framework\Component\ComponentRegistrar;

ComponentRegistrar::register(
    ComponentRegistrar::MODULE,
    'Kurufootwear_Maxmind',
    __DIR__
);
