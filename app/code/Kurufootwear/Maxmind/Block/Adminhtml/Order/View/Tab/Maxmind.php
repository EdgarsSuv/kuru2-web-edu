<?php
/**
 * @category     Kurufootwear
 * @package      Kurufootwear/Maxmind
 * @author       Krisjanis Veinbahs <krisjanisv@kurufootwear.com>
 * @copyright    Copyright (c) 2018 KURU Footwear. All rights reserved.
 */

namespace Kurufootwear\Maxmind\Block\Adminhtml\Order\View\Tab;

use WeltPixel\Maxmind\Block\Adminhtml\Order\View\Tab\Maxmind as WeltPixelTab;

class Maxmind extends WeltPixelTab
{
    /**
     * Retrieve maxmind fraud order data
     *
     * @return array
     */
    public function getMaxmindData()
    {
        if (empty($this->_maxmindData)) {
            $orderId = $this->getOrder()->getId();
            $maxmindModel = $this->_maxmindModel->loadMaxmindByOrderId($orderId);

            if ($maxmindModel->getFraudData()) {
                $this->_maxmindData = unserialize(utf8_decode($maxmindModel->getFraudData()));
            } else {
                $this->_maxmindData = [];
            }
        }

        return $this->_maxmindData;
    }

    /**
     * This is needed to take the correct templates
     *
     * @return string
     */
    protected function _toHtml()
    {
        $this->setModuleName($this->extractModuleName('WeltPixel\Maxmind\Block\Adminhtml\Order\View\Tab\Maxmind'));

        return parent::_toHtml();
    }
}
