<?php
/**
 * @vendor Kurufootwear
 * @module Kurufootwear_Shopby
 * @author Krisjanis Veinbahs <info@scandiweb.com>
 * @copyright Copyright (c) 2018 Scandiweb, Ltd (http://scandiweb.com)
 * @license http://opensource.org/licenses/afl-3.0.php Academic Free License (AFL 3.0)
 */
namespace Kurufootwear\Shopby\Block\Adminhtml\Form\Renderer\Fieldset;

use Amasty\Shopby\Block\Adminhtml\Form\Renderer\Fieldset\Element as AmastyElement;

/**
 * Class Element
 * Overridden to use custom template file
 *
 * @package Kurufootwear\Shopby\Block\Adminhtml\Form\Renderer\Fieldset
 */
class Element extends AmastyElement
{
    /**
     * Element template path variable
     * @var string
     */
    protected $_template = 'form/renderer/fieldset/element.phtml';
}
