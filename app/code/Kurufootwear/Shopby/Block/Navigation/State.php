<?php
/**
 * @vendor Kurufootwear
 * @module Kurufootwear_Shopby
 * @author Kristaps Stalidzāns <info@scandiweb.com>
 * @copyright Copyright (c) 2017 Scandiweb, Ltd (http://scandiweb.com)
 * @license http://opensource.org/licenses/afl-3.0.php Academic Free License (AFL 3.0)
 */
namespace Kurufootwear\Shopby\Block\Navigation;

use Amasty\Shopby\Helper\Data as ShopbyHelper;
use Amasty\Shopby\Helper\FilterSetting;
use Magento\Catalog\Model\Layer\Resolver;
use Magento\Framework\View\Element\Template\Context;

class State extends \Amasty\Shopby\Block\Navigation\State
{
    /**
     * @var string
     */
    protected $_template = 'Kurufootwear_Shopby::layer/state.phtml';

    /**
     * @var FilterRenderer
     */
    private $filterRenderer;

    /**
     * State constructor.
     * @param Context $context
     * @param Resolver $layerResolver
     * @param FilterSetting $filterSettingHelper
     * @param ShopbyHelper $helper
     * @param FilterRenderer $filterRenderer
     * @param array $data
     */
    public function __construct(
        Context $context,
        Resolver $layerResolver,
        FilterSetting $filterSettingHelper,
        ShopbyHelper $helper,
        FilterRenderer $filterRenderer,
        array $data = []
    )
    {
        $this->filterRenderer = $filterRenderer;

        parent::__construct(
            $context,
            $layerResolver,
            $filterSettingHelper,
            $helper,
            $data
        );
    }

    /**
     * Returns color data
     *
     * @param $attributeCode
     * @param $value
     * @return string
     */
    public function getFilterColor($attributeCode, $value)
    {
        if ($attributeCode == 'filterable_color') {
            $style = $this->filterRenderer->getAttribute($value);

            return $style;
        }

        return null;
    }
}
