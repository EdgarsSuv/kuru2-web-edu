<?php
/**
 * @vendor Kurufootwear
 * @module Kurufootwear_Shopby
 * @author Kristaps Stalidzāns <info@scandiweb.com>
 * @copyright Copyright (c) 2017 Scandiweb, Ltd (http://scandiweb.com)
 * @license http://opensource.org/licenses/afl-3.0.php Academic Free License (AFL 3.0)
 */
namespace Kurufootwear\Shopby\Block\Navigation;

use Amasty\Shopby\Helper\Category;
use Amasty\Shopby\Helper\UrlBuilder;
use Amasty\Shopby\Helper\FilterSetting;
use Magento\Catalog\Model\Layer\Resolver;
use Amasty\Shopby\Model\Source\DisplayMode;
use Amasty\Shopby\Helper\Data as ShopbyHelper;
use Magento\Store\Model\StoreManagerInterface;
use Amasty\Shopby\Api\Data\FilterSettingInterface;
use Magento\Framework\View\Element\Template\Context;
use Magento\Catalog\Model\ResourceModel\Product\Attribute\CollectionFactory;

class FilterRenderer extends \Amasty\Shopby\Block\Navigation\FilterRenderer
{
    /**
     * @var CollectionFactory
     */
    private $attributeCollectionFactory;

    /**
     * @var StoreManagerInterface
     */
    private $storeManager;

    /**
     * FilterRenderer constructor.
     * @param Context $context
     * @param FilterSetting $settingHelper
     * @param UrlBuilder $urlBuilder
     * @param ShopbyHelper $helper
     * @param Category $categoryHelper
     * @param Resolver $resolver
     * @param CollectionFactory $attributeCollectionFactory
     * @param array $data
     */
    public function __construct(
        Context $context,
        FilterSetting $settingHelper,
        UrlBuilder $urlBuilder,
        ShopbyHelper $helper,
        Category $categoryHelper,
        Resolver $resolver,
        CollectionFactory $attributeCollectionFactory,
        array $data = []
    ) {
        $this->attributeCollectionFactory = $attributeCollectionFactory;
        $this->storeManager = $context->getStoreManager();

        parent::__construct(
            $context,
            $settingHelper,
            $urlBuilder,
            $helper,
            $categoryHelper,
            $resolver,
            $data
        );
    }

    /**
     * Gets admin Store view EAV value
     *
     * @param $value
     * @return string
     */
    public function getAttribute($value)
    {
        $attributes = $this->attributeCollectionFactory->create();
        $attributes->addFieldToFilter('attribute_code', ['eq' => 'filterable_color']);
        $attributes->addIsFilterableFilter();
        $layeredNavAttributes = $attributes->load()->getItems();
        $storeId = $this->storeManager->getStore('admin')->getId();
        $colorLabel = '';

        foreach ($layeredNavAttributes AS $attr) {
            $attr->setStoreId($storeId);
            $options = $attr->getSource()->getAllOptions();

            foreach ($options as $option) {
                if ($option['value'] == $value) {
                    $colorLabel = $this->processColorValue((string)strtolower($option['label']));
                }
            }
        }

        return $colorLabel;
    }

    /**
     * Checks if variables is multiple then returns correct style string
     * It also checks rendering options. If string contains split flag in the front
     * it creates split like color background that is a gradient
     *
     * @param string $value
     * @return mixed
     */
    public function processColorValue($value)
    {
        $flagCheck = (strpos($value, ':') !== false);
        $flagValue = false;

        if (strpos($value, ':') !== false) {
            $data = explode(':', $value);

            if ((strpos($data[1], ',') !== false)) {
                $split = explode(',', $data[1]);
                $flagValue = (strtolower($data[0])  == 'split');
            }
        } else {
            $split = explode(',', $value);
        }

        if (!$flagCheck && (strpos($value, ',') !== false) && (count($split) == 2)) {
            $colorValue = 'style="background: linear-gradient(135deg,' . $split[0] . ' 0%, ' . $split[1] . ' 100%);"';
        } else if (!$flagCheck && (strpos($value, ',') !== false)) {
            $colorValue = 'style="background: linear-gradient(135deg,' . $value . ');"';
        } else if ($flagCheck && isset($split) && $flagValue) {
            $style = 'style="background: linear-gradient(to right';
            $count = count($split) * 2;
            $colors = '';

            /**
             * Multiplies gradient effect so it can be less noticed
             */
            foreach ($split as $splitItems) {
                $colors .= ',' . implode(',', array_fill( 0, $count, strtolower($splitItems)));
            }

            $colorValue = $style . $colors . '' .');"';
        } else {
            $colorValue = 'style="background-color: ' . $value . ';"';
        }

        return $colorValue;
    }

    /**
     * @param FilterSettingInterface $filterSetting
     * @return string
     */
    protected function getTemplateByFilterSetting(FilterSettingInterface $filterSetting)
    {
        switch($filterSetting->getDisplayMode()) {
            case DisplayMode::MODE_SLIDER:
                $template = 'layer/filter/slider.phtml';
                break;
            case DisplayMode::MODE_DROPDOWN:
                $template = 'layer/filter/dropdown.phtml';
                break;
            case DisplayMode::MODE_FROM_TO_ONLY:
                $template = 'layer/filter/fromto.phtml';
                break;
            default:
                $template = 'Kurufootwear_Shopby::layer/filter/default.phtml';
                break;
        }

        return $template;
    }

    /**
     * This is needed to take the correct templates
     *
     * @return string
     */
    protected function _toHtml()
    {
        $this->setModuleName($this->extractModuleName('Amasty\Shopby\Block\Navigation\FilterRenderer'));

        return parent::_toHtml();
    }
}
