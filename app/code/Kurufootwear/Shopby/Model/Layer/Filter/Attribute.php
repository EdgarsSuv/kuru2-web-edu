<?php
/**
 * @vendor Kurufootwear
 * @module Kurufootwear_Shopby
 * @author Kristaps Stalidzāns <info@scandiweb.com>
 * @copyright Copyright (c) 2017 Scandiweb, Ltd (http://scandiweb.com)
 * @license http://opensource.org/licenses/afl-3.0.php Academic Free License (AFL 3.0)
 */
namespace Kurufootwear\Shopby\Model\Layer\Filter;

use Amasty\Shopby\Helper\OptionSetting;
use Amasty\Shopby\Model\Request;
use Kurufootwear\Catalog\Block\Benefits;
use Magento\Catalog\Model\Layer;
use Amasty\Shopby\Helper\Group as GroupHelper;
use Magento\Framework\Filter\StripTags as TagFilter;
use Magento\Catalog\Model\Layer\Filter\ItemFactory as FilterItemFactory;
use Magento\Catalog\Model\Layer\Filter\Item\DataBuilder as ItemDataBuilder;
use Amasty\Shopby\Model\Search\Adapter\Mysql\AggregationAdapter as MysqlAggregationAdapter;
use Amasty\Shopby\Model\Search\RequestGenerator as ShopbyRequestGenerator;
use Magento\Framework\App\RequestInterface;
use Magento\Framework\Filter\StripTags;
use Magento\Store\Model\StoreManagerInterface;
use Amasty\Shopby\Helper\FilterSetting;
use Magento\Framework\App\Config\ScopeConfigInterface;
use Kurufootwear\Shopby\Helper\AttributeHelper;
use Amasty\Shopby\Api\Data\FilterSettingInterface;
use Magento\Search\Model\SearchEngine;

class Attribute extends \Amasty\Shopby\Model\Layer\Filter\Attribute
{
    /**
     * @var GroupHelper
     */
    protected $groupHelper;

    /**
     * @var AttributeHelper
     */
    private $attributeHelper;

    /**
     * @var Request
     */
    private $shopbyRequest;

    /**
     * @var FilterSettingInterface
     */
    private $filterSetting;

    /**
     * @var  FilterSetting
     */
    private $settingHelper;

    /**
     * Attribute constructor.
     * @param FilterItemFactory $filterItemFactory
     * @param StoreManagerInterface $storeManager
     * @param Layer $layer
     * @param ItemDataBuilder $itemDataBuilder
     * @param StripTags $tagFilter
     * @param MysqlAggregationAdapter $aggregationAdapter
     * @param SearchEngine $searchEngine
     * @param FilterSetting $settingHelper
     * @param ScopeConfigInterface $scopeConfig
     * @param Request $shopbyRequest
     * @param GroupHelper $groupHelper
     * @param OptionSetting $optionSettingHelper
     * @param AttributeHelper $attributeHelper
     * @param array $data
     */
    public function __construct(
        FilterItemFactory $filterItemFactory,
        StoreManagerInterface $storeManager,
        Layer $layer,
        ItemDataBuilder $itemDataBuilder,
        TagFilter $tagFilter,
        MysqlAggregationAdapter $aggregationAdapter,
        SearchEngine $searchEngine,
        FilterSetting $settingHelper,
        ScopeConfigInterface $scopeConfig,
        Request $shopbyRequest,
        GroupHelper $groupHelper,
        OptionSetting $optionSettingHelper,
        AttributeHelper $attributeHelper,
        array $data = []
    )
    {
        parent::__construct(
            $filterItemFactory,
            $storeManager,
            $layer,
            $itemDataBuilder,
            $tagFilter,
            $aggregationAdapter,
            $searchEngine,
            $settingHelper,
            $scopeConfig,
            $shopbyRequest,
            $groupHelper,
            $optionSettingHelper,
            $data
        );

        $this->attributeHelper = $attributeHelper;
        $this->shopbyRequest = $shopbyRequest;
        $this->settingHelper = $settingHelper;
    }

    /**
     * Apply attribute option filter to product collection.
     *
     * @param RequestInterface $request
     * @return $this
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function apply(RequestInterface $request)
    {
        if ($this->isApplied()) {
            return $this;
        }

        $requestedOptionsString = $this->shopbyRequest->getFilterParam($this);

        if (empty($requestedOptionsString)) {
            return $this;
        }

        $requestedOptions = explode(',', $requestedOptionsString);

        $this->updateAttributeIds($requestedOptions);

        if (count($requestedOptions) == 0) {
            // If the options are faulty don't apply filters
            return $this;
        }

        $this->setCurrentValue($requestedOptions);
        $this->addState($requestedOptions);

        if (!$this->isMultiselectAllowed() && count($requestedOptions) > 1) {
            $requestedOptions = array_slice($requestedOptions, 0, 1);
        }

        $attribute = $this->getAttributeModel();
        $id = $attribute->getAttributeId();
        $optionGroups = $id ? $this->groupHelper->getGroupsWithOptions($id) : [];

        /** @var \Amasty\Shopby\Model\ResourceModel\Fulltext\Collection $productCollection */
        $productCollection = $this->getLayer()->getProductCollection();

        // BENEFITS will be processed as a usual attribute with its custom strategy
        if ($this->getFilterSetting()->isUseAndLogic() && !$this->isAttributeBenefits($attribute)) {
            foreach ($requestedOptions as $key => $value) {
                $optionsFromGroup = $this->groupHelper->getGroup($optionGroups, $value);
                $value = $optionsFromGroup ?: $value;

                $fakeAttributeCode = $this->getFakeAttributeCodeForApply($attribute->getAttributeCode(), $key);
                $productCollection->addFieldToFilter($fakeAttributeCode, $value);
            }
        } else {
            $optionValues = $requestedOptions;
            foreach ($optionValues as $key => $value) {
                $optionsFromGroup = $this->groupHelper->getGroup($optionGroups, $value);
                if ($optionsFromGroup) {
                    unset($optionValues[$key]);
                    $optionValues = array_merge($optionValues, $optionsFromGroup);
                }
            }
            $productCollection->addFieldToFilter($attribute->getAttributeCode(), $optionValues);
        }

        return $this;
    }

    /**
     * @param array $values
     */
    private function addState(array $values)
    {
        if (!$this->shouldAddState()) {
            return;
        }

        $labels = [];

        $attribute = $this->getAttributeModel();

        foreach ($values as $value) {
            $labelGroup = null;
            if ($id = $attribute->getAttributeId()) {
                $labelGroup = $this->groupHelper->getGroupLabel($id, $value);
            }
            if ($labelGroup) {
                $labels[] = $labelGroup;
            } else {
                $labels[] = $this->getOptionText($value);
            }
        }

        $this->getLayer()->getState()
            ->addFilter(
                $this->_createItem(implode(', ', $labels), $values)
            );
    }

    /**
     * @param string $attributeCode
     * @param $key
     * @return string
     */
    private function getFakeAttributeCodeForApply($attributeCode, $key)
    {
        if ($key > 0) {
            $attributeCode .= ShopbyRequestGenerator::FAKE_SUFFIX . $key;
        }

        return $attributeCode;
    }

    /**
     * @return bool
     */
    private function isMultiselectAllowed()
    {
        return $this->getFilterSetting()->isMultiselect();
    }

    /**
     * @return FilterSettingInterface
     */
    private function getFilterSetting()
    {
        if ($this->filterSetting === null) {
            $this->filterSetting = $this->settingHelper->getSettingByLayerFilter($this);
        }

        return $this->filterSetting;
    }

    /**
     * Return if attribute is benefits
     *
     * @param \Magento\Catalog\Model\ResourceModel\Eav\Attribute $attribute
     * @return bool
     */
    private function isAttributeBenefits($attribute)
    {
        return $attribute->getAttributeCode() === Benefits::BENEFITS_CODE;
    }

    /**
     * Part that translates from Labels to Ids for filters
     *
     * @param array $values
     */
    public function updateAttributeIds(array &$values)
    {
        if (is_array($values) && $this->getRequestVar() !== 'cat') {
            foreach ($values as $index => &$valueItem) {
                $attribute = $this->getRequestVar();
                $optionId = $this->attributeHelper->getAttributeId($valueItem, $attribute);

                if ($optionId) {
                    $valueItem = $optionId;
                } else {
                    unset($values[$index]);
                }
            }
        }
    }
}
