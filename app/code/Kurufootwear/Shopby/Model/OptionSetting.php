<?php
/**
 * @vendor Kurufootwear
 * @module Kurufootwear_Shopby
 * @author Krisjanis Veinbahs <info@scandiweb.com>
 * @copyright Copyright (c) 2018 Scandiweb, Ltd (http://scandiweb.com)
 * @license http://opensource.org/licenses/afl-3.0.php Academic Free License (AFL 3.0)
 */
namespace Kurufootwear\Shopby\Model;

use Amasty\Shopby\Model\OptionSetting as AmastyOptionSetting;
use Magento\Framework\App\Filesystem\DirectoryList;

class OptionSetting extends AmastyOptionSetting
{
    /**
     * Override of Amasty attribute setting image upload function.
     * Now also include svg's as allowed extensions.
     *
     * @param int $fileId
     * @param bool $isSlider
     * @return string
     */
    public function uploadImage($fileId, $isSlider = false)
    {
        $mediaDir = $this->fileSystem->getDirectoryWrite(DirectoryList::MEDIA);
        $uploader = $this->uploaderFactory->create(['fileId' => $fileId]);
        $uploader->setFilesDispersion(false);
        $uploader->setFilenamesCaseSensitivity(false);
        $uploader->setAllowRenameFiles(true);
        $uploader->setAllowedExtensions(['jpg', 'png', 'jpeg', 'gif', 'bmp', 'svg']);
        $path = $isSlider ? self::IMAGES_DIR . self::SLIDER_DIR : self::IMAGES_DIR;
        $uploader->save($mediaDir->getAbsolutePath($path));
        $result = $uploader->getUploadedFileName();
        $this->removeImage($isSlider);
        return $result;
    }
}
