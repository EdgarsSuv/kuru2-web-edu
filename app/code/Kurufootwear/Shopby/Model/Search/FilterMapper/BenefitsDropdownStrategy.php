<?php
/**
 * @vendor Kurufootwear
 * @module Kurufootwear_Shopby
 * @author austris <info@scandiweb.com>
 * @copyright Copyright (c) 2017 Scandiweb, Ltd (http://scandiweb.com)
 * @license http://opensource.org/licenses/afl-3.0.php Academic Free License (AFL 3.0)
 */

namespace Kurufootwear\Shopby\Model\Search\FilterMapper;

use Magento\CatalogSearch\Model\Adapter\Mysql\Filter\AliasResolver;
use Magento\CatalogSearch\Model\Search\FilterMapper\FilterStrategyInterface;
use Magento\Framework\DB\Select;
use Magento\Framework\Search\Request\FilterInterface;
use Magento\Store\Model\StoreManagerInterface;
use Magento\Eav\Model\Config as EavConfig;
use Magento\Framework\App\Config\ScopeConfigInterface;
use Magento\Framework\App\ResourceConnection;
use Magento\Store\Model\ScopeInterface;

class BenefitsDropdownStrategy implements FilterStrategyInterface
{
    /**
     * Resolving table alias for Search Request filter.
     *
     * @var AliasResolver
     */
    private $aliasResolver;

    /**
     * Store manager.
     *
     * @var StoreManagerInterface
     */
    private $storeManager;

    /**
     * Eav attributes config.
     *
     * @var EavConfig
     */
    private $eavConfig;

    /**
     * Resource connection.
     *
     * @var ResourceConnection
     */
    private $resourceConnection;

    /**
     * Scope config.
     *
     * @var ScopeConfigInterface
     */
    private $scopeConfig;

    /**
     * @param StoreManagerInterface $storeManager
     * @param ResourceConnection $resourceConnection
     * @param EavConfig $eavConfig
     * @param ScopeConfigInterface $scopeConfig
     * @param AliasResolver $aliasResolver
     */
    public function __construct(
        StoreManagerInterface $storeManager,
        ResourceConnection $resourceConnection,
        EavConfig $eavConfig,
        ScopeConfigInterface $scopeConfig,
        AliasResolver $aliasResolver
    ) {
        $this->storeManager = $storeManager;
        $this->resourceConnection = $resourceConnection;
        $this->eavConfig = $eavConfig;
        $this->scopeConfig = $scopeConfig;
        $this->aliasResolver = $aliasResolver;
    }

    /**
     * In case of benefits - group the end result by entity ID, to avoid multiple records per ID
     * And (HAVING) filter them by count of selected filters so that match has all values
     *
     * @param FilterInterface $filter
     * @param Select $select
     *
     * @return bool is filter was applied
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function apply(
        FilterInterface $filter,
        Select $select
    ) {
        $alias = $this->aliasResolver->getAlias($filter);
        $attribute = $this->getAttributeByCode($filter->getField());
        $joinCondition = sprintf(
            'search_index.entity_id = %1$s.entity_id AND %1$s.attribute_id = %2$d AND %1$s.store_id = %3$d',
            $alias,
            $attribute->getId(),
            $this->storeManager->getWebsite()->getId()
        );

        $select->joinLeft(
            [$alias => $this->resourceConnection->getTableName('catalog_product_index_eav')],
            $joinCondition,
            []
        );

        if ($this->isAddStockFilter()) {
            $stockAlias = $alias . AliasResolver::STOCK_FILTER_SUFFIX;
            $select->joinLeft(
                [$stockAlias => $this->resourceConnection->getTableName('cataloginventory_stock_status')],
                sprintf('%2$s.product_id = %1$s.source_id', $alias, $stockAlias),
                []
            );
        }

        if ($this->canGroup($filter)) {
            $havingCondition = sprintf('count(DISTINCT %1$s.value) = %2$d',
                $alias,
                count($filter->getValue())
            );

            $select->group('search_index.entity_id')
                ->having($havingCondition);
        }

        return true;
    }


    /**
     * Returns attribute by attribute code.
     *
     * @param string $field
     *
     * @return \Magento\Catalog\Model\ResourceModel\Eav\Attribute
     *
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    private function getAttributeByCode($field)
    {
        return $this->eavConfig->getAttribute(\Magento\Catalog\Model\Product::ENTITY, $field);
    }

    /**
     * Check if it is necessary to show out of stock products.
     *
     * @return bool
     */
    private function isAddStockFilter()
    {
        $isShowOutOfStock = $this->scopeConfig->isSetFlag(
            'cataloginventory/options/show_out_of_stock',
            ScopeInterface::SCOPE_STORE
        );

        return false === $isShowOutOfStock;
    }

    /**
     * We will only group when there are more than one value selected
     *
     * @param $filter
     * @return bool
     */
    private function canGroup($filter)
    {
        $values = $filter->getValue();
        return is_array($values) && count($values) > 1;
    }
}