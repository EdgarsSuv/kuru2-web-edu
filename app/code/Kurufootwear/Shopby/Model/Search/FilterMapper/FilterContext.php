<?php
/**
 * @vendor Kurufootwear
 * @module Kurufootwear_Shopby
 * @author austris <info@scandiweb.com>
 * @copyright Copyright (c) 2017 Scandiweb, Ltd (http://scandiweb.com)
 * @license http://opensource.org/licenses/afl-3.0.php Academic Free License (AFL 3.0)
 */

namespace Kurufootwear\Shopby\Model\Search\FilterMapper;

use Kurufootwear\Catalog\Block\Benefits;
use Magento\CatalogSearch\Model\Adapter\Mysql\Filter\AliasResolver;
use Magento\CatalogSearch\Model\Search\FilterMapper\ExclusionStrategy;
use Magento\CatalogSearch\Model\Search\FilterMapper\FilterStrategyInterface;
use Magento\CatalogSearch\Model\Search\FilterMapper\StaticAttributeStrategy;
use Magento\CatalogSearch\Model\Search\FilterMapper\TermDropdownStrategy;
use Magento\Eav\Model\Config as EavConfig;
use Magento\Eav\Model\Entity\Attribute\AbstractAttribute;

/**
 * FilterContext represents a Context of the Strategy pattern.
 * Its responsibility is to choose appropriate strategy to apply passed filter to the Select.
 */
class FilterContext implements FilterStrategyInterface
{
    /**
     * Strategy which processes exclusions from general rules.
     *
     * @var ExclusionStrategy
     */
    private $exclusionStrategy;

    /**
     * Eav attributes config.
     *
     * @var EavConfig
     */
    private $eavConfig;

    /**
     * Strategy for handling dropdown or multi-select attributes.
     *
     * @var TermDropdownStrategy
     */
    private $termDropdownStrategy;

    /**
     * Strategy for handling static attributes.
     *
     * @var StaticAttributeStrategy
     */
    private $staticAttributeStrategy;

    /**
     * Resolving table alias for Search Request filter.
     *
     * @var AliasResolver
     */
    private $aliasResolver;

    /**
     * @var BenefitsDropdownStrategy
     */
    private $benefitsDropdownStrategy;

    /**
     * @param EavConfig $eavConfig
     * @param AliasResolver $aliasResolver
     * @param ExclusionStrategy $exclusionStrategy
     * @param TermDropdownStrategy $termDropdownStrategy
     * @param StaticAttributeStrategy $staticAttributeStrategy
     * @param BenefitsDropdownStrategy $benefitsDropdownStrategy
     */
    public function __construct(
        EavConfig $eavConfig,
        AliasResolver $aliasResolver,
        ExclusionStrategy $exclusionStrategy,
        TermDropdownStrategy $termDropdownStrategy,
        StaticAttributeStrategy $staticAttributeStrategy,
        BenefitsDropdownStrategy $benefitsDropdownStrategy
    ) {
        $this->eavConfig = $eavConfig;
        $this->aliasResolver = $aliasResolver;
        $this->exclusionStrategy = $exclusionStrategy;
        $this->termDropdownStrategy = $termDropdownStrategy;
        $this->staticAttributeStrategy = $staticAttributeStrategy;
        $this->benefitsDropdownStrategy = $benefitsDropdownStrategy;
    }

    /**
     * {@inheritDoc}
     */
    public function apply(
        \Magento\Framework\Search\Request\FilterInterface $filter,
        \Magento\Framework\DB\Select $select
    ) {
        $isApplied = $this->exclusionStrategy->apply($filter, $select);

        if (!$isApplied) {
            $attribute = $this->getAttributeByCode($filter->getField());

            if ($attribute) {
                if ($filter->getType() === \Magento\Framework\Search\Request\FilterInterface::TYPE_TERM
                    && $attribute->getAttributeCode() === Benefits::BENEFITS_CODE
                ) {
                    $isApplied = $this->benefitsDropdownStrategy->apply($filter, $select);
                } else if ($filter->getType() === \Magento\Framework\Search\Request\FilterInterface::TYPE_TERM
                    && in_array($attribute->getFrontendInput(), ['select', 'multiselect'], true)
                ) {
                    $isApplied = $this->termDropdownStrategy->apply($filter, $select);
                } elseif ($attribute->getBackendType() === AbstractAttribute::TYPE_STATIC) {
                    $isApplied = $this->staticAttributeStrategy->apply($filter, $select);
                }
            }
        }

        return $isApplied;
    }

    /**
     * Returns attribute by attribute_code.
     *
     * @param string $field
     *
     * @return \Magento\Catalog\Model\ResourceModel\Eav\Attribute
     *
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    private function getAttributeByCode($field)
    {
        return $this->eavConfig->getAttribute(\Magento\Catalog\Model\Product::ENTITY, $field);
    }
}
