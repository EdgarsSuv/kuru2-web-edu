/**
 * @vendor Kurufootwear
 * @module Kurufootwear_Shopby
 * @author Kristaps Stalidzāns <info@scandiweb.com>
 * @copyright Copyright (c) 2017 Scandiweb, Ltd (http://scandiweb.com)
 * @license http://opensource.org/licenses/afl-3.0.php Academic Free License (AFL 3.0)
 */

require([
    'jquery',
    'jquery/ui',
    'accordion'
], function($){
    'use strict';

    /**
     * Initialize accordion and
     * check it's state on resizes
     * @param $layeredBlock
     */
    function initAccordion() {
        var $layeredBlock = $('#layered-filter-block');
        var $filterOptions = $('.filter-options');

        if ($(window).width() >= 1024) {
            setTimeout(function() {
                $filterOptions.accordion().accordion('activate');
            }, 10);
        }
    }

    function bindResize() {
        var width = $(window).width();

        $(window).on('resize', function() {
            var $filterOptions = $('.filter-options');
            var $layeredBlock = $('#layered-filter-block');

            if ((width < 1024) && ($(window).width() >= 1024)) {
                $filterOptions.accordion().accordion('activate');
                $('.filter-options-item').collapsible('activate');
            } else if ((width >= 1024) && ($(window).width() < 1024)) {
                $layeredBlock.accordion().accordion('deactivate');
                $filterOptions.accordion().accordion('deactivate');
            }

            width = $(window).width();
        });
    }

    /**
     * Initialize accordion widget initially
     * and after AJAX calls
     */
    $(document).ready(function() {
        bindResize();
        initAccordion();

        $(document).arrive('.filter-options', function(e) {
            initAccordion();
        });
    });
});
