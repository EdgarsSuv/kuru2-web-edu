<?php
/**
 * @vendor Kurufootwear
 * @module Kurufootwear_Shopby
 * @author Kristaps Stalidzāns <info@scandiweb.com>
 * @copyright Copyright (c) 2017 Scandiweb, Ltd (http://scandiweb.com)
 * @license http://opensource.org/licenses/afl-3.0.php Academic Free License (AFL 3.0)
 */

namespace Kurufootwear\Shopby\Helper;

use Magento\Framework\App\Helper\Context;
use Magento\Framework\App\Helper\AbstractHelper;
use Detection\MobileDetect;

/**
 * Helper to be used for mobile detect and validations
 *
 * Class Validations
 * @package WM\Catalalog\Helper
 */
class Detect extends AbstractHelper
{

    const MOBILE = 'is_mobile';
    const TABLET = 'is_tablet';
    const DESKTOP = 'is_desktop';

    /**
     * @var MobileDetect
     */
    private $mobileDetect;

    /**
     * @var bool
     */
    private $detected = false;

    /**
     * Detect constructor.
     * @param Context $context
     */
    public function __construct(
        Context $context
    )
    {
        $this->mobileDetect = new MobileDetect();
        parent::__construct($context);
    }

    /**
     * @return bool
     */
    public function getDetected()
    {
        return $this->detected;
    }

    /**
     * If is mobile device
     * @return bool
     */
    public function isMobile()
    {
        $this->detected = self::MOBILE;

        return $this->mobileDetect->isMobile();
    }

    /**
     * If is a tablet
     * @return bool
     */
    public function isTablet()
    {
        $this->detected = self::TABLET;

        return $this->mobileDetect->isTablet();
    }

    /**
     * If is desktop device
     * @return bool
     */
    public function isDesktop()
    {
        if ($this->isMobile()) {

            return false;
        }

        $this->detected = self::DESKTOP;

        return true;
    }

    /**
     * The mobile detect instance to be able to use all the functionality
     * @return MobileDetect
     */
    public function getMobileDetect()
    {
        return $this->mobileDetect;
    }

}
