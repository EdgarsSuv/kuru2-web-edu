<?php
/**
 * @vendor Kurufootwear
 * @module Kurufootwear_Shopby
 * @author Kristaps Stalidzāns <info@scandiweb.com>
 * @copyright Copyright (c) 2017 Scandiweb, Ltd (http://scandiweb.com)
 * @license http://opensource.org/licenses/afl-3.0.php Academic Free License (AFL 3.0)
 */

namespace Kurufootwear\Shopby\Helper;

use Amasty\Shopby;
use Amasty\Shopby\Helper\FilterSetting;
use Magento\Catalog\Model\Layer;
use Magento\Framework\App\Helper\Context;
use Magento\Framework\Registry;
use Magento\Store\Model\StoreManagerInterface;

class Data extends \Amasty\Shopby\Helper\Data
{
    /**
     * @var AttributeHelper
     */
    private $attributeHelper;

    /**
     * Data constructor.
     * @param Context $context
     * @param FilterSetting $settingHelper
     * @param Layer\Resolver $layerResolver
     * @param StoreManagerInterface $storeManager
     * @param Shopby\Model\Request $shopbyRequest
     * @param Shopby\Model\Layer\FilterList $filterList
     * @param Registry $registry
     * @param AttributeHelper $attributeHelper
     */
    public function __construct(
        Context $context,
        FilterSetting $settingHelper,
        Layer\Resolver $layerResolver,
        StoreManagerInterface $storeManager,
        Shopby\Model\Request $shopbyRequest,
        Shopby\Model\Layer\FilterList $filterList,
        Registry $registry,
        AttributeHelper $attributeHelper
    )
    {
        $this->attributeHelper = $attributeHelper;

        parent::__construct(
            $context,
            $settingHelper,
            $layerResolver,
            $storeManager,
            $shopbyRequest,
            $filterList,
            $registry
        );
    }

    /**
     * @param Shopby\Model\Layer\Filter\Item $filterItem
     * @return int
     */
    public function isFilterItemSelected(Shopby\Model\Layer\Filter\Item $filterItem)
    {
        $data = $this->shopbyRequest->getFilterParam($filterItem->getFilter());

        if (!empty($data)) {
            $ids = explode(',', $data);

            if (in_array($filterItem->getLabel(), $ids)) {
                return 1;
            }
        }

        return 0;
    }
}
