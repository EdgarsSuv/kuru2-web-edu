<?php
/**
 * @vendor Kurufootwear
 * @module Kurufootwear_Shopby
 * @author Kristaps Stalidzāns <info@scandiweb.com>
 * @copyright Copyright (c) 2017 Scandiweb, Ltd (http://scandiweb.com)
 * @license http://opensource.org/licenses/afl-3.0.php Academic Free License (AFL 3.0)
 */

namespace Kurufootwear\Shopby\Helper;

use Amasty\Shopby\Model\Layer\Filter\Price;
use Amasty\Shopby\Model\Layer\Filter\Category as CategoryFilter;
use Magento\Catalog\Api\CategoryRepositoryInterface;
use Magento\Catalog\Model\Layer\Filter\FilterInterface;
use Magento\Framework\App\Helper\Context;
use Magento\Framework\Registry;
use Amasty\Shopby\Helper\FilterSetting;
use Magento\Framework\Url\QueryParamsResolverInterface;
use Amasty\Shopby\Helper\Category;

class UrlBuilder extends \Amasty\Shopby\Helper\UrlBuilder
{
    /**
     * @var AttributeHelper
     */
    private $attributeHelper;

    /**
     * @var CategoryRepositoryInterface
     */
    private $categoryRepository;

    /**
     * @var
     */
    private $filter;

    /**
     * @var FilterSetting
     */
    private $filterSettingHelper;

    /**
     * @var Category
     */
    private $categoryHelper;

    /**
     * UrlBuilder constructor.
     * @param Context $context
     * @param Registry $registry
     * @param CategoryRepositoryInterface $categoryRepository
     * @param FilterSetting $filterSettingHelper
     * @param QueryParamsResolverInterface $queryParamsResolver
     * @param Category $categoryHelper
     * @param AttributeHelper $attributeHelper
     */
    public function __construct(
        Context $context,
        Registry $registry,
        CategoryRepositoryInterface $categoryRepository,
        FilterSetting $filterSettingHelper,
        QueryParamsResolverInterface $queryParamsResolver,
        Category $categoryHelper,
        AttributeHelper $attributeHelper
    )
    {
        $this->attributeHelper = $attributeHelper;
        $this->filterSettingHelper = $filterSettingHelper;
        $this->categoryHelper = $categoryHelper;

        parent::__construct(
            $context,
            $registry,
            $categoryRepository,
            $filterSettingHelper,
            $queryParamsResolver,
            $categoryHelper
        );
    }

    /**
     * @param FilterInterface $filter
     * @param string|array $optionValue
     * @return string
     */
    public function buildUrl(FilterInterface $filter, $optionValue)
    {
        $this->filter = $filter;

        if ($filter instanceof CategoryFilter && !$filter->isMultiselect()) {
            $category = $this->categoryRepository->get($optionValue);
            if ($category) {
                return $category->getUrl();
            }
        }

        $routePath = '*/*/*';

        if ($filter instanceof Price && is_array($optionValue)) {
            $optionValue = implode('-', $optionValue);
        }

        $currentValues = $this->getCurrentValues();
        $resultValue = $this->calculateResultValue($optionValue, $currentValues);

        $query = $this->buildQuery($filter, $resultValue);
        $this->updateQuery($query, $filter->getRequestVar(), $optionValue);

        $query['shopbyAjax'] = null;
        $query['_'] = null;
        $params = ['_current' => true, '_use_rewrite' => true, '_query' => $query];
        //fix urls like catalogsearch/result/index/price/10-20/?price=10-60&q=bag
        $params['price'] = null;
        $url = $this->_urlBuilder->getUrl($routePath, $params);

        return $this->cleanUpUrl($url);
    }

    /**
     * @return array
     */
    protected function getCurrentValues()
    {
        $data = $this->_request->getParam($this->filter->getRequestVar());

        if (!empty($data)) {
            $values = explode(',', $data);
            foreach ($values as $key => $val) {
                if (empty($val)) {
                    unset($values[$key]);
                }
            }
        } else {
            $values = [];
        }

        return $values;
    }

    /**
     * Remove empty query tags from url
     * and remove question mark if no tags
     * available
     * @param $url
     * @return mixed
     */
    public function cleanUpUrl($url)
    {
        $step1 = preg_replace('/(&?\w+=((?=$)|(?=&)))/', '', $url);
        $step2 = preg_replace('/\?$/', '', $step1);
        return $step2;
    }

    /**
     * @param $optionValue
     * @param array $currentValues
     * @return string|null
     */
    protected function calculateResultValue($optionValue, array $currentValues)
    {
        if ($optionValue === null || is_array($optionValue)) {
            return null;
        }
        $key = array_search($optionValue, $currentValues);

        if ($this->isMultiselectAllowed()) {
            $result = $currentValues;
            if ($key !== false) {
                unset($result[$key]);
            } else {
                if ($this->filter instanceof CategoryFilter && $this->categoryHelper->isCategoryFilterExtended()) {
                    $parents = $this->filter->getItems()->getParentsAndChildrenByItemId($optionValue);
                    $result = array_diff($result, $parents);
                }
                $result[] = $optionValue;
            }
        } else {
            if ($key !== false) {
                $result = [];
            } else {
                $result = [$optionValue];
            }
        }

        $value = $result ? implode(',', $result) : null;
        return $value;
    }

    /**
     * @return bool
     */
    protected function isMultiselectAllowed()
    {
        $setting = $this->filterSettingHelper->getSettingByLayerFilter($this->filter);

        return $setting->isMultiselect();
    }

    /**
     * Updates Query with attribute names
     *
     * @param array $query
     * @param $updatedAttribute
     * @param $updatedOption
     */
    public function updateQuery(array &$query, $updatedAttribute, $updatedOption)
    {
        foreach ($query as $attribute => &$option) {
            if ($attribute !== 'cat') {
                $arrayData = explode(',', $option);

                if (is_array($arrayData)) {
                    /**
                     * Transform attribute id to label
                     */
                    foreach ($arrayData as &$item) {
                        if ($updatedAttribute == $attribute &&  $item == $updatedOption) {
                            $item = $this->attributeHelper->getAttributeLabel($item, $attribute);
                        }
                    }

                    /**
                     * Remove duplicates
                     */
                    foreach ($arrayData as $key => &$item) {
                        $itemIndexes = array_keys($arrayData, $item);
                        $instances = count(array_keys($itemIndexes));

                        if ($instances > 1) {
                            foreach ($itemIndexes as $instanceNo => $index) {
                                unset($arrayData[$index]);
                            }
                        }
                    }

                    /**
                     * Implode back
                     */
                    $queryData = implode(',', $arrayData);
                    $option = $queryData;
                }
            }
        }
    }
}
