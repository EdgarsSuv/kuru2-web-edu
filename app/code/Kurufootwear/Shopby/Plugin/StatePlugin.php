<?php
/**
 * @category    Jysk
 * @package     Jysk\Shopby
 * @author      Andrejs Kovtuns <info@scandiweb.com>
 * @copyright   Copyright (c) 2017 Scandiweb, Inc (https://scandiweb.com)
 * @license     http://opensource.org/licenses/OSL-3.0 The Open Software License 3.0 (OSL-3.0)
 */

namespace Kurufootwear\Shopby\Plugin;

use Closure;
use Magento\LayeredNavigation\Block\Navigation\State;

class StatePlugin
{
    /**
     * Removes price filter
     *
     * @param State $subject
     * @param Closure $closure
     *
     * @return string
     */
    public function aroundGetClearUrl(State $subject, Closure $closure)
    {
        $filterState = [];
        $filterState['isAjax'] = null;
        $filterState['_'] = null;

        foreach ($subject->getActiveFilters() as $item) {
            $filterState[$item->getFilter()->getRequestVar()] = $item->getFilter()->getCleanValue();
        }
        $params['_current'] = true;
        $params['_use_rewrite'] = true;
        $params['_query'] = $filterState;
        $params['_escape'] = true;
        $params['price'] = null;

        return $subject->getUrl('*/*/*', $params);
    }
}
