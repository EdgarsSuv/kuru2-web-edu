<?php
/**
 * @vendor Kurufootwear
 * @module Kurufootwear_Shopby
 * @author Kristaps Stalidzāns <info@scandiweb.com>
 * @copyright Copyright (c) 2017 Scandiweb, Ltd (http://scandiweb.com)
 * @license http://opensource.org/licenses/afl-3.0.php Academic Free License (AFL 3.0)
 */
namespace Kurufootwear\Shopby\Plugin;

use Amasty\Shopby\Model\Layer\FilterList;
use Magento\Catalog\Model\Layer\Resolver;
use Magento\Framework\App\Request\Http;
use Magento\Framework\UrlInterface;
use Magento\Framework\View\Element\Template;

class ProductListWrapper extends \Amasty\Shopby\Plugin\Ajax\ProductListWrapper
{
    /**
     * @var Http
     */
    protected $request;

    /**
     * @var UrlInterface
     */
    private $urlBuilder;

    /**
     * ProductListWrapper constructor.
     * @param Http $request
     * @param FilterList $filterListTop
     * @param Resolver $layerResolver
     * @param UrlInterface $urlBuilder
     */
    public function __construct(
        Http $request,
        FilterList $filterListTop,
        Resolver $layerResolver,
        UrlInterface $urlBuilder
    ){
        parent::__construct(
            $request,
            $filterListTop,
            $layerResolver
        );

        $this->urlBuilder = $urlBuilder;
        $this->request = $request;
    }

    /**
     * @param Template $subject
     * @param $result
     * @return string
     */
    public function afterToHtml(Template $subject, $result)
    {
        if ($subject->getNameInLayout() !== 'category.products.list'
            && $subject->getNameInLayout() !== 'search_result_list') {
            return $result;
        }

        if ($this->request->getParam('is_scroll')) {
            return $result;
        }

        $spinnerUrl = $this->urlBuilder->getBaseUrl(['_type' => UrlInterface::URL_TYPE_MEDIA]) . 'wysiwyg/loader.svg';
        $loader = '<img src="' . $spinnerUrl . '"
                 alt="' . __('Loading...') . '" style="height: 80px;width: 80px;position:fixed;top: 50%;left: 50%;margin-top: -50px;margin-left: -100px;" class="loader-spinner">';
        $style  = '
        style="
            background-color: #FFFFFF;
            height: 100%;
            left: 0;
            opacity: 0.5;
            filter: alpha(opacity = 50);
            position: absolute;
            top: 0;
            width: 100%;
            z-index: 555;
            display: none;
        "
        ';

        return '<div id="amasty-shopby-product-list" class="amasty-shopby-product-list">'.$result.'<div id="amasty-shopby-overlay" '.$style.'>' . $loader . '</div></div>';
    }
}
