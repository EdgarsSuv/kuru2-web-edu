<?php
/**
 * @vendor Kurufootwear
 * @module Kurufootwear_CacheWarmer
 * @author Aleksejs Baranovs <info@scandiweb.com>
 * @copyright Copyright (c) 2018 KURU Footwear. All rights reserved.
 */

namespace Kurufootwear\CacheWarmer\Helper;

use Exception;
use Kurufootwear\CacheWarmer\Model\ResourceModel\CacheWarmer\CollectionFactory;
use Kurufootwear\CacheWarmer\Model\ResourceModel\CacheWarmer as CacheWarmerResource;
use Magento\Framework\App\Helper\AbstractHelper;
use Magento\Framework\App\Helper\Context;
use Magento\Framework\App\ResourceConnection;
use Magento\Framework\Exception\AlreadyExistsException;
use Magento\Framework\HTTP\Client\Curl;
use Psr\Log\LoggerInterface;

/**
 * Class Warmer
 * @package Kurufootwear\CacheWarmer\Helper
 */
class Warmer extends AbstractHelper
{
    /**
     * cURL request timeout in msec
     */
    const CURL_TIMEOUT = 1000;

    /**
     * Cache warming progress flag
     */
    const CACHE_WARMING_PROGRESS_FLAG = 'cache/warming/inprogress';

    /**
     * Core config data table
     */
    const CORE_CONFIG_DATA_TABLE = 'core_config_data';

    /**
     * Flag to check if cache warmer is enabled
     */
    const CACHE_WARMER_ENABLE = 'cache_warmer_config/cache_warmer/enable';

    /**
     * Flag to check if cache warmer import / update cron is enabled
     */
    const CACHE_WARMER_CRON = 'cache_warmer_config/cache_warmer/cache_warmer_cron';

    /**
     * Flag to check if HTTP auth is enabled
     */
    const CACHE_WARMER_ENABLE_HTTP_AUTH = 'cache_warmer_config/cache_warmer/http_auth';

    /**
     * HTTP auth username
     */
    const CACHE_WARMER_ENABLE_HTTP_USERNAME = 'cache_warmer_config/cache_warmer/http_username';

    /**
     * HTTP auth password
     */
    const CACHE_WARMER_ENABLE_HTTP_PASSWORD = 'cache_warmer_config/cache_warmer/http_password';

    /**
     * @var CollectionFactory
     */
    private $warmerCollectionFactory;

    /**
     * @var LoggerInterface
     */
    private $logger;

    /**
     * @var Curl
     */
    private $curlClient;

    /**
     * @var ResourceConnection
     */
    private $resourceConnection;

    /**
     * @var CacheWarmerResource
     */
    private $cacheWarmerResource;

    /**
     * Warmer constructor.
     *
     * @param Context $context
     * @param CollectionFactory $warmerCollectionFactory
     * @param CacheWarmerResource $cacheWarmerResource
     * @param LoggerInterface $logger
     * @param ResourceConnection $resourceConnection
     * @param Curl $curlClient
     */
    public function __construct(
        Context $context,
        CollectionFactory $warmerCollectionFactory,
        CacheWarmerResource $cacheWarmerResource,
        LoggerInterface $logger,
        ResourceConnection $resourceConnection,
        Curl $curlClient
    ) {
        parent::__construct($context);

        $this->warmerCollectionFactory = $warmerCollectionFactory;
        $this->logger = $logger;
        $this->curlClient = $curlClient;
        $this->resourceConnection = $resourceConnection;
        $this->cacheWarmerResource = $cacheWarmerResource;
    }

    /**
     * Send cURL request to each page, that should be warmed up
     *
     * @param bool $cli
     * @param array $idsToFilter
     *
     * @throws AlreadyExistsException
     */
    public function warmCache($cli = false, $idsToFilter = [])
    {
        $collection = $this->warmerCollectionFactory->create();

        if (!empty($idsToFilter)) {
            $collection->addIdsToFilter($idsToFilter);
        }

        if ($this->getCacheWarmingFlag()) {
            throw new AlreadyExistsException(__('Cache Warmer is running in background, please wait...'));
        }

        $this->setCacheWarmingFlag(1);
        $this->cacheWarmerResource->setRunningFlagForAll();

        foreach ($collection as $item) {
            if (!$item->getPageUrl() || !$item->getWarmUp()) {
                continue;
            }

            $this->proceedCurlRequest($item->getPageUrl(), $cli);
            $this->cacheWarmerResource->setCompleteFlagByEntityId($item->getEntityId());
        }

        $this->setCacheWarmingFlag(0);
    }

    /**
     * Proceed CURL request
     *
     * @param $url
     * @param bool $cli
     */
    public function proceedCurlRequest($url, $cli = false)
    {
        try {
            $credentials = $this->getHttpAuth();

            if ($credentials) {
                $this->curlClient->setCredentials($credentials['username'], $credentials['password']);
            }

            $this->curlClient->setTimeout(self::CURL_TIMEOUT);
            $this->curlClient->get($url);

            if ($cli) {
                pcntl_signal_dispatch();
                echo sprintf('--Finished url: %s %s', $url, PHP_EOL);
            }
        } catch (Exception $e) {
            $this->logger->critical("Cache warmer failed for the URL '{$url}'. Error message:" . $e->getMessage());
        }
    }

    /**
     * Check if cache warmer is enabled
     *
     * @return int
     */
    public function isEnabled()
    {
        return $this->scopeConfig->getValue(self::CACHE_WARMER_ENABLE);
    }

    /**
     * Check if cache warmer import/ update cron is enabled
     *
     * @return int
     */
    public function isCronEnabled()
    {
        return $this->scopeConfig->getValue(self::CACHE_WARMER_CRON);
    }

    /**
     * Get HTTP auth data
     *
     * @return array|bool
     */
    private function getHttpAuth()
    {
        if (!$this->scopeConfig->getValue(self::CACHE_WARMER_ENABLE_HTTP_AUTH)) {
            return false;
        }

        return [
            'username' => $this->scopeConfig->getValue(self::CACHE_WARMER_ENABLE_HTTP_USERNAME),
            'password' => $this->scopeConfig->getValue(self::CACHE_WARMER_ENABLE_HTTP_PASSWORD),
        ];
    }

    /**
     * Dynamically change core config data cache warming flag
     *
     * @param $value
     */
    public function setCacheWarmingFlag($value)
    {
        $tableName = $this->resourceConnection->getTableName(self::CORE_CONFIG_DATA_TABLE);
        $connection = $this->resourceConnection->getConnection();
        $connection->update(
            $tableName,
            [
                'value' => $value,
            ],
            [
                'path = ?' => self::CACHE_WARMING_PROGRESS_FLAG
            ]
        );
    }

    /**
     * Dynamically get core config data cache warming flag
     *
     * @return string
     */
    private function getCacheWarmingFlag()
    {
        $tableName = $this->resourceConnection->getTableName(self::CORE_CONFIG_DATA_TABLE);
        $connection = $this->resourceConnection->getConnection();

        $select = $connection->select()
            ->from($tableName, 'value')
            ->where('path = ?', self::CACHE_WARMING_PROGRESS_FLAG);

        return $connection->fetchOne($select);
    }
}
