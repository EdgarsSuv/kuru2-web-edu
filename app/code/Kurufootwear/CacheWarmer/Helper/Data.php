<?php
/**
 * @vendor Kurufootwear
 * @module Kurufootwear_CacheWarmer
 * @author Aleksejs Baranovs <info@scandiweb.com>
 * @copyright Copyright (c) 2018 KURU Footwear. All rights reserved.
 */

namespace Kurufootwear\CacheWarmer\Helper;

use Magento\Catalog\Model\Product;
use Magento\Catalog\Model\Product\Attribute\Source\Status;
use Magento\Framework\App\Helper\AbstractHelper;
use Magento\Framework\App\Helper\Context;
use Magento\Catalog\Model\Product\Visibility;
use Magento\Catalog\Model\Product\Type;
use Kurufootwear\CacheWarmer\Api\CacheWarmerRepositoryInterface;
use Kurufootwear\CacheWarmer\Model\ResourceModel\CacheWarmer\CollectionFactory as CacheWarmerCollection;
use Kurufootwear\CacheWarmer\Model\CacheWarmerFactory;
use Magento\Cms\Model\ResourceModel\Page\CollectionFactory as CmsPageCollection;
use Magento\Catalog\Model\ResourceModel\Category\CollectionFactory as CategoryCollection;
use Magento\Catalog\Model\ResourceModel\Product\CollectionFactory as ProductCollection;
use Magento\Store\Model\StoreManagerInterface;

/**
 * Class Data
 * @package Kurufootwear\CacheWarmer\Helper
 */
class Data extends AbstractHelper
{
    /* CMS page type */
    const TYPE_CMS_PAGE = 'cms_page';

    /* Category page type */
    const TYPE_CATEGORY_PAGE = 'category_page';

    /* Product page type */
    const TYPE_PRODUCT_PAGE = 'product_page';

    /* All other pages */
    const TYPE_OTHER_PAGE = 'other_page';

    /* Sources */
    const SOURCE_MIGRATION = 'migration';
    const SOURCE_CRON = 'cron';
    const SOURCE_MANUAL = 'manual';

    /**
     * @var CacheWarmerRepositoryInterface
     */
    private $cacheWarmerRepository;

    /**
     * @var CmsPageCollection
     */
    private $cmsPageCollection;

    /**
     * @var CacheWarmerCollection
     */
    private $cacheWarmerCollection;

    /**
     * @var CacheWarmerFactory
     */
    private $cacheWarmerFactory;

    /**
     * @var CategoryCollection
     */
    private $categoryCollection;

    /**
     * @var ProductCollection
     */
    private $productCollection;

    /**
     * @var StoreManagerInterface
     */
    private $storeManager;

    /**
     * Data constructor.
     *
     * @param Context $context
     * @param CacheWarmerRepositoryInterface $cacheWarmerRepository
     * @param CacheWarmerCollection $cacheWarmerCollection
     * @param CmsPageCollection $cmsPageCollection
     * @param CategoryCollection $categoryCollection
     * @param ProductCollection $productCollection
     * @param CacheWarmerFactory $cacheWarmerFactory
     * @param StoreManagerInterface $storeManager
     */
    public function __construct(
        Context $context,
        CacheWarmerRepositoryInterface $cacheWarmerRepository,
        CacheWarmerCollection $cacheWarmerCollection,
        CmsPageCollection $cmsPageCollection,
        CategoryCollection $categoryCollection,
        ProductCollection $productCollection,
        CacheWarmerFactory $cacheWarmerFactory,
        StoreManagerInterface $storeManager
    ) {
        parent::__construct($context);

        $this->cacheWarmerRepository = $cacheWarmerRepository;
        $this->cmsPageCollection = $cmsPageCollection;
        $this->cacheWarmerCollection = $cacheWarmerCollection;
        $this->cacheWarmerFactory = $cacheWarmerFactory;
        $this->categoryCollection = $categoryCollection;
        $this->productCollection = $productCollection;
        $this->storeManager = $storeManager;
    }

    /**
     * Add cache warmer data to the table
     *
     * @param $source
     */
    public function addCacheWarmerData($source) {
        $this->addCacheWarmerCmsPageData($source);
        $this->addCacheWarmerCategoryPageData($source);
        $this->addCacheWarmerProductPageData($source);
    }

    /**
     * Add base URL data to cache warmer
     *
     * @param $source
     */
    public function addBaseUrlData($source)
    {
        $model = $this->cacheWarmerFactory->create()
            ->setPageName('Kurufootwear Homepage')
            ->setPageType(self::TYPE_CMS_PAGE)
            // '_nosid' param is necessary to avoid 'Area code is not set' setup:upgrade error
            ->setPageUrl($this->_urlBuilder->getDirectUrl('', ['_nosid' => true, '_secure' => true]))
            ->setWarmUp(1)
            ->setSource($source);

        $this->cacheWarmerRepository->save($model);
    }

    /**
     * Add cache warmer CMS page data
     *
     * @param $source
     */
    public function addCacheWarmerCmsPageData($source)
    {
        $cmsPages = $this->cmsPageCollection->create()
            ->addFieldToFilter('is_active', '1');

        foreach ($cmsPages as $cmsPage) {
            // Try to load existing record by identifier and page type, if exist, just update values, else create new record
            $model = $this->cacheWarmerCollection->create()
                ->addFieldToFilter('page_identifier', $cmsPage->getId())
                ->addFieldToFilter('page_type', self::TYPE_CMS_PAGE)
                ->getFirstItem();

            // Do not update manually changed entry
            if ($model->getId() && $model->getSource() === Data::SOURCE_MANUAL) {
                continue;
            }

            if (!$model->getId()) {
                $model = $this->cacheWarmerFactory->create();
            }

            $model->setPageName($cmsPage->getTitle())
                ->setPageType(self::TYPE_CMS_PAGE)
                ->setPageIdentifier($cmsPage->getId())
                // '_nosid' param is necessary to avoid 'Area code is not set' setup:upgrade error
                ->setPageUrl($this->_urlBuilder->getDirectUrl($cmsPage->getIdentifier(), ['_nosid' => true, '_secure' => true]))
                // do not change warm up status if the record exists
                ->setWarmUp(!is_null($model->getWarmUp()) ? $model->getWarmUp() : 1)
                ->setSource($source);

            $this->cacheWarmerRepository->save($model);
        }
    }

    /**
     * Add cache warmer Category page data
     *
     * @param $source
     */
    public function addCacheWarmerCategoryPageData($source)
    {
        $categoryCollection = $this->categoryCollection->create()
            ->addIsActiveFilter()
            ->addNameToResult()
            ->addUrlRewriteToResult();

        foreach ($categoryCollection as $category) {
            if ($url = $category->getRequestPath()) {
                // Try to load existing record by identifier and page type, if exist, just update values, else create new record
                $model = $this->cacheWarmerCollection->create()
                    ->addFieldToFilter('page_identifier', $category->getEntityId())
                    ->addFieldToFilter('page_type', self::TYPE_CATEGORY_PAGE)
                    ->getFirstItem();

                // Do not update manually changed entry
                if ($model->getId() && $model->getSource() === Data::SOURCE_MANUAL) {
                    continue;
                }

                if (!$model->getId()) {
                    $model = $this->cacheWarmerFactory->create();
                }

                $model->setPageName($category->getName())
                    ->setPageType(self::TYPE_CATEGORY_PAGE)
                    ->setPageIdentifier($category->getEntityId())
                    // '_nosid' param is necessary to avoid 'Area code is not set' setup:upgrade error
                    ->setPageUrl($this->_urlBuilder->getDirectUrl($url, ['_nosid' => true, '_secure' => true]))
                    // do not change warm up status if the record exists
                    ->setWarmUp(!is_null($model->getWarmUp()) ? $model->getWarmUp() : 1)
                    ->setSource($source);

                $this->cacheWarmerRepository->save($model);
            }
        }
    }

    /**
     * Add cache warmer Product page data
     *
     * @param $source
     */
    public function addCacheWarmerProductPageData($source)
    {
        $productCollection = $this->productCollection->create()
            ->addFieldToFilter([
                [
                    'attribute' => 'type_id',
                    'eq' => Type::TYPE_SIMPLE
                ],
                [
                    'attribute' => 'product_duplicate',
                    'eq' => 1
                ]
            ])->addAttributeToFilter(
                'visibility',
                [
                    'in' => [
                        Visibility::VISIBILITY_IN_CATALOG, Visibility::VISIBILITY_BOTH
                    ]
                ]
            )->addAttributeToSelect(
                'name'
            )->addAttributeToSelect(
                'product_color_data'
            )->addAttributeToSelect(
                'parent_sku'
            )->addAttributeToFilter(
                'status', Status::STATUS_ENABLED
            );

        foreach ($productCollection as $product) {
            // If duplicate product
            if ($product->getParentSku() && $color = $product->getProductColorData()) {
                // Need to use collection to avoid 'Area code is not set' setup:upgrade error
                $parentProduct = $this->productCollection->create()
                    ->addFieldToFilter('sku', $product->getParentSku())
                    ->setStoreId($this->storeManager->getDefaultStoreView()->getId())
                    ->addUrlRewrite()
                    ->getFirstItem();

                if (!$parentProduct->getId()) {
                    continue;
                }

                // '_nosid' param is necessary to avoid 'Area code is not set' setup:upgrade error
                $params = [
                    '_nosid' => true,
                    '_secure' => true,
                    '_query' => [
                        'color' => $color
                    ]
                ];

                $productUrl = $parentProduct->getUrlModel()->getUrl($parentProduct, $params);

                $this->addProductData($product, sprintf('%s %s', $product->getName(), $color), $productUrl, $source);
            } else {
                $this->_logger->warning(sprintf('Product with ID %s does not have Parent SKU or Color specified', $product->getId()));
            }

            // If simple product
            if ($product->getTypeId() === Type::TYPE_SIMPLE) {
                $this->addProductData($product, $product->getName(), $product->getUrlModel()->getUrl($product, ['_nosid' => true, '_secure' => true]), $source);
            }
        }
    }

    /**
     * Add product data to Cache Warmer
     *
     * @param $product
     * @param $name
     * @param $url
     * @param $source
     */
    private function addProductData(Product $product, $name, $url, $source)
    {
        // Try to load existing record by identifier and page type, if exist, just update values, else create new record
        $model = $this->cacheWarmerCollection->create()
            ->addFieldToFilter('page_identifier', $product->getEntityId())
            ->addFieldToFilter('page_type', self::TYPE_PRODUCT_PAGE)
            ->getFirstItem();

        // Do not update manually changed entry
        if ($model->getId() && $model->getSource() === Data::SOURCE_MANUAL) {
            return;
        }

        if (!$model->getId()) {
            $model = $this->cacheWarmerFactory->create();
        }

        $model->setPageName($name)
            ->setPageType(self::TYPE_PRODUCT_PAGE)
            ->setPageIdentifier($product->getEntityId())
            ->setPageUrl($url)
            // do not change warm up status if the record exists
            ->setWarmUp(!is_null($model->getWarmUp()) ? $model->getWarmUp() : 1)
            ->setSource($source);

        $this->cacheWarmerRepository->save($model);
    }
}
