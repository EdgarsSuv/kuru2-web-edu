<?php
/**
 * @vendor Kurufootwear
 * @module Kurufootwear_CacheWarmer
 * @author Aleksejs Baranovs <info@scandiweb.com>
 * @copyright Copyright (c) 2018 KURU Footwear. All rights reserved.
 */

namespace Kurufootwear\CacheWarmer\Model\Config\Source;

use Magento\Framework\Option\ArrayInterface;

/**
 * Class Status
 * @package Kurufootwear\CacheWarmer\Model\Config\Source
 */
class Status implements ArrayInterface
{
    /**
     * Return array of options as value-label pairs
     *
     * @return array
     */
    public function toOptionArray(): array
    {
        return [
            [
                'value' => 0,
                'label' => __('Running')
            ],
            [
                'value' => 1,
                'label' => __('Completed')
            ],
        ];
    }
}