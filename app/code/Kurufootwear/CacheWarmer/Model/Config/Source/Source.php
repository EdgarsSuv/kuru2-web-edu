<?php
/**
 * @vendor Kurufootwear
 * @module Kurufootwear_CacheWarmer
 * @author Aleksejs Baranovs <info@scandiweb.com>
 * @copyright Copyright (c) 2018 KURU Footwear. All rights reserved.
 */

namespace Kurufootwear\CacheWarmer\Model\Config\Source;

use Magento\Framework\Option\ArrayInterface;
use Kurufootwear\CacheWarmer\Helper\Data;

/**
 * Class PageType
 * @package Kurufootwear\CacheWarmer\Model\Config\Source
 */
class Source implements ArrayInterface
{
    /**
     * Return array of options as value-label pairs
     *
     * @return array
     */
    public function toOptionArray(): array
    {
        return [
            [
                'value' => Data::SOURCE_MANUAL,
                'label' => __('Manual')
            ],
            [
                'value' => Data::SOURCE_MIGRATION,
                'label' => __('Migration')
            ],
            [
                'value' => Data::SOURCE_CRON,
                'label' => __('CRON')
            ]
        ];
    }
}