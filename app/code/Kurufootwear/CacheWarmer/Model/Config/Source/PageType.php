<?php
/**
 * @vendor Kurufootwear
 * @module Kurufootwear_CacheWarmer
 * @author Aleksejs Baranovs <info@scandiweb.com>
 * @copyright Copyright (c) 2018 KURU Footwear. All rights reserved.
 */

namespace Kurufootwear\CacheWarmer\Model\Config\Source;

use Magento\Framework\Option\ArrayInterface;
use Kurufootwear\CacheWarmer\Helper\Data;

/**
 * Class PageType
 * @package Kurufootwear\CacheWarmer\Model\Config\Source
 */
class PageType implements ArrayInterface
{
    /**
     * Return array of options as value-label pairs
     *
     * @return array
     */
    public function toOptionArray(): array
    {
        return [
            [
                'value' => Data::TYPE_PRODUCT_PAGE,
                'label' => __('Product Page')
            ],
            [
                'value' => Data::TYPE_CATEGORY_PAGE,
                'label' => __('Category Page')
            ],
            [
                'value' => Data::TYPE_CMS_PAGE,
                'label' => __('CMS Page')
            ],
            [
                'value' => Data::TYPE_OTHER_PAGE,
                'label' => __('Other')
            ]
        ];
    }
}