<?php
/**
 * @vendor Kurufootwear
 * @module Kurufootwear_CacheWarmer
 * @author Aleksejs Baranovs <info@scandiweb.com>
 * @copyright Copyright (c) 2018 KURU Footwear. All rights reserved.
 */

namespace Kurufootwear\CacheWarmer\Model;

use Exception;
use Kurufootwear\CacheWarmer\Api\Data\CacheWarmerInterface;
use Kurufootwear\CacheWarmer\Api\CacheWarmerRepositoryInterface;
use Kurufootwear\CacheWarmer\Model\CacheWarmer as CacheWarmerModel;
use Kurufootwear\CacheWarmer\Model\CacheWarmerFactory;
use Kurufootwear\CacheWarmer\Model\ResourceModel\CacheWarmer as CacheWarmerResource;
use Magento\Framework\Exception\CouldNotDeleteException;
use Magento\Framework\Exception\CouldNotSaveException;
use Magento\Framework\Exception\NoSuchEntityException;

/**
 * Class CacheWarmerRepository
 * @package Kurufootwear\CacheWarmer\Model
 */
class CacheWarmerRepository implements CacheWarmerRepositoryInterface
{
    /**
     * @var CacheWarmerFactory
     */
    private $cacheWarmerFactory;

    /**
     * @var CacheWarmerResource
     */
    private $cacheWarmerResource;

    /**
     * CacheWarmerRepository constructor.
     * @param CacheWarmerFactory $cacheWarmerFactory
     * @param CacheWarmerResource $cacheWarmerResource
     */
    public function __construct(
        CacheWarmerFactory $cacheWarmerFactory,
        CacheWarmerResource $cacheWarmerResource
    ) {
        $this->cacheWarmerFactory = $cacheWarmerFactory;
        $this->cacheWarmerResource = $cacheWarmerResource;
    }

    /**
     * Get Cache Warmer object by id
     *
     * @param $id
     *
     * @return mixed
     *
     * @throws NoSuchEntityException
     */
    public function getById($id): CacheWarmerModel
    {
        $object = $this->cacheWarmerFactory->create();

        $this->cacheWarmerResource->load($object, $id);

        if (!$object->getId()) {
            throw new NoSuchEntityException(__('Cache Warmer Object with id "%1" does not exist.', $id));
        }

        return $object;
    }

    /**
     * Save cache warmer object
     *
     * @param $cacheWarmer
     *
     * @return mixed
     *
     * @throws CouldNotSaveException
     */
    public function save(CacheWarmerInterface $cacheWarmer): CacheWarmerModel
    {
        try {
            $this->cacheWarmerResource->save($cacheWarmer);
        } catch (Exception $e) {
            throw new CouldNotSaveException(
                __('Could not save the cache warmer object: %1', $e->getMessage())
            );
        }

        return $cacheWarmer;
    }

    /**
     * Delete cache warmer object
     *
     * @param CacheWarmerInterface $cacheWarmer
     *
     * @return bool
     *
     * @throws CouldNotDeleteException
     */
    public function delete(CacheWarmerInterface $cacheWarmer)
    {
        try {
            $this->cacheWarmerResource->delete($cacheWarmer);
        } catch (Exception $e) {
            throw new CouldNotDeleteException(
                __('Could not delete the cache warmer object: %1', $e->getMessage())
            );
        }

        return true;
    }

    /**
     * Delete cache warmer object by id
     *
     * @param $cacheWarmerId
     *
     * @return bool
     */
    public function deleteById($cacheWarmerId)
    {
        return $this->delete($this->getById($cacheWarmerId));
    }
}
