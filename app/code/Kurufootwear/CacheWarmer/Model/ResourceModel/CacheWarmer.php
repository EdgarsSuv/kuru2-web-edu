<?php
/**
 * @vendor Kurufootwear
 * @module Kurufootwear_CacheWarmer
 * @author Aleksejs Baranovs <info@scandiweb.com>
 * @copyright Copyright (c) 2018 KURU Footwear. All rights reserved.
 */

namespace Kurufootwear\CacheWarmer\Model\ResourceModel;

use Magento\Framework\Model\ResourceModel\Db\AbstractDb;

/**
 * Class CacheWarmer
 * @package Kurufootwear\CacheWarmer\Model\ResourceModel
 */
class CacheWarmer extends AbstractDb
{
    /**
     * Cache Warmer table
     */
    const CACHE_WARMER_TABLE = 'cache_warmer';

    /**
     * Model construct that should be used for object initialization
     *
     * @return void
     */
    protected function _construct()
    {
        $this->_init(self::CACHE_WARMER_TABLE, 'entity_id');
    }

    /**
     * Set running flag for all entries
     */
    public function setRunningFlagForAll()
    {
        $connection = $this->getConnection();

        $connection->beginTransaction();
        $connection->update(
            $this->getMainTable(),
            ['status' => 0],
            'warm_up = 1 AND page_url is NOT NULL'
        );
        $connection->commit();
    }

    /**
     * Set complete flag by entity id
     *
     * @param $entityId
     */
    public function setCompleteFlagByEntityId($entityId)
    {
        $connection = $this->getConnection();

        $connection->beginTransaction();
        $connection->update(
            $this->getMainTable(),
            ['status' => 1],
            [
                'entity_id = ?' => $entityId
            ]
        );
        $connection->commit();
    }
}
