<?php
/**
 * @vendor Kurufootwear
 * @module Kurufootwear_CacheWarmer
 * @author Aleksejs Baranovs <info@scandiweb.com>
 * @copyright Copyright (c) 2018 KURU Footwear. All rights reserved.
 */

namespace Kurufootwear\CacheWarmer\Model\ResourceModel\CacheWarmer;

use Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection;
use Kurufootwear\CacheWarmer\Model\CacheWarmer;
use Kurufootwear\CacheWarmer\Model\ResourceModel\CacheWarmer as CacheWarmerResource;
use Kurufootwear\CacheWarmer\Helper\Data;

/**
 * Class Collection
 * @package Kurufootwear\CacheWarmer\Model\ResourceModel\CacheWarmer
 */
class Collection extends AbstractCollection
{
    /**
     * @var string
     */
    protected $_idFieldName = 'entity_id';

    /**
     * Collection initialization
     *
     * @return void
     */
    protected function _construct()
    {
        $this->_init(CacheWarmer::class, CacheWarmerResource::class);
    }

    /**
     * Get all cms page IDs from collection
     *
     * @return array
     */
    public function getAllCmsPageIds()
    {
        $cmsIds = [];

        foreach ($this as $item) {
            if ($item->getData('page_type') === Data::TYPE_CMS_PAGE) {
                $cmsIds[] = $item->getData('page_identifier');
            }
        }

        return $cmsIds;
    }

    /**
     * Get all category page IDs from collection
     *
     * @return array
     */
    public function getAllCategoryPageIds()
    {
        $cdpIds = [];

        foreach ($this as $item) {
            if ($item->getData('page_type') === Data::TYPE_CATEGORY_PAGE) {
                $cdpIds[] = $item->getData('page_identifier');
            }
        }

        return $cdpIds;
    }

    /**
     * Get all product page IDs from collection
     *
     * @return array
     */
    public function getAllProductPageIds()
    {
        $pdpIds = [];

        foreach ($this as $item) {
            if ($item->getData('page_type') === Data::TYPE_PRODUCT_PAGE) {
                $pdpIds[] = $item->getData('page_identifier');
            }
        }

        return $pdpIds;
    }

    /**
     * Add filtering by ids
     *
     * @param array $entityIds
     *
     * @return $this
     */
    public function addIdsToFilter($entityIds = [])
    {
        $this->addFieldToFilter('main_table.entity_id', $entityIds);

        return $this;
    }
}
