<?php
/**
 * @vendor Kurufootwear
 * @module Kurufootwear_CacheWarmer
 * @author Aleksejs Baranovs <info@scandiweb.com>
 * @copyright Copyright (c) 2018 KURU Footwear. All rights reserved.
 */

namespace Kurufootwear\CacheWarmer\Model;

use Kurufootwear\CacheWarmer\Model\ResourceModel\CacheWarmer as CacheWarmerResource;
use Magento\Framework\DataObject\IdentityInterface;
use Magento\Framework\Model\AbstractModel;
use Kurufootwear\CacheWarmer\Api\Data\CacheWarmerInterface;

/**
 * Class CacheWarmer
 * @package Kurufootwear\CacheWarmer\Model
 */
class CacheWarmer extends AbstractModel implements CacheWarmerInterface, IdentityInterface
{
    /**
     * Identity tag which is prepended to model id
     *
     * @var string
     */
    const CACHE_TAG = 'cache_warmer';

    /**
     * Model construct that should be used for object initialization
     *
     * @return void
     */
    protected function _construct()
    {
        $this->_init(CacheWarmerResource::class);
    }

    /**
     * Return unique ID(s) for each object in system
     *
     * @return array
     */
    public function getIdentities()
    {
        return [self::CACHE_TAG . '_' . $this->getId()];
    }

    /**
     * Get page name
     *
     * @return string
     */
    public function getPageName()
    {
        return $this->getData(self::PAGE_NAME);
    }

    /**
     * Set page name
     *
     * @param $pageName
     *
     * @return $this
     */
    public function setPageName($pageName)
    {
        return $this->setData(self::PAGE_NAME, $pageName);
    }

    /**
     * Get page type
     *
     * @return string
     */
    public function getPageType()
    {
        return $this->getData(self::PAGE_TYPE);
    }

    /**
     * Set page type
     *
     * @param $pageType
     *
     * @return $this
     */
    public function setPageType($pageType)
    {
        return $this->setData(self::PAGE_TYPE, $pageType);
    }

    /**
     * Get page identifier
     *
     * @return string
     */
    public function getPageIdentifier()
    {
        return $this->getData(self::PAGE_IDENTIFIER);
    }

    /**
     * Set page identifier
     *
     * @param $pageIdentifier
     *
     * @return $this
     */
    public function setPageIdentifier($pageIdentifier)
    {
        return $this->setData(self::PAGE_IDENTIFIER, $pageIdentifier);
    }

    /**
     * Get page URL
     *
     * @return string
     */
    public function getPageUrl()
    {
        return $this->getData(self::PAGE_URL);
    }

    /**
     * Set page URL
     *
     * @param $pageUrl
     *
     * @return $this
     */
    public function setPageUrl($pageUrl)
    {
        return $this->setData(self::PAGE_URL, $pageUrl);
    }

    /**
     * Should be warmed up
     *
     * @return int
     */
    public function getWarmUp()
    {
        return $this->getData(self::WARM_UP);
    }

    /**
     * Set should be warmed up
     *
     * @param $warmUp
     *
     * @return $this
     */
    public function setWarmUp($warmUp)
    {
        return $this->setData(self::WARM_UP, $warmUp);
    }

    /**
     * Get source
     *
     * @return string
     */
    public function getSource()
    {
        return $this->getData(self::SOURCE);
    }

    /**
     * Set source
     *
     * @param $source
     *
     * @return $this
     */
    public function setSource($source)
    {
        return $this->setData(self::SOURCE, $source);
    }
}
