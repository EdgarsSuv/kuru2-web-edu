<?php
/**
 * @vendor Kurufootwear
 * @module Kurufootwear_CacheWarmer
 * @author Aleksejs Baranovs <info@scandiweb.com>
 * @copyright Copyright (c) 2018 KURU Footwear. All rights reserved.
 */

namespace Kurufootwear\CacheWarmer\Block\Adminhtml\CacheWarmer\Edit\Tab;

use Kurufootwear\CacheWarmer\Model\Config\Source\PageType;
use Kurufootwear\CacheWarmer\Model\Config\Source\Source;
use Magento\Backend\Block\Template\Context;
use Magento\Backend\Block\Widget\Form\Generic;
use Magento\Backend\Block\Widget\Tab\TabInterface;
use Magento\Framework\Data\Form;
use Magento\Framework\Data\FormFactory;
use Magento\Framework\Phrase;
use Magento\Framework\Registry;

/**
 * Class Main
 * @package Kurufootwear\CacheWarmer\Block\Adminhtml\CacheWarmer\Edit\Tab
 */
class Main extends Generic implements TabInterface
{
    /**
     * @var PageType
     */
    private $pageType;

    /**
     * @var Source
     */
    private $source;

    /**
     * Main constructor.
     * @param Context $context
     * @param Registry $registry
     * @param FormFactory $formFactory
     * @param PageType $pageType
     * @param Source $source
     * @param array $data
     */
    public function __construct(
        Context $context,
        Registry $registry,
        FormFactory $formFactory,
        PageType $pageType,
        Source $source,
        array $data = []
    ) {
        parent::__construct($context, $registry, $formFactory, $data);

        $this->pageType = $pageType;
        $this->source = $source;
    }

    /**
     * Prepare form
     *
     * @return Generic
     */
    protected function _prepareForm()
    {
        $model = $this->_coreRegistry->registry('cache_warmer');
        /** @var Form $form */
        $form = $this->_formFactory->create();

        $fieldset = $form->addFieldset(
            'base_fieldset',
            [
                'legend' => __('Main Information'),
            ]
        );

        if ($model->getId()) {
            $fieldset->addField(
                'entity_id',
                'hidden',
                [
                    'name' => 'entity_id',
                ]
            );
        }

        $fieldset->addField(
            'page_name',
            'text',
            [
                'name' => 'page_name',
                'label' => __('Page Name'),
                'title' => __('Page Name'),
                'required' => true,
            ]
        );

        $fieldset->addField(
            'page_type',
            'select',
            [
                'name' => 'page_type',
                'label' => __('Page Type'),
                'title' => __('Page Type'),
                'required' => true,
                'values' => $this->pageType->toOptionArray(),
            ]
        );

        $fieldset->addField(
            'page_identifier',
            'text',
            [
                'name' => 'page_identifier',
                'label' => __('Page Identifier'),
                'title' => __('Page Identifier'),
                'required' => false,
                'note' => sprintf(
                    '%s <br><strong>%s</strong>',
                    __('Used for CRON job to load a CMS Page / Category / Product and automatically update Cache Warmer Information.'),
                    __('If no page identifier, CRON will not update this entry automatically and if such CMS Page / Category / Product really exists, CRON will create a new record.')
                ),
            ]
        );

        $fieldset->addField(
            'page_url',
            'text',
            [
                'name' => 'page_url',
                'label' => __('Page URL'),
                'title' => __('Page URL'),
                'required' => true,
            ]
        );

        // Necessary for correct checkbox work
        $fieldset->addField(
            'warm_up_disabled',
            'hidden',
            [
                'name' => 'warm_up',
            ]
        );

        $fieldset->addField(
            'warm_up',
            'checkbox',
            [
                'name' => 'warm_up',
                'label' => __('Should be warmed up?'),
                'title' => __('Should be warmed up?'),
                'onclick' => 'this.value = this.checked ? 1 : 0;',
                'checked' => $model->getWarmUp(),
            ]
        );

        $fieldset->addField(
            'source',
            'select',
            [
                'name' => 'source',
                'label' => __('Source'),
                'title' => __('Source'),
                'required' => true,
                'values' => $this->source->toOptionArray(),
                'note' => sprintf('<strong>%s</strong>', __('If "Manual" selected -> Cron job will not update this record.'))
            ]
        );

        $model->setData('warm_up_disabled', '0');
        $form->setValues($model->getData());
        $this->setForm($form);

        return parent::_prepareForm();
    }

    /**
     * Prepare label for tab
     *
     * @return Phrase
     */
    public function getTabLabel()
    {
        return __('Main');
    }

    /**
     * Prepare title for tab
     *
     * @return Phrase
     */
    public function getTabTitle()
    {
        return __('Main');
    }

    /**
     * Can show tab
     *
     * @return bool
     */
    public function canShowTab()
    {
        return true;
    }

    /**
     * Is hidden
     *
     * @return bool
     */
    public function isHidden()
    {
        return false;
    }
}
