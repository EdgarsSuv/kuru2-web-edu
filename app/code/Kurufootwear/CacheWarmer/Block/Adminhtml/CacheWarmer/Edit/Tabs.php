<?php
/**
 * @vendor Kurufootwear
 * @module Kurufootwear_CacheWarmer
 * @author Aleksejs Baranovs <info@scandiweb.com>
 * @copyright Copyright (c) 2018 KURU Footwear. All rights reserved.
 */

namespace Kurufootwear\CacheWarmer\Block\Adminhtml\CacheWarmer\Edit;

use Magento\Backend\Block\Widget\Tabs as WidgetTabs;

/**
 * Class Tabs
 * @package Kurufootwear\CacheWarmer\Block\Adminhtml\CacheWarmer\Edit
 */
class Tabs extends WidgetTabs
{
    /**
     * Construct
     *
     * @return void
     */
    protected function _construct()
    {
        parent::_construct();

        $this->setId('cachewarmer_tabs');
        $this->setDestElementId('edit_form');
        $this->setTitle(__('Cache Warmer Page Information'));
    }
}
