<?php
/**
 * @vendor Kurufootwear
 * @module Kurufootwear_CacheWarmer
 * @author Aleksejs Baranovs <info@scandiweb.com>
 * @copyright Copyright (c) 2018 KURU Footwear. All rights reserved.
 */

namespace Kurufootwear\CacheWarmer\Block\Adminhtml\CacheWarmer;

use Magento\Backend\Block\Widget\Form\Container;

/**
 * Class Edit
 * @package Kurufootwear\CacheWarmer\Block\Adminhtml\CacheWarmer
 */
class Edit extends Container
{
    /**
     * Construct
     */
    protected function _construct()
    {
        $this->_objectId = 'entity_id';
        $this->_blockGroup = 'Kurufootwear_CacheWarmer';
        $this->_controller = 'adminhtml_cacheWarmer';

        parent::_construct();

        $this->buttonList->update('save', 'label', __('Save Cache Warmer Page'));
        $this->buttonList->add(
            'saveandcontinue',
            [
                'label' => __('Save and Continue Edit'),
                'class' => 'save',
                'data_attribute' => [
                    'mage-init' => [
                        'button' => [
                            'event' => 'saveAndContinueEdit',
                            'target' => '#edit_form'
                        ],
                    ],
                ]
            ],
            -100
        );

        $this->buttonList->update('delete', 'label', __('Delete Cache Warmer Page'));
    }
}
