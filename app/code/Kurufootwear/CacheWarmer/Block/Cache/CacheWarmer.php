<?php
/**
 * @vendor Kurufootwear
 * @module Kurufootwear_CacheWarmer
 * @author Aleksejs Baranovs <info@scandiweb.com>
 * @copyright Copyright (c) 2018 KURU Footwear. All rights reserved.
 */

namespace Kurufootwear\CacheWarmer\Block\Cache;

use Kurufootwear\CacheWarmer\Helper\Warmer;
use Magento\Backend\Block\Template;
use Magento\Backend\Block\Template\Context;

/**
 * Class CacheWarmer
 * @package Kurufootwear\CacheWarmer\Block\Cache
 */
class CacheWarmer extends Template
{
    /**
     * Cache warmer action
     */
    const CACHE_WARMER_ACTION = 'cachewarmer/index/warmCache';

    /**
     * @var Warmer
     */
    private $warmerHelper;

    /**
     * CacheWarmer constructor.
     *
     * @param Context $context
     * @param Warmer $warmerHelper
     * @param array $data
     */
    public function __construct(
        Context $context,
        Warmer $warmerHelper,
        array $data = []
    ) {
        parent::__construct($context, $data);
        $this->warmerHelper = $warmerHelper;
    }

    /**
     * Get cache warmer action url
     *
     * @return string
     */
    public function getCacheWarmerUrl()
    {
        return $this->getUrl(self::CACHE_WARMER_ACTION);
    }

    /**
     * Check if cache warmer is enabled
     *
     * @return int
     */
    public function isEnabled()
    {
        return $this->warmerHelper->isEnabled();
    }
}