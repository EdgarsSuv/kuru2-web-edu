<?php
/**
 * @vendor Kurufootwear
 * @module Kurufootwear_CacheWarmer
 * @author Aleksejs Baranovs <info@scandiweb.com>
 * @copyright Copyright (c) 2018 KURU Footwear. All rights reserved.
 */

namespace Kurufootwear\CacheWarmer\Cron;

use Kurufootwear\CacheWarmer\Helper\Data;
use Kurufootwear\CacheWarmer\Helper\Warmer;
use Psr\Log\LoggerInterface;

/**
 * Class ImportCacheWarmerData
 *
 * Cron task for importing and refreshing data into cache_warmer table
 *
 * @package Kurufootwear\CacheWarmer\Cron
 */
class ImportCacheWarmerData
{
    /**
     * @var Data
     */
    private $cacheWarmerHelper;

    /**
     * @var LoggerInterface
     */
    private $logger;

    /**
     * @var Warmer
     */
    private $warmerHelper;

    /**
     * ImportCacheWarmerData constructor.
     *
     * @param Data $cacheWarmerHelper
     * @param Warmer $warmerHelper
     * @param LoggerInterface $logger
     */
    public function __construct(
        Data $cacheWarmerHelper,
        Warmer $warmerHelper,
        LoggerInterface $logger
    ) {
        $this->cacheWarmerHelper = $cacheWarmerHelper;
        $this->logger = $logger;
        $this->warmerHelper = $warmerHelper;
    }

    /**
     * Entry point for the cron job.
     */
    public function execute()
    {
        if ($this->warmerHelper->isEnabled() && $this->warmerHelper->isCronEnabled()) {
            $this->logger->notice(__('Importing data into cache warmer table started.'));

            $this->cacheWarmerHelper->addCacheWarmerData(Data::SOURCE_CRON);

            $this->logger->notice(__('Importing data into cache warmer table finished.'));
        } else {
            $this->logger->notice(__('Cannot proceed with import. To enable it go to Stores -> Configuration -> Kurufootwear -> Cache Warmer.'));
        }
    }
}
