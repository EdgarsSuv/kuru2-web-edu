<?php
/**
 * @vendor Kurufootwear
 * @module Kurufootwear_CacheWarmer
 * @author Aleksejs Baranovs <info@scandiweb.com>
 * @copyright Copyright (c) 2018 KURU Footwear. All rights reserved.
 */

namespace Kurufootwear\CacheWarmer\Ui\Component\Listing\Column;

use Magento\Framework\View\Element\UiComponent\ContextInterface;
use Magento\Framework\View\Element\UiComponentFactory;
use Magento\Ui\Component\Listing\Columns\Column;
use Magento\Framework\UrlInterface;

/**
 * Class CacheWarmerActions
 * @package Kurufootwear\CacheWarmer\Ui\Component\Listing\Column
 */
class CacheWarmerActions extends Column
{
    /**
     * Edit path
     */
    const CACHE_WARMER_URL_PATH_EDIT = 'cachewarmer/index/edit';

    /**
     * Delete path
     */
    const CACHE_WARMER_URL_PATH_DELETE = 'cachewarmer/index/delete';

    /**
     * @var UrlInterface
     */
    private $urlBuilder;

    /**
     * CacheWarmerActions constructor.
     *
     * @param ContextInterface $context
     * @param UiComponentFactory $uiComponentFactory
     * @param UrlInterface $urlBuilder
     * @param array $components
     * @param array $data
     */
    public function __construct(
        ContextInterface $context,
        UiComponentFactory $uiComponentFactory,
        UrlInterface $urlBuilder,
        array $components = [],
        array $data = []
    ) {
        parent::__construct($context, $uiComponentFactory, $components, $data);

        $this->urlBuilder = $urlBuilder;
    }

    /**
     * Prepare Data Source
     *
     * @param array $dataSource
     *
     * @return array
     */
    public function prepareDataSource(array $dataSource)
    {
        if (!isset($dataSource['data']['items'])) {
            return $dataSource;
        }

        foreach ($dataSource['data']['items'] as &$item) {
            if (!isset($item['entity_id'])) {
                continue;
            }

            $name = $this->getData('name');

            $item[$name]['edit'] = [
                'href' => $this->urlBuilder->getUrl(self::CACHE_WARMER_URL_PATH_EDIT, ['entity_id' => $item['entity_id']]),
                'label' => __('Edit'),
            ];

            $item[$name]['delete'] = [
                'href' => $this->urlBuilder->getUrl(self::CACHE_WARMER_URL_PATH_DELETE, ['entity_id' => $item['entity_id']]),
                'label' => __('Delete'),
                'confirm' => [
                    'title' => __('Delete "${ $.$data.title }"'),
                    'message' => __('Are you sure you wan\'t to delete a "${ $.$data.title }" record?'),
                ],
            ];
        }

        return $dataSource;
    }
}
