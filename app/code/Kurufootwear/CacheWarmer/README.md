Kurufootwear_CacheWarmer
===================

This module is used for warming cache.

## Pages included in Cache Warmer: ## 
- All active CMS pages
- All active Category Pages
- All active PDP pages
- All manual entries

## Functionality: ##
* CLI command to run Cache Warmer -> bin/magento kuru:warm:cache
* Run Cache Warmer from Magento Cache Management in BE
* Stores -> Configuration -> Kurufootwear -> Cache Warmer:
    ** Enable Cache Warmer -> Yes / No
    ** Enable Cron (cron job to import / update data in cache_warmer table) -> Yes / No
    ** Use HTTP Auth -> Yes / No (for testing servers)
    ** HTTP Auth Username
    ** HTTP Auth Password
* BE grid with all pages, which are used for cache warming - System -> Manage Cache Warmer Pages
    ** Add
    ** Edit
    ** Remove
 