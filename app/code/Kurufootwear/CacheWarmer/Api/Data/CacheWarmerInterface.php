<?php
/**
 * @vendor Kurufootwear
 * @module Kurufootwear_CacheWarmer
 * @author Aleksejs Baranovs <info@scandiweb.com>
 * @copyright Copyright (c) 2018 KURU Footwear. All rights reserved.
 */

namespace Kurufootwear\CacheWarmer\Api\Data;

/**
 * Interface CacheWarmerInterface
 * @package Kurufootwear\CacheWarmer\Api\Data
 */
interface CacheWarmerInterface
{
    /**
     * Constants for keys of data array
     */
    const PAGE_NAME = 'page_name';
    const PAGE_TYPE = 'page_type';
    const PAGE_IDENTIFIER = 'page_identifier';
    const PAGE_URL = 'page_url';
    const WARM_UP = 'warm_up';
    const SOURCE = 'source';

    /**
     * Get page name
     *
     * @return string
     */
    public function getPageName();

    /**
     * Set page name
     *
     * @param $pageName
     *
     * @return $this
     */
    public function setPageName($pageName);

    /**
     * Get page type
     *
     * @return string
     */
    public function getPageType();

    /**
     * Set page type
     *
     * @param $pageType
     *
     * @return $this
     */
    public function setPageType($pageType);

    /**
     * Get page identifier
     *
     * @return string
     */
    public function getPageIdentifier();

    /**
     * Set page identifier
     *
     * @param $pageIdentifier
     *
     * @return $this
     */
    public function setPageIdentifier($pageIdentifier);

    /**
     * Get page URL
     *
     * @return string
     */
    public function getPageUrl();

    /**
     * Set page URL
     *
     * @param $pageUrl
     *
     * @return $this
     */
    public function setPageUrl($pageUrl);

    /**
     * Should be warmed up
     *
     * @return int
     */
    public function getWarmUp();

    /**
     * Set should be warmed up
     *
     * @param $warmUp
     *
     * @return $this
     */
    public function setWarmUp($warmUp);

    /**
     * Get source
     *
     * @return string
     */
    public function getSource();

    /**
     * Set source
     *
     * @param $source
     *
     * @return $this
     */
    public function setSource($source);
}