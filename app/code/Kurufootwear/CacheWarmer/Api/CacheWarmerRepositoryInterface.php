<?php
/**
 * @vendor Kurufootwear
 * @module Kurufootwear_CacheWarmer
 * @author Aleksejs Baranovs <info@scandiweb.com>
 * @copyright Copyright (c) 2018 KURU Footwear. All rights reserved.
 */

namespace Kurufootwear\CacheWarmer\Api;

use Kurufootwear\CacheWarmer\Api\Data\CacheWarmerInterface;
use Kurufootwear\CacheWarmer\Model\CacheWarmer;
use Magento\Framework\Exception\CouldNotDeleteException;
use Magento\Framework\Exception\CouldNotSaveException;
use Magento\Framework\Exception\NoSuchEntityException;

interface CacheWarmerRepositoryInterface
{
    /**
     * Get Cache Warmer object by id
     *
     * @param $id
     *
     * @return mixed
     *
     * @throws NoSuchEntityException
     */
    public function getById($id): CacheWarmer;

    /**
     * Save cache warmer object
     *
     * @param $cacheWarmer
     *
     * @return mixed
     *
     * @throws CouldNotSaveException
     */
    public function save(CacheWarmerInterface $cacheWarmer): CacheWarmer;

    /**
     * Delete cache warmer object
     *
     * @param CacheWarmerInterface $cacheWarmer
     *
     * @return bool
     *
     * @throws CouldNotDeleteException
     */
    public function delete(CacheWarmerInterface $cacheWarmer);


    /**
     * Delete cache warmer object by id
     *
     * @param $cacheWarmerId
     *
     * @return bool
     */
    public function deleteById($cacheWarmerId);
}