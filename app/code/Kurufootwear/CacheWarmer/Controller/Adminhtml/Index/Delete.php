<?php
/**
 * @vendor Kurufootwear
 * @module Kurufootwear_CacheWarmer
 * @author Aleksejs Baranovs <info@scandiweb.com>
 * @copyright Copyright (c) 2018 KURU Footwear. All rights reserved.
 */

namespace Kurufootwear\CacheWarmer\Controller\Adminhtml\Index;

use Exception;
use Magento\Backend\App\Action;
use Magento\Backend\App\Action\Context;
use Kurufootwear\CacheWarmer\Model\CacheWarmerFactory;
use Kurufootwear\CacheWarmer\Model\ResourceModel\CacheWarmer as CacheWarmerResource;
use Magento\Backend\Model\View\Result\Redirect;

/**
 * Class Delete
 * @package Kurufootwear\CacheWarmer\Controller\Adminhtml\Index
 */
class Delete extends Action
{
    /**
     * @var CacheWarmerFactory
     */
    private $cacheWarmerFactory;

    /**
     * @var CacheWarmerResource
     */
    private $cacheWarmerResource;

    /**
     * Delete constructor.
     *
     * @param Context $context
     * @param CacheWarmerFactory $cacheWarmerFactory
     * @param CacheWarmerResource $cacheWarmerResource
     */
    public function __construct(
        Context $context,
        CacheWarmerFactory $cacheWarmerFactory,
        CacheWarmerResource $cacheWarmerResource
    ) {
        parent::__construct($context);
        $this->cacheWarmerFactory = $cacheWarmerFactory;
        $this->cacheWarmerResource = $cacheWarmerResource;
    }

    /**
     * Delete action
     */
    public function execute()
    {
        $entityId = $this->getRequest()->getParam('entity_id');

        /** @var Redirect $resultRedirect */
        $resultRedirect = $this->resultRedirectFactory->create();
        if ($entityId) {
            try {
                $model = $this->cacheWarmerFactory->create();
                $this->cacheWarmerResource->load($model, $entityId);
                $this->cacheWarmerResource->delete($model);
                $this->messageManager->addSuccessMessage(__('The cache warmer page has been successfully deleted.'));

                return $resultRedirect->setPath('*/*/');
            } catch (Exception $e) {
                $this->messageManager->addErrorMessage($e->getMessage());

                return $resultRedirect->setPath('*/*/edit', ['entity_id' => $entityId]);
            }
        }
        $this->messageManager->addErrorMessage(__('We can\'t find cache warmer page to delete.'));

        return $resultRedirect->setPath('*/*/');
    }
}