<?php
/**
 * @vendor Kurufootwear
 * @module Kurufootwear_CacheWarmer
 * @author Aleksejs Baranovs <info@scandiweb.com>
 * @copyright Copyright (c) 2018 KURU Footwear. All rights reserved.
 */

namespace Kurufootwear\CacheWarmer\Controller\Adminhtml\Index;

use Exception;
use Magento\Backend\App\Action;
use Magento\Backend\App\Action\Context;
use Kurufootwear\CacheWarmer\Helper\Warmer;
use Magento\Backend\Model\View\Result\Redirect;
use Magento\Framework\App\ResponseInterface;
use Magento\Framework\Controller\ResultInterface;
use Magento\Framework\Exception\AlreadyExistsException;
use Magento\Framework\Exception\LocalizedException;
use Psr\Log\LoggerInterface;
use Magento\Framework\Controller\ResultFactory;

/**
 * Class WarmCache
 * @package Kurufootwear\CacheWarmer\Controller\Adminhtml\Index
 */
class WarmCache extends Action
{
    /**
     * @var Warmer
     */
    private $cacheWarmer;

    /**
     * @var LoggerInterface
     */
    private $logger;

    /**
     * WarmCache constructor.
     *
     * @param Context $context
     * @param Warmer $cacheWarmer
     * @param LoggerInterface $logger
     */
    public function __construct(
        Context $context,
        Warmer $cacheWarmer,
        LoggerInterface $logger
    ) {
        parent::__construct($context);

        $this->cacheWarmer = $cacheWarmer;
        $this->logger = $logger;
    }

    /**
     * Execute cache warming
     *
     * @return Redirect|ResponseInterface|ResultInterface
     */
    public function execute()
    {
        session_write_close();
        ob_implicit_flush();

        // Execution time may be very long
        $executionTime = ini_get('max_execution_time');
        set_time_limit(0);
        ini_set('max_execution_time', 0);

        try {
            $this->logger->notice(__('Cache warmer started work from Cache Management.'));

            $this->cacheWarmer->warmCache();

            $this->logger->notice(__('Cache warmer finished work from Cache Management.'));

            $this->messageManager->addSuccessMessage(__('Cache Warmer finished without errors'));
        } catch (AlreadyExistsException $e) {
            $this->messageManager->addErrorMessage($e->getMessage());
        } catch (LocalizedException $e) {
            $this->messageManager->addErrorMessage($e->getMessage());
        } catch (Exception $e) {
            $this->messageManager->addExceptionMessage($e, __('An error occurred while warming cache.'));
        }

        ini_set('max_execution_time', $executionTime);

        /** @var Redirect $resultRedirect */
        $resultRedirect = $this->resultFactory->create(ResultFactory::TYPE_REDIRECT);

        return $resultRedirect->setPath('adminhtml/cache/');
    }
}