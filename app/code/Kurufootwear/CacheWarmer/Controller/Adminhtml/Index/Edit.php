<?php
/**
 * @vendor Kurufootwear
 * @module Kurufootwear_CacheWarmer
 * @author Aleksejs Baranovs <info@scandiweb.com>
 * @copyright Copyright (c) 2018 KURU Footwear. All rights reserved.
 */

namespace Kurufootwear\CacheWarmer\Controller\Adminhtml\Index;

use Kurufootwear\CacheWarmer\Model\CacheWarmerFactory;
use Kurufootwear\CacheWarmer\Model\ResourceModel\CacheWarmer as CacheWarmerResource;
use Magento\Backend\App\Action;
use Magento\Framework\Controller\Result\Redirect;
use Magento\Framework\Registry;
use Magento\Backend\App\Action\Context;
use Magento\Framework\View\Result\Page;
use Magento\Framework\View\Result\PageFactory as ResultPageFactory;

/**
 * Class Edit
 * @package Kurufootwear\CacheWarmer\Controller\Adminhtml\Index
 */
class Edit extends Action
{
    /**
     * @var ResultPageFactory
     */
    private $resultPageFactory;

    /**
     * @var Registry
     */
    private $registry;

    /**
     * @var CacheWarmerFactory
     */
    private $cacheWarmerFactory;

    /**
     * @var CacheWarmerResource
     */
    private $cacheWarmerResource;

    /**
     * Edit constructor.
     *
     * @param Context $context
     * @param ResultPageFactory $resultPageFactory
     * @param Registry $registry
     * @param CacheWarmerFactory $cacheWarmerFactory
     * @param CacheWarmerResource $cacheWarmerResource
     */
    public function __construct(
        Context $context,
        ResultPageFactory $resultPageFactory,
        Registry $registry,
        CacheWarmerFactory $cacheWarmerFactory,
        CacheWarmerResource $cacheWarmerResource
    ) {
        parent::__construct($context);

        $this->resultPageFactory = $resultPageFactory;
        $this->registry = $registry;
        $this->cacheWarmerFactory = $cacheWarmerFactory;
        $this->cacheWarmerResource = $cacheWarmerResource;
    }

    /**
     * Execute edit action
     *
     * @return Redirect|Page
     */
    public function execute()
    {
        $entityId = $this->getRequest()->getParam('entity_id');

        $cacheWarmerModel = $this->cacheWarmerFactory->create();

        $this->cacheWarmerResource->load($cacheWarmerModel, $entityId);

        if ($entityId && !$cacheWarmerModel->getId()) {
            $this->messageManager->addErrorMessage('Cache Warmer Page not found');

            return $this->resultRedirectFactory->create()->setPath('*/*/');
        }

        if ($entityId && $cacheWarmerModel->getData()) {
            $data = $cacheWarmerModel->getData();
        }

        if (!empty($data)) {
            $cacheWarmerModel->setData($data);
        }

        $this->registry->register('cache_warmer', $cacheWarmerModel);

        $resultPage = $this->resultPageFactory->create();
        $resultPage->getConfig()->getTitle()->prepend(__('Cache Warmer Page'));
        $resultPage->getConfig()->getTitle()
            ->prepend($cacheWarmerModel->getId() ? $cacheWarmerModel->getTitle() : __('New Cache Warmer Page'));

        return $resultPage;
    }
}
