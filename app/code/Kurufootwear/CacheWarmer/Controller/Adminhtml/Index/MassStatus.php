<?php
/**
 * @vendor Kurufootwear
 * @module Kurufootwear_CacheWarmer
 * @author Aleksejs Baranovs <info@scandiweb.com>
 * @copyright Copyright (c) 2018 KURU Footwear. All rights reserved.
 */

namespace Kurufootwear\CacheWarmer\Controller\Adminhtml\Index;

use Exception;
use Magento\Backend\App\Action;
use Magento\Backend\App\Action\Context;
use Magento\Framework\Controller\ResultInterface;
use Magento\Ui\Component\MassAction\Filter;
use Kurufootwear\CacheWarmer\Model\ResourceModel\CacheWarmer\CollectionFactory;
use Kurufootwear\CacheWarmer\Api\CacheWarmerRepositoryInterface;

/**
 * Class MassStatus
 * @package Kurufootwear\CacheWarmer\Controller\Adminhtml\Index
 */
class MassStatus extends Action
{
    /**
     * @var Filter
     */
    private $filter;

    /**
     * @var CollectionFactory
     */
    private $cacheWarmerCollection;

    /**
     * @var CacheWarmerRepositoryInterface
     */
    private $cacheWarmerRepository;

    /**
     * MassStatus constructor.
     *
     * @param Context $context
     * @param Filter $filter
     * @param CollectionFactory $cacheWarmerCollection
     * @param CacheWarmerRepositoryInterface $cacheWarmerRepository
     */
    public function __construct(
        Context $context,
        Filter $filter,
        CollectionFactory $cacheWarmerCollection,
        CacheWarmerRepositoryInterface $cacheWarmerRepository
    ) {
        parent::__construct($context);
        $this->filter = $filter;
        $this->cacheWarmerCollection = $cacheWarmerCollection;
        $this->cacheWarmerRepository = $cacheWarmerRepository;
    }

    /**
     * Execute mass status change action
     *
     * @return ResultInterface
     */
    public function execute()
    {
        $statusValue = $this->getRequest()->getParam('status');
        $collection = $this->filter->getCollection($this->cacheWarmerCollection->create());

        try {
            foreach ($collection as $item) {
                $item->setWarmUp($statusValue);
                $this->cacheWarmerRepository->save($item);
            }

            $this->messageManager->addSuccessMessage(
                __('A total of %1 record(s) have been modified.', $collection->getSize())
            );
        } catch (Exception $e) {
            $this->messageManager->addErrorMessage(__('Something went wrong while saving changes.'));
        }

        $resultRedirect = $this->resultRedirectFactory->create();

        return $resultRedirect->setPath('*/*/');
    }
}
