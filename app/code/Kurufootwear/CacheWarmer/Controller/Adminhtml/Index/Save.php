<?php
/**
 * @vendor Kurufootwear
 * @module Kurufootwear_CacheWarmer
 * @author Aleksejs Baranovs <info@scandiweb.com>
 * @copyright Copyright (c) 2018 KURU Footwear. All rights reserved.
 */

namespace Kurufootwear\CacheWarmer\Controller\Adminhtml\Index;

use Exception;
use Magento\Backend\App\Action;
use Magento\Backend\App\Action\Context;
use Magento\Backend\Model\SessionFactory;
use Magento\Framework\Exception\LocalizedException;
use Kurufootwear\CacheWarmer\Model\CacheWarmerFactory;
use Kurufootwear\CacheWarmer\Model\ResourceModel\CacheWarmer as CacheWarmerResource;
use RuntimeException;

/**
 * Class Save
 * @package Kurufootwear\CacheWarmer\Controller\Adminhtml\Index
 */
class Save extends Action
{
    /**
     * @var SessionFactory
     */
    private $sessionFactory;

    /**
     * @var CacheWarmerFactory
     */
    private $cacheWarmerFactory;

    /**
     * @var CacheWarmerResource
     */
    private $cacheWarmerResource;

    /**
     * Save constructor.
     *
     * @param Context $context
     * @param SessionFactory $sessionFactory
     * @param CacheWarmerFactory $cacheWarmerFactory
     * @param CacheWarmerResource $cacheWarmerResource
     */
    public function __construct(
        Context $context,
        SessionFactory $sessionFactory,
        CacheWarmerFactory $cacheWarmerFactory,
        CacheWarmerResource $cacheWarmerResource
    ) {
        parent::__construct($context);

        $this->sessionFactory = $sessionFactory;
        $this->cacheWarmerFactory = $cacheWarmerFactory;
        $this->cacheWarmerResource = $cacheWarmerResource;
    }

    /**
     * Save action
     *
     * @return \Magento\Framework\Controller\ResultInterface
     */
    public function execute()
    {
        $data = $this->getRequest()->getPostValue();

        /** @var \Magento\Backend\Model\View\Result\Redirect $resultRedirect */
        $resultRedirect = $this->resultRedirectFactory->create();

        if ($data) {
            $model = $this->cacheWarmerFactory->create();
            $entityId = $this->getRequest()->getParam('entity_id');

            if ($entityId) {
                $this->cacheWarmerResource->load($model, $entityId);
            }

            $model->setData($data);

            try {
                $this->cacheWarmerResource->save($model);
                $this->messageManager->addSuccessMessage(__('You saved this Cache Warmer Page.'));
                $this->sessionFactory->create()->setFormData(false);

                if ($this->getRequest()->getParam('back')) {
                    return $resultRedirect->setPath('*/*/edit', ['entity_id' => $model->getId(), '_current' => true]);
                }

                return $resultRedirect->setPath('*/*/');
            } catch (LocalizedException $e) {
                $this->messageManager->addErrorMessage($e->getMessage());
            } catch (RuntimeException $e) {
                $this->messageManager->addErrorMessage($e->getMessage());
            } catch (Exception $e) {
                $this->messageManager->addExceptionMessage($e, __('Something went wrong while saving the cache warmer page.'));
            }
            $this->_getSession()->setFormData($data);

            return $resultRedirect->setPath('*/*/edit', ['list_id' => $this->getRequest()->getParam('entity_id')]);
        }

        return $resultRedirect->setPath('*/*/');
    }
}
