<?php
/**
 * @vendor Kurufootwear
 * @module Kurufootwear_CacheWarmer
 * @author Aleksejs Baranovs <info@scandiweb.com>
 * @copyright Copyright (c) 2018 KURU Footwear. All rights reserved.
 */

namespace Kurufootwear\CacheWarmer\Controller\Adminhtml\Index;

use Exception;
use Kurufootwear\CacheWarmer\Helper\Warmer;
use Magento\Backend\App\Action;
use Magento\Backend\App\Action\Context;
use Magento\Framework\Controller\ResultInterface;
use Magento\Framework\Exception\AlreadyExistsException;

/**
 * Class MassWarm
 * @package Kurufootwear\CacheWarmer\Controller\Adminhtml\Index
 */
class MassWarm extends Action
{
    /**
     * @var Warmer
     */
    private $warmer;

    /**
     * MassWarm constructor.
     *
     * @param Context $context
     * @param Warmer $warmer
     */
    public function __construct(
        Context $context,
        Warmer $warmer
    ) {
        parent::__construct($context);

        $this->warmer = $warmer;
    }

    /**
     * Execute mass status change action
     *
     * @return ResultInterface
     */
    public function execute()
    {
        $idsToWarm = $this->getRequest()->getPostValue('selected');

        session_write_close();
        ob_implicit_flush();

        try {
            $this->warmer->warmCache(false, $idsToWarm);

            $this->messageManager->addSuccessMessage(
                __('A total of %1 record(s) have been warmed.', count($idsToWarm))
            );
        } catch (AlreadyExistsException $e) {
            $this->messageManager->addErrorMessage($e->getMessage());
        } catch (Exception $e) {
            $this->messageManager->addExceptionMessage($e, __('An error occurred while warming cache.'));
        }

        $resultRedirect = $this->resultRedirectFactory->create();

        return $resultRedirect->setPath('*/*/');
    }
}
