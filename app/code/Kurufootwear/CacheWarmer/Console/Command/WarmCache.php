<?php
/**
 * @vendor Kurufootwear
 * @module Kurufootwear_CacheWarmer
 * @author Aleksejs Baranovs <info@scandiweb.com>
 * @copyright Copyright (c) 2018 KURU Footwear. All rights reserved.
 */

namespace Kurufootwear\CacheWarmer\Console\Command;

use Kurufootwear\CacheWarmer\Helper\Warmer;
use Magento\Framework\Exception\AlreadyExistsException;
use Magento\Framework\Exception\LocalizedException;
use Psr\Log\LoggerInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Magento\Framework\App\State;

/**
 * Class WarmCache
 * @package Kurufootwear\CacheWarmer\Console\Command
 */
class WarmCache extends Command
{
    /**
     * @var State
     */
    private $appState;

    /**
     * @var Warmer
     */
    private $cacheWarmer;

    /**
     * @var LoggerInterface
     */
    private $logger;

    /**
     * WarmCache constructor.
     *
     * @param State $appState
     * @param Warmer $cacheWarmer
     * @param LoggerInterface $logger
     */
    public function __construct(
        State $appState,
        Warmer $cacheWarmer,
        LoggerInterface $logger
    ) {
        $this->appState = $appState;
        $this->cacheWarmer = $cacheWarmer;
        $this->logger = $logger;

        parent::__construct();
    }

    /**
     * {@inheritdoc}
     */
    protected function configure()
    {
        $this->setName('kuru:warm:cache')
            ->setDescription('Warm Cache')
            ->addArgument('url', InputArgument::OPTIONAL, __('Specify the URL for warming'));

        parent::configure();
    }

    /**
     * Execute CLI task
     *
     * @param InputInterface $input
     * @param OutputInterface $output
     *
     * @return void
     */
    public function execute(InputInterface $input, OutputInterface $output)
    {
        try {
            $this->appState->getAreaCode();
        } catch (LocalizedException $e) {
            $this->appState->setAreaCode('adminhtml');
        }

        if (!$this->cacheWarmer->isEnabled()) {
            $output->writeln('Cache warmer is disabled. To enable it go to Stores -> Configuration -> Kurufootwear -> Cache Warmer.');

            return;
        }

        // Catch CTRL-Z and CTRL-C
        pcntl_signal(SIGTSTP, [$this, 'stopExecution']);
        pcntl_signal(SIGINT, [$this, 'stopExecution']);

        if ($url = $input->getArgument('url')) {
            return $this->warmOneUrl($output, $url);
        }

        try {
            $startTime = microtime(true);
            $output->writeln('Cache warmer started work from CLI.');
            $output->writeln('<info> Started at ' . gmdate('H:i:s', $startTime) . ' GMT</info>');
            $this->logger->notice(__('Cache warmer started work from CLI.'));

            $this->cacheWarmer->warmCache(true);

            $finishTime =  microtime(true);
            $output->writeln('Cache warmer finished work from CLI.');
            $this->logger->notice(__('Cache warmer finished work from CLI.'));
            $output->writeln('<info> Finished at ' . gmdate('H:i:s',  $finishTime) . ' GMT</info>');
            $output->writeln('<info> Done in ' . gmdate('H:i:s', $finishTime - $startTime) . '</info>');
        } catch (AlreadyExistsException $e) {
            $output->writeln($e->getMessage());
            $this->logger->error($e->getMessage());
        }
    }

    /**
     * Reset the flag value if stopped from CLI using CTRL-Z / CTRL-C
     */
    public function stopExecution()
    {
        $this->cacheWarmer->setCacheWarmingFlag(0);

        exit(sprintf('Stopped cache warming %s', PHP_EOL));
    }

    /**
     * Warm one url
     *
     * @param OutputInterface $output
     * @param $url
     */
    private function warmOneUrl(OutputInterface $output, $url)
    {
        $output->writeln(sprintf('<info>Cache warmer started warming %s</info>', $url));

        $this->cacheWarmer->proceedCurlRequest($url, true);

        $output->writeln('<info>Cache warmer finished work from CLI.</info>');
    }
}