<?php
/**
 * @vendor Kurufootwear
 * @module Kurufootwear_CacheWarmer
 * @author Aleksejs Baranovs <info@scandiweb.com>
 * @copyright Copyright (c) 2018 KURU Footwear. All rights reserved.
 */

namespace Kurufootwear\CacheWarmer\Setup;

use Magento\Framework\DB\Ddl\Table;
use Magento\Framework\Setup\InstallSchemaInterface;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\SchemaSetupInterface;

/**
 * Class InstallSchema
 * @package Kurufootwear\CacheWarmer\Setup
 */
class InstallSchema implements InstallSchemaInterface
{
    /**
     * Install
     *
     * @param SchemaSetupInterface $setup
     * @param ModuleContextInterface $context
     */
    public function install(SchemaSetupInterface $setup, ModuleContextInterface $context)
    {
        $setup->startSetup();

        $this->installCacheWarmerTable($setup);

        $setup->endSetup();
    }

    /**
     * Install table with product list data
     *
     * @param $setup
     */
    private function installCacheWarmerTable($setup)
    {
        if (!$setup->tableExists($setup->getTable('cache_warmer'))) {
            $table = $setup->getConnection()->newTable(
                $setup->getTable('cache_warmer')
            )->addColumn(
                'entity_id',
                Table::TYPE_SMALLINT,
                null,
                ['identity' => true, 'unsigned' => true, 'nullable' => false, 'auto_increment' => true, 'primary' => true],
                'Entity ID'
            )->addColumn(
                'page_name',
                Table::TYPE_TEXT,
                255,
                ['nullable' => false],
                'Page Name'
            )->addColumn(
                'page_type',
                Table::TYPE_TEXT,
                255,
                ['nullable' => false],
                'Page Type'
            )->addColumn(
                'page_identifier',
                Table::TYPE_TEXT,
                255,
                ['nullable' => false],
                'Page Identifier'
            )->addColumn(
                'page_url',
                Table::TYPE_TEXT,
                512,
                ['nullable' => false],
                'Page URL'
            )->addColumn(
                'warm_up',
                Table::TYPE_SMALLINT,
                6,
                ['nullable' => false, 'default' => '1'],
                'Should be warmed up'
            )->addColumn(
                'source',
                Table::TYPE_TEXT,
                255,
                ['nullable' => false],
                'Source'
            );

            $setup->getConnection()->createTable($table);
        }
    }
}
