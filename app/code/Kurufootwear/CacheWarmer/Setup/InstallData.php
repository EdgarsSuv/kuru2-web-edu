<?php
/**
 * @vendor Kurufootwear
 * @module Kurufootwear_CacheWarmer
 * @author Aleksejs Baranovs <info@scandiweb.com>
 * @copyright Copyright (c) 2018 KURU Footwear. All rights reserved.
 */

namespace Kurufootwear\CacheWarmer\Setup;

use Kurufootwear\CacheWarmer\Helper\Data;
use Kurufootwear\CacheWarmer\Helper\Warmer;
use Magento\Framework\App\Config\Storage\WriterInterface;
use Magento\Framework\Setup\InstallDataInterface;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\ModuleDataSetupInterface;

/**
 * Class InstallData
 * @package Kurufootwear\CacheWarmer\Setup
 */
class InstallData implements InstallDataInterface
{
    /**
     * @var Data
     */
    private $cacheWarmerHelper;

    /**
     * @var WriterInterface
     */
    private $configWriter;

    /**
     * InstallData constructor.
     *
     * @param Data $cacheWarmerHelper
     * @param WriterInterface $configWriter
     */
    public function __construct(
        Data $cacheWarmerHelper,
        WriterInterface $configWriter
    ) {
        $this->cacheWarmerHelper = $cacheWarmerHelper;
        $this->configWriter = $configWriter;
    }

    /**
     * Install data
     *
     * @param ModuleDataSetupInterface $setup
     * @param ModuleContextInterface $context
     */
    public function install(
        ModuleDataSetupInterface $setup,
        ModuleContextInterface $context
    ) {
        $setup->startSetup();

        $this->cacheWarmerHelper->addBaseUrlData(Data::SOURCE_MIGRATION);
        $this->cacheWarmerHelper->addCacheWarmerData(Data::SOURCE_MIGRATION);

        // Save the flag initial value
        $this->configWriter->save(Warmer::CACHE_WARMING_PROGRESS_FLAG, 0);

        $setup->endSetup();
    }
}
