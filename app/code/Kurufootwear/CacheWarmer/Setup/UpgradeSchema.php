<?php
/**
 * @vendor Kurufootwear
 * @module Kurufootwear_CacheWarmer
 * @author Aleksejs Baranovs <info@scandiweb.com>
 * @copyright Copyright (c) 2018 KURU Footwear. All rights reserved.
 */

namespace Kurufootwear\CacheWarmer\Setup;

use Magento\Framework\Setup\UpgradeSchemaInterface;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\SchemaSetupInterface;
use Magento\Framework\DB\Ddl\Table;

/**
 * Class UpgradeSchema
 * @package Kurufootwear\CacheWarmer\Setup
 */
class UpgradeSchema implements UpgradeSchemaInterface
{
    /**
     * Upgrade schema
     *
     * @param SchemaSetupInterface $setup
     * @param ModuleContextInterface $context
     */
    public function upgrade(
        SchemaSetupInterface $setup,
        ModuleContextInterface $context
    ) {
        $setup->startSetup();

        if (version_compare($context->getVersion(), '0.1.1') < 0) {
            $this->addCacheWarmerStatusColumn($setup);
        }

        $setup->endSetup();
    }

    /**
     * Add cache warmer status column
     *
     * @param $setup
     */
    private function addCacheWarmerStatusColumn($setup)
    {
        // Get module table
        $tableName = $setup->getTable('cache_warmer');

        // Check if the table already exists
        if ($setup->getConnection()->isTableExists($tableName)) {
            $connection = $setup->getConnection();
            $connection->addColumn(
                $tableName,
                'status',
                [
                    'type' => Table::TYPE_BOOLEAN,
                    'nullable' => false,
                    'default' => '1',
                    'comment' => 'Status'
                ]
            );
        }
    }
}
