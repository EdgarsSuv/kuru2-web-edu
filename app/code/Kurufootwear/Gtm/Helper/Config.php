<?php

/**
 * @category     Kurufootwear
 * @package      Kurufootwear/Gtm
 * @copyright    Copyright (c) 2017 Scandiweb, Inc (http://scandiweb.com)
 * @license      http://opensource.org/licenses/OSL-3.0 The Open Software License 3.0 (OSL-3.0)
 */

namespace Kurufootwear\Gtm\Helper;

use Scandi\Gtm\Helper\Config as ScandiwebConfig;

class Config extends ScandiwebConfig
{

    /**
     * @return array
     */
    public function getVariableArray()
    {
        $colorName = $this->getColorVariable();
        $variables = [];
        if ($colorName) {
            $variables['color'] = $colorName;
        }
        $sizeName = $this->getSizeVariable();
        if ($sizeName) {
            $variables['g_size'] = $sizeName;
        }
        $variables['g_brand'] = 'brand';
        return $variables;
    }
}
