<?php
/**
 * @category     Kurufootwear
 * @package      Kurufootwear/Gtm
 * @copyright    Copyright (c) 2017 Scandiweb, Inc (http://scandiweb.com)
 * @license      http://opensource.org/licenses/OSL-3.0 The Open Software License 3.0 (OSL-3.0)
 */
namespace Kurufootwear\Gtm\Helper;

use Exception;
use Magento\Framework\Exception\NoSuchEntityException;
use Psr\Log\LoggerInterface;
use Scandi\Gtm\Helper\Configurable as ScandiwebConfigurable;
use Magento\Catalog\Model\Product\Interceptor;
use Magento\Catalog\Model\ProductRepository;
use Magento\ConfigurableProduct\Model\ResourceModel\Product\Type\Configurable as ConfigurableType;
use Magento\ConfigurableProduct\Model\Product\Type\Configurable as ConfigurableProduct;
use Scandi\Gtm\Helper\Collectors\Attributes;
use Magento\Catalog\Model\ProductFactory;
use Magento\Catalog\Model\ResourceModel\Product\CollectionFactory as ProductCollectionFactory;

/**
 * Class Configurable
 * @package Kurufootwear\Gtm\Helper
 */
class Configurable extends ScandiwebConfigurable
{
    /**
     * @var ProductCollectionFactory
     */
    protected $productCollectionFactory;

    /**
     * @var ProductFactory
     */
    protected $productFactory;

    /**
     * @var LoggerInterface
     */
    protected $logger;

    /**
     * Configurable constructor.
     *
     * @param ProductRepository $productRepository
     * @param ConfigurableType $configurableType
     * @param Config $config
     * @param Attributes $attributes
     * @param ProductCollectionFactory $productCollectionFactory
     * @param ProductFactory $productFactory
     * @param LoggerInterface $logger
     */
    public function __construct(
        ProductRepository $productRepository,
        ConfigurableType $configurableType,
        Config $config,
        Attributes $attributes,
        ProductCollectionFactory $productCollectionFactory,
        ProductFactory $productFactory,
        LoggerInterface $logger
    ) {
        $this->productCollectionFactory = $productCollectionFactory;
        $this->productFactory = $productFactory;
        $this->logger = $logger;

        parent::__construct(
            $productRepository,
            $configurableType,
            $config,
            $attributes
        );
    }

    /**
     * @param $product
     * @return array
     */
    public function getProductData($product)
    {
        $color = $this->getColor($product);
        $duplicate = $this->getDuplicateProduct($product, $color);
        $productData = [];
        $productData = $this->addAttributesToData($product, $productData);
        if ($color) {
            $productData['dimension1'] = $color;
            $productData['position'] = $this->getColorPosition($product, $color);
        }
        if ($duplicate) {
            $productData['id'] = $duplicate->getSku();
        }
        $productData['brand'] = $this->getBrandName($product);
        $productData['name'] = $this->getName($product, $color, $productData['brand']);
        $productData['price'] = number_format($product->getData("price_incl_tax"), 2);
        $productData['qty'] = (int)$product->getQty();

        if ($product->getTypeId() === ConfigurableProduct::TYPE_CODE || !$duplicate) {
            $productData['wide'] = $this->productIsWide($product);
        } else {
            $productData['wide'] = $this->productIsWide($duplicate);
        }

        return $productData;
    }

    /**
     * Return whether product is wide
     * @param $product
     * @return bool
     */
    public function productIsWide($product)
    {
        $isWide = false;
        $isConfigurable = $product->getTypeId() == ConfigurableProduct::TYPE_CODE;
        $isDuplicate = $product->getData('product_duplicate');

        // Brand should be master original name
        if ($isConfigurable && $isDuplicate) { // Duplicate -> Master
            try {
                $parentSku = $product->getData('parent_sku');
                $product = $this->productRepository->get($parentSku);
                $isWide = $product->getAttributeText('filterable_width') === 'Wide';
            } catch (NoSuchEntityException $exception) {
                // Parent_sku attribute is invalid
                if (empty($parentSku)) {
                    $parentSku = '-';
                }

                $this->logger->critical(sprintf(
                    "GTM Error: Duplicate product id #%s has invalid parent_sku attribute '%s', such SKU doesn't exist.",
                    $product->getId(),
                    $parentSku
                ));
            }
        } else if ($isConfigurable && !$isDuplicate) {
            $isWide = $product->getAttributeText('filterable_width') === 'Wide';
        } else if (!$isConfigurable) { // Simple -> Master
            try {
                $masterSku = substr($product->getSku(), 0, 4);
                $master = $this->productRepository->get($masterSku);

                if ($master->hasData()) {
                    $product = $master;
                }

                $isWide = $product->getResource()
                    ->getAttribute('filterable_width')
                    ->getFrontend()
                    ->getValue($product) === 'Wide';
            } catch (Exception $exception) {
                // It's not a configuration, but a simple product
                $isWide = false;
            }
        }

        return $isWide ? 'T' : 'F';
    }

    public function getBrandName($product)
    {
        $isConfigurable = $product->getTypeId() == ConfigurableProduct::TYPE_CODE;
        $isDuplicate = $product->getData('product_duplicate');

        // Brand should be master original name
        if ($isConfigurable && $isDuplicate) { // Duplicate -> Master
            $product = $this->productRepository->get($product->getData('parent_sku'));
        } else if (!$isConfigurable) { // Simple -> Master
            try {
                $masterSku = substr($product->getSku(), 0, 4);
                $master = $this->productRepository->get($masterSku);

                if ($master->hasData()) {
                    $product = $master;
                }
            } catch (Exception $exception) {
                // Master product doesn't exist,
                // continue with current product
            }
        }

        $populatedProduct = $this->productRepository->get($product->getSku());

        if ($brand = $product->getData('orig_name')) {
            return $brand;
        } else if ($brand = $populatedProduct->getData('orig_name')) {
            return $brand;
        } else {
            return $this->config->getBrand();
        }
    }

    public function getMasterFromProduct($product)
    {
        try {
            $masterSku = substr($product->getSku(), 0, 4);
            $master = $this->productRepository->get($masterSku);

            if ($master->hasData()) {
                return $master;
            } else {
                return null;
            }
        } catch (Exception $exception) {
            return null;
        }
    }

    public function getColorPosition($product, $currentColor = null)
    {
        $firstPosition = 1;
        $position = $firstPosition;
        $master = $this->getMasterFromProduct($product);
        $typeInstance = $product->getTypeInstance();

        if (is_null($typeInstance) || is_null($master)) {
            return 1;
        }

        $colorProducts = $typeInstance->getSalableUsedProducts($master, null);

        if (is_null($currentColor)) {
            return $position;
        } else {
            foreach ($colorProducts as $colorProduct) {
                $color = $colorProduct->getAttributeText('color');

                if ($color == $currentColor) {
                    return $position;
                }

                if (!isset($previousColors)) {
                    $position++;
                    $previousColors = [$color];
                }

                if (!in_array($color, $previousColors)) {
                    $position++;
                    $previousColors[] = $color;
                }
            }
        }

        // Fallback return
        return $firstPosition;
    }

    public function getColor($product)
    {
        $isConfigurable = $product->getTypeId() == ConfigurableProduct::TYPE_CODE;
        $isDuplicate = $product->getData('product_duplicate');

        // Name should be master *name* - *color*
        if ($isConfigurable && !$isDuplicate) { // Master -> Duplicate
            $color = $product->getData('product_color_data');
            $duplicate = $this->getDuplicateProduct($product, $color);
            if ($duplicate) {
                $product = $duplicate;
            }
        } else if (!$isConfigurable) { // Simple -> Duplicate
            $simpleSku = $product->getSku();
            $skuDashPosition = strpos($simpleSku, '-');
            if ($skuDashPosition !== false) {
                $duplicateSku = substr($simpleSku, 0, $skuDashPosition);
                $duplicate = $this->productRepository->get($duplicateSku);

                if ($duplicate->hasData()) {
                    $product = $duplicate;
                }
            }
        }

        return $product->getData('product_color_data');
    }

    public function getName($product, $color, $brand)
    {
        if ($color && $brand) {
            return $brand .  ' - ' . $color;
        } else {
            return $product->getName();

        }
    }

    /**
     * @param $product
     * @param array $productData
     * @return array|null
     */
    public function addAttributesToData($product, array $productData)
    {
        if (!$this->isConfigurable($product)) {
            return $productData;
        }
        $this->getChildAndParent($product);
        foreach ($this->config->getVariableArray() as $key => $value) {
            if ($this->child->getData($key)) {
                $product = $this->retrieveAttribute($key, $product);
                $productData[$value] = $product->getData($key);
            }
        }
        $productData['id'] = $this->parent->getSku();
        if ($productData['id'] !== $this->child->getSku()) {
            $productData['id'] = substr($this->child->getSku(), 0, 6);
            $productData[$this->config->getChildSkuVariable()] = $this->child->getSku();
        }
        return $productData;
    }

    /**
     * @param $product
     * @return array
     */
    public function getDuplicateProducts($product)
    {
        $associatedProducts = $this->getChildAndParent($product);

        $duplicates = [];

        if (array_key_exists('parents', $associatedProducts)) {
            foreach ($associatedProducts['parents'] as $parentProduct) {
                if ($parentProduct->getData('product_duplicate')) {
                    $duplicates[] = $parentProduct;
                }
            }
        }

        if (empty($duplicates)) {
            /**
             * Backwards compatibility for Impressions
             * Master products have no direct link
             * Only way to find all duplicates is by
             * checking all childrens parents.
             */
            if (strpos($product->getSku(), 'E') !== false) {
                $masterSku = substr($product->getSku(), 0, 5);
            } else {
                $masterSku = substr($product->getSku(), 0, 4);
            }

            /** @var ProductModel $duplicateProduct */
            $duplicates = $this->productCollectionFactory->create()
                ->addAttributeToSelect('*')
                ->addAttributeToFilter('parent_sku', $masterSku)
                ->addFieldToFilter('type_id', 'configurable')
                ->getItems();
        }

        return $duplicates;
    }

    /**
     * Get a duplicate product from master SKU & color
     * @param $product
     * @param $color
     * @return mixed|null
     */
    public function getDuplicateProduct($product, $color)
    {
        $duplicateProducts = $this->getDuplicateProducts($product);

        foreach ($duplicateProducts as $duplicateProduct) {
            if ($duplicateProduct->getData('product_color_data') == $color) {
                return $duplicateProduct;
            }
        }

        return null;
    }

    /**
     * @param $product
     * @return array
     */
    public function getChildAndParent($product)
    {
        $this->child = $this->productRepository->get($product->getSku());
        $parentsByChild = $this->configurableType->getParentIdsByChild($this->child->getEntityId());

        if (sizeof($parentsByChild) === 0) {
            $this->parent = $product;

            return [
                'parents' => [$product]
            ];
        }

        /**
         * Handle simple product
         */
        $parents = $this->productCollectionFactory->create()
            ->addAttributeToSelect('*')
            ->addFieldToFilter('entity_id', $parentsByChild);

        $this->parent = $parents->getFirstItem();

        return [
            'child' => $this->child,
            'parents' => $parents->getItems()
        ];
    }
}
