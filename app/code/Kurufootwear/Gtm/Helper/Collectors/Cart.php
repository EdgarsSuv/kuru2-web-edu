<?php
/**
 * @category     Kurufootwear
 * @package      Kurufootwear/Gtm
 * @copyright    Copyright (c) 2017 Scandiweb, Inc (http://scandiweb.com)
 * @license      http://opensource.org/licenses/OSL-3.0 The Open Software License 3.0 (OSL-3.0)
 */
namespace Kurufootwear\Gtm\Helper\Collectors;

use Magento\Catalog\Model\ProductRepository;
use Magento\Checkout\Model\Session;
use Kurufootwear\Gtm\Helper\Configurable;

class Cart extends \Scandi\Gtm\Helper\Collectors\Cart
{
    /**
     * @var Category
     */
    protected $category;

    /**
     * @var Configurable
     */
    protected $configurable;

    /**
     * Cart constructor.
     * @param Session $checkoutSession
     * @param ProductRepository $productRepository
     * @param Configurable $configurable
     * @param Category $category
     */
    public function __construct(
        Session $checkoutSession,
        ProductRepository $productRepository,
        Configurable $configurable,
        Category $category
    )
    {
        $this->category = $category;
        $this->configurable = $configurable;

        parent::__construct(
            $checkoutSession,
            $productRepository,
            $configurable
        );
    }

    /**
     * @return null
     */
    public function collectCart()
    {
        if ($this->quote->getItemsQty() == 0) {
            return null;
        }

        //Value will include discount if any exist
        $cartData["total"] = number_format($this->quote->getGrandTotal(), 2);
        $cartData["quantity"] = (int)$this->quote->getItemsQty();
        $cartData["quantity"] = $cartData['quantity'];
        $cartData["products"] = $this->collectProducts($this->quote);
        return $cartData;
    }

    /**
     * @param $quote
     * @return array|bool
     */
    public function collectProducts($quote)
    {
        foreach ($quote->getAllItems() as $index => $product) {
            if ($product->getData("price_incl_tax") == 0) {
                continue;
            }

            $productData = [
                "id" => $product->getSku(),
                "name" => $product->getName(),
                "category" => $this->category->getCategoryName($product),
                "price" => number_format($product->getData("price_incl_tax"), 2),
                "quantity" => (string)$product->getQty(),
                "brand" => $this->configurable->config->getBrand()
            ];
            $extraData = $this->configurable->getProductData($product);
            $productsData[] = array_merge($productData, $extraData);
        }

        return isset($productsData) ? $productsData : false;
    }
}
