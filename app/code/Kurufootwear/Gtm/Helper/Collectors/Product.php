<?php
/**
 * @category     Kurufootwear
 * @package      Kurufootwear/Gtm
 * @copyright    Copyright (c) 2017 Scandiweb, Inc (http://scandiweb.com)
 * @license      http://opensource.org/licenses/OSL-3.0 The Open Software License 3.0 (OSL-3.0)
 */
namespace Kurufootwear\Gtm\Helper\Collectors;

use Magento\Catalog\Model\Product as ProductModel;
use Scandi\Gtm\Helper\Collectors\Category;
use Magento\Catalog\Block\Product\View;
use Magento\Framework\Json\Helper\Data;
use Magento\Framework\Registry;
use Scandi\Gtm\Helper\Config;
use Kurufootwear\Gtm\Helper\Configurable;
use Magento\Catalog\Model\ProductRepository;
use Magento\Catalog\Model\ProductFactory;
use Magento\Catalog\Model\ResourceModel\Product\CollectionFactory as ProductCollectionFactory;

class Product extends \Scandi\Gtm\Helper\Collectors\Product
{
    /**
     * @var ProductCollectionFactory
     */
    protected $productCollectionFactory;

    /**
     * @var ProductCollectionFactory
     */
    protected $productFactory;

    /**
     * Product constructor.
     * @param View $view
     * @param Registry $registry
     * @param Category $category
     * @param ProductRepository $productRepository
     * @param Config $config
     * @param Data $jsonHelper
     * @param Configurable $configurable
     * @param ProductCollectionFactory $productCollectionFactory
     * @param ProductFactory $productFactory
     */
    public function __construct(
        View $view,
        Registry $registry,
        Category $category,
        ProductRepository $productRepository,
        Config $config,
        Data $jsonHelper,
        Configurable $configurable,
        ProductCollectionFactory $productCollectionFactory,
        ProductFactory $productFactory
    )
    {
        $this->productCollectionFactory = $productCollectionFactory;
        $this->productFactory = $productFactory;
        $this->configurable = $configurable;

        parent::__construct(
            $view,
            $registry,
            $category,
            $productRepository,
            $config,
            $jsonHelper,
            $configurable
        );
    }

    /**
     * @return mixed
     */
    public function createDetails()
    {
        $product = $this->view->getProduct();
        $request = $this->view->getRequest();

        if (!empty($color = $request->getParam('color'))) {
            if (!empty($duplicate = $this->configurable->getDuplicateProduct($product, $color))) {
                // If a preselected color is in url
                // And current (master) product has a matching
                // duplicate with such color, then set it as the product
                $product = $duplicate;
            }
        }

        return $this->collectProductData($product, 'product');
    }

    /**
     * Create impressions from non-preselected configurable options
     */
    public function createImpressions()
    {
        $product = $this->view->getProduct(); // Master
        $request = $this->view->getRequest();
        $duplicates = $this->configurable->getDuplicateProducts($product);
        $price = number_format($product->getFinalPrice(), 2);

        if (empty($color = $request->getParam('color'))) {
            return null;
        }

        $impressions = [];
        foreach ($duplicates as $duplicate) {
            $loadedDuplicate = $this->productRepository->get($duplicate->getSku());

            if ($loadedDuplicate->getData('product_color_data') == $color) {
                continue;
            }

            $productData = $this->collectProductData($loadedDuplicate);
            if (empty($productData['price']) || (int)$productData['price'] === 0) {
                $productData['price'] = $price;
            }
            $impressions[] = $productData;
        }

        return $impressions;
    }

    /**
     * @param ProductModel $product
     * @return int
     */
    public function getDuplicatePrice($product)
    {
        $simpleProduct = $this->productCollectionFactory->create()
            ->addAttributeToSelect('*')
            ->addAttributeToFilter('sku', $product->getSku() . '-*')
            ->getFirstItem();

        return $this->productRepository->getById($simpleProduct->getId())->getFinalPrice();
    }

    /**
     * @param $product
     * @param null $pageType
     * @return string | array
     */
    public function collectProductData($product, $pageType = null)
    {
        $productData['id'] = $product->getSku();
        $productData['category'] = $this->category->getCategoryName($product);
        $productData['currencyCode'] = $this->config->getStoreCurrency();
        $productData['price'] = $this->config->price->collectProductPrice($product);

        if ($product->getQty()) {
            $productData['quantity'] = (int)$product->getQty();
        }

        $productData = $this->configurable->addAttributesToData($product, $productData);
        $extraData = $this->configurable->getProductData($product);
        $productData['brand'] = $extraData['brand'];
        $productData['name'] = $extraData['name'];

        if (array_key_exists('dimension1', $extraData)) {
            $productData['dimension1'] = $extraData['dimension1'];
        }

        if (array_key_exists('id', $extraData)) {
            $productData['id'] = $extraData['id'];
        }

        $productData['wide'] = $extraData['wide'];

        if (array_key_exists('position', $extraData)) {
            $productData['position'] = $extraData['position'];
            $productData['list'] = 'PDP_ColorSwatch';
        }

        if ($pageType !== 'product') {
            return $productData;
        }

        return $this->handleDetailsPush($productData);
    }

    /**
     * Generate javascript dataLayer push function for impressions data
     * @param $productsDetails
     * @return string
     */
    private function handleImpressionsPush($productsDetails)
    {
        $push['event'] = 'impressions';
        $push['ecommerce']['impressions']['products'] = array($productsDetails);
        return "dataLayer.push(" . $this->jsonHelper->jsonEncode($push) . ");";
    }

    /**
     * Generate javascript dataLayer push function for details data
     * @param $productsDetails
     * @return string
     */
    private function handleDetailsPush($productsDetails)
    {
        $push['event'] = 'detail';
        $push['ecommerce']['detail']['products'] = array($productsDetails);
        return "dataLayer.push(" . $this->jsonHelper->jsonEncode($push) . ");";
    }
}
