<?php
/**
 * @category     Kurufootwear
 * @package      Kurufootwear/Gtm
 * @copyright    Copyright (c) 2017 Scandiweb, Inc (http://scandiweb.com)
 * @license      http://opensource.org/licenses/OSL-3.0 The Open Software License 3.0 (OSL-3.0)
 */
namespace Kurufootwear\Gtm\Helper\Collectors;

use Scandi\Gtm\Helper\Collectors\Event as ScandiEvent;
use Magento\Customer\Model\Session;
use Scandi\Gtm\Helper\Collectors\Product as ScandiGTMProduct;
use Kurufootwear\Gtm\Helper\Collectors\Product as GTMProduct;
use Scandi\Gtm\Helper\Collectors\Search;
use Scandi\Gtm\Helper\Collectors\Checkout;

/**
 * Class Event
 * @package Scandi\Gtm\Helper\Collectors
 */
class Event extends ScandiEvent
{
    /**
     * @var Product
     */
    protected $product;

    /**
     * Event constructor.
     * @param Session $customerSession
     * @param Category $category
     * @param Search $search
     * @param Checkout $checkout
     * @param ScandiGTMProduct $scandiProduct
     * @param Product $product
     * @param Success $success
     */
    public function __construct(
        Session $customerSession,
        Category $category,
        Search $search,
        Checkout $checkout,
        ScandiGTMProduct $scandiProduct,
        GTMProduct $product,
        Success $success
    )
    {
        $this->product = $product;

        parent::__construct(
            $customerSession,
            $category,
            $search,
            $checkout,
            $scandiProduct,
            $success
        );
    }

    /**
     * Method to handle all possible pushes
     * @param null $pageName
     * @return string
     */
    public function gatherPushes($pageName = null)
    {
        $pushes = [];
        $pushes[] = "dataLayer.push(" . $this->getEventPushData() . ")";
        switch ($pageName) {
            case 'category':
                $impressionsPush = $this->category->createImpressions();
                break;
            case 'search_result':
                $impressionsPush = $this->search->createImpressions();
                $keywordPush = $this->search->getKeyWordPush();
                break;
            case 'checkout':
                $pushes[] = $this->checkout->getCheckoutSteps();
                $pushes[] = $this->checkout->getOptionWrappers();
                $pushes[] = $this->checkout->getCart();
                break;
            case 'product':
                $impressionsPush = $this->product->createImpressions();
                $pushes[] = $this->product->createDetails();
                break;
            case 'success':
                $pushes[] = $this->success->collectSuccess();
                break;
            default:
                return json_encode($pushes);
        }

        if (isset($keywordPush)) {
            $pushes[] = $keywordPush;
        }
        if (isset($impressionsPush)) {
            foreach($this->handleImpressions($impressionsPush) as $impressionPush) {
                $pushes[] = $impressionPush;
            }
        }
        return json_encode($pushes);
    }

    /**
     * @return string
     */
    public function getEventPushData()
    {
        $data = '';

        if ($this->customerSession->getAddToCart()) {
            $data .= $this->customerSession->getAddToCart();
            $this->customerSession->unsAddToCart();
        }

        if ($this->customerSession->getRemoveFromCart()) {
            if ($data !== '') { // Add seperator if add to cart event exists
                $data .= ', ';
            }
            $removePush = $this->customerSession->getRemoveFromCart();
            $this->customerSession->unsRemoveFromCart();
            $data .= $removePush;
        }

        return $data;
    }
}
