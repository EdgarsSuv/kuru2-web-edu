<?php
/**
 * @category     Kurufootwear
 * @package      Kurufootwear/Gtm
 * @copyright    Copyright (c) 2017 Scandiweb, Inc (http://scandiweb.com)
 * @license      http://opensource.org/licenses/OSL-3.0 The Open Software License 3.0 (OSL-3.0)
 */
namespace Kurufootwear\Gtm\Helper\Collectors;

use Magento\Sales\Model\Order;
use Scandi\Gtm\Helper\Configurable;

class Success extends \Scandi\Gtm\Helper\Collectors\Success
{
    /**
     * @param Order $order
     * @return array|bool
     */
    public function gatherProducts($order)
    {
        $brand = $this->category->config->getBrand();
        foreach ($order->getAllItems() as $product) {

            if ((int)$product->getPrice() == 0) {
                continue;
            }
            $productData = [
                "name" => $product->getName(),
                "id" => $product->getSku(),
                "price" => $product->getPrice(),
                "category" => $this->category->getCategoryName($product),
                "quantity" => (int)$product->getQtyOrdered(),
                "brand" => $brand,
                "affiliate" => "www.kurufootwear.com"
            ];
            if ($product->getProductType() === Configurable::CONFIGURABLE_TYPE_ID) {
                $productData = $this->configurable->addAttributesToData($product, $productData);
                $extraData = $this->configurable->getProductData($product);
                $productData['name'] = $extraData['name'];
                $productData['brand'] = $extraData['brand'];
                $productData['wide'] = $extraData['wide'];
            }
            $productsData[] = $productData;
        }
        return isset($productsData) ? $productsData : false;
    }
}
