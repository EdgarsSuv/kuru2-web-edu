<?php
/**
 * @category     Kurufootwear
 * @package      Kurufootwear/Gtm
 * @copyright    Copyright (c) 2017 Scandiweb, Inc (http://scandiweb.com)
 * @license      http://opensource.org/licenses/OSL-3.0 The Open Software License 3.0 (OSL-3.0)
 */
namespace Kurufootwear\Gtm\Helper\Collectors;

use Magento\Catalog\Block\Product\ListProduct;
use Magento\Catalog\Model\CategoryRepository;
use Magento\Catalog\Model\ProductRepository;
use Magento\Framework\App\Request\Http;
use Magento\Framework\Exception\NoSuchEntityException;
use Magento\Framework\Registry;
use Magento\Theme\Block\Html\Pager;
use Psr\Log\LoggerInterface;
use Scandi\Gtm\Helper\Collectors\Category as ScandiCategory;
use Scandi\Gtm\Helper\Config;
use Kurufootwear\Gtm\Helper\Configurable as ConfigurableHelper;
use Scandi\Gtm\Helper\Price;

/**
 * Class Category
 * @package Kurufootwear\Gtm\Helper\Collectors\Category
 */
class Category extends ScandiCategory
{
    /**
     * @var Http
     */
    protected $request;

    /**
     * @var ConfigurableHelper
     */
    protected $configurableHelper;

    /**
     * @var ProductRepository
     */
    protected $productRepository;

    /**
     * @var LoggerInterface
     */
    protected $logger;

    /**
     * @var array
     */
    private $cachedCategories = null;

    /**
     * Category constructor.
     * @param ListProduct $listProduct
     * @param Registry $registry
     * @param Pager $pager
     * @param CategoryRepository $category
     * @param Config $config
     * @param Price $price
     * @param Http $request
     * @param ConfigurableHelper $configurableHelper
     * @param ProductRepository $productRepository
     * @param LoggerInterface $logger
     */
    public function __construct(
        ListProduct $listProduct,
        Registry $registry,
        Pager $pager,
        CategoryRepository $category,
        Config $config,
        Price $price,
        Http $request,
        ConfigurableHelper $configurableHelper,
        ProductRepository $productRepository,
        LoggerInterface $logger
    )
    {
        $this->request = $request;
        $this->configurableHelper = $configurableHelper;
        $this->productRepository = $productRepository;
        $this->logger = $logger;

        parent::__construct(
            $listProduct,
            $registry,
            $pager,
            $category,
            $config,
            $price
        );
    }

    /**
     * @return array|null
     */
    public function createImpressions()
    {
        $requestPath = $this->request->getRequestString();
        $pagePath = str_replace('.html', '', $requestPath);
        $products = $this->listProduct->getLoadedProductCollection();
        $products->setPageSize($this->config->getImpressionsMaximum())
            ->setCurPage($this->pager->getCurrentPage());
        if (count($products->getAllIds()) === 0) {
            return null;
        }
        $i = 1;
        foreach ($products as $product) {
            /* @var \Magento\Catalog\Model\Product $product */
            $productData = [
                "id" => $product->getSku(),
                "name" => $product->getName(),
                "price" => $this->price->collectProductPrice($product),
                "category" => $this->getCategoryName($product),
                "position" => (int)$i,
                "list" => $pagePath
            ];

            if ($color = $product->getData('product_color_data')) {
                $productData['dimension1'] = $color;
            }

            $extraData = $this->configurableHelper->getProductData($product);
            $productData['brand'] = $extraData['brand'];
            $productData['name'] = $extraData['name'];
            $productData['wide'] = $extraData['wide'];

            $impressions[] = $productData;

            $i++;
        }
        return isset($impressions) ? $impressions : null;
    }

    /**
     * Returns a string containing the categories a product belongs to formatted specifically for
     * Google Tag Manager. The format will have 5 categories separated by slashes. The 5 categories are
     *
     *      Men or Women
     *      New
     *      Outdoor
     *      Athletic
     *      Casual
     *
     * So for example: "Women/New/Outdoor/Athletic/Casual"
     *
     * If the product does not fall into one of the categories then it is given the value "null".
     * For example: "Men/null/null/null/Casual"
     *
     * @param $product
     * @return bool|string
     */
    public function getCategoryName($product)
    {
        $isConfigurable = $product->getProductType() === ConfigurableHelper::CONFIGURABLE_TYPE_ID;
        $isDuplicate = $product->getData('product_duplicate');
        if ($isConfigurable && !$isDuplicate) {
            $color = $product->getData('color');
            $duplicate = $this->configurableHelper->getDuplicateProduct($product, $color);
            if ($duplicate) {
                $product = $duplicate;
            }
        } else if (!$isConfigurable) {
            $simpleSku = $product->getSku();
            $skuDashPosition = strpos($simpleSku, '-');
            if ($skuDashPosition !== false) {
                $duplicateSku = substr($simpleSku, 0, $skuDashPosition);
                try {
                    $duplicate = $this->productRepository->get($duplicateSku);

                    if ($duplicate->hasData()) {
                        $product = $duplicate;
                    }
                } catch (NoSuchEntityException $exception) {
                    $this->logger->error("Missing duplicate product in GTM category helper." . $exception->getMessage());
                }
            }
        }
        $ids = $this->productRepository->get($product->getSku())->getCategoryIds();

        // Detect main category
        $isMens = false;
        $isWomens = false;

        if ($this->categoriesContainUrlKey('mens-shoes', $ids) ||
            $this->categoriesContainUrlKey('discontinued-mens-shoes', $ids)) {
            $isMens = true;
        }

        if ($this->categoriesContainUrlKey('womens-shoes', $ids) ||
            $this->categoriesContainUrlKey('discontinued-womens-shoes', $ids)) {
            $isWomens = true;
        }

        // Start collecting category string
        // GTM cares about only 5 main categories,
        // so these are hardcoded by urlkeys
        $separator = '/';
        $nullName = 'null';

        if ($isMens && $isWomens) {
            $category = 'Men and Women';
        } else if ($isMens) {
            $category = 'Men';
        } else if ($isWomens) {
            $category = 'Women';
        } else {
            $category = 'Other';
        }

        $category .= $this->categoryAppendString(['womens-new-arrivals', 'mens-new-arrivals'], 'New', $ids, $separator, $nullName);
        $category .= $this->categoryAppendString(['womens-outdoor-shoes', 'mens-outdoor-shoes'], 'Outdoor', $ids, $separator, $nullName);
        $category .= $this->categoryAppendString(['womens-athletic-shoes', 'mens-athletic-shoes'], 'Athletic', $ids, $separator, $nullName);
        $category .= $this->categoryAppendString(['womens-casual-shoes', 'mens-casual-shoes'], 'Casual', $ids, $separator, $nullName);

        return $category;
    }

    /**
     * Generate substring appendable to category collection string
     *
     * @param $urlKeys
     * @param $label
     * @param $categoryIds
     * @param $separator
     * @param $nullName
     * @return string
     */
    private function categoryAppendString($urlKeys, $label, $categoryIds, $separator, $nullName)
    {
        $append = $nullName;

        if (is_array($urlKeys)) {
            foreach ($urlKeys as $urlKey) {
                if ($this->categoriesContainUrlKey($urlKey, $categoryIds)) {
                    $append = $label;
                }
            }
        } else {
            $urlKey = $urlKeys;
            $append = $this->categoriesContainUrlKey($urlKey, $categoryIds) ? $label : $nullName;
        }

        return $separator . $append;
    }

    /**
     * Return whether list of category ids contains
     * an id of a category with the matching urlkey(s)
     *
     * @param $urlKey
     * @param $ids
     * @return bool|\Magento\Catalog\Model\Category
     */
    private function categoriesContainUrlKey($urlKey, $ids)
    {
        foreach ($ids as $categoryId) {
            $category = $this->category->get($categoryId);

            if ($category->getUrlKey() == $urlKey) {
                return $category;
            }
        }

        return false;
    }
}
