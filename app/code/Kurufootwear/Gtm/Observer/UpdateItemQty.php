<?php
/**
 * @category     Kurufootwear
 * @package      Kurufootwear/Gtm
 * @copyright    Copyright (c) 2017 Scandiweb, Inc (http://scandiweb.com)
 * @license      http://opensource.org/licenses/OSL-3.0 The Open Software License 3.0 (OSL-3.0)
 */
namespace Kurufootwear\Gtm\Observer;

use Magento\Customer\Model\Session as CustomerSession;
use Magento\Framework\App\RequestInterface;
use Magento\Framework\DataObject;
use Magento\Framework\Event\Observer;
use Magento\Framework\Event\ObserverInterface;
use Scandi\Gtm\Helper\Collectors\Attributes;
use Scandi\Gtm\Helper\Collectors\Product;
use Scandi\Gtm\Helper\Config;

/**
 * Class AddToCart
 * @package Scandi\Gtm\Observer
 */
class UpdateItemQty implements ObserverInterface
{

    /**
     * @var CustomerSession
     */
    protected $customerSession;

    /**
     * @var Product
     */
    protected $product;

    /**
     * @var Config
     */
    protected $config;

    /**
     * @var Attributes
     */
    protected $attributes;

    /**
     * @var RequestInterface
     */
    protected $request;

    /**
     * UpdateItemQty constructor.
     * @param CustomerSession $customerSession
     * @param Product $product
     * @param Config $config
     * @param Attributes $attributes
     * @param RequestInterface $request
     */
    public function __construct(
        CustomerSession $customerSession,
        Product $product,
        Config $config,
        Attributes $attributes,
        RequestInterface $request
    )
    {
        $this->request = $request;
        $this->customerSession = $customerSession;
        $this->product = $product;
        $this->config = $config;
        $this->attributes = $attributes;
    }

    /**
     * @param Observer $observer
     * @throws \Exception
     */
    public function execute(Observer $observer)
    {
        if ($this->config->isEnabled()) {
            if (!$this->request->isAjax()) {
                // Supposing only one product update at a time

                /* @var \Magento\Checkout\Model\Cart $cart */
                /* @var DataObject $info */
                $cart = $observer->getCart();
                $info = $observer->getInfo();

                foreach ($info->getData() as $productId => $productData) {

                    /* @var \Magento\Quote\Model\Quote\Item $item */
                    $item = $cart->getItems()->getItemById($productId);
                    $product = $item->getProduct();

                    $oldQty = (int)$item->getOrigData('qty');
                    $newQty = (int)$productData['qty'];

                    $diffQty = $newQty - $oldQty;
                    if ($diffQty > 0) {
                        $eventName = 'addToCart';
                        $ecommerceVariable = 'add';
                    } else if ($diffQty < 0) {
                        $eventName = 'removeFromCart';
                        $ecommerceVariable = 'remove';
                    } else {
                        // Sometimes info object has items
                        // with no quantity change, this observer
                        // doesn't care about them, continue to next item
                        continue;
                    }

                    $content = $this->product->collectCartEvent($product, $eventName);
                    $content["ecommerce"][$ecommerceVariable]["products"][0]['quantity'] = abs($diffQty);

                    // Depending on quantity being positive / negative
                    // and the customer session (not) having a previous event,
                    // add or append event to "addToCart" or "removeFromCart" session variable
                    if ($diffQty >= 0) {
                        $oldEvent = $this->customerSession->getAddToCart();
                        if (empty($oldEvent)) {
                            $this->customerSession->setAddToCart(json_encode($content));
                        } else {
                            $this->customerSession->setAddToCart($oldEvent . ', ' . json_encode($content));
                        }
                    } else {
                        $oldEvent = $this->customerSession->getRemoveFromCart();
                        if (empty($oldEvent)) {
                            $this->customerSession->setRemoveFromCart(json_encode($content));
                        } else {
                            $this->customerSession->setRemoveFromCart($oldEvent . ', ' . json_encode($content));
                        }
                    }
                }
            }
        }
    }
}
