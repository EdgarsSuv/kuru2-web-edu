<?php
/**
 * @category     Kurufootwear
 * @package      Kurufootwear/Gtm
 * @copyright    Copyright (c) 2017 Scandiweb, Inc (http://scandiweb.com)
 * @license      http://opensource.org/licenses/OSL-3.0 The Open Software License 3.0 (OSL-3.0)
 */
namespace Kurufootwear\Gtm\Block;

use Kurufootwear\Gtm\Helper\Collectors\Success;
use Magento\Checkout\Model\Session as CheckoutSession;
use Magento\Customer\Model\Session;
use Magento\Framework\App\Request\Http;
use Magento\Framework\View\Element\Template\Context;
use Scandi\Gtm\Block\DataLayer as ScandiDataLayer;
use Scandi\Gtm\Helper\Customer;
use Scandi\Gtm\Helper\Collectors\Cart;
use Scandi\Gtm\Helper\Collectors\Event;
use Scandi\Gtm\Helper\Name;
use Magento\Framework\Json\Helper\Data;

/**
 * Class DataLayer
 * @package Scandi\Gtm\Block
 */
class DataLayer extends ScandiDataLayer
{
    /**
     * @var Http
     */
    protected $request;

    /**
     * @var Name
     */
    protected $nameHelper;

    /**
     * @var Session
     */
    protected $customerSession;

    /**
     * @var Customer
     */
    protected $customerHelper;

    /**
     * @var Cart
     */
    protected $cart;

    /**
     * @var Event
     */
    protected $event;

    /**
     * @var Data
     */
    protected $jsonHelper;

    /**
     * @var CheckoutSession
     */
    protected $checkoutSession;

    /**
     * @var Success
     */
    protected $checkoutSuccess;

    /**
     * DataLayer constructor.
     * @param Context $context
     * @param Http $request
     * @param Name $nameHelper
     * @param Session $customerSession
     * @param Customer $customerHelper
     * @param Cart $cart
     * @param Event $event
     * @param Data $jsonHelper
     * @param CheckoutSession $checkoutSession
     * @param Success $checkoutSuccess
     */
    public function __construct(
        Context $context,
        Http $request,
        Name $nameHelper,
        Session $customerSession,
        Customer $customerHelper,
        Cart $cart,
        Event $event,
        Data $jsonHelper,
        CheckoutSession $checkoutSession,
        Success $checkoutSuccess
    )
    {
        $this->request = $request;
        $this->nameHelper = $nameHelper;
        $this->customerSession = $customerSession;
        $this->customerHelper = $customerHelper;
        $this->cart = $cart;
        $this->event = $event;
        $this->jsonHelper = $jsonHelper;
        $this->checkoutSession = $checkoutSession;
        $this->checkoutSuccess = $checkoutSuccess;

        parent::__construct(
            $context,
            $request,
            $nameHelper,
            $customerSession,
            $customerHelper,
            $cart,
            $event
        );
    }

    /**
     * @return null|string
     */
    public function getPurchaseSuccess()
    {
        if (!$this->isCheckoutSuccess()) {
            return null;
        }

        $lastOrder = $this->checkoutSession->getLastRealOrder();
        if (!$lastOrder->getId()) {
            return null;
        }

        $couponCode = $lastOrder->getCouponCode();
        $purchaseSuccess = [
            'event' => 'transaction',
            'ecommerce' => [
                'currencyCode' => $lastOrder->getBaseCurrencyCode(),
                'purchase' => [
                    'actionField' => [
                        'action' => 'purchase',
                        'coupon_code' => $couponCode ? $couponCode : '',
                        'coupon_discount_amount' => $lastOrder->getDiscountAmount(),
                        'coupon_discount_amount_abs' => abs($lastOrder->getDiscountAmount()),
                        'id' => $lastOrder->getIncrementId(),
                        'revenue' => $lastOrder->getGrandTotal(),
                        'shipping' => $lastOrder->getShippingAmount(),
                        'tax' => $lastOrder->getTaxAmount()
                    ],
                    'products' => $this->checkoutSuccess->gatherProducts($lastOrder)
                ]
            ],
        ];

        return $this->jsonHelper->jsonEncode($purchaseSuccess);
    }

    /**
     * @return bool
     */
    public function isCheckoutSuccess()
    {
        $routeName        = $this->request->getRouteName();
        $controllerName   = $this->request->getControllerName();
        $actionName       = $this->request->getActionName();

        $currentPath = $routeName . '/' . $controllerName . '/' . $actionName;
        $cartPath = 'checkout/onepage/success';

        return $currentPath == $cartPath;
    }

    /**
     * Fail-safe'ing GTM pushes
     *
     * @return string
     */
    public function gatherPushes()
    {
        try {
            return parent::gatherPushes();
        } catch (\Exception $exception) {
            $this->_logger->error($exception->getMessage());
        }
    }
}
