<?php
/**
 * @category     Kurufootwear
 * @package      Kurufootwear/Gtm
 * @copyright    Copyright (c) 2017 Scandiweb, Inc (http://scandiweb.com)
 * @license      http://opensource.org/licenses/OSL-3.0 The Open Software License 3.0 (OSL-3.0)
 */
namespace Kurufootwear\Gtm\Block;

use Magento\Framework\View\Element\Template\Context;
use Kurufootwear\Gtm\Block\DataLayer;
use Scandi\Gtm\Block\Gtm as ScandiGtm;
use Scandi\Gtm\Helper\Config;
use Scandi\Gtm\Helper\Script;
use Magento\Framework\App\Request\Http;
use Magento\Theme\Block\Html\Header\Logo;

/**
 * Class Gtm
 * @package Scandi\Gtm\Block
 */
class Gtm extends ScandiGtm
{
    /**
     * @var Http
     */
    public $request;

    /**
     * @var DataLayer
     */
    public $dataLayer;

    /**
     * @var Config
     */
    public $configHelper;

    /**
     * @var Script
     */
    protected $script;

    /**
     * @var Logo
     */
    public $logo;

    /**
     * Gtm constructor.
     * @param Context $context
     * @param DataLayer $dataLayer
     * @param Config $configHelper
     * @param Script $script
     * @param Http $request
     * @param Logo $logo
     */
    public function __construct(
        Context $context,
        DataLayer $dataLayer,
        Config $configHelper,
        Script $script,
        Http $request,
        Logo $logo
    )
    {
        $this->configHelper = $configHelper;
        $this->script = $script;
        $this->dataLayer = $dataLayer;
        $this->request = $request;
        $this->logo = $logo;

        parent::__construct(
            $context,
            $dataLayer,
            $configHelper,
            $script,
            $request,
            $logo
        );
    }

    /**
     * @param null $scriptType
     * @return string
     */
    public function injectScript($scriptType = null)
    {
        if ($scriptType === null) {
            return $this->script->buildScript();
        } else {
            return $this->script->buildNoScript();
        }
    }

    /**
     * @param $templateName
     * @return string
     */
    public function getGtm($templateName)
    {
        if (!$this->configHelper->isEnabled()) {
            return '';
        }
        switch ($templateName) {
            case 'head':
                if (!$this->configHelper->injectInHead()) {
                    return '';
                }
                return "<script>var dataLayer = [];</script>" .
                    $this->injectScript();
                break;
            case 'body':
                if ($this->configHelper->injectInHead()) {
                    return '';
                }
                return "<script>var dataLayer = [];</script>" .
                    $this->injectScript();
                break;
            default:
                break;
        }
    }
}
