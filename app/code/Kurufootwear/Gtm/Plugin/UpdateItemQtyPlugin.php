<?php
/**
 * @category     Kurufootwear
 * @package      Kurufootwear/Gtm
 * @copyright    Copyright (c) 2017 Scandiweb, Inc (http://scandiweb.com)
 * @license      http://opensource.org/licenses/OSL-3.0 The Open Software License 3.0 (OSL-3.0)
 */

namespace Kurufootwear\Gtm\Plugin;

use Magento\Checkout\Controller\Sidebar\UpdateItemQty;
use Magento\Catalog\Model\ProductRepository;
use Magento\Framework\Json\Helper\Data;
use Magento\Store\Model\StoreManagerInterface;
use Scandi\Gtm\Helper\Collectors\Attributes;
use Kurufootwear\Gtm\Helper\Collectors\Product;
use Magento\Framework\Exception\NoSuchEntityException;
use Scandi\Gtm\Helper\Config;
use Magento\Checkout\Model\Cart;

class UpdateItemQtyPlugin
{
    /**
     * @var Product
     */
    protected $product;

    /**
     * @var Data
     */
    protected $jsonHelper;

    /**
     * @var ProductRepository
     */
    protected $productRepository;

    /**
     * @var StoreManagerInterface
     */
    protected $storeManager;

    /**
     * @var Config
     */
    protected $config;

    /**
     * @var Attributes
     */
    protected $attributes;

    /**
     * @var Cart
     */
    protected $cart;

    /**
     * UpdateItemQtyPlugin constructor.
     * @param Product $product
     * @param Data $jsonHelper
     * @param ProductRepository $productRepository
     * @param StoreManagerInterface $storeManager
     * @param Config $config
     * @param Attributes $attributes
     * @param Cart $cart
     */
    public function __construct(
        Product $product,
        Data $jsonHelper,
        ProductRepository $productRepository,
        StoreManagerInterface $storeManager,
        Config $config,
        Attributes $attributes,
        Cart $cart
    )
    {
        $this->product = $product;
        $this->jsonHelper = $jsonHelper;
        $this->productRepository = $productRepository;
        $this->storeManager = $storeManager;
        $this->config = $config;
        $this->attributes = $attributes;
        $this->cart = $cart;
    }

    /**
     * @return $this
     */
    public function afterExecute(UpdateItemQty $subject, $result)
    {
        if (!$this->config->isEnabled()) {
            return $result;
        }
        if (!$subject->getResponse()->getStatusCode() === 200) {
            return $result;
        }

        $content = $subject->getResponse()->getContent();

        if ($content !== '') {
            $content = $this->jsonHelper->jsonDecode($content);
        } else {
            $content = array();
        }

        if (array_key_exists("error_message", $content)) {
            return $result;
        }

        $item = $this->itemInit($subject);
        if (!$item) {
            return $result;
        }
        $product = $this->productInit($subject, $item);
        if (!$product) {
            return $result;
        }

        $oldQty = $item->getOrigData('qty');
        $newQty = (int)$subject->getRequest()->getParam('item_qty');
        $diffQty = $newQty - $oldQty;

        if ($diffQty > 0) {
            $eventName = 'addToCart';
            $ecommerceVariable = 'add';
        } else {
            $eventName = 'removeFromCart';
            $ecommerceVariable = 'remove';
        }

        $content["eventPush"] = $this->product->collectCartEvent($product, $eventName);
        $content["eventPush"]["ecommerce"][$ecommerceVariable]["products"][0]['quantity'] = abs($diffQty);
        $content["eventPush"]["ecommerce"][$ecommerceVariable]["products"][0]['dimension2'] = $product->getSku();
        if ($variant = $this->getVariant($product)) {
            $content["eventPush"]["ecommerce"][$ecommerceVariable]["products"][0]['variant'] = $variant;
        }

        $subject->getResponse()->representJson(
            $this->jsonHelper->jsonEncode($content)
        );

        return $result;
    }

    /**
     * @param $subject
     * @return bool|\Magento\Catalog\Api\Data\ProductInterface|mixed
     */
    public function itemInit($subject)
    {
        if ($itemId = (int)$subject->getRequest()->getParam('item_id')) {
            try {
                return $this->cart->getQuote()->getItemById($itemId);
            } catch (NoSuchEntityException $e) {
                return false;
            }
        }
        return false;
    }

    /**
     * @param $item
     * @param $subject
     * @return bool|\Magento\Catalog\Api\Data\ProductInterface|mixed
     */
    public function productInit($subject, $item)
    {
        $storeId = $this->storeManager->getStore()->getId();

        try {
            $product = $this->productRepository->get($item->getSku(), false, $storeId);
            $product = $this->attributes->handleAttributes($product, $subject->getRequest()->getParams());

            return $product;
        } catch (NoSuchEntityException $e) {
            return false;
        }
    }

    /**
     * @param $product
     * @return null|string
     */
    public function getVariant($product)
    {
        $product = $this->productRepository->get($product->getSku());

        if ($duplicateSku = $product->getData('parent_sku')) {
            $size = $product->getAttributeText('size');
            $womensSize = $product->getAttributeText('womens_size');
            $width = $this->getDuplicateWidth($duplicateSku);

            if ($size) {
                return $size . ' ' . $width;
            } else if ($womensSize) {
                return $womensSize . ' ' . $width;
            } else {
                return null;
            }
        } else {
            return null;
        }
    }

    public function getDuplicateWidth($duplicateSku)
    {
        $product = $this->productRepository->get($duplicateSku);
        $width = $product->getAttributeText('width');

        return strtoupper(substr($width, 0, 1));
    }
}