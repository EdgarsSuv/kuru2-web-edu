<?php

/**
 * @category     Kurufootwear
 * @package      Kurufootwear/Gtm
 * @copyright    Copyright (c) 2017 Scandiweb, Inc (http://scandiweb.com)
 * @license      http://opensource.org/licenses/OSL-3.0 The Open Software License 3.0 (OSL-3.0)
 */

namespace Kurufootwear\Gtm\Plugin;
use Magento\Customer\Model\ResourceModel\CustomerRepository;
use Magento\Customer\Model\Session;
use Magento\Framework\Exception\NotFoundException;
use Scandi\Gtm\Block\DataLayer;

/**
 * Class AddToCartPlugin
 * @package Scandi\Gtm\Plugin
 */
class DataLayerPlugin
{

    const NOT_LOGGED_CUSTOMER_STATE = 'NOT LOGGED IN';

    /**
     * @var Session
     */
    protected $session;

    /**
     * @var Session
     */
    protected $customerSession;

    public function __construct(
        CustomerRepository $customerRepository,
        Session $customerSession
    )
    {
        $this->customerRepository = $customerRepository;
        $this->customerSession = $customerSession;
    }

    /**
     * @param DataLayer $subject
     * @param $result
     * @return mixed
     */
    public function afterCollectLayer(DataLayer $subject, $result)
    {
        if (!array_key_exists('customerId', $result)) {
            return $result;
        }
        if ($result['customerId'] === self::NOT_LOGGED_CUSTOMER_STATE) {
            return $result;
        }
        try {
            $result = $this->assignDataToResult($result, $this->customerRepository->getById($this->customerSession->getId()));
            return $result;
        } catch (NotFoundException $e) {
            return $result;
        }

    }

    /**
     * @param $result
     * @param $user
     * @return mixed
     */
    public function assignDataToResult($result, $user)
    {
        $result['email'] = $user->getEmail();
        $result['name'] = $user->getFirstName();
        $result['surname'] = $user->getLastName();
        $addresses = $user->getAddresses();
        if (sizeof($addresses) > 0) {
            $address = $addresses[0];
            $result['city'] = $address->getCity();
            $result['users_phone_number'] = $address->getTelephone();
            $result['zip'] = $address->getPostcode();
        }
        return $result;
    }
}

?>
