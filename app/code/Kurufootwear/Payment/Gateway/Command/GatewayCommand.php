<?php
/**
 * @vendor Kurufootwear
 * @module Kurufootwear_Payment
 * @author Aleksejs Baranovs <info@scandiweb.com>
 * @copyright Copyright (c) 2018 KURU Footwear. All rights reserved.
 */

namespace Kurufootwear\Payment\Gateway\Command;

use Magento\Framework\App\Area;
use Magento\Framework\Exception\LocalizedException;
use Magento\Framework\Phrase;
use Magento\Payment\Gateway\Command\CommandException;
use Magento\Payment\Gateway\Command\GatewayCommand as BaseGatewayCommand;
use Magento\Payment\Gateway\Http\ClientException;
use Magento\Payment\Gateway\Http\ClientInterface;
use Magento\Payment\Gateway\Http\ConverterException;
use Magento\Payment\Gateway\Http\TransferFactoryInterface;
use Magento\Payment\Gateway\Request\BuilderInterface;
use Magento\Payment\Gateway\Response\HandlerInterface;
use Magento\Payment\Gateway\Validator\ValidatorInterface;
use Psr\Log\LoggerInterface;
use Magento\Framework\App\State;

/**
 * Class GatewayCommand
 * @package Kurufootwear\Payment\Gateway\Command
 */
class GatewayCommand extends BaseGatewayCommand
{
    /**
     * @var TransferFactoryInterface
     */
    private $transferFactory;

    /**
     * @var BuilderInterface
     */
    private $requestBuilder;

    /**
     * @var ValidatorInterface|null
     */
    private $validator;

    /**
     * @var ClientInterface
     */
    private $client;

    /**
     * @var HandlerInterface|null
     */
    private $handler;

    /**
     * @var LoggerInterface
     */
    private $logger;

    /**
     * @var State
     */
    private $appState;

    /**
     * GatewayCommand constructor.
     *
     * @param BuilderInterface $requestBuilder
     * @param TransferFactoryInterface $transferFactory
     * @param ClientInterface $client
     * @param LoggerInterface $logger
     * @param State $appState
     * @param HandlerInterface|null $handler
     * @param ValidatorInterface|null $validator
     */
    public function __construct(
        BuilderInterface $requestBuilder,
        TransferFactoryInterface $transferFactory,
        ClientInterface $client,
        LoggerInterface $logger,
        State $appState,
        HandlerInterface $handler = null,
        ValidatorInterface $validator = null
    ) {
        parent::__construct($requestBuilder, $transferFactory, $client, $logger, $handler, $validator);

        $this->transferFactory = $transferFactory;
        $this->requestBuilder = $requestBuilder;
        $this->validator = $validator;
        $this->client = $client;
        $this->handler = $handler;
        $this->logger = $logger;
        $this->appState = $appState;
    }

    /**
     * Executes command basing on business object
     * Override  to include detailed info from the gateway into exception message
     *
     * @param array $commandSubject
     *
     * @return void
     *
     * @throws CommandException
     * @throws ClientException
     * @throws ConverterException
     */
    public function execute(array $commandSubject)
    {
        $transferO = $this->transferFactory->create(
            $this->requestBuilder->build($commandSubject)
        );

        $response = $this->client->placeRequest($transferO);

        if ($this->validator !== null) {
            $result = $this->validator->validate(
                array_merge($commandSubject, ['response' => $response])
            );

            if (!$result->isValid()) {
                $this->logExceptions($result->getFailsDescription());
                $errorMessage = '';

                try {
                    $appState = $this->appState->getAreaCode();
                } catch (LocalizedException $e) {
                    $appState = null;
                }

                if ($appState === Area::AREA_ADMINHTML && isset($response['object']) && $response['object']->message) {
                    $errorMessage =  $response['object']->message;
                }

                throw new CommandException(
                    __('Transaction has been declined. Please try again later. %1', $errorMessage)
                );
            }
        }

        if ($this->handler) {
            $this->handler->handle(
                $commandSubject,
                $response
            );
        }
    }

    /**
     * Log exceptions
     *
     * @param Phrase[] $fails
     *
     * @return void
     */
    private function logExceptions(array $fails)
    {
        foreach ($fails as $failPhrase) {
            $this->logger->critical((string) $failPhrase);
        }
    }
}
