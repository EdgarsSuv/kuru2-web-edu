<?php
/**
 * @vendor Kurufootwear
 * @module Kurufootwear_Payment
 * @author Aleksejs Baranovs <info@scandiweb.com>
 * @copyright Copyright (c) 2018 KURU Footwear. All rights reserved.
 */

namespace Kurufootwear\Payment\Logger;

use Monolog\Logger;

/**
 * Class RefundErrorLogger
 * @package Kurufootwear\Payment\Logger
 */
class RefundErrorLogger extends Logger
{

}
