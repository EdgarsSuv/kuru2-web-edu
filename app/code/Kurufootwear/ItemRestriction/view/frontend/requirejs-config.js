/**
 * @vendor Kurufootwear
 * @module Kurufootwear_ItemRestriction
 * @author Aleksejs Baranovs <info@scandiweb.com>
 * @copyright Copyright (c) 2018 KURU Footwear. All rights reserved.
 */

var config = {
    config: {
        mixins: {
            'Magento_Checkout/js/action/set-shipping-information': {
                'Kurufootwear_ItemRestriction/js/action/set-shipping-information-mixin': true
            },
            'Magento_OfflineShipping/js/model/shipping-rates-validation-rules/tablerate': {
                'Kurufootwear_ItemRestriction/js/model/shipping-rates-validation-rules/tablerate-mixin': true
            },
        }
    }
};
