/**
 * @vendor Kurufootwear
 * @module Kurufootwear_ItemRestriction
 * @author Aleksejs Baranovs <info@scandiweb.com>
 * @copyright Copyright (c) 2018 KURU Footwear. All rights reserved.
 */

define(
    [],
    function () {
        return function(tableRate) {
            /**
             * Get table rate rules
             * Override to set region as not required for switching to non US countries
             *
             * @returns {Object}
             */
            tableRate.getRules = function() {
                return {
                    'postcode': {
                        'required': true
                    },
                    'country_id': {
                        'required': true
                    },
                    'region_id': {
                        'required': false
                    },
                    'region_id_input': {
                        'required': false
                    }
                };
            };

            return tableRate;
        };
    }
);
