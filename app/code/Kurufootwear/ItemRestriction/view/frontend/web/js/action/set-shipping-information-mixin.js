/**
 * @vendor Kurufootwear
 * @module Kurufootwear_ItemRestriction
 * @author Aleksejs Baranovs <info@scandiweb.com>
 * @copyright Copyright (c) 2018 KURU Footwear. All rights reserved.
 */

define([
    'jquery',
    'mage/utils/wrapper',
    'Magento_Checkout/js/model/quote',
    'Kurufootwear_ItemRestriction/js/model/item-restriction'
], function ($, wrapper, quote, itemRestriction) {
    'use strict';

    return function (setShippingInformationAction) {

        return wrapper.wrap(setShippingInformationAction, function (originalAction) {
            /* Override to add item restriction check on shipping information save */
            return $.when(itemRestriction.restrictItems(), originalAction());
        });
    };
});
