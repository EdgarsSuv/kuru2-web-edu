/**
 * @vendor Kurufootwear
 * @module Kurufootwear_ItemRestriction
 * @author Aleksejs Baranovs <info@scandiweb.com>
 * @copyright Copyright (c) 2018 KURU Footwear. All rights reserved.
 */

define(
    [
        'ko',
        'jquery',
        'Magento_Checkout/js/model/quote',
        'Magento_Checkout/js/model/resource-url-manager',
        'mage/storage',
        'Magento_Checkout/js/model/payment-service',
        'Magento_Checkout/js/model/payment/method-converter',
        'Magento_Checkout/js/model/error-processor',
        'Magento_Checkout/js/model/full-screen-loader'
    ],
    function (
        ko,
        $,
        quote,
        resourceUrlManager,
        storage,
        paymentService,
        methodConverter,
        errorProcessor,
        fullScreenLoader
    ) {
        'use strict';

        return {
            restrictItems: function () {
                var payload,
                    self = this;

                payload = {
                    addressInformation: {
                        shipping_address: quote.shippingAddress(),
                        billing_address: quote.billingAddress()
                    },
                    items: quote.getItems()
                };

                fullScreenLoader.startLoader();

                return storage.post(
                    self.getRestrictItemsUrl(quote),
                    JSON.stringify(payload)
                ).done(
                    function (response) {
                        fullScreenLoader.stopLoader();
                    }
                ).fail(
                    function (response) {
                        errorProcessor.process(response);
                        fullScreenLoader.stopLoader();
                    }
                );
            },

            /**
             * Get item restriction URL
             *
             * @param quote
             *
             * @returns {string|*|String}
             */
            getRestrictItemsUrl: function (quote) {
                var params = {
                    cartId: quote.getQuoteId()
                },
                    url = {
                    'default': '/checkout/:cartId/item-restriction'
                };

                return resourceUrlManager.getUrl(url, params);
            }
        };
    }
);
