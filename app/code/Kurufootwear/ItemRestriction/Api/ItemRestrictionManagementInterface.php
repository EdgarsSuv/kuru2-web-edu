<?php
/**
 * @vendor Kurufootwear
 * @module Kurufootwear_ItemRestriction
 * @author Aleksejs Baranovs <info@scandiweb.com>
 * @copyright Copyright (c) 2018 KURU Footwear. All rights reserved.
 */

namespace Kurufootwear\ItemRestriction\Api;

use Magento\Checkout\Api\Data\ShippingInformationInterface;
use Magento\Framework\Exception\StateException;

/**
 * Interface ItemRestrictionManagementInterface
 * @package Kurufootwear\ItemRestriction\Api
 */
interface ItemRestrictionManagementInterface
{
    /**
     * Check for restricted items
     *
     * @param string $cartId
     * @param ShippingInformationInterface $addressInformation
     * @param mixed $items
     *
     * @return bool
     *
     * @throws StateException
     */
    public function checkForRestrictedItems(
        $cartId,
        ShippingInformationInterface $addressInformation,
        $items
    );
}