<?php
/**
 * @vendor Kurufootwear
 * @module Kurufootwear_ItemRestriction
 * @author Aleksejs Baranovs <info@scandiweb.com>
 * @copyright Copyright (c) 2018 KURU Footwear. All rights reserved.
 */

namespace Kurufootwear\ItemRestriction\Model;

use Kurufootwear\ItemRestriction\Api\ItemRestrictionManagementInterface;
use Magento\Checkout\Api\Data\ShippingInformationInterface;
use Magento\Framework\App\Config\ScopeConfigInterface;
use Magento\Framework\Exception\StateException;

/**
 * Class ItemRestrictionManagement
 * @package Kurufootwear\ItemRestriction\Model
 */
class ItemRestrictionManagement implements ItemRestrictionManagementInterface
{
    /**
     * XML paths
     */
    const RESTRICTED_PRODUCT_SKUS_XML = 'item_restriction_config/item_restriction/item_restriction_skus';
    const RESTRICTED_REGION_CODES_XML = 'item_restriction_config/item_restriction/item_restriction_regions';

    /**
     * @var ScopeConfigInterface
     */
    private $scopeConfig;

    /**
     * ItemRestrictionManagement constructor.
     *
     * @param ScopeConfigInterface $scopeConfig
     */
    public function __construct(
        ScopeConfigInterface $scopeConfig
    ) {
        $this->scopeConfig = $scopeConfig;
    }

    /**
     * Check for restricted items
     *
     * @param string $cartId
     * @param ShippingInformationInterface $addressInformation
     * @param mixed $items
     *
     * @return bool
     *
     * @throws StateException
     */
    public function checkForRestrictedItems(
        $cartId,
        ShippingInformationInterface $addressInformation,
        $items
    ) {
        $restrictedSkus = $this->getRestrictedProductSkusArray();
        $restrictedRegions = $this->getRestrictedRegionCodesArray();

        if (!$restrictedSkus || empty($restrictedSkus) || !$restrictedRegions || empty($restrictedRegions)) {
            return false;
        }

        $regionCode = $addressInformation->getShippingAddress()->getRegionCode();
        $countryCode = $addressInformation->getShippingAddress()->getCountryId();

        foreach ($items as $item) {
            if ((isset($item['product']) && in_array($item['product']['sku'], $restrictedSkus))
                    && ($countryCode !== 'US' || in_array($regionCode, $restrictedRegions))) {
                throw new StateException(
                    __('We\'re sorry, but the %1 can\'t be shipped to your location. Please remove it from your bag.', $item['name'])
                );
            }
        }

        return true;
    }

    /**
     * Get restricted product SKUs array
     *
     * @return null|string|string[]
     */
    private function getRestrictedProductSkusArray()
    {
        $productSkus = $this->scopeConfig->getValue(self::RESTRICTED_PRODUCT_SKUS_XML);

        return explode(PHP_EOL, $productSkus);
    }

    /**
     * Get restricted region codes array
     *
     * @return null|string|string[]
     */
    private function getRestrictedRegionCodesArray()
    {
        $regionCodes = $this->scopeConfig->getValue(self::RESTRICTED_REGION_CODES_XML);

        return explode(PHP_EOL, $regionCodes);
    }
}