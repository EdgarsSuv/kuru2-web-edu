<?php
/**
 * @vendor      Kurufootwear
 * @module      Kurufootwear_Review
 * @author      Kristaps Stalidzans <info@scandiweb.com>
 * @copyright   Copyright (c) 2017 Scandiweb. All rights reserved.
 * @license     http://opensource.org/licenses/afl-3.0.php Academic Free License (AFL 3.0)
 */
namespace Kurufootwear\Review\Block\Customer;

use Magento\Customer\Api\AccountManagementInterface;
use Magento\Customer\Api\CustomerRepositoryInterface;
use Magento\Customer\Helper\Session\CurrentCustomer;
use Magento\Customer\Model\Session;
use Magento\Framework\View\Element\Template\Context;
use Magento\Newsletter\Model\SubscriberFactory;
use Magento\Catalog\Model\ProductFactory;
use Magento\Review\Model\ResourceModel\Review\Product\Collection;
use Magento\Review\Model\ResourceModel\Review\Product\CollectionFactory;

class ListCustomer extends \Magento\Review\Block\Customer\ListCustomer
{
    /**
     * @var ProductFactory
     */
    protected $productFactory;

    /**
     * ListCustomer constructor.
     * @param Context $context
     * @param Session $customerSession
     * @param SubscriberFactory $subscriberFactory
     * @param CustomerRepositoryInterface $customerRepository
     * @param AccountManagementInterface $customerAccountManagement
     * @param CollectionFactory $collectionFactory
     * @param CurrentCustomer $currentCustomer
     * @param ProductFactory $productFactory
     * @param array $data
     */
    public function __construct(
        Context $context,
        Session $customerSession,
        SubscriberFactory $subscriberFactory,
        CustomerRepositoryInterface $customerRepository,
        AccountManagementInterface $customerAccountManagement,
        CollectionFactory $collectionFactory,
        CurrentCustomer $currentCustomer,
        ProductFactory $productFactory,
        array $data = []
    ){
        $this->productFactory = $productFactory;

        parent::__construct(
            $context,
            $customerSession,
            $subscriberFactory,
            $customerRepository,
            $customerAccountManagement,
            $collectionFactory,
            $currentCustomer,
            $data
        );
    }

    /**
     * Returns the product URL by the passed product ID
     *
     * @param int $productId
     * @return mixed
     */
    public function getProductUrl($productId)
    {
        return $this->productFactory->create()->load($productId)->getProductUrl();
    }

    /**
     * Format date in short format
     *
     * @param string $date
     * @return string
     */
    public function dateFormat($date)
    {
        return $this->formatDate($date, \IntlDateFormatter::FULL);
    }

    /**
     * This is needed to take the correct templates
     *
     * @return string
     */
    protected function _toHtml()
    {
        $this->setModuleName($this->extractModuleName('Magento\Review\Block\Customer\ListCustomer'));

        return parent::_toHtml();
    }

    /**
     * Get reviews
     * Override to remove store filter
     *
     * @return bool|Collection
     */
    public function getReviews()
    {
        if (!($customerId = $this->currentCustomer->getCustomerId())) {
            return false;
        }

        if (!$this->_collection) {
            $this->_collection = $this->_collectionFactory->create();
            $this->_collection
                ->addCustomerFilter($customerId)
                ->setDateOrder();
        }

        return $this->_collection;
    }
}
