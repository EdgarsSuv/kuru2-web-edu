<?php
/**
 * @category     Kurufootwear
 * @package      Kurufootwear/Paypal
 * @author       Krisjanis Veinbahs <krisjanisv@kurufootwear.com>
 * @copyright    Copyright (c) 2018 KURU Footwear. All rights reserved.
 */
namespace Kurufootwear\Paypal\Plugin;

use Magento\Checkout\Controller\Cart\Index;
use Magento\Customer\Model\Session as CustomerSession;
use Magento\Framework\Message\ManagerInterface as MessageManagerInterface;
use Magento\Framework\Message\MessageInterface;

class CartIndexPlugin
{
    /**
     * @var CustomerSession
     */
    protected $customerSession;

    /**
     * @var MessageManagerInterface
     */
    protected $messageManager;

    /**
     * CartIndexPlugin constructor.
     *
     * @param CustomerSession $customerSession
     * @param MessageManagerInterface $messageManager
     */
    public function __construct(
        CustomerSession $customerSession,
        MessageManagerInterface $messageManager
    ) {
        $this->customerSession = $customerSession;
        $this->messageManager = $messageManager;
    }

    /**
     * Display and unset PayPal error messages if they exist
     */
    public function aroundExecute(Index $subject, callable $proceed)
    {
        $result = $proceed();

        $payPalErrorMessage = $this->customerSession->getPayPalErrorMessage();
        if ($payPalErrorMessage) {
            if (!$this->isMessageInManager($payPalErrorMessage)) {
                $this->messageManager->addErrorMessage(__($payPalErrorMessage));
            }

            $this->customerSession->unsPayPalErrorMessage();
        }

        return $result;
    }

    /**
     * Check if a message is already polling and to be displayed in
     * the message manager collection
     *
     * @param $message
     * @return bool
     */
    private function isMessageInManager($message)
    {
        $managedMessages = $this->messageManager->getMessages();

        foreach ($managedMessages as $managedMessage) {
            /* @var MessageInterface $managedMessage */
            if ($managedMessage->toString() == $message) {
                return true;
            }
        }

        return false;
    }
}