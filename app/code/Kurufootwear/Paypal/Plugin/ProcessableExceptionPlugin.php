<?php

namespace Kurufootwear\Paypal\Plugin;

use Magento\Paypal\Model\Api\ProcessableException;

/**
 * Class ProcessableExceptionPlugin
 *
 * Override PayPal user friendly messages with more helpful solution.
 *
 * @package Kurufootwear\Paypal\Plugin
 */
class ProcessableExceptionPlugin
{
    /**
     * The internal and processing errors usually happen because of invalid user data.
     *
     * @param ProcessableException $subject
     * @param callable $proceed
     *
     * @return \Magento\Framework\Phrase
     */
    public function aroundGetUserMessage(ProcessableException $subject, callable $proceed)
    {
        switch ($subject->getCode()) {
            case ProcessableException::API_INTERNAL_ERROR:
            case ProcessableException::API_UNABLE_PROCESS_PAYMENT_ERROR_CODE:
                $message = __(
                    'We were not able to process your payment. Please recheck shipping address and credit card in PayPal.'
                );
                break;
            default:
                $message = $proceed();
                break;
        }

        return $message;
    }
}