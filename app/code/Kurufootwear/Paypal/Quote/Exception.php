<?php
/**
 * @category     Kurufootwear
 * @package      Kurufootwear/Paypal
 * @author       Krisjanis Veinbahs <krisjanisv@kurufootwear.com>
 * @copyright    Copyright (c) 2018 KURU Footwear. All rights reserved.
 */
namespace Kurufootwear\Paypal\Quote;

use Magento\Framework\Exception\LocalizedException;

/**
 * Class Exception
 *
 * Created for Paypal token creation when quote has
 * issues and therefore breaks token creation
 *
 * @package Kurufootwear\Paypal\Quote
 */
class Exception extends LocalizedException {}
