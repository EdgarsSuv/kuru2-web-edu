<?php
/**
 * @category     Kurufootwear
 * @package      Kurufootwear/Paypal
 * @author       Krisjanis Veinbahs <krisjanisv@kurufootwear.com>
 * @copyright    Copyright (c) 2018 KURU Footwear. All rights reserved.
 */
namespace Kurufootwear\Paypal\Controller\Express;

use Exception as GenericException;
use Kurufootwear\Paypal\Quote\Exception as QuoteException;
use Magento\Checkout\Model\Session as CheckoutSession;
use Magento\Customer\Model\Session;
use Magento\Customer\Model\Url;
use Magento\Framework\App\Action\Context;
use Magento\Framework\Controller\ResultFactory;
use Magento\Framework\Controller\ResultInterface;
use Magento\Framework\Exception\LocalizedException;
use Magento\Framework\Session\Generic;
use Magento\Framework\Url\Helper\Data;
use Magento\Paypal\Controller\Express\GetToken as MagentoGetToken;
use Magento\Framework\Webapi\Exception;
use Magento\Paypal\Model\Express\Checkout\Factory;
use Magento\Quote\Model\Quote;
use Magento\Sales\Model\OrderFactory;
use Psr\Log\LoggerInterface;

class GetToken extends MagentoGetToken
{
    /**
     * @var LoggerInterface
     */
    protected $logger;

    /**
     * Config mode type
     *
     * @var string
     */
    protected $_configType = 'Kurufootwear\Paypal\Model\Config';

    /**
     * Checkout mode type
     *
     * @var string
     */
    protected $_checkoutType = 'Kurufootwear\Paypal\Model\Express\Checkout';

    /**
     * GetToken constructor.
     * @param Context $context
     * @param Session $customerSession
     * @param CheckoutSession $checkoutSession
     * @param OrderFactory $orderFactory
     * @param Factory $checkoutFactory
     * @param Generic $paypalSession
     * @param Data $urlHelper
     * @param Url $customerUrl
     * @param LoggerInterface $logger
     */
    public function __construct(
        Context $context,
        Session $customerSession,
        CheckoutSession $checkoutSession,
        OrderFactory $orderFactory,
        Factory $checkoutFactory,
        Generic $paypalSession,
        Data $urlHelper,
        Url $customerUrl,
        LoggerInterface $logger
    ) {
        $this->logger = $logger;

        parent::__construct(
            $context,
            $customerSession,
            $checkoutSession,
            $orderFactory,
            $checkoutFactory,
            $paypalSession,
            $urlHelper,
            $customerUrl
        );
    }

    /**
     * Overridden to verbosely log information
     * when PayPal requests fail.
     */
    public function execute()
    {
        /* @var Quote $quote */
        $quote = $this->_getQuote();
        $controllerResult = $this->resultFactory->create(ResultFactory::TYPE_JSON);

        try {
            try {
                $token = $this->getToken();
                if ($token === null) {
                    $token = false;
                }
                $url = $this->_checkout->getRedirectUrl();
                $this->_initToken($token);
                $controllerResult->setData(['url' => $url]);
            } catch (GenericException $exception) {
                $this->logExceptionQuote($quote, $exception);

                throw $exception;
            }
        } catch (QuoteException $exception) {
            $this->messageManager->addErrorMessage(__("Paypal initialisation failed because of invalid cart data."));
            foreach ($quote->getErrors() as $quoteError) {
                $this->messageManager->addErrorMessage($quoteError);
            }

            return $this->getErrorResponse($controllerResult);
        } catch (LocalizedException $exception) {
            $this->messageManager->addExceptionMessage(
                $exception,
                $exception->getMessage()
            );

            return $this->getErrorResponse($controllerResult);
        } catch (GenericException $exception) {
            $this->messageManager->addExceptionMessage(
                $exception,
                __('We can\'t start Express Checkout.')
            );

            return $this->getErrorResponse($controllerResult);
        }

        return $controllerResult;
    }

    /**
     * Helper to log info about the current quote and the PayPal exception
     *
     * @param Quote $quote
     * @param $exception
     */
    public function logExceptionQuote($quote, GenericException $exception)
    {
        /**
         * Serialize complex log payload
         */
        $quoteMessageTexts = array_map(function($message) {
            return $message->toString();
        }, $quote->getMessages());

        $quoteItems = array_map(function($item) {
            /* @var Quote\Item $item */
            return [
                'item_id' => $item->getId(),
                'item_quantity' => $item->getQty(),
                'product_id' => $item->getProduct()->getId(),
                'product_quantity' => $item->getProduct()->getQty()
            ];
        }, $quote->getAllItems());

        /**
         * Log issue on server
         */
        $this->logger->critical(
            __("PayPal token generation failed, additional info below."),
            [
                'quote' => [
                    'id' => $quote->getId(),
                    'customer_id' => $quote->getCustomerId(),
                    'items' => $quoteItems,
                    'messages' => $quoteMessageTexts,
                    'addresses' => [
                        'shipping' => $quote->getShippingAddress()->getData(),
                        'billing' => $quote->getBillingAddress()->getData()
                    ],
                    'info' => $quote->getData(),
                ],
                'exception' => [
                    'message' => $exception->getMessage(),
                    'trace' => $exception->getTraceAsString()
                ]
            ]
        );
    }

    /**
     * Instantiate quote and checkout
     * Overridden to throw more specific errors, which can be handled differently.
     *
     * @throws LocalizedException
     * @return void
     */
    protected function _initCheckout()
    {
        /* @var Quote $quote */
        $quote = $this->_getQuote();

        if (!$quote->hasItems()) {
            $this->getResponse()->setStatusHeader(403, '1.1', 'Forbidden');
            throw new LocalizedException(__('Cart contains invalid or no items.'));
        }

        if ($quote->getHasError()) {
            $this->getResponse()->setStatusHeader(403, '1.1', 'Forbidden');

            if (count($quote->getMessages()) == 0) {
                throw new LocalizedException(__('Cart has faulty contents.'));
            } else {
                throw new QuoteException(__('Cart has faulty contents.'));
            }
        }

        if (!isset($this->_checkoutTypes[$this->_checkoutType])) {
            $parameters = [
                'params' => [
                    'quote' => $quote,
                    'config' => $this->_config,
                ],
            ];
            $this->_checkoutTypes[$this->_checkoutType] = $this->_checkoutFactory
                ->create($this->_checkoutType, $parameters);
        }
        $this->_checkout = $this->_checkoutTypes[$this->_checkoutType];
    }

    /**
     * Re-declared, because of private scope
     *
     * @param ResultInterface $controllerResult
     *
     * @return ResultInterface
     */
    private function getErrorResponse(ResultInterface $controllerResult)
    {
        $controllerResult->setHttpResponseCode(Exception::HTTP_BAD_REQUEST);
        $controllerResult->setData(['message' => __('Sorry, but something went wrong')]);

        return $controllerResult;
    }
}