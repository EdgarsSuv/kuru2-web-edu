<?php
/**
 * @category     Kurufootwear
 * @package      Kurufootwear/Paypal
 * @author       Krisjanis Veinbahs <krisjanisv@kurufootwear.com>
 * @copyright    Copyright (c) 2018 KURU Footwear. All rights reserved.
 */
namespace Kurufootwear\Paypal\Controller\Express;

use Magento\Paypal\Controller\Express\PlaceOrder as MagentoPlaceOrder;
use Magento\Paypal\Model\Api\ProcessableException;

/**
 * Class PlaceOrder
 *
 * Overriding to fix PayPal errors not appearing in shopping bag.
 *
 * @package Kurufootwear\Paypal\Controller\Express
 */
class PlaceOrder extends MagentoPlaceOrder
{
    /**
     * Use customer session manager for PayPal error message managing,
     * because the error messages don't appear after checkout -> shopping bag redirect,
     * because the session persistence fix doesn't work in the message manager,
     * because it has its own session managing implementation
     *
     * For message displaying see the shopping bag controller plugin
     *
     * @param ProcessableException $exception
     */
    protected function _processPaypalApiError($exception)
    {
        $this->_customerSession->setPayPalErrorMessage(
            $exception->getUserMessage()->__toString()
        );

        parent::_processPaypalApiError($exception);
    }
}