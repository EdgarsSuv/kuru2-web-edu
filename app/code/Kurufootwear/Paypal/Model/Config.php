<?php
/**
 * @vendor Kurufootwear
 * @module Kurufootwear_Paypal
 * @author Krisjanis Veinbahs <info@scandiweb.com>
 * @copyright Copyright (c) 2018 KURU Footwear. All rights reserved.
 */
namespace Kurufootwear\Paypal\Model;

use Magento\Paypal\Model\Config as MagentoPaypalConfig;

/**
 * Class Config
 *
 * Overridden for compatibility with PayPal's checkout.js V4
 * Magento2's expected and supported PayPal checkout.js V3.5 was deprecated
 *
 * @package Kurufootwear\Paypal\Model
 */
class Config extends MagentoPaypalConfig
{
    /**
     * PayPal web URL generic getter
     *
     * @param array $params
     * @return string
     */
    public function getPaypalUrl(array $params = [])
    {
        return sprintf(
            'https://www.%spaypal.com/checkoutnow%s',
            $this->getValue('sandboxFlag') ? 'sandbox.' : '',
            $params ? '?' . http_build_query($params) : ''
        );
    }

    /**
     * Get url for dispatching customer to express checkout start
     *
     * @param string $token
     * @return string
     */
    public function getExpressCheckoutStartUrl($token)
    {
        return $this->getPaypalUrl(['token' => $token]);
    }

    /**
     * Get url for dispatching customer to checkout retrial
     *
     * @param string $orderId
     * @return string
     */
    public function getExpressCheckoutOrderUrl($orderId)
    {
        return $this->getPaypalUrl(['order_id' => $orderId]);
    }

    /**
     * Get url that allows to edit checkout details on paypal side
     *
     * @param \Magento\Paypal\Controller\Express|string $token
     * @return string
     */
    public function getExpressCheckoutEditUrl($token)
    {
        return $this->getPaypalUrl(['useraction' => 'continue', 'token' => $token]);
    }
}