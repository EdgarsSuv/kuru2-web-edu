<?php
/**
 * @category    Kurufootwear
 * @package     Kurufootwear/Paypal
 * @author      Krisjanis Veinbahs <krisjanisv@scandiweb.com>
 * @copyright   Copyright (c) 2017 Scandiweb, Inc (http://scandiweb.com)
 * @license     http://opensource.org/licenses/OSL-3.0 The Open Software License 3.0 (OSL-3.0)
 */
namespace Kurufootwear\Paypal\Model\Paypal\Helper;

use Magento\Quote\Model\Quote;
use Magento\Quote\Api\CartRepositoryInterface;
use Magento\Braintree\Gateway\Config\PayPal\Config;

/**
 * Class ShippingMethodUpdater
 * @package Kurufootwear\Paypal\Model\Paypal\Helper
 */
class ShippingMethodUpdater extends \Magento\Braintree\Model\Paypal\Helper\ShippingMethodUpdater
{
    /**
     * @var CartRepositoryInterface
     */
    private $quoteRepository;

    public function __construct(
        Config $config,
        CartRepositoryInterface $quoteRepository
    )
    {
        $this->quoteRepository = $quoteRepository;
    }

    /**
     * Execute operation
     *
     * @param string $shippingMethod
     * @param Quote $quote
     * @return void
     * @throws \InvalidArgumentException
     */
    public function execute($shippingMethod, Quote $quote)
    {
        if (empty($shippingMethod)) {
            throw new \InvalidArgumentException('The "shippingMethod" field does not exists.');
        }

        if (!$quote->getIsVirtual()) {
            $shippingAddress = $quote->getShippingAddress();
            $this->disabledQuoteAddressValidation($quote);

            $shippingAddress->setShippingMethod($shippingMethod);
            $shippingAddress->setCollectShippingRates(true);

            $quote->collectTotals();

            $this->quoteRepository->save($quote);
        }
    }

}
