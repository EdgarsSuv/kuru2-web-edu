<?php
/**
 * @vendor Kurufootwear
 * @module Kurufootwear_Paypal
 * @author Krisjanis Veinbahs <info@scandiweb.com>
 * @copyright Copyright (c) 2018 KURU Footwear. All rights reserved.
 */
namespace Kurufootwear\Paypal\Model\Express;

use Exception;
use Magento\Paypal\Model\Cart as PaypalCart;
use Magento\Payment\Model\Cart;
use Magento\Paypal\Model\Config as PaypalConfig;
use Magento\Checkout\Helper\Data as CheckoutData;
use Magento\Checkout\Model\Session;
use Magento\Checkout\Model\Type\OnepageFactory;
use Magento\Customer\Api\CustomerRepositoryInterface;
use Magento\Customer\Model\AccountManagement;
use Magento\Customer\Model\Session as CustomerSession;
use Magento\Customer\Model\Url;
use Magento\Framework\App\Cache\Type\Config as CacheTypeConfig;
use Magento\Framework\DataObject\Copy;
use Magento\Framework\Encryption\EncryptorInterface;
use Magento\Framework\Exception\LocalizedException;
use Magento\Framework\Locale\ResolverInterface as LocaleResolverInterface;
use Magento\Framework\Message\ManagerInterface;
use Magento\Framework\UrlInterface;
use Magento\Payment\Model\Method\Free;
use Magento\Paypal\Model\Api\Type\Factory;
use Magento\Paypal\Model\Billing\AgreementFactory;
use Magento\Paypal\Model\CartFactory;
use Magento\Paypal\Model\Config;
use Magento\Paypal\Model\Express\Checkout as MagentoExpressCheckout;
use Magento\Paypal\Model\Info;
use Magento\Quote\Api\CartManagementInterface;
use Magento\Quote\Api\CartRepositoryInterface;
use Magento\Quote\Model\Quote\Address;
use Magento\Quote\Model\Quote\TotalsCollector;
use Magento\Sales\Model\Order\Email\Sender\OrderSender;
use Magento\Quote\Api\Data\PaymentInterfaceFactory as PaymentFactory;
use Magento\Store\Model\StoreManagerInterface;
use Magento\Tax\Helper\Data;
use Psr\Log\LoggerInterface;

/**
 * Class Checkout
 *
 * Overridden to work around PayPal no-$0-order policy.
 * Created an override instead of a plugin, because access
 * to internal quote data is required for the work around.
 *
 * Overridden to fix url creation compatible w/ PayPal checkout.js V4
 *
 * @package Kurufootwear\Paypal\Model\Express
 */
class Checkout extends MagentoExpressCheckout
{
    /**
     * @var PaymentFactory
     */
    protected $paymentFactory;

    /**
     * Checkout constructor.
     * @param LoggerInterface $logger
     * @param Url $customerUrl
     * @param Data $taxData
     * @param CheckoutData $checkoutData
     * @param CustomerSession $customerSession
     * @param CacheTypeConfig $configCacheType
     * @param LocaleResolverInterface $localeResolver
     * @param Info $paypalInfo
     * @param StoreManagerInterface $storeManager
     * @param UrlInterface $coreUrl
     * @param CartFactory $cartFactory
     * @param OnepageFactory $onepageFactory
     * @param CartManagementInterface $quoteManagement
     * @param AgreementFactory $agreementFactory
     * @param Factory $apiTypeFactory
     * @param Copy $objectCopyService
     * @param Session $checkoutSession
     * @param EncryptorInterface $encryptor
     * @param ManagerInterface $messageManager
     * @param CustomerRepositoryInterface $customerRepository
     * @param AccountManagement $accountManagement
     * @param OrderSender $orderSender
     * @param CartRepositoryInterface $quoteRepository
     * @param TotalsCollector $totalsCollector
     * @param PaymentFactory $paymentFactory
     * @param array $params
     * @throws Exception
     */
    public function __construct(
        LoggerInterface $logger,
        Url $customerUrl,
        Data $taxData,
        CheckoutData $checkoutData,
        CustomerSession $customerSession,
        CacheTypeConfig $configCacheType,
        LocaleResolverInterface $localeResolver,
        Info $paypalInfo,
        StoreManagerInterface $storeManager,
        UrlInterface $coreUrl,
        CartFactory $cartFactory,
        OnepageFactory $onepageFactory,
        CartManagementInterface $quoteManagement,
        AgreementFactory $agreementFactory,
        Factory $apiTypeFactory,
        Copy $objectCopyService,
        Session $checkoutSession,
        EncryptorInterface $encryptor,
        ManagerInterface $messageManager,
        CustomerRepositoryInterface $customerRepository,
        AccountManagement $accountManagement,
        OrderSender $orderSender,
        CartRepositoryInterface $quoteRepository,
        TotalsCollector $totalsCollector,
        PaymentFactory $paymentFactory,
        $temp = [], /* TODO - Remove this before comitting, no time to cache flush to remove this */
        $params = []
    ) {
        $this->paymentFactory = $paymentFactory;

        parent::__construct(
            $logger,
            $customerUrl,
            $taxData,
            $checkoutData,
            $customerSession,
            $configCacheType,
            $localeResolver,
            $paypalInfo,
            $storeManager,
            $coreUrl,
            $cartFactory,
            $onepageFactory,
            $quoteManagement,
            $agreementFactory,
            $apiTypeFactory,
            $objectCopyService,
            $checkoutSession,
            $encryptor,
            $messageManager,
            $customerRepository,
            $accountManagement,
            $orderSender,
            $quoteRepository,
            $totalsCollector,
            $params
        );
    }

    /**
     * Overridden to fake $0 orders as $0.01 orders
     * to workaround PayPal's no-zero-total-order policy
     * to get customer shipping addresses stored in PayPal
     *
     * @param string $returnUrl
     * @param string $cancelUrl
     * @param null $button
     * @throws LocalizedException
     * @throws Exception
     *
     * @return string
     */
    public function start($returnUrl, $cancelUrl, $button = null)
    {
        $this->_quote->collectTotals();
        $this->_quote->reserveOrderId();
        $this->quoteRepository->save($this->_quote);

        // prepare API
        $this->_getApi();
        $solutionType = $this->_config->getMerchantCountry() == 'DE'
            ? Config::EC_SOLUTION_TYPE_MARK
            : $this->_config->getValue('solutionType');
        $this->_api->setAmount($this->_quote->getBaseGrandTotal())
            ->setCurrencyCode($this->_quote->getBaseCurrencyCode())
            ->setInvNum($this->_quote->getReservedOrderId())
            ->setReturnUrl($returnUrl)
            ->setCancelUrl($cancelUrl)
            ->setSolutionType($solutionType)
            ->setPaymentAction($this->_config->getValue('paymentAction'));
        if ($this->_giropayUrls) {
            list($successUrl, $cancelUrl, $pendingUrl) = $this->_giropayUrls;
            $this->_api->addData(
                [
                    'giropay_cancel_url' => $cancelUrl,
                    'giropay_success_url' => $successUrl,
                    'giropay_bank_txn_pending_url' => $pendingUrl,
                ]
            );
        }

        if ($this->_isBml) {
            $this->_api->setFundingSource('BML');
        }

        $this->_setBillingAgreementRequest();

        if ($this->_config->getValue('requireBillingAddress') == PaypalConfig::REQUIRE_BILLING_ADDRESS_ALL) {
            $this->_api->setRequireBillingAddress(1);
        }

        // suppress or export shipping address
        $address = null;
        if ($this->_quote->getIsVirtual()) {
            if ($this->_config->getValue('requireBillingAddress')
                == PaypalConfig::REQUIRE_BILLING_ADDRESS_VIRTUAL
            ) {
                $this->_api->setRequireBillingAddress(1);
            }
            $this->_api->setSuppressShipping(true);
        } else {
            $billingAddress = $this->_quote->getBillingAddress();

            if ($billingAddress) {
                $this->_api->setBillingAddress($billingAddress);
            }

            $address = $this->_quote->getShippingAddress();
            $isOverridden = 0;
            if (true === $address->validate()) {
                $isOverridden = 1;
                $this->_api->setAddress($address);
            }
            $this->_quote->getPayment()->setAdditionalInformation(
                self::PAYMENT_INFO_TRANSPORT_SHIPPING_OVERRIDDEN,
                $isOverridden
            );
            $this->_quote->getPayment()->save();
        }

        /** @var $cart Cart */
        $cart = $this->_cartFactory->create(['salesModel' => $this->_quote]);

        $this->_api->setPaypalCart($cart);

        if (!$this->_taxData->getConfig()->priceIncludesTax()) {
            $this->setShippingOptions($cart, $address);
        }

        $this->_config->exportExpressCheckoutStyleSettings($this->_api);

        /* Temporary solution. @TODO: do not pass quote into Nvp model */
        $this->_api->setQuote($this->_quote);

        /**
         * Override - force non-zero total as a workaround
         * to PayPal's no $0 order policy
         */
        if (!$this->_quote->getGrandTotal()) {
            $this->_checkoutSession->setIsPaypalFreeWorkaround(true);
            $this->_api->setAmount(0.01);
        }

        $this->_api->callSetExpressCheckout();
        $token = $this->_api->getToken();

        $this->_setRedirectUrl($button, $token);

        $payment = $this->_quote->getPayment();
        $payment->unsAdditionalInformation(self::PAYMENT_INFO_TRANSPORT_BILLING_AGREEMENT);
        // Set flag that we came from Express Checkout button
        if (!empty($button)) {
            $payment->setAdditionalInformation(self::PAYMENT_INFO_BUTTON, 1);
        } elseif ($payment->hasAdditionalInformation(self::PAYMENT_INFO_BUTTON)) {
            $payment->unsAdditionalInformation(self::PAYMENT_INFO_BUTTON);
        }
        $payment->save();

        return $token;
    }

    /**
     * Update quote when returned from PayPal
     * rewrite billing address by paypal
     * save old billing address for new customer
     * export shipping address in case address absence
     *
     * Overridden to set address from PayPal even if an old one
     * is set (guests don't have an address assigned)
     *
     * @param string $token
     * @throws LocalizedException
     * @SuppressWarnings(PHPMD.CyclomaticComplexity)
     *
     * @return void
     */
    public function returnFromPaypal($token)
    {
        $this->_getApi();
        $this->_api->setToken($token)
            ->callGetExpressCheckoutDetails();
        $quote = $this->_quote;

        $this->ignoreAddressValidation();

        // import shipping address
        $exportedShippingAddress = $this->_api->getExportedShippingAddress();

        if (!$exportedShippingAddress) {
            $this->_logger->critical(__("Paypal response seems successful, but no address in API response."));
        }

        if (!$quote->getIsVirtual()) {
            $shippingAddress = $quote->getShippingAddress();
            // Override - removed 'if shippingAddress' condition
            if ($exportedShippingAddress
                && $quote->getPayment()->getAdditionalInformation(self::PAYMENT_INFO_BUTTON) == 1
            ) {
                $this->_setExportedAddressData($shippingAddress, $exportedShippingAddress);
                // PayPal doesn't provide detailed shipping info: prefix, middlename, lastname, suffix
                $shippingAddress->setPrefix(null);
                $shippingAddress->setMiddlename(null);
                $shippingAddress->setLastname(null);
                $shippingAddress->setSuffix(null);
                $shippingAddress->setCollectShippingRates(true);
                $shippingAddress->setSameAsBilling(0);
            }

            // import shipping method
            $code = '';
            if ($this->_api->getShippingRateCode()) {
                $code = $this->_matchShippingMethodCode($shippingAddress, $this->_api->getShippingRateCode());
                if ($code) {
                    // possible bug of double collecting rates :-/
                    $shippingAddress->setShippingMethod($code)->setCollectShippingRates(true);
                }
            }
            $quote->getPayment()->setAdditionalInformation(
                self::PAYMENT_INFO_TRANSPORT_SHIPPING_METHOD,
                $code
            );
        }

        // import billing address
        $portBillingFromShipping = $quote->getPayment()->getAdditionalInformation(self::PAYMENT_INFO_BUTTON) == 1
            && $this->_config->getValue(
                'requireBillingAddress'
            ) != \Magento\Paypal\Model\Config::REQUIRE_BILLING_ADDRESS_ALL
            && !$quote->isVirtual();
        if ($portBillingFromShipping) {
            $billingAddress = clone $shippingAddress;
            $billingAddress->unsAddressId()->unsAddressType()->setCustomerAddressId(null);
            $data = $billingAddress->getData();
            $data['save_in_address_book'] = 0;
            $quote->getBillingAddress()->addData($data);
            $quote->getShippingAddress()->setSameAsBilling(1);
        } else {
            $billingAddress = $quote->getBillingAddress();
        }
        $exportedBillingAddress = $this->_api->getExportedBillingAddress();

        $this->_setExportedAddressData($billingAddress, $exportedBillingAddress);
        $billingAddress->setCustomerNote($exportedBillingAddress->getData('note'));
        $quote->setBillingAddress($billingAddress);
        $quote->setCheckoutMethod($this->getCheckoutMethod());

        // import payment info
        $payment = $quote->getPayment();
        $payment->setMethod($this->_methodType);
        $this->_paypalInfo->importToPayment($this->_api, $payment);
        $payment->setAdditionalInformation(self::PAYMENT_INFO_TRANSPORT_PAYER_ID, $this->_api->getPayerId())
            ->setAdditionalInformation(self::PAYMENT_INFO_TRANSPORT_TOKEN, $token);
        $quote->collectTotals();
        $this->quoteRepository->save($quote);
    }

    /**
     * Overridden PP quote place action to not actually try capturing
     * $0 order payments.
     *
     * @param string $token
     * @param string|null $shippingMethodCode
     * @return void
     * @SuppressWarnings(PHPMD.CyclomaticComplexity)
     * @SuppressWarnings(PHPMD.NPathComplexity)
     */
    public function place($token, $shippingMethodCode = null)
    {
        /**
         * Handle $0-total quotes with a workaround
         */
        if ($this->_checkoutSession->getIsPaypalFreeWorkaround()) {
            /**
             * Remove any set PP payments and replace with free payment
             */
            foreach ($this->_quote->getPaymentsCollection() as $payment) {
                $payment->delete();
            }
            $freePayment = $this->paymentFactory->create()
                ->setMethod(Free::PAYMENT_METHOD_FREE_CODE)
                ->setQuote($this->_quote)
                ->save();
            $this->_quote->setPayment($freePayment);
            $this->quoteRepository->save($this->_quote);

            /**
             * Original PP order placing functionality
             * (The first section without the embedded flow)
             */
            if ($shippingMethodCode) {
                $this->updateShippingMethod($shippingMethodCode);
            }

            if ($this->getCheckoutMethod() == \Magento\Checkout\Model\Type\Onepage::METHOD_GUEST) {
                $this->prepareGuestQuote();
            }

            $this->ignoreAddressValidation();
            $this->_quote->collectTotals();
            $order = $this->quoteManagement->submit($this->_quote);

            if ($order) {
                $this->_order = $order;
            }
        } else {
            parent::place($token, $shippingMethodCode);
        }
    }

    /**
     * Re-declared, because its private and is required in the override.
     * Make sure addresses will be saved without validation errors
     *
     * @return void
     */
    private function ignoreAddressValidation()
    {
        $this->_quote->getBillingAddress()->setShouldIgnoreValidation(true);
        if (!$this->_quote->getIsVirtual()) {
            $this->_quote->getShippingAddress()->setShouldIgnoreValidation(true);
            if (!$this->_config->getValue('requireBillingAddress')
                && !$this->_quote->getBillingAddress()->getEmail()
            ) {
                $this->_quote->getBillingAddress()->setSameAsBilling(1);
            }
        }
    }

    /**
     * Re-declared, because its private and is required in the override.
     * Set shipping options to api
     *
     * @param PaypalCart $cart
     * @param Address|null $address
     *
     * @return void
     */
    private function setShippingOptions(PaypalCart $cart, Address $address = null)
    {
        // for included tax always disable line items (related to paypal amount rounding problem)
        $this->_api->setIsLineItemsEnabled($this->_config->getValue(PaypalConfig::TRANSFER_CART_LINE_ITEMS));

        // add shipping options if needed and line items are available
        $cartItems = $cart->getAllItems();
        if ($this->_config->getValue(PaypalConfig::TRANSFER_CART_LINE_ITEMS)
            && $this->_config->getValue(PaypalConfig::TRANSFER_SHIPPING_OPTIONS)
            && !empty($cartItems)
        ) {
            if (!$this->_quote->getIsVirtual()) {
                $options = $this->_prepareShippingOptions($address, true);
                if ($options) {
                    $this->_api->setShippingOptionsCallbackUrl(
                        $this->_coreUrl->getUrl(
                            '*/*/shippingOptionsCallback',
                            ['quote_id' => $this->_quote->getId()]
                        )
                    )->setShippingOptions($options);
                }
            }
        }
    }
}