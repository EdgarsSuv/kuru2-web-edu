<?php
/**
 * @category     Kurufootwear
 * @package      Kurufootwear/SeoMarkup
 * @copyright    Copyright (c) 2017 Scandiweb, Inc (http://scandiweb.com)
 * @license      http://opensource.org/licenses/OSL-3.0 The Open Software License 3.0 (OSL-3.0)
 */
namespace Kurufootwear\SeoMarkup\Block\Head\Json;

use Magento\Framework\View\Element\Template\Context;
use MageWorx\SeoMarkup\Helper\Seller as HelperSeller;

class Seller extends \MageWorx\SeoMarkup\Block\Head\Json\Seller
{
    const HOMEPAGE = 'cms_index_index';

    protected $currentRoute = null;

    /**
     * @var \Magento\Framework\App\RequestInterface
     */
    protected $request;

    /**
     * Seller constructor.
     * @param HelperSeller $helperSeller
     * @param Context $context
     * @param array $data
     */
    public function __construct(
        HelperSeller $helperSeller,
        Context $context,
        array $data = []
    ){
        $this->request = $context->getRequest();
        $this->currentRoute = $this->request->getRouteName() . '_' . $this->request->getControllerName() . '_' . $this->request->getActionName();

        parent::__construct(
            $helperSeller,
            $context,
            $data
        );
    }

    /**
     *
     * {@inheritDoc}
     */
    protected function getMarkupHtml()
    {
        $html = '';

        if (!$this->helperSeller->isRsEnabled()) {
            return $html;
        }

        $sellerJsonData = $this->getJsonOrganizationData();
        $sellerJson     = $sellerJsonData  ? json_encode($sellerJsonData) : '';

        if ($sellerJsonData && ($this->currentRoute == self::HOMEPAGE)) {
            $html .= '<script type="application/ld+json">' . $sellerJson . '</script>';
        }

        return $html;
    }

    /**
     * This is needed to take the correct templates
     *
     * @return string
     */
    protected function _toHtml()
    {
        $this->setModuleName($this->extractModuleName('MageWorx\SeoMarkup\Block\Head\Json\Seller'));

        return parent::_toHtml();
    }
}
