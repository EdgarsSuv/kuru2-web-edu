<?php
/**
 * @vendor Kurufootwear
 * @module Kurufootwear_MergeCustomer
 * @author Kristaps Stalidzāns <info@scandiweb.com>
 * @copyright Copyright (c) 2017 Scandiweb, Ltd (http://scandiweb.com)
 * @license http://opensource.org/licenses/afl-3.0.php Academic Free License (AFL 3.0)
 */
namespace Kurufootwear\MergeCustomer\Controller\Adminhtml;

use Magento\Backend\App\Action\Context;
use Magento\Customer\Api\AddressRepositoryInterface;
use Magento\Customer\Api\CustomerRepositoryInterface;
use Magento\Framework\Api\SearchCriteriaBuilder;
use Magento\Framework\App\ResourceConnection;
use Magento\Sales\Api\OrderRepositoryInterface;
use Magento\Sales\Model\ResourceModel\Order\CollectionFactory;
use Magento\Vault\Api\PaymentTokenRepositoryInterface;
use Magento\Wishlist\Model\WishlistFactory;
use Magento\Newsletter\Model\SubscriberFactory;
use Scandiweb\MergeCustomer\Logger\Logger;
use Mirasvit\Rewards\Model\ResourceModel\Transaction\CollectionFactory as TransactionCollectionFactory;
use Mirasvit\Rewards\Model\ResourceModel\ReferralLink\CollectionFactory as ReferralLinkCollectionFactory;
use Mirasvit\Rewards\Model\ResourceModel\Earning\Rule\Queue\CollectionFactory as QueueCollectionFactory;
use Magento\Framework\Data\Collection\AbstractDb as DbCollection;

abstract class Merge extends \Scandiweb\MergeCustomer\Controller\Adminhtml\Merge
{
    /**
     * @var TransactionCollectionFactory
     */
    protected $rewardsTransactionCollectionFactory;

    /**
     * @var ReferralLinkCollectionFactory
     */
    protected $rewardsReferralLinkCollectionFactory;

    /**
     * @var QueueCollectionFactory
     */
    protected $rewardsQueueCollectionFactory;

    /**
     * Merge constructor.
     * @param Context $context
     * @param CollectionFactory $orderCollectionFactory
     * @param SearchCriteriaBuilder $searchCriteriaBuilder
     * @param OrderRepositoryInterface $orderRepository
     * @param AddressRepositoryInterface $addressRepository
     * @param WishlistFactory $wishlistFactory
     * @param PaymentTokenRepositoryInterface $paymentTokenRepository
     * @param CustomerRepositoryInterface $customerRepository
     * @param ResourceConnection $resourceConnection
     * @param SubscriberFactory $subscriberFactory
     * @param Logger $logger
     * @param TransactionCollectionFactory $rewardsTransactionCollectionFactory
     * @param ReferralLinkCollectionFactory $rewardsReferralLinkCollectionFactory
     * @param QueueCollectionFactory $rewardsQueueCollectionFactory
     */
    public function __construct
    (
        Context $context,
        CollectionFactory $orderCollectionFactory,
        SearchCriteriaBuilder $searchCriteriaBuilder,
        OrderRepositoryInterface $orderRepository,
        AddressRepositoryInterface $addressRepository,
        WishlistFactory $wishlistFactory,
        PaymentTokenRepositoryInterface $paymentTokenRepository,
        CustomerRepositoryInterface $customerRepository,
        ResourceConnection $resourceConnection,
        SubscriberFactory $subscriberFactory,
        Logger $logger,
        TransactionCollectionFactory $rewardsTransactionCollectionFactory,
        ReferralLinkCollectionFactory $rewardsReferralLinkCollectionFactory,
        QueueCollectionFactory $rewardsQueueCollectionFactory
    )
    {
        $this->rewardsTransactionCollectionFactory = $rewardsTransactionCollectionFactory;
        $this->rewardsReferralLinkCollectionFactory = $rewardsReferralLinkCollectionFactory;
        $this->rewardsQueueCollectionFactory = $rewardsQueueCollectionFactory;

        parent::__construct(
            $context,
            $orderCollectionFactory,
            $searchCriteriaBuilder,
            $orderRepository,
            $addressRepository,
            $wishlistFactory,
            $paymentTokenRepository,
            $customerRepository,
            $resourceConnection,
            $subscriberFactory,
            $logger
        );
    }

    /**
     * @param DbCollection $collection
     * @param $masterUserId
     * @param $selectedCustomerId
     * @param string $filter
     */
    protected function saveCollectionItems(DbCollection $collection, $masterUserId, $selectedCustomerId, $filter = 'customer_id')
    {
        $collection->addFieldToFilter($filter, $masterUserId);

        foreach ($collection as $item) {
            $item->setCustomerId($selectedCustomerId);
            $item->save();
        }
    }
}
