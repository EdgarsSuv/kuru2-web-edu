<?php
/**
 * @vendor Kurufootwear
 * @module Kurufootwear_MergeCustomer
 * @author Kristaps Stalidzāns <info@scandiweb.com>
 * @copyright Copyright (c) 2017 Scandiweb, Ltd (http://scandiweb.com)
 * @license http://opensource.org/licenses/afl-3.0.php Academic Free License (AFL 3.0)
 */
namespace Kurufootwear\MergeCustomer\Controller\Adminhtml\Customer;

use Kurufootwear\MergeCustomer\Controller\Adminhtml\Merge;
use Magento\Framework\Controller\ResultInterface;
use Magento\Framework\Controller\ResultFactory;

class MergeAction extends Merge
{
    /**
     * Merges Customer with switching ids then deletes the merged customer
     *
     * @return ResultInterface
     */
    public function execute()
    {
        $params = $this->getRequest()->getParams();
        $resultRedirect = $this->resultRedirect->create(ResultFactory::TYPE_REDIRECT);

        try {
            $direction = $params['action'];
            $masterUserId = $params['id'];
            $selectedUserId = $params['entity_id'];

            if ($direction == Merge::MERGE_IN) {
                $masterUserId = $params['id'];
                $selectedUserId = $params['entity_id'];
            } elseif ($direction == Merge::MERGE_FROM) {
                $masterUserId = $params['entity_id'];
                $selectedUserId = $params['id'];
            }

            $this->logger->addNotice(
                __(
                    'Merge Direction: ' . $direction .
                    ' Selected Master User: ' . $masterUserId .
                    ' Selected user: ' . $selectedUserId
                )
            );

            $this->saveRepositoryItem($this->orderRepository, $masterUserId, $selectedUserId, 'customer_id');
            $this->logger->addNotice(__('Orders merged Successfully'));

            $this->saveRepositoryItem($this->paymentTokenRepository, $masterUserId, $selectedUserId, 'customer_id');
            $this->logger->addNotice(__('Payment tokens merged Successfully'));

            $this->saveRepositoryItem($this->addressRepository, $masterUserId, $selectedUserId, 'parent_id');
            $this->logger->addNotice(__('Addresses merged Successfully'));

            $this->mergeWishlistItems($selectedUserId, $masterUserId);
            $this->logger->addNotice(__('Wishlist items merged Successfully'));

            $this->mergeReviews($selectedUserId, $masterUserId);
            $this->logger->addNotice(__('Reviews merged Successfully'));

            $this->mergeSubscriptions($selectedUserId, $masterUserId);
            $this->logger->addNotice(__('Newsletters Subscriptions merged Successfully'));

            $rewardsTransaction = $this->rewardsTransactionCollectionFactory->create();
            $this->saveCollectionItems($rewardsTransaction, $masterUserId, $selectedUserId);
            $rewardsReferralLink = $this->rewardsReferralLinkCollectionFactory->create();
            $this->saveCollectionItems($rewardsReferralLink, $masterUserId, $selectedUserId);
            $rewardsQueue = $this->rewardsQueueCollectionFactory->create();
            $this->saveCollectionItems($rewardsQueue, $masterUserId, $selectedUserId);
            $this->logger->addNotice(__('Rewards merged Successfully'));

            $this->deleteCustomer($masterUserId);
            $this->messageManager->addSuccessMessage(__('You Merged successfully user with id ' . $masterUserId . ' to user id ' . $selectedUserId));
            $resultRedirect->setPath('customer/index/edit/', ['id' => $selectedUserId, '_current' => true]);

            return $resultRedirect;
        } catch (\Exception $e) {
            $resultRedirect->setPath('customer/index/edit/', ['id' => $params['id'], '_current' => true]);
            $this->logger->addError(__($e->getMessage()));
            $this->messageManager->addErrorMessage(__('Something failed Please check logs.'));

            return $resultRedirect;
        }
    }
}
