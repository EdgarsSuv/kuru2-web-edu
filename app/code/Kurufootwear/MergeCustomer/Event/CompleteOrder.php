<?php
/**
 * @vendor Kurufootwear
 * @module Kurufootwear_MergeCustomer
 * @author Kristaps Stalidzāns <info@scandiweb.com>
 * @copyright Copyright (c) 2017 Scandiweb, Ltd (http://scandiweb.com)
 * @license http://opensource.org/licenses/afl-3.0.php Academic Free License (AFL 3.0)
 */
namespace Kurufootwear\MergeCustomer\Event;

use Magento\Framework\Event\Observer;
use Magento\Sales\Api\Data\OrderInterface;
use Magento\Sales\Model\Order;

class CompleteOrder extends \Amazon\Payment\Observer\CompleteOrder
{
    /**
     * Fixes on save stored data issues for customer merging
     *
     * @param Observer $observer
     */
    public function execute(Observer $observer)
    {
        /**
         * @var OrderInterface $order
         */
        $order = $observer->getOrder();
        $complete = Order::STATE_COMPLETE;

        if ($order->getState() == $complete && $order->getData('state') !== $complete) {
            $this->closeOrderReference($order);
        }
    }
}
