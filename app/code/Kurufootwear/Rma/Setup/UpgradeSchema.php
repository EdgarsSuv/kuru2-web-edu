<?php
/**
 * @category    Kurufootwear
 * @package     Kurufootwear\Rma
 * @author      Kristaps Stalidzāns <info@scandiweb.com>
 * @copyright   Copyright (c) 2017 Scandiweb, Inc (http://scandiweb.com)
 * @license     http://opensource.org/licenses/OSL-3.0 The Open Software License 3.0 (OSL-3.0)
 */
namespace Kurufootwear\Rma\Setup;

use Magento\Framework\DB\Ddl\Table;
use Magento\Framework\DB\Adapter\AdapterInterface;
use Magento\Framework\Setup\SchemaSetupInterface;
use Magento\Framework\Setup\UpgradeSchemaInterface;
use Magento\Framework\Setup\ModuleContextInterface;

class UpgradeSchema implements UpgradeSchemaInterface
{
    /**
     * {@inheritdoc}
     */
    public function upgrade(SchemaSetupInterface $setup, ModuleContextInterface $context)
    {
        $setup->startSetup();

        if (version_compare($context->getVersion(), '1.0.1', '<')) {
            $this->addQuestionIdentifier($setup);
            $this->addWarehouseTable($setup);
            $this->addCustomerExchangeTable($setup);
            $this->changeSizeForStatusIds($setup);
        }
    
        if (version_compare($context->getVersion(), '1.0.2', '<')) {
            $this->addShippingLabelTable($setup);
        }
    
        if (version_compare($context->getVersion(), '1.0.6', '<')) {
            $this->addShippingProviderField($setup);
        }

        $setup->endSetup();
    }

    /**
     * @param SchemaSetupInterface $setup
     */
    private function addQuestionIdentifier(SchemaSetupInterface $setup)
    {
        // Get custom fields table
        $tableName = $setup->getTable('aw_rma_custom_field');

        // Check if the table already exists
        if ($setup->getConnection()->isTableExists($tableName)) {
            $connection = $setup->getConnection();
            $connection->addColumn(
                $tableName,
                'identifier',
                [
                    'type' => Table::TYPE_TEXT,
                    'nullable' => false,
                    'length' => 255,
                    'comment' => 'Adds question identifier',
                    'after' => 'id'
                ]
            );
            $connection->addIndex(
                $tableName,
                $setup->getIdxName('aw_rma_custom_field', ['identifier']),
                ['identifier', 'id'],
                AdapterInterface::INDEX_TYPE_UNIQUE
            );
        }
    }
    
    private function addWarehouseTable(SchemaSetupInterface $setup) 
    {
        $tableName = $setup->getTable('kuru_rma_warehouse_inspection');
        if ($setup->getConnection()->isTableExists($tableName) != true) {
            $table = $setup->getConnection()
                ->newTable($tableName)
                ->addColumn(
                    'id',
                    Table::TYPE_INTEGER,
                    null,
                    ['identity' => true, 'unsigned' => true, 'nullable' => false, 'primary' => true],
                    'Record ID'
                )
                ->addColumn(
                    'request_id',
                    Table::TYPE_INTEGER,
                    null,
                    ['nullable' => false, 'unsigned' => true],
                    'RMA request ID'
                )
                ->addColumn(
                    'inspected_date',
                    Table::TYPE_TIMESTAMP,
                    null,
                    ['nullable' => false, 'default' => Table::TIMESTAMP_INIT],
                    'Date inspected'
                )
                ->addColumn(
                    'inspector_initials',
                    Table::TYPE_TEXT,
                    null,
                    ['nullable' => false],
                    'Initials of inspector'
                )
                ->addColumn(
                    'received_items',
                    Table::TYPE_TEXT,
                    null,
                    ['nullable' => false],
                    'Items received'
                )
                ->addColumn(
                    'items_match',
                    Table::TYPE_BOOLEAN,
                    null,
                    ['nullable' => false],
                    'Items received match items in request'
                )
                ->addColumn(
                    'other_skus',
                    Table::TYPE_TEXT,
                    null,
                    ['nullable' => true, 'default' => ''],
                    'SKUs of items received that are not in request'
                )
                ->addColumn(
                    'warehouse_notes',
                    Table::TYPE_TEXT,
                    null,
                    ['nullable' => true, 'default' => ''],
                    'Warehouse notes'
                )
                ->setComment('RMA Warehouse Inspection Table')
                ->setOption('type', 'InnoDB')
                ->setOption('charset', 'utf8');
            $setup->getConnection()->createTable($table);
        }
    }
    
    private function addCustomerExchangeTable(SchemaSetupInterface $setup)
    {
        $tableName = $setup->getTable('kuru_rma_customer_exchange');
        if ($setup->getConnection()->isTableExists($tableName) != true) {
            $table = $setup->getConnection()
                ->newTable($tableName)
                ->addColumn(
                    'id',
                    Table::TYPE_INTEGER,
                    null,
                    ['identity' => true, 'unsigned' => true, 'nullable' => false, 'primary' => true],
                    'Record ID'
                )
                ->addColumn(
                    'date_added',
                    Table::TYPE_TIMESTAMP,
                    null,
                    ['nullable' => false, 'default' => Table::TIMESTAMP_INIT],
                    'Datetime record was added'
                )
                ->addColumn(
                    'customer_id',
                    Table::TYPE_INTEGER,
                    null,
                    ['nullable' => false, 'unsigned' => true],
                    'Customer ID'
                )
                ->addColumn(
                    'request_id',
                    Table::TYPE_INTEGER,
                    null,
                    ['nullable' => false, 'unsigned' => true],
                    'Request ID'
                )
                ->addColumn(
                    'points_issued',
                    Table::TYPE_INTEGER,
                    null,
                    ['nullable' => false, 'unsigned' => true],
                    'Number of reward points issued for this exchange'
                )
                ->addColumn(
                    'pending_order_id',
                    Table::TYPE_INTEGER,
                    null,
                    ['nullable' => true, 'unsigned' => true],
                    'The ID of the new order with which the points were used.'
                )
                ->addColumn(
                    'rma_complete',
                    Table::TYPE_BOOLEAN,
                    null,
                    ['nullable' => true, 'default' => '0'],
                    'Flag for the completion of the RMA'
                )
                ->setComment('RMA Customer Exchange Points Table')
                ->setOption('type', 'InnoDB')
                ->setOption('charset', 'utf8');
            $setup->getConnection()->createTable($table);
        }
    }
    
    private function changeSizeForStatusIds(SchemaSetupInterface $setup)
    {
        // *** Need to increase the size of these columns to hold all custom statuses. ***
        $tableName = $setup->getTable('aw_rma_custom_field');
        if ($setup->getConnection()->isTableExists($tableName) == true) {
            $connection = $setup->getConnection();
            $connection->changeColumn(
                $tableName,
                'visible_for_status_ids',
                'visible_for_status_ids',
                [
                    'type' => Table::TYPE_TEXT,
                    'size' => null,
                    ['nullable' => false],
                    'comment' => 'Visible For Status Ids'
                ]
            );
            $connection->changeColumn(
                $tableName,
                'editable_for_status_ids',
                'editable_for_status_ids',
                [
                    'type' => Table::TYPE_TEXT,
                    'size' => null,
                    ['nullable' => false],
                    'comment' => 'Editable For Status Ids'
                ]
            );
            $connection->changeColumn(
                $tableName,
                'editable_admin_for_status_ids',
                'editable_admin_for_status_ids',
                [
                    'type' => Table::TYPE_TEXT,
                    'size' => null,
                    ['nullable' => false],
                    'comment' => 'Editable By Admin For Status Ids'
                ]
            );
        }
    }
    
    private function addShippingLabelTable(SchemaSetupInterface $setup)
    {
        $tableName = $setup->getTable('kuru_rma_shipping_label');
        if ($setup->getConnection()->isTableExists($tableName) != true) {
            $table = $setup->getConnection()
                ->newTable($tableName)
                ->addColumn(
                    'id',
                    Table::TYPE_INTEGER,
                    null,
                    ['identity' => true, 'unsigned' => true, 'nullable' => false, 'primary' => true],
                    'Record ID'
                )
                ->addColumn(
                    'request_id',
                    Table::TYPE_INTEGER,
                    null,
                    ['nullable' => false, 'unsigned' => true],
                    'Request ID'
                )
                ->addColumn(
                    'label_pdf',
                    Table::TYPE_BLOB,
                    null,
                    ['nullable' => false],
                    'RMA Shipping Label'
                )
                ->addColumn(
                    'tracking_number',
                    Table::TYPE_TEXT,
                    null,
                    ['nullable' => false, 'default' => ''],
                    'Warehouse notes'
                )
                ->setComment('RMA Shipping Labels')
                ->setOption('type', 'InnoDB')
                ->setOption('charset', 'utf8');
            $setup->getConnection()->createTable($table);
        }
    }
    
    /**
     * Adds the `provider` column to the `kuru_rma_shipping_label` table.
     * 
     * @param SchemaSetupInterface $setup
     */
    private function addShippingProviderField(SchemaSetupInterface $setup)
    {
        $tableName = $setup->getTable('kuru_rma_shipping_label');
    
        if (!$setup->getConnection()->tableColumnExists($tableName, 'provider')) {
            $setup->getConnection()
                ->addColumn(
                    $tableName,
                    'provider',
                    [
                        'type' => Table::TYPE_TEXT,
                        'length' => '8',
                        'nullable' => true,
                        'comment' => 'Shipping Provider'
                    ]
                );
        }
    }
}
