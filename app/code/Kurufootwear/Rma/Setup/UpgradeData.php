<?php
/**
 * @category    Kurufootwear
 * @package     Kurufootwear\Rma
 * @author      Jim McGowen <jim@kurufootwear.com>
 * @copyright   Copyright (c) 2017 KURU Footwear. All rights reserved.
 */

namespace Kurufootwear\Rma\Setup;

use Magento\Framework\Setup\UpgradeDataInterface;
use Magento\Framework\Setup\ModuleDataSetupInterface;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\App\Config\Storage\WriterInterface as Config;
use Kurufootwear\Rma\Model\Config\RmaShippingCarrier;

class UpgradeData implements UpgradeDataInterface
{
    private $config;
    
    public function __construct(
        Config $config
    )
    {
        $this->config = $config;
    }
    
    public function upgrade(ModuleDataSetupInterface $setup, ModuleContextInterface $context)
    {
        $setup->startSetup();
    
        if (version_compare($context->getVersion(), '1.0.1', '<')) {
            $this->initializeIdentifierFields($setup);
        }
    
        if (version_compare($context->getVersion(), '1.0.2', '<')) {
            $this->addShippingLabelCarrierConfig($setup);
        }
        
        if (version_compare($context->getVersion(), '1.0.3', '<')) {
            $this->updateShippingLabelCarrierConfig($setup);
        }
    
        if (version_compare($context->getVersion(), '1.0.5', '<')) {
            $this->addOnHoldStatus($setup);
        }
    
        $setup->endSetup();
    }
    
    private function initializeIdentifierFields(ModuleDataSetupInterface $setup)
    {
        $rmaCustomFieldTable = $setup->getTable('aw_rma_custom_field');
        $select = $setup->getConnection()->select()->from(
            $rmaCustomFieldTable,
            ['id', 'name', 'identifier']
        )->where(
            'is_system = ?',
            1
        );
    
        $customFields = $setup->getConnection()->fetchAll($select);
        foreach ($customFields as $field) {
            // Use name, make lowercase and replace spaces with underscores
            $identifier = str_replace(' ', '_', strtolower($field['name']));
            $bind = ['identifier' => $identifier];
            $where = ['id = ?' => (int)$field['id']];
            $setup->getConnection()->update($rmaCustomFieldTable, $bind, $where);
        }
    }
    
    private function addShippingLabelCarrierConfig(ModuleDataSetupInterface $setup)
    {
        $this->config->save('kuru/rma/carrier', 'fedex');
    }
    
    private function updateShippingLabelCarrierConfig(ModuleDataSetupInterface $setup) 
    {
        $this->config->save(RmaShippingCarrier::CARRIER_CONFIG_PATH, 'fedex');
        $this->config->delete('kuru/rma/carrier');
    }
    
    private function addOnHoldStatus(ModuleDataSetupInterface $setup)
    {
        $select = $setup->getConnection()->select()->from(
            'aw_rma_request_status',
            ['id']
        )->where(
            'id = ?',
            31
        );
    
        $data = $setup->getConnection()->fetchOne($select);
        if (empty($data)) {
            $setup->getConnection()->insert('aw_rma_request_status', [
                'id' => 31,
                'name' => 'On Hold',
                'is_email_customer' => 0,
                'is_email_admin' => 0,
                'is_thread' => 0
            ]);
        }
    
        $select = $setup->getConnection()->select()->from(
            'aw_rma_status_attr_value',
            ['id']
        )->where(
            'status_id = ?',
            31
        );
    
        $data = $setup->getConnection()->fetchOne($select);
        if (empty($data)) {
            $setup->getConnection()->insert('aw_rma_status_attr_value', [
                'status_id' => 31,
                'store_id' => 1,
                'attribute_code' => 'frontend_label',
                'value' => 'On Hold'
            ]);
            $setup->getConnection()->insert('aw_rma_status_attr_value', [
                'status_id' => 31,
                'store_id' => 1,
                'attribute_code' => 'template_to_admin',
                'value' => ''
            ]);
            $setup->getConnection()->insert('aw_rma_status_attr_value', [
                'status_id' => 31,
                'store_id' => 1,
                'attribute_code' => 'template_to_customer',
                'value' => ''
            ]);
            $setup->getConnection()->insert('aw_rma_status_attr_value', [
                'status_id' => 31,
                'store_id' => 1,
                'attribute_code' => 'template_to_thread',
                'value' => ''
            ]);
        }
    
        $select = $setup->getConnection()->select()
            ->from('aw_rma_custom_field', ['identifier', 'visible_for_status_ids', 'editable_admin_for_status_ids'])
            ->where('identifier = ?','other_description')
            ->orWhere('identifier = ?', 'type')
            ->orWhere('identifier = ?', 'admin_notes')
            ->orWhere('identifier = ?', 'reason');
        $rows = $setup->getConnection()->fetchAssoc($select);
        foreach ($rows as &$row) {
            $changed = false;
            if ($row['identifier'] != 'admin_notes') {
                $data = unserialize($row['visible_for_status_ids']);
                if (!in_array(31, $data)) {
                    $data[] = '31';
                    $row['visible_for_status_ids'] = serialize($data);
                    $changed = true;
                }
            }
            
            $data = unserialize($row['editable_admin_for_status_ids']);
            if (!in_array(31, $data)) {
                $data[] = '31';
                $row['editable_admin_for_status_ids'] = serialize($data);
                $changed = true;
            }
    
            if ($changed) {
                $bind = [
                    'visible_for_status_ids' => $row['visible_for_status_ids'],
                    'editable_admin_for_status_ids' => $row['editable_admin_for_status_ids']
                ];
                $where = ['identifier = ?' => $row['identifier']];
                $setup->getConnection()->update('aw_rma_custom_field', $bind, $where);
            }
        }
    }
}
