<?php
/**
 * @category    Kurufootwear
 * @package     Kurufootwear\Rma
 * @author      Jim McGowen <jim@kurufootwear.com>
 * @copyright   Copyright (c) 2018 KURU Footwear. All rights reserved.
 */

namespace Kurufootwear\Rma\Helper\ShippingLabel\Carrier;

use Kurufootwear\Rma\Model\ShippingLabelFactory;
use Magento\Ups\Model\Carrier as UpsCarrier;
use Magento\Shipping\Model\Shipment\RequestFactory as ShippmentRequestFactory;
use Magento\Shipping\Model\Shipment\ReturnShipmentFactory;
use Magento\Store\Model\Information;
use Magento\Store\Model\StoreManagerInterface;
use Magento\Framework\App\Config\ScopeConfigInterface;
use Magento\Directory\Model\Region;
use Magento\Catalog\Api\ProductRepositoryInterface;
use Magento\Shipping\Model\Shipping\LabelGenerator;
use Kurufootwear\Rma\Exception\ShippingLabelException;

/**
 * Class UPS
 * @package Kurufootwear\Rma\Helper\ShippingLabel\Carrier
 */
class UPS extends AbstractCarrier implements CarrierInterface
{
    /** @var UpsCarrier  */
    private $upsCarrier;
    
    private $shipmentRequestFactory;
    
    /** @var ReturnShipmentFactory  */
    private $returnShipmentFactory;
    
    /** @var Information  */ 
    private $storeInfo;
    
    /** @var StoreManagerInterface  */
    private $storeManager;
    
    /** @var ScopeConfigInterface  */
    private $config;
    
    /** @var Region  */
    private $region;
    
    /** @var ProductRepositoryInterface  */
    private $productRepository;
    
    /** @var LabelGenerator  */
    private $labelGenerator;
    
    /**
     * UPS constructor.
     *
     * @param ShippingLabelFactory $shippingLabelFactory
     * @param UpsCarrier $upsCarrier
     * @param ReturnShipmentFactory $returnShipmentFactory
     * @param Information $storeInfo
     * @param StoreManagerInterface $storeManager
     * @param ScopeConfigInterface $config
     * @param Region $region
     * @param ProductRepositoryInterface $productRepository
     * @param LabelGenerator $labelGenerator
     */
    public function __construct(
        ShippingLabelFactory $shippingLabelFactory,
        UpsCarrier $upsCarrier,
        ShippmentRequestFactory $shipmentRequestFactory,
        ReturnShipmentFactory $returnShipmentFactory,
        Information $storeInfo,
        StoreManagerInterface $storeManager,
        ScopeConfigInterface $config,
        Region $region,
        ProductRepositoryInterface $productRepository,
        LabelGenerator $labelGenerator
    )
    {
        parent::__construct($shippingLabelFactory);
        
        $this->upsCarrier = $upsCarrier;
        $this->shipmentRequestFactory = $shipmentRequestFactory;
        $this->returnShipmentFactory = $returnShipmentFactory;
        $this->storeInfo = $storeInfo;
        $this->storeManager = $storeManager;
        $this->config = $config;
        $this->region = $region;
        $this->productRepository = $productRepository;
        $this->labelGenerator = $labelGenerator;
    }
    
    /**
     * @param \Kurufootwear\Rma\Model\Request $rma
     *
     * @throws ShippingLabelException
     */
    public function requestLabel($rma)
    {
        $order = $rma->getOrder();
        $orderShipment = $order->getShipmentsCollection()->getFirstItem();
        if (empty($orderShipment) || !$orderShipment->hasData()) {
            throw new ShippingLabelException(__('Could no create shipping label because the order has not shipped.'));
        }
        
        $address = $order->getShippingAddress();
        
        $store = $this->storeManager->getStore(null);
        $storeInfo = $this->storeInfo->getStoreInformationObject($store);
        
        $recipientCountryId = $this->config->getValue('shipping/origin/country_id');
        $recipientRegionCode = $this->config->getValue('shipping/origin/region_id');
        if (is_numeric($recipientRegionCode)) {
            $recipientRegionCode = $this->region->load($recipientRegionCode)->getCode();
        }
    
        if ($address->getCountryId() != 'US' || $address->getRegionCode() == 'AK' || $address->getRegionCode() == 'HI') {
            // Can not continue
            throw new ShippingLabelException(__('Can not create RMA shipping label for the region %1, %2', $address->getCountryId(), $address->getRegionCode()));
        }
        
        $orderItems = $order->getItems();
        
        $packages = array();
        
        $shippingWeight = 0;
        $items = $rma->getItemsCollection();
        if ($items->getSize() == 0) {
            throw new ShippingLabelException(__('RMA request contains no items.'));
        }
        foreach ($items as $item) {
            $itemWeight = $orderItems[$item->getItemId()]->getWeight();
            if (empty($itemWeight)) {
                // Default to this in case the item is missing weight
                $itemWeight = '3.0000';
            }
            $shippingWeight += $itemWeight;
            
            $packages[1]['params']['container'] = '02';
            $packages[1]['params']['weight'] = $itemWeight;
            $packages[1]['params']['customs_value'] = 139;
            $packages[1]['params']['length'] = '';
            $packages[1]['params']['width'] = '';
            $packages[1]['params']['height'] = '';
            $packages[1]['params']['weight_units'] = 'POUND';
            $packages[1]['params']['dimension_units'] = 'INCH';
            $packages[1]['params']['content_type'] = '';
            $packages[1]['params']['content_type_other'] = '';
            $packages[1]['items'][$item->getItemId()]['qty'] = $item->getQty();
            $packages[1]['items'][$item->getItemId()]['customs_value'] = $item->getPrice();
            $packages[1]['items'][$item->getItemId()]['price'] = $item->getPrice();
            $packages[1]['items'][$item->getItemId()]['name'] = $item->getName();
            $packages[1]['items'][$item->getItemId()]['weight'] = $itemWeight;
            $packages[1]['items'][$item->getItemId()]['product_id'] = $item->getProductId();
            $packages[1]['items'][$item->getItemId()]['order_item_id'] = $item->getItemId();
        }
    
        $returnShipment = $this->returnShipmentFactory->create();
    
        $returnShipment->setRecipientContactCompanyName('KURU Footwear');
        $returnShipment->setRecipientContactPhoneNumber($storeInfo->getPhone());
        $returnShipment->setRecipientAddressStreet($this->config->getValue('shipping/origin/street_line1'));
        $returnShipment->setRecipientAddressStreet1($this->config->getValue('shipping/origin/street_line1'));
        $returnShipment->setRecipientAddressStreet2($this->config->getValue('shipping/origin/street_line2'));
        $returnShipment->setRecipientAddressCity($this->config->getValue('shipping/origin/city'));
        $returnShipment->setRecipientAddressStateOrProvinceCode($recipientRegionCode);
        $returnShipment->setRecipientAddressPostalCode($this->config->getValue('shipping/origin/postcode'));
        $returnShipment->setRecipientAddressCountryCode($recipientCountryId);
        
        $returnShipment->setShipperContactPersonName(trim($address->getName()));
        $returnShipment->setShipperContactCompanyName($address->getCompany());
        $returnShipment->setShipperContactPhoneNumber($address->getTelephone());
        $returnShipment->setShipperAddressStreet1($address->getStreetLine(1));
        $returnShipment->setShipperAddressStreet2($address->getStreetLine(2));
        $returnShipment->setShipperAddressCity($address->getCity());
        $returnShipment->setShipperAddressStateOrProvinceCode($address->getRegionCode());
        $returnShipment->setShipperAddressPostalCode($address->getPostcode());
        $returnShipment->setShipperAddressCountryCode($address->getCountryId());
        
        $returnShipment->setPackageWeight($shippingWeight);
        $returnShipment->setPackages($packages);
        $returnShipment->setOrderShipment($orderShipment);
        $returnShipment->setShippingMethod('03'); // GROUND
        $returnShipment->setPackagingType('02'); // Customer supplied
        
        $returnShipment->setReferenceData('ORDER ' . $order->getIncrementId() . ' RMA ' . $rma->getId() . '  P');
        
        try {
            $response = $this->upsCarrier->returnOfShipment($returnShipment);
            if ($response->hasErrors()) {
                throw new ShippingLabelException(__('Error requesting shipping label. %1', $response->getErrors()));
            }
            if (!$response->hasInfo()) {
                throw new ShippingLabelException(__('Error requesting shipping label'));
            }
    
            $info = $response->getInfo()[0];
            // TODO: Multiple info?
    
            /** @var \Zend_Pdf $pdf */
            $pdf = new \Zend_Pdf();
            $pdf->pages[] = $this->labelGenerator->createPdfPageFromImageString($info['label_content']);
    
            $this->saveShippingLabel($rma, $pdf, $info['tracking_number'], 'UPS');
        }
        catch (\Exception $e) {
            throw new ShippingLabelException(__($e->getMessage()));
        }
    }
}
