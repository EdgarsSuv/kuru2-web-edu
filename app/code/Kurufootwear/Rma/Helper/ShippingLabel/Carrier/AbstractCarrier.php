<?php
/**
 * @category    Kurufootwear
 * @package     Kurufootwear\Rma
 * @author      Jim McGowen <jim@kurufootwear.com>
 * @copyright   Copyright (c) 2017 KURU Footwear. All rights reserved.
 */

namespace Kurufootwear\Rma\Helper\ShippingLabel\Carrier;

use Kurufootwear\Rma\Model\ShippingLabelFactory;
use Kurufootwear\Rma\Model\Request;

/**
 * Class AbstractCarrier
 * @package Kurufootwear\Rma\Helper\ShippingLabel\Carrier
 */
abstract class AbstractCarrier
{
    protected $shippingLabelFactory;
    
    /**
     * AbstractCarrier constructor.
     *
     * @param ShippingLabelFactory $shippingLabelFactory
     */
    public function __construct(
        ShippingLabelFactory $shippingLabelFactory
    )
    {
        $this->shippingLabelFactory = $shippingLabelFactory;
    }
    
    /**
     * @param \Kurufootwear\Rma\Model\Request $rma
     * @param \Zend_Pdf $pdf
     * @param string $trackingNumber
     * @param string $provider
     *
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    protected function saveShippingLabel($rma, $pdf, $trackingNumber, $provider)
    {
        $shippingLabel = $this->shippingLabelFactory->create()->loadByRequestId($rma->getId());
        $shippingLabel->setRequestId($rma->getId());
        $shippingLabel->setLabelPdf(base64_encode($pdf->render()));
        $shippingLabel->setTrackingNumber($trackingNumber);
        $shippingLabel->setProvider($provider);
        $shippingLabel->save();
    }
}
