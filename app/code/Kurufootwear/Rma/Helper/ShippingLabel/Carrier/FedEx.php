<?php
/**
 * @category    Kurufootwear
 * @package     Kurufootwear\Rma
 * @author      Jim McGowen <jim@kurufootwear.com>
 * @copyright   Copyright (c) 2017 KURU Footwear. All rights reserved.
 */

namespace Kurufootwear\Rma\Helper\ShippingLabel\Carrier;

use Kurufootwear\Rma\Model\ShippingLabelFactory;
use Magento\Fedex\Model\Carrier;
use Magento\Shipping\Model\Shipment\ReturnShipmentFactory;
use Magento\Store\Model\Information;
use Magento\Store\Model\StoreManagerInterface;
use Magento\Framework\App\Config\ScopeConfigInterface;
use Magento\Directory\Model\Region;
use Magento\Catalog\Api\ProductRepositoryInterface;
use Magento\Shipping\Model\Shipping\LabelGenerator;
use Kurufootwear\Rma\Exception\ShippingLabelException;

/**
 * Class FedEx
 * @package Kurufootwear\Rma\Helper\ShippingLabel\Carrier
 */
class FedEx extends AbstractCarrier implements CarrierInterface
{
    /** @var Carrier  */
    private $fedExCarrier;
    
    /** @var ReturnShipmentFactory  */
    private $returnShipmentFactory;
    
    /** @var Information  */ 
    private $storeInfo;
    
    /** @var StoreManagerInterface  */
    private $storeManager;
    
    /** @var ScopeConfigInterface  */
    private $config;
    
    /** @var Region  */
    private $region;
    
    /** @var ProductRepositoryInterface  */
    private $productRepository;
    
    /** @var LabelGenerator  */
    private $labelGenerator;
    
    /**
     * FedEx constructor.
     *
     * @param ShippingLabelFactory $shippingLabelFactory
     * @param Carrier $fedExCarrier
     * @param ReturnShipmentFactory $returnShipmentFactory
     * @param Information $storeInfo
     * @param StoreManagerInterface $storeManager
     * @param ScopeConfigInterface $config
     * @param Region $region
     * @param ProductRepositoryInterface $productRepository
     * @param LabelGenerator $labelGenerator
     */
    public function __construct(
        ShippingLabelFactory $shippingLabelFactory,
        Carrier $fedExCarrier,
        ReturnShipmentFactory $returnShipmentFactory,
        Information $storeInfo,
        StoreManagerInterface $storeManager,
        ScopeConfigInterface $config,
        Region $region,
        ProductRepositoryInterface $productRepository,
        LabelGenerator $labelGenerator
    )
    {
        parent::__construct($shippingLabelFactory);
        
        $this->fedExCarrier = $fedExCarrier;
        $this->returnShipmentFactory = $returnShipmentFactory;
        $this->storeInfo = $storeInfo;
        $this->storeManager = $storeManager;
        $this->config = $config;
        $this->region = $region;
        $this->productRepository = $productRepository;
        $this->labelGenerator = $labelGenerator;
    }
    
    /**
     * @param \Kurufootwear\Rma\Model\Request $rma
     *
     * @throws ShippingLabelException
     */
    public function requestLabel($rma)
    {
        $order = $rma->getOrder();
        $orderShipment = $order->getShipmentsCollection()->getFirstItem();
        if (empty($orderShipment) || !$orderShipment->hasData()) {
            throw new ShippingLabelException(__('Could no create shipping label because the order has not shipped.'));
        }
        
        $address = $order->getShippingAddress();
        
        $store = $this->storeManager->getStore(null);
        $storeInfo = $this->storeInfo->getStoreInformationObject($store);
        
        $recipientCountryId = $this->config->getValue('shipping/origin/country_id');
        $recipientRegionCode = $this->config->getValue('shipping/origin/region_id');
        if (is_numeric($recipientRegionCode)) {
            $recipientRegionCode = $this->region->load($recipientRegionCode)->getCode();
        }
    
        if ($address->getCountryId() != 'US' || $address->getRegionCode() == 'AK' || $address->getRegionCode() == 'HI') {
            // Can not continue
            throw new ShippingLabelException(__('Can not create RMA shipping label for the region %1, %2', $address->getCountryId(), $address->getRegionCode()));
        }
        
        $orderItems = $order->getItems();
        
        $packages = array();
        $shippingWeight = 0;
        $items = $rma->getItemsCollection();
        if ($items->getSize() == 0) {
            throw new ShippingLabelException(__('RMA request contains no items.'));
        }
        foreach ($items as $item) {
            $shippingWeight += $orderItems[$item->getItemId()]->getWeight();
            
            $packages[1]['params']['container'] = 'YOUR_PACKAGING';
            $packages[1]['params']['weight'] = $orderItems[$item->getItemId()]->getWeight();
            $packages[1]['params']['customs_value'] = 139;
            $packages[1]['params']['length'] = '';
            $packages[1]['params']['width'] = '';
            $packages[1]['params']['height'] = '';
            $packages[1]['params']['weight_units'] = 'POUND';
            $packages[1]['params']['dimension_units'] = 'INCH';
            $packages[1]['params']['content_type'] = '';
            $packages[1]['params']['content_type_other'] = '';
            $packages[1]['params']['delivery_confirmation'] = 'NO_SIGNATURE_REQUIRED';
            $packages[1]['items'][$item->getItemId()]['qty'] = $item->getQty();
            $packages[1]['items'][$item->getItemId()]['customs_value'] = $item->getPrice();
            $packages[1]['items'][$item->getItemId()]['price'] = $item->getPrice();
            $packages[1]['items'][$item->getItemId()]['name'] = $item->getName();
            $packages[1]['items'][$item->getItemId()]['weight'] = $orderItems[$item->getItemId()]->getWeight();
            $packages[1]['items'][$item->getItemId()]['product_id'] = $item->getProductId();
            $packages[1]['items'][$item->getItemId()]['order_item_id'] = $item->getItemId();
        }
        
        $data = [
            'shipper_contact_person_name' => trim($address->getName()),
            'shipper_contact_person_first_name' => $address->getFirstname(),
            'shipper_contact_person_last_name' => $address->getLastname(),
            'shipper_contact_company_name' => $address->getCompany(),
            'shipper_contact_phone_number' => $address->getTelephone(),
            'shipper_email' => $address->getEmail(),
            'shipper_address_street' => trim($address->getStreetLine(1) . ' ' . $address->getStreetLine(2)),
            'shipper_address_street1' => $address->getStreetLine(1),
            'shipper_address_street2' => $address->getStreetLine(2),
            'shipper_address_city' => $address->getCity(),
            'shipper_address_state_or_province_code' => $address->getRegionCode(),
            'shipper_address_postal_code' => $address->getPostcode(),
            'shipper_address_country_code' => $address->getCountryId(),
            
            'recipient_contact_person_name' => 'KURU Footwear',
            'recipient_contact_person_first_name' => 'KURU Guru',
            'recipient_contact_person_last_name' => 'Customer Care',
            'recipient_contact_company_name' => '',
            'recipient_contact_phone_number' => $storeInfo->getPhone(),
            'recipient_email' => $this->config->getValue('trans_email/ident_general/email'),
            'recipient_address_street' => $this->config->getValue('shipping/origin/address1'),
            'recipient_address_street1' => $this->config->getValue('shipping/origin/street_line1'),
            'recipient_address_street2' => $this->config->getValue('shipping/origin/street_line2'),
            'recipient_address_city' => $this->config->getValue('shipping/origin/city'),
            'recipient_address_state_or_province_code' => $recipientRegionCode,
            'recipient_address_region_code' => $recipientRegionCode,
            'recipient_address_postal_code' => $this->config->getValue('shipping/origin/postcode'),
            'recipient_address_country_code' => $recipientCountryId,
            
            'shipping_method' => 'FEDEX_GROUND',
            'package_weight' => $shippingWeight,
            'base_currency_code' => $order->getBaseCurrencyCode(),
            'store_id' => $order->getStoreId(),
            // Adding P at the end because Magento's FedEx implementation always adds the package number at the end whether you want it or not... lame.
            'reference_data' => 'ORDER ' . $order->getIncrementId() . ' RMA ' . $rma->getId() . '  P',
            'packages' => $packages,
            'order_shipment' => $orderShipment
        ];
    
        $returnShipment = $this->returnShipmentFactory->create();
        $returnShipment->setData($data);
    
        try {
            $response = $this->fedExCarrier->returnOfShipment($returnShipment);
            if ($response->hasErrors()) {
                throw new ShippingLabelException(__('Error requesting shipping label. %1', $response->getErrors()));
            }
            if (!$response->hasInfo()) {
                throw new ShippingLabelException(__('Error requesting shipping label'));
            }
    
            $info = $response->getInfo()[0];
            // TODO: Multiple info?
    
            /** @var \Zend_Pdf $pdf */
            $pdf = new \Zend_Pdf();
            $pdf->pages[] = $this->labelGenerator->createPdfPageFromImageString($info['label_content']);
    
            $this->saveShippingLabel($rma, $pdf, $info['tracking_number'], 'FEDEX');
        }
        catch (\Exception $e) {
            throw new ShippingLabelException(__($e->getMessage()));
        }
    }
}
