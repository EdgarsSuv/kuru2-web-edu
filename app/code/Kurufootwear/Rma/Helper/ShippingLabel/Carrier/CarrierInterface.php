<?php
/**
 * @category    Kurufootwear
 * @package     Kurufootwear\Rma
 * @author      Jim McGowen <jim@kurufootwear.com>
 * @copyright   Copyright (c) 2017 KURU Footwear. All rights reserved.
 */

namespace Kurufootwear\Rma\Helper\ShippingLabel\Carrier;

use Kurufootwear\Rma\Model\Request;
use Kurufootwear\Rma\Exception\ShippingLabelException;

/**
 * Interface CarrierInterface
 * @package Kurufootwear\Rma\Helper\ShippingLabel\Carrier
 */
interface CarrierInterface
{
    /**
     * @param $rma Request
     * @throws ShippingLabelException
     */
    public function requestLabel($rma);
}
