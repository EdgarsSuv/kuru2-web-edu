<?php
/**
 * @category    Kurufootwear
 * @package     Kurufootwear\Rma
 * @author      Kristaps Stalidzāns <info@scandiweb.com>
 * @copyright   Copyright (c) 2017 Scandiweb, Inc (http://scandiweb.com)
 * @license     http://opensource.org/licenses/OSL-3.0 The Open Software License 3.0 (OSL-3.0)
 */

namespace Kurufootwear\Rma\Helper;

use Aheadworks\Rma\Helper\Order as AheadworksOrder;
use Kurufootwear\Sales\Model\Order\Item;
use Magento\Bundle\Model\Product\Type;
use Magento\Sales\Model\Order as OrderModel;
use Magento\Sales\Model\Order\Item as OrderItem;

class Order extends AheadworksOrder
{
    /**
     * Return how many returnable items are in order
     * @param OrderModel $order
     * @return bool
     */
    public function getOrderLeftCount($order)
    {
        $count = 0;

        /** @var Item $item */
        foreach ($order->getItemsCollection() as $item) {
            /**
             * Check only non-configurables
             * as only they appear in order views
             */
            if (!is_null($item->getParentItemId())) {
                $maxItems = $this->getItemMaxCount($item);
                $allowedForOrder = $this->isAllowedForOrder($order);
                $itemFinished = !($maxItems && $allowedForOrder);

                if (!$itemFinished) {
                    $count++;
                }
            }
        }

        return $count;
    }

    /**
     * Retrieves maximal order item count available for RMA
     * Override to change $item->getQtyInvoiced() to $item->getQtyOrdered()
     *
     * @param OrderItem $item
     *
     * @return int
     */
    public function getItemMaxCount(OrderItem $item)
    {
        $max = 0;

        if ($item->getProductType() == Type::TYPE_CODE) {
            if ($item->getChildrenItems()) {
                foreach ($item->getChildrenItems() as $childrenItem) {
                    $childrenMax = $childrenItem->getQtyInvoiced() - $childrenItem->getQtyRefunded();
                    $requestItems = $this->getAllRequestItems($item->getData('order_id'));
                    foreach ($requestItems as $requestItem) {
                        if ($requestItem->getItemId() == $childrenItem->getId()) {
                            $childrenMax -= $requestItem->getQty();
                        }
                    }
                    $max += $childrenMax;
                }
            }
        } else {
            /* Changed $item->getQtyInvoiced() to $item->getQtyOrdered() */
            $max = $item->getQtyOrdered() - $item->getQtyRefunded();
            $requestItems = $this->getAllRequestItems($item->getData('order_id'));
            foreach ($requestItems as $requestItem) {
                if ($requestItem->getItemId() == $item->getId()) {
                    $max -= $requestItem->getQty();
                }
            }
        }

        return max($max, 0);
    }
}