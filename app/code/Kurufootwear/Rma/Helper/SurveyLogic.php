<?php
/**
 * @category    Kurufootwear
 * @package     Kurufootwear\Rma
 * @author      Krisjanis Veinbahs <info@scandiweb.com>
 * @copyright   Copyright (c) 2017 KURU Footwear. All rights reserved.
 */

namespace Kurufootwear\Rma\Helper;

use Kurufootwear\Migration\Setup\Migration\RmaFieldMigration;
use Kurufootwear\Rma\Controller\Customer\Success;
use Kurufootwear\Rma\Controller\Customer\Failure;
use Kurufootwear\Rma\Exception\ShippingLabelException;
use Kurufootwear\Rma\Model\CustomField;
use Kurufootwear\Rma\Model\Request;
use Kurufootwear\Rma\Model\RequestItem;
use Kurufootwear\Rma\Model\RequestItemFactory;
use Kurufootwear\Rma\Model\ResourceModel\RequestItem\Collection;
use Kurufootwear\Rma\Model\Source\CustomField\Type;
use Kurufootwear\Rma\Model\Source\Request\Status;
use Kurufootwear\Rma\Model\Survey;
use Kurufootwear\Rma\Model\SurveyAction;
use Kurufootwear\Rma\Model\SurveyItem;
use Kurufootwear\Rma\Model\RequestManager;
use Magento\Framework\Exception\LocalizedException;
use Magento\Framework\Message\ManagerInterface;
use Magento\Store\Model\StoreManagerInterface;
use Magento\Framework\UrlInterface;

class SurveyLogic
{
    /**
     * @var SurveyAction
     */
    protected $surveyAction;

    /**
     * @var SurveyItem
     */
    protected $surveyItem;

    /**
     * @var SurveyQuestions
     */
    protected $surveyQuestions;

    /**
     * @var RequestManager
     */
    protected $requestManager;

    /**
     * @var RequestItemFactory
     */
    protected $requestItemFactory;

    /**
     * @var StoreManagerInterface
     */
    protected $storeManager;

    /**
     * @var UrlInterface
     */
    protected $urlBuilder;

    /**
     * @var CustomField
     */
    protected $customField;

    /**
     * @var Collection
     */
    protected $requestItemCollection;

    /**
     * @var ManagerInterface
     */
    protected $messageManager;

    /**
     * SurveyLogic constructor.
     * @param SurveyAction $surveyAction
     * @param SurveyItem $surveyItem
     * @param SurveyQuestions $surveyQuestions
     * @param RequestItemFactory $requestItemFactory
     * @param RequestManager $requestManager
     * @param StoreManagerInterface $storeManager
     * @param UrlInterface $urlBuilder
     * @param CustomField $customField
     * @param Collection $requestItemCollection
     * @param ManagerInterface $messageManager
     */
    public function __construct(
        SurveyAction $surveyAction,
        SurveyItem $surveyItem,
        SurveyQuestions $surveyQuestions,
        RequestItemFactory $requestItemFactory,
        RequestManager $requestManager,
        StoreManagerInterface $storeManager,
        UrlInterface $urlBuilder,
        CustomField $customField,
        Collection $requestItemCollection,
        ManagerInterface $messageManager
    )
    {
        $this->surveyAction = $surveyAction;
        $this->surveyItem = $surveyItem;
        $this->surveyQuestions = $surveyQuestions;
        $this->requestManager = $requestManager;
        $this->requestItemFactory = $requestItemFactory;
        $this->storeManager = $storeManager;
        $this->urlBuilder = $urlBuilder;
        $this->customField = $customField;
        $this->requestItemCollection = $requestItemCollection;
        $this->messageManager = $messageManager;
    }

    /**
     * Survey business logic
     * @param Survey $survey
     * @return array
     */
    public function generateAction($survey)
    {
        $currentAction = $survey->getCurrentAction();
        $surveyAction = null;

        if ($currentAction == null) {
            // Survey just initialized
            // Get first question
            $firstItem = $survey->getFirstItem();
            $firstItemFullId = $this->surveyItem->getItemFullId($firstItem);
            $surveyAction = $this->loadSurveyQuestion('affirm_is_new')
                ->setActionId(0)
                ->setActionItemFullId($firstItemFullId);
        } else {
            $currentActionId = $currentAction[SurveyAction::ACTION_ID];
            $currentQuestion = $currentAction[SurveyAction::QUESTION_IDENTIFIER];
            $currentAnswerIdentifier = array_keys($currentAction[SurveyAction::QUESTION_ANSWER])[0];
            $currentItemFullId = $currentAction[SurveyAction::ACTION_ITEM_FULL_ID];
            $nextItemFullId = $survey->nextItemFullId();

            $isLastItem = $survey->isLastItem();
            $isMultipleItems = count(array_keys($survey->getItems())) > 1;

            switch($currentQuestion) {
                case 'affirm_is_new': {
                    switch ($currentAnswerIdentifier) {
                        case 'yes':
                            $surveyAction = $this->loadSurveyQuestion('reason_return')
                                ->setActionId($currentActionId + 1)
                                ->setActionItemFullId($currentItemFullId);
                            break;
                        case 'no':
                            // Shoe isn't new, go to failure page
                            $survey->setIsExchange(false)->save();
                            $surveyAction = $this->createSurveyRedirect(Failure::FAILURE_URL)
                                ->setActionItemColor($currentAction[SurveyAction::ACTION_ITEM_COLOR])
                                ->setActionItemSize($currentAction[SurveyAction::ACTION_ITEM_SIZE])
                                ->setActionItemThumbnail($currentAction[SurveyAction::ACTION_ITEM_THUMBNAIL])
                                ->setActionItemTitle($currentAction[SurveyAction::ACTION_ITEM_TITLE]);
                            break;
                    }
                    break;
                }
                case 'reason_return': {
                    switch ($currentAnswerIdentifier) {
                        case 'no_fit':
                            $surveyAction = $this->loadSurveyQuestion('no_fit_specify')
                                ->setActionId($currentActionId + 1)
                                ->setActionItemFullId($currentItemFullId);
                            break;
                        case 'no_color':
                        case 'no_style': {
                            if ($isMultipleItems && $isLastItem) {
                                // Return RMA
                                $survey->setIsExchange(false)->save();
                                $surveyAction = $this->createSurveyRedirect(Success::RESOLUTION_URL);
                            } else if ($isMultipleItems && !$isLastItem) {
                                // Continue surveying next items
                                $surveyAction = $this->loadSurveyQuestion('affirm_is_new')
                                    ->setActionId(0)
                                    ->setActionItemFullId($nextItemFullId);
                            } else {
                                // Survey if this one item should be exchanged for points
                                $surveyAction = $this->loadSurveyQuestion('affirm_exchange')
                                    ->setActionId($currentActionId + 1)
                                    ->setActionItemFullId($currentItemFullId);
                            }
                            break;
                        }
                        case 'reason_other':
                            $surveyAction = $this->loadSurveyQuestion('reason_return_other')
                                ->setActionId($currentActionId + 1)
                                ->setActionItemFullId($currentItemFullId);
                            break;
                    }
                    break;
                }
                case 'no_fit_specify':
                case 'reason_return_other': {
                    if ($isMultipleItems && $isLastItem) {
                        // Return RMA
                        $survey->setIsExchange(false)->save();
                        $surveyAction = $this->createSurveyRedirect(Success::RESOLUTION_URL);
                    } else if ($isMultipleItems && !$isLastItem) {
                        // Continue surveying next items
                        $surveyAction = $this->loadSurveyQuestion('affirm_is_new')
                            ->setActionId(0)
                            ->setActionItemFullId($nextItemFullId);
                    } else {
                        // Survey if this one item should be exchanged for points
                        $surveyAction = $this->loadSurveyQuestion('affirm_exchange')
                            ->setActionId($currentActionId + 1)
                            ->setActionItemFullId($currentItemFullId);
                    }
                    break;
                }

                case 'affirm_exchange': {
                    switch ($currentAnswerIdentifier) {
                        case 'yes':
                            // Exchange RMA
                            $survey->setIsExchange(true)->save();
                            $surveyAction = $this->createSurveyRedirect(Success::RESOLUTION_URL);
                            break;
                        case 'no':
                            // Return RMA
                            $survey->setIsExchange(false)->save();
                            $surveyAction = $this->createSurveyRedirect(Success::RESOLUTION_URL);
                            break;
                    }

                    break;
                }
            }
        }

        return $surveyAction->get();
    }

    /**
     * Save survey to AheadWorks RMA and
     * optionally issue reward points
     * Return RMA request id or null
     * @param Survey $survey
     * @return mixed|null
     */
    public function saveReturn($survey)
    {
        /**
         * Just return RMA request id,
         * if already finished
         */
        if ($survey->getIsFinished()) {
            return $survey->getRequestId();
        }

        /**
         * Catch empty survey and save it
         * safely without making an RMA request
         */
        if (count($survey->getItems()) == 0) {
            $survey->setIsFinished(true)
                ->setRequestId(null)
                ->save();

            return null;
        }

        /**
         * Set fallback return values
         * if request creation is unsuccessful
         */
        $requestId = null;
        $survey->setIsFinished(true)
            ->setRequestId($requestId)
            ->save();

        try {
            /**
             * Generate RMA request data
             */
            $guestMode = false;
            $requestComment = '-'; // Out Survey UI doesn't include request comments
            $requestedOrder = $survey->getOrder();
            $formKey = $requestedOrder['form_key'];
            $orderId = $requestedOrder['order_id'];
    
            $data = [
                'form_key' => $formKey,
                'order_id' => $orderId,
                'text' => $requestComment
            ];
    
            /**
             * Create RMA request items
             */
            $items = $survey->getItems();
            $itemValues = array_values($items);
            $requestItems = [];
            for ($i = 0; $i < count($itemValues); $i++) {
                /** @var RequestItem $requestItem */
                $requestItem = $this->requestItemFactory->create();
                $item = $itemValues[$i];
                $itemId = $item[SurveyItem::ITEM_ID];
                $itemQty = $requestedOrder['item'][$itemId]['qty'];
        
                $requestItem->setItemId($itemId)
                    ->setQty($itemQty);
                $requestItems[$i] = $requestItem->getData();
                $requestItems[$i]['custom_fields'] = [];
            }
            $data['items'] = $requestItems;
            
            // Sanity check, do we have any items?
            if (empty($data['items'])) {
                throw new LocalizedException(__('No items for request.'));
            }
            
            /**
             * Create RMA request
             * and issue points if it's exchange
             * (important to note, the RMA type is not set on creation, only after)
             * @var Request $request
             */
            $request = $this->requestManager->create($data, $guestMode);
            if ($survey->getIsExchange()) {
                $request->setStatusId(Status::POINTS_ISSUED)
                    ->setType(RequestManager::REQUEST_TYPE_EXCHANGE)
                    ->issueExchangePoints();
                $request->save();
            } else {
                $request->setStatusId(Status::APPROVED)
                    ->setType(RequestManager::REQUEST_TYPE_RETURN)
                    ->save();
            }
    
            /**
             * Send the new RMA email.
             * (this has to be done after the RMA type has been set)
             */
            $this->requestManager->sendNewRequestEmail($request);

            $requestId = $request->getId();
            $requestItems = $this->requestItemCollection->addRequestFilter($requestId)->load();
    
            /**
             * Add options to RMA Request items
             */
            foreach ($requestItems as $requestItem) {
                $qtyId = 0;
                $item = $items[$requestItem->getItemId() . '_' . $qtyId];
                $actions = array_values($item[SurveyItem::ACTIONS]);
        
                for ($j = 0; $j < count($actions); $j++) {
                    $action = $actions[$j];
                    $type = $action[SurveyAction::TYPE];
                    if ($type == SurveyAction::TYPE_QUESTION) {
                        $questionIdentifier = $action[SurveyAction::QUESTION_IDENTIFIER];
                        $questionAnswer = $action[SurveyAction::QUESTION_ANSWER];
                        $answerKey = array_keys($questionAnswer)[0];
                        $answerValue = array_values($questionAnswer)[0];
                        $reasonFieldId = $this->customField->load('reason')->getId();
                        $otherFieldId = $this->customField->load('other_description')->getId();
                        // Set custom field reason - other/wrong color/wrong style
                        if ($questionIdentifier == 'reason_return' && (
                                $answerKey == RmaFieldMigration::NO_COLOR ||
                                $answerKey == RmaFieldMigration::NO_STYLE ||
                                $answerKey == RmaFieldMigration::REASON_OTHER
                            )
                        ) {
                            $label = RmaFieldMigration::$fields['reason']['choices'][$answerKey];
                            $requestItem->setCustomFieldValue($reasonFieldId, $label);
                        }
                        // Set custom field reason - wrong size
                        if ($questionIdentifier == 'no_fit_specify') {
                            $label = RmaFieldMigration::$fields['reason']['choices'][$answerKey];
                            $requestItem->setCustomFieldValue($reasonFieldId, $label);
                        }
                        // Set custom field other reason description
                        if ($questionIdentifier == 'reason_return_other') {
                            $requestItem->setCustomFieldValue($otherFieldId, $answerValue);
                        }
                    }
                }
        
                $requestItem->save();
            }
    
            $survey->setRequestId($requestId)
                ->save();

        } catch (ShippingLabelException $e) {
            $this->messageManager->addException($e, $e->getMessage());

            return null;
        } catch (\Exception $e) {
            $this->messageManager->addException($e, __('Something went wrong while saving request. ') . $e->getMessage());

            return null;
        }

        return $requestId;
    }

    /**
     * Create a survey action for a page redirect
     * @param string $relativePath
     * @return SurveyAction
     */
    public function createSurveyRedirect($relativePath)
    {
        $url = $this->urlBuilder->getBaseUrl() . $relativePath;

        $this->surveyAction->set(array())
            ->setType(SurveyAction::TYPE_REDIRECT)
            ->setRedirectUrl($url)
            ->setRedirectPath($relativePath);

        return $this->surveyAction;
    }

    /**
     * Retrieve Aheadworks custom field by identifier
     * and convert it to a survey action (question)
     * @param $identifier
     * @return SurveyAction
     */
    public function loadSurveyQuestion($identifier)
    {
        $field = $this->surveyQuestions->questions[$identifier];

        $this->surveyAction->set(array())
            ->setType(SurveyAction::TYPE_QUESTION)
            ->setQuestionIdentifier($identifier)
            ->setQuestionType($field['type'])
            ->setQuestionTitle($field['title'])
            ->setQuestionRequired($field['required']);

        if ($field['type'] === Type::RADIO_BUTTON_VALUE) {
            $this->surveyAction->setQuestionChoices($field['choices']);
        }

        return $this->surveyAction;
    }
}
