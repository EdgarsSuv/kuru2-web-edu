<?php
/**
 * @category    Kurufootwear
 * @package     Kurufootwear\Rma
 * @author      Kristaps Stalidzāns <info@scandiweb.com>
 * @copyright   Copyright (c) 2017 Scandiweb, Inc (http://scandiweb.com)
 * @license     http://opensource.org/licenses/OSL-3.0 The Open Software License 3.0 (OSL-3.0)
 */
namespace Kurufootwear\Rma\Helper;

use Kurufootwear\Rma\Model\Source\CustomField\Type;

class CustomField extends \Aheadworks\Rma\Helper\CustomField
{
    /**
     * @var array
     */
    private $rendererClassMap = [
        Type::TEXT_VALUE            => 'Aheadworks\Rma\Block\CustomField\Input\Renderer\Text',
        Type::TEXT_AREA_VALUE       => 'Aheadworks\Rma\Block\CustomField\Input\Renderer\Textarea',
        Type::SELECT_VALUE          => 'Aheadworks\Rma\Block\CustomField\Input\Renderer\Select',
        Type::MULTI_SELECT_VALUE    => 'Aheadworks\Rma\Block\CustomField\Input\Renderer\Multiselect',
        Type::RADIO_BUTTON_VALUE    => 'Kurufootwear\Rma\Block\CustomField\Input\Renderer\Radio'
    ];

    /**
     * @param int $type
     * @return string
     */
    public function getElementRendererClass($type)
    {
        return $this->rendererClassMap[$type];
    }
}
