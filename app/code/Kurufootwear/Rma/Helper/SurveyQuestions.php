<?php
/**
 * @category  Kurufootwear
 * @package   Kurufootwear\Rma
 * @author    Krisjanis Veinbahs <info@scandiweb.com>
 * @copyright Copyright (c) 2017 Scandiweb, Inc (http://scandiweb.com)
 * @license   http://opensource.org/licenses/OSL-3.0 The Open Software License 3.0 (OSL-3.0)
 */
namespace Kurufootwear\Rma\Helper;

use Aheadworks\Rma\Model\Source\CustomField\Refers;
use Kurufootwear\Migration\Setup\Migration\RmaFieldMigration;
use Kurufootwear\Rma\Model\Source\CustomField\Type;

class SurveyQuestions
{
    /**
     * Rma survey questions
     * @var array
     */
    public $questions = [
        'affirm_is_new' => [
            'title' => 'Can this item be sold as new?',
            'required' => true,
            'type' => Type::RADIO_BUTTON_VALUE,
            'refers' => Refers::ITEM_VALUE,
            'choices' => [
                'yes' => 'Yes',
                'no' => 'No'
            ]
        ],
        'reason_return' => [
            'title' => 'What is your reason for returning this item?',
            'required' => true,
            'type' => Type::RADIO_BUTTON_VALUE,
            'refers' => Refers::ITEM_VALUE,
            'choices' => [
                'no_fit' => 'Didn’t fit',
                RmaFieldMigration::NO_COLOR => 'Unhappy with color',
                RmaFieldMigration::NO_STYLE => 'Unhappy with style',
                RmaFieldMigration::REASON_OTHER => 'Other reason'
            ]
        ],
        'no_fit_specify' => [
            'title' => 'What didn\'t fit?',
            'required' => true,
            'type' => Type::RADIO_BUTTON_VALUE,
            'refers' => Refers::ITEM_VALUE,
            'choices' => [
                RmaFieldMigration::TOO_LARGE => '<i class="fa fa-expand"></i>  Length too large',
                RmaFieldMigration::TOO_SMALL => '<i class="fa fa-compress"></i>  Length too small',
                RmaFieldMigration::TOO_NARROW => '<i class="fa fa-expand width-narrow"></i> Width too narrow',
                RmaFieldMigration::TOO_WIDE => '<i class="fa fa-compress width-wide"></i> Width too wide'
            ]
        ],
        'reason_return_other' => [
            'title' => 'Please describe the reason for returning this item.',
            'required' => true,
            'type' => Type::TEXT_AREA_VALUE,
            'refers' => Refers::ITEM_VALUE
        ],
        'affirm_exchange' => [
            'title' => 'Would you like to exchange this item?',
            'required' => true,
            'type' => Type::RADIO_BUTTON_VALUE,
            'refers' => Refers::ITEM_VALUE,
            'choices' => [
                'yes' => 'Yes',
                'no' => 'No'
            ]
        ]
    ];
}
