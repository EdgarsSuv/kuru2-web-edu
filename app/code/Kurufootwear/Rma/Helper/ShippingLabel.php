<?php
/**
 * @category    Kurufootwear
 * @package     Kurufootwear\Rma
 * @author      Jim McGowen <jim@kurufootwear.com>
 * @copyright   Copyright (c) 2017 KURU Footwear. All rights reserved.
 */

namespace Kurufootwear\Rma\Helper;

use Magento\Framework\App\Config\ScopeConfigInterface;
use Kurufootwear\Rma\Helper\ShippingLabel\Carrier\CarrierInterface;
use Kurufootwear\Rma\Helper\ShippingLabel\Carrier\FedExFactory;
use Kurufootwear\Rma\Helper\ShippingLabel\Carrier\UPSFactory;
use Kurufootwear\Rma\Model\ShippingLabelFactory;
use Kurufootwear\Rma\Exception\ShippingLabelException;
use Kurufootwear\Rma\Model\Config\RmaShippingCarrier;

/**
 * Class ShippingLabel
 * @package Kurufootwear\Rma\Helper
 */
class ShippingLabel
{
    const CARRIER_FEDEX = 'fedex';
    const CARRIER_UPS = 'ups';
    // TODO: Future carriers
    const CARRIER_USPS = 'usps';
    
    /** @var ScopeConfigInterface  */
    protected $config;
    
    /** @var  CarrierInterface */
    protected $carrier;
    
    /** @var FedExFactory  */
    protected $fedExFactory;
    
    /** @var UPSFactory */
    protected $upsFactory;
    
    /** @var ShippingLabelFactory  */
    protected $shippingLabelFactory;
    
    /**
     * ShippingLabel constructor.
     *
     * @param ScopeConfigInterface $config
     * @param FedExFactory $fedExFactory
     * @param UPSFactory $upsFactory
     * @param ShippingLabelFactory $shippingLabelFactory
     */
    public function __construct(
        ScopeConfigInterface $config,
        FedExFactory $fedExFactory,
        UPSFactory $upsFactory,
        ShippingLabelFactory $shippingLabelFactory
    )
    {
        $this->config = $config;
        $this->fedExFactory = $fedExFactory;
        $this->upsFactory = $upsFactory;
        $this->shippingLabelFactory = $shippingLabelFactory;
    }
    
    /**
     * @param $rma
     *
     * @return mixed
     * @throws ShippingLabelException
     */
    public function getLabel($rma)
    {
        $label = $this->shippingLabelFactory->create()->loadByRequestId($rma->getId());
        if (empty($label->getId())) {
            $this->createLabel($rma);
            $label = $this->shippingLabelFactory->create()->loadByRequestId($rma->getId());
        }
        
        return $label;
    }
    
    /**
     * @param $rma
     * @param null $carrier
     *
     * @throws ShippingLabelException
     */
    private function createLabel($rma, $carrier = null)
    {
        if (!empty($carrier) || empty($this->carrier)) {
            $this->setCarrier($carrier);
        }
        
        $this->carrier->requestLabel($rma);
    }
    
    /**
     * @param null $carrier string
     *
     * @throws ShippingLabelException
     */
    private function setCarrier($carrier = null)
    {
        if (empty($carrier)) {
            $carrier = $this->config->getValue(RmaShippingCarrier::CARRIER_CONFIG_PATH);
        }

        switch ($carrier) {
            case self::CARRIER_FEDEX: {
                /** @var \Kurufootwear\Rma\Helper\ShippingLabel\Carrier\FedEx carrier */
                $this->carrier = $this->fedExFactory->create();
                break;
            }
            case self::CARRIER_UPS: {
                /** @var \Kurufootwear\Rma\Helper\ShippingLabel\Carrier\UPS carrier */
                $this->carrier = $this->upsFactory->create();
                break;
            }
            case self::CARRIER_USPS: {
                throw new ShippingLabelException(__("Carrier '$carrier' not yet supported."));
            }
            default:{
                throw new ShippingLabelException(__("Unknown carrier '$carrier'"));
            }
        }
    }
}
