<?php
/**
 * @category    Kurufootwear
 * @package     Kurufootwear\Rma
 * @author      Jim McGowen <jim@kurufootwear.com>
 * @copyright   Copyright (c) 2017 KURU Footwear. All rights reserved.
 */

namespace Kurufootwear\Rma\Ui\Component\Listing\Column;

use Magento\Framework\View\Element\UiComponentFactory;
use Magento\Framework\View\Element\UiComponent\ContextInterface;
use Kurufootwear\Rma\Model\RequestFactory as RmaRequestFactory;

class AdminNotes extends \Magento\Ui\Component\Listing\Columns\Column implements \Magento\Framework\Option\ArrayInterface
{
    /** @var RmaRequestFactory  */
    protected $rmaRequestFactory;
    
    protected $options;
    
    /**
     * AdminNotes constructor.
     *
     * @param ContextInterface $context
     * @param UiComponentFactory $uiComponentFactory
     * @param RmaRequestFactory $rmaRequestFactory
     * @param array $components
     * @param array $data
     */
    public function __construct(
        ContextInterface $context,
        UiComponentFactory $uiComponentFactory,
        RmaRequestFactory $rmaRequestFactory,
        array $components = [],
        array $data = []
    ) {
        parent::__construct(
            $context,
            $uiComponentFactory,
            $components,
            $data
        );
        
        $this->rmaRequestFactory = $rmaRequestFactory;
    }

    /**
     * Prepare Data Source
     *
     * @param array $dataSource
     * @return array
     */
    public function prepareDataSource(array $dataSource)
    {
        if (isset($dataSource['data']['items'])) {
            foreach ($dataSource['data']['items'] as & $item) {
                /** @var \Kurufootwear\Rma\Model\Request $rma */
                $rma = $this->rmaRequestFactory->create()->load($item['id']);
                $adminNotes = trim($rma->getAdminNotes());
                $item['admin_notes'] = empty($adminNotes) ? 'No' : 'Yes';
            }
        }
        return $dataSource;
    }
    
    public function toOptionArray()
    {
        $this->options = [
            [
                'label' => ' ',
                'value' => ''
            ],
            [
                'label' => 'Yes',
                'value' => 'Yes'
            ],
            [
                'label' => 'No',
                'value' => 'No'
            ]
        ];
        return $this->options;
    }
}
