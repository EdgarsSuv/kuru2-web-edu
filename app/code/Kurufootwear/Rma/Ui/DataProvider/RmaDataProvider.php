<?php

namespace Kurufootwear\Rma\Ui\DataProvider;

use \Aheadworks\Rma\Model\ResourceModel\Request\CollectionFactory;
use Exception;

/**
 * Class RmaDataProvider
 */
class RmaDataProvider extends \Aheadworks\Rma\Ui\DataProvider\RmaDataProvider
{
    /**
     * @return array
     */
    public function getData()
    {
        if (!$this->getCollection()->isLoaded()) {
            $this->getCollection()
                ->joinCustomFieldValues($this->customFieldCollection)
                ->joinOrders()
                ->load();
            foreach ($this->getCollection() as $request) {
                $request->setData(
                    'products',
                    $request->getItemsCollection()->joinReason()->toArray(['product_id', 'name', 'reason'])
                );
            }
        }
        return parent::getData();
    }
    
    /**
     * @inheritdoc
     */
    public function addFilter(\Magento\Framework\Api\Filter $filter)
    {
        $dbSelectFieldName = $filter->getField();
        if ( $dbSelectFieldName == 'id') {
            $dbSelectFieldName = 'main_table.id';
        }
        else if ($dbSelectFieldName == 'store_id') {
            $dbSelectFieldName = 'main_table.store_id';
        }
        else if ($dbSelectFieldName == 'order_id') {
            $dbSelectFieldName = 'order_table.increment_id';
        }
        else if ($dbSelectFieldName == 'customer_id') {
            $dbSelectFieldName = 'customer_name';
        }
        else if ($dbSelectFieldName == 'created_at') {
            $dbSelectFieldName = 'main_table.created_at';
        }
        else if ($dbSelectFieldName == 'updated_at') {
            $dbSelectFieldName = 'main_table.updated_at';
        }
        elseif (preg_match('/^cf[0-9]+_value$/', $dbSelectFieldName)) {
            $dbSelectFieldName = str_replace('_', '.', $dbSelectFieldName);
        }
    
        if ($dbSelectFieldName == 'admin_notes') {
            $dbSelectFieldName = "TRIM(IFNULL(`cf7`.`value`,''))";
            
            if (in_array('Yes', $filter->getValue()) && in_array('No', $filter->getValue())) {
                $this->getCollection()->getSelect()
                    ->where("$dbSelectFieldName != '' OR $dbSelectFieldName = ''");
            }
            else if (in_array('Yes', $filter->getValue())) {
                $this->getCollection()->getSelect()->where("$dbSelectFieldName != ''");
            }
            else if (in_array('No', $filter->getValue())) {
                $this->getCollection()->getSelect()->where("$dbSelectFieldName = ''");
            }
        }
        else {
            $this->getCollection()->addFieldToFilter(
                $dbSelectFieldName,
                [$filter->getConditionType() => $filter->getValue()]
            );
        }

        $this->getCollection();
    }

    /**
     * Returns SearchResult
     *
     * @return null
     */
    public function getSearchResult()
    {
        try {
            return $this->getCollection()
                ->joinCustomFieldValues($this->customFieldCollection)
                ->joinOrders();
        } catch (Exception $exception) {
            /**
             * Aheadworks joinCustomFieldValues doesn't have checks
             * for whether the custom fields are already joined -> skip joins
             */
            return $this->getCollection();
        }
    }
}
