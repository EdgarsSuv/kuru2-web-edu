<?php
/**
 * @category    Kurufootwear
 * @package     Kurufootwear\Rma
 * @author      Jim McGowen <jim@kurufootwear.com>
 * @copyright   Copyright (c) 2017 KURU Footwear. All rights reserved.
 */

namespace Kurufootwear\Rma\Controller\Adminhtml\Rma;

use Magento\Backend\App\Action;
use Kurufootwear\Rma\Model\RequestFactory;
use Magento\Backend\App\Action\Context;
use Kurufootwear\Rma\Helper\ShippingLabel as ShippingLabelHelper;

/**
 * Class Save
 * @package Kurufootwear\Rma\Controller\Adminhtml\Rma
 */
class ShippingLabel extends Action
{
    /** @var ShippingLabelHelper  */
    private $shippingLabel;
    
    /** @var RequestFactory  */
    private $requestFactory;
    
    /** @var \Magento\Framework\Message\ManagerInterface  */
    protected $messageManager;
    
    /**
     * ShippingLabel constructor.
     *
     * @param Context $context
     * @param ShippingLabelHelper $shippingLabel
     * @param RequestFactory $requestFactory
     */
    public function __construct(
        Context $context,
        ShippingLabelHelper $shippingLabel,
        RequestFactory $requestFactory
        
    ) {
        $this->shippingLabel = $shippingLabel;
        $this->requestFactory = $requestFactory;
        $this->messageManager = $context->getMessageManager();
        parent::__construct($context);
    }
    
    /**
     * Outputs the pdf shipping label.
     */
    public function execute()
    {
        try {
            $rmaId = $this->getRequest()->getParam('id');
            $rma = $this->requestFactory->create()->load($rmaId);
            $label = $this->shippingLabel->getLabel($rma);
            $pdf = base64_decode($label->getLabelPdf());
            $name = 'KURU_Footwear_RMA_' . $rma->getIncrementId() . '.pdf';
    
            header('Content-Type: application/pdf');
            header('Content-Length: '.strlen( $pdf ));
            header('Content-disposition: inline; filename="' . $name . '"');
            header('Cache-Control: public, must-revalidate, max-age=0');
            header('Pragma: public');
            header('Expires: Sat, 26 Jul 1997 05:00:00 GMT');
            header('Last-Modified: '.gmdate('D, d M Y H:i:s').' GMT');
            echo $pdf;
        }
        catch (\Exception $e) {
            echo htmlentities(__('Error: ' . $e->getMessage()));
        }
    }
}
