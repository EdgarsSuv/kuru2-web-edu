<?php
/**
 * @category    Kurufootwear
 * @package     Kurufootwear\Rma
 * @author      Jim McGowen <jim@kurufootwear.com>
 * @copyright   Copyright (c) 2018 KURU Footwear. All rights reserved.
 */

namespace Kurufootwear\Rma\Controller\Adminhtml\Rma;

use Magento\Backend\App\Action;
use Magento\Backend\App\Action\Context;
use Kurufootwear\Rma\Model\Source\Request\Status as RmaStatus;
use Kurufootwear\Rma\Model\RequestManager;
use Magento\Sales\Api\OrderManagementInterface;
use Mirasvit\Rewards\Helper\Balance as RewardsBalanceHelper;
use Aheadworks\Rma\Model\RequestFactory as RmaRequestFactory;
use Aheadworks\Rma\Model\Request as RmaRequest;
use Magento\Framework\App\ResponseInterface;
use Magento\Framework\Controller\ResultInterface;
use Magento\Sales\Model\OrderRepository;

/**
 * Class Expire
 * @package Kurufootwear\Rma\Controller\Adminhtml\Rma
 */
class Expire extends Action
{
    /** @var OrderManagementInterface */
    protected $orderManagement;
    
    /** @var RewardsBalanceHelper */
    protected $rewardsBalanceHelper;
    
    /** @var RmaRequestFactory */
    protected $rmaRequestFactory;
    
    /** @var OrderRepository */
    protected $orderRepository;
    
    /**
     * Expire constructor.
     *
     * @param Context $context
     * @param OrderManagementInterface $orderManagement
     * @param RewardsBalanceHelper $rewardsBalanceHelper
     * @param RmaRequestFactory $rmaRequestFactory
     * @param OrderRepository $orderRepository
     */
    public function __construct(
        Context $context,
        OrderManagementInterface $orderManagement,
        RewardsBalanceHelper $rewardsBalanceHelper,
        RmaRequestFactory $rmaRequestFactory,
        OrderRepository $orderRepository
    ) {
        $this->orderManagement = $orderManagement;
        $this->rewardsBalanceHelper = $rewardsBalanceHelper;
        $this->rmaRequestFactory = $rmaRequestFactory;
        $this->orderRepository = $orderRepository;
        
        parent::__construct($context);
    }
    
    /**
     * Mass action handler for expiring RMAs.
     * 
     * @return $this|ResponseInterface|ResultInterface
     */
    public function execute()
    {
        if ($this->getRequest()->getMethod() != 'POST' || empty($this->getRequest()->getParam('selected'))) {
            throw new LocalizedException(__('Invalid request or nothing selected.'));
        }
    
        $executionTime = ini_get('max_execution_time');
        ini_set('max_execution_time', 0);
    
        $successCount = 0;
        $failedCount = 0;
        $wrongTypeCount = 0;
    
        $rmaIds = $this->getRequest()->getParam('selected');
        foreach ($rmaIds as $rmaId) {
            /** @var \Kurufootwear\Rma\Model\Request $rmaRequest */
            $rmaRequest = $this->rmaRequestFactory->create()->load($rmaId);
            if (!empty($rmaRequest->getData())) {
                $type = $rmaRequest->getTypeId();
            
                if ($type == RequestManager::REQUEST_TYPE_EXCHANGE) {
                    if ($this->expireExchangeRma($rmaRequest)) {
                        $successCount++;
                    }
                    else {
                        $failedCount++;
                    }
                }
                else {
                    $wrongTypeCount++;
                }
            }
            else {
                $this->messageManager->addError(__('Error loading RMA # ' . $rmaId));
                $failedCount++;
            }
        }
    
        if ($successCount > 0) {
            $this->messageManager->addSuccessMessage(__("Successfully expired %1 RMA(s).", $successCount));
        }
    
        if ($failedCount > 0) {
            $this->messageManager->addWarningMessage(__("%1 RMA(s) failed processing.", $failedCount));
        }
    
        if ($wrongTypeCount > 0) {
            $this->messageManager->addWarningMessage(__("%1 RMA(s) are not exchanges.", $wrongTypeCount));
        }
    
        ini_set('max_execution_time', $executionTime);
    
        $resultRedirect = $this->resultRedirectFactory->create();
        
        return $resultRedirect->setPath('*/*/');
    }
    
    /**
     * Expires an exchange RMA.
     *  - Cancels the pending exchange order if there is one.
     *  - Revokes points given for the RMA.
     *  - Set the status for the RMA to Expired Exchange.
     * 
     * @param $rmaRequest RmaRequest
     *
     * @return bool
     */
    private function expireExchangeRma($rmaRequest)
    {
        /** @var \Kurufootwear\Rma\Model\CustomerExchange $customerExchange */
        $customerExchange = $rmaRequest->getCustomerExchange();
        if (!empty($customerExchange)) {
            if ($customerExchange->getRmaComplete() == true) {
                $this->messageManager->addErrorMessage(
                    "Exchange order " . $customerExchange->getPendingOrderId() . 
                    " for RMA " . $rmaRequest->getIncrementId() . " has already been processed.");
        
                return false;
            }
    
            if (!empty($customerExchange->getPendingOrderId())) {
                // If the order total is > $0 we can not proceed
                $order = null;
                try {
                    $order = $this->orderRepository->get($customerExchange->getPendingOrderId());
                }
                catch (\Exception $e) {
                    $this->messageManager->addErrorMessage('Can not expire RMA ' . $rmaRequest->getIncrementId() . ' because the exchange order does not exist');
                }
                
                if ($order->getGrandTotal() > 0) {
                    $this->messageManager->addErrorMessage('Can not expire RMA ' . $rmaRequest->getIncrementId() . ' because the exchange order(' . $order->getIncrementId() . ') is greater than $0');
                    return false;
                }
                
                // Cancel the pending order
                $this->orderManagement->cancel($customerExchange->getPendingOrderId());
            }
    
            // Revoke exchange points
            $points = 0 - $customerExchange->getPointsIssued();
    
            $this->rewardsBalanceHelper->changePointsBalance(
                $customerExchange->getCustomerId(),
                $points,
                'Points revoked for expired exchange RMA ' . $rmaRequest->getIncrementId() . '.'
            );
    
            $customerExchange->setRmaComplete(true);
            $customerExchange->save();
        }
        
        try {
            $rmaRequest->setStatusId(RmaStatus::EXPIRED_EXCHANGE);
            $rmaRequest->save();
        }
        catch(\Exception $e) {
            $this->messageManager->addErrorMessage("An error occurred saving RMA " . $rmaRequest->getIncrementId());
        }
        
        return true;
    }
}
