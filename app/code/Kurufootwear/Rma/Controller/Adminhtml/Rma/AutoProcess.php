<?php
/**
 * @category    Kurufootwear
 * @package     Kurufootwear\Rma
 * @author      Jim McGowen <jim@kurufootwear.com>
 * @copyright   Copyright (c) 2017 KURU Footwear. All rights reserved.
 */

namespace Kurufootwear\Rma\Controller\Adminhtml\Rma;

use Magento\Backend\App\Action;
use Magento\Backend\App\Action\Context;
use Aheadworks\Rma\Model\RequestFactory;
use Kurufootwear\Rma\Model\RequestManager;
use Kurufootwear\Rma\Model\InspectionFactory;
use Magento\Framework\App\State;
use Magento\Framework\Exception\LocalizedException;
use Magento\Framework\Stdlib\DateTime\DateTime;
use Kurufootwear\Rma\Model\CustomerExchangeFactory;
use Kurufootwear\Rma\Model\Source\Request\Status as RmaStatus;
use Kurufootwear\NetSuite\Helper\Inventory as NSInventoryHelper;
use Magento\Sales\Api\OrderRepositoryInterface;
use Magento\Catalog\Api\ProductRepositoryInterface;
use Magento\CatalogInventory\Api\StockRegistryInterface;
use Magento\Backend\Model\Auth\Session;
use Magento\ConfigurableProduct\Model\ResourceModel\Product\Type\Configurable;
use Kurufootwear\Ui\Component\MassAction\Filter;
use Magento\Sales\Model\Order\Email\Sender\CreditmemoSender;
use Mirasvit\Rewards\Helper\Balance as RewardsBalanceHelper;
use Mirasvit\Rewards\Helper\Purchase as RewardsPurchaseHelper;
use Magento\Sales\Controller\Adminhtml\Order\CreditmemoLoader;
use Magento\Sales\Api\CreditmemoManagementInterface;
use Magento\Framework\Registry;

/**
 * Class AutoProcess
 * @package Kurufootwear\Rma\Controller\Adminhtml\Rma
 */
class AutoProcess extends Action
{
    const CATEGORY_MENS = 4;
    const CATEGORY_MENS_DISCONTINUED = 201;
    const CATEGORY_WOMENS = 4;
    const CATEGORY_WOMENS_DISCONTINUED = 201;
    
    /** @var RequestFactory */
    protected $requestModelFactory;
    
    /** @var \Aheadworks\Rma\Model\RequestManager|RequestManager */
    protected $requestManager;
    
    /** @var InspectionFactory KuruFootwear\Rma\Model\InspectionFactory */
    protected $inspectionFactory;
    
    /** @var CustomerExchangeFactory KuruFootwear\Rma\Model\CustomerExchangeFactory */
    protected $customerExchangeFactory;
    
    /** @var OrderRepositoryInterface */
    protected $orderRepository;
    
    /** @var ProductRepositoryInterface */
    protected $productRepository;
    
    /** @var StockRegistryInterface */
    protected $stockRegistry;
    
    /** @var Session */
    protected $authSession;
    
    /** @var NSInventoryHelper */
    protected $netSuiteInventory;
    
    /** @var Configurable */
    protected $catalogProductTypeConfigurable;

    /** @var RewardsBalanceHelper */
    protected $rewardsBalanceHelper;
    
    /** @var RewardsPurchaseHelper */
    protected $rewardsPurchaseHelper;
    
    /** @var CreditmemoLoader  */
    protected $creditmemoLoader;
    
    /** @var CreditmemoManagementInterface  */
    protected $creditmemoManagement;
    
    /** @var DateTime Magento\Framework\Stdlib\DateTime\DateTime */
    protected $date;

    /** @var Registry */
    protected $registry;

    /** @var Filter */
    protected $filter;

    /** @var CreditmemoSender */
    protected $creditmemoSender;

    /** @var State */
    protected $appState;

    /**
     * AutoProcess constructor.
     *
     * @param Context $context
     * @param RequestFactory $requestModelFactory
     * @param RequestManager $requestManager
     * @param InspectionFactory $inspectionFactory
     * @param DateTime $date
     * @param CustomerExchangeFactory $customerExchangeFactory
     * @param OrderRepositoryInterface $orderRepository
     * @param ProductRepositoryInterface $productRepository
     * @param StockRegistryInterface $stockRegistry
     * @param Session $authSession
     * @param NSInventoryHelper $netSuiteInventory
     * @param Configurable $catalogProductTypeConfigurable
     * @param RewardsBalanceHelper $rewardsBalanceHelper
     * @param RewardsPurchaseHelper $rewardsPurchaseHelper
     * @param CreditmemoLoader $creditmemoLoader
     * @param CreditmemoManagementInterface $creditmemoManagement
     * @param Registry $registry
     * @param Filter $filter
     * @param CreditmemoSender $creditmemoSender
     */
    public function __construct(
        Context $context,
        RequestFactory $requestModelFactory,
        RequestManager $requestManager,
        InspectionFactory $inspectionFactory,
        DateTime $date,
        CustomerExchangeFactory $customerExchangeFactory,
        OrderRepositoryInterface $orderRepository,
        ProductRepositoryInterface $productRepository,
        StockRegistryInterface $stockRegistry,
        Session $authSession,
        NSInventoryHelper $netSuiteInventory,
        Configurable $catalogProductTypeConfigurable,
        RewardsBalanceHelper $rewardsBalanceHelper,
        RewardsPurchaseHelper $rewardsPurchaseHelper,
        CreditmemoLoader $creditmemoLoader,
        CreditmemoManagementInterface $creditmemoManagement,
        Registry $registry,
        Filter $filter,
        CreditmemoSender $creditmemoSender,
        State $appState
    ) {
        $this->requestModelFactory = $requestModelFactory;
        $this->requestManager = $requestManager;
        $this->inspectionFactory = $inspectionFactory;
        $this->date = $date;
        $this->customerExchangeFactory = $customerExchangeFactory;
        $this->orderRepository = $orderRepository;
        $this->productRepository = $productRepository;
        $this->stockRegistry = $stockRegistry;
        $this->authSession = $authSession;
        $this->netSuiteInventory = $netSuiteInventory;
        $this->catalogProductTypeConfigurable = $catalogProductTypeConfigurable;
        $this->rewardsBalanceHelper = $rewardsBalanceHelper;
        $this->rewardsPurchaseHelper = $rewardsPurchaseHelper;
        $this->creditmemoLoader = $creditmemoLoader;
        $this->creditmemoManagement = $creditmemoManagement;
        $this->registry = $registry;
        $this->filter = $filter;
        $this->creditmemoSender = $creditmemoSender;
        $this->appState = $appState;

        parent::__construct($context);
    }
    
    /**
     * @return $this|\Magento\Framework\App\ResponseInterface|\Magento\Framework\Controller\ResultInterface
     * @throws LocalizedException
     */
    public function execute()
    {
        if ($this->getRequest()->getMethod() != 'POST') {
            throw new LocalizedException(__('Invalid request method.'));
        }

        $rmaIds = $this->getRequest()->getParam('selected');
        if (empty($rmaIds)) {
            /**
             * Fallback to core massAction selected collection filter,
             * when no ids in request payload are present.
             *
             * M2 core feature - when the whole filtered results are selected
             * the ids aren't sent, just the filter used and excluded ids (optional).
             */
            $rmaIds = $this->filter->getFilterIds();

            if (empty($rmaIds)) {
                throw new LocalizedException(__('Invalid request or nothing selected.'));
            }
        }
    
        $executionTime = ini_get('max_execution_time');
        ini_set('max_execution_time', 0);
        
        $successCount = 0;
        $failedCount = 0;
        $wrongStatusCount = 0;
    
        foreach ($rmaIds as $rmaId) {
            /** @var \Kurufootwear\Rma\Model\Request $rmaRequest */
            $rmaRequest = $this->requestModelFactory->create()->load($rmaId);
            if (!empty($rmaRequest->getData())) {
                $status = $rmaRequest->getStatusId();
                $type = $rmaRequest->getTypeId();
                
                // Only process warehouse approved rmas
                if ($status != RmaStatus::WAREHOUSE_APPROVED) {
                    $wrongStatusCount++;
                    continue;
                }
            
                if ($type == RequestManager::REQUEST_TYPE_RETURN) {
                    if ($this->processReturnRMA($rmaRequest)) {
                        $successCount++;
                    }
                    else {
                        $failedCount++;
                    }
                }
                else if ($type == RequestManager::REQUEST_TYPE_EXCHANGE) {
                    if ($this->processExchangeRMA($rmaRequest)) {
                        $successCount++;
                    }
                    else {
                        $failedCount++;
                    }
                }
            }
            else {
                $this->messageManager->addError(__('Error loading RMA # ' . $rmaId));
                $failedCount++;
            }
        }
    
        if ($successCount > 0) {
            $this->messageManager->addSuccessMessage(__("Successfully processed %1 RMA(s).", $successCount));
        }
    
        if ($failedCount > 0) {
            $this->messageManager->addSuccessMessage(__("%1 RMA(s) failed processing.", $failedCount));
        }
    
        if ($wrongStatusCount > 0) {
            $this->messageManager->addSuccessMessage(__("%1 RMA(s) are not warehouse approved.", $wrongStatusCount));
        }
    
        ini_set('max_execution_time', $executionTime);
    
        $resultRedirect = $this->resultRedirectFactory->create();
        return $resultRedirect->setPath('*/*/');
    }
    
    /**
     * @param $rmaRequest \Kurufootwear\Rma\Model\Request
     * @return boolean
     */
    protected function processExchangeRMA($rmaRequest)
    {
        try {
            // If other skus were entered we can not continue
            if (!empty($rmaRequest->getInspection()->getOtherSkus())) {
                $this->messageManager->addWarning(__('Skipping RMA <a href="' . $rmaRequest->getAdminUrl() . '">' . $rmaRequest->getIncrementId() . '</a> because other SKUs were entered by the warehouse.'));
        
                return false;
            }
    
            // Check for warehouse notes
            if (!empty($rmaRequest->getInspection()->getWarehouseNotes())) {
                $this->messageManager->addWarning(__('Skipping RMA <a href="' . $rmaRequest->getAdminUrl() . '">' . $rmaRequest->getIncrementId() . '</a> because it has warehouse notes.'));
        
                return false;
            }
            
            // Check for admin notes
            if (!empty($rmaRequest->getAdminNotes())) {
                $this->messageManager->addWarning(__('Skipping RMA <a href="' . $rmaRequest->getAdminUrl() . '">' . $rmaRequest->getIncrementId() . '</a> because it has admin notes.'));
        
                return false;
            }
    
            // Get the CustomerExchange record
            // If there is no CustomerExchange record then this is probably a legacy RMA, before points were automatically issued
            $customerExchange = $rmaRequest->getCustomerExchange();
    
            // Check for a new order
            if ($customerExchange && empty($customerExchange->getPendingOrderId())) {
                $this->messageManager->addWarning(__('Skipping RMA <a href="' . $rmaRequest->getAdminUrl() . '">#' . $rmaRequest->getIncrementId() . '</a> because customer has not yet re-ordered.'));
                return false;
            }
            
            // Gather item data
            $receivedItems = $rmaRequest->getInspection()->getReceivedItems();
            if (empty($receivedItems)) {
                $this->messageManager->addWarning(__('Skipping RMA <a href="' . $rmaRequest->getAdminUrl() . '">' . $rmaRequest->getIncrementId() . '</a> because no items have been received.'));
                return false;
            }
            
            foreach ($rmaRequest->getItemsCollection() as $item) {
                $itemId = $item->getItemId();
                $qty = $item->getQty();
                if (array_key_exists($itemId, $receivedItems)) {
                    if ($receivedItems[$itemId]['qty'] != $qty) {
                        $this->messageManager->addWarning(__('Skipping RMA <a href="' . $rmaRequest->getAdminUrl() . '">#' . $rmaRequest->getIncrementId() . '</a> because quantities of the items do not match.'));
                
                        return false;
                    }
            
                    if (empty($receivedItems[$itemId]['grade'])) {
                        $this->messageManager->addWarning(__('Skipping RMA <a href="' . $rmaRequest->getAdminUrl() . '">#' . $rmaRequest->getIncrementId() . '</a> because one or more items do not have a grade.'));
                
                        return false;
                    }
            
                    $orderItem = $rmaRequest->getOrder()->getItemById($itemId);
                    $product = $this->productRepository->getById($orderItem->getProductId());
            
                    // Return to stock if grade A
                    $stockItem = $this->stockRegistry->getStockItem($product->getId());
                    if ($stockItem->getManageStock()) {
                        
                        // Adjust inventory in NetSuite
                        $account = NSInventoryHelper::INV_ACCOUNT_CARE_PRODUCTS;
                        $catIds = $product->getCategoryIds();
                        $parents = $this->catalogProductTypeConfigurable->getParentIdsByChild($product->getId());
                        if ($product->gettypeId() == 'simple' && !empty($parents)) {
                            foreach ($parents as $parentId) {
                                $p = $this->productRepository->getById($parentId);
                                $catIds = array_unique(array_merge($catIds, $p->getCategoryIds()));
                            }
                            
                            if (in_array(self::CATEGORY_MENS, $catIds) || in_array(self::CATEGORY_MENS_DISCONTINUED, $catIds)) {
                                $account = NSInventoryHelper::INV_ACCOUNT_TCG_MEN;
                            } else if (in_array(self::CATEGORY_WOMENS, $catIds) || in_array(self::CATEGORY_WOMENS_DISCONTINUED, $catIds)) {
                                $account = NSInventoryHelper::INV_ACCOUNT_TCG_WOMEN;
                            }
                        }
                        
                        $bin = 'UT';
                        $location = NSInventoryHelper::INV_LOCATION_COMBINED_WAREHOUSE;
    
                        // If grade C or D we still need an inventory adjustment in NS but in the UT-B and Quarantine location.
                        if ($receivedItems[$itemId]['grade'] != 'A') {
                            $bin = 'UT-B';
                            $location = NSInventoryHelper::INV_LOCATION_COMBINED_QUARANTINE;
                        }
                        
                        // Add inventory adjustment to NetSuite
                        if (!$this->addInventoryInNetSuite($account, $orderItem->getSku(), $qty, $bin, $location,'Inventory Adjustment from auto-processing Exchange RMA ' . $rmaRequest->getIncrementId())) {
                            return false;
                        }

                        // Adjust inventory in Magento
                        if ($receivedItems[$itemId]['grade'] == 'A') {
                            $stockItem->setQty($stockItem->getQty() + 1);
                            $this->stockRegistry->updateStockItemBySku($product->getSku(), $stockItem);
                        }
                    }
                }
            }
    
            $user = $this->authSession->getUser();
            $newAdminNotes = [
                $user->getName() . ' - ' . date("m/d/Y") . ' - Auto-processed RMA.'
            ];
            
            if ($customerExchange) {
                // Change the status on the pending order
                $order = $this->orderRepository->get($customerExchange->getPendingOrderId());
                if (!empty($order->getData()) && $order->getStatus() == 'pending_exchange') {
                    // TODO: What status to change to?
                    // TODO: Need to change state too? Confirm after NetSuite integration.
                    $order->setState(\Magento\Sales\Model\Order::STATE_PROCESSING);
                    $order->setStatus('processing');
                    $this->orderRepository->save($order);
                }
    
                $customerExchange->setRmaComplete(true);
                $customerExchange->save();
            }
            else {
                // If no customer exchange record then this is probably a legacy RMA so
                // we need to issue exchange points without creating a customer exchange record
                $points = $rmaRequest->issueExchangePoints(false);
                $newAdminNotes[] = $user->getName() . ' - ' . date("m/d/Y") . ' - issued ' . $points . ' points from auto-processing RMA. (no customer exchange record found)';
            }
    
            // Add current date to Admin Notes for RMA
            $notes = $rmaRequest->getAdminNotes();
            if (strlen(trim($notes)) > 0) {
                $notes .= "\n\n";
            }
            foreach ($newAdminNotes as $newNote) {
                $notes .= $newNote . "\n";
            }
            $rmaRequest->setAdminNotes(trim($notes));
            
            // Change RMA status and save
            $rmaRequest->setStatusId(RmaStatus::COMPLETE_POINTS_ISSUED);
            $rmaRequest->save();
            
            return true;
        }
        catch (\Exception $e) {
            $this->messageManager->addError('An error occurred processing RMA <a href="' . $rmaRequest->getAdminUrl() . '">#' . $rmaRequest->getIncrementId() . '</a>: ' . $e->getMessage());
            return false;
        }
    }
    
    /**
     * @param $rmaRequest \Kurufootwear\Rma\Model\Request
     *
     * @return bool
     */
    protected function processReturnRMA($rmaRequest) 
    {
        try {
            // If other skus were entered we can not continue
            if (!empty($rmaRequest->getInspection()->getOtherSkus())) {
                $this->messageManager->addWarning(__('Skipping RMA <a href="' . $rmaRequest->getAdminUrl() . '">' . $rmaRequest->getIncrementId() . '</a> because other SKUs were entered by the warehouse.'));
        
                return false;
            }
    
            // Check for warehouse notes
            if (!empty($rmaRequest->getInspection()->getWarehouseNotes())) {
                $this->messageManager->addWarning(__('Skipping RMA <a href="' . $rmaRequest->getAdminUrl() . '">' . $rmaRequest->getIncrementId() . '</a> because it has warehouse notes.'));
        
                return false;
            }
    
            // TODO: Check for customer comments?
    
            // Create a new Credit Memo
            $data = array(
                'do_offline' => '0',
                'comment_text' => '',
                'shipping_amount' => '0',
                'send_email' => '1',
                'adjustment_positive' => '0',
                'adjustment_negative' => '0',
            );
    
            // Don't refund with payment processor or send email unless we are in production
            // TODO: Might want to make this configurable in the admin
            if ($this->appState->getMode() == State::MODE_DEVELOPER) {
                $data['do_offline'] = '1';
                $data['send_email'] = '0';
            }
    
            $order = $rmaRequest->getOrder();
    
            // Set items
            $data['items'] = array();
            $receivedItems = $rmaRequest->getInspection()->getReceivedItems();
            foreach ($rmaRequest->getItemsCollection() as $item) {
    
                $itemId = $item->getItemId();
                $orderItemId = $itemId;
                
                if ($item->getParentProductId() && $item->getProductType() == 'simple') {               
                    // For credit memo item ID needs to be the parent from the order
                    $orderItemId = $order->getItemById($itemId)->getParentItem()->getItemId();
                }
                
                $qty = $item->getQty();
                if (array_key_exists($itemId, $receivedItems)) {
                    if ($receivedItems[$itemId]['qty'] != $qty) {
                        $this->messageManager->addWarning(__('Skipping RMA <a href="' . $rmaRequest->getAdminUrl() . '">' . $rmaRequest->getIncrementId() . '</a> because quantities of the items do not match.'));
                
                        return false;
                    }
                    $data['items'][$orderItemId] = array('qty' => $receivedItems[$itemId]['qty']);
            
                    if (empty($receivedItems[$itemId]['grade'])) {
                        $this->messageManager->addWarning(__('Skipping RMA <a href="' . $rmaRequest->getAdminUrl() . '">' . $rmaRequest->getIncrementId() . '</a> because one or more items do not have a grade.'));
                
                        return false;
                    }
                    if ($receivedItems[$itemId]['grade'] == 'A') {
                        $data['items'][$orderItemId]['back_to_stock'] = '1';
                    }
                }
            }
    
            if (empty($data['items'])) {
                $this->messageManager->addError(__('Could not process RMA <a href="' . $rmaRequest->getAdminUrl() . '">' . $rmaRequest->getIncrementId() . '</a> because no items are selected by warehouse.'));
        
                return false;
            }
    
            $invoices = $order->getInvoiceCollection();
    
            // If the order has no invoice we can not continue
            if ($invoices->count() == 0) {
                $this->messageManager->addError(__('Could not process RMA <a href="' . $rmaRequest->getAdminUrl() . '">' . $rmaRequest->getIncrementId() . '</a> because the order has no invoice.'));
        
                return false;
            } // If there is more than one invoice in the order we cannot continue
            else if ($invoices->count() > 1) {
                $this->messageManager->addError(__('Could not process RMA <a href="' . $rmaRequest->getAdminUrl() . '">' . $rmaRequest->getIncrementId() . '</a> because the order has multiple invoices.'));
        
                return false;
            }
    
            try {
                // Create the Credit Memo
                if ($order->canCreditmemo()) {
                    
                    /** @var \Magento\Sales\Model\Order\Invoice $invoice */
                    $invoice = $invoices->getFirstItem();
    
                    // Creating the credit memo triggers the reward point revocation and inventory adjustment
                    $this->creditmemoLoader->setOrderId($order->getId());
                    $this->creditmemoLoader->setCreditmemo($data);
                    $this->creditmemoLoader->setInvoiceId($invoice->getId());
                    if (!empty($this->registry->registry('current_creditmemo'))) {
                        $this->registry->unregister('current_creditmemo');
                    }
                    $creditmemo = $this->creditmemoLoader->load();
                    if ($creditmemo) {
                        $creditmemo->addComment('Created from auto-processing RMA ' . $rmaRequest->getIncrementId(), true, true);
                        $this->creditmemoManagement->refund($creditmemo, (bool)$data['do_offline']);

                        /** @var \Magento\Sales\Model\Order\Creditmemo $creditMemo */
                        if ($data['send_email']) {
                            $this->creditmemoSender->send($creditmemo);
                        }
    
                        // Returns for customers whose reward points were imported from M1 do not get deducted points for the purchase.
                        $rewards = $this->rewardsPurchaseHelper->getByOrder($order);
                        if ($rewards->getEarnPoints() == null) {
                            $points = 0;
                            foreach ($data['items'] as $itemId => $item) {
                                $item = $order->getItemById($itemId);
                                $rowTotal = $item->getRowTotal() - $item->getDiscountAmount();
                                $points += $rowTotal * 5;
                            }
                            $pointsBalance = $this->rewardsBalanceHelper->getBalancePoints($order->getCustomerId());
                            $points = $pointsBalance < $points ? $pointsBalance : $points;
                            $this->rewardsBalanceHelper->changePointsBalance($order->getCustomerId(), (0 - $points), 'Points revoked for return RMA ' . $rmaRequest->getIncrementId());
                        }
                    }
                    else {
                        $this->messageManager->addError(__('Could not process RMA <a href="' . $rmaRequest->getAdminUrl() . '">' . $rmaRequest->getIncrementId() . '</a> because the credit memo could not be created.'));
                        return false;
                    }
                }
                else {
                    $this->messageManager->addError(__('Could not process RMA <a href="' . $rmaRequest->getAdminUrl() . '">' . $rmaRequest->getIncrementId() . '</a> because the credit memo could not be created.'));
                    return false;
                }
            } catch (\Exception $e) {
                $this->messageManager->addError(__('Could not process RMA <a href="' . $rmaRequest->getAdminUrl() . '">' . $rmaRequest->getIncrementId() . '</a> : ' . $e->getMessage()));
                return false;
            }
    
            // Change RMA status to Return Complete
            $rmaRequest->setStatusId(RmaStatus::RETURN_COMPLETE);
            $rmaRequest->save();
    
            return true;
        }
        catch (\Exception $e) {
            $this->messageManager->addError(__('An error occurred processing RMA <a href="' . $rmaRequest->getAdminUrl() . '">#' . $rmaRequest->getIncrementId() . '</a>: ' . $e->getMessage()));
            return false;
        }
    }
    
    /**
     * Adds to NetSuite item inventory (creates an inventory adjustment
     * transaction in NS) for the item given by $sku.
     *
     * @param $account int The NS inventory account to use. One of Inventory::INV_ACCOUNT_* constants.
     * @param $sku
     * @param $qtyToAdd
     * @param $bin
     * @param $location
     * @param $memo
     *
     * @return bool
     * @throws \Magento\Framework\Exception\IntegrationException
     */
    protected function addInventoryInNetSuite($account, $sku, $qtyToAdd, $bin, $location, $memo) {
        $internalId = $this->netSuiteInventory->searchInventoryItem($sku);
        if (is_null($internalId)) {
            $this->messageManager->addError(__('Could not find SKU ' . $sku . '  in NetSuite'));
            return false;
        }
        if (!$this->netSuiteInventory->addInventoryAdjustment($account, $internalId, $qtyToAdd, $bin, $location, $memo)) {
            $this->messageManager->addError(__('Could not add inventory adjustment (' . $qtyToAdd . ') in NetSuite for SKU ' . $sku));
            return false;
        }
        return true;
    }
}
