<?php
/**
 * @category    Kurufootwear
 * @package     Kurufootwear\Rma
 * @author      Jim McGowen <jim@kurufootwear.com>
 * @copyright   Copyright (c) 2017 KURU Footwear. All rights reserved.
 */

namespace Kurufootwear\Rma\Controller\Adminhtml\Rma;

use Aheadworks\Rma\Controller\Adminhtml\Rma\Save as AheadworksSave;
use Exception;
use Magento\Backend\App\Action\Context;
use Magento\Customer\Api\CustomerRepositoryInterface;
use Magento\Framework\Exception\IntegrationException;
use Magento\Framework\Exception\NoSuchEntityException;
use Magento\Framework\Registry;
use Magento\Framework\View\Result\PageFactory;
use Aheadworks\Rma\Model\RequestFactory;
use Kurufootwear\Rma\Model\RequestManager;
use Kurufootwear\Rma\Model\InspectionFactory;
use Magento\Framework\Stdlib\DateTime\DateTime;
use Kurufootwear\Rma\Model\Source\Request\Status as RmaStatus;
use \Magento\Sales\Api\OrderManagementInterface;
use \Mirasvit\Rewards\Helper\Balance as RewardsBalanceHelper;
use Magento\Framework\Exception\NotFoundException;

/**
 * Class Save
 * @package Kurufootwear\Rma\Controller\Adminhtml\Rma
 */
class Save extends AheadworksSave
{
    /**
     * @var CustomerRepositoryInterface
     */
    protected $customerRepository;

    /** @var InspectionFactory  */
    protected $inspectionFactory;
    
    /** @var OrderManagementInterface  */
    protected $orderManagement;
    
    /** @var RewardsBalanceHelper  */
    protected $rewardsBalanceHelper;
    
    /** @var DateTime  */
    protected $date;

    /**
     * Save constructor.
     *
     * @param Registry $registry
     * @param Context $context
     * @param CustomerRepositoryInterface $customerRepository
     * @param PageFactory $resultPageFactory
     * @param RequestFactory $requestModelFactory
     * @param RequestManager $requestManager
     * @param InspectionFactory $inspectionFactory
     * @param OrderManagementInterface $orderManagement
     * @param RewardsBalanceHelper $rewardsBalanceHelper
     * @param DateTime $date
     */
    public function __construct(
        Registry $registry,
        Context $context,
        CustomerRepositoryInterface $customerRepository,
        PageFactory $resultPageFactory,
        RequestFactory $requestModelFactory,
        RequestManager $requestManager,
        InspectionFactory $inspectionFactory,
        OrderManagementInterface $orderManagement,
        RewardsBalanceHelper $rewardsBalanceHelper,
        DateTime $date
    ) {
        $this->customerRepository = $customerRepository;
        $this->inspectionFactory = $inspectionFactory;
        $this->orderManagement = $orderManagement;
        $this->rewardsBalanceHelper = $rewardsBalanceHelper;
        $this->date = $date;
        parent::__construct($registry, $context, $resultPageFactory, $requestModelFactory, $requestManager);
    }
    
    /**
     * Save action
     * 
     * Overridden to save the warehouse inspection and process expired exchange RMAs.
     *
     * @return \Magento\Framework\Controller\Result\Redirect
     * @throws NotFoundException
     */
    public function execute()
    {
        if ($this->getRequest()->getMethod() != 'POST' || empty($this->getRequest()->getParam('request_id'))) {
            throw new NotFoundException(__('Parameter is incorrect.'));
        }
    
        $data = $this->getRequest()->getPostValue();
        
        /** @var \Kurufootwear\Rma\Model\Request $rmaRequest */
        $rmaRequest = $this->requestModelFactory->create()->load($data['request_id']);
        
        $oldType = $rmaRequest->getTypeId();
        $newType = $data['custom_fields'][6];
        
        // Check for type change from exchange to return
        if ($oldType == RequestManager::REQUEST_TYPE_EXCHANGE && 
            $newType == RequestManager::REQUEST_TYPE_RETURN) 
        {
            $rmaRequest->setType(RequestManager::REQUEST_TYPE_RETURN);
            $this->cancelExchange($rmaRequest);
            $data['request_status'] = RmaStatus::APPROVED;
        }
    
        // Check for type change from return to exchange
        if ($oldType == RequestManager::REQUEST_TYPE_RETURN &&
            $newType == RequestManager::REQUEST_TYPE_EXCHANGE)
        {
            try {
                $rmaRequest->setType(RequestManager::REQUEST_TYPE_EXCHANGE);
                $data['request_status'] = RmaStatus::POINTS_ISSUED;
                $points = $rmaRequest->issueExchangePoints();
                $this->messageManager->addNoticeMessage(__($points . ' reward points have been issued for this exchange.'));
            }
            catch(IntegrationException $e) {
                $this->messageManager->addErrorMessage(__($e->getMessage()));
            }
        }
        
        $oldStatus = $rmaRequest->getStatusId();
        $newStatus = $data['request_status'];
        
        // Check for exchange status change to expired or canceled
        if ($rmaRequest->getTypeId() == RequestManager::REQUEST_TYPE_EXCHANGE &&
            ($oldStatus == RmaStatus::APPROVED || 
                $oldStatus == RmaStatus::WAREHOUSE_APPROVED || 
                $oldStatus == RmaStatus::POINTS_ISSUED) && 
            ($newStatus == RmaStatus::EXPIRED_EXCHANGE || 
                $newStatus == RmaStatus::CANCELED_DUPLICATE))
        {
            $this->cancelExchange($rmaRequest);
        }
        
        // Only save the inspection if status is Warehouse Approved
        if ($newStatus == RmaStatus::WAREHOUSE_APPROVED) {
            // Save inspection form
            $inspection = $this->inspectionFactory->create()->loadByRequestId($data['request_id']);
            $inspection->setRequestId($data['request_id']);
            
            if ($inspection->getInspectedDate() == null) {
                $inspection->setInspectedDate($this->date->gmtDate());
            }
            
            // Set received items
            $receivedItems = array();
            if (!empty($data['warehouse_inspection']['received_items'])) {
                $grades = $data['warehouse_inspection']['grades'];
                $qtys = $data['warehouse_inspection']['qty'];
                
                foreach ($data['warehouse_inspection']['received_items'] as $receivedItem) {
                    $receivedItems[$receivedItem] = array();
                    if (!empty($grades[$receivedItem])) {
                        $receivedItems[$receivedItem]['grade'] = $grades[$receivedItem];
                    } else {
                        // Missing grade
                        $this->messageManager->addErrorMessage(__('Warehouse Inspection: Missing grade for one or more items.'));
                        
                        return $this->resultRedirectFactory->create()->setPath('*/*/edit', ['id' => $data['request_id']]);
                        break;
                    }
                    
                    if (!empty($qtys[$receivedItem])) {
                        $receivedItems[$receivedItem]['qty'] = $qtys[$receivedItem];
                    } else {
                        // Missing quantity
                        $this->messageManager->addErrorMessage(__('Warehouse Inspection: Missing quantity for one or more items.'));
                        
                        return $this->resultRedirectFactory->create()->setPath('*/*/edit', ['id' => $data['request_id']]);
                        break;
                    }
                }
            } else if (empty($data['warehouse_inspection']['other_skus'])) {
                $this->messageManager->addErrorMessage(__('Warehouse Inspection: No items selected.'));
                
                return $this->resultRedirectFactory->create()->setPath('*/*/edit', ['id' => $data['request_id']]);
            }
            $inspection->setReceivedItems($receivedItems);
            
            // Check if items match
            $orderItems = $rmaRequest->getItemsCollection();
            if (count($receivedItems) == $orderItems->count() && empty($data['warehouse_inspection']['other_skus'])) {
                $inspection->setItemsMatch(true);
            } else {
                $inspection->setItemsMatch(false);
            }
            
            $inspection->setInspectorInitials(strtoupper(trim($data['warehouse_inspection']['initials'])));
            $inspection->setOtherSkus(trim($data['warehouse_inspection']['other_skus']));
            $inspection->setWarehouseNotes(trim($data['warehouse_inspection']['notes']));
            
            $inspection->save();
        }
    
        // Parent wants 'status' not 'request_status'
        $data['status'] = $newStatus;
        $this->getRequest()->setParams($data);
        
        return parent::execute();
    }
    
    /**
     * Cancels any pending order or revokes exchange points.
     * Sets the CustomerExchange record to complete.
     * 
     * @param $rmaRequest \Kurufootwear\Rma\Model\Request
     * @param $revokePoints bool Pass false to leave points alone. Default is true.
     */
    protected function cancelExchange($rmaRequest, $revokePoints = true) 
    {
        /** @var \Kurufootwear\Rma\Model\CustomerExchange $customerExchange */
        $customerExchange = $rmaRequest->getCustomerExchange();
        if ($customerExchange == null || !$customerExchange->hasData() || $customerExchange->getRmaComplete() == true) {
            // Nothing to do but this should never happen
            return;
        }
        
        if (!empty($customerExchange->getPendingOrderId())) {
            // Cancel the pending order
            $this->orderManagement->cancel($customerExchange->getPendingOrderId());
            $this->messageManager->addNoticeMessage("Pending exchange order " . $customerExchange->getPendingOrderId() . " has been canceled.");
        }
        else {
            try {
                $customer = $this->customerRepository->getById($customerExchange->getCustomerId());

                if ($revokePoints) {
                    // Revoke exchange points
                    $points = 0 - $customerExchange->getPointsIssued();

                    $this->rewardsBalanceHelper->changePointsBalance(
                        $customer,
                        $points,
                        'Points revoked for exchange RMA ' . $rmaRequest->getIncrementId() . '.'
                    );
                    $this->messageManager->addNoticeMessage('Points revoked for exchange RMA ' . $rmaRequest->getIncrementId() . '.');
                }
            } catch (Exception $noSuchEntityException) {
                $missingCustomer = true;
            }
        }
        
        $customerExchange->setRmaComplete(true);
        $customerExchange->save();

        if (!empty($missingCustomer)) {
            // Notify missing customer, only when the actual save was successful.
            $this->messageManager->addWarningMessage(
                __('Couldn\'t revoke points, because customer doesn\'t exist.')
            );
        }
    }
}
