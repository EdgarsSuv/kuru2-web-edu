<?php
/**
 * @category    Kurufootwear
 * @package     Kurufootwear\Rma
 * @author      Krisjanis Veinbahs <krisjanisv@scandiweb.com>
 * @copyright   Copyright (c) 2017 KURU Footwear. All rights reserved.
 */

namespace Kurufootwear\Rma\Controller\Customer;

use Exception;
use Kurufootwear\Rma\Exception\ShippingLabelException;
use Magento\Customer\Model\Session;
use Magento\Framework\App\Action\Action;
use Magento\Framework\App\Action\Context;
use Kurufootwear\Rma\Model\RequestFactory;
use Kurufootwear\Rma\Helper\ShippingLabel as ShippingLabelHelper;
use Magento\Framework\Controller\ResultFactory;
use Magento\Framework\Message\ManagerInterface;
use Psr\Log\LoggerInterface;

class ShippingLabel extends Action
{
    /**
     * @var ShippingLabelHelper
     */
    private $shippingLabel;

    /**
     * @var RequestFactory
     */
    private $requestFactory;

    /**
     * @var ManagerInterface
     */
    protected $messageManager;

    /**
     * @var Session
     */
    protected $customerSession;

    /**
     * @var ResultFactory
     */
    protected $resultRedirectFactory;

    /**
     * @var LoggerInterface
     */
    protected $loggerInterface;

    /**
     * ShippingLabel constructor.
     *
     * @param Context $context
     * @param ShippingLabelHelper $shippingLabel
     * @param RequestFactory $requestFactory
     * @param Session $customerSession
     * @param LoggerInterface $loggerInterface
     */
    public function __construct(
        Context $context,
        ShippingLabelHelper $shippingLabel,
        RequestFactory $requestFactory,
        Session $customerSession,
        LoggerInterface $loggerInterface
    ) {
        $this->shippingLabel = $shippingLabel;
        $this->requestFactory = $requestFactory;
        $this->messageManager = $context->getMessageManager();
        $this->customerSession = $customerSession;
        $this->resultRedirectFactory = $context->getResultRedirectFactory();
        $this->loggerInterface = $loggerInterface;

        parent::__construct($context);
    }

    /**
     * Outputs the pdf shipping label.
     */
    public function execute()
    {
        try {
            $customerId = $this->customerSession->getCustomer()->getId();

            if ($customerId) {
                $rmaId = $this->getRequest()->getParam('id');
                $rma = $this->requestFactory->create()->load($rmaId);

                if ($rma->getCustomerId() == $customerId) {
                    $label = $this->shippingLabel->getLabel($rma);
                    $pdf = base64_decode($label->getLabelPdf());
                    $name = 'KURU_Footwear_RMA_' . $rma->getIncrementId() . '.pdf';

                    header('Content-Type: application/pdf');
                    header('Content-Length: ' . strlen($pdf));
                    header('Content-disposition: inline; filename="' . $name . '"');
                    header('Cache-Control: public, must-revalidate, max-age=0');
                    header('Pragma: public');
                    header('Expires: Sat, 26 Jul 1997 05:00:00 GMT');
                    header('Last-Modified: ' . gmdate('D, d M Y H:i:s') . ' GMT');
                    echo $pdf;
                } else {
                    $this->messageManager->addErrorMessage('You\'re not allowed to view this RMA request.');
                    return $this->resultRedirectFactory->create()->setPath('aw_rma/customer/');
                }
            } else {
                $this->messageManager->addErrorMessage('Guests are not allowed to view RMA requests.');
                return $this->resultRedirectFactory->create()->setPath('aw_rma/customer/');
            }
        } catch (ShippingLabelException $e) {
            $this->loggerInterface->warning($e->getMessage() . ' Trace: ' . $e->getTraceAsString());
            $this->messageManager->addErrorMessage($e->getMessage());
            return $this->resultRedirectFactory->create()->setPath('aw_rma/customer/');
        } catch (Exception $e) {
            $this->loggerInterface->warning('Failed to create shipping label: ' . $e->getMessage() . ' Trace: ' . $e->getTraceAsString());
            $this->messageManager->addErrorMessage('Failed to create shipping label.');
            return $this->resultRedirectFactory->create()->setPath('aw_rma/customer/');
        }
    }
}
