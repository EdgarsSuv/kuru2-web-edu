<?php
/**
 * @category    Kurufootwear
 * @package     Kurufootwear\Rma
 * @author      Krisjanis Veinbahs <info@scandiweb.com>
 * @copyright   Copyright (c) 2017 KURU Footwear. All rights reserved.
 */

namespace Kurufootwear\Rma\Controller\Customer;

use Aheadworks\Rma\Model\RequestFactory;
use Aheadworks\Rma\Model\RequestManager;
use Magento\Customer\Model\Session;
use Magento\Framework\App\Action\Context;
use Magento\Framework\App\Config\ScopeConfigInterface;
use Magento\Framework\Data\Form\FormKey\Validator;
use Magento\Framework\Registry;
use Magento\Framework\View\Result\PageFactory;
use Magento\Framework\Exception\LocalizedException;
use Aheadworks\Rma\Controller\Customer\CreateRequestStep as AheadworksCreateRequestStep;
use Magento\Sales\Model\OrderFactory;
use Kurufootwear\Rma\Model\Survey;

class CreateRequestStep extends AheadworksCreateRequestStep
{
    /**
     * @var OrderFactory
     */
    private $orderFactory;

    /**
     * @var Survey $survey
     */
    protected $survey;

    public function __construct(
        Context $context,
        PageFactory $resultPageFactory,
        Session $customerSession,
        Registry $coreRegistry,
        Validator $formKeyValidator,
        ScopeConfigInterface $scopeConfig,
        RequestManager $requestManager,
        RequestFactory $requestFactory,
        OrderFactory $orderFactory,
        Survey $survey
    )
    {
        $this->orderFactory = $orderFactory;
        $this->survey = $survey;

        parent::__construct(
            $context,
            $resultPageFactory,
            $customerSession,
            $coreRegistry,
            $formKeyValidator,
            $scopeConfig,
            $requestManager,
            $requestFactory,
            $orderFactory
        );
    }

    /**
     * @return $this|\Magento\Framework\View\Result\Page
     */
    public function execute()
    {
        $order = $this->orderFactory->create();
        try {
            if ($this->getRequest()->getMethod() == \Magento\Framework\App\Request\Http::METHOD_POST) {
                if (!$this->formKeyValidator->validate($this->getRequest())) {
                    return $this->resultRedirectFactory->create()->setPath('*/*/');
                }
                $data = $this->getRequest()->getPostValue();
                $this->customerSession->setOrderSelectData($data);
                if ($data) {
                    $orderId = isset($data['order_id']) ? $data['order_id'] : null;
                }
            } else {
                if ($data = $this->customerSession->getOrderSelectData()) {
                    $orderId = $data['order_id'];
                } else {
                    $orderId = $this->getRequest()->getParam('id');
                    $data = ['order_id' => $orderId];
                }
            }

            if (!$orderId) {
                throw new LocalizedException(__('Order is not specified.'));
            }
            $order->load($orderId);

            /**
             * Refresh survey if new order
             * Redirect if old order and finished
             */
            if (!$this->survey->load() ||
                ($this->survey->getOrder() != $data)) {
                $this->survey->clean()
                    ->setOrder($data)
                    ->populateItems()
                    ->save();
            } else if ($this->survey->getIsFinished()) {
                return $this->resultRedirectFactory->create()->setPath('*/*/');
            }

            $this->coreRegistry->register('aw_rma_request_data', $data);
            if ($formData = $this->customerSession->getFormData()) {
                $this->coreRegistry->register('aw_rma_form_data', $formData);
            }

            /** $resultPage @var \Magento\Framework\View\Result\Page */
            $resultPage = $this->resultPageFactory->create();
            $resultPage->getConfig()->getTitle()->set(__('New Return for Order #%1', $order->getIncrementId()));
            /** @var \Magento\Customer\Block\Account\Dashboard $linkBack */
            $linkBack = $resultPage->getLayout()->getBlock('customer.account.link.back');
            if ($linkBack) {
                $linkBack->setRefererUrl($this->_redirect->getRefererUrl());
            }
            return $resultPage;

        } catch (LocalizedException $e) {
            $this->messageManager->addError($e->getMessage());
        } catch (\RuntimeException $e) {
            $this->messageManager->addError($e->getMessage());
        } catch (\Exception $e) {
            $this->messageManager->addException($e, __('Something went wrong while loading the page.'));
        }
        return $this->resultRedirectFactory->create()->setUrl($this->_redirect->getRefererUrl());
    }
}
