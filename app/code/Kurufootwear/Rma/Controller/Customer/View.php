<?php
/**
 * @category    Kurufootwear
 * @package     Kurufootwear\Rma
 * @author      Krisjanis Veinbahs <krisjanisv@scandiweb.com>
 * @copyright   Copyright (c) 2017 Scandiweb, Inc (http://scandiweb.com)
 * @license     http://opensource.org/licenses/OSL-3.0 The Open Software License 3.0 (OSL-3.0)
 */

namespace Kurufootwear\Rma\Controller\Customer;

use Aheadworks\Rma\Controller\Customer\View as AheadworksView;
use Magento\Framework\Exception\LocalizedException;

class View extends AheadworksView
{
    /**
     * @return \Magento\Framework\Controller\Result\Redirect|\Magento\Framework\View\Result\Page
     */
    public function execute()
    {
        try {
            $rmaRequest = $this->getRmaRequest();
            if ($this->isRequestValid($rmaRequest)) {
                $this->coreRegistry->register('aw_rma_request', $rmaRequest);
            }
        } catch (LocalizedException $e) {
            $this->messageManager->addErrorMessage($e->getMessage());
            return $this->goBack();
        }
        return $this->getResultPage([
            'title' => __('RMA Request %1', $rmaRequest->getIncrementId()),
            'link_back' => ['name' => 'customer.account.link.back', 'route_path' => 'aw_rma/customer']
        ]);
    }
}