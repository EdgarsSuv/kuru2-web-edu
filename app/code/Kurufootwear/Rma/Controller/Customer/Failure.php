<?php
/**
 * @category    Kurufootwear
 * @package     Kurufootwear\Rma
 * @author      Krisjanis Veinbahs <info@scandiweb.com>
 * @copyright   Copyright (c) 2017 KURU Footwear. All rights reserved.
 */

namespace Kurufootwear\Rma\Controller\Customer;

use Kurufootwear\Rma\Helper\SurveyLogic;
use Kurufootwear\Rma\Model\Survey;
use Kurufootwear\Rma\Model\SurveyAction;
use Magento\Framework\App\Action\Context;
use Magento\Framework\App\Action\Action;
use Magento\Framework\Controller\ResultFactory;
use Magento\Framework\App\Request\Http;

class Failure extends Action
{
    /**
     * @var Survey
     */
    protected $survey;

    /**
     * @var SurveyAction
     */
    protected $surveyAction;

    /**
     * @var SurveyLogic
     */
    protected $surveyLogic;

    /**
     * Path of this controller
     */
    const FAILURE_URL = 'kf_rma/customer/failure';

    /**
     * Keys used in failure response
     */
    const FAILURE_RESPONSE = 'failure_response';
    const ACCEPT_REMOVE = 'accept_remove';
    const FINISH_SURVEY = 'finish_survey';
    const REDIRECT_URL = 'redirect_url';

    /**
     * RmaResolution constructor.
     * @param Context $context
     */
    public function __construct(
        Context $context,
        Survey $survey,
        SurveyAction $surveyAction,
        SurveyLogic $surveyLogic
    )
    {
        $this->survey = $survey;
        $this->surveyAction = $surveyAction;
        $this->surveyLogic = $surveyLogic;

        parent::__construct(
            $context
        );
    }

    /**
     * Set layout and render page
     */
    public function execute()
    {
        if ($this->isFailureResponse()) {
            if ($this->isRemoveAccepted()) {
                $this->removeSurveyItem();
            }

            if ($this->isFinishSurvey()) {
                $this->finishSurvey();
            }

            $result = $this->resultFactory->create(ResultFactory::TYPE_REDIRECT);
            return $result->setUrl($this->getRedirectUrl());
        } else if ($this->isSurveyFailure()) {
            $this->_view->loadLayout();
            $this->_view->renderLayout();
        } else {
            $result = $this->resultFactory->create(ResultFactory::TYPE_REDIRECT);
            return $result->setUrl($this->_url->getBaseUrl());
        }
    }

    public function getRedirectUrl()
    {
        /** @var RequestInterface $request */
        $request = $this->getRequest();
        if ($request->getMethod() == Http::METHOD_POST) {
            return $this->getRequest()->getPostValue(self::REDIRECT_URL);
        }

        return null;
    }

    /**
     * Check if POST has response flag
     */
    public function isFailureResponse()
    {
        /** @var RequestInterface $request */
        $request = $this->getRequest();
        if ($request->getMethod() == Http::METHOD_POST) {
            return $this->getRequest()->getPostValue(self::FAILURE_RESPONSE) == 1;
        }

        return false;
    }

    public function isSurveyFailure()
    {
        /** @var RequestInterface $request */
        $request = $this->getRequest();
        if ($request->getMethod() == Http::METHOD_POST) {
            return !is_null($this->getRequest()->getPostValue(SurveyAction::CLASS_NAME));
        }

        return false;
    }

    /**
     * Return whether accept remove flag was set true
     * @return bool
     */
    public function isRemoveAccepted()
    {
        /** @var RequestInterface $request */
        $request = $this->getRequest();
        if ($request->getMethod() == Http::METHOD_POST) {
            return $this->getRequest()->getPostValue(self::ACCEPT_REMOVE) == true;
        }

        return false;
    }

    /**
     * Remove current item from survey
     */
    public function removeSurveyItem()
    {
        $itemId = $this->survey->load()->getCurrentItemFullId();
        $request = $this->survey->removeItem($itemId)->save();
    }

    /**
     * Return whether finish survey flag was set true
     * @return bool
     */
    public function isFinishSurvey()
    {
        /** @var RequestInterface $request */
        $request = $this->getRequest();
        if ($request->getMethod() == Http::METHOD_POST) {
            return $this->getRequest()->getPostValue(self::FINISH_SURVEY) == true;
        }

        return false;
    }

    /**
     * Save survey if possible
     */
    public function finishSurvey()
    {
        $survey = $this->survey->load();
        if (is_null($nextId = $survey->nextItemFullId())) {
            $this->surveyLogic->saveReturn($this->survey);
        }
    }
}
