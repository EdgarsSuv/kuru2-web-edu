<?php
/**
 * @category    Kurufootwear
 * @package     Kurufootwear\Rma
 * @author      Krisjanis Veinbahs <info@scandiweb.com>
 * @copyright   Copyright (c) 2017 KURU Footwear. All rights reserved.
 */

namespace Kurufootwear\Rma\Controller\Customer;

use Kurufootwear\Rma\Model\Survey;
use Kurufootwear\Rma\Model\SurveyItem;
use Kurufootwear\Rma\Model\SurveyAction;
use Kurufootwear\Rma\Helper\SurveyLogic;
use Kurufootwear\Catalog\Model\UrlParser;
use Aheadworks\Rma\Controller\Customer;
use Aheadworks\Rma\Model\RequestFactory;
use Aheadworks\Rma\Model\RequestManager;
use Magento\Backend\App\Action\Context;
use Magento\Customer\Model\Session;
use Magento\Framework\View\Result\PageFactory;
use Magento\Framework\App\Config\ScopeConfigInterface;
use Magento\Framework\App\Request\Http;
use Magento\Framework\Controller\Result\Json;
use Magento\Framework\Controller\Result\JsonFactory;
use Magento\Framework\Data\Form\FormKey\Validator;
use Magento\Framework\Registry;
use Magento\Framework\Json\Helper\Data as JsonHelper;
use Magento\Sales\Model\OrderFactory;
use Magento\Sales\Model\Order\ItemFactory;
use Magento\Framework\UrlInterface;
use Magento\Store\Model\StoreManagerInterface;

class RequestSurveyAction extends Customer
{
    /**
     * @var Survey
     */
    protected $survey;

    /**
     * @var SurveyItem
     */
    protected $surveyItem;

    /**
     * @var SurveyAction
     */
    protected $surveyAction;

    /**
     * @var SurveyLogic
     */
    protected $surveyLogic;

    /**
     * @var array
     */
    protected $requestedOrder;

    /**
     * @var JsonFactory
     */
    protected $resultJsonFactory;

    /**
     * @var Validator
     */
    protected $formKeyValidator;

    /**
     * @var JsonHelper
     */
    protected $jsonHelper;

    /**
     * @var OrderFactory
     */
    protected $orderFactory;

    /**
     * @var ItemFactory
     */
    protected $orderItemFactory;

    /**
     * @var StoreManagerInterface
     */
    protected $storeManager;

    /**
     * @var UrlParser
     */
    protected $urlParser;

    /**
     * RequestSurveyAction constructor.
     * @param Context $context
     * @param Survey $survey
     * @param SurveyItem $surveyItem
     * @param SurveyAction $surveyAction
     * @param SurveyLogic $surveyLogic
     * @param JsonFactory $resultJsonFactory
     * @param JsonHelper $jsonHelper
     * @param PageFactory $resultPageFactory
     * @param Registry $coreRegistry
     * @param ScopeConfigInterface $scopeConfig
     * @param RequestManager $requestManager
     * @param RequestFactory $requestFactory
     * @param Session $customerSession
     * @param OrderFactory $orderFactory
     * @param ItemFactory $orderItemFactory
     * @param StoreManagerInterface $storeManager
     */
    public function __construct(
        Context $context,
        Survey $survey,
        SurveyItem $surveyItem,
        SurveyAction $surveyAction,
        SurveyLogic $surveyLogic,
        JsonFactory $resultJsonFactory,
        JsonHelper $jsonHelper,
        PageFactory $resultPageFactory,
        Registry $coreRegistry,
        ScopeConfigInterface $scopeConfig,
        RequestManager $requestManager,
        RequestFactory $requestFactory,
        Session $customerSession,
        OrderFactory $orderFactory,
        ItemFactory $orderItemFactory,
        StoreManagerInterface $storeManager,
        UrlParser $urlParser
    )
    {
        $this->survey = $survey;
        $this->surveyItem = $surveyItem;
        $this->surveyAction = $surveyAction;
        $this->surveyLogic = $surveyLogic;
        $this->resultJsonFactory = $resultJsonFactory;
        $this->jsonHelper = $jsonHelper;
        $this->formKeyValidator = $context->getFormKeyValidator();
        $this->orderItemFactory = $orderItemFactory;
        $this->storeManager = $storeManager;
        $this->orderFactory = $orderFactory;
        $this->urlParser = $urlParser;

        parent::__construct(
            $context,
            $resultPageFactory,
            $coreRegistry,
            $this->formKeyValidator,
            $scopeConfig,
            $requestManager,
            $requestFactory,
            $customerSession
        );
    }

    /**
     * Return rma survey action in json
     * @return Json
     */
    public function execute()
    {
        try {
            $this->survey->load();
            $surveyAction = null;

            /**
             * Save action if POST && REQUESTED_PREVIOUS
             * Set action flags
             */
            if ($this->getRequest()->getMethod() == Http::METHOD_POST) {
                $postJson = $this->getRequest()->getPostValue(SurveyAction::CLASS_NAME);
                $surveyAction = $this->jsonHelper->jsonDecode($postJson);
                $current = false;
                if (array_key_exists(SurveyAction::REQUESTED_PREVIOUS, $surveyAction)) {
                    $previous = $surveyAction[SurveyAction::REQUESTED_PREVIOUS];
                } else {
                    $previous = false;
                }
                if (array_key_exists(SurveyAction::QUESTION_ANSWER, $surveyAction)) {
                    // If this answer has been answered previously
                    // and this answer is different, set flag
                    $answer = $surveyAction[SurveyAction::QUESTION_ANSWER];
                    $currentAction = $this->survey->getCurrentAction();
                    if (array_key_exists(SurveyAction::QUESTION_ANSWER, $currentAction)) {
                        $currentAnswer = $currentAction[SurveyAction::QUESTION_ANSWER];
                        $changedHistory = $currentAnswer != $answer;
                    } else {
                        $changedHistory = false;
                    }
                }
                $this->survey->getCurrentAction();
                if (array_key_exists(SurveyAction::REQUESTED_NEXT, $surveyAction)) {
                    $next = $surveyAction[SurveyAction::REQUESTED_NEXT];
                } else {
                    $next = false;
                }
                if ($next) {
                    $this->survey->setCurrentAction($surveyAction)
                        ->save();
                }
            } else {
                $previous = false;
                $current = true;
                $next = false;
                $changedHistory = false;
            }

            /**
             * Load/generate previous/current/next action
             * Save it as the current one
             */
            $previousAction = $this->survey->loadPreviousAction();
            $currentAction = $this->survey->getCurrentAction();
            $nextAction = $this->survey->loadNextAction();
            if ($current && ($currentAction !== null)) {
                $surveyAction = $this->survey->getCurrentAction();
                $surveyAction[SurveyAction::PREVIOUS_AVAILABLE] = !is_null($previousAction);
            } else if ($previous && ($previousAction !== null)) {
                $surveyAction = $previousAction;
                $prePreviousAction = $this->survey->loadPreviousAction($previousAction);
                $surveyAction[SurveyAction::PREVIOUS_AVAILABLE] = !is_null($prePreviousAction);
            } else if ($next && ($nextAction !== null) && !$changedHistory) {
                $surveyAction = $nextAction;
                $surveyAction[SurveyAction::PREVIOUS_AVAILABLE] = true;
            } else {
                $surveyAction = $this->surveyLogic->generateAction($this->survey);
                $currentId = $currentAction[SurveyAction::ACTION_ITEM_FULL_ID];
                $currentAfterLogic = $this->survey->getItem($currentId);
                if (is_null($currentAfterLogic)) {
                    $surveyAction[SurveyAction::PREVIOUS_AVAILABLE] = !is_null($previousAction);
                } else {
                    $surveyAction[SurveyAction::PREVIOUS_AVAILABLE] = !is_null($currentAction);
                }
            }
            $surveyAction[SurveyAction::NEXT_AVAILABLE] = true;

            /**
             * If action is question add item
             * info to action and save it
             */
            if ($surveyAction[SurveyAction::TYPE] != SurveyAction::TYPE_REDIRECT) {
                $surveyAction = $this->surveyAction->set($surveyAction);

                $currentOrderItemId = $surveyAction->generateItemId();
                $currentProduct = $this->orderItemFactory->create()->load($currentOrderItemId)->getProduct();

                $surveyAction->setActionItemTitle($currentProduct->getData('orig_name'));
                $surveyAction->setActionItemColor($this->urlParser->getAttributeLabel(intval($currentProduct->getData('color')), 'color'));
                if ($currentProduct->getData('size')) {
                    $surveyAction->setActionItemSize($this->urlParser->getAttributeLabel(intval($currentProduct->getData('size')), 'size'));
                }
                else {
                    $surveyAction->setActionItemSize($this->urlParser->getAttributeLabel(intval($currentProduct->getData('womens_size')), 'womens_size'));
                }
                $store = $this->storeManager->getStore();

                // Todo - load placeholder if there is no image!
                $imageUrl = $store->getBaseUrl(UrlInterface::URL_TYPE_MEDIA) . 'catalog/product' . $currentProduct->getThumbnail();
                $surveyAction->setActionItemThumbnail($imageUrl);

                $surveyAction = $surveyAction->get();
                $this->survey->setCurrentAction($surveyAction);
            }

            /**
             * Save new state of survey
             */
            $this->survey->save();
        } catch (\Exception $e) {
            $this->messageManager->addExceptionMessage($e, __('Something went wrong while loading the page.'));
        }

        /** @var Json $result */
        $resultJson = $this->resultJsonFactory->create();

        return $resultJson->setData($surveyAction);
    }
}
