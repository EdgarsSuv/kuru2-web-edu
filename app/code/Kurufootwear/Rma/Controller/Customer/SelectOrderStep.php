<?php
/**
 * @category    Kurufootwear
 * @package     Kurufootwear\Rma
 * @author      Krisjanis Veinbahs <info@scandiweb.com>
 * @copyright   Copyright (c) 2017 KURU Footwear. All rights reserved.
 */

namespace Kurufootwear\Rma\Controller\Customer;

use Aheadworks\Rma\Controller\Customer\SelectOrderStep as AheadworksSelectOrderStep;
use Aheadworks\Rma\Model\RequestFactory;
use Aheadworks\Rma\Model\RequestManager;
use Magento\Customer\Model\Session;
use Magento\Framework\App\Action\Context;
use Magento\Framework\App\Config\ScopeConfigInterface;
use Magento\Framework\Data\Form\FormKey\Validator;
use Magento\Framework\Registry;
use Magento\Framework\View\Result\PageFactory;

class SelectOrderStep extends AheadworksSelectOrderStep
{
    public function __construct(
        Context $context,
        PageFactory $resultPageFactory,
        Registry $coreRegistry,
        Validator $formKeyValidator,
        ScopeConfigInterface $scopeConfig,
        RequestManager $requestManager,
        RequestFactory $requestFactory,
        Session $customerSession
    )
    {
        parent::__construct(
            $context,
            $resultPageFactory,
            $coreRegistry,
            $formKeyValidator,
            $scopeConfig,
            $requestManager,
            $requestFactory,
            $customerSession
        );
    }

    /**
     * Remove old survey data
     * and result in selection page
     * @return \Magento\Framework\View\Result\Page
     */
    public function execute()
    {
        $this->customerSession->setRmaSurvey(null);
        return $this->getResultPage(['title' => __('New Return')]);
    }
}
