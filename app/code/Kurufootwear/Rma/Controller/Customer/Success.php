<?php
/**
 * @category    Kurufootwear
 * @package     Kurufootwear\Rma
 * @author      Krisjanis Veinbahs <info@scandiweb.com>
 * @copyright   Copyright (c) 2017 KURU Footwear. All rights reserved.
 */

namespace Kurufootwear\Rma\Controller\Customer;

use Kurufootwear\Rma\Model\Survey;
use Kurufootwear\Rma\Helper\SurveyLogic;
use Magento\Customer\Model\Session;
use Magento\Framework\App\Action\Context;
use Magento\Framework\App\Action\Action;

class Success extends Action
{
    /**
     * Path of this controller
     */
    const RESOLUTION_URL = 'kf_rma/customer/success';

    /**
     * @var Survey
     */
    protected $survey;

    /**
     * @var SurveyLogic
     */
    protected $surveyLogic;

    /**
     * @var Session
     */
    protected $customerSession;

    /**
     * RmaResolution constructor.
     * @param Context $context
     */
    public function __construct(
        Context $context,
        Survey $survey,
        SurveyLogic $surveyLogic,
        Session $customerSession
    )
    {
        $this->survey = $survey;
        $this->surveyLogic = $surveyLogic;
        $this->customerSession = $customerSession;

        parent::__construct(
            $context
        );
    }

    /**
     * Set layout and render page
     */
    public function execute()
    {
        $this->survey->load();

        /**
         * If request id not available
         * try saving survey rma request
         * If that fails, redirect
         */
        if (is_null($this->survey->getRequestId())) {
            if (is_null($this->saveRmaRequest())) {
                return $this->resultRedirectFactory->create()->setPath('aw_rma/customer/');
            }
        }

        $this->customerSession->setIsRmaExchange($this->survey->getIsExchange());

        $this->_view->loadLayout();
        $this->_view->renderLayout();
    }


    /**
     * Retrieve POST RMA request id
     * @return mixed|null
     */
    public function saveRmaRequest()
    {
        $survey = $this->survey->load();

        return $this->surveyLogic->saveReturn($survey);
    }
}
