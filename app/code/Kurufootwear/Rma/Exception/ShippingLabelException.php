<?php

namespace Kurufootwear\Rma\Exception;

use Magento\Framework\Exception\LocalizedException;

class ShippingLabelException extends LocalizedException
{
    
}