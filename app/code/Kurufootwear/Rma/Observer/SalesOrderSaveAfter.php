<?php
/**
 * @vendor Kurufootwear
 * @module Kurufootwear_Rma
 * @author Krisjanis Veinbahs <krisjanisv@scandiweb.com>
 * @copyright Copyright (c) 2017 Scandiweb. All rights reserved.
 * @license http://opensource.org/licenses/afl-3.0.php Academic Free License (AFL 3.0)
 */

namespace Kurufootwear\Rma\Observer;

use Kurufootwear\Rma\Model\CustomerExchangeFactory;
use Kurufootwear\Rma\Model\ResourceModel\CustomerExchange\Collection;
use Magento\Framework\Event\Observer;
use Magento\Framework\Event\ObserverInterface;
use Magento\Sales\Model\Order;
use Mirasvit\Rewards\Helper\Purchase;

class SalesOrderSaveAfter implements ObserverInterface
{
    /**
     * @var CustomerExchangeFactory
     */
    protected $customerExchange;

    /**
     * @var Purchase
     */
    protected $rewardsPurchase;

    /**
     * SalesOrderObserver constructor.
     * @param CustomerExchangeFactory $customerExchange
     * @param Purchase $rewardsPurchase
     */
    public function __construct(
        CustomerExchangeFactory $customerExchange,
        Purchase $rewardsPurchase
    )
    {
        $this->customerExchange = $customerExchange;
        $this->rewardsPurchase = $rewardsPurchase;
    }

    /**
     * On exchange orders update CustomerExchange field
     * and set order to pending exchange
     * 
     * @param Observer $observer
     * @return mixed
     */
    public function execute(Observer $observer)
    {
        /** @var Order $order */
        $order = $observer->getData('order');
        $orderId = $order->getId();
        $rewardsField = $this->rewardsPurchase->getByOrder($order);
        $exchangeField = $this->getExchange($order);

        if (isset($rewardsField) && isset($exchangeField)) {
            if (empty($exchangeField->getPendingOrderId())) {
                /**
                 * ! All unused points get merged with all
                 *   other normal, usual points
                 */
                if ($rewardsField->getData('spend_points') != 0) {
                    $exchangeField->setPendingOrderId($orderId)->save();
                }
            }
        }
    }

    /**
     * Return RMA Customer Exchange field
     * @param $order
     * @return \Magento\Framework\DataObject|null
     */
    public function getExchange($order)
    {
        if ($customerId = $order->getCustomerId()) {
            /**
             * @var Collection $exchangeCollection
             */
            $exchange = $this->customerExchange->create();
            $exchangeCollection = $exchange->getCollection()
                ->addFieldToFilter('customer_id', ['eq' => $customerId])
                ->addFieldToFilter('pending_order_id', ['null' => true])
                ->addFieldToFilter('rma_complete', ['eq' => false]);

            if ($exchangeCollection->count() > 0) {
                return $exchangeCollection->getFirstItem();
            } else {
                return null;
            }
        } else {
            return null;
        }
    }
}