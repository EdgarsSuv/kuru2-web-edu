<?php
/**
 * @vendor      Kurufootwear
 * @module      Kurufootwear_Rma
 * @author      Viesturs Ruzans <viestursr@scandiweb.com>
 * @copyright   Copyright (c) 2017 Scandiweb, Inc (http://scandiweb.com)
 * @license     http://opensource.org/licenses/OSL-3.0 The Open Software License 3.0 (OSL-3.0)
 */

\Magento\Framework\Component\ComponentRegistrar::register(
    \Magento\Framework\Component\ComponentRegistrar::MODULE,
    'Kurufootwear_Rma',
    __DIR__
);
