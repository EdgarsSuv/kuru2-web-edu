/**
 * @category    Kurufootwear
 * @package     Kurufootwear\Rma
 * @author      Kristaps Stalidzāns <info@scandiweb.com>
 * @copyright   Copyright (c) 2017 Scandiweb, Inc (http://scandiweb.com)
 * @license     http://opensource.org/licenses/OSL-3.0 The Open Software License 3.0 (OSL-3.0)
 */
define([
    'jquery',
    'mage/validation'
], function($) {
    'use strict';

    return function(selectItemWidget) {
        $.widget("awrma.awRmaSelectItemForm", $.awrma.awRmaSelectItemForm, {
            options: {
                selectItemSelector: '.table-content.selected-item',
                rowItemCheckboxSelector: '.order-item-row.preselected-item .order-item-select'
            },

            _create: function() {
                this._bind();
                this._initValidators();
                this.updateOrderItemRows($(this.options.selectItemSelector).attr('data-order-id'));
                this.updatePreselectedOrderItem();
                this.updateSubmitBtnVisibility();
            },

            /**
             * Update preselected order item
             */
            updatePreselectedOrderItem: function () {
                var self = this;

                $(self.options.rowItemCheckboxSelector).each(function () {
                    self.updateOrderItem($(this).val(), $(this).prop('checked'));
                })
            },

            /**
             * Trigger order selection
             * Override to add update order_id url param
             *
             * @param event
             */
            onSelectOrderClick: function(event) {
                var orderId = $(event.target).val();

                this.updateOrderItemRows(orderId);
                this.updateSubmitBtnVisibility();
                this.updateOrderUrlParam(orderId);
            },

            /**
             * Update order_id url param
             *
             * @param orderId
             */
            updateOrderUrlParam: function (orderId) {
                var url = new URL(window.location.href);
                url.searchParams.set('order_id', orderId);

                if (typeof (history.replaceState) !== 'undefined') {
                    window.history.replaceState('', '', url);
                }
            }
        });

        return $.awrma.awRmaSelectItemForm;
    }
});