/**
 * @category  Kurufootwear
 * @package   Kurufootwear\Rma
 * @author    Krisjanis Veinbahs <info@scandiweb.com>
 * @copyright Copyright (c) 2017 Scandiweb, Inc (http://scandiweb.com)
 * @license   http://opensource.org/licenses/OSL-3.0 The Open Software License 3.0 (OSL-3.0)
 */

define([
    'jquery',
    'Kurufootwear_Rma/js/lib/survey_jquery',
    'Kurufootwear_Rma/js/lib/redirect_jquery',
    'jquery/ui'
], function($, Survey) {
    'use strict';

    $.widget('rmaSurvey.js', {
        $survey: undefined,
        $questions: undefined,
        $previous: undefined,
        $next: undefined,
        survey: undefined,
        action: undefined,
        customCss: undefined,
        previousItem: undefined,
        currentItem: undefined,
        productInfoBlockWrapper: $('.product-info-block-wrapper'),

        /**
         * Generate and return question in
         * SurveyJs format from this.action
         * @returns {{}}
         * @private
         */
        _convertToSurveyJS: function() {
            var self = this;
            var question = {};
            if (this.action.question_type === 'radio') {
                question.type = 'radiogroup';
            } else if (this.action.question_type === 'textarea') {
                question.type = 'comment';
            } else {
                question.type = this.action.question_type;
            }
            question.name = this.action.type;
            question.title = this.action.question_title;
            question.isRequired = (this.action.question_required);
            // Set choices if not custom text field
            if ('question_choices' in this.action) {
                question.choices = Object.keys(this.action.question_choices).map(function(key) {
                    return {
                        value: key,
                        locTextValue: {
                            hasHtml: true,
                            renderedHtml: '<span class="choice-wrapper">' + self.action.question_choices[key] + '</span>'
                        }
                    };
                });
                question.colCount = question.choices.length;
            }
            question.hasOther = false;

            return question;
        },
        /**
         * Generate action for POST from
         * this.survey and this.action
         * @returns {*|Object|Array}
         * @private
         */
        _convertFromSurveyJS: function() {
            var action = $.extend({}, this.action);
            if (this.action.question_type === 'radio') {
                var key = this.survey.data[this.action.type];
                var value = this.action.question_choices[key];
                var answer = {};
                answer[key] = value;
            } else {
                var answer = {
                    value: this.survey.data[this.action.type]
                };
            }
            action['question_answer'] = answer;

            return action;
        },
        /**
         * Fade in or out navigation buttons that
         * shouldn't be available
         * @private
         */
        _transitionNavigation: function() {
            if (this.action.next_available) {
                this.$next.animate({'opacity': 1});
                this.$next.fadeIn();
            } else {
                this.$next.fadeOut();
            }

            if (this.action.previous_available) {
                this.$previous.animate({'opacity': 1});
                this.$previous.fadeIn();
            } else {
                this.$previous.fadeOut();
            }
        },
        /**
         * Assign survey submission to buttons
         * @private
         */
        _loadNavigation: function() {
            var self = this;

            this.$previous.unbind('click');
            this.$previous.click(function() {
                self.clicked = 'previous';
                self._send();
                self.$previous.unbind('click');
            });

            this.$next.unbind('click');
            this.$next.click(function() {
                self.clicked = 'next';
                self.survey.completeLastPage();
            });

            // Forcefully set complete trigger to only
            // send request and not do native transition
            this.survey.onComplete.callbacks = [function() {
                self.survey.onComplete.remove();
                self.survey.onComplete.remove();
                self._send(self._convertFromSurveyJS());
            }];

            this._transitionNavigation();
        },
        /**
         * Transition old question out
         * and new question in
         * @private
         */
        _transitionQuestions: function() {
            var self = this;
            var transition = 500; // Transition time
            var animations = 'from-left from-right in in to-left to-right';
            var $old = this.$questions.find('.question-form:last-child');
            var oldHeight = $old.height();

            /**
             * Create question form
             */
            var $new = $('<div class="question-form"></div>');

            // Add survey model
            $new.Survey({
                model: self.survey,
                css: self.customCss
            });

            // Add title
            var required = self.action.question_required ? 'required' : '';
            var title = self.action.question_title;
            var titleElement = '<div class="question-title ' + required + '">' + title + '</div>';
            $new.prepend($(titleElement));


            /**
             * Transition question forms
             */
            if (self.clicked === 'previous') {
                $old.addClass('to-right');
            } else {
                $old.addClass('to-left');
            }
            setTimeout(function() {
                self.$questions.height(oldHeight);
                // Remove old form
                $old.remove();

                // Add new form
                self.$questions.append($new);
                var newHeight = $new.height();
                if (self.clicked === 'previous') {
                    $new.addClass('from-left');
                } else {
                    $new.addClass('from-right');
                }
                self.$questions.animate({height: newHeight}, 'slow', function() {
                    self.$questions.height('auto');
                });
                // Transition new form in
                setTimeout(function() {
                    $new.addClass('in');
                }, 50);
            }, transition);
        },
        /**
         * Create survey model for question
         * and remove native navigation
         * @private
         */
        _loadQuestion: function() {
            // Init SurveyJS model
            this.survey = new Survey.Model({
                questions: [ this._convertToSurveyJS() ]
            });

            // Add custom classes for styling purposes
            this.customCss = {
                radiogroup: {
                    root: "answers-wrapper",
                    other: "hello"
                }
            };

            // Set previous answer if question has
            // already been answered
            if ('question_answer' in this.action) {
                if (this.action.question_type === 'radio') {
                    var answer = Object.keys(this.action.question_answer)[0];
                    this.survey.setValue(this.action.type, answer);
                } else {
                    var answer = Object.values(this.action.question_answer)[0];
                    this.survey.setValue(this.action.type, answer);
                }
            }

            // Don't show native navigation
            this.survey.showNavigationButtons = false;

            // Transition questions
            this._transitionQuestions()
        },
        /**
         * Load survey current item info
         * to the info block
         * @private
         */
        _loadProductInfo: function () {
            var self = this;

            $('.rma-image-box').html('<img src=' + self.action.action_item_thumbnail + ' width="60" height="60">');
            $('.rma-product-name').html(self.action.action_item_title);

            if (self.action.action_item_color) {
                $('.rma-product-options').html('<div class="rma-product-swatch color"><span class="label">Color:</span>' +
                    '<span class="value">' + self.action.action_item_color + '</span></div>');
            }

            if (self.action.action_item_size) {
                $('.rma-product-options').append('<div class="rma-product-swatch size"><span class="label">Size:</span>' +
                    '<span class="value">' + self.action.action_item_size + '</span></div>');
            }

            setTimeout(function () {
                self.productInfoBlockWrapper.css('display', 'flex');
                $('.product-info-block').animate({'opacity': 1}, 'fast');
            }, 500);
        },
        /**
         * If survey finished then redirect
         * to success and post request id
         * Else redirect to url
         */
        _redirect: function() {
            if ('redirect_path' in this.action) {
                if (this.action.redirect_path === 'kf_rma/customer/success') {
                    // Replace current url with returns page
                    history.replaceState(null, '', '/aw_rma/customer');

                    // Redirect with survey action as POST payload
                    var payload = { 'request_id': this.action.action_request_id };
                    $.redirect(this.action.redirect_url, payload, 'post');
                } else if (this.action.redirect_path === 'kf_rma/customer/failure') {
                    // Redirect with survey action as POST payload
                    var payload = { 'survey_action': JSON.stringify(this.action) };
                    $.redirect(this.action.redirect_url, payload, 'post');
                }
            } else {
                // Regular redirect
                $.redirect(this.action.redirect_url, null, 'get');
            }
        },
        /**
         * Handle AJAX received survey actions
         * @param action
         * @param status
         * @private
         */
        _initAction: function(action, status) {
            if (status === 'success' && action !== null) {
                this.action = action;
                this.currentItem = action;
                if (this.action.type === 'question') {
                    this._loadQuestion();
                    this._loadNavigation();
                    this._loadProductInfo();
                } else if (this.action.type === 'redirect') {
                    this._redirect();
                }
                this.previousItem = action;
            }
        },
        /**
         * Create GET request to receive initial
         * survey data / action
         * @private
         */
        _request: function() {
            var self = this;

            $.get(this.options.apiUrl, function(data, status) {
                self._initAction(data, status);
            });
        },
        /**
         * Create POST request to send finished survey
         * data and receive additional survey action
         * @param finishedAction
         * @private
         */
        _send: function(finishedAction) {
            var self = this;

            if (typeof finishedAction === 'undefined') {
                finishedAction = {};
            }

            if (self.clicked === 'previous') {
                finishedAction['requested_previous'] = true;
                finishedAction['requested_next'] = false;
            } else if (self.clicked === 'next') {
                finishedAction['requested_previous'] = false;
                finishedAction['requested_next'] = true;
            }

            var payload = { 'survey_action': JSON.stringify(finishedAction) };

            $.post(this.options.apiUrl, payload, function(data, status) {
                self._initAction(data, status);
            })
        },
        /**
         * Survey widget initialization
         * @private
         */
        _create: function () {
            // Load survey, question, navigation elements
            this.$survey = $(this.element);
            this.$questions = this.$survey.find('.questions');
            this.$previous = this.$survey.find('.previous');
            this.$next = this.$survey.find('.next');

            // Request survey data
            this._request();
        }
    });

    return $.rmaSurvey.js;
});
