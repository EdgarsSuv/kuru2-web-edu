/**
 * @category  Kurufootwear
 * @package   Kurufootwear\Rma
 * @copyright Copyright (c) 2017 Scandiweb, Inc (http://scandiweb.com)
 * @license   http://opensource.org/licenses/OSL-3.0 The Open Software License 3.0 (OSL-3.0)
 */

define([
    'jquery',
    'jquery/ui'
], function($) {
    'use strict';

    $.widget('infoHover.js', {
        $productInfoBlockWrapper: $('.product-info-block-wrapper'),
        $productInfoBlockToggler: $('.info-block-toggler'),
        /**
         * Load survey current item info
         * to the info block
         * @private
         */
        _loadProductInfo: function () {
            var self = this;
            setTimeout(function () {
                self.$productInfoBlockWrapper.css('display', 'flex');
                if ($(window).width() >= 1024) {
                    self._showProductInfo();
                    setTimeout(function () {
                        self._hideProductInfo();
                    }, 3000);
                }
            }, 500);
        },
        /**
         * Show current item info block
         * @private
         */
        _showProductInfo: function () {
            $('.product-info-block').animate({'margin-right': 0}, 'fast');
            if (typeof(this.$productInfoBlockToggler) !== 'undefined') {
                this.$productInfoBlockToggler.animate({'width': 5}, 'fast');
            }
        },
        /**
         * Hide current item info block
         * @private
         */
        _hideProductInfo: function () {
            $('.product-info-block').animate({'margin-right': -300}, 'fast');
            if (typeof(this.$productInfoBlockToggler) !== 'undefined') {
                this.$productInfoBlockToggler.animate({'width': 10}, 'fast');
            }
        },
        /**
         * Item info hover widget initialization
         */
        _create: function () {
            // Load widget, init & bind animations
            var self = this;

            if (this.previousItem === undefined || this.currentItem.action_item_full_id !== this.previousItem.action_item_full_id) {
                this._loadProductInfo();
            }

            self.productInfoBlockWrapper.hover(this._showProductInfo, this._hideProductInfo);

            $(window).on('resize', function() {
                if ($(window).width() >= 1024) {
                    self.productInfoBlockWrapper.hover(self._showProductInfo, self._hideProductInfo);
                    $('.info-block-toggler').css('width', 10);
                }
                else {
                    self.productInfoBlockWrapper.unbind('mouseenter mouseleave');
                    $('.info-block-toggler').css('width', 0);
                }
            });
        }
    });

    return $.infoHover.js;
});
