/**
 * @category    Kurufootwear
 * @package     Kurufootwear\Rma
 * @author      Krisjanis Veinbahs <info@scandiweb.com>
 * @copyright   Copyright (c) 2017 KURU Footwear. All rights reserved.
 */

var config = {
    paths: {
        rmaSurvey: 'Kurufootwear_Rma/js/rmaSurvey',
        infoHover: 'Kurufootwear_Rma/js/infoHover'
    },
    shim: {
        'Kurufootwear_Rma/js/lib/survey_jquery': ['jquery'],
        'Kurufootwear_Rma/js/lib/redirect_jquery': ['jquery']
    },
    config: {
        mixins: {
            'Aheadworks_Rma/js/customer/request/new/select-order-form': {
                'Kurufootwear_Rma/js/customer/request/new/select-order-form-mixin': true
            }
        }
    }
};
