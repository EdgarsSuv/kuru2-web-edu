/**
 * @category    Kurufootwear
 * @package     Kurufootwear\Rma
 * @author      Jim McGowen <jim@kurufootwear.com>
 * @copyright   Copyright (c) 2017 KURU Footwear. All rights reserved.
 */

define([
    'jquery',
    'jquery/ui',
    'jquery/validate',
    'mage/translate'
], function($) {
    'use strict';
    
    return function(config) {
        $.validator.addMethod(
            'validate_other_skus',
            function(value) {
                var checked = false;
                $('.wi-item-checkbox').each(function(i, e) {
                    e = $(e);
                    if (checked === false && e.is(':checked')) {
                        checked = true;
                    }
                });
                
                if (checked === false && value.trim().length === 0) {
                    return false;
                }
                
                return true;
            },
            $.mage.__('Please check an item in the list above or enter another SKU here.')
        );
        
        $.validator.addMethod(
            'validate_grade',
            function(value) {
                var el = $('#item_' + value);
                if (el.is(':checked')) {
                    var gradeChosen = $('input[name="warehouse_inspection[grades][' + value + ']').is(':checked');
                    return gradeChosen;
                }
                else {
                    $('input[name="warehouse_inspection[grades][' + value + ']').prop('checked', false);
                }
                
                return true;
            },
            $.mage.__('Please select a grade.')
        );
        
        var enableWarehouseTabValidation = function(enable) {
            if (enable) {
                $('#wi_inspector_initials').attr('data-validate', '{required:true}');
                $('#wi_other_skus').attr('data-validate', '{validate_other_skus:true}');
            }
            else {
                $('#wi_inspector_initials').removeAttr('data-validate');
                $('#wi_other_skus').removeAttr('data-validate');
                // TODO: Reset any validation errors
            }
        };
        
        $('document').ready(function() {
            var statusSelect = $('#request_status');
            if (statusSelect.val() == config.STATUS_WAREHOUSE_APPROVED) {
                enableWarehouseTabValidation(true);
            }
            
            statusSelect.change(function() {
                if ($(this).val() == config.STATUS_WAREHOUSE_APPROVED) {
                    enableWarehouseTabValidation(true);
                }
                else {
                    console.log('here');
                    enableWarehouseTabValidation(false);
                }
            });
            
            $('#complete_inspection_btn').click(function() {
                statusSelect.val(config.STATUS_WAREHOUSE_APPROVED).change();
            });
        });
    };
});
