<?php
/**
 * @category    Kurufootwear
 * @package     Kurufootwear\Rma
 * @author      Krišjānis Veinbahs <info@scandiweb.com>
 * @copyright   Copyright (c) 2017 Scandiweb, Inc (http://scandiweb.com)
 * @license     http://opensource.org/licenses/OSL-3.0 The Open Software License 3.0 (OSL-3.0)
 */
namespace Kurufootwear\Rma\Block\Customer;

use Kurufootwear\Rma\Controller\Customer\Success;
use Kurufootwear\Rma\Model\Survey;
use Kurufootwear\Rma\Model\SurveyAction;
use Magento\Sales\Model\Order\ItemFactory;
use Kurufootwear\Rma\Model\RequestFactory;
use Magento\Framework\View\Element\Template;
use Magento\Framework\App\Request\Http;
use Magento\Framework\Json\Helper\Data as JsonHelper;
use Magento\Framework\App\RequestInterface;
use Magento\Framework\View\Element\Template\Context;

class Failure extends Template
{
    /**
     * @var JsonHelper
     */
    protected $jsonHelper;

    /**
     * @var Survey
     */
    protected $survey;

    /**
     * Failure constructor.
     * @param JsonHelper $jsonHelper
     * @param Context $context
     * @param Survey $survey
     * @param array $data
     */
    public function __construct(
        JsonHelper $jsonHelper,
        Context $context,
        Survey $survey,
        array $data = []
    )
    {
        $this->jsonHelper = $jsonHelper;
        $this->survey = $survey;

        parent::__construct(
            $context,
            $data
        );
    }

    /**
     * Retrieve POST'ed Survey action
     * @return mixed|null
     */
    public function getSurveyAction()
    {
        /** @var RequestInterface $request */
        $request = $this->getRequest();
        if ($request->getMethod() == Http::METHOD_POST) {
            $postJson = $this->getRequest()->getPostValue(SurveyAction::CLASS_NAME);
            $surveyAction = $this->jsonHelper->jsonDecode($postJson);

            return $surveyAction;
        }

        return null;
    }

    /**
     * Generate url to success page
     * @return string
     */
    public function getSuccessUrl()
    {
        return $this->getBaseUrl() . Success::RESOLUTION_URL;
    }


    /**
     * Generate url to this failure controller
     * @return string
     */
    public function getFailureUrl()
    {
        return $this->getBaseUrl() . 'kf_rma/customer/failure';
    }

    /**
     * Generate url to rma page
     * @return string
     */
    public function getRmaUrl()
    {
        return $this->getBaseUrl() . 'aw_rma/customer/createRequestStep';
    }

    /**
     * Return running survey's item count
     * @return int
     */
    public function getSurveyItemCount()
    {
        return count($this->survey->load()
            ->getItems());
    }

    public function getIsItemLast()
    {
        return $this->survey->isLastItem();
    }

    /**
     * Return failed item thumbnail if it exists
     * @param $action
     * @return null
     */
    public function getItemThumbnail($action)
    {
        if (array_key_exists(SurveyAction::ACTION_ITEM_THUMBNAIL, $action)) {
            return $action[SurveyAction::ACTION_ITEM_THUMBNAIL];
        } else {
            return null;
        }
    }

    /**
     * Return failed item color if it exists
     * @param $action
     * @return null
     */
    public function getItemColor($action)
    {
        if (array_key_exists(SurveyAction::ACTION_ITEM_COLOR, $action)) {
            return $action[SurveyAction::ACTION_ITEM_COLOR];
        } else {
            return null;
        }
    }

    /**
     * Return failed item size if it exists
     * @param $action
     * @return null
     */
    public function getItemSize($action)
    {
        if (array_key_exists(SurveyAction::ACTION_ITEM_SIZE, $action)) {
            return $action[SurveyAction::ACTION_ITEM_SIZE];
        } else {
            return null;
        }
    }

    /**
     * Return failed item title if it exists
     * @param $action
     * @return null
     */
    public function getItemTitle($action)
    {
        if (array_key_exists(SurveyAction::ACTION_ITEM_TITLE, $action)) {
            return $action[SurveyAction::ACTION_ITEM_TITLE];
        } else {
            return null;
        }
    }
}
