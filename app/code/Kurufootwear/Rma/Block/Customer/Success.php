<?php
/**
 * @category    Kurufootwear
 * @package     Kurufootwear\Rma
 * @author      Krišjānis Veinbahs <info@scandiweb.com>
 * @copyright   Copyright (c) 2017 Scandiweb, Inc (http://scandiweb.com)
 * @license     http://opensource.org/licenses/OSL-3.0 The Open Software License 3.0 (OSL-3.0)
 */
namespace Kurufootwear\Rma\Block\Customer;

use Kurufootwear\Migration\Setup\Migration\RmaFieldMigration as Migration;
use Kurufootwear\Rma\Helper\SurveyLogic;
use Kurufootwear\Rma\Model\CustomerExchangeFactory;
use Kurufootwear\Rma\Model\Survey;
use Magento\Catalog\Model\CategoryRepository;
use Magento\Customer\Model\Session;
use Magento\Sales\Model\Order\ItemFactory;
use Magento\Catalog\Model\Category;
use Kurufootwear\Rma\Model\RequestFactory;
use Magento\Catalog\Api\Data\ProductInterface;
use Magento\Framework\View\Element\Template;
use Magento\Framework\App\Request\Http;
use Magento\Framework\App\RequestInterface;
use Kurufootwear\Catalog\Model\UrlParser;
use Magento\Framework\View\Element\Template\Context;
use Magento\Store\Model\Information;
use Magento\Store\Model\StoreManagerInterface;

class Success extends Template
{
    /**
     * @var RequestFactory
     */
    protected $requestFactory;

    /**
     * @var ProductInterface
     */
    protected $product;

    /**
     * @var ItemFactory
     */
    protected $orderItemFactory;

    /**
     * @var CategoryRepository
     */
    protected $categoryRepository;

    /**
     * @var UrlParser
     */
    protected $urlParser;

    /**
     * @var SurveyLogic
     */
    protected $surveyLogic;

    /**
     * @var Survey
     */
    protected $survey;

    /**
     * @var string
     */
    protected $rmaId;

    /**
     * @var Information
     */
    protected $storeInfo;

    /**
     * @var StoreManagerInterface
     */
    protected $storeManager;

    /**
     * @var CustomerExchangeFactory
     */
    protected $customerExchange;

    /**
     * @var Session
     */
    protected $customerSession;

    /**
     * Success constructor.
     * @param Context $context
     * @param RequestFactory $requestFactory
     * @param ItemFactory $orderItemFactory
     * @param CategoryRepository $categoryRepository
     * @param UrlParser $urlParser
     * @param Survey $survey
     * @param SurveyLogic $surveyLogic
     * @param Information $storeInfo
     * @param CustomerExchangeFactory $customerExchangeFactory
     * @param Session $customerSession
     * @param array $data
     */
    public function __construct(
        Context $context,
        RequestFactory $requestFactory,
        ItemFactory $orderItemFactory,
        CategoryRepository $categoryRepository,
        UrlParser $urlParser,
        Survey $survey,
        SurveyLogic $surveyLogic,
        Information $storeInfo,
        CustomerExchangeFactory $customerExchangeFactory,
        Session $customerSession,
        array $data = []
    )
    {
        $this->requestFactory = $requestFactory;
        $this->orderItemFactory = $orderItemFactory;
        $this->categoryRepository = $categoryRepository;
        $this->urlParser = $urlParser;
        $this->survey = $survey;
        $this->surveyLogic = $surveyLogic;
        $this->storeInfo = $storeInfo;
        $this->customerExchange = $customerExchangeFactory;
        $this->customerSession = $customerSession;
        $this->storeManager = $context->getStoreManager();

        parent::__construct(
            $context,
            $data
        );
    }

    /**
     * Check if single request item return reason
     * by logic requires category page link
     * @return bool
     */
    public function isCategoryLinkNecessary()
    {
        if ($this->isSingleItemRequest()) {
            $reasonLabel = $this->getFirstItemReason();
            $styleLabel = Migration::$fields['reason']['choices'][Migration::NO_STYLE];
            $otherLabel = Migration::$fields['reason']['choices'][Migration::REASON_OTHER];

            return ($reasonLabel == $styleLabel) ||
                ($reasonLabel == $otherLabel);
        }

        return false;
    }

    /**
     * Check if single request item return reason
     * by logic requires preselected product page link
     * @return bool
     */
    public function isProductLinkNecessary()
    {
        if ($this->isSingleItemRequest()) {
            $reasonLabel = $this->getFirstItemReason();
            $styleLabel = Migration::$fields['reason']['choices'][Migration::NO_STYLE];
            $otherLabel = Migration::$fields['reason']['choices'][Migration::REASON_OTHER];

            return ($reasonLabel != $styleLabel) ||
                ($reasonLabel != $otherLabel);
        }

        return false;
    }

    public function getLabelUrl()
    {
        $baseUrl = $this->getBaseUrl();
        $printLabelPath = 'kf_rma/customer/shippingLabel/id/' . $this->getRequestedRmaId();

        return $baseUrl . $printLabelPath;
    }

    /**
     * Return points issued for exchange RMA
     */
    public function getIssuedPoints()
    {
        $request = $this->getRequestedRma();
        $exchange = $this->customerExchange->create()->load($request->getId(), 'request_id');
        $points = $exchange ? $exchange->getPointsIssued() : '';

        return $points;
    }

    /**
     * Return whether last RMA was an exchange
     * @return mixed
     */
    public function getIsExchange()
    {
        return $this->customerSession->getIsRmaExchange();
    }

    /**
     * Return single item request's product page
     * w/ color preselected if possible
     */
    public function getRequestProductUrl()
    {
        if ($this->isProductLinkNecessary()) {
            $item = $this->getFirstOrderItem();
            $product = $item->getProduct();

            /**
             * If product has configurable parent
             * create preselected color url
             * Else just return products url
             */
            if ($parentItemId = $item->getParentItemId()) {
                $parentItem = $this->orderItemFactory->create()->load($parentItemId);
                $parentProduct = $parentItem->getProduct();
                $colorCode = $this->urlParser->getAttributeLabel(intval($product->getData('color')), 'color');
                $url = $parentProduct->getProductUrl() . '?color=' . $colorCode;
            } else {
                $url = $product->getProductUrl();
            }

            return $url;
        }

        return null;
    }

    /**
     * Return single item request's product root category
     * or whatever else is available
     */
    public function getRequestCategoryUrl()
    {
        if ($this->isCategoryLinkNecessary()) {
            $item = $this->getFirstOrderItem();
            // Get configurable product if possible
            if ($parentItemId = $item->getParentItemId()) {
                $item = $this->orderItemFactory->create()->load($parentItemId);
            }
            $product = $item->getProduct();

            $categoryIds = $product->getCategoryIds();

            /**
             * Find in which main categories is
             * this product
             * @var Category $category
             */
            foreach($categoryIds as $categoryId) {
                $category = $this->categoryRepository->get($categoryId);
                $urlKey = $category->getUrlKey();

                if ($urlKey == 'womens-shoes') {
                    $womensUrl = $category->getUrl();
                } else if ($urlKey == 'mens-shoes') {
                    $mensUrl = $category->getUrl();
                } else {
                    $fallbackUrl = $category->getUrl();
                }
            }

            if (isset($womensUrl) && isset($mensUrl)) {
                return (bool)rand(0,1) ? $mensUrl : $womensUrl;
            } else if (isset($womensUrl)) {
                return $womensUrl;
            } else if (isset($mensUrl)) {
                return $mensUrl;
            } else {
                return $fallbackUrl;
            }
        }

        return null;
    }

    /**
     * Add base url to path and
     * return full URL path
     * @param $path
     * @return string
     */
    public function getFullPathUrl($path)
    {
        return $this->getBaseUrl() . $path;
    }

    /**
     * Get contact information for store
     */
    public function getStoreContacts()
    {
        $store = $this->storeManager->getStore();

        return $this->storeInfo->getStoreInformationObject($store);
    }

    /**
     * Retrieve first rma request item
     * @return mixed
     */
    public function getFirstOrderItem()
    {
        $request = $this->getRequestedRma();
        $firstItemId = $request->getItemsCollection()->getFirstItem()->getItemId();

        return $this->orderItemFactory->create()->load($firstItemId);
    }

    /**
     * Check whether current RMA contains one item
     */
    public function isSingleItemRequest()
    {
        $request = $this->getRequestedRma();
        $items = $request->getItemsCollection();

        return count($items) == 1;
    }

    /**
     * Retrieve POST RMA request
     * @return mixed
     */
    public function getRequestedRma()
    {
        $id = $this->getRequestedRmaId();

        return $id ? $this->requestFactory->create()->load($id) : null;
    }

    /**
     * Return RMA request id, try saving
     * request if id is null
     * @return mixed|null|string
     */
    private function getRequestedRmaId()
    {
        if (is_null($this->rmaId)) {
            $survey = $this->survey->load();
            $this->rmaId = $survey->getRequestId();
        }

        return $this->rmaId;
    }

    /**
     * Get reason label for first item return
     */
    private function getFirstItemReason()
    {
        $request = $this->getRequestedRma();
        $firstItem = $request->getItemsCollection()->getFirstItem();
        $reason = $firstItem->getCustomFieldByName('reason');
        $itemReasonId = $firstItem->getCustomFields($reason->getId());

        return $reason->getOptionLabelByValue($itemReasonId);
    }
}
