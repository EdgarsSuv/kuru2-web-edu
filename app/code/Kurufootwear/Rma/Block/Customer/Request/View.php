<?php
/**
 * @category    Kurufootwear
 * @package     Kurufootwear\Rma
 * @author      Krisjanis Veinbahs <krisjanisv@scandiweb.com>
 * @copyright   Copyright (c) 2017 Scandiweb, Inc (http://scandiweb.com)
 * @license     http://opensource.org/licenses/OSL-3.0 The Open Software License 3.0 (OSL-3.0)
 */

namespace Kurufootwear\Rma\Block\Customer\Request;

use Aheadworks\Rma\Helper\CustomField;
use Aheadworks\Rma\Helper\File;
use Aheadworks\Rma\Helper\Order;
use Aheadworks\Rma\Helper\Status;
use Aheadworks\Rma\Model\CustomFieldFactory as AheadworksCustomFieldFactory;
use Kurufootwear\Rma\Model\CustomFieldFactory;
use Magento\Catalog\Model\ProductFactory;
use Magento\Customer\Model\Session;
use Magento\Framework\Pricing\PriceCurrencyInterface;
use Magento\Framework\Registry;
use Magento\Framework\View\Element\Template\Context;

class View extends \Aheadworks\Rma\Block\Customer\Request\View
{
    /**
     * Custom template file path
     * @var string
     */
    protected $_template = 'customer/request/view.phtml';

    public function __construct(
        Context $context,
        Registry $coreRegistry,
        PriceCurrencyInterface $priceCurrency,
        ProductFactory $productFactory,
        Session $customerSession,
        AheadworksCustomFieldFactory $aheadworksCustomFieldFactory,
        CustomField $customFieldHelper,
        File $fileHelper,
        Status $statusHelper,
        Order $orderHelper,
        CustomFieldFactory $customFieldFactory,
        array $data = []
    )
    {
        parent::__construct(
            $context,
            $coreRegistry,
            $priceCurrency,
            $productFactory,
            $customerSession,
            $aheadworksCustomFieldFactory,
            $customFieldHelper,
            $fileHelper,
            $statusHelper,
            $orderHelper,
            $data
        );
    }

    /**
     * @param string $customFieldName
     * @param \Aheadworks\Rma\Model\RequestItem $requestItem
     * @return string
     */
    public function getRequestItemCustomFieldHtml($customFieldName, $requestItem)
    {
        /** @var \Aheadworks\Rma\Model\CustomField $customField */
        $customField = $this->customFieldFactory->create()->loadByName($customFieldName);
        $id = $requestItem->getCustomFieldValue($customField->getId());
        if (is_null($id)) {
            $value = null;
        } else {
            $value = $customField->getStoreOptions()[$id];
        }

        return $value;
    }
}
