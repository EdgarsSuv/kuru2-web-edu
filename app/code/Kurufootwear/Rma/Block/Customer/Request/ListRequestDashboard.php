<?php
/**
 * @vendor      Kurufootwear
 * @module      Kurufootwear_Rma
 * @author      Krisjanis Veinbahs <info@scandiweb.com>
 * @copyright   Copyright (c) 2017 Scandiweb. All rights reserved.
 * @license     http://opensource.org/licenses/afl-3.0.php Academic Free License (AFL 3.0)
 */
namespace Kurufootwear\Rma\Block\Customer\Request;

use Aheadworks\Rma\Block\Customer\Request\ListRequest as AheadworksListRequest;

class ListRequestDashboard extends AheadworksListRequest
{
    /**
     * Use a custom template for summary
     * @var string
     */
    protected $_template = 'customer/request/list-summary.phtml';
}
