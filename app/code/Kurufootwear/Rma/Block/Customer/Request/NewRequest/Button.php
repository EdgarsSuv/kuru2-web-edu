<?php
/**
 * @category    Kurufootwear
 * @package     Kurufootwear\Rma
 * @author      Krisjanis Veinbahs <info@scandiweb.com>
 * @copyright   Copyright (c) 2017 Scandiweb, Inc (http://scandiweb.com)
 * @license     http://opensource.org/licenses/OSL-3.0 The Open Software License 3.0 (OSL-3.0)
 */
namespace Kurufootwear\Rma\Block\Customer\Request\NewRequest;

use Kurufootwear\Rma\Model\CustomerExchangeFactory;
use Magento\Customer\Model\Session;
use Magento\Framework\View\Element\Template;
use Magento\Framework\View\Element\Template\Context;

class Button extends Template {
    /**
     * @var Session
     */
    protected $customerSession;

    /**
     * @var CustomerExchangeFactory
     */
    protected $customerExchangeFactory;

    /**
     * Button constructor.
     * @param Context $context
     * @param Session $customerSession
     * @param CustomerExchangeFactory $customerExchangeFactory
     * @param array $data
     */
    public function __construct(
        Context $context,
        Session $customerSession,
        CustomerExchangeFactory $customerExchangeFactory,
        array $data = []
    )
    {
        $this->customerSession = $customerSession;
        $this->customerExchangeFactory = $customerExchangeFactory;

        parent::__construct(
            $context,
            $data
        );
    }

    /**
     * @return bool
     */
    public function isOpenExchange()
    {
        if ($customerId = $this->customerSession->getCustomerId()) {
            /**
             * @var \Kurufootwear\Rma\Model\ResourceModel\CustomerExchange $exchangeCollection
             */
            $exchange = $this->customerExchangeFactory->create();
            $exchangeCollection = $exchange->getCollection()
                ->addFieldToFilter('customer_id', ['eq' => $customerId])
                ->addFieldToFilter('pending_order_id', ['null' => true])
                ->addFieldToFilter('rma_complete', ['eq' => 0]);

            return $exchangeCollection->count() > 0;
        } else {
            return false;
        }
    }
}
