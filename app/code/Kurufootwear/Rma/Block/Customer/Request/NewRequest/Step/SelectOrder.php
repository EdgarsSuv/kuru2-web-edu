<?php
/**
 * @category    Kurufootwear
 * @package     Kurufootwear\Rma
 * @author      Kristaps Stalidzāns <info@scandiweb.com>
 * @copyright   Copyright (c) 2017 Scandiweb, Inc (http://scandiweb.com)
 * @license     http://opensource.org/licenses/OSL-3.0 The Open Software License 3.0 (OSL-3.0)
 */

namespace Kurufootwear\Rma\Block\Customer\Request\NewRequest\Step;

use Aheadworks\Rma\Helper\CmsBlock;
use Aheadworks\Rma\Helper\Order;
use Magento\Customer\Model\Session;
use Magento\Framework\Pricing\PriceCurrencyInterface;
use Magento\Framework\Registry;
use Magento\Framework\View\Element\Template\Context;
use Magento\Sales\Model\ResourceModel\Order\CollectionFactory;
use Kurufootwear\Rma\Model\CustomerExchangeFactory;

class SelectOrder extends \Aheadworks\Rma\Block\Customer\Request\NewRequest\Step\SelectOrder
{
    /**
     * @var string
     */
    protected $_template = 'Kurufootwear_Rma::customer/request/newrequest/step/selectorder.phtml';

    /**
     * @var Session
     */
    protected $customerSession;

    /**
     * @var CustomerExchangeFactory
     */
    protected $customerExchangeFactory;

    /**
     * SelectOrder constructor.
     * @param Context $context
     * @param Session $customerSession
     * @param PriceCurrencyInterface $priceCurrency
     * @param Registry $coreRegistry
     * @param CollectionFactory $orderCollectionFactory
     * @param CmsBlock $cmsBlockHelper
     * @param Order $orderHelper
     * @param CustomerExchangeFactory $customerExchangeFactory
     * @param array $data
     */
    public function __construct(
        Context $context,
        Session $customerSession,
        PriceCurrencyInterface $priceCurrency,
        Registry $coreRegistry,
        CollectionFactory $orderCollectionFactory,
        CmsBlock $cmsBlockHelper,
        Order $orderHelper,
        CustomerExchangeFactory $customerExchangeFactory,
        array $data = []
    )
    {
        $this->customerSession = $customerSession;
        $this->customerExchangeFactory = $customerExchangeFactory;

        parent::__construct(
            $context,
            $customerSession,
            $priceCurrency,
            $coreRegistry,
            $orderCollectionFactory,
            $cmsBlockHelper,
            $orderHelper,
            $data
        );
    }

    /**
     * Check if any order is available for return - by default false
     * If any order is not finished return true
     * @return bool
     */
    public function areReturnsAvailable() {
        $this->getOrders();

        foreach ($this->getOrders() as $order) {
            if (!$this->isOrderFinished($order)) {
                return true;
            }
        }

        return false;
    }

    /**
     * @return bool
     */
    public function isOpenExchange()
    {
        if ($customerId = $this->customerSession->getCustomerId()) {
            /**
             * @var \Kurufootwear\Rma\Model\ResourceModel\CustomerExchange $exchangeCollection
             */
            $exchange = $this->customerExchangeFactory->create();
            $exchangeCollection = $exchange->getCollection()
                ->addFieldToFilter('customer_id', ['eq' => $customerId])
                ->addFieldToFilter('pending_order_id', ['null' => true])
                ->addFieldToFilter('rma_complete', ['eq' => 0]);

            return $exchangeCollection->count() > 0;
        } else {
            return false;
        }
    }

    /**
     * Return whether order is finished
     * @param $order
     * @return bool
     */
    public function isOrderFinished($order)
    {
        return $this->getOrderLeftCount($order) == 0;
    }

    /**
     * Return how many returnable items are in order
     * @param $order
     * @return bool
     */
    public function getOrderLeftCount($order)
    {
        $count = 0;

        foreach ($order->getItemsCollection() as $item) {
            /**
             * Check only non-configurables
             * as only they appear in order views
             */
            if ($item->getProductType() == 'simple') {
                $itemRenderer = $this->getItemRenderer($item);
                $maxItems = $itemRenderer->getItemMaxCount($item);
                $allowedForOrder = $itemRenderer->isAllowedForOrder($order);
                $itemFinished = !($maxItems && $allowedForOrder);

                if (!$itemFinished) {
                    $count++;
                }
            }
        }

        return $count;
    }

    /**
     * Get selected order id
     *
     * @return string
     */
    public function getSelectedOrderId()
    {
        return $this->getRequest()->getParam('order_id');
    }
}
