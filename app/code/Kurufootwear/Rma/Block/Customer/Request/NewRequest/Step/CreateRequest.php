<?php
/**
 * @category    Kurufootwear
 * @package     Kurufootwear\Rma
 * @author      Krisjanis Veinbahs <info@scandiweb.com>
 * @copyright   Copyright (c) 2017 KURU Footwear. All rights reserved.
 */
namespace Kurufootwear\Rma\Block\Customer\Request\NewRequest\Step;

use Aheadworks\Rma\Block\Customer\Request\NewRequest\Step\CreateRequest as AheadworksCreateRequest;

class CreateRequest extends AheadworksCreateRequest
{
    protected $_template = 'customer/request/newrequest/step/createrequest.phtml';

    /**
     * Return survey request controller path
     * @return string
     */
    public function getSurveyActionUrl()
    {
        $surveyController = 'kf_rma/customer/requestSurveyAction';

        return $this->getUrl($surveyController);
    }
}
