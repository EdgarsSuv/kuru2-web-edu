<?php
/**
 * @category    Kurufootwear
 * @package     Kurufootwear\Rma
 * @author      Kristaps Stalidzāns <info@scandiweb.com>
 * @copyright   Copyright (c) 2017 Scandiweb, Inc (http://scandiweb.com)
 * @license     http://opensource.org/licenses/OSL-3.0 The Open Software License 3.0 (OSL-3.0)
 */
namespace Kurufootwear\Rma\Block\Customer\Request\NewRequest\Step\SelectOrder\Items;

use Kurufootwear\Rma\Helper\Order;
use Magento\Sales\Model\Order\Item;
use Magento\Framework\UrlInterface;
use Magento\Framework\View\Element\Template\Context;
use Magento\Store\Model\StoreManagerInterface;

class Renderer extends \Aheadworks\Rma\Block\Customer\Request\NewRequest\Step\SelectOrder\Items\Renderer
{
    /**
     * @var array
     */
    protected $itemRenderersTemplates = [
        'default' => 'Kurufootwear_Rma::customer/request/newrequest/step/selectorder/items/renderer/default.phtml',
        'bundle' => 'Kurufootwear_Rma::customer/request/newrequest/step/selectorder/items/renderer/bundle.phtml',
        'configurable' => 'Kurufootwear_Rma::customer/request/newrequest/step/selectorder/items/renderer/configurable.phtml'
    ];

    /**
     * @var StoreManagerInterface
     */
    protected $storeManager;

    /**
     * @var Order
     */
    protected $orderHelper;

    /**
     * Renderer constructor.
     * @param Context $context
     * @param Order $orderHelper
     * @param array $data
     */
    public function __construct(
        Context $context,
        Order $orderHelper,
        array $data = []
    )
    {
        $this->storeManager = $context->getStoreManager();
        $this->orderHelper = $orderHelper;

        parent::__construct(
            $context,
            $orderHelper,
            $data
        );
    }

    /**
     * @param Item $orderItem
     * @return string
     */
    public function getProductImage(Item $orderItem)
    {
        $store = $this->storeManager->getStore();
        $imageUrl = $store->getBaseUrl(UrlInterface::URL_TYPE_MEDIA) . 'catalog/product' . $orderItem->getProduct()->getImage();

        return $imageUrl;
    }

    /**
     * Helper proxy function
     */
    public function getOrderLeftItemCount($order)
    {
        return $this->orderHelper->getOrderLeftCount($order);
    }

    /**
     * This is needed to take the correct templates
     *
     * @return string
     */
    protected function _toHtml()
    {
        $this->setModuleName($this->extractModuleName('Aheadworks\Rma\Block\Customer\Request\NewRequest\Step\SelectOrder\Items\Renderer'));

        return parent::_toHtml();
    }

    /**
     * Get selected order id
     *
     * @return string
     */
    public function getSelectedOrderId()
    {
        return $this->getRequest()->getParam('order_id');
    }
}
