<?php
/**
 * @category    Kurufootwear
 * @package     Kurufootwear\Rma
 * @author      Jelena Sorohova <info@scandiweb.com>
 * @copyright   Copyright (c) 2017 Scandiweb, Inc (http://scandiweb.com)
 * @license     http://opensource.org/licenses/OSL-3.0 The Open Software License 3.0 (OSL-3.0)
 */

namespace Kurufootwear\Rma\Block\Customer\Request\View;

class Actions extends \Aheadworks\Rma\Block\Customer\Request\View\Actions
{
    /**
     * Custom template file path
     * @var string
     */
    protected $_template = 'customer/request/view/actions.phtml';

    /**
     * Return current RMA's shipping label url
     * @return string
     */
    public function getShippingLabelUrl()
    {
        $rmaId = $this->getRequestModel()->getId();

        return $this->getBaseUrl() . 'kf_rma/customer/shippingLabel/id/' . $rmaId;
    }
}
