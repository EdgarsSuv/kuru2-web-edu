<?php
/**
 * @category    Kurufootwear
 * @package     Kurufootwear\Rma
 * @author      Kristaps Stalidzāns <info@scandiweb.com>
 * @copyright   Copyright (c) 2017 Scandiweb, Inc (http://scandiweb.com)
 * @license     http://opensource.org/licenses/OSL-3.0 The Open Software License 3.0 (OSL-3.0)
 */
namespace Kurufootwear\Rma\Block\Customer\Request;

use Aheadworks\Rma\Block\Customer\Request\ListRequest as AheadworksListRequest;
use Aheadworks\Rma\Helper\Order;
use Aheadworks\Rma\Helper\Order as AheadworksOrderHelper;
use Aheadworks\Rma\Model\ResourceModel\Request\CollectionFactory;
use Kurufootwear\Rma\Model\CustomerExchangeFactory;
use Magento\Catalog\Model\ProductFactory;
use Magento\Customer\Model\Session;
use Magento\Framework\App\Config\ScopeConfigInterface;
use Magento\Framework\View\Element\Template\Context;

class ListRequest extends AheadworksListRequest
{
    /**
     * @var ScopeConfigInterface
     */
    protected $scopeConfig;

    /**
     * Use a custom template for summary
     * @var string
     */
    protected $_template = 'Kurufootwear_Rma::customer/request/list.phtml';

    /**
     * @var CustomerExchangeFactory
     */
    protected $customerExchangeFactory;

    public function __construct(
        Context $context,
        Session $customerSession,
        ProductFactory $productFactory,
        CollectionFactory $requestCollectionFactory,
        Order $orderHelper,
        CustomerExchangeFactory $customerExchangeFactory,
        array $data = []
    )
    {
        $this->scopeConfig = $context->getScopeConfig();
        $this->customerExchangeFactory = $customerExchangeFactory;

        parent::__construct(
            $context,
            $customerSession,
            $productFactory,
            $requestCollectionFactory,
            $orderHelper,
            $data
        );
    }

    /**
     * Fetch from config the request period
     */
    public function getRequestPeriod()
    {
        return $this->scopeConfig->getValue(AheadworksOrderHelper::XML_PATH_RETURN_PERIOD);
    }

    /**
     * @return bool
     */
    public function isOpenExchange()
    {
        if ($customerId = $this->customerSession->getCustomerId()) {
            /**
             * @var \Kurufootwear\Rma\Model\ResourceModel\CustomerExchange $exchangeCollection
             */
            $exchange = $this->customerExchangeFactory->create();
            $exchangeCollection = $exchange->getCollection()
                ->addFieldToFilter('customer_id', ['eq' => $customerId])
                ->addFieldToFilter('pending_order_id', ['null' => true])
                ->addFieldToFilter('rma_complete', ['eq' => 0]);

            return $exchangeCollection->count() > 0;
        } else {
            return false;
        }
    }

    /**
     * Create CMS block return policy url
     */
    public function getReturnPolicyUrl()
    {
        return $this->getBaseUrl() . 'customer-service/returns-and-exchanges.html';
    }

}
