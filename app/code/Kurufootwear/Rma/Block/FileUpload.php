<?php
/**
 * @category    Kurufootwear
 * @package     Kurufootwear\Rma
 * @author      Jelena Sorohova <info@scandiweb.com>
 * @copyright   Copyright (c) 2017 Scandiweb, Inc (http://scandiweb.com)
 * @license     http://opensource.org/licenses/OSL-3.0 The Open Software License 3.0 (OSL-3.0)
 */

namespace Kurufootwear\Rma\Block;

class FileUpload extends \Aheadworks\Rma\Block\FileUpload
{
    /**
     * Custom template file path
     * @var string
     */
    protected $_template = 'fileupload.phtml';
}
