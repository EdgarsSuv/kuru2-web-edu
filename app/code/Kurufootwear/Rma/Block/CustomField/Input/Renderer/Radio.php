<?php
/**
 * @category    Kurufootwear
 * @package     Kurufootwear\Rma
 * @author      Kristaps Stalidzāns <info@scandiweb.com>
 * @copyright   Copyright (c) 2017 Scandiweb, Inc (http://scandiweb.com)
 * @license     http://opensource.org/licenses/OSL-3.0 The Open Software License 3.0 (OSL-3.0)
 */
namespace Kurufootwear\Rma\Block\CustomField\Input\Renderer;

use Aheadworks\Rma\Block\CustomField\Input\Renderer\RendererAbstract;

/**
 * Class Radio
 * @package Kurufootwear\Rma\Block\CustomField\Input\Renderer
 */
class Radio extends RendererAbstract
{
    /**
     * @var string
     */
    protected $_template = 'Kurufootwear_Rma::customfield/input/renderer/radio.phtml';

    /**
     * @var array
     */
    protected $classNames = ['radio'];

    /**
     * @return array
     */
    public function getOptions()
    {
        return $this->getWithCaptions() ? [] :
            $this->getOptionArray();
    }

    /**
     * @param int $value
     * @return bool
     */
    public function isSelected($value)
    {
        if ($this->getValue()) {
            return $value == (int)$this->getValue();
        }

        $default = $this->getCustomField()->getOption('default');

        if (is_array($default)) {
            return in_array($value, $default);
        }

        return false;
    }

    /**
     * @return bool
     */
    public function isEditable()
    {
        if (!$this->getCustomField()->getIsSystem() && $this->getValue()) {
            $enable = $this->getCustomField()->getOption('enable');

            if (!in_array($this->getValue(), $enable)) {
                return false;
            }
        }

        return parent::isEditable();
    }

    /**
     * @return string
     */
    public function getValueLabel()
    {
        if ($this->getValue()) {
            return $this->getCustomField()->getOptionLabelByValue($this->getValue());
        }

        return '';
    }

    /**
     * @return array
     */
    protected function getOptionArray()
    {
        $optionArray = $this->getCustomField()->toOptionArray();

        return is_array($optionArray) ? $optionArray : [];
    }
}
