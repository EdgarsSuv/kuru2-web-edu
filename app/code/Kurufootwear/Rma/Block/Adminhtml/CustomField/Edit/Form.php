<?php
/**
 * @category    Kurufootwear
 * @package     Kurufootwear\Rma
 * @author      Kristaps Stalidzāns <info@scandiweb.com>
 * @copyright   Copyright (c) 2017 Scandiweb, Inc (http://scandiweb.com)
 * @license     http://opensource.org/licenses/OSL-3.0 The Open Software License 3.0 (OSL-3.0)
 */
namespace Kurufootwear\Rma\Block\Adminhtml\CustomField\Edit;

use Kurufootwear\Rma\Model\Source\CustomField\Type;

class Form extends \Aheadworks\Rma\Block\Adminhtml\CustomField\Edit\Form
{
    /**
     * @var array
     */
    protected $typesWithOptions = [
        Type::SELECT_VALUE,
        Type::MULTI_SELECT_VALUE,
        Type::RADIO_BUTTON_VALUE
    ];

    /**
     * Prepare form
     *
     * @return $this
     */
    protected function _prepareForm()
    {
        parent::_prepareForm();
        $model = $this->_coreRegistry->registry('aw_rma_custom_field');
        $fieldSet = $this->getForm()->getElement('general_fieldset');

        $fieldSet->addField(
            'identifier',
            'text',
            [
                'name' => 'identifier',
                'label' => __('Question Identifier'),
                'title' => __('Question Identifier'),
                'value' => $model->getIdentifier(),
                'required' => true,
            ]
        );

        return $this;
    }
}
