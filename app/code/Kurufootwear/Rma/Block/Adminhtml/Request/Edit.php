<?php
/**
 * @category    Kurufootwear
 * @package     Kurufootwear\Rma
 * @author      Jim McGowen <jim@kurufootwear.com>
 * @copyright   Copyright (c) 2017 KURU Footwear. All rights reserved.
 */

namespace Kurufootwear\Rma\Block\Adminhtml\Request;

use Aheadworks\Rma\Block\Adminhtml\Request\Edit as AheadworksEdit;

/**
 * Class Edit
 * @package Kurufootwear\Rma\Block\Adminhtml\Request
 */
class Edit extends AheadworksEdit
{
    public function _construct()
    {
        $this->_objectId = 'id';
        $this->_blockGroup = 'Aheadworks_Rma';
        $this->_controller = 'adminhtml_request';
        
        $labelUrl = $this->getUrl('*/*/shippinglabel', ['_current' => true]);
        
        \Magento\Backend\Block\Widget\Form\Container::_construct();
        
        $this->buttonList->remove('delete');
        $this->buttonList->remove('reset');
        
        $this->buttonList->update('save', 'class', 'save');
        $this->buttonList->add(
            'saveandcontinue',
            [
                'label' => __('Save and Continue Edit'),
                'data_attribute' => [
                    'mage-init' => ['button' => ['event' => 'saveAndContinueEdit', 'target' => '#edit_form']],
                ]
            ]
        );
        $this->buttonList->add(
            'shippinglabel',
            [
                'label' => __('Display Shipping Label'),
                'onclick' => "window.open('$labelUrl', '_blank');"
            ]
        );
    }
}
