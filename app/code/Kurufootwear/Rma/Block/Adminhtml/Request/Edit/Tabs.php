<?php
/**
 * @category    Kurufootwear
 * @package     Kurufootwear\Rma
 * @author      Jim McGowen <jim@kurufootwear.com>
 * @copyright   Copyright (c) 2017 KURU Footwear. All rights reserved.
 */

namespace Kurufootwear\Rma\Block\Adminhtml\Request\Edit;

use Aheadworks\Rma\Block\Adminhtml\Request\Edit\Tabs as AheadworksTabs;

/**
 * Class Tabs
 * @package Kurufootwear\Rma\Block\Adminhtml\Request\Edit
 */
class Tabs extends AheadworksTabs
{
    /**
     * @return $this
     */
    protected function _prepareLayout()
    {
        $this->addTabAfter(
            'warehouse_inspection',
            [
                'label' => __('Warehouse Inspection'),
                'content' => $this->getLayout()->createBlock(
                    'Kurufootwear\Rma\Block\Adminhtml\Request\Edit\Tabs\WarehouseInspection',
                    'kurufootwear_rma_edit_tabs_warehouse_inspection'
                )->toHtml()
            ],
            'products'
        );
        
        parent::_prepareLayout();
        
        $this->setActiveTab('general');
        
        return $this;
    }
}
