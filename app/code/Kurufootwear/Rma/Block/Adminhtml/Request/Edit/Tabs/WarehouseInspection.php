<?php
/**
 * @category    Kurufootwear
 * @package     Kurufootwear\Rma
 * @author      Jim McGowen <jim@kurufootwear.com>
 * @copyright   Copyright (c) 2017 KURU Footwear. All rights reserved.
 */

namespace Kurufootwear\Rma\Block\Adminhtml\Request\Edit\Tabs;

use Magento\Backend\Block\Template;
use Magento\Backend\Block\Template\Context;
use Magento\Framework\Registry;
use Kurufootwear\Rma\Model\InspectionFactory;
use Aheadworks\Rma\Model\ResourceModel\RequestItem\CollectionFactory as ItemsCollectionFactory;

/**
 * Class WarehouseInspection
 * @package Kurufootwear\Rma\Block\Adminhtml\Request\Edit\Tabs
 */
class WarehouseInspection extends Template
{
    /**
     * Core registry
     *
     * @var \Magento\Framework\Registry
     */
    protected $coreRegistry;
    
    /**
     * @var string
     */
    protected $_template = 'request/edit/tabs/warehouse_inspection.phtml';
    
    /**
     * @var \KuruFootwear\Rma\Model\InspectionFactory
     */
    protected $inspectionFactory;
    
    /**
     * @var \KuruFootwear\Rma\Model\Inspection
     */
    protected $inspection;
    
    /**
     * @var \Aheadworks\Rma\Model\Request
     */
    protected $rmaRequest;
    
    /**
     * @var \Aheadworks\Rma\Model\ResourceModel\RequestItem\CollectionFactory
     */
    protected $itemsCollectionFactory;
    
    /**
     * @var \Aheadworks\Rma\Model\ResourceModel\RequestItem\Collection
     */
    protected $itemsCollection;
    
    /**
     * @param \Magento\Backend\Block\Template\Context $context
     * @param \Magento\Framework\Registry $registry
     * @param \KuruFootwear\Rma\Model\InspectionFactory $inspectionFactory
     * @param \Aheadworks\Rma\Model\ResourceModel\RequestItem\CollectionFactory $itemsCollectionFactory
     * @param array $data
     */
    public function __construct(
        Context $context,
        Registry $registry,
        InspectionFactory $inspectionFactory,
        ItemsCollectionFactory $itemsCollectionFactory,
        array $data = []
    ) {
        $this->coreRegistry = $registry;
        $this->inspectionFactory = $inspectionFactory;
        $this->itemsCollectionFactory = $itemsCollectionFactory;
        parent::__construct($context, $data);
    }
    
    /**
     * Internal constructor
     *
     * Sets the rma request object from the core registry.
     */
    protected function _construct()
    {
        $this->setRmaRequest($this->coreRegistry->registry('aw_rma_request'));
        parent::_construct();
    }
    
    /**
     * @return \Aheadworks\Rma\Model\Request
     */
    public function getRmaRequest()
    {
        return $this->rmaRequest;
    }
    
    /**
     * @param $rmaRequest
     */
    public function setRmaRequest($rmaRequest)
    {
        $this->rmaRequest = $rmaRequest;
    }
    
    /**
     * @param bool $forceReload
     *
     * @return $this|\KuruFootwear\Rma\Model\Inspection
     */
    public function getInspection($forceReload = false)
    {
        if (empty($this->inspection) || $forceReload === false) {
            $requestId = $this->getRmaRequest()->getId();
            $this->inspection = $this->inspectionFactory->create()->loadByRequestId($requestId);
        }
        
        return $this->inspection;
    }
    
    /**
     * @return string
     */
    public function getFormattedInspectedDate()
    {
        if (empty($this->inspection)) {
            $this->getInspection();
        }
        
        if (!empty($this->inspection->getInspectedDate())) {
            return $this->formatDate($this->inspection->getInspectedDate(), \IntlDateFormatter::MEDIUM, false) .
                ' ' . $this->formatTime($this->inspection->getInspectedDate(), \IntlDateFormatter::SHORT, false);
        }
        
        return '';
    }
}
