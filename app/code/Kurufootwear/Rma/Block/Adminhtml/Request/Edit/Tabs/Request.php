<?php
/**
 * @category    Kurufootwear
 * @package     Kurufootwear\Rma
 * @author      Jim McGowen <jim@kurufootwear.com>
 * @copyright   Copyright (c) 2017 KURU Footwear. All rights reserved.
 */

namespace Kurufootwear\Rma\Block\Adminhtml\Request\Edit\Tabs;

use Kurufootwear\Rma\Exception\ShippingLabelException;
use Kurufootwear\Rma\Model\Source\CustomField\Type;
use Aheadworks\Rma\Block\Adminhtml\Request\Edit\Tabs\Request as AheadworksRequest;
use Kurufootwear\Rma\Helper\ShippingLabel as ShippingLabelHelper;
use Magento\Sales\Api\OrderRepositoryInterface;
use Magento\Sales\Model\OrderFactory;

class Request extends AheadworksRequest
{
    protected $shippingLabel;
    
    protected $orderRepository;
    
    protected $orderFactory;
    
    public function __construct(
        \Aheadworks\Rma\Model\ResourceModel\CustomField\CollectionFactory $customFieldCollectionFactory,
        \Aheadworks\Rma\Model\Source\Request\Status $statusSource,
        \Magento\Backend\Block\Template\Context $context,
        \Magento\Framework\Registry $registry,
        \Magento\Framework\Data\FormFactory $formFactory,
        ShippingLabelHelper $shippingLabel,
        OrderRepositoryInterface $orderRepository,
        OrderFactory $orderFactory,
        array $data = []
    ) {
        $this->shippingLabel = $shippingLabel;
        $this->orderRepository = $orderRepository;
        $this->orderFactory = $orderFactory;
        parent::__construct($customFieldCollectionFactory, $statusSource, $context, $registry, $formFactory, $data);
    }
    
    /**
     * Overridden to make the status an editable select.
     *
     * @return $this
     */
    protected function _prepareForm()
    {
        parent::_prepareForm();

        /** @var \Kurufootwear\Rma\Model\Request $rmaRequest */
        $rmaRequest = $this->_coreRegistry->registry('aw_rma_request');
        
        $fieldSet = $this->getForm()->getElement('request_fieldset');
        $fieldSet->removeField('status_text');
        $fieldSet->addField(
            'status',
            'select',
            [
                'disabled' => false,
                'label' => 'Status',
                'name' => 'request_status',
                'required' => true,
                'title' => 'Status',
                'value' => $rmaRequest->getStatusId(),
                'values' => $this->statusSource->toOptionArrayActiveOnly($rmaRequest->getTypeId())
            ]
        );

        // Remove and re-add all the fields so that status is on top
        foreach ($this->customFieldCollection as $customField) {
            $fieldSet->removeField($customField->getId());
            $isDisabled = !in_array($rmaRequest->getStatusId(), $customField->getEditableAdminForStatusIds());
            $fieldConfig = [
                'name'      => "custom_fields[{$customField->getId()}]",
                'label'     => $customField->getName(),
                'title'     => $customField->getName(),
                'required'  => $customField->getIsRequired() && !$isDisabled,
                'disabled'  => $isDisabled,
                'value'     => $rmaRequest->getCustomFields($customField->getId())
            ];
            if (in_array($customField->getType(), [Type::SELECT_VALUE, Type::MULTI_SELECT_VALUE, Type::RADIO_BUTTON_VALUE])) {
                $fieldConfig['values'] = $customField->toOptionArray();
            }
            if ($customField->getType() == Type::MULTI_SELECT_VALUE && !$isDisabled) {
                //hack for saving empty multiselects
                $fieldSet->removeField("hidden{$customField->getId()}");
                $fieldSet->addField(
                    "hidden{$customField->getId()}",
                    "hidden",
                    ['name' => "custom_fields[{$customField->getId()}]"]);
            }
            $fieldType = $customField->getType() == Type::RADIO_BUTTON_VALUE ? Type::SELECT_VALUE : $customField->getType();
            $fieldSet->addField(
                $customField->getId(),
                $fieldType,
                $fieldConfig
            );
        }
    
        // Add the tracking number if we have one
        try {
            $shippingLabel = $this->shippingLabel->getLabel($rmaRequest);
            $trackingNumber = $shippingLabel->getTrackingNumber();
            
            switch ($shippingLabel->getProvider())
            {
                case 'FEDEX':
                    $trackingUrl = 'https://www.fedex.com/apps/fedextrack/?tracknumbers=' . $trackingNumber;
                    break;
                case 'UPS':
                    $trackingUrl = 'https://wwwapps.ups.com/tracking/tracking.cgi?tracknum=' . $trackingNumber;
                    break;
                case null:
                    // UPS tracking numbers start with "1Z"
                    if (strpos($trackingNumber, '1Z', 0) !== false) {
                        $trackingUrl = 'https://wwwapps.ups.com/tracking/tracking.cgi?tracknum=' . $trackingNumber;
                        break;
                    }
                    break;
                default:
                    $trackingUrl = '';
            }
        }
        catch (ShippingLabelException $e)
        {
            $trackingNumber = $e->getMessage();
            $trackingUrl = '';
        }
        
        if (!empty($trackingUrl)) {
            $fieldSet->addField(
                'tracking_number',
                'link',
                [
                    'label' => 'Tracking Number',
                    'name' => 'tracking_number',
                    'title' => 'Tracking Number',
                    'value' => $trackingNumber,
                    'href' => $trackingUrl,
                    'target' => '_blank'
                ]
            );
        }
        else {
            $fieldSet->addField(
                'tracking_number',
                'label',
                [
                    'label' => 'Tracking Number',
                    'name' => 'tracking_number',
                    'title' => 'Tracking Number',
                    'value' => $trackingNumber,
                ]
            );
        }
        
        // Add the pending Exchange order if there is one
        $customerExchange = $rmaRequest->getCustomerExchange();
        if ($customerExchange) {

            $fieldType = 'label';
            $fieldConfig = [
                'label' => 'Exchange Order',
                'name' => 'exchange_order',
                'title' => 'Exchange Order',
                'value' => 'Not placed'
            ];

            $exchangeOrderId = $customerExchange->getPendingOrderId();
            if (!empty($exchangeOrderId)) {
                
                /** @var \Magento\Sales\Api\Data\OrderInterface $exchangeOrder */
                $exchangeOrder = $this->orderRepository->get($exchangeOrderId);

                $fieldType = 'link';
                $fieldConfig['value'] = $exchangeOrder->getIncrementId();
                $fieldConfig['href'] = $this->getUrl(
                    'sales/order/view',
                    ['order_id' => $exchangeOrderId]
                );
                $fieldConfig['target'] = '_blank';
                $fieldConfig['note'] = 'Status: ' . $this->orderFactory->create()->loadByIncrementId($exchangeOrder->getIncrementId())->getStatusLabel();
            }

            $fieldSet->addField('exchange_order', $fieldType, $fieldConfig);
        }
        
        return $this;
    }
}
