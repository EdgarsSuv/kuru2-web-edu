<?php
/**
 * @category    Kurufootwear
 * @package     Kurufootwear\Rma
 * @author      Kristaps Stalidzāns <info@scandiweb.com>
 * @copyright   Copyright (c) 2017 Scandiweb, Inc (http://scandiweb.com)
 * @license     http://opensource.org/licenses/OSL-3.0 The Open Software License 3.0 (OSL-3.0)
 */
namespace Kurufootwear\Rma\Block\Adminhtml\Request\Edit\Tabs;

use Kurufootwear\Rma\Model\Source\CustomField\Type;

class Products extends \Aheadworks\Rma\Block\Adminhtml\Request\Edit\Tabs\Products
{
    /**
     * Overridden to make the status an editable select.
     *
     * @return $this
     */
    protected function _prepareForm()
    {
        parent::_prepareForm();

        /** @var \Aheadworks\Rma\Model\Request $rmaRequest */
        $rmaRequest = $this->_coreRegistry->registry('aw_rma_request');

        foreach ($rmaRequest->getItemsCollection() as $item) {
            $fieldset = $this->getForm()->getElement("item_{$item->getId()}_fieldset");

            foreach ($this->customFieldCollection as $customField) {
                $fieldset->removeField("item{$item->getId()}_cf{$customField->getId()}");
                $isDisabled = !in_array($rmaRequest->getStatusId(), $customField->getEditableAdminForStatusIds());
                $fieldConfig = [
                    'name'      => "items[{$item->getId()}][custom_fields][{$customField->getId()}]",
                    'label'     => $customField->getName(),
                    'title'     => $customField->getName(),
                    'required'  => $customField->getIsRequired() && !$isDisabled,
                    'disabled'  => $isDisabled,
                    'value'     => $item->getCustomFields($customField->getId())
                ];
                if (in_array($customField->getType(), [Type::SELECT_VALUE, Type::MULTI_SELECT_VALUE, Type::RADIO_BUTTON_VALUE])) {
                    $fieldConfig['values'] = $customField->toOptionArray();
                }
                if ($customField->getType() == Type::MULTI_SELECT_VALUE && !$isDisabled) {
                    //hack for saving empty multiselects
                    $fieldset->addField(
                        "item{$item->getId()}_hidden_cf{$customField->getId()}",
                        "hidden",
                        ['name' => "items[{$item->getId()}][custom_fields][{$customField->getId()}]"]);
                }
                $fieldType = $customField->getType() == Type::RADIO_BUTTON_VALUE ? Type::SELECT_VALUE : $customField->getType();
                $fieldset->addField(
                    "item{$item->getId()}_cf{$customField->getId()}",
                    $fieldType,
                    $fieldConfig
                );
            }
        }

        return $this;
    }
}
