<?php
/**
 * @category    Kurufootwear
 * @package     Kurufootwear\Rma
 * @author      Jim McGowen <jim@kurufootwear.com>
 * @copyright   Copyright (c) 2017 KURU Footwear. All rights reserved.
 */

namespace Kurufootwear\Rma\Block\Adminhtml\Status;

class Grid extends \Aheadworks\Rma\Block\Adminhtml\Status\Grid
{
    /**
     * Overridden to make the statuses list pageable since we have way more statuses 
     * then AW planned for.
     * 
     * @return void
     */
    protected function _construct()
    {
        parent::_construct();
        $this->setPagerVisibility(true);
    }
}
