<?php
/**
 * @category    Kurufootwear
 * @package     Kurufootwear\Rma
 * @author      Krisjanis Veinbahs <info@scandiweb.com>
 * @copyright   Copyright (c) 2017 KURU Footwear. All rights reserved.
 */

namespace Kurufootwear\Rma\Model;

use Magento\Customer\Model\Session;

/**
 * SurveyItem
 *
 * A pseudo model
 * for storing survey data about
 * specific item/request
 * (an array of survey actions)
 *
 * @package Kurufootwear\Rma\Model
 */
class SurveyItem
{
    /**
     * SurveyItem data array
     */
    private $data;

    /**
     * SurveyItem data fields
     */
    const ACTIONS = 'actions';
    const ITEM_ID = 'item_id';
    const ITEM_QUANTITY_ID = 'item_quantity_id';

    /**
     * SurveyItem constructor.
     * @param Session $customerSession
     * @param array $data
     */
    public function __construct(
        Session $customerSession,
        array $data = []
    )
    {
        $this->customerSession = $customerSession;
        $this->data = $data;
    }

    /**
     * Data (de/en)coding, sanitization
     * @param $data
     * @return string
     */
    private function encode($data)
    {
        return json_encode($data, JSON_HEX_QUOT|JSON_HEX_TAG|JSON_HEX_AMP|JSON_HEX_APOS);
    }

    /**
     * @param $data
     * @return mixed
     */
    private function decode($data)
    {
        return json_decode($data);
    }

    /**
     * SurveyItem CRUD functions
     */
    public function save()
    {
        $encodedSurveyItem = $this->encode($this->data);
        $this->customerSession->setRmaSurveyItem($encodedSurveyItem);

        return $this;
    }

    /**
     * @return $this
     */
    public function load()
    {
        $encodedSurveyItem = $this->customerSession->getRmaSurveyItem();
        $this->data = $this->decode($encodedSurveyItem);

        return $this;
    }

    /**
     * @return $this
     */
    public function clean()
    {
        $this->data = [];

        return $this;
    }

    /**
     * @param $surveyItem
     * @return $this
     */
    public function update($surveyItem)
    {
        foreach ($surveyItem as $key => $value) {
            $this->setData($key, $value);
        }
        $this->data = $surveyItem;

        return $this;
    }

    /**
     * @return array
     */
    public function get()
    {
        return $this->data;
    }

    /**
     * @param $surveyItem
     * @return $this
     */
    public function set($surveyItem)
    {
        $this->data = $surveyItem;

        return $this;
    }

    /**
     * SurveyItem field CRUD functions
     * @param $key
     * @param $value
     * @return $this
     */
    public function setData($key, $value)
    {
        $this->data[$key] = $value;

        return $this;
    }

    /**
     * @param $key
     * @return mixed
     */
    public function getData($key)
    {
        return $this->data[$key];
    }

    /**
     * @param $actions
     * @return SurveyItem
     */
    public function setActions($actions)
    {
        return $this->setData(self::ACTIONS, $actions);
    }

    /**
     * @return mixed
     */
    public function getActions()
    {
        return $this->getData(self::ACTIONS);
    }

    /**
     * @param $itemId
     * @return SurveyItem
     */
    public function setItemId($itemId)
    {
        return $this->setData(self::ITEM_ID, $itemId);
    }

    /**
     * @return mixed
     */
    public function getItemId()
    {
        return $this->getData(self::ITEM_ID);
    }


    /**
     * @param $itemQuantityId
     * @return SurveyItem
     */
    public function setItemQuantityId($itemQuantityId)
    {
        return $this->setData(self::ITEM_QUANTITY_ID, $itemQuantityId);
    }

    /**
     * @return mixed
     */
    public function getItemQuantityId()
    {
        return $this->getData(self::ITEM_QUANTITY_ID);
    }

    /**
     * @param $item
     * @return mixed
     */
    public function getItemFullId($item = null)
    {
        if ($item !== null) {
            $itemId = $item[SurveyItem::ITEM_ID];
            $itemQuantityId = $item[SurveyItem::ITEM_QUANTITY_ID];
        } else {
            $itemId = $this->getItemId();
            $itemQuantityId = $this->getItemQuantityId();
        }

        return $itemId . '_' . $itemQuantityId;
    }

    /**
     * @param array $action
     * @return $this
     */
    public function addAction(array $action)
    {
        $this->data[self::ACTIONS][$action[SurveyAction::ACTION_ID]] = $action;

        return $this;
    }

    /**
     * @param null $type
     * @return mixed
     */
    public function getLastAction($type = null)
    {
        $actions = $this->data[self::ACTIONS];

        if ($type != null) {
            $isOfType = array($this, 'isActionOfType');
            $actions = array_filter($actions, $isOfType);
        }

        return end($actions);
    }

    /**
     * @param $action
     * @param $type
     * @return bool
     */
    private function isActionOfType($action, $type)
    {
        return $action[SurveyAction::TYPE] == $type;
    }
}
