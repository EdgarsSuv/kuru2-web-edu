<?php
/**
 * @category    Kurufootwear
 * @package     Kurufootwear\Rma
 * @author      Jim McGowen <jim@kurufootwear.com>
 * @copyright   Copyright (c) 2017 KURU Footwear. All rights reserved.
 */

namespace Kurufootwear\Rma\Model;

use Magento\Framework\Model\AbstractModel;
use Magento\Framework\DataObject\IdentityInterface;
use Magento\Framework\Data\Collection\AbstractDb;
use Magento\Framework\Model\Context;
use Magento\Framework\Model\ResourceModel\AbstractResource;
use Magento\Framework\Registry;
use Magento\Framework\Stdlib\DateTime\DateTime;

/**
 * Class Inspection
 *
 * @package Kurufootwear\Rma\Model
 */
class Inspection extends AbstractModel implements IdentityInterface
{
    const CACHE_TAG = 'kuru_rma_warehouse_inspection';
    
    protected $_cacheTag = 'kuru_rma_warehouse_inspection';
    
    protected $_eventPrefix = 'kuru_rma_warehouse_inspection';
    
    /**
     * @var DateTime
     */
    protected $dateTime;
    
    /**
     * @var \Psr\Log\LoggerInterface
     */
    protected $logger;
    
    /**
     * Inspection constructor.
     *
     * @param Context $context
     * @param Registry $registry
     * @param DateTime $dateTime
     * @param AbstractResource|null $resource
     * @param AbstractDb|null $resourceCollection
     * @param array $data
     */
    public function __construct(
        Context $context,
        Registry $registry,
        DateTime $dateTime,
        AbstractResource $resource = null,
        AbstractDb $resourceCollection = null,
        array $data = []
    ) {
        $this->dateTime = $dateTime;
        $this->logger = $context->getLogger();
        parent::__construct($context, $registry, $resource, $resourceCollection, $data);
    }
    
    /**
     * Internal Constructor
     */
    protected function _construct()
    {
        $this->_init('Kurufootwear\Rma\Model\ResourceModel\Inspection');
    }
    
    /**
     * @return array
     */
    public function getIdentities()
    {
        return [self::CACHE_TAG . '_' . $this->getId()];
    }
    
    /**
     * @param $id
     *
     * @return $this
     */
    public function loadById($id)
    {
        $this->_getResource()->load($this, $id, 'id');
        
        return $this;
    }
    
    /**
     * @param $id
     *
     * @return $this
     */
    public function loadByRequestId($id)
    {
        $this->_getResource()->load($this, $id, 'request_id');
        
        return $this;
    }
    
    /**
     * Save object data.
     */
    public function save()
    {
        $this->_getResource()->save($this);
    }
    
    /**
     * @return AbstractModel
     */
    public function beforeSave()
    {
        if (!is_string($this->getReceivedItems())) {
            $this->setReceivedItems(serialize($this->getReceivedItems()));
        }
        
        $bItemsMatch = $this->getItemsMatch() == true ? 1 : 0;
        $this->setItemsMatch($bItemsMatch);
        
        return parent::beforeSave();
    }
}
