<?php
/**
 * @category    Kurufootwear
 * @package     Kurufootwear\Rma
 * @author      Kristaps Stalidzāns <info@scandiweb.com>
 * @copyright   Copyright (c) 2017 Scandiweb, Inc (http://scandiweb.com)
 * @license     http://opensource.org/licenses/OSL-3.0 The Open Software License 3.0 (OSL-3.0)
 */
namespace Kurufootwear\Rma\Model;

use Magento\Framework\Model\ResourceModel\AbstractResource;
use Magento\Framework\Model\Context;
use Magento\Framework\Data\Collection\AbstractDb;
use Magento\Framework\Registry;
use Magento\Store\Model\StoreManagerInterface;

class CustomField extends \Aheadworks\Rma\Model\CustomField
{
    /**
     * @var StoreManagerInterface
     */
    protected $storeManager;

    /**
     * CustomField constructor.
     * @param Context $context
     * @param StoreManagerInterface $storeManager
     * @param Registry $registry
     * @param AbstractResource|null $resource
     * @param AbstractDb|null $resourceCollection
     * @param array $data
     */
    public function __construct(
        Context $context,
        StoreManagerInterface $storeManager,
        Registry $registry,
        AbstractResource $resource = null,
        AbstractDb $resourceCollection = null,
        array $data = []
    )
    {
        $this->storeManager = $storeManager;

        parent::__construct(
            $context,
            $registry,
            $resource,
            $resourceCollection,
            $data
        );
    }

    /**
     * Adds ability to load module by id and identifier
     *
     * @param int $value
     * @param null $field
     * @return $this
     */
    public function load($value, $field = null)
    {
        if (!is_numeric($value) && is_null($field)) {
            $field = 'identifier';
        }

        parent::load($value, $field);

        return $this;
    }

    /**
     * Retrieve array of store specific field options
     * @return array
     */
    public function getStoreOptions()
    {
        $options = $this->getOption('value');
        $storeOptions = array_map(array($this, 'getStoreOption'), $options);

        return $storeOptions;
    }

    /**
     * Convert option to store specific option
     * @param $option
     * @return mixed
     */
    private function getStoreOption($option)
    {
        $storeId = $this->storeManager->getStore()->getId();

        return $option[$storeId];
    }
}
