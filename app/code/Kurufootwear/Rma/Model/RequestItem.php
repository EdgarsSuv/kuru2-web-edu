<?php
/**
 * @category    Kurufootwear
 * @package     Kurufootwear\Rma
 * @author      Jim McGowen <jim@kurufootwear.com>
 * @copyright   Copyright (c) 2017 KURU Footwear. All rights reserved.
 */
namespace Kurufootwear\Rma\Model;

use Aheadworks\Rma\Model\RequestItem as AheadworksRequestItem;
use Magento\Framework\Model\Context;
use Magento\Framework\Registry;
use Magento\Framework\Model\ResourceModel\AbstractResource;
use Magento\Framework\Data\Collection\AbstractDb;

/**
 * Class RequestItem
 * @package Aheadworks\Rma\Model
 */
class RequestItem extends AheadworksRequestItem
{
    /** @var CustomFieldFactory */
    protected $customFieldFactory;
    
    /**
     * RequestItem constructor.
     *
     * @param Context $context
     * @param Registry $registry
     * @param CustomFieldFactory $customFieldFactory
     * @param AbstractResource|null $resource
     * @param AbstractDb|null $resourceCollection
     * @param array $data
     */
    public function __construct(
        Context $context,
        Registry $registry,
        CustomFieldFactory $customFieldFactory,
        AbstractResource $resource = null,
        AbstractDb $resourceCollection = null,
        array $data = []
    ) {
        $this->customFieldFactory = $customFieldFactory;
        parent::__construct($context, $registry, $resource, $resourceCollection, $data);
    }
    
    /**
     * @return int
     */
    public function getReasonId()
    {
        $id = $this->getCustomFieldByName('reason')->getId();
        $customFields = $this->getCustomFields();
        return $customFields[$id];
    }
    
    /**
     * @return string
     */
    public function getReasonLabel()
    {
        $customField = $this->getCustomFieldByName('reason');
        $value = $this->getCustomFieldValue('reason');
        return $customField->getOptionLabelByValue($value);
    }
    
    /**
     * @param $reasonId
     *
     * @return $this
     */
    public function setReason($reasonId)
    {
        $this->setCustomFieldValueByName('reason', $reasonId);
        return $this;
    }
    
    /**
     * @return string
     */
    public function getOtherDescription()
    {
        return $this->getCustomFieldValue('other_description');
    }
    
    /**
     * @param $otherDescription
     *
     * @return $this
     */
    public function setOtherDescription($otherDescription)
    {
        $this->setCustomFieldValueByName('other_description', $otherDescription);
        return $this;
    }
    
    /**
     * @param $customField int|\Aheadworks\Rma\Model\CustomField The CustomField or it's ID.
     * @param $value int|string|array The ID or the value to set the custom field to. For multiselects this will be an array of IDs or values.
     * @throws \Exception
     */
    public function setCustomFieldValue($customField, $value)
    {
        if (is_numeric($customField)) {
            $id = $customField;
            $customField = $this->customFieldFactory->create()->load($id);
        }
        else {
            $id = $customField->getId();
        }
        
        $customFields = $this->getCustomFields();
        
        if (empty($customField->getId())) {
            throw new \Exception(__("Custom field with ID $id does not exist."));
        }
        
        switch ($customField->getType()) {
            case 'select':
            case 'radio': {
                $options = $customField->toOptionArray();
                foreach ($options as $option) {
                    if (is_int($value)) {
                        $customFields[$id] = $value;
                        break;
                    }
                    else {
                        if ($value == $option['label']) {
                            $customFields[$id] = $option['value'];
                            break;
                        }
                    }
                }
                break;
            }
            case 'multiselect': {
                $customFields[$id] = array();
                $options = $customField->toOptionArray();
                foreach ($value as $val) {
                    if (is_int($val)) {
                        $customFields[$id][] = $val;
                        break;
                    }
                    else {
                        foreach ($options as $option) {
                            if ($val == $option['label']) {
                                $customFields[$id][] = $option['value'];
                                break;
                            }
                        }
                    }
                }
                break;
            }
            case 'text':
            case 'textarea': {
                $customFields[$customField->getId()] = $value;
                break;
            }
        }
        
        $this->setData('custom_fields', $customFields);
    }
    
    /**
     * @param $name string The name or identifier of a CustomField
     * @param $value int|string|array The ID or the value to set the custom field to. For multiselects this will be an array of IDs or values.
     */
    public function setCustomFieldValueByName($name, $value)
    {
        $customField = $this->getCustomFieldByName($name);
        $this->setCustomFieldValue($customField, $value);
    }
    
    /**
     * @param $name string Custom field name or identifier
     *
     * @return \Aheadworks\Rma\Model\CustomField
     * @throws \Exception
     */
    public function getCustomFieldByName($name) 
    {
        $customField = $this->customFieldFactory->create()->loadByName($name);
        if (is_null($customField->getId())) {
            // Try by identifier
            $customField = $this->customFieldFactory->create()->load($name, 'identifier');
            if (empty($customField->getId())) {
                throw new \Exception(__("Custom field '$name' does not exist."));
            }
        }
        return $customField;
    }
}
