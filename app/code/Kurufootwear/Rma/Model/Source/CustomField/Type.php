<?php
/**
 * @category    Kurufootwear
 * @package     Kurufootwear\Rma
 * @author      Kristaps Stalidzāns <info@scandiweb.com>
 * @copyright   Copyright (c) 2017 Scandiweb, Inc (http://scandiweb.com)
 * @license     http://opensource.org/licenses/OSL-3.0 The Open Software License 3.0 (OSL-3.0)
 */
namespace Kurufootwear\Rma\Model\Source\CustomField;

class Type extends \Aheadworks\Rma\Model\Source\CustomField\Type
{
    const RADIO_BUTTON_VALUE = 'radio';
    const RADIO_BUTTON_LABEL = 'Radio Select';

    /**
     * @return array
     */
    public function getOptions()
    {
        return [
            self::TEXT_VALUE            => __(self::TEXT_LABEL),
            self::TEXT_AREA_VALUE       => __(self::TEXT_AREA_LABEL),
            self::SELECT_VALUE          => __(self::SELECT_LABEL),
            self::MULTI_SELECT_VALUE    => __(self::MULTI_SELECT_LABEL),
            self::RADIO_BUTTON_VALUE    => __(self::RADIO_BUTTON_LABEL)
        ];
    }
}
