<?php
/**
 * @category    Kurufootwear
 * @package     Kurufootwear\Rma
 * @author      Jim McGowen <jim@kurufootwear.com>
 * @copyright   Copyright (c) 2017 KURU Footwear. All rights reserved.
 */

namespace Kurufootwear\Rma\Model\Source\Request;

use Aheadworks\Rma\Model\Source\Request\Status as AheadworksStatus;
use Aheadworks\Rma\Model\StatusFactory;
use Kurufootwear\Rma\Model\RequestManager;

/**
 * Class Status
 * @package Kurufootwear\Rma\Model\Source\Request
 */
class Status extends AheadworksStatus
{
    // Hard coded values that must match DB table aw_rma_request_status
    const APPROVED = 1;
    const RETURN_COMPLETE = 5;
    const WAREHOUSE_APPROVED = 29;
    const POINTS_ISSUED = 30;
    const COMPLETE_POINTS_ISSUED = 20;
    const EXPIRED_EXCHANGE = 16;
    const EXPIRED_RETURN = 17;
    const CANCELED_DUPLICATE = 19;
    const EXCHANGE_COMPLETE_REFUND_ISSUED = 21;
    const EXCHANGE_COMPLETE_NO_REFUND_OR_POINTS = 24;
    const ON_HOLD = 31;

    /** Used in migration */
    const STATUS_PENDING_EXCHANGE = 'pending_exchange';

    /** Only display these options in the RMA details page for exchanges */
    const ACTIVE_EXCHANGE_STATUSES = [
        self::APPROVED,
        self::WAREHOUSE_APPROVED,
        self::POINTS_ISSUED,
        self::COMPLETE_POINTS_ISSUED,
        self::EXPIRED_EXCHANGE,
        self::CANCELED_DUPLICATE,
        self::ON_HOLD
    ];
    
    /** Only display these options in the RMA details page for returns */
    const ACTIVE_RETURN_STATUSES = [
        self::APPROVED,
        self::WAREHOUSE_APPROVED,
        self::RETURN_COMPLETE,
        self::EXPIRED_RETURN,
        self::CANCELED_DUPLICATE,
        self::ON_HOLD
    ];

    /** @var StatusFactory StatusFactory */
    private $statusFactory;
    
    /** @var array $statusOptsWithoutTranslation */
    private $statusOptsWithoutTranslation = array();
    
    /** @var array Array of status options to display in RMA details page */
    private $optionArrayActive = null;
    
    /**
     * Status constructor.
     *
     * @param StatusFactory $statusFactory
     */
    public function __construct(StatusFactory $statusFactory)
    {
        $this->statusFactory = $statusFactory;
    }
    
    /**
     * Overridden to pull statuses from DB instead of hard coded constants.
     *
     * @return array
     */
    public function getOptionsWithoutTranslation()
    {
        if (empty($this->statusOptsWithoutTranslation)) {
            $statuses = $this->statusFactory->create()->getCollection();
            foreach ($statuses as $status) {
                $this->statusOptsWithoutTranslation[$status->getId()] = $status->getName();
            }
        }
        
        return $this->statusOptsWithoutTranslation;
    }
    
    /**
     * @param $rmaTypeId RequestManager::REQUEST_TYPE_RETURN | RequestManager::REQUEST_TYPE_EXCHANGE
     *
     * @return array|null
     */
    public function toOptionArrayActiveOnly($rmaTypeId)
    {
        if ($this->optionArrayActive === null) {
            
            $activeStatuses = $rmaTypeId == RequestManager::REQUEST_TYPE_RETURN ? self::ACTIVE_RETURN_STATUSES : self::ACTIVE_EXCHANGE_STATUSES;
            
            $this->optionArrayActive = [];
            foreach ($this->getOptions() as $value => $label) {
                if (in_array($value, $activeStatuses)) {
                    $this->optionArrayActive[] = ['value' => $value, 'label' => $label];
                }
            }
        }
        
        // Sort alphabetically
        usort($this->optionArrayActive, function($a, $b) {
            if ($a['label']->getText() == $b['label']->getText()) {
                return 0;
            }
    
            return ($a['label']->getText() < $b['label']->getText()) ? -1 : 1;
        });
        
        return $this->optionArrayActive;
    }
    
    /**
     * @return array
     */
    public function getInactiveStatuses()
    {
        // TODO: WHat are the "inactive" statues?
        return [];
    }
}
