<?php
/**
 * @category    Kurufootwear
 * @package     Kurufootwear\Rma
 * @author      Krisjanis Veinbahs <info@scandiweb.com>
 * @copyright   Copyright (c) 2017 KURU Footwear. All rights reserved.
 */

namespace Kurufootwear\Rma\Model\ResourceModel\RequestItem;

use Aheadworks\Rma\Model\ResourceModel\RequestItem\Collection as AwCollection;

class Collection extends AwCollection
{
    protected function _construct()
    {
        $this->_init('Kurufootwear\Rma\Model\RequestItem', 'Kurufootwear\Rma\Model\ResourceModel\RequestItem');
    }
}
