<?php
/**
 * @category    Kurufootwear
 * @package     Kurufootwear\Rma
 * @author      Jim McGowen <jim@kurufootwear.com>
 * @copyright   Copyright (c) 2017 KURU Footwear. All rights reserved.
 */

namespace Kurufootwear\Rma\Model\ResourceModel;

use Magento\Framework\Model\ResourceModel\Db\AbstractDb;
use Magento\Framework\Model\AbstractModel;

/**
 * Class CustomerExchange
 * @package Kurufootwear\Rma\Model\ResourceModel
 */
class CustomerExchange extends AbstractDb
{
    /**
     * Internal Constructor
     */
    protected function _construct()
    {
        $this->_init('kuru_rma_customer_exchange', 'id');
    }
    
    /**
     * @param \Magento\Framework\Model\AbstractModel $object
     *
     * @return $this
     */
    public function _afterLoad(AbstractModel $object)
    {
        $boolVal = $object->getRmaComplete() == '1' ? true : false;
        $object->setRmaComplete($boolVal);
        
        parent::_afterLoad($object);
    }
    
    public function _beforeSave(\Magento\Framework\Model\AbstractModel $object)
    {
        if (is_bool($object->getRmaComplete())) {
            $intVal = $object->getRmaComplete() == true ? '1' : '0';
            $object->setRmaComplete($intVal);
        }
        
        return parent::_beforeSave($object);
    }
}
