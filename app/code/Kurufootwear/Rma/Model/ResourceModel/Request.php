<?php
/**
 * @category    Kurufootwear
 * @package     Kurufootwear\Rma
 * @author      Jim McGowen <jim@kurufootwear.com>
 * @copyright   Copyright (c) 2017 KURU Footwear. All rights reserved.
 */

namespace Kurufootwear\Rma\Model\ResourceModel;

use \Magento\Framework\Model\AbstractModel;
use Aheadworks\Rma\Model\ResourceModel\Request as AheadworksRequest;
use Magento\Framework\Model\ResourceModel\Db\Context;
use Aheadworks\Rma\Model\RequestItemFactory;
use Kurufootwear\Rma\Helper\ShippingLabel;

/**
 * Class Request
 * @package Kurufootwear\Rma\Model\ResourceModel
 */
class Request extends AheadworksRequest
{
    protected $shippingLabel;
    
    public function __construct(
        Context $context, 
        RequestItemFactory $requestItemFactory,
        ShippingLabel $shippingLabel,
        $connectionName = null
    )
    {
        $this->shippingLabel = $shippingLabel;
        parent::__construct($context, $requestItemFactory, $connectionName);
    }
    
    /**
     * Had to override this method from Aheadworks\Rma\Model\ResourceModel\AbstractResource because 
     * of a bug in the original implementation where it would not account for the rma request ID. It 
     * was just returning the first custom field it found for ANY request.
     * 
     * Also enhanced to search by name or identifier.
     * 
     * Overrode the RequestItem class because I can't override AbstractResource :(
     * 
     * @param \Magento\Framework\Model\AbstractModel $object
     * @param $name
     *
     * @return null
     */
    public function getCustomFieldValueByName(AbstractModel $object, $name)
    {
        if ($this->customFieldTableName === null) {
            return null;
        }
    
        $connection = $this->getConnection();
        $mainTable = $this->getTable($this->customFieldTableName);
        $select = $connection->select()
            ->from($mainTable, ['value' => 'value'])
            ->joinLeft(
                ['custom_field_table' => $this->getTable('aw_rma_custom_field')],
                "{$mainTable}.field_id = custom_field_table.id",
                ['name' => 'custom_field_table.name']
            )
            ->where("custom_field_table.name = ? OR custom_field_table.identifier = ?", $name)
            ->where("{$mainTable}.entity_id = ?", $object->getId())
        ;
        
        if ($result = $connection->fetchRow($select)) {
            return $result['value'];
        }
        return null;
    }
}
