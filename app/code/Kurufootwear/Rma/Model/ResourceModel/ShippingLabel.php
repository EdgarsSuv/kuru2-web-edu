<?php
/**
 * @category    Kurufootwear
 * @package     Kurufootwear\Rma
 * @author      Jim McGowen <jim@kurufootwear.com>
 * @copyright   Copyright (c) 2017 KURU Footwear. All rights reserved.
 */

namespace Kurufootwear\Rma\Model\ResourceModel;

use Magento\Framework\Model\ResourceModel\Db\AbstractDb;
use Magento\Framework\Model\AbstractModel;

/**
 * Class ShippingLabel
 * @package Kurufootwear\Rma\Model\ResourceModel
 */
class ShippingLabel extends AbstractDb
{
    /**
     * Internal Constructor
     */
    protected function _construct()
    {
        $this->_init('kuru_rma_shipping_label', 'id');
    }
}
