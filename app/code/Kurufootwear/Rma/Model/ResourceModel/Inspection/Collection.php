<?php
/**
 * @category    Kurufootwear
 * @package     Kurufootwear\Rma
 * @author      Jim McGowen <jim@kurufootwear.com>
 * @copyright   Copyright (c) 2017 KURU Footwear. All rights reserved.
 */

namespace Kurufootwear\Rma\Model\ResourceModel\Inspection;

use Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection;

/**
 * Class Collection
 * @package Kurufootwear\Rma\Model\ResourceModel\Inspection
 */
class Collection extends AbstractCollection
{
    /**
     * Internal Constructor
     */
    protected function _construct()
    {
        $this->_init('Kurufootwear\Rma\Model\Inspection', 'Kurufootwear\Rma\Model\ResourceModel\Inspection');
    }
}
