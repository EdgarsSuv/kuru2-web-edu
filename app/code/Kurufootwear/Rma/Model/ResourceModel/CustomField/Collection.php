<?php
/**
 * @category    Kurufootwear
 * @package     Kurufootwear\Rma
 * @author      Jim McGowen <jim@kurufootwear.com>
 * @copyright   Copyright (c) 2017 KURU Footwear. All rights reserved.
 */

namespace Kurufootwear\Rma\Model\ResourceModel\CustomField;

use Aheadworks\Rma\Model\ResourceModel\CustomField\Collection as RmaCustomFieldCollection;
use Aheadworks\Rma\Model\Source\CustomField\Type;

class Collection extends RmaCustomFieldCollection
{
    /**
     * Overridden to add field type textarea.
     * 
     * @return $this
     */
    public function setFilterForRmaGrid()
    {
        $this->addFieldToFilter('refers', \Aheadworks\Rma\Model\Source\CustomField\Refers::REQUEST_VALUE)
            ->addFieldToFilter('type', ['in' => [Type::TEXT_VALUE, Type::SELECT_VALUE, Type::TEXT_AREA_VALUE]]);
        return $this;
    }
}
