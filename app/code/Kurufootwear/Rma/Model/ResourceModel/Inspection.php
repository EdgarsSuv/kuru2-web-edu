<?php
/**
 * @category    Kurufootwear
 * @package     Kurufootwear\Rma
 * @author      Jim McGowen <jim@kurufootwear.com>
 * @copyright   Copyright (c) 2017 KURU Footwear. All rights reserved.
 */

namespace Kurufootwear\Rma\Model\ResourceModel;

use Magento\Framework\Model\ResourceModel\Db\AbstractDb;
use Magento\Framework\Model\AbstractModel;

/**
 * Class Inspection
 * @package Kurufootwear\Rma\Model\ResourceModel
 */
class Inspection extends AbstractDb
{
    /**
     * Internal Constructor
     */
    protected function _construct()
    {
        $this->_init('kuru_rma_warehouse_inspection', 'id');
    }
    
    /**
     * @param \Magento\Framework\Model\AbstractModel $object
     * 
     * Note: Overriding afterLoad in the Model does not get called 
     * so that is why this is here. M2 bug?
     *
     * @return $this
     */
    public function _afterLoad(AbstractModel $object)
    {
        if (is_string($object->getReceivedItems())) {
            $object->setReceivedItems(unserialize($object->getReceivedItems()));
        }
        
        $bItemsMatch = $object->getItemsMatch() == "1" ? true : false;
        $object->setItemsMatch($bItemsMatch);
        
        parent::_afterLoad($object);
        
        return $this;
    }
}
