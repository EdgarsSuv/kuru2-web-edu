<?php
/**
 * @category     Kurufootwear
 * @package      Kurufootwear/RMA
 * @author       Jim McGowen <jim*kurufootwear.com>
 * @copyright    Copyright (c) 2018 KURU Footwear. All rights reserved.
 */

namespace Kurufootwear\Rma\Model;

use Aheadworks\Rma\Model\Sender as  AWSender;
use Magento\Framework\App\Area;
use Kurufootwear\Mail\Template\TransportBuilder;
use Zend_Mime;

/**
 * Class Sender
 * @package Aheadworks\Rma\Model
 */
class Sender extends AWSender
{
    /**
     * Overridden to optionally add attachment to the email.
     * 
     * @param array $from
     * @param array $to
     * @param array $templateData
     * 
     * @throws \Magento\Framework\Exception\MailException
     */
    public function send(array $from, array $to, array $templateData)
    {
        /** @var TransportBuilder $transportBuilder */
        $transportBuilder = $this->transportBuilder;
        $transportBuilder->setTemplateIdentifier($templateData['template_id']);
        
        $transportBuilder->setTemplateOptions([
                'area' => Area::AREA_FRONTEND, 
                'store' => $templateData['store_id']
            ])
            ->setTemplateVars($this->prepareTemplateVars($templateData))
            ->setFrom($from)
            ->addTo($to);
        
        if (!empty($templateData['attachment'])) {
            $transportBuilder->addAttachment(
                base64_decode($templateData['attachment']), 
                'application/pdf', 
                Zend_Mime::DISPOSITION_ATTACHMENT, 
                Zend_Mime::ENCODING_BASE64,
                'RMA Shipping Label.pdf'
            );
        }
        
        $transportBuilder->getTransport()->sendMessage();
    }
    
    /**
     * Overridden to fix request 'type' var.
     * 
     * @param array $data
     *
     * @return array
     */
    protected function prepareTemplateVars($data)
    {
        $templateVars = [];
        if (isset($data['request_model'])) {
            /** @var \Aheadworks\Rma\Model\Request $requestModel */
            $requestModel = $data['request_model'];
            $templateVars['subject'] = __('Notify about RMA %1', $requestModel->getIncrementId());
            
            $storeId = $requestModel->getStoreId();
            
            $requestData = [
                'text_id' => $requestModel->getIncrementId(),
                'status_name' => $requestModel->getStatusFrontendLabel(),
                'formatted_created_at' => $this->formatDate($requestModel->getCreatedAt()),
                'customer_name' => $requestModel->getCustomerName(),
                'customer_email' => $requestModel->getCustomerEmail(),
                'items' => $requestModel->getItemsCollection(true)->toArray(),
                'url' => $this->dataHelper->getRmaLink($requestModel),
                'admin_url' => $this->dataHelper->getAdminRmaLink($requestModel),
                'notify_rma_address' => $this->scopeConfig->getValue('aw_rma/contacts/department_address'),
                'type' => strtolower($requestModel->getTypeLabel())
            ];
            if (isset($data['message'])) {
                $requestData['notify_comment_text'] = $data['message'];
            }
            if (isset($data['order'])) {
                $order = $data['order'];
                $requestData['order_id'] = $order->getIncrementId();
                $requestData['notify_order_admin_link'] = $this->dataHelper->getAdminOrderLink($order);
            }
            $templateVars['request'] = new \Magento\Framework\DataObject($requestData);
            $templateVars['store'] = $this->storeManager->getStore($storeId);
        }
        return $templateVars;
    }
}
