<?php
namespace Kurufootwear\Rma\Model\Config;

class RmaShippingCarrier implements \Magento\Framework\Option\ArrayInterface
{
    const CARRIER_CONFIG_PATH = 'kurufootwear_rma/shipping/carrier';
    
    /**
     * @return array
     */
    public function toOptionArray()
    {
        return [
            ['value' => 'fedex', 'label' => __('Fedex')],
            ['value' => 'ups', 'label' => __('UPS')]
        ];
    }
}
