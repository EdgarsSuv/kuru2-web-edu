<?php
/**
 * @category    Kurufootwear
 * @package     Kurufootwear\Rma
 * @author      Jim McGowen <jim@kurufootwear.com>
 * @copyright   Copyright (c) 2018 KURU Footwear. All rights reserved.
 */

namespace Kurufootwear\Rma\Model;

use Aheadworks\Rma\Model\Request as AwRequest;
use Magento\Framework\Exception\IntegrationException;
use Magento\Framework\Exception\NoSuchEntityException;
use \Magento\Framework\Model\Context;
use Magento\Framework\Registry;
use Aheadworks\Rma\Model\ResourceModel\RequestItem\CollectionFactory as RequestItemCollectionFactory;
use Aheadworks\Rma\Model\ResourceModel\ThreadMessage\CollectionFactory as ThreadMessageCollectionFactory;
use Magento\Sales\Api\OrderItemRepositoryInterface;
use Magento\Sales\Api\OrderRepositoryInterface;
use Magento\Sales\Model\OrderFactory;
use Magento\Customer\Model\CustomerFactory;
use Aheadworks\Rma\Model\CustomFieldFactory;
use Magento\Framework\Model\ResourceModel\AbstractResource;
use Magento\Framework\Data\Collection\AbstractDb;
use Magento\Catalog\Api\ProductRepositoryInterface;
use \Mirasvit\Rewards\Helper\Balance as RewardsBalanceHelper;
use \Magento\Backend\Model\UrlInterface;
use Kurufootwear\Rma\Helper\ShippingLabel as ShippingLabelHelper;

/**
 * Class Request
 * @package Kurufootwear\Rma\Model
 */
class Request extends AwRequest
{
    /** @var InspectionFactory */
    protected $inspectionFactory;
    
    /** @var Inspection */
    protected $inspection = null;
    
    /** @var CustomerExchangeFactory */
    protected $customerExchangeFactory;
    
    /** @var CustomerExchange */
    protected $customerExchange = null;
    
    /** @var CustomFieldFactory */
    protected $customFieldFactory;
    
    /** @var ProductRepositoryInterface */
    protected $productRepository;
    
    /** @var OrderRepositoryInterface */
    protected $orderItemRepository;
    
    /** @var RewardsBalanceHelper */
    protected $rewardsBalanceHelper;
    
    /** @var UrlInterface */
    protected $urlInterface;
    
    /** @var ShippingLabelHelper */
    protected $shippingLabelHelper;
    
    /**
     * Request constructor.
     *
     * @param Context $context
     * @param Registry $registry
     * @param RequestItemCollectionFactory $itemsCollectionFactory
     * @param ThreadMessageCollectionFactory $threadMessageCollectionFactory
     * @param OrderFactory $orderFactory
     * @param OrderItemRepositoryInterface $orderItemRepository
     * @param CustomerFactory $customerFactory
     * @param InspectionFactory $inspectionFactory
     * @param CustomerExchangeFactory $customerExchangeFactory
     * @param CustomFieldFactory $customFieldFactory
     * @param ProductRepositoryInterface $productRepository
     * @param RewardsBalanceHelper $rewardsBalanceHelper
     * @param AbstractResource|null $resource
     * @param AbstractDb|null $resourceCollection
     * @param UrlInterface $urlInterface
     * @param array $data
     */
    public function __construct(
        Context $context,
        Registry $registry,
        RequestItemCollectionFactory $itemsCollectionFactory,
        ThreadMessageCollectionFactory $threadMessageCollectionFactory,
        OrderFactory $orderFactory,
        OrderItemRepositoryInterface $orderItemRepository,
        CustomerFactory $customerFactory,
        InspectionFactory $inspectionFactory,
        CustomerExchangeFactory $customerExchangeFactory,
        CustomFieldFactory $customFieldFactory,
        ProductRepositoryInterface $productRepository,
        RewardsBalanceHelper $rewardsBalanceHelper,
        UrlInterface $urlInterface,
        AbstractResource $resource = null,
        AbstractDb $resourceCollection = null,
        array $data = []
    ) {
        $this->inspectionFactory = $inspectionFactory;
        $this->customerExchangeFactory = $customerExchangeFactory;
        $this->customFieldFactory = $customFieldFactory;
        $this->productRepository = $productRepository;
        $this->orderItemRepository = $orderItemRepository;
        $this->rewardsBalanceHelper = $rewardsBalanceHelper;
        $this->urlInterface = $urlInterface;
        
        parent::__construct($context, $registry, $itemsCollectionFactory, $threadMessageCollectionFactory, 
            $orderFactory, $customerFactory, $resource, $resourceCollection, $data);
    }
    
    /**
     * Returns the Inspection associated with this request or null if there isn't one.
     * 
     * @param bool $forceReload
     *
     * @return Inspection
     */
    public function getInspection($forceReload = false)
    {
        if (empty($this->inspection) || $forceReload) {
            $inspection = $this->inspectionFactory->create()->loadByRequestId($this->getId());
            if (!empty($inspection->getId())) {
                $this->inspection = $inspection;
            }
        }
        return $this->inspection;
    }
    
    /**
     * @return $this|CustomerExchange
     */
    public function getCustomerExchange()
    {
        if (empty($this->customerExchange)) {
            $customerExchange = $this->customerExchangeFactory->create()->loadByRequestId($this->getId());
            if (!empty($customerExchange->getId())) {
                $this->customerExchange = $customerExchange;
            }
        }
        return $this->customerExchange;
    }
    
    /**
     * @param $customField int|\Aheadworks\Rma\Model\CustomField The CustomField or it's ID.
     * @param $value int|string|array The ID or the value to set the custom field to. For multiselects this will be an array of IDs or values.
     * @throws \Exception
     */
    public function setCustomFieldValue($customField, $value)
    {
        if (is_numeric($customField)) {
            $id = $customField;
            $customField = $this->customFieldFactory->create()->load($id);
        }
        else {
            $id = $customField->getId();
        }
        
        $customFields = $this->getCustomFields();
        
        if (empty($customField->getId())) {
            throw new \Exception(__("Custom field with ID $id does not exist."));
        }
        
        switch ($customField->getType()) {
            case 'select':
            case 'radio': {
                $options = $customField->toOptionArray();
                foreach ($options as $option) {
                    if (is_int($value)) {
                        $customFields[$id] = $value;
                        break;
                    }
                    else {
                        if ($value == $option['label']) {
                            $customFields[$id] = $option['value'];
                            break;
                        }
                    }
                }
                break;
            }
            case 'multiselect': {
                $customFields[$id] = array();
                $options = $customField->toOptionArray();
                foreach ($value as $val) {
                    if (is_int($val)) {
                        $customFields[$id][] = $val;
                        break;
                    }
                    else {
                        foreach ($options as $option) {
                            if ($val == $option['label']) {
                                $customFields[$id][] = $option['value'];
                                break;
                            }
                        }
                    }
                }
                break;
            }
            case 'text':
            case 'textarea': {
                $customFields[$customField->getId()] = $value;
                break;
            }
        }
        
        $this->setData('custom_fields', $customFields);
    }
    
    /**
     * @param $name string The name or identifier of a CustomField
     * @param $value int|string|array The ID or the value to set the custom field to. For multiselects this will be an array of IDs or values.
     */
    public function setCustomFieldValueByName($name, $value)
    {
        $customField = $this->getCustomFieldByName($name);
        $this->setCustomFieldValue($customField, $value);
    }
    
    /**
     * @param $name string Custom field name or identifier
     *
     * @return \Aheadworks\Rma\Model\CustomField
     * @throws \Exception
     */
    public function getCustomFieldByName($name) 
    {
        $customField = $this->customFieldFactory->create()->loadByName($name);
        if (is_null($customField->getId())) {
            // Try by identifier
            $customField = $this->customFieldFactory->create()->load($name, 'identifier');
            if (empty($customField->getId())) {
                throw new \Exception(__("Custom field '$name' does not exist."));
            }
        }
        return $customField;
    }
    
    /**
     * @return string
     */
    public function getTypeLabel() 
    {
        $customField = $this->getCustomFieldByName('type');
        $value = $this->getCustomFieldValue('type');
        return $customField->getOptionLabelByValue($value);
    }
    
    /**
     * @return int Returns one of CustomField::TYPE_*
     */
    public function getTypeId() 
    {
        $id = $this->getCustomFieldByName('type')->getId();
        $customFields = $this->getCustomFields();
    
        // Sanity check for migrated RMAs that don't have the custom fields set
        if (!isset($customFields[$id])) {
            return null;
        }
        
        return $customFields[$id];
    }
    
    /**
     * @param $typeId int One of RequestManager::REQUEST_TYPE_*
     * 
     * @return $this
     */
    public function setType($typeId)
    {
        $this->setCustomFieldValueByName('type', $typeId);
        return $this;
    }
    
    /**
     * @return string
     */
    public function getAdminNotes()
    {
        return $this->getResource()->getCustomFieldValueByName($this, 'admin_notes');
    }
    
    /**
     * @param $value
     *
     * @return $this
     */
    public function setAdminNotes($value)
    {
        $this->setCustomFieldValueByName('admin_notes', $value);
        return $this;
    }
    
    /**
     * Issues exchange reward points for this RMA. Creates or updates the
     * CustomerExchange. Throws an exception if the customer has already been issued points.
     * 
     * @param $createCustomerExchange bool
     * @return int The number of points issued.
     * @throws IntegrationException
     */
    public function issueExchangePoints($createCustomerExchange = true) 
    {
        $points = $this->calcExchangePoints();
        if ($points == 0) {
            throw new IntegrationException(__('0 points calculated for RMA.'));
        }
        
        // Make the rewards points transaction
        $result = $this->rewardsBalanceHelper->changePointsBalance(
            $this->getCustomer()->getId(),
            $points,
            'Points issued for exchange RMA ' . $this->getIncrementId() . '.'
        );
        if ($result === false) {
            throw new IntegrationException(__('Could not issue exchange points'));
        }
        
        if ($createCustomerExchange) {
            // Create a CustomerExchange record
            $customerExchange = $this->customerExchangeFactory->create()->loadByRequestId($this->getId());
            $customerExchange->setRequestId($this->getId());
            $customerExchange->setCustomerId($this->getCustomer()->getId());
            $customerExchange->setPointsIssued($points);
            $customerExchange->setRmaComplete(false);
            $customerExchange->save();
    
            $this->customerExchange = $customerExchange;
        }
        
        return $points;
    }
    
    /**
     * Updates the existing CustomerExchange record with the pending order ID.
     * 
     * @param $orderId
     *
     * @throws NoSuchEntityException
     */
    public function updateExchangeOrder($orderId) 
    {
        $customerExchange = $this->getCustomerExchange();
        if (is_null($customerExchange)) {
            throw new NoSuchEntityException(__('CustomerExchange record does not exist.'));
        }
        
        $customerExchange->setPendingOrderId($orderId);
        $customerExchange->save();
    }
    
    /**
     * @return string
     */
    public function getAdminUrl() 
    {
        if($this->getId()) {
            return $this->urlInterface->getUrl('aw_rma_admin/rma/edit', array('id' => $this->getId()));
        }
        
        return '';
    }
    
    /**
     * Returns the reward points for this RMA based on the items.
     * Typically the customer will keep the points they earned from the original purchase
     * because when they make a new purchase the total will be $0 so they wont earn any 
     * more points.
     * 
     * @return int
     */
    protected function calcExchangePoints() 
    {
        $points = 0;
        
        if ($this->getTypeId() == RequestManager::REQUEST_TYPE_EXCHANGE) {
            
            // If we have an inspection the points will be based on the received items
            // otherwise it will be based on the request items.
            $inspection = $this->getInspection();
            $receivedItems = null;
            if (!empty($inspection)) {
                $receivedItems = $inspection->getReceivedItems();
            }
            
            foreach ($this->getItemsCollection() as $requestItem) {
                if ($receivedItems === null || array_key_exists($requestItem->getItemId(), $receivedItems)) {
                    if (!empty($requestItem->getParentProductId())) {
                        $product = $this->productRepository->getById($requestItem->getParentProductId());
                    }
                    else {
                        $product = $this->productRepository->getById($requestItem->getProductId());
                    }
                    
                    // Use the higher of the current price or the price paid to calculate points adjustment
                    if ($product->getPrice() > $requestItem->getPrice()) {
                        $points += $product->getPrice() * $requestItem->getQty() * 100;
                    }
                    else {
                        $points += $requestItem->getPrice() * $requestItem->getQty() * 100;
                    }
                }
            }
        }
        
        return $points;
    }
}
