<?php
/**
 * @category    Kurufootwear
 * @package     Kurufootwear\Rma
 * @author      Krisjanis Veinbahs <info@scandiweb.com>
 * @copyright   Copyright (c) 2017 KURU Footwear. All rights reserved.
 */

namespace Kurufootwear\Rma\Model;

use Magento\Customer\Model\Session;

/**
 * A pseudo model
 * for storing survey data
 * about items and requests
 * and order info
 */
class Survey
{
    /**
     * @var SurveyAction
     */
    protected $surveyAction;

    /**
     * @var SurveyItem
     */
    protected $surveyItem;

    /**
     * @var Session
     */
    protected $customerSession;

    /**
     * Survey data array
     * @var array(SurveyStep)
     */
    private $data;

    /**
     * Survey data fields
     */
    const ITEMS = 'items';
    const ORDER = 'order';
    const CURRENT_ACTION_FULL_ID = 'current_action_full_id';
    const IS_EXCHANGE = 'is_exchange';
    const IS_FINISHED = 'is_finished';
    const REQUEST_ID = 'request_id';

    /**
     * Survey constructor.
     * @param Session $customerSession
     * @param SurveyAction $surveyAction
     * @param SurveyItem $surveyItem
     * @param array $data
     */
    public function __construct(
        Session $customerSession,
        SurveyAction $surveyAction,
        SurveyItem $surveyItem,
        array $data = []
    )
    {
        $this->surveyAction = $surveyAction;
        $this->surveyItem = $surveyItem;
        $this->customerSession = $customerSession;
        $this->data = $data;
    }

    /**
     * Data (de/en)coding, sanitization
     * @param $data
     * @return string
     */
    private function encode($data)
    {
        return json_encode($data, JSON_HEX_QUOT|JSON_HEX_TAG|JSON_HEX_AMP|JSON_HEX_APOS);
    }

    /**
     * @param $data
     * @return mixed
     */
    private function decode($data)
    {
        return json_decode($data, true);
    }

    /**
     * Survey CRUD functions
     */
    public function save()
    {
        $encodedSurvey = $this->encode($this->data);
        $this->customerSession->setRmaSurvey($encodedSurvey);

        return $this;
    }

    /**
     * @return $this
     */
    public function load()
    {
        $encodedSurvey = $this->customerSession->getRmaSurvey();
        $this->data = $this->decode($encodedSurvey);

        return $this;
    }

    /**
     * @return $this
     */
    public function clean()
    {
        $this->data = [];

        return $this;
    }

    /**
     * @param $survey
     * @return $this
     */
    public function update($survey)
    {
        foreach ($survey as $key => $value) {
            $this->setData($key, $value);
        }
        $this->data = $survey;

        return $this;
    }

    /**
     * @return array
     */
    public function get()
    {
        return $this->data;
    }

    /**
     * @param $surveyAction
     * @return $this
     */
    public function set($surveyAction)
    {
        $this->data = $surveyAction;

        return $this;
    }

    /**
     * Survey step field CRUD functions
     * @param $key
     * @param $value
     * @return $this
     */
    public function setData($key, $value)
    {
        $this->data[$key] = $value;

        return $this;
    }

    /**
     * @param $key
     * @return mixed
     */
    public function getData($key)
    {
        return isset($this->data[$key]) ?
            $this->data[$key] :
            null;
    }

    /**
     * @param $order
     * @return Survey
     */
    public function setOrder($order)
    {
        return $this->setData(self::ORDER, $order);
    }

    /**
     * @return mixed
     */
    public function getOrder()
    {
        return $this->getData(self::ORDER);
    }

    /**
     * @return mixed
     */
    public function getOrderItems()
    {
        return $this->getOrder()['item'];
    }

    /**
     * @param $action
     * @return Survey
     */
    public function setCurrentAction($action)
    {
        if (is_array($action)) {
            $fullId = $this->surveyAction->getActionFullId($action);
        } else {
            $fullId = $action->getActionFullId();
        }
        $this->setAction($action);

        return $this->setCurrentActionFullId($fullId);
    }


    /**
     * @return mixed|null
     */
    public function getCurrentAction()
    {
        $actionId = $this->getCurrentActionFullId();

        if ($actionId == null) {
            return null;
        } else {
            return $this->getAction($actionId);
        }
    }

    /**
     * @param $actionFullId
     * @return Survey
     */
    public function setCurrentActionFullId($actionFullId)
    {
        return $this->setData(self::CURRENT_ACTION_FULL_ID, $actionFullId);
    }

    /**
     * @return mixed
     */
    public function getCurrentActionFullId()
    {
        return $this->getData(self::CURRENT_ACTION_FULL_ID);
    }

    /**
     * @param $isExchange
     * @return Survey
     */
    public function setIsExchange($isExchange)
    {
        return $this->setData(self::IS_EXCHANGE, $isExchange);
    }

    /**
     * @return mixed
     */
    public function getIsExchange()
    {
        return $this->getData(self::IS_EXCHANGE);
    }

    /**
     * @param $isFinished
     * @return Survey
     */
    public function setIsFinished($isFinished)
    {
        return $this->setData(self::IS_FINISHED, $isFinished);
    }

    /**
     * @return mixed
     */
    public function getIsFinished()
    {
        return $this->getData(self::IS_FINISHED);
    }


    /**
     * @param $requestId
     * @return Survey
     */
    public function setRequestId($requestId)
    {
        return $this->setData(self::REQUEST_ID, $requestId);
    }

    /**
     * @return mixed
     */
    public function getRequestId()
    {
        return $this->getData(self::REQUEST_ID);
    }

    /**
     * Return index of item in survey by its id
     * @param $itemId
     * @return mixed
     */
    public function generateOrderIndexedId($itemId)
    {
        $itemsKeys = array_keys($this->getItems());

        return array_search($itemId, $itemsKeys);
    }

    /**
     * Return id of item by its index in survey
     * @param $itemIndex
     * @return mixed
     */
    public function generateOrderId($itemIndex)
    {
        $itemsKeys = array_keys($this->getItems());

        if ($itemIndex < count($itemsKeys)) {
            return $itemsKeys[$itemIndex];
        } else {
            return null;
        }
    }

    /**
     * Retrieve action before the current one
     * or before the one in parameters
     * @return mixed|null
     */
    public function loadPreviousAction($action = null)
    {
        $previousAction = null;

        if (!is_null($action)) {
            $currentAction = $action;
        } else {
            $currentAction = $this->getCurrentAction();
        }

        if ($currentAction !== null) {
            $item = $this->getItem($currentAction[SurveyAction::ACTION_ITEM_FULL_ID]);
            $actionId = $currentAction[SurveyAction::ACTION_ID];

            if ($actionId == 0) {
                // First action in item
                // Load previous item
                $itemId = $currentAction[SurveyAction::ACTION_ITEM_FULL_ID];
                $itemIndex = $this->generateOrderIndexedId($itemId);
                if ($itemIndex != 0) {
                    // Not first item in survey
                    // Keep loading previous item
                    $previousItemId = $this->generateOrderId($itemIndex - 1);
                    $previousItem = $this->getItem($previousItemId);
                    $previousAction = end($previousItem[SurveyItem::ACTIONS]);
                }
            } else {
                $previousAction = $item[SurveyItem::ACTIONS][$actionId - 1];
            }
        }

        return $previousAction;
    }

    /**
     * @return mixed
     */
    public function loadNextAction()
    {
        $nextAction = null;
        $currentAction = $this->getCurrentAction();

        if ($currentAction !== null) {
            $item = $this->getItem($currentAction[SurveyAction::ACTION_ITEM_FULL_ID]);
            $actionId = $currentAction[SurveyAction::ACTION_ID];

            if ($this->isLastAction($item, $actionId)) {
                if (!$this->isLastItem()) {
                    $itemId = $currentAction[SurveyAction::ACTION_ITEM_FULL_ID];
                    $itemIndex = $this->generateOrderIndexedId($itemId);
                    $nextItemId = $this->generateOrderId($itemIndex + 1);
                    $nextItem = $this->getItem($nextItemId);
                    $nextActions = $nextItem[SurveyItem::ACTIONS];

                    if (count($nextActions) != 0) {
                        $nextAction = $nextActions[0];
                    }
                }
            } else {
                $nextAction = $item[SurveyItem::ACTIONS][$actionId + 1];
            }
        }

        return $nextAction;
    }

    /**
     * @return mixed
     */
    public function nextItemFullId()
    {
        $nextItem = null;
        $currentAction = $this->getCurrentAction();
        $itemId = $currentAction[SurveyAction::ACTION_ITEM_FULL_ID];
        $itemIndex = $this->generateOrderIndexedId($itemId);
        $nextId = $this->generateOrderId($itemIndex + 1);

        return $nextId;
    }

    /**
     * @return bool
     */
    public function isLastItem()
    {
        $items = $this->getItems();
        $currentAction = $this->getCurrentAction();
        $itemId = $currentAction[SurveyAction::ACTION_ITEM_FULL_ID];
        $lastItemIndex = count($items) - 1;
        $lastItemId = $this->generateOrderId($lastItemIndex);
        return $itemId == $lastItemId;
    }

    /**
     * @param null $item
     * @return bool
     */
    public function isLastAction($item = null, $actionId = null)
    {
        if ($item == null) {
            $currentAction = $this->getCurrentAction();
            $actionId = $currentAction[SurveyAction::ACTION_ID];
            $itemId = $currentAction[SurveyAction::ACTION_ITEM_FULL_ID];
            $item = $this->getItem($itemId);
        }

        $lastActionId = end($item[SurveyItem::ACTIONS])[SurveyAction::ACTION_ID];

        return $actionId == $lastActionId;
    }

    /**
     * @param $items
     * @return Survey
     */
    public function setItems($items)
    {
        return $this->setData(self::ITEMS, $items);
    }

    /**
     * @return mixed
     */
    public function getItems()
    {
        return $this->getData(self::ITEMS);
    }

    /**
     * @param $itemFullId
     * @return mixed
     */
    public function getItem($itemFullId)
    {
        $items = $this->getItems();
        $keys = array_keys($items);
        if (in_array($itemFullId, $keys)) {
            return $this->getItems()[$itemFullId];
        } else {
            return null;
        }
    }

    /**
     * @return mixed
     */
    public function getFirstItem()
    {
        $firstId = $this->generateOrderId(0);
        $firstItem = $this->getItems()[$firstId];

        return $firstItem;
    }

    /**
     * @param $item
     * @return Survey
     */
    public function setItem($item)
    {
        $items = $this->getItems();
        $items[$this->surveyItem->getItemFullId($item)] = $item;
        return $this->setData(self::ITEMS, $items);
    }

    public function removeItem($itemId)
    {
        $items = $this->getItems();
        unset($items[$itemId]);

        return $this->setData(self::ITEMS, $items);
    }

    /**
     * @param $newAction
     * @return Survey
     */
    public function setAction($newAction)
    {
        if (!is_array($newAction)) {
            $newAction = $newAction->get();
        }
        if (!($item = $this->getItem($newAction[SurveyAction::ACTION_ITEM_FULL_ID]))) {
            // No item, no action
            // Create item, add action, add item
            $item = $this->surveyItem->addAction($newAction)->get();
            $this->setItem($item);
        } else {
            // Yes item, no action OR
            // Yes item, yes action
            // Set action, set action, set item
            $item[SurveyItem::ACTIONS][$newAction[SurveyAction::ACTION_ID]] = $newAction;
            $this->setItem($item);
        }

        return $this->save();
    }

    /**
     * @param $actionFullId
     * @return mixed
     */
    public function getAction($actionFullId)
    {
        $ids = $this->surveyAction->generateActionIds($actionFullId);
        $item = $this->getItem($ids[1] . '_' . $ids[2]);

        return $item[SurveyItem::ACTIONS][$ids[3]];
    }

    /**
     * Format order items data array
     * @return Survey
     */
    public function populateItems()
    {
        $orderItems = $this->getOrderItems();
        $items = [];
        foreach ($orderItems as $itemId => $qty) {
            /**
             * A for loop can be inserted here in the future
             * to create separate items for items with quantities
             */
            $itemFullId = $itemId . '_0';
            $items[$itemFullId] = array(
                SurveyItem::ACTIONS => [],
                SurveyItem::ITEM_ID => $itemId,
                SurveyItem::ITEM_QUANTITY_ID => 0
            );
        }

        return $this->setItems($items);
    }

    /**
     * @return string
     */
    public function getCurrentItemFullId() {
        $fullId = $this->getCurrentActionFullId();
        $ids = $this->surveyAction->generateActionIds($fullId);

        return $ids[1] . '_' . $ids[2];
    }
}
