<?php
/**
 * @category    Kurufootwear
 * @package     Kurufootwear\Rma
 * @author      Jim McGowen <jim@kurufootwear.com>
 * @copyright   Copyright (c) 2018 KURU Footwear. All rights reserved.
 */

namespace Kurufootwear\Rma\Model;

use Aheadworks\Rma\Model\Request;
use Aheadworks\Rma\Model\RequestFactory;
use Aheadworks\Rma\Model\RequestManager as AheadworksRequestManager;
use Aheadworks\Rma\Model\Sender;
use Aheadworks\Rma\Model\StatusFactory;
use Aheadworks\Rma\Model\ThreadMessageFactory;
use Kurufootwear\Rma\Model\Source\Request\Status;
use Kurufootwear\Rma\Helper\ShippingLabel as ShippingLabelHelper;
use Magento\Framework\Exception\LocalizedException;
use Magento\Store\Model\ScopeInterface as StoreScope;
use Magento\Framework\App\Config\ScopeConfigInterface;
use Potato\Zendesk\Api\Data\AttachmentInterfaceFactory;
use Magento\Framework\Registry;
use Magento\Sales\Model\OrderFactory;
use Magento\Customer\Model\Session;
use Aheadworks\Rma\Helper\Order;
use Aheadworks\Rma\Helper\Status as AWHelperStatus;
use Aheadworks\Rma\Model\PrintLabel\Pdf;
use Magento\Email\Model\TemplateFactory;
use Magento\Email\Model\ResourceModel\Template\CollectionFactory as MailTemplateCollectionFactory;

/**
 * Class RequestManager
 * @package Aheadworks\Rma\Model
 */
class RequestManager extends AheadworksRequestManager
{
    // TODO: Better way to get these values?
    const REQUEST_TYPE_EXCHANGE = 18;
    const REQUEST_TYPE_RETURN = 19;
    
    // TODO: Make these configurable in the backend?
    const TEMPLATE_CODE_RETURN = 'RMA New Return';
    const TEMPLATE_CODE_EXCHANGE = 'RMA New Exchange';
    
    /** @var ShippingLabelHelper  */
    protected $shippingLabelHelper;
    
    /** @var AttachmentInterfaceFactory  */
    protected $attachmentInterfaceFactory;
    
    /** @var TemplateFactory  */
    protected $templateFactory;
    
    /** @var MailTemplateCollectionFactory  */
    protected $templateCollectionFactory;
    
    /**
     * RequestManager constructor.
     *
     * @param RequestFactory $requestFactory
     * @param StatusFactory $statusFactory
     * @param ThreadMessageFactory $threadMessageFactory
     * @param Sender $sender
     * @param ScopeConfigInterface $scopeConfig
     * @param Registry $registry
     * @param OrderFactory $orderFactory
     * @param Session $customerSession
     * @param Order $orderHelper
     * @param AWHelperStatus $statusHelper
     * @param Pdf $printLabelPdf
     * @param ShippingLabelHelper $shippingLabelHelper
     * @param AttachmentInterfaceFactory $attachmentInterfaceFactory
     * @param TemplateFactory $templateFactory
     * @param MailTemplateCollectionFactory $templateCollectionFactory
     */
    public function __construct(
        RequestFactory $requestFactory,
        StatusFactory $statusFactory,
        ThreadMessageFactory $threadMessageFactory,
        Sender $sender,
        ScopeConfigInterface $scopeConfig,
        Registry $registry,
        OrderFactory $orderFactory,
        Session $customerSession,
        Order $orderHelper,
        AWHelperStatus $statusHelper,
        Pdf $printLabelPdf,
        ShippingLabelHelper $shippingLabelHelper,
        AttachmentInterfaceFactory $attachmentInterfaceFactory,
        TemplateFactory $templateFactory,
        MailTemplateCollectionFactory $templateCollectionFactory
    ) {
        $this->shippingLabelHelper = $shippingLabelHelper;
        $this->attachmentInterfaceFactory = $attachmentInterfaceFactory;
        $this->templateFactory = $templateFactory;
        $this->templateCollectionFactory = $templateCollectionFactory;
        
        parent::__construct($requestFactory, $statusFactory, $threadMessageFactory, $sender, $scopeConfig, $registry, $orderFactory, $customerSession, $orderHelper, $statusHelper, $printLabelPdf);
    }
    
    /**
     * Overridden to set the initial status of RMAs to Approved
     * and to send email on new RMA.
     *
     * @param array $data
     * @param bool $guestMode
     *
     * @return \Aheadworks\Rma\Model\Request
     * @throws \Exception
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function create($data, $guestMode = false)
    {
        $requestModel = parent::create($data, $guestMode);
        $requestModel->setStatusId(Status::APPROVED); // Approved
        $requestModel->save();
        
        return $requestModel;
    }
    
    /**
     * Sends email to customer for a new RMA. The mail template 
     * is based on the RMA type.
     * 
     * @param $request
     *
     * @throws LocalizedException
     * @throws \Kurufootwear\Rma\Exception\ShippingLabelException
     */
    public function sendNewRequestEmail($request)
    {
        $requestModel = $this->getRequestModel($request);
        
        // The email template depends on the RMA type
        $templateCode = null;
        if ($requestModel->getTypeId() == self::REQUEST_TYPE_RETURN) {
            $templateCode = self::TEMPLATE_CODE_RETURN;
        }
        else {
            $templateCode = self::TEMPLATE_CODE_EXCHANGE;
        }
        $template = $this->templateCollectionFactory->create()
            ->addFieldToFilter('template_code', $templateCode)
            ->getFirstItem();
        if (!$template->hasData()) {
            throw new LocalizedException(__('Missing email template "' . $templateCode . '"'));
        }
    
        $this->send($requestModel, $template->getId(), false);
    }
    
    /**
     * Overridden to add shipping label to email as an attachment.
     * 
     * @param Request $requestModel
     * @param null $template
     * @param bool $toAdmin
     * @param array $templateData
     * 
     * @throws \Kurufootwear\Rma\Exception\ShippingLabelException
     */
    private function send(Request $requestModel, $template = null, $toAdmin = true, $templateData = [])
    {
        $emailConfigData = $this->getEmailConfigData($requestModel->getStoreId());
        $to = $toAdmin ?
            [$emailConfigData['department']['name'] => $emailConfigData['department']['email']] :
            [$requestModel->getCustomerName() => $requestModel->getCustomerEmail()]
        ;
        if (!$template) {
            $template = $toAdmin ?
                $emailConfigData['template']['to_admin'] :
                $emailConfigData['template']['to_customer']
            ;
        }
    
        $attachment = null;
        $shippingLabel = $this->shippingLabelHelper->getLabel($requestModel);
        $labelPdf = null;
        if (!empty($shippingLabel['label_pdf'])) {
            $labelPdf = $shippingLabel['label_pdf'];
        }
        
        $this->sender->send(
            $emailConfigData['department'],
            $to,
            array_merge(
                $templateData,
                [
                    'store_id' => $requestModel->getStoreId(),
                    'order' => $this->orderFactory->create()->load($requestModel->getOrderId()),
                    'request_model' => $requestModel,
                    'template_id' => $template,
                    'attachment' => $labelPdf
                ]
            )
        );
    }
    
    /**
     * Copied from parent because it's private and used by the send function above :(
     * No changes from the original were made.
     * 
     * @param int|null $storeId
     * @return array
     */
    private function getEmailConfigData($storeId = null)
    {
        // todo: move to config helper
        $departmentName = $this->scopeConfig->getValue(
            self::XML_PATH_DEPARTMENT_NAME,
            StoreScope::SCOPE_STORE, $storeId
        );
        $departmentEmail = $this->scopeConfig->getValue(
            self::XML_PATH_DEPARTMENT_EMAIL,
            StoreScope::SCOPE_STORE, $storeId
        );
        if (!$departmentEmail) {
            $departmentEmail = $this->scopeConfig->getValue(
                'trans_email/ident_general/email',
                StoreScope::SCOPE_STORE, $storeId
            );
        }
        $templateToCustomer = $this->scopeConfig->getValue(
            self::XML_PATH_TEMPLATE_TO_CUSTOMER,
            StoreScope::SCOPE_STORE, $storeId
        );
        $templateToAdmin = $this->scopeConfig->getValue(
            self::XML_PATH_TEMPLATE_TO_ADMIN,
            StoreScope::SCOPE_STORE, $storeId
        );
        return [
            'department' => ['name' => $departmentName, 'email' => $departmentEmail],
            'template' => ['to_customer' => $templateToCustomer, 'to_admin' => $templateToAdmin]
        ];
    }
}
