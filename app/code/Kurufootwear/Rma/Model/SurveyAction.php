<?php
/**
 * @category    Kurufootwear
 * @package     Kurufootwear\Rma
 * @author      Krisjanis Veinbahs <info@scandiweb.com>
 * @copyright   Copyright (c) 2017 KURU Footwear. All rights reserved.
 */

namespace Kurufootwear\Rma\Model;

/**
 * A pseudo model
 * for storing survey actions
 */
class SurveyAction
{
    /**
     * Survey action data array
     *
     * @var array
     */
    private $data;

    /**
     * Survey action fields
     */
    const TYPE = 'type';
    const TYPE_REDIRECT = 'redirect';
    const REDIRECT_URL = 'redirect_url';
    const REDIRECT_PATH = 'redirect_path';
    const TYPE_QUESTION = 'question';
    const QUESTION_IDENTIFIER = 'question_identifier';
    const QUESTION_TYPE = 'question_type';
    const QUESTION_TITLE = 'question_title';
    const QUESTION_REQUIRED = 'question_required';
    const QUESTION_CHOICES = 'question_choices';
    const QUESTION_ANSWER = 'question_answer';
    const CLASS_NAME = 'survey_action'; // For use in requests
    const ACTION_ITEM_FULL_ID = 'action_item_full_id'; // For storing relation between requests
    const ACTION_ITEM_TITLE = 'action_item_title';
    const ACTION_ITEM_THUMBNAIL = 'action_item_thumbnail';
    const ACTION_ITEM_COLOR = 'action_item_color';
    const ACTION_ITEM_SIZE = 'action_item_size';
    const ACTION_REQUEST_ID = 'action_request_id';
    const ACTION_REQUEST_FAIL = 'action_request_fail';
    const ACTION_ID = 'action_id'; // For storing relation between requests
    const REQUESTED_PREVIOUS = 'requested_previous'; // Previous question request flag
    const REQUESTED_NEXT = 'requested_next'; // Next question request flag
    const PREVIOUS_AVAILABLE = 'previous_available'; // Previous available flag
    const NEXT_AVAILABLE = 'next_available'; // Next available flag

    /**
     * SurveyAction constructor.
     * @param array $data
     */
    public function __construct(
        array $data = []
    )
    {
        $this->data = $data;
    }

    /**
     * Survey action CRUD functions
     * @param $surveyAction
     * @return $this
     */
    public function update($surveyAction)
    {
        foreach ($surveyAction as $key => $value) {
            $this->setData($key, $value);
        }
        $this->data = $surveyAction;

        return $this;
    }

    /**
     * @return array
     */
    public function get()
    {
        return $this->data;
    }

    /**
     * @param $surveyAction
     * @return $this
     */
    public function set($surveyAction)
    {
        $this->data = $surveyAction;

        return $this;
    }

    /**
     * Survey action field CRUD functions
     * @param $key
     * @param $value
     * @return $this
     */
    public function setData($key, $value)
    {
        $this->data[$key] = $value;

        return $this;
    }

    /**
     * @param $key
     * @return mixed
     */
    public function getData($key)
    {
        return $this->data[$key];
    }

    /**
     * @param $type
     * @return SurveyAction
     */
    public function setType($type)
    {
        return $this->setData(self::TYPE, $type);
    }

    /**
     * @return mixed
     */
    public function getType()
    {
        return $this->getData(self::TYPE);
    }

    /**
     * @param $actionItemFullId
     * @return SurveyAction
     */
    public function setActionItemFullId($actionItemFullId)
    {
        return $this->setData(self::ACTION_ITEM_FULL_ID, $actionItemFullId);
    }

    /**
     * @return mixed
     */
    public function getActionItemFullId()
    {
        return $this->getData(self::ACTION_ITEM_FULL_ID);
    }

    /**
     * @param $actionRequestId
     * @return SurveyAction
     */
    public function setActionRequestId($actionRequestId)
    {
        return $this->setData(self::ACTION_REQUEST_ID, $actionRequestId);
    }

    /**
     * @return mixed
     */
    public function getActionRequestId()
    {
        return $this->getData(self::ACTION_REQUEST_ID);
    }

    /**
     * @param $actionRequestFail
     * @return SurveyAction
     */
    public function setActionRequestFail($actionRequestFail)
    {
        return $this->setData(self::ACTION_REQUEST_FAIL, $actionRequestFail);
    }

    /**
     * @return mixed
     */
    public function getActionRequestFail()
    {
        return $this->getData(self::ACTION_REQUEST_FAIL);
    }

    /**
     * @return int
     */
    public function generateItemId()
    {
        $actionFullId = $this->getActionFullId();
        $ids = $this->generateActionIds($actionFullId);

        return intval($ids[1]);
    }

    /**
     * @param $actionItemTitle
     * @return SurveyAction
     */
    public function setActionItemTitle($actionItemTitle)
    {
        return $this->setData(self::ACTION_ITEM_TITLE, $actionItemTitle);
    }

    /**
     * @return mixed
     */
    public function getActionItemTitle()
    {
        return $this->getData(self::ACTION_ITEM_TITLE);
    }

    /**
     * @param $actionItemThumbnail
     * @return SurveyAction
     */
    public function setActionItemThumbnail($actionItemThumbnail)
    {
        return $this->setData(self::ACTION_ITEM_THUMBNAIL, $actionItemThumbnail);
    }

    /**
     * @return mixed
     */
    public function getActionItemThumbnail()
    {
        return $this->getData(self::ACTION_ITEM_THUMBNAIL);
    }

    /**
     * @param $actionItemColor
     * @return SurveyAction
     */
    public function setActionItemColor($actionItemColor)
    {
        return $this->setData(self::ACTION_ITEM_COLOR, $actionItemColor);
    }

    /**
     * @return mixed
     */
    public function getActionItemColor()
    {
        return $this->getData(self::ACTION_ITEM_COLOR);
    }

    /**
     * @param $actionItemSize
     * @return SurveyAction
     */
    public function setActionItemSize($actionItemSize)
    {
        return $this->setData(self::ACTION_ITEM_SIZE, $actionItemSize);
    }

    /**
     * @return mixed
     */
    public function getActionItemSize()
    {
        return $this->getData(self::ACTION_ITEM_SIZE);
    }

    /**
     * Returns an array with item id,
     * qty id, action id in such order
     * @param $actionFullId
     * @return mixed
     */
    public function generateActionIds($actionFullId)
    {
        $idMatch = null;
        preg_match(
            '/(.*)\_(.*)\_(.*)/',
            $actionFullId,
            $idMatch
        );

        return $idMatch;
    }

    /**
     * @param $actionId
     * @return SurveyAction
     */
    public function setActionId($actionId)
    {
        return $this->setData(self::ACTION_ID, $actionId);
    }

    /**
     * @return mixed
     */
    public function getActionId()
    {
        return $this->getData(self::ACTION_ID);
    }

    /**
     * @param null $action
     * @return string
     */
    public function getActionFullId($action = null)
    {
        if ($action !== null) {
            $itemFullId = $action[self::ACTION_ITEM_FULL_ID];
            $actionId = $action[self::ACTION_ID];
        } else {
            $itemFullId = $this->getActionItemFullId();
            $actionId = $this->getActionId();
        }

        return $itemFullId . '_' . $actionId;
    }

    /**
     * @param $redirectUrl
     * @return SurveyAction
     */
    public function setRedirectUrl($redirectUrl)
    {
        return $this->setData(self::REDIRECT_URL, $redirectUrl);
    }

    /**
     * @return mixed
     */
    public function getRedirectUrl()
    {
        return $this->getData(self::REDIRECT_URL);
    }


    /**
     * @param $redirectPath
     * @return SurveyAction
     */
    public function setRedirectPath($redirectPath)
    {
        return $this->setData(self::REDIRECT_PATH, $redirectPath);
    }

    /**
     * @return mixed
     */
    public function getRedirectPath()
    {
        return $this->getData(self::REDIRECT_PATH);
    }

    /**
     * @param $requestedPrevious
     * @return SurveyAction
     */
    public function setRequestedPrevious($requestedPrevious)
    {
        return $this->setData(self::REQUESTED_PREVIOUS, $requestedPrevious);
    }

    /**
     * @return mixed
     */
    public function getRequestedPrevious()
    {
        return $this->getData(self::REQUESTED_PREVIOUS);
    }

    /**
     * @param $requestedNext
     * @return SurveyAction
     */
    public function setRequestedNext($requestedNext)
    {
        return $this->setData(self::REQUESTED_NEXT, $requestedNext);
    }

    /**
     * @return mixed
     */
    public function getRequestedNext()
    {
        return $this->getData(self::REQUESTED_NEXT);
    }

    /**
     * @param $previousAvailable
     * @return SurveyAction
     */
    public function setPreviousAvailable($previousAvailable)
    {
        return $this->setData(self::PREVIOUS_AVAILABLE, $previousAvailable);
    }

    /**
     * @return mixed
     */
    public function getPreviousAvailable()
    {
        return $this->getData(self::PREVIOUS_AVAILABLE);
    }

    /**
     * @param $nextAvailable
     * @return SurveyAction
     */
    public function setNextAvailable($nextAvailable)
    {
        return $this->setData(self::NEXT_AVAILABLE, $nextAvailable);
    }

    /**
     * @return mixed
     */
    public function getNextAvailable()
    {
        return $this->getData(self::NEXT_AVAILABLE);
    }

    /**
     * @param $questionIdentifier
     * @return SurveyAction
     */
    public function setQuestionIdentifier($questionIdentifier)
    {
        return $this->setData(self::QUESTION_IDENTIFIER, $questionIdentifier);
    }

    /**
     * @return mixed
     */
    public function getQuestionIdentifier()
    {
        return $this->getData(self::QUESTION_IDENTIFIER);
    }

    /**
     * @param $questionType
     * @return SurveyAction
     */
    public function setQuestionType($questionType)
    {
        return $this->setData(self::QUESTION_TYPE, $questionType);
    }

    /**
     * @return mixed
     */
    public function getQuestionType()
    {
        return $this->getData(self::QUESTION_TYPE);
    }

    /**
     * @param $questionTitle
     * @return SurveyAction
     */
    public function setQuestionTitle($questionTitle)
    {
        return $this->setData(self::QUESTION_TITLE, $questionTitle);
    }

    /**
     * @return mixed
     */
    public function getQuestionTitle()
    {
        return $this->getData(self::QUESTION_TITLE);
    }

    /**
     * @param $questionChoices
     * @return SurveyAction
     */
    public function setQuestionChoices($questionChoices)
    {
        return $this->setData(self::QUESTION_CHOICES, $questionChoices);
    }

    /**
     * @return mixed
     */
    public function getQuestionChoices()
    {
        return $this->getData(self::QUESTION_CHOICES);
    }

    /**
     * @param $questionRequired
     * @return SurveyAction
     */
    public function setQuestionRequired($questionRequired)
    {
        return $this->setData(self::QUESTION_REQUIRED, $questionRequired);
    }

    /**
     * @return mixed
     */
    public function getQuestionRequired()
    {
        return $this->getData(self::QUESTION_REQUIRED);
    }

    /**
     * @param $questionAnswer
     * @return SurveyAction
     */
    public function setQuestionAnswer($questionAnswer)
    {
        return $this->setData(self::QUESTION_ANSWER, $questionAnswer);
    }

    /**
     * @return mixed
     */
    public function getQuestionAnswer()
    {
        return $this->getData(self::QUESTION_ANSWER);
    }
}
