<?php
/**
 * @category    Kurufootwear
 * @package     Kurufootwear\Rma
 * @author      Jim McGowen <jim@kurufootwear.com>
 * @copyright   Copyright (c) 2017 KURU Footwear. All rights reserved.
 */

namespace Kurufootwear\Rma\Model;

use Magento\Framework\Model\AbstractModel;
use Magento\Framework\DataObject\IdentityInterface;
use Magento\Framework\Data\Collection\AbstractDb;
use Magento\Framework\Model\Context;
use Magento\Framework\Model\ResourceModel\AbstractResource;
use Magento\Framework\Registry;

/**
 * Class ShippingLabel
 * @package Kurufootwear\Rma\Model
 */
class ShippingLabel extends AbstractModel implements IdentityInterface
{
    const CACHE_TAG = 'kuru_rma_shipping_label';
    
    protected $_cacheTag = 'kuru_rma_shipping_label';
    
    protected $_eventPrefix = 'kuru_rma_shipping_label';
    
    /**
     * Inspection constructor.
     *
     * @param Context $context
     * @param Registry $registry
     * @param AbstractResource|null $resource
     * @param AbstractDb|null $resourceCollection
     * @param array $data
     */
    public function __construct(
        Context $context,
        Registry $registry,
        AbstractResource $resource = null,
        AbstractDb $resourceCollection = null,
        array $data = []
    ) {
        parent::__construct($context, $registry, $resource, $resourceCollection, $data);
    }
    
    /**
     * Internal Constructor
     */
    protected function _construct()
    {
        $this->_init('Kurufootwear\Rma\Model\ResourceModel\ShippingLabel');
    }
    
    /**
     * @return array
     */
    public function getIdentities()
    {
        return [self::CACHE_TAG . '_' . $this->getId()];
    }
    
    /**
     * @param $id
     *
     * @return $this
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function loadById($id)
    {
        $this->_getResource()->load($this, $id, 'id');
        
        return $this;
    }
    
    /**
     * @param $id
     *
     * @return $this
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function loadByRequestId($id)
    {
        $this->_getResource()->load($this, $id, 'request_id');
        
        return $this;
    }
    
    /**
     * Save object data.
     */
    public function save()
    {
        $this->_getResource()->save($this);
    }
}
