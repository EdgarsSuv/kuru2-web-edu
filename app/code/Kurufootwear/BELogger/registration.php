<?php
/**
 * @category    Kurufootwear
 * @package     Kurufootwear\BELogger
 * @author      Krisjanis Veinbahs <krisjanisv@scandiweb.com>
 * @copyright   Copyright (c) 2018 KURU Footwear
 */
\Magento\Framework\Component\ComponentRegistrar::register(
    \Magento\Framework\Component\ComponentRegistrar::MODULE,
    'Kurufootwear_BELogger',
    __DIR__
);
