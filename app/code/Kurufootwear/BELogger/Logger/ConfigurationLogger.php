<?php
/**
 * @category    Kurufootwear
 * @package     Kurufootwear\BELogger
 * @author      Krisjanis Veinbahs <krisjanisv@scandiweb.com>
 * @copyright   Copyright (c) 2018 KURU Footwear
 */
namespace Kurufootwear\BELogger\Logger;

use Monolog\Logger;
use Magento\Framework\HTTP\PhpEnvironment\RemoteAddress;
use Magento\Backend\Model\Auth\Session;


/**
 * Class ConfigurationLogger
 *
 * Store configuration change logger class
 *
 * @package Kurufootwear\BELogger\Logger
 */
class ConfigurationLogger extends Logger
{
    /**
     * @var RemoteAddress
     */
    private $remoteAddress;

    /**
     * @var Session
     */
    private $authSession;

    /**
     * ConfigurationLogger constructor.
     *
     * @param RemoteAddress $remoteAddress
     * @param Session $authSession
     * @param array|callable[] $name
     * @param array $handlers
     * @param array $processors
     */
    public function __construct(
        RemoteAddress $remoteAddress,
        Session $authSession,
        $name,
        $handlers = [],
        $processors = []
    ) {
        $this->remoteAddress = $remoteAddress;
        $this->authSession = $authSession;

        parent::__construct(
            $name,
            $handlers,
            $processors
        );
    }

    /**
     * Log change in configrations with admin info
     *
     * @param $message
     */
    public function adminLog($message)
    {
        $accessIP = print_r($this->remoteAddress->getRemoteAddress(), true);
        $adminUser = $this->authSession->getUser();
        $adminUsername = $adminUser ? print_r($adminUser->getUserName(), true) : '-';

        $this->log('info', sprintf(
            __("Admin '%s' from IP '%s' made a change: %s"),
            $adminUsername,
            $accessIP,
            $message
        ));
    }
}