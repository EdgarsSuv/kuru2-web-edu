<?php
/**
 * @category    Kurufootwear
 * @package     Kurufootwear\BELogger
 * @author      Krisjanis Veinbahs <krisjanisv@scandiweb.com>
 * @copyright   Copyright (c) 2018 KURU Footwear
 */
namespace Kurufootwear\BELogger\Logger;

use Monolog\Logger;
use Magento\Framework\Logger\Handler\Base;

class ConfigurationHandler extends Base
{
    /**
     * Logging level
     *
     * @var int
     */
    protected $loggerType = Logger::INFO;

    /**
     * File name
     *
     * @var string
     */
    protected $fileName = '/var/log/configuration.log';
}