<?php
/**
 * @category    Kurufootwear
 * @package     Kurufootwear\BELogger
 * @author      Krisjanis Veinbahs <krisjanisv@scandiweb.com>
 * @copyright   Copyright (c) 2018 KURU Footwear
 */
namespace Kurufootwear\BELogger\DB;

use Exception;
use Kurufootwear\BELogger\Logger\ConfigurationLogger;
use Magento\Framework\App\ResourceConnection;
use Magento\Framework\App\Config\Value;
use Magento\Framework\DB\Transaction;
use Zend_Db_Statement_Interface;

/**
 * Database transaction class with commit data logger
 *
 * Written to be injected for logging in \Magento\Config\Model\Config
 */
class ConfigTransaction extends Transaction
{
    /**
     * @var ConfigurationLogger
     */
    private $configurationLogger;

    /**
     * @var ResourceConnection
     */
    private $resourceConnection;

    /**
     * ConfigTransaction constructor.
     *
     * @param ConfigurationLogger $configurationLogger
     * @param ResourceConnection $resourceConnection
     */
    public function __construct(
        ConfigurationLogger $configurationLogger,
        ResourceConnection $resourceConnection
    ) {
        $this->configurationLogger = $configurationLogger;
        $this->resourceConnection = $resourceConnection;
    }

    /**
     * Transaction save with a logger
     *
     * @throws Exception
     */
    public function save()
    {
        $oldConfigIds = [];
        foreach ($this->_objects as $config) {
            /* @var Value $config */
            $oldConfigIds[] = (int)$config->getId();
        }
        $oldConfigs = $this->getUncachedConfigValues($oldConfigIds);

        parent::save();

        /**
         * If exception wasn't thrown, we continue with
         * logging the changed config data
         *
         * Note - we're not checking the value straight from the config objects
         * because they may return '****' if the payload is sensitive,
         * which may result in a false positive detection of a config change
         */
        $newConfigIds = [];
        foreach ($this->_objects as $config) {
            /* @var Value $config */
            $newConfigIds[] = (int)$config->getId();
        }
        $newConfigs = $this->getUncachedConfigValues($newConfigIds);

        $this->logChangedConfigs($oldConfigs, $newConfigs);
    }

    /**
     * Detect changed configs and log information
     *
     * @param $oldConfigs
     * @param $newConfigs
     */
    private function logChangedConfigs($oldConfigs, $newConfigs) {
        $configs = array_unique(array_merge(array_keys($oldConfigs), array_keys($newConfigs)));

        foreach ($configs as $configId) {
            $oldConfig = $oldConfigs[$configId] ?? null;
            $newConfig = $newConfigs[$configId] ?? null;

            if ($oldConfig && $newConfig && $oldConfig['value'] !== $newConfig['value']) {
                // Config changed
                $this->configurationLogger->adminLog(sprintf(
                    __("Changed config path '%s' from '%s' to '%s', scope '%s', scope id '%s'"),
                    $newConfig['path'],
                    $oldConfig['value'],
                    $newConfig['value'],
                    $newConfig['scope'],
                    $newConfig['scope_id']
                ));
            } else if (!$oldConfig && $newConfig) {
                // Config added
                $this->configurationLogger->adminLog(sprintf(
                    __("Added config path '%s' as '%s', scope '%s', scope id '%s'"),
                    $newConfig['path'],
                    $newConfig['value'],
                    $newConfig['scope'],
                    $newConfig['scope_id']
                ));
            } else if ($oldConfig && !$newConfig) {
                // Config removed
                $this->configurationLogger->adminLog(sprintf(
                    __("Removed config path '%s' value '%s', scope '%s', scope id '%s'"),
                    $oldConfig['path'],
                    $oldConfig['value'],
                    $oldConfig['scope'],
                    $oldConfig['scope_id']
                ));
            }
        }
    }

    /**
     * Get uncached config values by ids
     *
     * @param array $configIds
     *
     * @return array|Zend_Db_Statement_Interface
     */
    private function getUncachedConfigValues(array $configIds) {
        try {
            $connection = $this->resourceConnection->getConnection();
            $select = $connection->select()
                ->from('core_config_data')
                ->where(
                    'config_id IN (?)',
                    $configIds
                );

            $configs = $connection->query($select)->fetchAll();
        } catch (Exception $exception) {
            $this->configurationLogger->log('error', __('Failed to read un-cached config values'));

            $configs = [];
        }

        return $this->formatConfigsWithIdentifiers($configs);
    }

    /**
     * Format configs from SQL query for easier later parsing
     *
     * @param array $configs
     *
     * @return array
     */
    private function formatConfigsWithIdentifiers(array $configs) {
        $formatted = [];

        foreach ($configs as $config) {
            $uniqueIdentifier = sprintf(
                '%s,%s,%s',
                $config['path'],
                $config['scope'],
                $config['scope_id']
            );
            $formatted[$uniqueIdentifier] = $config;
        }

        return $formatted;
    }
}