<?php
/**
 * @category    Kurufootwear
 * @package     Kurufootwear\BELogger
 * @author      Krisjanis Veinbahs <krisjanisv@scandiweb.com>
 * @copyright   Copyright (c) 2018 KURU Footwear
 */
namespace Kurufootwear\BELogger\Plugin;

use Kurufootwear\BELogger\Logger\ConfigurationLogger;
use Magento\Framework\App\Config\MutableScopeConfigInterface;
use Magento\Framework\App\Config\ScopeConfigInterface;

/**
 * Class MutableScopeConfigLoggerPlugin
 *
 * Created to log who changes what in BE store configurations
 *
 * @package Kurufootwear\BELogger\Plugin
 */
class MutableScopeConfigPlugin {
    /**
     * @var ConfigurationLogger
     */
    protected $configurationLogger;

    /**
     * MutableScopeConfigPlugin constructor.
     *
     * @param ConfigurationLogger $configurationLogger
     */
    public function __construct(
        ConfigurationLogger $configurationLogger
    ) {
        $this->configurationLogger = $configurationLogger;
    }

    /**
     * Log changed BE configuration info
     *
     * @param MutableScopeConfigInterface $subject
     * @param callable $proceed
     * @param $path
     * @param $value
     * @param string $scope
     * @param null $scopeCode
     *
     * @return mixed
     */
    public function aroundSetValue(
        MutableScopeConfigInterface $subject,
        callable $proceed,
        $path,
        $value,
        $scope = ScopeConfigInterface::SCOPE_TYPE_DEFAULT,
        $scopeCode = null
    ) {
        $this->configurationLogger->adminLog(sprintf(
            __("Changed config path '%s', scope '%s', scope id '%s'"),
            $path,
            $scope,
            $scopeCode
        ));

        return $proceed($path, $value, $scope, $scopeCode);
    }
}