<?php
/**
 * @vendor Kurufootwear
 * @module Kurufootwear_Slider
 * @author Aleksejs Baranovs <info@scandiweb.com>
 * @copyright Copyright (c) 2018 KURU Footwear. All rights reserved.
 */

namespace Kurufootwear\Slider\Block\Adminhtml\PdpConfiguration\Edit\Tab;

use Magento\Backend\Block\Widget\Form\Generic;
use Magento\Backend\Block\Widget\Tab\TabInterface;
use Magento\Framework\Data\Form;
use Magento\Framework\Exception\LocalizedException;
use Magento\Framework\Phrase;

/**
 * Class PdpConfiguration
 * @package Kurufootwear\Slider\Block\Adminhtml\PdpConfiguration\Edit\Tab
 */
class PdpConfiguration extends Generic implements TabInterface
{
    /**
     * Prepare form
     *
     * @return Generic
     *
     * @throws LocalizedException
     */
    protected function _prepareForm()
    {
        $model = $this->_coreRegistry->registry('slide');

        /** @var Form $form */
        $form = $this->_formFactory->create();
        $form->setHtmlIdPrefix('slide_');

        $fieldset = $form->addFieldset(
            'base_fieldset',
            [
                'legend' => __('PDP Slide Configuration')
            ]
        );

        $fieldset->addField(
            'header_text',
            'textarea',
            [
                'name' => 'header_text',
                'label' => __('Header Text'),
                'title' => __('Header Text'),
                'required' => false,
                'note' => htmlspecialchars(__('Enter banner header text with necessary breaks using <br> tag')),
            ]
        );

        $fieldset->addField(
            'header_hex_color',
            'text',
            [
                'name' => 'header_hex_color',
                'label' => __('Header HEX Color'),
                'title' => __('Header HEX Color'),
                'required' => false,
                'note' => __('Enter the HEX color value, like #FFFFFF'),
            ]
        );

        $fieldset->addField(
            'header_mobile_size',
            'text',
            [
                'name' => 'header_mobile_size',
                'label' => __('Header Mobile Text Size'),
                'title' => __('Header Mobile Text Size'),
                'required' => false,
                'note' => __('Enter the header mobile text size value, like 22px. Mobile width to 768px. Leave blank to use homepage slider styles.'),
            ]
        );

        $fieldset->addField(
            'header_tablet_size',
            'text',
            [
                'name' => 'header_tablet_size',
                'label' => __('Header Tablet Text Size'),
                'title' => __('Header Tablet Text Size'),
                'required' => false,
                'note' => __('Enter the header tablet text size value, like 22px. Tablet width from 768px to 1024px. Leave blank to use homepage slider styles.'),
            ]
        );

        $fieldset->addField(
            'header_desktop_size',
            'text',
            [
                'name' => 'header_desktop_size',
                'label' => __('Header Desktop Text Size'),
                'title' => __('Header Desktop Text Size'),
                'required' => false,
                'note' => __('Enter the header desktop text size value, like 22px. Desktop width from 1024px. Leave blank to use homepage slider styles.'),
            ]
        );

        $fieldset->addField(
            'message_text',
            'textarea',
            [
                'name' => 'message_text',
                'label' => __('Message Text'),
                'title' => __('Message Text'),
                'required' => false,
                'note' => htmlspecialchars(__('Enter banner message text with necessary breaks using <br> tag')),
            ]
        );

        $fieldset->addField(
            'message_hex_color',
            'text',
            [
                'name' => 'message_hex_color',
                'label' => __('Message HEX Color'),
                'title' => __('Message HEX Color'),
                'required' => false,
                'note' => __('Enter the HEX color value, like #FFFFFF'),
            ]
        );

        $fieldset->addField(
            'message_mobile_size',
            'text',
            [
                'name' => 'message_mobile_size',
                'label' => __('Message Mobile Text Size'),
                'title' => __('Message Mobile Text Size'),
                'required' => false,
                'note' => __('Enter the message mobile text size value, like 22px. Mobile width to 768px. Leave blank to use homepage slider styles.'),
            ]
        );

        $fieldset->addField(
            'message_tablet_size',
            'text',
            [
                'name' => 'message_tablet_size',
                'label' => __('Message Tablet Text Size'),
                'title' => __('Message Tablet Text Size'),
                'required' => false,
                'note' => __('Enter the message tablet text size value, like 22px. Tablet width from 768px to 1024px. Leave blank to use homepage slider styles.'),
            ]
        );

        $fieldset->addField(
            'message_desktop_size',
            'text',
            [
                'name' => 'message_desktop_size',
                'label' => __('Message Desktop Text Size'),
                'title' => __('Message Desktop Text Size'),
                'required' => false,
                'note' => __('Enter the message desktop text size value, like 22px. Desktop width from 1024px. Leave blank to use homepage slider styles.'),
            ]
        );

        $form->addFieldNameSuffix('slide');
        $form->setValues($model->getData());
        $this->setForm($form);

        return parent::_prepareForm();
    }

    /**
     * Tab label
     *
     * @return Phrase
     */
    public function getTabLabel()
    {
        return __('PDP Slide Configuration');
    }

    /**
     * Tab title
     *
     * @return Phrase
     */
    public function getTabTitle()
    {
        return __('PDP Slide Configuration');
    }

    /**
     * Show tab
     *
     * @return bool
     */
    public function canShowTab()
    {
        return true;
    }

    /**
     * Is hidden
     *
     * @return bool
     */
    public function isHidden()
    {
        return false;
    }
}
