<?php
/**
 * @vendor Kurufootwear
 * @module Kurufootwear_Slider
 * @author Aleksejs Baranovs <info@scandiweb.com>
 * @copyright Copyright (c) 2018 KURU Footwear. All rights reserved.
 */

namespace Kurufootwear\Slider\Block\Widget;

use Magento\Widget\Block\BlockInterface;
use Scandiweb\Slider\Block\Widget\Slider as BaseSlider;

/**
 * Class Slider
 * Override to implement BlockInterface
 *
 * @package Kurufootwear\Slider\Block\Widget
 */
class Slider extends BaseSlider implements BlockInterface
{
}
