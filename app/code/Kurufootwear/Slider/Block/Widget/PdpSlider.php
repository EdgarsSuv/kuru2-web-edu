<?php
/**
 * @vendor Kurufootwear
 * @module Kurufootwear_Slider
 * @author Aleksejs Baranovs <info@scandiweb.com>
 * @copyright Copyright (c) 2018 KURU Footwear. All rights reserved.
 */

namespace Kurufootwear\Slider\Block\Widget;

use Magento\Widget\Block\BlockInterface;
use Scandiweb\Slider\Block\Slider;

/**
 * Class PdpSlider
 * @package Kurufootwear\Slider\Block\Widget
 */
class PdpSlider extends Slider implements BlockInterface
{
    /* @var string $_template */
    protected $_template = 'Kurufootwear_Slider::slider-pdp.phtml';
}
