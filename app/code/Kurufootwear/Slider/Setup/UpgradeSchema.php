<?php
/**
 * @vendor Kurufootwear
 * @module Kurufootwear_Slider
 * @author Aleksejs Baranovs <info@scandiweb.com>
 * @copyright Copyright (c) 2018 KURU Footwear. All rights reserved.
 */

namespace Kurufootwear\Slider\Setup;

use Magento\Framework\Setup\UpgradeSchemaInterface;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\SchemaSetupInterface;
use Magento\Framework\DB\Ddl\Table;

/**
 * Class UpgradeSchema
 * @package Kurufootwear\Slider\Setup
 */
class UpgradeSchema implements UpgradeSchemaInterface
{
    /**
     * Upgrade
     *
     * @param SchemaSetupInterface $setup
     * @param ModuleContextInterface $context
     */
    public function upgrade(
        SchemaSetupInterface $setup,
        ModuleContextInterface $context
    ) {
        $setup->startSetup();

        if (version_compare($context->getVersion(), '0.1.1') < 0) {
            $this->addScandiSliderPdpColumns($setup);
        }

        $setup->endSetup();
    }

    /**
     * Add scandi slider pdp slide configuration columns
     *
     * @param $setup
     */
    private function addScandiSliderPdpColumns($setup)
    {
        // Get module table
        $tableName = $setup->getTable('scandiweb_slider_slide');

        // Check if the table already exists
        if ($setup->getConnection()->isTableExists($tableName)) {
            $connection = $setup->getConnection();
            $connection->addColumn(
                $tableName,
                'header_text',
                [
                    'type' => Table::TYPE_TEXT,
                    'nullable' => true,
                    'comment' => 'Header text'
                ]
            );

            $connection->addColumn(
                $tableName,
                'header_hex_color',
                [
                    'type' => Table::TYPE_TEXT,
                    'nullable' => true,
                    'comment' => 'Header HEX color'
                ]
            );

            $connection->addColumn(
                $tableName,
                'header_mobile_size',
                [
                    'type' => Table::TYPE_TEXT,
                    'nullable' => true,
                    'comment' => 'Header mobile text size'
                ]
            );

            $connection->addColumn(
                $tableName,
                'header_tablet_size',
                [
                    'type' => Table::TYPE_TEXT,
                    'nullable' => true,
                    'comment' => 'Header tablet text size'
                ]
            );

            $connection->addColumn(
                $tableName,
                'header_desktop_size',
                [
                    'type' => Table::TYPE_TEXT,
                    'nullable' => true,
                    'comment' => 'Header desktop text size'
                ]
            );

            $connection->addColumn(
                $tableName,
                'message_text',
                [
                    'type' => Table::TYPE_TEXT,
                    'nullable' => true,
                    'comment' => 'Message text'
                ]
            );

            $connection->addColumn(
                $tableName,
                'message_hex_color',
                [
                    'type' => Table::TYPE_TEXT,
                    'nullable' => true,
                    'comment' => 'Message HEX color'
                ]
            );

            $connection->addColumn(
                $tableName,
                'message_mobile_size',
                [
                    'type' => Table::TYPE_TEXT,
                    'nullable' => true,
                    'comment' => 'Message mobile text size'
                ]
            );

            $connection->addColumn(
                $tableName,
                'message_tablet_size',
                [
                    'type' => Table::TYPE_TEXT,
                    'nullable' => true,
                    'comment' => 'Message tablet text size'
                ]
            );

            $connection->addColumn(
                $tableName,
                'message_desktop_size',
                [
                    'type' => Table::TYPE_TEXT,
                    'nullable' => true,
                    'comment' => 'Message desktop text size'
                ]
            );
        }
    }
}
