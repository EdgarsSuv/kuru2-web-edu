<?php
/**
 * @vendor Kurufootwear
 * @module Kurufootwear_GuestToReg
 * @author Aleksejs Baranovs <info@scandiweb.com>
 * @copyright Copyright (c) 2018 KURU Footwear. All rights reserved.
 */

use Magento\Framework\Component\ComponentRegistrar;

ComponentRegistrar::register(
    ComponentRegistrar::MODULE,
    'Kurufootwear_GuestToReg',
    __DIR__
);
