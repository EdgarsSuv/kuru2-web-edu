<?php
/**
 * @vendor Kurufootwear
 * @module Kurufootwear_GuestToReg
 * @author Aleksejs Baranovs <info@scandiweb.com>
 * @copyright Copyright (c) 2018 KURU Footwear. All rights reserved.
 */

namespace Kurufootwear\GuestToReg\Controller\Adminhtml\GuestToRegForm;

use CommerceExtensions\GuestToReg\Controller\Adminhtml\GuestToRegForm\Index as BaseIndex;

/**
 * Class Index
 * @package Kurufootwear\GuestToReg\Controller\Adminhtml\GuestToRegForm
 */
class Index extends BaseIndex
{
    /**
     * Check Grid List Permission.
     * Override to change resource name
     *
     * @return bool
     */
    protected function _isAllowed()
    {
        return $this->_authorization->isAllowed('CommerceExtensions_GuestToReg::guestcustomer');
    }
}
