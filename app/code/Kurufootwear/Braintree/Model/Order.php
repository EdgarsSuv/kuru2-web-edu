<?php
/**
 * @category     Kurufootwear
 * @package      Kurufootwear/Braintree
 * @author       Krisjanis Veinbahs <krisjanisv@kurufootwear.com>
 * @copyright    Copyright (c) 2018 KURU Footwear. All rights reserved.
 */
namespace Kurufootwear\Braintree\Model;

use Magento\Framework\Api\AttributeValueFactory;
use Magento\Catalog\Api\ProductRepositoryInterface;
use Magento\Catalog\Model\Product\Visibility;
use Magento\Catalog\Model\ResourceModel\Product\CollectionFactory as ProductCollectionFactory;
use Magento\Directory\Model\CurrencyFactory;
use Magento\Eav\Model\Config as EavConfig;
use Magento\Framework\Api\ExtensionAttributesFactory;
use Magento\Framework\Data\Collection\AbstractDb;
use Magento\Framework\Exception\LocalizedException;
use Magento\Framework\Model\Context;
use Magento\Framework\Model\ResourceModel\AbstractResource;
use Magento\Framework\Pricing\PriceCurrencyInterface;
use Magento\Framework\Registry;
use Magento\Framework\Stdlib\DateTime\TimezoneInterface;
use Magento\Payment\Gateway\Command\CommandException as PaymentCommandException;
use Magento\Sales\Api\InvoiceManagementInterface;
use Magento\Sales\Model\Order\Config;
use Magento\Sales\Model\Order as MagentoOrder;
use Magento\Sales\Model\ResourceModel\Order\Address\CollectionFactory as AddressCollectionFactory;
use Magento\Sales\Model\ResourceModel\Order\CollectionFactory as OrderCollectionFactory;
use Magento\Sales\Model\ResourceModel\Order\Creditmemo\CollectionFactory as CreditMemoCollectionFactory;
use Magento\Sales\Model\ResourceModel\Order\Invoice\CollectionFactory as InvoiceCollectionFactory;
use Magento\Sales\Model\ResourceModel\Order\Item\CollectionFactory;
use Magento\Framework\Message\ManagerInterface as MessageManagerInterface;
use Magento\Sales\Model\ResourceModel\Order\Payment\CollectionFactory as PaymentCollectionFactory;
use Magento\Sales\Model\ResourceModel\Order\Shipment\CollectionFactory as ShipmentCollectionFactory;
use Magento\Sales\Model\ResourceModel\Order\Shipment\Track\CollectionFactory as ShipmentTrackCollectionFactory;
use Magento\Sales\Model\ResourceModel\Order\Status\History\CollectionFactory as StatusHistoryCollectionFactory;
use Magento\Sales\Model\Order\Status\HistoryFactory;
use Magento\Store\Model\StoreManagerInterface;

/**
 * Class Order override
 *
 * Overridden to ignore failed payment cancelling transactions
 * This should only be overridden in adminhtml scope
 *
 * @package Kurufootwear\Braintree\Model
 */
class Order extends MagentoOrder
{
    /**
     * @var MessageManagerInterface
     */
    protected $messageManager;

    /**
     * Order constructor.
     * @param Context $context
     * @param Registry $registry
     * @param ExtensionAttributesFactory $extensionFactory
     * @param AttributeValueFactory $customAttributeFactory
     * @param TimezoneInterface $timezone
     * @param StoreManagerInterface $storeManager
     * @param Config $orderConfig
     * @param ProductRepositoryInterface $productRepository
     * @param CollectionFactory $orderItemCollectionFactory
     * @param Visibility $productVisibility
     * @param InvoiceManagementInterface $invoiceManagement
     * @param CurrencyFactory $currencyFactory
     * @param EavConfig $eavConfig
     * @param HistoryFactory $orderHistoryFactory
     * @param AddressCollectionFactory $addressCollectionFactory
     * @param PaymentCollectionFactory $paymentCollectionFactory
     * @param StatusHistoryCollectionFactory $historyCollectionFactory
     * @param InvoiceCollectionFactory $invoiceCollectionFactory
     * @param ShipmentCollectionFactory $shipmentCollectionFactory
     * @param CreditMemoCollectionFactory $memoCollectionFactory
     * @param ShipmentTrackCollectionFactory $trackCollectionFactory
     * @param OrderCollectionFactory $salesOrderCollectionFactory
     * @param PriceCurrencyInterface $priceCurrency
     * @param ProductCollectionFactory $productListFactory
     * @param AbstractResource|null $resource
     * @param AbstractDb|null $resourceCollection
     * @param MessageManagerInterface $messageManager
     * @param array $data
     */
    public function __construct(
        Context $context,
        Registry $registry,
        ExtensionAttributesFactory $extensionFactory,
        AttributeValueFactory $customAttributeFactory,
        TimezoneInterface $timezone,
        StoreManagerInterface $storeManager,
        Config $orderConfig,
        ProductRepositoryInterface $productRepository,
        CollectionFactory $orderItemCollectionFactory,
        Visibility $productVisibility,
        InvoiceManagementInterface $invoiceManagement,
        CurrencyFactory $currencyFactory,
        EavConfig $eavConfig,
        HistoryFactory $orderHistoryFactory,
        AddressCollectionFactory $addressCollectionFactory,
        PaymentCollectionFactory $paymentCollectionFactory,
        StatusHistoryCollectionFactory $historyCollectionFactory,
        InvoiceCollectionFactory $invoiceCollectionFactory,
        ShipmentCollectionFactory $shipmentCollectionFactory,
        CreditMemoCollectionFactory $memoCollectionFactory,
        ShipmentTrackCollectionFactory $trackCollectionFactory,
        OrderCollectionFactory $salesOrderCollectionFactory,
        PriceCurrencyInterface $priceCurrency,
        ProductCollectionFactory $productListFactory,
        MessageManagerInterface $messageManager,
        AbstractResource $resource = null,
        AbstractDb $resourceCollection = null,
        array $data = []
    ) {
        parent::__construct(
            $context,
            $registry,
            $extensionFactory,
            $customAttributeFactory,
            $timezone,
            $storeManager,
            $orderConfig,
            $productRepository,
            $orderItemCollectionFactory,
            $productVisibility,
            $invoiceManagement,
            $currencyFactory,
            $eavConfig,
            $orderHistoryFactory,
            $addressCollectionFactory,
            $paymentCollectionFactory,
            $historyCollectionFactory,
            $invoiceCollectionFactory,
            $shipmentCollectionFactory,
            $memoCollectionFactory,
            $trackCollectionFactory,
            $salesOrderCollectionFactory,
            $priceCurrency,
            $productListFactory,
            $resource,
            $resourceCollection,
            $data
        );

        $this->messageManager = $messageManager;
    }

    /**
     * Cancel order
     * Overridden to ignore failed payment cancelling transactions
     *
     * @throws LocalizedException
     * @return $this
     */
    public function cancel()
    {
        if ($this->canCancel()) {
            try {
                $this->getPayment()->cancel();
            } catch (PaymentCommandException $exception) {
                $fullWarning = sprintf(
                    __("Cancelling order (#%s) payment (#%s) failed - ignoring, please review manually."),
                    $this->getId(),
                    $this->getPayment()->getId()
                );
                $this->_logger->addWarning($fullWarning);
                $this->messageManager->addWarningMessage($fullWarning);
            }
            $this->registerCancellation();

            $this->_eventManager->dispatch('order_cancel_after', ['order' => $this]);
        }

        return $this;
    }
}