<?php
/**
 * @category  Kurufootwear
 * @package   Kurufootwear\Migration
 * @author    Kristaps Stalidzāns <kristapss@scandiweb.com>
 * @copyright Copyright (c) 2017 Scandiweb, Inc (http://scandiweb.com)
 * @license   http://opensource.org/licenses/OSL-3.0 The Open Software License 3.0 (OSL-3.0)
 */
namespace Kurufootwear\Migration\Command;

use Magento\Catalog\Api\Data\ProductInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Magento\Framework\App\ResourceConnection;
use Magento\Framework\App\State as AppState;
use Magento\Framework\DB\Adapter\AdapterInterface;
use Magento\Framework\Api\SearchCriteriaBuilder;
use Magento\Catalog\Api\ProductRepositoryInterface;
use Magento\UrlRewrite\Service\V1\Data\UrlRewrite;
use Magento\UrlRewrite\Model\UrlPersistInterface;
use Magento\CatalogUrlRewrite\Model\ProductUrlRewriteGenerator;
use Magento\Catalog\Model\Product;
use Throwable;
use Magento\Framework\Exception\LocalizedException;

class RegenerateProductUrl extends Command
{
    const PRODUCT_PATH = 'catalog/product/view/id/';

    /**
     * @var AppState
     */
    protected $state;

    /**
     * @var ResourceConnection
     */
    private $resourceConnection;

    /**
     * @var SearchCriteriaBuilder
     */
    private $searchCriteriaBuilder;

    /**
     * @var ProductRepositoryInterface
     */
    private $productRepository;

    /**
     * @var ProductUrlRewriteGenerator
     */
    private $productUrlRewriteGenerator;

    /**
     * @var UrlPersistInterface
     */
    private $urlPersist;

    /**
     * RegenerateUrls constructor.
     * @param ResourceConnection $resourceConnection
     * @param SearchCriteriaBuilder $searchCriteriaBuilder
     * @param ProductRepositoryInterface $productRepository
     * @param AppState $state
     * @param ProductUrlRewriteGenerator $productUrlRewriteGenerator
     * @param UrlPersistInterface $urlPersist
     */
    public function __construct(
        ResourceConnection $resourceConnection,
        SearchCriteriaBuilder $searchCriteriaBuilder,
        ProductRepositoryInterface $productRepository,
        AppState $state,
        ProductUrlRewriteGenerator $productUrlRewriteGenerator,
        UrlPersistInterface $urlPersist
    ) {
        $this->state = $state;
        $this->resourceConnection = $resourceConnection;
        $this->searchCriteriaBuilder = $searchCriteriaBuilder;
        $this->productRepository = $productRepository;
        $this->productUrlRewriteGenerator = $productUrlRewriteGenerator;
        $this->urlPersist = $urlPersist;
        parent::__construct();
    }

    protected function configure()
    {
        $this->setName('regenerate:product:url');
        $this->setDescription('A command that regenerates product url using sku');
        $this->addArgument('sku', InputArgument::REQUIRED, 'Product SKU');
        parent::configure();
    }

    /**
     * Updates product Urls
     *
     * @param InputInterface $input
     * @param OutputInterface $output
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        try {
            $this->state->getAreaCode();
        } catch (LocalizedException $e) {
            $this->state->setAreaCode('adminhtml');
        }

        $sku = $input->getArgument('sku');
        $output->writeln('Getting product');

        try {
            $product = $this->productRepository->get($sku);
            $product->setUrlPath($product->getUrlKey());
            $this->setUrlRewrites($product);

            $output->writeln('Url Regenerated');
        } catch (\Exception $e) {
            echo $e->getMessage();
        }
    }

    /**
     * Fixes Url Rewrites
     *
     * @param Product $product
     */
    public function setUrlRewrites(Product $product)
    {
        $this->urlPersist->deleteByData([
            UrlRewrite::ENTITY_ID => $product->getId(),
            UrlRewrite::ENTITY_TYPE => ProductUrlRewriteGenerator::ENTITY_TYPE,
            UrlRewrite::REDIRECT_TYPE => 0,
            UrlRewrite::STORE_ID => 0
        ]);
        $this->urlPersist->replace(
            $this->productUrlRewriteGenerator->generate($product)
        );
    }

    /**
     * Function updates new product URL paths
     *
     * @param ProductInterface $product
     * @param ProductInterface $copiedProduct
     * @param $attribute
     */
    public function updateUrlRewrite(ProductInterface $product, ProductInterface $copiedProduct, $attribute)
    {
        $connection = $this->resourceConnection->getConnection();
        $tableName = $this->resourceConnection->getTableName('url_rewrite');
        $productId = $product->getId();
        $copiedProductId = $copiedProduct->getId();
        $results = $this->pathQuery(
            $connection,
            $tableName,
            $product,
            self::PRODUCT_PATH . $productId
        );

        /**
         * Results for plain product
         */
        if ($results) {
            $this->updatePath(
                $connection,
                $tableName,
                $results[0],
                $attribute,
                $copiedProduct,
                self::PRODUCT_PATH . $copiedProductId
            );
        }

        /**
         * Updates category Urls
         */
        foreach ($product->getCategoryIds() as $items) {
            $results = $this->pathQuery(
                $connection,
                $tableName,
                $product,
                self::PRODUCT_PATH . $productId . '/category/' . $items
            );

            if ($results) {
                $this->updatePath(
                    $connection,
                    $tableName,
                    $results[0],
                    $attribute,
                    $copiedProduct,
                    self::PRODUCT_PATH . $copiedProductId . '/category/' . $items
                );
            }
        }
    }

    /**
     * Universal function to get table information based on product for URL rewrite
     *
     * @param AdapterInterface $connection
     * @param $tableName
     * @param ProductInterface $product
     * @param $path
     * @return array
     */
    public function pathQuery(AdapterInterface $connection, $tableName, ProductInterface $product, $path)
    {
        $sql = $connection->select()
            ->from($tableName)
            ->where('entity_id = ?', $product->getId())
            ->where('target_path = ?', $path);

        return $connection->fetchAll($sql);
    }

    /**
     * Updates product path in database
     *
     * @param AdapterInterface $connection
     * @param $tableName
     * @param $result
     * @param $attribute
     * @param ProductInterface $product
     * @param $path
     */
    public function updatePath(AdapterInterface $connection, $tableName, $result, $attribute, ProductInterface $product, $path)
    {
        $connection->update(
            $tableName,
            [
                'request_path' => $result['request_path'] . '?color=' . $attribute
            ],
            [
                'target_path = ?' => $path,
                'entity_id = ?' => $product->getId()
            ]
        );
    }
} 