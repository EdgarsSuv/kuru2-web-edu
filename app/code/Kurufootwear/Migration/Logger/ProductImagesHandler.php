<?php
namespace Kurufootwear\Migration\Logger;

use Monolog\Logger;

class ProductImagesHandler extends \Magento\Framework\Logger\Handler\Base
{
    /**
     * Logging level
     * @var int
     */
    protected $loggerType = Logger::INFO;

    /**
     * File name
     * @var string
     */
    protected $fileName = '/var/log/product_images_migration.log';
}
