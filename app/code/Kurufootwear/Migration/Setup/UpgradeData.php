<?php
/**
 * @category  Kurufootwear
 * @package   Kurufootwear\Migration
 * @author    Viesturs Ruzans <viestursr@scandiweb.com>
 * @copyright Copyright (c) 2017 Scandiweb, Inc (http://scandiweb.com)
 * @license   http://opensource.org/licenses/OSL-3.0 The Open Software License 3.0 (OSL-3.0)
 */

namespace Kurufootwear\Migration\Setup;

use Kurufootwear\Migration\Api\UpgradeMigrationInterface;
use Kurufootwear\Migration\Setup\Migration\AmastyFilterSettingsMigration;
use Kurufootwear\Migration\Setup\Migration\AmastyShippingRateMigration;
use Kurufootwear\Migration\Setup\Migration\BenefitsMigration;
use Kurufootwear\Migration\Setup\Migration\BestWalkingShoesBlockMigration;
use Kurufootwear\Migration\Setup\Migration\CategoryCMSBlocksMigration;
use Kurufootwear\Migration\Setup\Migration\CategoryLayoutMigration;
use Kurufootwear\Migration\Setup\Migration\CheckoutImageMigration;
use Kurufootwear\Migration\Setup\Migration\CopyrightBlockMigration;
use Kurufootwear\Migration\Setup\Migration\DuplicateConfigurationMigration;
use Kurufootwear\Migration\Setup\Migration\EmailTemplatesConfigMigration;
use Kurufootwear\Migration\Setup\Migration\EmailTemplatesMigration;
use Kurufootwear\Migration\Setup\Migration\EnableAmastyAjax;
use Kurufootwear\Migration\Setup\Migration\ErrorPageMigration;
use Kurufootwear\Migration\Setup\Migration\ExpertsCornerBlockMigration;
use Kurufootwear\Migration\Setup\Migration\FilterableAttributesUpdateMigration;
use Kurufootwear\Migration\Setup\Migration\HeaderPromoBlockMigration;
use Kurufootwear\Migration\Setup\Migration\HelpMenuLogoMediaMigration;
use Kurufootwear\Migration\Setup\Migration\InnovationPageMigration;
use Kurufootwear\Migration\Setup\Migration\MinicartShippingPromoBlockMigration;
use Kurufootwear\Migration\Setup\Migration\PlantarFasciitisBlockMigration;
use Kurufootwear\Migration\Setup\Migration\PromoBlocksMigration;
use Kurufootwear\Migration\Setup\Migration\DevGuidelinesMigration;
use Kurufootwear\Migration\Setup\Migration\FooterBlocksMigration;
use Kurufootwear\Migration\Setup\Migration\HeaderMigration;
use Kurufootwear\Migration\Setup\Migration\ReviewsPageMigration;
use Kurufootwear\Migration\Setup\Migration\RmaFieldMigration;
use Kurufootwear\Migration\Setup\Migration\SizeChartMigration;
use Kurufootwear\Migration\Setup\Migration\SocialLinkBlockMigration;
use Kurufootwear\Migration\Setup\Migration\SocksAttributeSetMigration;
use Kurufootwear\Migration\Setup\Migration\StoreConfiguration;
use Kurufootwear\Migration\Setup\Migration\StoreInfoMigration;
use Kurufootwear\Migration\Setup\Migration\StoreThemeMigration;
use Kurufootwear\Migration\Setup\Migration\HpSliderMigration;
use Kurufootwear\Migration\Setup\Migration\LightweightBlockMigration;
use Kurufootwear\Migration\Setup\Migration\RevoHpSliderMigration;
use Kurufootwear\Migration\Setup\Migration\TechnologyPageMigration;
use Kurufootwear\Migration\Setup\Migration\UniversalTemplatePageMigration;
use Kurufootwear\Migration\Setup\Migration\WhyKuruPageMigration;
use Kurufootwear\Migration\Setup\Migration\WishlistConfigMigration;
use Kurufootwear\Migration\Setup\Migration\YotpoAccessMigration;
use Kurufootwear\Migration\Setup\Migration\YotpoReviewsBlockMigration;
use Kurufootwear\Migration\Setup\Migration\YotpoUGCBlockMigration;
use Kurufootwear\Migration\Setup\Migration\SalespointsBlockMigration;
use Kurufootwear\Migration\Setup\Migration\LoaderMigration;
use Kurufootwear\Migration\Setup\Migration\TechnologyBlockMigration;
use Kurufootwear\Migration\Setup\Migration\ProductDuplicationCSVMigration;
use Kurufootwear\Migration\Setup\Migration\SizeChartBlockMigration;
use Kurufootwear\Migration\Setup\Migration\ContactBlockMigration;
use Kurufootwear\Migration\Setup\Migration\ContactInfoBlocksMigration;
use Kurufootwear\Migration\Setup\Migration\RelatedSKUsMigration;
use Kurufootwear\Migration\Setup\Migration\RelatedSKUsMigrationData;
use Kurufootwear\Migration\Setup\Migration\RemoveNewsroomBlockMigration;
use Kurufootwear\Migration\Setup\Migration\ZendeskMigration;
use Kurufootwear\Migration\Setup\Migration\RmaOrderStatusMigration;
use Kurufootwear\Migration\Setup\Migration\RewardsRuleMigration;
use Kurufootwear\Migration\Setup\Migration\SwatchMigration;
use Kurufootwear\Migration\Setup\Migration\ProductImagesMigration;
use Kurufootwear\Migration\Setup\Migration\CategoryMenuBlockMigration;
use Kurufootwear\Migration\Setup\Migration\NewAttributesNameAndColor;
use Kurufootwear\Migration\Setup\Migration\UnsubscribePageMigration;
use Kurufootwear\Migration\Setup\Migration\RevertBackTo217;
use Kurufootwear\Migration\Setup\Migration\RemoveIncorrectGuestRewards;
use Kurufootwear\Migration\Setup\Migration\SubscriptionRewardsRuleMigration;
use Kurufootwear\Migration\Setup\Migration\CreateGuestOrderStatusPage;
use Magento\Framework\Exception\LocalizedException;
use Magento\Framework\ObjectManagerInterface;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\ModuleDataSetupInterface;
use Magento\Framework\Setup\UpgradeDataInterface;
use Magento\Framework\App\State;
use Traversable;

class UpgradeData extends AbstractSetup implements UpgradeDataInterface
{
    /**
     * Migration list.
     *
     * @var array
     */
    private $migrations = array(
        '0.1.0' => StoreThemeMigration::class,
        '0.1.1' => DevGuidelinesMigration::class,
        '0.1.2' => DevGuidelinesMigration::class,
        '0.2.0' => HpSliderMigration::class,
        '0.2.2' => YotpoReviewsBlockMigration::class,
        '0.2.3' => RevoHpSliderMigration::class,
        '0.2.5' => CopyrightBlockMigration::class,
        '0.2.7' => YotpoReviewsBlockMigration::class,
        '0.2.10' => PromoBlocksMigration::class,
        '0.2.12' => CopyrightBlockMigration::class,
        '0.2.14' => EnableAmastyAjax::class,
        '0.2.17' => SizeChartMigration::class,
        '0.2.19' => LoaderMigration::class,
        '0.2.20' => WishlistConfigMigration::class,
        '0.2.24' => CheckoutImageMigration::class,
        '0.2.25' => ErrorPageMigration::class,
        '0.2.26' => ContactBlockMigration::class,
        '0.2.27' => SocialLinkBlockMigration::class,
        '0.2.28' => ContactInfoBlocksMigration::class,
        '0.2.29' => TechnologyPageMigration::class,
        '0.2.30' => ErrorPageMigration::class,
        '0.2.34' => RmaFieldMigration::class,
        '0.2.35' => RelatedSKUsMigration::class,
        '0.2.38' => RemoveNewsroomBlockMigration::class,
        '0.2.39' => FilterableAttributesUpdateMigration::class,
        '0.2.43' => ZendeskMigration::class,
        '0.2.44' => RmaOrderStatusMigration::class,
        '0.2.55' => SalespointsBlockMigration::class,
        '0.2.57' => SwatchMigration::class,
        '0.2.58' => ReviewsPageMigration::class,
        '0.2.59' => YotpoReviewsBlockMigration::class,
        '0.2.66' => CategoryLayoutMigration::class,
        '0.2.70' => HeaderPromoBlockMigration::class,
        '0.2.92' => ExpertsCornerBlockMigration::class,
        '0.2.95' => AmastyShippingRateMigration::class,
        '0.2.97' => EmailTemplatesMigration::class,
        '0.2.98' => EmailTemplatesConfigMigration::class,
        '0.2.100' => YotpoUGCBlockMigration::class,
        '0.2.101' => LightweightBlockMigration::class,
        '0.2.102' => PromoBlocksMigration::class,
        '0.2.105' => NewAttributesNameAndColor::class,
//        '0.2.106' => RelatedSKUsMigrationData::class,
//        '0.2.107' => ProductDuplicationCSVMigration::class,
//        '0.2.109' => ProductImagesMigration::class, // This has to execute after ProductDuplicationCSVMigration
        '0.2.111' => RewardsRuleMigration::class,
        '0.2.112' => PlantarFasciitisBlockMigration::class,
        '0.2.116' => InnovationPageMigration::class,
        '0.2.117' => UniversalTemplatePageMigration::class,
        '0.2.125' => MinicartShippingPromoBlockMigration::class,
        '0.2.127' => AmastyFilterSettingsMigration::class,
        '0.2.130' => ExpertsCornerBlockMigration::class,
//        '0.2.135' => SizeChartBlockMigration::class,
        '0.2.149' => CategoryMenuBlockMigration::class,
        '0.2.151' => ContactInfoBlocksMigration::class,
        '0.2.154' => HpSliderMigration::class,
        '0.2.156' => WhyKuruPageMigration::class,
        '0.2.159' => BestWalkingShoesBlockMigration::class,
        '0.2.160' => StoreConfiguration::class,
        '0.2.161' => RevertBackTo217::class,
        '0.2.162' => DuplicateConfigurationMigration::class,
        '0.2.166' => RemoveIncorrectGuestRewards::class,
        '0.2.167' => TechnologyBlockMigration::class,
        '0.2.168' => FooterBlocksMigration::class,
        '0.2.169' => SizeChartBlockMigration::class,
        '0.2.170' => CategoryCMSBlocksMigration::class,
        '0.2.173' => BenefitsMigration::class,
        '0.2.174' => SocksAttributeSetMigration::class,
        '0.2.175' => CategoryCMSBlocksMigration::class,
        '0.2.176' => StoreInfoMigration::class,
        '0.2.177' => UnsubscribePageMigration::class,
        '0.2.178' => SizeChartBlockMigration::class,
        '0.2.179' => CategoryCMSBlocksMigration::class,
        '0.2.180' => SubscriptionRewardsRuleMigration::class,
        '0.2.181' => HelpMenuLogoMediaMigration::class,
        '0.2.182' => CreateGuestOrderStatusPage::class,
        '0.2.183' => HeaderMigration::class,
        '0.2.184' => SalespointsBlockMigration::class,
    );

    /**
     * @var ObjectManagerInterface
     */
    private $objectManager;

    /**
     * UpgradeData constructor.
     *
     * @param ObjectManagerInterface $objectManager
     * @param State $state
     */
    public function __construct(ObjectManagerInterface $objectManager, State $state)
    {
        // Set area code if not already set.
        try {
            $state->getAreaCode();
        } catch (LocalizedException $e) {
            $state->setAreaCode('adminhtml');
        }

        $this->objectManager = $objectManager;
        // Enable module version output
        $this->setOutputEnabled(true);
    }

    /**
     * @inheritDoc
     */
    public function upgrade(ModuleDataSetupInterface $setup, ModuleContextInterface $context)
    {
        $setup->startSetup();
        $this->context = $context;

        /** @var UpgradeMigrationInterface $migration */
        foreach ($this->getMigrations() as $version => $migration) {
            if ($this->getVersion($version)) {
                $migration->apply($setup);
            }
        }

        $setup->endSetup();
    }

    /**
     * @return Traversable
     * @throws LocalizedException
     */
    private function getMigrations(): Traversable
    {
        foreach ($this->migrations as $version => $migration) {
            $migration = $this->objectManager->get($migration);
            if (!$migration instanceof UpgradeMigrationInterface) {
                throw new LocalizedException(
                    __('%1 must implement %2', get_class($migration), UpgradeMigrationInterface::class)
                );
            }

            yield $version => $migration;
        }
    }
}
