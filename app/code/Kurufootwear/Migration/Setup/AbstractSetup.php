<?php
/**
 * @category  Kurufootwear
 * @package   Kurufootwear\Migration
 * @author    Viesturs Ruzans <viestursr@scandiweb.com>
 * @copyright Copyright (c) 2017 Scandiweb, Inc (http://scandiweb.com)
 * @license   http://opensource.org/licenses/OSL-3.0 The Open Software License 3.0 (OSL-3.0)
 */

namespace Kurufootwear\Migration\Setup;

use Magento\Framework\Setup\ModuleContextInterface;

abstract class AbstractSetup
{
    /**
     * @var ModuleContextInterface
     */
    protected $context;

    /**
     * @var bool
     */
    private $outputEnabled = false;

    /**
     * Check if provided version is the current module version.
     *
     * @param string $version
     *
     * @return bool
     */
    public function getVersion($version)
    {
        if (version_compare($this->context->getVersion(), $version, '<')) {
            // Print out version number in console
            if ($this->outputEnabled) {
                echo PHP_EOL . "\033[35mKurufootwear migration version " . $version . " \033[0m" . PHP_EOL;
            }

            return true;
        }

        return false;
    }

    /**
     * Enable/disable module version output.
     *
     * @param bool $value
     */
    protected function setOutputEnabled($value)
    {
        $this->outputEnabled = $value;
    }
}
