<?php
/**
 * @category  Kurufootwear
 * @package   Kurufootwear\Migration
 * @copyright Copyright (c) 2017 Scandiweb, Inc (http://scandiweb.com)
 * @license   http://opensource.org/licenses/OSL-3.0 The Open Software License 3.0 (OSL-3.0)
 */

namespace Kurufootwear\Migration\Setup;

use Kurufootwear\Migration\Api\UpgradeMigrationInterface;
use Kurufootwear\Migration\Setup\Migration\MenumanagerDbMigration;
use Kurufootwear\Migration\Setup\Migration\SizeChartMigration;
use Magento\Framework\Exception\LocalizedException;
use Magento\Framework\ObjectManagerInterface;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\UpgradeSchemaInterface;
use Magento\Framework\Setup\SchemaSetupInterface;
use Traversable;

class UpgradeSchema extends AbstractSetup implements UpgradeSchemaInterface
{
    /**
     * Migration list.
     *
     * @var array
     */
    private $migrations = [
        '0.1.3' => MenumanagerDbMigration::class
    ];

    /**
     * @var ObjectManagerInterface
     */
    private $objectManager;

    /**
     * UpgradeSchema constructor.
     *
     * @param ObjectManagerInterface $objectManager
     */
    public function __construct(ObjectManagerInterface $objectManager)
    {
        $this->objectManager = $objectManager;
        // Enable module version output
        $this->setOutputEnabled(true);
    }

    /**
     * @inheritDoc
     */
    public function upgrade(SchemaSetupInterface $setup, ModuleContextInterface $context)
    {
        $setup->startSetup();
        $this->context = $context;

        /** @var UpgradeMigrationInterface $migration */
        foreach ($this->getMigrations() as $version => $migration) {
            if ($this->getVersion($version)) {
                $migration->apply($setup);
            }
        }

        $setup->endSetup();
    }

    /**
     * @return Traversable
     * @throws LocalizedException
     */
    private function getMigrations(): Traversable
    {
        foreach ($this->migrations as $version => $migration) {
            $migration = $this->objectManager->get($migration);
            if (!$migration instanceof UpgradeMigrationInterface) {
                throw new LocalizedException(
                    __('%1 must implement %2', get_class($migration), UpgradeMigrationInterface::class)
                );
            }

            yield $version => $migration;
        }
    }
}
