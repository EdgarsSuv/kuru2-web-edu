<?php
/**
 * @vendor      Kurufootwear
 * @module      Kurufootwear_Migration
 * @author      Viktors Vipolzovs <info@scandiweb.com>
 * @copyright   Copyright (c) 2017 Scandiweb, Ltd (http://scandiweb.com)
 * @license     http://opensource.org/licenses/afl-3.0.php Academic Free License (AFL 3.0)
 */
namespace Kurufootwear\Migration\Setup\Migration;

use Kurufootwear\Migration\Api\UpgradeMigrationInterface;
use Magento\Framework\Setup\SetupInterface;
use Kurufootwear\Migration\Helper\FileParser;
use Magento\Cms\Model\BlockFactory;

class ExpertsCornerBlockMigration implements UpgradeMigrationInterface
{
    /**
     * @var BlockFactory
     */
    private $blockFactory;

    /**
     * @var FileParser
     */
    private $fileParser;

    /**
     * SidebarBlockMigration constructor
     *
     * @param BlockFactory $blockFactory
     * @param FileParser $fileParser
     */
    public function __construct(
        BlockFactory $blockFactory,
        FileParser $fileParser
    ) {
        $this->blockFactory = $blockFactory;
        $this->fileParser = $fileParser;
    }

    /**
     * @inheritDoc
     */
    public function apply(SetupInterface $setup = null)
    {
        $cmsBlocks = [
            [
                'file_name' => 'experts_corner.html',
                'title' => 'Experts Corner',
                'identifier' => 'experts-corner'
            ],
            [
                'file_name' => 'experts_corner_with_side_menu.html',
                'title' => 'Experts Corner with static CMS sidebar',
                'identifier' => 'experts-corner-with-side-menu'
            ]
        ];

        foreach ($cmsBlocks as $cmsBlock) {
            $content = '';

            if ($cmsBlock['file_name'] != '') {
                $content = $this->fileParser->getHtmlContent('blocks/' . $cmsBlock['file_name']);
            }

            $blockData = [
                'title' => $cmsBlock['title'],
                'identifier' => $cmsBlock['identifier'],
                'content' => $content,
                'stores' => [0]
            ];

            $block = $this->blockFactory->create()->load($blockData['identifier'], 'identifier');

            if ($block->getId()) {
                $block->setTitle($blockData['title'])
                    ->setContent($blockData['content'])
                    ->setStores($blockData['stores'])
                    ->save();
            } else {
                $block->addData($blockData)->save();
            }
        }
    }
}
