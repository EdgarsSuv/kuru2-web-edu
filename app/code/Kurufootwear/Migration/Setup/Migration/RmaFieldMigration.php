<?php
/**
 * @category  Kurufootwear
 * @package   Kurufootwear\Migration
 * @author    Krisjanis Veinbahs <info@scandiweb.com>
 * @copyright Copyright (c) 2017 Scandiweb, Inc (http://scandiweb.com)
 * @license   http://opensource.org/licenses/OSL-3.0 The Open Software License 3.0 (OSL-3.0)
 */
namespace Kurufootwear\Migration\Setup\Migration;

use Aheadworks\Rma\Model\Source\CustomField\Refers;
use Kurufootwear\Migration\Api\UpgradeMigrationInterface;
use Kurufootwear\Rma\Model\CustomFieldFactory;
use Kurufootwear\Rma\Model\Source\CustomField\Type;
use Magento\Framework\Setup\SetupInterface;
use Magento\Store\Api\StoreRepositoryInterface;

class RmaFieldMigration implements UpgradeMigrationInterface
{
    /**
     * @var CustomFieldFactory
     */
    protected $customField;

    /**
     * @var StoreRepositoryInterface
     */
    protected $storeRepository;

    /**
     * Reason question keys
     */
    const TOO_LARGE = 'too_large';
    const TOO_SMALL = 'too_small';
    const TOO_WIDE = 'too_wide';
    const TOO_NARROW = 'too_narrow';
    const NO_COLOR = 'no_color';
    const NO_STYLE = 'no_style';
    const REASON_OTHER = 'reason_other';
    const TYPE_EXCHANGE = 'exchange';
    const TYPE_RETURN = 'return';

    /**
     * Rma survey questions
     * @var array
     */
    public static $fields = [
        'reason' => [
            'title' => 'Reason',
            'required' => true,
            'type' => Type::RADIO_BUTTON_VALUE,
            'refers' => Refers::ITEM_VALUE,
            'choices' => [
                self::TOO_LARGE => 'Length too large',
                self::TOO_SMALL => 'Length too small',
                self::TOO_WIDE   => 'Width too large',
                self::TOO_NARROW => 'Width too small',
                self::NO_COLOR => 'Unhappy with color',
                self::NO_STYLE => 'Unhappy with style',
                self::REASON_OTHER => 'Other'
            ],
            'override' => true
        ],
        'other_description' => [
            'title' => 'Other Description',
            'required' => false,
            'type' => Type::TEXT_VALUE,
            'refers' => Refers::ITEM_VALUE
        ],
        'type' => [
            'title' => 'Type',
            'required' => true,
            'type' => Type::SELECT_VALUE,
            'refers' => Refers::REQUEST_VALUE,
            'choices' => [
                self::TYPE_EXCHANGE => 'Exchange',
                self::TYPE_RETURN => 'Return'
            ]
        ],
        'admin_notes' => [
            'title' => 'Admin Notes',
            'required' => false,
            'type' => Type::TEXT_AREA_VALUE,
            'refers' => Refers::REQUEST_VALUE,
            'view' => [],
            'display_on_shipping' => false
        ]
    ];

    /**
     * RmaQuestionMigration constructor.
     * @param CustomFieldFactory $customField
     * @param StoreRepositoryInterface $storeRepository
     */
    public function __construct(
        CustomFieldFactory $customField,
        StoreRepositoryInterface $storeRepository
    )
    {
        $this->customField = $customField;
        $this->storeRepository = $storeRepository;
    }


    /**
     * Migrate rma questions and generate additional
     * default question values
     * @param SetupInterface|null $setup
     */
    public function apply(SetupInterface $setup = null)
    {
        $adminStoreId = $this->storeRepository->get('admin')->getId();
        $defaultStoreId = $this->storeRepository->get('default')->getId();
    
        $rmaStatuses = $setup->getTable('aw_rma_request_status');
        $select = $setup->getConnection()->select()->from(
            $rmaStatuses,
            ['COUNT(*)']
        );
    
        $statusCount = $setup->getConnection()->fetchOne($select);
        $statusPermissions = array();
        for ($i = 0; $i < $statusCount; $i++) {
            $statusPermissions[$i] = $i + 1;
        }
    
        $defaults = array(
            'edit' => array(0 => '-1'),
            'view' => $statusPermissions,
            'admin_edit' => $statusPermissions,
            'display_on_shipping' => true
        );

        foreach (self::$fields as $identifier => $field) {
            $customField = $this->customField->create()->load($identifier);
            
            /**
             * Only migrate new fields
             */
            if ($customField->getIdentifier() == '' || (isset($field['override']) && $field['override'] == true)) {
                
                if (isset($field['override']) && $field['override'] == true) {
                    /** 
                     * Delete the existing field so it can be recreated
                     */
                    $connection = $setup->getConnection();
                    
                    // Delete the options
                    $options = $customField->getOption();
                    $customFieldOptionTable = $setup->getTable('aw_rma_custom_field_option');
                    $where = ['field_id = ?' => (int)$customField->getId()];
                    $connection->delete($customFieldOptionTable, $where);
                    
                    $customFieldOptionValueTable = $setup->getTable('aw_rma_custom_field_option_value');
                    foreach ($options['value'] as $id => $labels) {
                        $where = ['option_id = ?' => (int)$id];
                        $connection->delete($customFieldOptionValueTable, $where);
                    }
                    
                    // Delete the field
                    $customFieldTable = $setup->getTable('aw_rma_custom_field');
                    $where = ['id = ?' => (int)$customField->getId()];
                    $connection->delete($customFieldTable, $where);
    
                    $customField = $this->customField->create();
                }
                
                /**
                 * Set generic question info
                 */
                $customField
                    ->setIdentifier($identifier)
                    ->setName($field['title'])
                    ->setType($field['type'])
                    ->setRefers($field['refers'])
                    ->setIsRequired($field['required'])
                    ->setAttribute([
                        'frontend_label' => [
                            $defaultStoreId => $field['title']
                        ]
                    ]);

                /**
                 * Try to set options
                 */
                if (isset($field['choices'])) {
                    $options = [
                        'default' => [],
                        'enable' => [],
                        'order' => [],
                        'value' => []
                    ];

                    foreach ($field['choices'] as $choiceKey => $choiceValue) {
                        $options['enable'][] = $choiceKey;
                        $options['order'][$choiceKey] = 0;
                        $options['value'][$choiceKey] = [
                            $adminStoreId => $choiceValue,
                            $defaultStoreId => $choiceValue
                        ];
                    }

                    $customField->setOption($options);
                }
                
                /**
                 * Set additional options that should be
                 * the same for all questions
                 */
                $values = $defaults;
                if (isset($field['view'])) {
                    $values['view'] = $field['view'];
                }
                if (isset($field['edit'])) {
                    $values['edit'] = $field['edit'];
                }
                if (isset($field['admin_edit'])) {
                    $values['admin_edit'] = $field['admin_edit'];
                }
                if (isset($field['display_on_shipping'])) {
                    $values['display_on_shipping'] = $field['display_on_shipping'];
                }
                
                $customField
                    ->setIsDisplayInLabel($defaults['display_on_shipping'])
                    ->setWebsiteIds([$defaultStoreId])
                    ->setEditableForStatusIds($values['edit'])
                    ->setVisibleForStatusIds($values['view'])
                    ->setEditableAdminForStatusIds($values['admin_edit']);

                $customField->save();
            }
        }
    }
}
