<?php
/**
 * @category  Kurufootwear
 * @package   Kurufootwear\Migration
 * @author    Krišjānis Veinbahs <krisjanisv@scandiweb.com>
 * @copyright Copyright (c) 2017 Scandiweb, Inc (http://scandiweb.com)
 * @license   http://opensource.org/licenses/OSL-3.0 The Open Software License 3.0 (OSL-3.0)
 */
namespace Kurufootwear\Migration\Setup\Migration;

use Amasty\ShippingTableRates\Model\MethodFactory;
use Amasty\ShippingTableRates\Model\RateFactory;
use Kurufootwear\Migration\Api\UpgradeMigrationInterface;
use Magento\Framework\Setup\SetupInterface;

class AmastyShippingRateMigration implements UpgradeMigrationInterface
{
    /**
     * @var MethodFactory
     */
    protected $method;

    /**
     * @var RateFactory
     */
    protected $rate;

    /**
     * Shipping rate info
     * method_name => [rate_data]
     * @var array
     */
    protected $shippingRates = [
        'Overnight' => [
            'timeDelivery' => 1,
            'costBase' => 37,
            'country' => 'US',
            'priceFrom' => 0,
            'priceTo' => 99999999,
            'weightFrom' => 0,
            'weightTo' => 99999999,
            'qtyFrom' => 0,
            'qtyTo' => 99999999
        ],
        '2-Day' => [
            'timeDelivery' => 1,
            'costBase' => 25,
            'country' => 'US',
            'priceFrom' => 0,
            'priceTo' => 99999999,
            'weightFrom' => 0,
            'weightTo' => 99999999,
            'qtyFrom' => 0,
            'qtyTo' => 99999999
        ]
    ];

    /**
     * AmastyShippingRateMigration constructor.
     * @param MethodFactory $method
     * @param RateFactory $rate
     */
    public function __construct(
        MethodFactory $method,
        RateFactory $rate
    )
    {
        $this->method = $method;
        $this->rate = $rate;
    }

    /**
     * Create or update shipping methods
     * @param SetupInterface|null $setup
     */
    public function apply(SetupInterface $setup = null)
    {
        foreach ($this->shippingRates as $methodName => $rateData) {
            /**
             * Load existing method
             */
            $methodModel = $this->method->create()->getCollection()
                ->addFieldToFilter('name', $methodName)
                ->getFirstItem();

            /**
             * Create new method if load failed
             */
            if (!$methodModel->getId()) {
                $methodModel = $this->method->create()
                    ->setIsActive(true)
                    ->setName($methodName)
                    ->save();
            }

            /**
             * Load existing rate
             */
            $rateModel = $this->rate->create()->getCollection()
                ->addFieldToFilter('method_id', $methodModel->getId())
                ->getFirstItem();

            /**
             * Create new rate if load failed
             */
            if (!$rateModel->getId()) {
                $rateModel = $this->rate->create()->setMethodId($methodModel->getId());
            }

            $rateModel->setTimeDelivery($rateData['timeDelivery'])
                ->setCostBase($rateData['costBase'])
                ->setCountry($rateData['country'])
                ->setPriceFrom($rateData['priceFrom'])
                ->setPriceTo($rateData['priceTo'])
                ->setWeightFrom($rateData['weightFrom'])
                ->setWeightTo($rateData['weightTo'])
                ->setQtyFrom($rateData['qtyFrom'])
                ->setQtyTo($rateData['qtyTo'])
                ->save();
        }
    }
}
