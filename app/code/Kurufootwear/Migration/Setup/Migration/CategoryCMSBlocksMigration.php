<?php
/**
 * @vendor      Kurufootwear
 * @module      Kurufootwear_Migration
 * @author      Jelena Sorohova <info@scandiweb.com>
 * @copyright   Copyright (c) 2017 Scandiweb, Ltd (http://scandiweb.com)
 * @license     http://opensource.org/licenses/afl-3.0.php Academic Free License (AFL 3.0)
 */
namespace Kurufootwear\Migration\Setup\Migration;

use Kurufootwear\Migration\Api\UpgradeMigrationInterface;
use Magento\Framework\Setup\SetupInterface;
use Magento\Framework\Filesystem;
use Magento\Framework\Filesystem\Driver\File;
use Kurufootwear\Migration\Helper\FileParser;
use Magento\Cms\Model\BlockFactory;
use Kurufootwear\Migration\Helper\FileHelper;

class CategoryCMSBlocksMigration implements UpgradeMigrationInterface
{
    /**
     * @var BlockFactory
     */
    private $blockFactory;

    /**
     * @var FileParser
     */
    private $fileParser;

    /**
     * @var FileHelper
     */
    private $fileHelper;

    /**
     * CategoryCMSBlocksMigration constructor.
     * @param BlockFactory $blockFactory
     * @param FileParser $fileParser
     */
    public function __construct(
        BlockFactory $blockFactory,
        FileParser $fileParser,
        FileHelper $fileHelper
    ) {
        $this->blockFactory = $blockFactory;
        $this->fileParser = $fileParser;
        $this->fileHelper = $fileHelper;
    }

    /**
     * @inheritDoc
     */
    public function apply(SetupInterface $setup = null)
    {
        $this->fileHelper->copyMediaFiles(['register-your-purchase.png'], 'wysiwyg');
        $this->fileHelper->copyMediaFiles(['caduceus-support-mobile.jpg'], 'wysiwyg');
        $this->fileHelper->copyMediaFiles(['leaves-support-mobile.jpg'], 'wysiwyg');
        $this->fileHelper->copyMediaFiles(['feet-support-mobile.jpg'], 'wysiwyg');
        $this->fileHelper->copyMediaFiles(['feet-and-dandelions.jpg'], 'wysiwyg');

        $cmsBlocks = array(
            array(
                'file_name' => 'shoes-for-plantar-fasciitis-responsive.html',
                'identifier' => 'shoes-for-plantar-fasciitis-responsive'
            ),
            array(
                'file_name' => 'flat-feet-support.html',
                'identifier' => 'flat-feet-support'
            ),
            array(
                'file_name' => 'foot-pain.html',
                'identifier' => 'foot-pain'
            ),
            array(
                'file_name' => 'heel-pain.html',
                'identifier' => 'heel-pain'
            ),
            array(
                'file_name' => 'nurse-shoes.html',
                'identifier' => 'nurse-shoes'
            ),
            array(
                'file_name' => 'shoes-for-hiking.html',
                'identifier' => 'shoes-for-hiking'
            ),
            array(
                'file_name' => 'shoes-for-standing.html',
                'identifier' => 'shoes-for-standing'
            ),
            array(
                'file_name' => 'orthopedic-shoes.html',
                'identifier' => 'orthopedic-shoes'
            ),
            array(
                'file_name' => 'shoes-for-heel-spurs.html',
                'identifier' => 'shoes-for-heel-spurs'
            ),
            array(
                'file_name' => 'legal-privacy-policy.html',
                'identifier' => 'legal-privacy-policy'
            ),
            array(
                'file_name' => 'ra.html',
                'identifier' => 'ra'
            ),
            array(
                'file_name' => 'register.html',
                'identifier' => 'register'
            ),
            array(
                'file_name' => 'returns-and-exchanges.html',
                'identifier' => 'returns-and-exchanges'
            ),
            array(
                'file_name' => 'shipping-and-delivery.html',
                'identifier' => 'shipping-and-delivery'
            ),
            array(
                'file_name' => 'warranty.html',
                'identifier' => 'warranty'
            ),
            array(
                'file_name' => 'reward-points-loyalty.html',
                'identifier' => 'reward-points-loyalty'
            ),
            array(
                'file_name' => 'sustainability.html',
                'identifier' => 'sustainability'
            ),
            array(
                'file_name' => 'travel-shoes.html',
                'identifier' => 'travel-shoes'
            ),
            array(
                'file_name' => 'experts-corner-wide-shoes.html',
                'identifier' => 'wide-shoes'
            ),
            array(
                'file_name' => 'support-shoes.html',
                'identifier' => 'support-shoes'
            ),
            array(
                'file_name' => 'experts_corner_work_shoes.html',
                'identifier' => 'work-shoes'
            )
        );

        foreach ($cmsBlocks as $cmsBlock) {
            $content = '';

            if ($cmsBlock['file_name'] != '') {
                $content = $this->fileParser->getHtmlContent('blocks/' . $cmsBlock['file_name']);
            }

            $blockData = array(
                'identifier' => $cmsBlock['identifier'],
                'content' => $content,
                'stores' => array(0)
            );

            $block = $this->blockFactory->create()->load($blockData['identifier'], 'identifier');

            if ($block->getId()) {
                $block->setContent($blockData['content'])
                    ->setStores($blockData['stores'])
                    ->save();
            } else {
                $block->addData($blockData)
                    ->save();
            }
        }
    }
}
