<?php
/**
 * @category  Kurufootwear
 * @package   Kurufootwear\Migration
 * @author    Krišjānis Veinbahs <krisjanisv@scandiweb.com>
 * @copyright Copyright (c) 2017 Scandiweb, Inc (http://scandiweb.com)
 * @license   http://opensource.org/licenses/OSL-3.0 The Open Software License 3.0 (OSL-3.0)
 */
namespace Kurufootwear\Migration\Setup\Migration;

use Magento\Catalog\Model\Category;
use Magento\Catalog\Model\CategoryFactory;
use Magento\Catalog\Model\CategoryRepository;
use Magento\Catalog\Model\ResourceModel\Category\CollectionFactory;
use Magento\Framework\Setup\SetupInterface;
use Kurufootwear\Migration\Api\UpgradeMigrationInterface;
use Magento\Store\Model\StoreManagerInterface;
use Magento\Theme\Model\Theme;

class CategoryLayoutMigration implements UpgradeMigrationInterface
{
    /**
     * @var CollectionFactory
     */
    protected $collectionFactory;

    /**
     * @var CategoryFactory
     */
    protected $categoryFactory;

    /**
     * @var CategoryRepository
     */
    protected $categoryRepository;

    /**
     * @var Theme
     */
    protected $theme;

    /**
     * @var StoreManagerInterface
     */
    private $storeManager;

    /**
     * CategoryLayoutMigration constructor.
     * @param CollectionFactory $collectionFactory
     * @param CategoryFactory $categoryFactory
     * @param CategoryRepository $categoryRepository
     * @param Theme $theme
     * @param StoreManagerInterface $storeManager
     */
    public function __construct(
        CollectionFactory $collectionFactory,
        CategoryFactory $categoryFactory,
        CategoryRepository $categoryRepository,
        Theme $theme,
        StoreManagerInterface $storeManager
    )
    {
        $this->collectionFactory = $collectionFactory;
        $this->categoryFactory = $categoryFactory;
        $this->categoryRepository = $categoryRepository;
        $this->theme = $theme;
        $this->storeManager = $storeManager;
    }

    /**
     * @inheritDoc
     */
    public function apply(SetupInterface $setup = null)
    {
        $themeId = $this->getTheme('Kurufootwear/default')->getId();
        $pageLayout = '1column';
        $collection = $this->collectionFactory->create()
            ->addAttributeToSelect('*')
            ->addIsActiveFilter();

        foreach ($collection as $categoryData) {
            /**
             * @var Category $category
             */
            $category = $this->categoryRepository->get($categoryData->getId());
            $category->setPageLayout($pageLayout)
                ->setCustomDesign((string)$themeId)
                ->save();
        }

        $this->changeCategoryDisplayMode();
    }

    /**
     * @param $themeCode
     * @return \Magento\Framework\DataObject
     */
    protected function getTheme($themeCode)
    {
        $collection = $this->theme->getCollection();
        $collection->addFieldToFilter('code', $themeCode);

        return $collection->getFirstItem();
    }

    /**
     * Changes categories display mode
     */
    public function changeCategoryDisplayMode()
    {
        $stores = $this->storeManager->getWebsites();
        $categoryIds = [
            $this->getCategoryId('Men\'s Shoes'),
            // Mens new arraivals
            220,
            // Mens outdoor shoes
            38,
            // Mens alethic shoes
            40,
            // Mens casual
            58,
            // Mens wide
            238,
            $this->getCategoryId('Women\'s Shoes'),
            // Womens new arraivals
            219,
            // Womens outdoor shoes
            37,
            // Womens alethic shoes
            39,
            // Womens casual
            57,
            // Womens wide
            237
        ];

        foreach ($stores as $store) {
            foreach ($categoryIds as $id) {
                try {
                    $category = $this->categoryRepository->get($id, $store->getId());
                    $category->setDisplayMode(Category::DM_PRODUCT)
                        ->save();
                } catch (\Exception $e) {}
            }
        }
    }

    /**
     * @param $categoryTitle
     * @return null
     */
    public function getCategoryId($categoryTitle)
    {
        $categoryId = null;
        $collection = $this->categoryFactory->create()
            ->getCollection()
            ->addAttributeToFilter('name' ,$categoryTitle)
            ->setPageSize(1);

        if ($collection->getSize()) {
            $categoryId = $collection->getFirstItem()->getId();
        }

        return $categoryId;
    }
}
