<?php

namespace Kurufootwear\Migration\Setup\Migration;

use Kurufootwear\Migration\Api\UpgradeMigrationInterface;
use Kurufootwear\Migration\Helper\FileParser;
use Magento\Framework\Setup\SetupInterface;
use Magento\Config\Model\ResourceModel\Config;
use Magento\Framework\App\Filesystem\DirectoryList;

class StoreConfiguration implements UpgradeMigrationInterface
{
    /** @var FileParser  */
    private $fileParser;
    
    /** @var Config  */
    private $resourceConfig;
    
    /**
     * FooterBlocksMigration constructor.
     * @param FileParser $fileParser
     * @param Config $resourceConfig
     */
    public function __construct(
        FileParser $fileParser,
        Config $resourceConfig
    ) {
        $this->fileParser = $fileParser;
        $this->resourceConfig = $resourceConfig;
    }
    
    public function apply(SetupInterface $setup = null)
    {
        $columns = array(
            'config_id',
            'scope',
            'scope_id',
            'path',
            'value'
        );
        $header = array_flip($columns);
        
        $path = DirectoryList::APP . '/' . FileParser::PATH_TO_DATA . 'csv/config';
        if (is_dir($path)) {
            $flags = \FilesystemIterator::SKIP_DOTS | \FilesystemIterator::UNIX_PATHS;
            $iterator = new \FilesystemIterator($path, $flags);
            
            /** @var \FilesystemIterator $file */
            foreach ($iterator as $file) {
                if ($file->getType() == 'file' && $file->getExtension() == 'csv') {
                    $data = $this->fileParser->getCsvContent('config/' . $file->getFilename());
                    
                    // First row must be the header and must match $columns
                    if ($data[0] === $columns) {
                        for ($row = 1; $row < count($data); $row++) {
                            try {
                                if (is_null($data[$row][$header['value']]) || $data[$row][$header['value']] === 'NULL') {
                                    $data[$row][$header['value']] = '';
                                }
                                
                                $this->resourceConfig->saveConfig(
                                    $data[$row][$header['path']],
                                    $data[$row][$header['value']],
                                    $data[$row][$header['scope']],
                                    $data[$row][$header['scope_id']]
                                );
                            }
                            catch (\Exception $e) {
                                echo PHP_EOL . "\033[35mWarning: Problem occurred parsing csv " . $file->getFilename() . " row $row \033[0m" . PHP_EOL;
                            }
                        }
                    } else {
                        echo PHP_EOL . "\033[35mWarning: Invalid header in " . $file->getFilename() . " \033[0m" . PHP_EOL;
                    }
                }
            }
        }
        
        $this->remove($setup);
    }
    
    public function remove(SetupInterface $setup = null)
    {
        $configsToRemove = [
            [
                'path' => 'crontab/jobs/push_cancelled_orders_to_ns/schedule/cron_expr',
                'scope' => 'default',
                'scope_id' => '0'
            ],
            [
                'path' => 'crontab/jobs/push_orders_to_ns/schedule/cron_expr',
                'scope' => 'default',
                'scope_id' => '0'
            ],
            [
                'path' => 'crontab/jobs/push_orders_to_ns/run/model',
                'scope' => 'default',
                'scope_id' => '0'
            ],
            [
                'path' => 'crontab/jobs/push_cancelled_orders_to_ns/run/model',
                'scope' => 'default',
                'scope_id' => '0'
            ],
        ];
        
        foreach ($configsToRemove as $config) {
            $this->resourceConfig->deleteConfig($config['path'], $config['scope'], $config['scope_id']);
        }
    }
}
