<?php
/**
 * @vendor      Kurufootwear
 * @module      Kurufootwear_Migration
 * @author      Krisjanis Veinbahs <info@scandiweb.com>
 * @copyright   Copyright (c) 2017 Scandiweb, Ltd (http://scandiweb.com)
 * @license     http://opensource.org/licenses/afl-3.0.php Academic Free License (AFL 3.0)
 */
namespace Kurufootwear\Migration\Setup\Migration;

use Kurufootwear\Migration\Api\UpgradeMigrationInterface;
use Magento\Framework\Setup\SetupInterface;
use Magento\Framework\Filesystem;
use Magento\Framework\Filesystem\Driver\File;
use Kurufootwear\Migration\Helper\FileParser;
use Magento\Cms\Model\BlockFactory;

class SalespointsBlockMigration implements UpgradeMigrationInterface
{
    const ICONS_PUBLIC_DIR = 'icons';

    /**
     * @var Filesystem
     */
    private $filesystem;

    /**
     * @var File
     */
    private $fileDriver;

    /**
     * @var BlockFactory
     */
    private $blockFactory;

    /**
     * @var FileParser
     */
    private $fileParser;

    /**
     * FooterBlocksMigration constructor.
     * @param Filesystem $filesystem
     * @param BlockFactory $blockFactory
     * @param FileParser $fileParser
     */
    public function __construct(
        Filesystem $filesystem,
        File $fileDriver,
        BlockFactory $blockFactory,
        FileParser $fileParser
    ) {
        $this->filesystem = $filesystem;
        $this->fileDriver = $fileDriver;
        $this->blockFactory = $blockFactory;
        $this->fileParser = $fileParser;
    }

    /**
     * @inheritDoc
     */
    public function apply(SetupInterface $setup = null)
    {
        $mediaDir = $this->filesystem->getDirectoryWrite('media');
        $mediaDir->create(self::ICONS_PUBLIC_DIR);

        $sourcePath = realpath(__DIR__ . '/../../files/data/media/icons');
        $destinationPath = $mediaDir->getAbsolutePath(self::ICONS_PUBLIC_DIR);

        $filenames = $this->fileDriver->readDirectory($sourcePath);
        foreach ($filenames as $filename) {
            $name = basename($filename);
            $this->fileDriver->copy(
                sprintf('%s/%s', $sourcePath, $name),
                sprintf('%s/%s', $destinationPath, $name)
            );
        }

        $cmsBlocks = [
            [
                'file_name' => 'product_salespoints.html',
                'title' => 'Product salespoints',
                'identifier' => 'product_salespoints'
            ],
            [
                'file_name' => 'product_salespoints_socks.html',
                'title' => 'Product salespoints socks',
                'identifier' => 'product_salespoints_socks'
            ],
            [
                'file_name' => 'product_salespoints_newpdp.html',
                'title' => 'Product salespoints new PDP',
                'identifier' => 'product_salespoints_newpdp'
            ]
        ];

        foreach ($cmsBlocks as $cmsBlock) {
            $content = '';

            if ($cmsBlock['file_name'] != '') {
                $content = $this->fileParser->getHtmlContent('blocks/' . $cmsBlock['file_name']);
            }

            $blockData = [
                'title' => $cmsBlock['title'],
                'identifier' => $cmsBlock['identifier'],
                'content' => $content,
                'stores' => [0]
            ];

            $block = $this->blockFactory->create()->load($blockData['identifier'], 'identifier');

            if ($block->getId()) {
                $block->setTitle($blockData['title'])
                    ->setContent($blockData['content'])
                    ->setStores($blockData['stores'])
                    ->save();
            } else {
                $block->addData($blockData)
                    ->save();
            }
        }
    }
}
