<?php
/**
 * @category  Kurufootwear
 * @package   Kurufootwear\Migration
 * @author    Kristaps Stalidzāns <kristapss@scandiweb.com>
 * @copyright Copyright (c) 2017 Scandiweb, Inc (http://scandiweb.com)
 * @license   http://opensource.org/licenses/OSL-3.0 The Open Software License 3.0 (OSL-3.0)
 */
namespace Kurufootwear\Migration\Setup\Migration;

use Kurufootwear\Migration\Api\UpgradeMigrationInterface;
use Magento\Framework\Setup\SetupInterface;
use Magento\Framework\App\ResourceConnection;

class RevertBackTo217 implements UpgradeMigrationInterface
{
    /**
     * @var ResourceConnection
     */
    private $resourceConnection;

    /**
     * SliderMigration constructor.
     * @param ResourceConnection $resourceConnection
     */
    public function __construct(
        ResourceConnection $resourceConnection
    ) {
        $this->resourceConnection = $resourceConnection;
    }

    public function apply(SetupInterface $setup = null)
    {
        $downgradeModules = [
            'Magento_Catalog' => '2.1.3',
            'Magento_Quote' => '2.0.3',
            'Magento_Usps' => '2.0.0'
        ];
        $connection = $this->resourceConnection->getConnection();
        $tableName = $this->resourceConnection->getTableName('setup_module');
        foreach ($downgradeModules as $moduleName => $version) {
            $connection->update(
                $tableName,
                [
                    'data_version' => $version,
                    'schema_version' => $version
                ],
                [
                    'module = ?' => $moduleName
                ]

            );
        }

        // Need to remove source_id column from eav tables
        $tables = [
            'catalog_product_index_eav',
            'catalog_product_index_eav_idx',
            'catalog_product_index_eav_tmp',
            'catalog_product_index_eav_decimal',
            'catalog_product_index_eav_decimal_idx',
            'catalog_product_index_eav_decimal_tmp',
        ];
        foreach ($tables as $table) {

            $connection->dropIndex($table, $connection->getPrimaryKeyName($table));
            $primaryKeyFields = ['entity_id', 'attribute_id', 'store_id', 'value'];
            $setup->getConnection()->addIndex(
                $table,
                $connection->getIndexName($table, $primaryKeyFields),
                $primaryKeyFields,
                \Magento\Framework\DB\Adapter\AdapterInterface::INDEX_TYPE_PRIMARY
            );
            $connection->dropColumn($table, 'source_id');
        }
    }
}
