<?php
/**
 * @vendor Kurufootwear
 * @module Kurufootwear_Migration
 * @author Jim McGowen <jim@kurufootwear.com>
 * @copyright Copyright (c) 2017 KURU Footwear. All rights reserved.
 */
namespace Kurufootwear\Migration\Setup\Migration;

use Magento\Framework\Setup\SetupInterface;
use Magento\Framework\App\Filesystem\DirectoryList;
use Kurufootwear\Migration\Api\UpgradeMigrationInterface;
use Magento\Catalog\Model\ResourceModel\Product\CollectionFactory as ProductCollectionFactory;
use Kurufootwear\Migration\Logger\ProductImagesLogger;
use Magento\ConfigurableProduct\Api\LinkManagementInterface;
use Magento\Framework\App\ResourceConnection;
use Magento\Catalog\Model\ProductFactory;
use Magento\Catalog\Model\Product\Action;
use Magento\Catalog\Model\Product\Attribute\Source\Status as ProductStatus;
use Kurufootwear\Migration\Helper\FileParser;

/**
 * Prerequisites:
 * The following conditions should be met before executing this script:
 * 
 * All products should be migrated before executing this code.
 * All parent products should have the same images as defined in M1.
 * All duplicate products should be created preferably with no images.
 *   (all images in the duplicate products will be removed prior to moving images from the parent to the duplicate)
 * 
 * pub/media/catalog/product directory should be empty.
 * pub/media/import/thumbnails directory should contain all the new product images (efefef and ffffff images)
 * pub/media/import/catalog/product directory should be a copy from current M1 production
 * 
 * The idea here is to copy to pub/media/catalog/product ONLY those images being used so 
 * as to clean up our mess of a media directory with it's thousands of unused image files.
 * 
 * It is important to note that this script can only be run once. If it is run again, without 
 * restoring the state of the product images prior to the first time this script is run, then 
 * images will be lost! It might be a good idea to backup the following tables before running 
 * this script, then if for any reason you need to run this script again, restore these tables 
 * and run it again. These are the only tables that are affected by this code.
 *
 * catalog_product_entity_media_gallery
 * catalog_product_entity_media_gallery_value
 * catalog_product_entity_media_gallery_value_to_entity
 * 
 * Results are logged in /var/log/product_images_migration.log
 */
class ProductImagesMigration implements UpgradeMigrationInterface
{
    /** @var DirectoryList  */
    private $directoryList;
    
    /** @var ProductCollectionFactory  */
    private $productCollectionFactory;
    
    /** @var ProductImagesLogger  */
    private $logger;
    
    /** @var \Magento\Framework\DB\Adapter\AdapterInterface  */
    private $connection;
    
    /** @var ProductFactory  */
    private $productFactory;
    
    /** @var Action  */
    private $productAction;
    
    /**
     * ProductImagesMigration constructor.
     *
     * @param DirectoryList $directoryList
     * @param ProductCollectionFactory $productCollectionFactory
     * @param ProductImagesLogger $logger
     * @param LinkManagementInterface $linkManagement
     * @param ResourceConnection $resourceConnection
     * @param ProductFactory $productFactory
     * @param Action $productAction
     */
    public function __construct(
        DirectoryList $directoryList,
        ProductCollectionFactory $productCollectionFactory,
        ProductImagesLogger $logger,
        LinkManagementInterface $linkManagement,
        ResourceConnection $resourceConnection,
        ProductFactory $productFactory,
        Action $productAction
    )
    {
        $this->directoryList = $directoryList;
        $this->productCollectionFactory = $productCollectionFactory;
        $this->logger = $logger;
        $this->productFactory = $productFactory;
        $this->productAction = $productAction;
        
        $this->connection = $resourceConnection->getConnection();
    }
    
    /**
     * @param SetupInterface|null $setup
     *
     * @throws \Exception
     */
    public function apply(SetupInterface $setup = null)
    {
        try {
            echo 'Migrating product images... See /var/log/product_images_migration.log';
            $this->logger->addInfo('Starting product image migration.');
            $this->connection->beginTransaction();
            $this->processProductGalleries();
            $this->connection->commit();
            $this->logger->addInfo('Finished product image migration.');
        }
        catch (\Exception $e) {
            $this->connection->rollBack();
            $this->logger->addError('Exception occurred: ' . $e->getMessage());
            throw $e;
        }
    }
    
    /**
     * Moves images from parents to duplicates and adds new thumbnail images to parents, duplicates, and children.
     */
    private function processProductGalleries()
    {
        // Get all parent products
        $parents = $this->productCollectionFactory->create()
            ->addAttributeToSelect('parent_sku')
            ->addAttributeToSelect('status')
            ->addFieldToFilter('type_id', 'configurable')
            ->addAttributeToFilter('product_duplicate', 0);
            //->addFieldToFilter('entity_id', '1213'); // For testing single parent
            //->addFieldToFilter('sku', '1001');
    
        /** @var \Magento\Catalog\Model\Product $parent */
        foreach ($parents as $parent) {
            
            // Get all duplicate products fro this parent
            $duplicates = $this->productCollectionFactory->create()
                ->addAttributeToSelect('parent_sku')
                ->addAttributeToSelect('status')
                ->addFieldToFilter('type_id', 'configurable')
                ->addFieldToFilter('product_duplicate', '1')
                ->addAttributeToFilter('parent_sku', $parent->getSku());
    
            $this->logger->addInfo('Found ' . count($duplicates) . ' duplicates for parent product ' . $parent->getSku());
            
            // Get parent images
            $parentImages = $this->getGalleryImages($parent);
            
            $fistDuplicateSku = '';
            $imagesMoved = [];
            
            /** @var \Magento\Catalog\Model\Product $duplicate */
            foreach ($duplicates as $duplicate) {
                $statusMsg = $duplicate->getStatus() == ProductStatus::STATUS_DISABLED ? 'disabled' : 'enabled';
                $this->logger->addInfo('Processing ' . $statusMsg . ' duplicate ' . $duplicate->getSku());
                
                $this->removeAllGalleryImages($duplicate);
                
                // Add the thumbnails to the duplicate
                $missingThumbnail = $this->addThumbnails($duplicate->getSku(), $duplicate);
    
                if (empty($fistDuplicateSku) && !$missingThumbnail) {
                    $fistDuplicateSku = $duplicate->getSku();
                }
                
                // Move parent images for this duplicate
                $moveCount = 0;
                foreach ($parentImages as $image) {
                    // Matches on the exact sku (case insensitive) or the integer only portion of the sku
                    if (stristr($image['file'], $duplicate->getSku()) || strstr($image['file'], (int)$duplicate->getSku())) {
                        if ($this->copyMediaFile($image['file'])) {
                            $this->moveGalleryImage($image['image_id'], $duplicate);
                            $imagesMoved[] = $image;
                            $moveCount++;
                        }
                        else {
                            $this->logger->addError('Missing media file ' . $image['file'] . ' Parent SKU: ' . $parent->getSku() . ' Duplicate SKU: ' . $duplicate->getSku());
                            // TODO: Remove missing gallery image from parent if the file is missing?
                        }
                    }
                }
                
                if ($moveCount == 0) {
                    $this->logger->addError('No images were moved from parent for ' . $statusMsg . ' duplicate ' . $duplicate->getSku());
                }
                
                // Get all child products for this duplicate
                $children = $this->productCollectionFactory->create()
                    ->addAttributeToSelect('parent_sku')
                    ->addFieldToFilter('type_id', 'simple')
                    ->addAttributeToFilter('parent_sku', $duplicate->getSku());
                
                foreach ($children as $child) {
                    $this->removeAllGalleryImages($child);
                    
                    if (!$missingThumbnail) {
                        // Add the thumbnails to the child
                        $this->addThumbnails($duplicate->getSku(), $child);
                    }
                }
            }
            
            $unmovedImages = array_udiff($parentImages, $imagesMoved, function($a, $b) {
                return $a['file'] <=> $b['file'];
            });
            
            if (count($unmovedImages) > 0) {
                $this->logger->addAlert('The following images from parent SKU ' . $parent->getSku() . ' could not be moved to a duplicate because the duplicate could not be identified from the filename or the duplicate could not be found:');
                foreach ($unmovedImages as $image) {
                    $this->logger->addAlert('    ' . $image['file']);
                    $this->copyMediaFile($image['file']);
                }
            }
    
            // Add the thumbnails to the parent
            // TODO: In case there are still images on the parent we need to make sure thumbnails get positioned first
            if (!empty($fistDuplicateSku)) {
                $this->addThumbnails($fistDuplicateSku, $parent);
            }
            else {
                $this->logger->addError('Could not add thumbnails to parent SKU ' . $parent->getSku() . ' because it has no duplicates or none of it\'s duplicates have thumbnails.');
            }
        }
        
        // TODO: What to do with these? Zach to fix them in M1?
        // Find simple products with no parent sku
        /*
        $simples = $this->productCollectionFactory->create()
            ->addAttributeToSelect('parent_sku')
            ->addAttributeToSelect('status')
            ->addFieldToFilter('type_id', 'simple')
            ->addAttributeToFilter('parent_sku', '');
        
        $this->logger->addInfo('Processing ' . $simples->count() . ' simple products with no parent_sku');
        
        /** @var \Magento\Catalog\Model\Product $simple *
        foreach ($simples as $simple) {
            $this->logger->addInfo('Processing simple product ' . $simple->getSku());
            /*
            $images = $this->getGalleryImages($simple);
            foreach ($images as $image) {
                $this->copyMediaFile($image['file']);
            }
            *
        }
        */
        
        // Process care products
        $this->logger->addInfo('Processing care products');
        $careProductSkus = [
            410001,
            410008,
            410009
        ];
        $careProducts = $this->productCollectionFactory->create()
            ->addFieldToFilter('sku', ['in' => $careProductSkus]);
        foreach ($careProducts as $product) {
            $this->logger->addInfo('Processing care product ' . $product->getSku());
            
            $this->addThumbnails($product->getSku(), $product);
            
            $images = $this->getGalleryImages($product);
            foreach ($images as $image) {
                $this->copyMediaFile($image['file']);
            }
        }
    }
    
    /**
     * @param string $sku
     * @param \Magento\Catalog\Model\Product $product
     *
     * @return bool True if either thumbnail is missing.
     */
    private function addThumbnails($sku, $product)
    {
        // In some cases the sku will have letters appended to them.
        // We want the 6 digit sku without letters to find the right thumbnail files.
        $skuInt = (int)$sku;
    
        $importDir = $this->directoryList->getPath(DirectoryList::APP) . '/' . FileParser::PATH_TO_DATA . 'media/thumbnails';
        $destDir = $this->directoryList->getPath(DirectoryList::MEDIA) . '/catalog/product';
        
        $parts = pathinfo($destDir);
        if (!file_exists($parts['dirname'])) {
            mkdir($parts['dirname']);
        }
        if (!file_exists($destDir)) {
            mkdir($destDir);
        }
    
        $colors = ['ffffff', 'efefef'];
        
        $missing = false;
    
        $pos = 0;
        foreach ($colors as $color) {
            $filename = "$skuInt-$color.jpg";
            
            if (file_exists("$importDir/$filename")) {
                /** Copy the file to media dir **/
                
                $firstDir = substr($filename, 0, 1);
                if (!file_exists("$destDir/$firstDir")) {
                    mkdir("$destDir/$firstDir");
                }
            
                $secondDir = substr($filename, 1, 1);
                if (!file_exists("$destDir/$firstDir/$secondDir")) {
                    mkdir("$destDir/$firstDir/$secondDir");
                }
            
                $subDir = "/$firstDir/$secondDir";
            
                $file = "$subDir/$filename";
                $src = "$importDir/$filename";
                $dest = $destDir . $file;
            
                if (!file_exists($dest)) {
                    copy($src, $dest);
                }
            
                /** Add thumbnail gallery image to db **/
                $hide = false;
                $attributes = [];
                if ($color == 'efefef') {
                    // Grey background images should be hidden from the gallery and 
                    // have the product attributes small_image, thumbnail, swatch_image
                    $attributes = [
                        'image' => $file,
                        'small_image' => $file,
                        'thumbnail' => $file,
                        'swatch_image' => $file
                    ];
                    $hide = true;
                }
                
                $this->addGalleryImage($file, $product, $pos++, $hide, $attributes);
            } 
            else {
                $statusMsg = $product->getStatus() == ProductStatus::STATUS_DISABLED ? 'disabled' : 'enabled';
                $this->logger->addWarning('Missing thumbnail image ' . $filename . ' for ' . $statusMsg. ' duplicate SKU ' . $sku);
                $missing = true;
            }
        }
        
        return $missing;
    }
    
    /**
     * @param \Magento\Catalog\Model\Product $product
     *
     * @return array
     */
    private function getGalleryImages($product): array
    {
        $select = $this->connection->select()
            ->from(
                ['v2e' => 'catalog_product_entity_media_gallery_value_to_entity'],
                ['v2e.entity_id', 'v2e.value_id as image_id', 'mg.value as file', 'mgv.disabled', 'mgv.position']
            )
            ->join(['mg' => 'catalog_product_entity_media_gallery'], 'mg.value_id = v2e.value_id', ['mg.value as file'])
            ->join(['mgv' => 'catalog_product_entity_media_gallery_value'], 'mgv.value_id = v2e.value_id', ['mgv.disabled', 'mgv.position'])
            ->where('v2e.entity_id = ?', $product->getId());
        $data = $this->connection->fetchAll($select);
        
        return $data;
    }
    
    /**
     * @param int $imageId
     * @param \Magento\Catalog\Model\Product $toProduct
     */
    private function moveGalleryImage($imageId, $toProduct)
    {
        $this->connection->update(
            'catalog_product_entity_media_gallery_value_to_entity',
            ['entity_id' => $toProduct->getId()],
            ['value_id = ?' => $imageId]
        );
        
        $this->connection->update(
            'catalog_product_entity_media_gallery_value',
            ['entity_id' => $toProduct->getId()],
            ['value_id = ?' => $imageId]
        );
        
        // TODO: Unhide the image?
    }
    
    /**
     * @param \Magento\Catalog\Model\Product $product
     */
    private function removeAllGalleryImages($product)
    {
        $select = $this->connection->select()
            ->from(
                ['v2e' => 'catalog_product_entity_media_gallery_value_to_entity'],
                ['value_id']
            )
            ->where('v2e.entity_id = ?', $product->getId());
        $valueIds = $this->connection->fetchCol($select);

        if (count($valueIds) > 0) {
            $this->connection->delete(
                'catalog_product_entity_media_gallery',
                ['value_id IN(?)' => $valueIds]
            );
    
            $this->connection->delete(
                'catalog_product_entity_media_gallery_value',
                ['value_id IN(?)' => $valueIds]
            );
    
            $this->connection->delete(
                'catalog_product_entity_media_gallery_value_to_entity',
                ['value_id IN(?)' => $valueIds]
            );
        }
    }
    
    /**
     * @param string $file
     * @param \Magento\Catalog\Model\Product $product
     * @param int $position
     * @param bool $disabled
     * @param array $attributes
     */
    private function addGalleryImage($file, $product, $position, $disabled = false, $attributes = []) {
        $image = [
            'catalog_product_entity_media_gallery' => [
                'attribute_id' => 77,
                'value' => $file,
                'media_type' => 'image',
                'disabled' => 0
            ],
            'catalog_product_entity_media_gallery_value' => [
                'value_id' => '?',
                'store_id' => 0,
                'entity_id' => $product->getId(),
                'label' => '',
                'position' => $position,
                'disabled' => $disabled
            ],
            'catalog_product_entity_media_gallery_value_to_entity' => [
                'value_id' => '?',
                'entity_id' => $product->getId()
            ]
        ];
        
        $this->connection->insert('catalog_product_entity_media_gallery', $image['catalog_product_entity_media_gallery']);
        $valueId = $this->connection->lastInsertId();
        
        $image['catalog_product_entity_media_gallery_value']['value_id'] = $valueId;
        $this->connection->insert('catalog_product_entity_media_gallery_value', $image['catalog_product_entity_media_gallery_value']);
        
        $image['catalog_product_entity_media_gallery_value_to_entity']['value_id'] = $valueId;
        $this->connection->insert('catalog_product_entity_media_gallery_value_to_entity', $image['catalog_product_entity_media_gallery_value_to_entity']);
        
        if (!empty($attributes)) {
            // Set image attributes on the product (image, small_image, thumbnail, swatch_image)
            $this->productAction->updateAttributes([$product->getId()], $attributes, 0);
        }
    }
    
    /**
     * @param string $file
     *
     * @return bool
     */
    private function copyMediaFile($file) {
        $importDir = $this->directoryList->getPath(DirectoryList::MEDIA) . '/import/catalog/product';
        $destDir = $this->directoryList->getPath(DirectoryList::MEDIA) . '/catalog/product';
        
        if (file_exists($importDir . $file)) {
            
            $parts = explode('/', $file);
            if (!count($parts) == 4) {
                return false;
            }
            
            $firstDir = $parts[1];
            if (!file_exists("$destDir/$firstDir")) {
                mkdir("$destDir/$firstDir");
            }
    
            $secondDir = $parts[2];
            if (!file_exists("$destDir/$firstDir/$secondDir")) {
                mkdir("$destDir/$firstDir/$secondDir");
            }
            
            copy($importDir . $file, $destDir . $file);
        }
        else {
            return false;
        }
        
        return true;
    }
}
