<?php
/**
 * @vendor      Kurufootwear
 * @module      Kurufootwear_Migration
 * @author      Kirstaps Stalidzans <info@scandiweb.com>
 * @copyright   Copyright (c) 2017 Scandiweb, Ltd (http://scandiweb.com)
 * @license     http://opensource.org/licenses/afl-3.0.php Academic Free License (AFL 3.0)
 */
namespace Kurufootwear\Migration\Setup\Migration;

use Kurufootwear\Migration\Api\UpgradeMigrationInterface;
use Magento\Cms\Model\PageFactory;
use Magento\Framework\Setup\SetupInterface;
use Magento\Framework\Filesystem;
use Kurufootwear\Migration\Helper\FileParser;
use Magento\Cms\Api\PageRepositoryInterface;

class RemoveNewsroomBlockMigration implements UpgradeMigrationInterface
{
    /**
     * @var Filesystem
     */
    private $filesystem;

    /**
     * @var PageFactory
     */
    private $pageFactory;

    /**
     * @var FileParser
     */
    private $fileParser;

    /**
     * @var PageRepositoryInterface
     */
    private $pageRepository;

    /**
     * CopyrightBlockMigration constructor.
     * @param Filesystem $filesystem
     * @param PageFactory $pageFactory
     * @param FileParser $fileParser
     * @param PageRepositoryInterface $pageRepository
     */
    public function __construct(
        Filesystem $filesystem,
        PageFactory $pageFactory,
        FileParser $fileParser,
        PageRepositoryInterface $pageRepository
    ) {
        $this->filesystem = $filesystem;
        $this->pageFactory = $pageFactory;
        $this->fileParser = $fileParser;
        $this->pageRepository = $pageRepository;
    }

    /**
     * @inheritDoc
     */
    public function apply(SetupInterface $setup = null)
    {
        $cmsPages = [
            'newsroom',
            'newsroom/about-our-market-industry',
            'newsroom/about-the-company',
            'newsroom/history-timeline',
            'newsroom/what-makes-kuru-unique'
        ];

        foreach ($cmsPages as $pageIdentifier) {
            $page = $this->pageFactory->create()
                ->load($pageIdentifier, 'identifier');

            if ($page->getId()) {
                $this->pageRepository->delete($page);
            }
        }
    }
}
