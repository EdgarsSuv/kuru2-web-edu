<?php
/**
 * @category  Kurufootwear
 * @package   Kurufootwear\Migration
 * @author    Krišjānis Veinbahs <krisjanisv@scandiweb.com>
 * @copyright Copyright (c) 2017 Scandiweb, Inc (http://scandiweb.com)
 * @license   http://opensource.org/licenses/OSL-3.0 The Open Software License 3.0 (OSL-3.0)
 */
namespace Kurufootwear\Migration\Setup\Migration;

use Amasty\Shopby\Model\FilterSettingFactory;
use Kurufootwear\Catalog\Helper\AttributeHelper;
use Kurufootwear\Migration\Api\UpgradeMigrationInterface;
use Magento\Customer\Model\Attribute;
use Magento\Eav\Model\Config as EavConfig;
use Magento\Eav\Model\ResourceModel\Entity\Attribute as EavAttribute;
use Magento\Eav\Setup\EavSetupFactory;
use Magento\Framework\Setup\SetupInterface;

class AmastyFilterSettingsMigration implements UpgradeMigrationInterface
{
    private $attributeBestFor = 'benefits';

    /**
     * @var FilterSettingFactory
     */
    private $settingFactory;

    /**
     * @var Attribute
     */
    private $attribute;

    /**
     * @var AttributeHelper
     */
    private $attributeHelper;

    /**
     * @var EavAttribute
     */
    private $eavAttribute;

    /**
     * @var EavConfig
     */
    private $eavConfig;

    /**
     * @var EavSetupFactory
     */
    private $eavSetupFactory;

    /**
     * AmastyFilterSettingsMigration constructor.
     * @param FilterSettingFactory $settingFactory
     * @param Attribute $attribute
     * @param EavAttribute $eavAttribute
     * @param AttributeHelper $attributeHelper
     * @param EavConfig $eavConfig
     * @param EavSetupFactory $eavSetupFactory
     */
    public function __construct(
        FilterSettingFactory $settingFactory,
        Attribute $attribute,
        EavAttribute $eavAttribute,
        AttributeHelper $attributeHelper,
        EavConfig $eavConfig,
        EavSetupFactory $eavSetupFactory
    )
    {
        $this->settingFactory = $settingFactory;
        $this->attribute = $attribute;
        $this->attributeHelper = $attributeHelper;
        $this->eavAttribute = $eavAttribute;
        $this->eavConfig = $eavConfig;
        $this->eavSetupFactory = $eavSetupFactory;
    }

    /**
     * Create or update shipping methods
     * @param SetupInterface|null $setup
     */
    public function apply(SetupInterface $setup = null)
    {
        $filterSettingsData = [
            [
                'filter_code' => 'attr_color',
                'is_multiselect' => 0,
                'display_mode' => 4,
                'is_seo_significant' => 0,
                'slider_step' => 1.0000,
                'units_label_use_currency_symbol' => 1,
                'units_label' => '',
                'index_mode' => 0,
                'follow_mode' => 0,
                'hide_one_option' => 0,
                'is_expanded' => 0,
                'sort_options_by' => 0,
                'show_product_quantities' => '',
                'is_show_search_box'  => 0,
                'number_unfolded_options' => 0,
                'tooltip' => '',
                'is_use_and_logic' => 0,
                'add_from_to_widget' => 0,
                'visible_in_categories' => 'visible_everywhere',
                'categories_filter' => '',
                'attributes_filter' => '',
                'attributes_options_filter' => '',
                'block_position' => 0
            ],
            [
                'filter_code' => 'attr_filterable_color',
                'is_multiselect' => 1,
                'display_mode' => 0,
                'is_seo_significant' => 0,
                'slider_step' => 1.0000,
                'units_label_use_currency_symbol' => 1,
                'units_label' => '',
                'index_mode' => 0,
                'follow_mode' => 0,
                'hide_one_option' => 0,
                'is_expanded' => 0,
                'sort_options_by' => 0,
                'show_product_quantities' => '',
                'is_show_search_box'  => 0,
                'number_unfolded_options' => 0,
                'tooltip' => '',
                'is_use_and_logic' => 0,
                'add_from_to_widget' => 0,
                'visible_in_categories' => 'visible_everywhere',
                'categories_filter' => '',
                'attributes_filter' => '',
                'attributes_options_filter' => '',
                'block_position' => 0
            ],
            [
                'filter_code' => 'attr_benefits',
                'is_multiselect' => 1,
                'display_mode' => 0,
                'is_seo_significant' => 0,
                'slider_step' => 1.0000,
                'units_label_use_currency_symbol' => 1,
                'units_label' => '',
                'index_mode' => 0,
                'follow_mode' => 0,
                'hide_one_option' => 0,
                'is_expanded' => 0,
                'sort_options_by' => 0,
                'show_product_quantities' => '',
                'is_show_search_box'  => 0,
                'number_unfolded_options' => 0,
                'tooltip' => '',
                'is_use_and_logic' => 1,
                'add_from_to_widget' => 0,
                'visible_in_categories' => 'visible_everywhere',
                'categories_filter' => '',
                'attributes_filter' => '',
                'attributes_options_filter' => '',
                'block_position' => 0
            ]
        ];

        $attributeId = $this->eavAttribute->getIdByCode('catalog_product', $this->attributeBestFor);
        $optionId = $this->attributeHelper->getAttributeId('Wide Width', $this->attributeBestFor);
        $attributeModel = $this->attribute->load($attributeId);
        $options = $attributeModel->getSource()->getAllOptions(); //get all options

        if ($optionId) {
            $options['value'][$optionId] = true;
            $options['delete'][$optionId] = true;
            $eavSetup = $this->eavSetupFactory->create(['setup' => $setup]);;
            $eavSetup->addAttributeOption($options);
        }

        foreach ($filterSettingsData as $item) {
            $filterSettings = $this->settingFactory->create();
            $filterSettings->load($item['filter_code'], 'filter_code');

            if (!$filterSettings->getId()) {
                $filterSettings->addData($item);
                $filterSettings->save();
            }
        }
    }
}
