<?php
/**
 * @category  Kurufootwear
 * @package   Kurufootwear\Migration
 * @author    Krisjanis Veinbahs <info@scandiweb.com>
 * @copyright Copyright (c) 2017 Scandiweb, Inc (http://scandiweb.com)
 * @license   http://opensource.org/licenses/OSL-3.0 The Open Software License 3.0 (OSL-3.0)
 */
namespace Kurufootwear\Migration\Setup\Migration;

use Kurufootwear\Migration\Api\UpgradeMigrationInterface;
use Magento\Customer\Model\ResourceModel\Group\Collection as GroupCollection;
use Magento\Framework\Setup\SetupInterface;
use Magento\Store\Api\WebsiteRepositoryInterface;
use Mirasvit\Rewards\Model\Config;
use Mirasvit\Rewards\Model\Earning\Rule as EarningRule;
use Mirasvit\Rewards\Model\Earning\RuleFactory as EarningRuleFactory;
use Mirasvit\Rewards\Model\Spending\RuleFactory as SpendingRuleFactory;

class RewardsRuleMigration implements UpgradeMigrationInterface
{
    /**
     * @var SpendingRuleFactory
     */
    protected $spendingRuleFactory;

    /**
     * @var EarningRuleFactory
     */
    protected $earningRuleFactory;

    /**
     * @var GroupCollection
     */
    protected $groupCollection;

    /**
     * @var WebsiteRepositoryInterface
     */
    protected $websiteRepository;

    /**
     * RewardsRuleMigration constructor.
     * @param EarningRuleFactory $earningRuleFactory
     * @param SpendingRuleFactory $spendingRuleFactory
     * @param GroupCollection $groupCollection
     */
    public function __construct(
        EarningRuleFactory $earningRuleFactory,
        SpendingRuleFactory $spendingRuleFactory,
        GroupCollection $groupCollection,
        WebsiteRepositoryInterface $websiteRepository
    )
    {
        $this->earningRuleFactory = $earningRuleFactory;
        $this->spendingRuleFactory = $spendingRuleFactory;
        $this->groupCollection = $groupCollection;
        $this->websiteRepository = $websiteRepository;
    }

    /**
     * @param SetupInterface|null $setup
     */
    public function apply(SetupInterface $setup = null)
    {
        $customerGroups = $this->getGroupIds();
        $websites = $this->getWebsiteIds();

        /**
         * (Re)Create spending rule
         */
        $spendingRuleName = 'Basic spending rule (1 point : 1 ¢)';
        $this->spendingRuleFactory->create()->load($spendingRuleName, 'name')
            ->setName($spendingRuleName)
            ->setIsActive(true)
            ->setCustomerGroupIds($customerGroups)
            ->setSpendingStyle('partial_point')
            ->setSpendPoints(1)
            ->setMonetaryStep(0.01)
            ->setWebsiteIds($websites)
            ->save();

        /**
         * (Re)Create earning rule
         */
        $earningRuleName = 'Basic earning rule (1 $ : 5 points)';
        $this->earningRuleFactory->create()->load($earningRuleName, 'name')
            ->setName($earningRuleName)
            ->setType(EarningRule::TYPE_CART)
            ->setIsActive(true)
            ->setCustomerGroupIds($customerGroups)
            ->setEarningStyle(Config::EARNING_STYLE_AMOUNT_SPENT)
            ->setMonetaryStep(1)
            ->setEarnPoints(5)
            ->setWebsiteIds($websites)
            ->save();
    }

    public function getGroupIds()
    {
        $customerGroups = $this->groupCollection->toOptionArray();

        foreach ($customerGroups as $group => $info) {
            $customerGroups[$group] = $info['value'];
        }

        return $customerGroups;
    }


    public function getWebsiteIds()
    {
        $websites = $this->websiteRepository->getList();

        foreach ($websites as $index => $website) {
            $websites[$index] = $website->getId();
        }

        return $websites;
    }
}