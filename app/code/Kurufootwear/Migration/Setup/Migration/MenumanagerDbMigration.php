<?php
/**
 * @vendor      Kurufootwear
 * @module      Kurufootwear_Migration
 * @copyright   Copyright (c) 2017 Scandiweb, Ltd (http://scandiweb.com)
 * @license     http://opensource.org/licenses/afl-3.0.php Academic Free License (AFL 3.0)
 */
namespace Kurufootwear\Migration\Setup\Migration;

use Kurufootwear\Migration\Api\UpgradeMigrationInterface;
use Magento\Framework\Setup\SetupInterface;
use Kurufootwear\Migration\Helper\FileParser;
use Magento\Cms\Model\PageFactory;
use Magento\Framework\DB\Ddl\Table;

class MenumanagerDbMigration implements UpgradeMigrationInterface
{
    /**
     * @var PageFactory
     */
    private $pageFactory;

    /**
     * @var FileParser
     */
    private $fileParser;

    /**
     * UpgradeData constructor.
     * @param PageFactory $pageFactory
     * @param FileParser $fileParser
     */
    public function __construct(
        PageFactory $pageFactory,
        FileParser $fileParser
    ) {
        $this->pageFactory = $pageFactory;
        $this->fileParser = $fileParser;
    }

    /**
     * @inheritDoc
     */
    public function apply(SetupInterface $setup = null)
    {
        // Get module table
        $tableName = $setup->getTable('scandiweb_menumanager_item');

        // Check if the table already exists
        if ($setup->getConnection()->isTableExists($tableName)) {

            $connection = $setup->getConnection();
            $connection->addColumn(
                $tableName,
                'css_class',
                [
                    'type' => Table::TYPE_TEXT,
                    'length' => 100,
                    'nullable' => true,
                    'comment' => 'Custom CSS Class'
                ]
            );
        }
    }
}
