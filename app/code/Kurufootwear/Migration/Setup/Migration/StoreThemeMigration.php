<?php
/**
 * @vendor Kurufootwear
 * @module Kurufootwear_Migration
 * @author Viesturs Ruzans <viestursr@scandiweb.com>
 * @copyright Copyright (c) 2017 Scandiweb, Ltd (http://scandiweb.com)
 * @license http://opensource.org/licenses/afl-3.0.php Academic Free License (AFL 3.0)
 */
namespace Kurufootwear\Migration\Setup\Migration;

use Kurufootwear\Migration\Api\UpgradeMigrationInterface;
use Magento\Config\Block\System\Config\Form;
use Magento\Theme\Model\Theme;
use Magento\Config\Model\ResourceModel\Config;
use Magento\Framework\Setup\SetupInterface;

class StoreThemeMigration implements UpgradeMigrationInterface
{
    /**
     * @var Config
     */
    private $_resourceConfig;

    /**
     * @var Theme
     */
    private $_themeResource;

    /**
     * StoreThemeMigration constructor.
     *
     * @param Config $resourceConfig
     * @param Theme $themeResource
     */
    public function __construct(
        Config $resourceConfig,
        Theme $themeResource
    ) {
        $this->_resourceConfig = $resourceConfig;
        $this->_themeResource = $themeResource;
    }

    /**
     * @inheritDoc
     */
    public function apply(SetupInterface $setup = null)
    {
        $theme = $this->_getTheme();

        if ($themeId = $theme->getThemeId()) {
            $this->_resourceConfig->saveConfig(
                'design/theme/theme_id',
                $themeId,
                Form::SCOPE_DEFAULT,
                0
            );
        }
    }

    /**
     * @return \Magento\Framework\DataObject
     */
    protected function _getTheme()
    {
        $collection = $this->_themeResource->getCollection();
        $collection->addFieldToFilter('theme_path', 'Kurufootwear/default');

        return $collection->getFirstItem();
    }
}
