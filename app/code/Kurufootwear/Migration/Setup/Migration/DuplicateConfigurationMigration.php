<?php
/**
 * @category  Kurufootwear
 * @package   Kurufootwear\Migration
 * @author    Krisjanis Veinbahs <info@scandiweb.com>
 * @copyright Copyright (c) 2017 Scandiweb, Inc (http://scandiweb.com)
 * @license   http://opensource.org/licenses/OSL-3.0 The Open Software License 3.0 (OSL-3.0)
 */
namespace Kurufootwear\Migration\Setup\Migration;

use Kurufootwear\Migration\Api\UpgradeMigrationInterface;
use Magento\Catalog\Model\Product;
use Magento\Catalog\Model\Product\Attribute\Repository;
use Magento\Catalog\Model\ProductFactory;
use Magento\Catalog\Model\ResourceModel\Product\CollectionFactory as ProductCollectionFactory;
use Magento\ConfigurableProduct\Model\Product\Type\Configurable\Attribute as ConfigurableAttribute;
use Magento\ConfigurableProduct\Model\Product\Type\Configurable\AttributeFactory as ConfigurableAttributeFactory;
use Magento\Eav\Model\Entity\Attribute;
use Magento\Framework\Exception\NoSuchEntityException;
use Magento\Framework\Setup\SetupInterface;
use Magento\Indexer\Model\IndexerFactory;
use Magento\Catalog\Model\Product\Action;
use Magento\Catalog\Api\ProductRepositoryInterface;
use Psr\Log\LoggerInterface;

class DuplicateConfigurationMigration implements UpgradeMigrationInterface
{
    /**
     * @var ProductCollectionFactory
     */
    protected $productCollectionFactory;

    /**
     * @var ProductRepositoryInterface
     */
    protected $productRepository;

    /**
     * @var Action
     */
    protected $productAction;

    /**
     * @var ProductFactory
     */
    protected $productFactory;

    /**
     * @var Attribute
     */
    protected $attributeModel;

    /**
     * @var ConfigurableAttributeFactory
     */
    protected $configurableAttributeModelFactory;

    /**
     * @var LoggerInterface
     */
    protected $logger;

    /**
     * @var Repository
     */
    protected $productAttributeRepository;

    /**
     * DuplicateConfigurationMigration constructor.
     * @param ConfigurableAttributeFactory $configurableAttributeModelFactory
     * @param Attribute $attributeModel
     * @param ProductCollectionFactory $productCollectionFactory
     * @param ProductRepositoryInterface $productRepository
     * @param Action $productAction
     * @param ProductFactory $productFactory
     * @param LoggerInterface $logger
     * @param Repository $productAttributeRepository
     */
    public function __construct(
        ConfigurableAttributeFactory $configurableAttributeModelFactory,
        Attribute $attributeModel,
        ProductCollectionFactory $productCollectionFactory,
        ProductRepositoryInterface $productRepository,
        Action $productAction,
        ProductFactory $productFactory,
        LoggerInterface $logger,
        Repository $productAttributeRepository
    )
    {
        $this->configurableAttributeModelFactory = $configurableAttributeModelFactory;
        $this->attributeModel = $attributeModel;
        $this->productCollectionFactory = $productCollectionFactory;
        $this->productRepository = $productRepository;
        $this->productAction = $productAction;
        $this->productFactory = $productFactory;
        $this->logger = $logger;
        $this->productAttributeRepository = $productAttributeRepository;
    }

    /**
     * Fix and (re)populate duplicate product configurations
     * @param SetupInterface|null $setup
     */
    public function apply(SetupInterface $setup = null)
    {
        $products = $this->getFixableDuplicateCollection();

        $count = $products->count();
        echo "-- Fixing " . $count . " products --\n";

        $index = 1;
        foreach ($products as $product) {
            echo "\n" . $index++ . "/" . $count . ") ";

            // Get product
            /* @var Product $product */
            try {
                $product = $this->productRepository->get($product->getSku());
            } catch (NoSuchEntityException $e) {
                // The fixable product collection is now large enough
                // to sometimes run into broken/unloadable products -> skip them
                echo "Failed on broken product w/ SKU - '" . $product->getSku() . " & id - " . $product->getId();
                continue;
            }

            // Remove old configurations
            $product->setCanSaveConfigurableAttributes(true);
            $product->setAffectConfigurableProductAttributes(4);

            $product->getExtensionAttributes()->setConfigurableProductOptions([]);
            $product->getExtensionAttributes()->setConfigurableProductLinks([]);

            try {
                $this->productRepository->save($product);
            } catch (\Exception $e) {
                // Removing configurations failed -> Skip
                echo "^- Tried to remove old configurations, failed. Investigation necessary.\n";
                continue;
            }

            // Add new configurations
            $isMens = $this->isProductMens($product);

            // Get configuration product ids
            $product = $this->productRepository->get($product->getSku());
            $simpleIds = $this->getSimpleIdsFromDuplicate($product);
            $master = $this->getMasterFromDuplicate($product);

            // Log info
            echo "Fixing attributes for duplicate '" . $product->getName() . "' w/ SKU - " . $product->getSku() . " & id - " . $product->getId();
            echo $isMens ? " & is mens" : " & is womens";
            echo $product->getStatus() == 1 ? " & is enabled" : " & is disabled";
            echo "\n";

            /**
             * Configuration attribute fix.
             * (Generate attributes and apply)
             * (Specifically color and mens/womens size)
             * (Reuse info from master product and apply simple configurations)
             */
            // Generate all necessary attributes (color / size)
            $productEntityType = 'catalog_product';

            $colorId = $this->attributeModel->getIdByCode($productEntityType,'color');
            if ($isMens) {
                $sizeId = $this->attributeModel->getIdByCode($productEntityType, 'size');
                $attributes = [
                    'color' => $colorId,
                    'size' => $sizeId
                ];
            } else {
                $sizeId = $this->attributeModel->getIdByCode($productEntityType, 'womens_size');
                $attributes = [
                    'color' => $colorId,
                    'womens_size' => $sizeId
                ];
            }
            $attributesData = $product->getTypeInstance()->getConfigurableAttributesAsArray($product);

            // Replicate masters configurations
            $product->setCanSaveConfigurableAttributes(true);
            $product->getTypeInstance()->setUsedProductAttributeIds(array_values($attributes), $product);
            $product->setAffectConfigurableProductAttributes(4);
            $product->setConfigurableAttributesData($attributesData);

            $linkedProducts = $master->getExtensionAttributes()->getConfigurableProductLinks();
            foreach ($linkedProducts as $id => $linkedProductId ) {
                if (!in_array($linkedProductId, $simpleIds)) {
                    unset($linkedProducts[$linkedProductId]);
                }
            }

            if (count($linkedProducts) == 0) {
                echo "^- Skipping duplicate fix, because master also has no configurations.\n";
                echo "^- Manual intervention necessary!\n";
                continue;
            }

            $replicableConfigurableOptions = $master->getExtensionAttributes()->getConfigurableProductOptions();
            $configurableOptions = [];
            foreach ($replicableConfigurableOptions as $replicableConfigurableOption) {
                /* @var ConfigurableAttribute $configurableOption */
                $configurableOption = $this->configurableAttributeModelFactory->create();
                $configurableOption->setProductId($product->getId())
                    ->setAttributeId($a = $replicableConfigurableOption->getAttributeId())
                    ->setProductAttribute($b = $replicableConfigurableOption->getProductAttribute())
                    ->setLabel($c = $replicableConfigurableOption->getLabel())
                    ->setPosition($d = $replicableConfigurableOption->getPosition())
                    ->setValues($e = $replicableConfigurableOption->getValues());
                $configurableOptions[] = $configurableOption;
            }

            $product->getExtensionAttributes()->setConfigurableProductOptions($configurableOptions);
            $product->getExtensionAttributes()->setConfigurableProductLinks($linkedProducts);

            // Save
            try {
                $this->productRepository->save($product);
                echo "^- Successfully fixed & saved\n";
            } catch (\Exception $e) {
                // Sad
                echo "^- Failed to fix & save product\n";
            }
        }
    }

    /**
     * Detect if a duplicate product is mens or womens by
     * masters children attributes
     * @param $product
     * @return bool
     */
    public function isProductMens($product)
    {
        /**
         * If any simple product has 'womens_size' attribute,
         * then it's a womens shoe and this duplicate should also be such
         */
        $simple = $this->getSimplesFromDuplicate($product)->getFirstItem();
        $simpleModel = $this->productRepository->get($simple->getSku());

        return !$simpleModel->getData('womens_size');
    }

    /**
     * Get simple product ids from duplicate
     *
     * @param $product
     * @return array
     */
    public function getSimpleIdsFromDuplicate($product) {
        $simples = $this->getSimplesFromDuplicate($product)->getItems();
        $simpleIds = [];

        if (!$simples) {
            return null;
        }

        foreach ($simples as $simple) {
            $simpleIds[] = $simple->getId();
        }

        return $simpleIds;
    }

    /**
     * Get a collection of active duplicate products
     * @return mixed
     */
    public function getFixableDuplicateCollection()
    {
        return $this->productCollectionFactory->create()
            ->addAttributeToFilter('type_id', 'configurable')
            ->addAttributeToFilter('product_duplicate', ['eq' => true])
            ->load();
    }

    /**
     * Get simple products from duplicate
     * @param $product
     * @return null
     */
    public function getSimplesFromDuplicate($product)
    {
        $duplicateSku = $product->getSku();
        $simpleSearchMatch = $duplicateSku . '-%';
        $simpleCollection = $this->productCollectionFactory->create()
            ->addAttributeToFilter('type_id', 'simple')
            ->addAttributeToFilter('product_duplicate', ['eq' => false])
            ->addAttributeToFilter('sku', ['like' => $simpleSearchMatch])
            ->load();

        return $simpleCollection;
    }

    /**
     * Get master product from duplicate
     * @param $product
     * @return \Magento\Catalog\Api\Data\ProductInterface
     */
    public function getMasterFromDuplicate($product)
    {
        $masterSku = $product->getParentSku();

        return $this->productRepository->get($masterSku);
    }

    /**
     * Fetch super attribute
     * @param $product
     * @param $attributeId
     * @return null
     */
    public function getAttributeFromProduct($product, $attributeId)
    {
        $attributes = $product->getTypeInstance()->getConfigurableAttributes($product);
        foreach ($attributes as $attribute) {
            /* @var ConfigurableAttribute $attribute */
            if ($attribute->getProductAttribute()->getId() === $attributeId) {
                return $attribute->getProductAttribute();
            }
        }

        return null;
    }

    /**
     * Get products from id array
     * @param $productIds
     * @return array
     */
    public function getSimpleProducts($productIds)
    {
        $products = [];
        foreach($productIds as $productId) {
            $products[] = $this->productRepository->getById($productId);
        }
        return $products;
    }
}