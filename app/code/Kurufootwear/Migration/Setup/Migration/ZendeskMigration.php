<?php
/**
 * @category    Kurufootwear
 * @package     Kurufootwear\Rma
 * @author      Jim McGowen <jim@kurufootwear.com>
 * @copyright   Copyright (c) 2017 KURU Footwear. All rights reserved.
 */

namespace Kurufootwear\Migration\Setup\Migration;

use Kurufootwear\Migration\Api\UpgradeMigrationInterface;
use Kurufootwear\Migration\Helper\Configurator;
use Magento\Framework\Setup\SetupInterface;

class ZendeskMigration implements UpgradeMigrationInterface
{
    /**
     * @var Configurator
     */
    private $configurator;

    /**
     * EnableAmastyAjax constructor.
     * @param Configurator $configurator
     */
    public function __construct(
        Configurator $configurator
    ) {
        $this->configurator = $configurator;
    }

    /**
     * @inheritDoc
     */
    public function apply(SetupInterface $setup = null)
    {
        $this->configurator->save(
            'potato_zendesk/general/token',
            '1a8ad6d812fcfe000a4c866274924f6b',
            Configurator::SCOPE_DEFAULT
        );
    }
}
