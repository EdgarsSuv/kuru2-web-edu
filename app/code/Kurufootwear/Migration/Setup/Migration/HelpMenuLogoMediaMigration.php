<?php
/**
 * @vendor      Kurufootwear
 * @module      Kurufootwear_Migration
 * @author      Jelena Sorohova <info@scandiweb.com>
 * @copyright   Copyright (c) 2017 Scandiweb, Ltd (http://scandiweb.com)
 * @license     http://opensource.org/licenses/afl-3.0.php Academic Free License (AFL 3.0)
 */
namespace Kurufootwear\Migration\Setup\Migration;

use Kurufootwear\Migration\Api\UpgradeMigrationInterface;
use Magento\Framework\Setup\SetupInterface;
use Kurufootwear\Migration\Helper\FileHelper;

class HelpMenuLogoMediaMigration implements UpgradeMigrationInterface
{
    /**
     * @var FileHelper
     */
    private $fileHelper;

    /**
     * BenefitsMediaMigration constructor.
     * @param FileHelper $fileHelper
     */
    public function __construct(
        FileHelper $fileHelper
    ) {
        $this->fileHelper = $fileHelper;
    }

    /**
     * @inheritDoc
     */
    public function apply(SetupInterface $setup = null)
    {
        $benefitsIcons = [
            'chat.svg',
            'mail.svg',
            'phone-larger.svg',
            'question-mark.svg',
            'package.svg'
        ];

        $this->fileHelper->copyMediaFiles($benefitsIcons, 'icons');
    }
}
