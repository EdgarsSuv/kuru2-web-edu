<?php
/**
 * @vendor      Kurufootwear
 * @module      Kurufootwear_Migration
 * @author      Jelena Sorohova <info@scandiweb.com>
 * @copyright   Copyright (c) 2017 Scandiweb, Ltd (http://scandiweb.com)
 * @license     http://opensource.org/licenses/afl-3.0.php Academic Free License (AFL 3.0)
 */
namespace Kurufootwear\Migration\Setup\Migration;

use Kurufootwear\Migration\Api\UpgradeMigrationInterface;
use Magento\Framework\Setup\SetupInterface;
use Magento\Framework\Filesystem;
use Magento\Framework\Filesystem\Driver\File;
use Kurufootwear\Migration\Helper\FileParser;
use Magento\Cms\Model\BlockFactory;

class FooterBlocksMigration implements UpgradeMigrationInterface
{
    const LOGO_PUBLIC_DIR = 'logos';

    /**
     * @var File
     */
    private $fileDriver;

    /**
     * @var Filesystem
     */
    private $filesystem;

    /**
     * @var BlockFactory
     */
    private $blockFactory;

    /**
     * @var FileParser
     */
    private $fileParser;

    /**
     * FooterBlocksMigration constructor.
     * @param Filesystem $filesystem
     * @param File $fileDriver
     * @param BlockFactory $blockFactory
     * @param FileParser $fileParser
     */
    public function __construct(
        Filesystem $filesystem,
        File $fileDriver,
        BlockFactory $blockFactory,
        FileParser $fileParser
    ) {
        $this->filesystem = $filesystem;
        $this->fileDriver = $fileDriver;
        $this->blockFactory = $blockFactory;
        $this->fileParser = $fileParser;
    }

    /**
     * @inheritDoc
     */
    public function apply(SetupInterface $setup = null)
    {
        $mediaDir = $this->filesystem->getDirectoryWrite('media');
        $mediaDir->create(self::LOGO_PUBLIC_DIR);

        $sourcePath = realpath(__DIR__ . '/../../files/data/media/logos');
        $destinationPath = $mediaDir->getAbsolutePath(self::LOGO_PUBLIC_DIR);

        $filenames = $this->fileDriver->readDirectory($sourcePath);
        foreach ($filenames as $filename) {
            $name = basename($filename);
            $this->fileDriver->copy(
                sprintf('%s/%s', $sourcePath, $name),
                sprintf('%s/%s', $destinationPath, $name)
            );
        }

        $cmsBlocks = array(
            array(
                'file_name' => 'footer_content_links.html',
                'title' => 'Footer Content Links',
                'identifier' => 'footer_content_links'
            ),
            array(
                'file_name' => 'footer_delivery_block.html',
                'title' => 'Footer Delivery Block',
                'identifier' => 'footer_delivery_block'
            )
        );

        foreach ($cmsBlocks as $cmsBlock) {
            $content = '';

            if ($cmsBlock['file_name'] != '') {
                $content = $this->fileParser->getHtmlContent('blocks/' . $cmsBlock['file_name']);
            }

            $blockData = array(
                'title' => $cmsBlock['title'],
                'identifier' => $cmsBlock['identifier'],
                'content' => $content,
                'stores' => array(0)
            );

            $block = $this->blockFactory->create()->load($blockData['identifier'], 'identifier');

            if ($block->getId()) {
                $block->setTitle($blockData['title'])
                    ->setContent($blockData['content'])
                    ->setStores($blockData['stores'])
                    ->save();
            } else {
                $block->addData($blockData)
                    ->save();
            }
        }
    }
}
