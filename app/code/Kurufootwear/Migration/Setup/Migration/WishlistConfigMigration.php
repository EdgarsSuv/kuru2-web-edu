<?php
/**
 * @vendor      Kurufootwear
 * @module      Kurufootwear_Migration
 * @author      Jelena Sorohova <info@scandiweb.com>
 * @copyright   Copyright (c) 2017 Scandiweb, Ltd (http://scandiweb.com)
 * @license     http://opensource.org/licenses/afl-3.0.php Academic Free License (AFL 3.0)
 */
namespace Kurufootwear\Migration\Setup\Migration;

use Kurufootwear\Migration\Api\UpgradeMigrationInterface;
use Magento\Framework\Setup\SetupInterface;
use Magento\Config\Model\ResourceModel\Config;
use Magento\Config\Block\System\Config\Form;

class WishlistConfigMigration implements UpgradeMigrationInterface
{
    /**
     * @var Config
     */
    private $_resourceConfig;

    /**
     * WishlistConfigMigration constructor.
     * @param Config $resourceConfig
     */
    public function __construct(
        Config $resourceConfig
    ) {
        $this->_resourceConfig = $resourceConfig;
    }

    /**
     * @inheritDoc
     */
    public function apply(SetupInterface $setup = null)
    {
        $this->_resourceConfig->saveConfig(
            'wishlist/general/active',
            '1',
            Form::SCOPE_DEFAULT,
            0
        );
    }
}
