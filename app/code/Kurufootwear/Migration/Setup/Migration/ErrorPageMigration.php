<?php
/**
 * @vendor      Kurufootwear
 * @module      Kurufootwear_Migration
 * @author      Jelena Sorohova <info@scandiweb.com>
 * @copyright   Copyright (c) 2017 Scandiweb, Ltd (http://scandiweb.com)
 * @license     http://opensource.org/licenses/afl-3.0.php Academic Free License (AFL 3.0)
 */
namespace Kurufootwear\Migration\Setup\Migration;

use Kurufootwear\Migration\Api\UpgradeMigrationInterface;
use Magento\Framework\Setup\SetupInterface;
use Magento\Cms\Model\PageFactory;
use Kurufootwear\Migration\Helper\FileParser;
use Kurufootwear\Migration\Helper\FileHelper;

class ErrorPageMigration implements UpgradeMigrationInterface
{
    /**
     * @var PageFactory
     */
    private $pageFactory;

    /**
     * @var FileParser
     */
    private $fileParser;

    /**
     * @var FileHelper
     */
    private $fileHelper;

    /**
     * UpgradeData constructor.
     * @param PageFactory $pageFactory
     * @param FileParser $fileParser
     */
    public function __construct(
        PageFactory $pageFactory,
        FileParser $fileParser,
        FileHelper $fileHelper
    ) {
        $this->pageFactory = $pageFactory;
        $this->fileParser = $fileParser;
        $this->fileHelper = $fileHelper;
    }

    /**
     * @inheritDoc
     */
    public function apply(SetupInterface $setup = null)
    {
        $cmsPages = [
            [
                'identifier' => 'no-route',
                'file_name' => 'no-route.html'
            ]
        ];

        foreach ($cmsPages as $cmsPage) {
            $content = '';

            if ($cmsPage['file_name'] != '') {
                $content = $this->fileParser->getHtmlContent('pages/' . $cmsPage['file_name']);
            }

            $pageData = [
                'identifier' => $cmsPage['identifier'],
                'content' => $content,
                'page_layout' => 'custom-header',
                'stores' => array(0)
            ];

            $page = $this->pageFactory->create()->load($pageData['identifier'], 'identifier');

            if ($page->getId()) {
                $page->setContent($pageData['content'])
                    ->setStores($pageData['stores'])
                    ->setPageLayout($pageData['page_layout'])
                    ->save();
            } else {
                $page->addData($pageData)
                    ->save();
            }
        }

        $this->fileHelper->copyMediaFiles(['404-image.png'], 'wysiwyg');
        $this->fileHelper->copyMediaFiles(['logo.png'], 'logos');
    }
}
