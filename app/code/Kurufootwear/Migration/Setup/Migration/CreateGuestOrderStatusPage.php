<?php
/**
 * @vendor Kurufootwear
 * @module Kurufootwear_Migration
 * @author Aleksejs Baranovs <info@scandiweb.com>
 * @copyright Copyright (c) 2018 KURU Footwear. All rights reserved.
 */

namespace Kurufootwear\Migration\Setup\Migration;

use Exception;
use Kurufootwear\Catalog\Model\Category;
use Kurufootwear\Migration\Api\UpgradeMigrationInterface;
use Magento\Framework\Setup\SetupInterface;
use Kurufootwear\Migration\Helper\CmsHelper;
use Kurufootwear\Migration\Helper\FileParser;
use Magento\Catalog\Api\CategoryRepositoryInterface;
use Magento\Catalog\Model\CategoryFactory;
use Magento\Catalog\Model\ResourceModel\Category\CollectionFactory as CategoryCollection;
use Magento\Cms\Model\BlockFactory;
use Magento\Cms\Model\ResourceModel\Block as BlockResource;

/**
 * Class CreateGuestOrderStatusPage
 * @package Kurufootwear\Migration\Setup\Migration
 */
class CreateGuestOrderStatusPage implements UpgradeMigrationInterface
{
    /**
     * @var CmsHelper
     */
    private $cmsHelper;

    /**
     * @var FileParser
     */
    private $fileParser;

    /**
     * @var CategoryFactory
     */
    private $categoryFactory;

    /**
     * @var CategoryRepositoryInterface
     */
    private $categoryRepository;

    /**
     * @var CategoryCollection
     */
    private $categoryCollection;

    /**
     * @var BlockFactory
     */
    private $blockFactory;

    /**
     * @var BlockResource
     */
    private $blockResource;

    /**
     * CreateGuestOrderStatusPage constructor.
     *
     * @param CmsHelper $cmsHelper
     * @param FileParser $fileParser
     * @param CategoryFactory $categoryFactory
     * @param CategoryRepositoryInterface $categoryRepository
     * @param CategoryCollection $categoryCollection
     * @param BlockFactory $blockFactory
     * @param BlockResource $blockResource
     */
    public function __construct(
        CmsHelper $cmsHelper,
        FileParser $fileParser,
        CategoryFactory $categoryFactory,
        CategoryRepositoryInterface $categoryRepository,
        CategoryCollection $categoryCollection,
        BlockFactory $blockFactory,
        BlockResource $blockResource
    ) {
        $this->cmsHelper = $cmsHelper;
        $this->fileParser = $fileParser;
        $this->categoryFactory = $categoryFactory;
        $this->categoryRepository = $categoryRepository;
        $this->categoryCollection = $categoryCollection;
        $this->blockFactory = $blockFactory;
        $this->blockResource = $blockResource;
    }

    /**
     * @inheritDoc
     */
    public function apply(SetupInterface $setup = null)
    {
        $this->createOrderStatusCategory();
        $this->updateLinksBlocks();
    }

    /**
     * Create order status CMS block
     *
     * @return bool|int
     */
    private function createOrderStatusCmsBlock()
    {
        $content = $this->fileParser->getHtmlContent('blocks' . DIRECTORY_SEPARATOR . 'check-order-status.html');

        if ($content) {
            $data = [
                'title' => 'Check my order status',
                'identifier' => 'order-status',
                'content' => $content,
                'stores' => [0]
            ];

            $cmsBlock = $this->cmsHelper->createBlock($data['identifier'], $data['content'], $data);

            return $cmsBlock->getId();
        }

        return false;
    }

    /**
     * Create order status category
     * KURU way to display CMS pages
     */
    private function createOrderStatusCategory()
    {
        $cmsBlockId = $this->createOrderStatusCmsBlock();

        // If no CMS block do not continue
        if (!$cmsBlockId) {
            return;
        }

        // Get "Customer Service" category
        $parentCategory = $this->categoryCollection->create()
            ->addFieldToFilter('name', 'Customer Service')
            ->getFirstItem();

        if ($parentCategory->getId()) {
            $category = $this->categoryFactory->create()
                ->setName('Check my order status')
                ->setStoreId(0)
                ->setIsActive(1)
                ->setUrlKey('order-status')
                ->setParentId($parentCategory->getCategoryId())
                ->setPath($parentCategory->getPath())
                ->setDisplayMode(Category::DM_PAGE)
                ->setLandingPage($cmsBlockId);

            try {
                $category = $this->categoryRepository->save($category);
            } catch (Exception $e) {
                return false;
            }

            // Set new path for the category
            if ($category->getId()) {
                $category->setPath($parentCategory->getPath() . DIRECTORY_SEPARATOR . $category->getId());

                $this->categoryRepository->save($category);
            }
        }
    }

    /**
     * Update footer and category menu links blocks
     */
    private function updateLinksBlocks()
    {
        $cmsBlocks =[
            [
                'file_name' => 'footer_content_links.html',
                'identifier' => 'footer_content_links'
            ],
            [
                'file_name' => 'category_menu.html',
                'identifier' => 'category-menu'
            ]
        ];

        foreach ($cmsBlocks as $cmsBlock) {
            $content = $this->fileParser->getHtmlContent('blocks' . DIRECTORY_SEPARATOR . $cmsBlock['file_name']);

            if (!$content) {
                continue;
            }

            $model = $this->blockFactory->create();

            // Cannot use CMS helper because of underscores in footer content links identifier
            $this->blockResource->load($model, $cmsBlock['identifier'], 'identifier');

            if (!$model->getId()) {
                continue;
            }

            $model->setContent($content);

            $this->blockResource->save($model);
        }
    }
}
