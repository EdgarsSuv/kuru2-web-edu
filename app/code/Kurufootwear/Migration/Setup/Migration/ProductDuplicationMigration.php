<?php
/**
 * @category  Kurufootwear
 * @package   Kurufootwear\Migration
 * @author    Kristaps Stalidzāns <kristapss@scandiweb.com>
 * @copyright Copyright (c) 2017 Scandiweb, Inc (http://scandiweb.com)
 * @license   http://opensource.org/licenses/OSL-3.0 The Open Software License 3.0 (OSL-3.0)
 */
namespace Kurufootwear\Migration\Setup\Migration;

use Kurufootwear\Catalog\Helper\AttributeHelper;
use Magento\Catalog\Model\Product\Action;
use Magento\Catalog\Model\ResourceModel\Product\CollectionFactory;
use Magento\CatalogInventory\Api\StockRegistryInterface;
use Magento\Eav\Model\Config;
use Magento\Catalog\Model\Product;
use Magento\Catalog\Model\CategoryFactory;
use Magento\Catalog\Model\Product\Visibility;
use Magento\Framework\App\ResourceConnection;
use Kurufootwear\Migration\Helper\FileParser;
use Magento\Indexer\Model\IndexerFactory;
use Magento\Store\Model\StoreManagerInterface;
use Magento\Framework\DB\Adapter\AdapterInterface;
use Magento\Catalog\Api\ProductRepositoryInterface;
use Scandiweb\ConfigurableProducts\Model\Product\Copier;
use Scandiweb\ConfigurableProducts\Model\CategoryPosition;
use Magento\ConfigurableProduct\Api\LinkManagementInterface;
use Magento\Catalog\Model\ResourceModel\Product\Attribute\CollectionFactory as AttributeCollectionFactory;
use Magento\UrlRewrite\Service\V1\Data\UrlRewrite;
use Magento\UrlRewrite\Model\UrlPersistInterface;
use Magento\CatalogUrlRewrite\Model\ProductUrlRewriteGenerator;

class ProductDuplicationMigration
{
    const PRODUCT_PATH = 'catalog/product/view/id/';
    const MEDIUM = 'Medium';
    const WIDE = 'Wide';

    /**
     * @var CategoryFactory
     */
    private $categoryFactory;

    /**
     * @var ProductRepositoryInterface
     */
    private $productRepository;

    /**
     * @var LinkManagementInterface
     */
    private $linkManagement;

    /**
     * @var AttributeCollectionFactory
     */
    private $attributeCollectionFactory;

    /**
     * @var StoreManagerInterface
     */
    private $storeManager;

    /**
     * @var Config
     */
    private $eavConfig;

    /**
     * @var Copier
     */
    private $productCopier;

    /**
     * @var Product
     */
    private $product;

    /**
     * @var ResourceConnection
     */
    private $resourceConnection;
    
    /**
     * @var CategoryPosition
     */
    private $categoryPosition;

    /**
     * @var StockRegistryInterface
     */
    private $stockRegistry;

    /**
     * @var FileParser
     */
    protected $fileParser;

    /**
     * @var AttributeHelper
     */
    private $attributeHelper;

    /**
     * @var Action
     */
    private $productAction;

    /**
     * @var CollectionFactory
     */
    private $productCollectionFactory;

    /**
     * @var ProductUrlRewriteGenerator
     */
    protected $productUrlRewriteGenerator;

    /**
     * @var UrlPersistInterface
     */
    protected $urlPersist;

    /**
     * SliderMigration constructor.
     * @param CategoryFactory $categoryFactory
     * @param ProductRepositoryInterface $productRepository
     * @param LinkManagementInterface $linkManagement
     * @param AttributeCollectionFactory $attributeCollectionFactory
     * @param StoreManagerInterface $storeManager
     * @param Config $eavConfig
     * @param Copier $productCopier
     * @param Product $product
     * @param ResourceConnection $resourceConnection
     * @param CategoryPosition $categoryPosition
     * @param StockRegistryInterface $stockRegistry
     * @param FileParser $fileParser
     * @param AttributeHelper $attributeHelper
     * @param CollectionFactory $productCollectionFactory
     * @param Action $productAction
     * @param ProductUrlRewriteGenerator $productUrlRewriteGenerator
     * @param UrlPersistInterface $urlPersist
     */
    public function __construct(
        CategoryFactory $categoryFactory,
        ProductRepositoryInterface $productRepository,
        LinkManagementInterface $linkManagement,
        AttributeCollectionFactory $attributeCollectionFactory,
        StoreManagerInterface $storeManager,
        Config $eavConfig,
        Copier $productCopier,
        Product $product,
        ResourceConnection $resourceConnection,
        CategoryPosition $categoryPosition,
        StockRegistryInterface $stockRegistry,
        FileParser $fileParser,
        AttributeHelper $attributeHelper,
        CollectionFactory $productCollectionFactory,
        Action $productAction,
        ProductUrlRewriteGenerator $productUrlRewriteGenerator,
        UrlPersistInterface $urlPersist
    ) {
        $this->categoryFactory = $categoryFactory;
        $this->productRepository = $productRepository;
        $this->linkManagement = $linkManagement;
        $this->attributeCollectionFactory = $attributeCollectionFactory;
        $this->storeManager = $storeManager;
        $this->eavConfig = $eavConfig;
        $this->productCopier = $productCopier;
        $this->product = $product;
        $this->resourceConnection = $resourceConnection;
        $this->categoryPosition = $categoryPosition;
        $this->stockRegistry = $stockRegistry;
        $this->fileParser = $fileParser;
        $this->attributeHelper = $attributeHelper;
        $this->productAction = $productAction;
        $this->productCollectionFactory = $productCollectionFactory;
        $this->productUrlRewriteGenerator = $productUrlRewriteGenerator;
        $this->urlPersist = $urlPersist;
    }

    /**
     *
     */
    public function createAllDuplicates()
    {
        $collection = $this->productCollectionFactory->create();
        $collection->addAttributeToFilter('type_id', 'configurable');
        $collection->addAttributeToFilter('visibility', ['eq' => Visibility::VISIBILITY_IN_SEARCH]);
        $products = $collection->load();

        foreach ($products as $p) {
            if ($p->getSku()) {
                try {
                    $originalProduct = $this->productRepository->get($p->getSku());
                    $product = clone $originalProduct;

                    if ($originalProduct->getId()) {
                        echo PHP_EOL . $originalProduct->getId() . PHP_EOL;
                        echo PHP_EOL . $originalProduct->getUrlKey() . PHP_EOL;
                        echo PHP_EOL . $originalProduct->getUrlPath() . PHP_EOL;
                        $this->urlPersist->deleteByData([
                            UrlRewrite::ENTITY_ID => $originalProduct->getId(),
                            UrlRewrite::ENTITY_TYPE => ProductUrlRewriteGenerator::ENTITY_TYPE,
                            UrlRewrite::REDIRECT_TYPE => 0,
                            UrlRewrite::STORE_ID => 0
                        ]);
                        $this->urlPersist->replace(
                            $this->productUrlRewriteGenerator->generate($originalProduct)
                        );
                        $config = $originalProduct->getTypeInstance(true);
                        $attributesArray = $config->getConfigurableAttributesAsArray($originalProduct);
                        echo PHP_EOL . $p->getSku() . PHP_EOL;

                        if (strpos($p->getSku(), 'E') !== false) {
                            $size = self::WIDE;
                        } else {
                            $size = self::MEDIUM;
                        }

                        $optionWidthId = $this->attributeHelper->getAttributeId($size, 'filterable_width');
                        $this->productAction->updateAttributes([$originalProduct->getId()], ['filterable_width' => $optionWidthId], 0);
                        $childProduct = $this->linkManagement->getChildren($p->getSku());

                        foreach ($attributesArray as $attributeItem) {
                            if ($attributeItem['attribute_code'] == 'color') {
                                foreach ($attributeItem['values'] as $valueItem) {
                                    $childProductData = $this->getChildProductsData($childProduct, $valueItem['value_index']);

                                    $originalProduct->addData(
                                        [
                                            'sku' => $childProductData['sku'],
                                            'name' => $originalProduct->getData('orig_name'),
                                            'thumbnail' => $childProductData['images']['thumbnail'],
                                            'image' => $childProductData['images']['image'],
                                            'small_image' => $childProductData['images']['small_image'],
                                            'product_duplicate' => 1,
                                            'parent_sku' => $p->getSku(),
                                            'url_key' => $originalProduct->getUrlKey() . '-' . strtolower($valueItem['label']),
                                            'filterable_width' => $optionWidthId,
                                            'product_color_data' => $valueItem['label'],
                                        ]
                                    );

                                    $originalProductDuplicate = $this->productRepository->get($childProductData['sku']);
                                    $originalProductDuplicate->addData(
                                        [
                                            'sku' => $childProductData['sku'],
                                            'name' => $originalProduct->getData('orig_name'),
                                            'thumbnail' => $childProductData['images']['thumbnail'],
                                            'image' => $childProductData['images']['image'],
                                            'small_image' => $childProductData['images']['small_image'],
                                            'product_duplicate' => 1,
                                            'parent_sku' => $p->getSku(),
                                            'url_key' => $originalProduct->getUrlKey() . '-' . strtolower($valueItem['label']),
                                            'filterable_width' => $optionWidthId,
                                            'product_color_data' => $valueItem['label'],
                                        ]
                                    );
                                    $this->productRepository->save($originalProductDuplicate);
                                    $this->updateUrlRewrite($product, $originalProductDuplicate, $valueItem['label']);
                                }
                            }
                        }
                    }
                } catch (\Exception $e) {
                    if ($e->getMessage() == 'Requested product doesn\'t exist') {
                        /**
                         * If no duplicate product is present create a copy of it
                         */
                        $duplicatedProductNew = $this->productCopier->copy($product);
                        $this->updateUrlRewrite($product, $duplicatedProductNew, $valueItem['label']);
                    }
                    echo $e->getMessage();
                }
            }
        }
    }

    /**
     * Update product duplicate attributes
     */
    public function addDuplicateAttribute()
    {
        $collection = $this->productCollectionFactory->create();
        $collection->addAttributeToFilter('type_id', 'configurable');
        $collection->addAttributeToFilter('parent_sku', ['notnull' => true]);
        $collection->addAttributeToFilter('parent_sku', ['neq' => ' ']);
        $products = $collection->load();
        $clone = null;

        foreach ($products as $p) {
            if ($p->getSku() != $p->getParentSku()) {
                echo PHP_EOL . $p->getSku() . '=>' . $p->getParentSku() . '!!!' . PHP_EOL;

                try {
                    $originalProductDuplicate = $this->productRepository->get($p->getSku());
                    $parentProduct = $this->productRepository->get($p->getParentSku());
                    $this->setUrlRewrites($parentProduct);
                    $childProduct = $this->linkManagement->getChildren($p->getSku());
                    $colorId = $this->getChildProductsColorName($childProduct, $p->getSku());
                    $childProductData = $this->getChildProductsData($childProduct, $colorId);

                    if ($colorId) {
                        echo PHP_EOL . 'Color Found' . PHP_EOL;
                        $colorLabel = $this->attributeHelper->getAttributeLabel($colorId, 'color');
                        echo PHP_EOL . $colorLabel . PHP_EOL;

                        $productDuplicateCheck = $this->productRepository->get($p->getSku());

                        if (strpos($p->getParentSku(), 'E') !== false) {
                            $size = self::WIDE;
                        } else {
                            $size = self::MEDIUM;
                        }

                        $optionWidthId = $this->attributeHelper->getAttributeId($size, 'filterable_width');
                        $this->productAction->updateAttributes([$parentProduct->getId()], ['filterable_width' => $optionWidthId], 0);

                        if ($productDuplicateCheck) {
                            $newAttributeData = [
                                'name' => $parentProduct->getData('orig_name'),
                                'product_duplicate' => 1,
                                'parent_sku' => $p->getParentSku(),
                                'url_key' => $originalProductDuplicate->getUrlKey() . '-' . strtolower($colorLabel),
                                'filterable_width' => $optionWidthId,
                                'filterable_color' => $originalProductDuplicate->getData('filterable_color'),
                                'benefits' => $originalProductDuplicate->getData('benefits'),
                                'product_color_data' => $colorLabel,
                                'visibility' => Visibility::VISIBILITY_IN_CATALOG
                            ];
                            $name = explode('- ',$originalProductDuplicate->getName());

                            if (is_array($name) & (sizeof($name) >= 2)) {
                                $newAttributeData['fename'] = $name[0];
                                $newAttributeData['fecolor'] = $name[1];
                            }
                            
                            print_r($newAttributeData);

//                            $productDuplicateCheck->addData($newAttributeData);
                            $this->productAction->updateAttributes([$productDuplicateCheck->getId()], $newAttributeData, 0);
                            echo PHP_EOL . 'id => ' . $productDuplicateCheck->getId() . PHP_EOL;
                            echo PHP_EOL . 'product status => ' . $productDuplicateCheck->getStatus() . PHP_EOL;
                            echo PHP_EOL . 'sku => ' . $productDuplicateCheck->getSku() . PHP_EOL;
                            echo PHP_EOL . 'name => ' . $productDuplicateCheck->getName() . PHP_EOL;
                            echo PHP_EOL . 'parent_sku => ' . $productDuplicateCheck->getParentSku() . PHP_EOL;
                            echo PHP_EOL . 'colorLabel => ' . $colorId ? $productDuplicateCheck->getProductColorData() : 'No color found' . PHP_EOL;
                            echo PHP_EOL . '[32mDuplicate product present![32m' . PHP_EOL;
                            $this->setUrlRewrites($productDuplicateCheck);

                            if ($colorLabel) {
                                $this->updateUrlRewrite($parentProduct, $productDuplicateCheck, $colorLabel);
                            }

                            echo PHP_EOL . $productDuplicateCheck->getName() . PHP_EOL;
                            echo PHP_EOL . $colorId ? $colorLabel : 'No color found' . PHP_EOL;
                        }
                    }
                } catch (\Exception $e) {
                    echo PHP_EOL . $e->getMessage() . PHP_EOL;
                }
            }
        }
    }

    /**
     * @param array $products
     * @param $category
     */
    public function generateDuplicates(array $products, $category)
    {
        $categoryId = $this->getCategoryId($category);
        $colorOptions = $this->getColorOptions();

        foreach ($products as $productSku => $position) {
            try {
                $product = $this->product->load($this->productRepository->get($productSku)->getId());
                echo PHP_EOL . 'SKU => ' . $productSku . '[36m';

                if ($product) {
                    echo PHP_EOL . '[32mProduct Found![32m';
                    $originalProductDuplicate = clone $product;
                    $childProduct = $this->linkManagement->getChildren($product->getSku());

                    foreach ($position as $positionId => $color) {
                        $colorId = $this->getAttributeColorId($colorOptions, $color);
                        $childProductData = $this->getChildProductsData($childProduct, $colorId);

                        echo PHP_EOL .'[36m' . $colorId . ' => ' . $color . ' name => ' . $product->getName() . '[36m';

                        if ($childProductData && $colorId) {
                            /**
                             * Sets new product data to be duplicated saving all other data
                             */
                            $product->addData(
                                [
                                    'price' => $childProductData['price'],
                                    'thumbnail' =>  $childProductData['images']['thumbnail'],
                                    'image' =>  $childProductData['images']['image'],
                                    'small_image' =>  $childProductData['images']['small_image'],
                                    'name' => $originalProductDuplicate->getData('orig_name'),
                                    'sku' => $originalProductDuplicate->getSku() . '-' . strtolower($color),
                                    'status' => 1,
                                    'parent_sku' => $originalProductDuplicate->getSku(),
                                    'product_duplicate' => 1,
                                    // Will add filtabre color when
                                    // 'filterable_color'=> null,
                                    'visibility' => 4,
                                    'url_key' => $product->getUrlKey() . '-' . strtolower($color),
                                    'product_color_data' => $color
                                ]
                            );

                            try {
                                $productDuplicateCheck = $this->product->load($this->productRepository->get($originalProductDuplicate->getSku() . '-' . strtolower($color))->getId());

                                if ($productDuplicateCheck) {
                                    echo PHP_EOL . '[32mDuplicate product present![32m';
                                    $this->categoryPosition->updateProductPosition($productDuplicateCheck, [['position' => $positionId, 'category_id' => $categoryId]]);
                                }
                            } catch (\Exception $e) {
                                echo PHP_EOL . '[32mCreating Duplicate![32m';
                                $this->deleteOldProduct($product);
                                $duplicatedProduct = $this->productCopier->copy($product);
                                $stockItem = $this->stockRegistry->getStockItemBySku($duplicatedProduct->getSku());
                                $stockItem->setIsInStock(1);
                                $stockItem->setQty(1);
                                $this->stockRegistry->updateStockItemBySku($duplicatedProduct->getSku(), $stockItem);
                                $this->updateUrlRewrite($originalProductDuplicate, $duplicatedProduct, $color);
                                $this->categoryPosition->updateProductPosition($duplicatedProduct, [['position' => $positionId, 'category_id' => $categoryId]]);
                            }

                            echo PHP_EOL . '[32mDone![32m';
                        } else {
                            echo PHP_EOL . '[31mSkipped. Because of missing child product data[31m';
                        }
                    }
                }
            } catch (\Exception $e) {
                echo PHP_EOL . '[31mProduct Not found';
            }
        }
    }

    /**
     * @param $position
     * @param $color
     * @param $filterableColor
     * @param $category
     * @param $productSku
     */
    public function enableDuplicates($position, $color, $filterableColor, $category, $productSku)
    {
        $product = null;

        try {
            $product = $this->productRepository->get($productSku);
            echo PHP_EOL . 'SKU => ' . $productSku . '[36m';

            if ($product) {
                $this->productAction->updateAttributes([$product->getId()], ['visibility' => Visibility::VISIBILITY_BOTH], 0);
                $this->categoryPosition->updateProductPositionMigration($product, ['position' => $position, 'category_ids' => $product->getCategoryIds()]);

            }
        } catch (\Exception $e) {
            echo PHP_EOL . '[31mProduct ' . $productSku . ' Not found: ' . $e->getMessage() . ' ' . PHP_EOL;
        }
    }

    /**
     * @param $position
     * @param $color
     * @param $filterableColor
     * @param $category
     * @param $productSku
     */
    public function generateDuplicatesCSV($position, $color, $filterableColor, $category, $productSku)
    {
        $product = null;
        $categoryId = $this->getCategoryId($category);
        $colorOptions = $this->getColorOptions();

        try {
            $product = $this->productRepository->get($productSku);
            echo PHP_EOL . 'SKU => ' . $productSku . '[36m';

            if ($product) {
                $originalProductDuplicate = clone $product;
                echo PHP_EOL . '[32mProduct Found![32m' . PHP_EOL;
                $childProduct = $this->linkManagement->getChildren($productSku);

                echo 'color => ' . $color . PHP_EOL;
                $colorId = $this->getAttributeColorId($colorOptions, $color);
                echo 'colorId => ' . $colorId . PHP_EOL;
                $childProductData = $this->getChildProductsData($childProduct, $colorId);
                echo 'Not failed';
                echo PHP_EOL .'[36m' . $colorId . ' => ' . $color . ' name => ' . $product->getName() . '[36m';

                if ($childProductData && $colorId) {

                    try {
                        $filterableColorOptionId = [$this->attributeHelper->getAttributeId($filterableColor, 'filterable_color')];
                    } catch (\Exception $e) {
                        echo PHP_EOL . '[32mOption Id for '. $filterableColor . 'not found product stll creating[32m';
                        $filterableColorOptionId = null;
                    }

                    if (strpos($productSku, 'E') !== false) {
                        $size = self::WIDE;
                    } else {
                        $size = self::MEDIUM;
                    }

                    $optionWidthId = $this->attributeHelper->getAttributeId($size,'filterable_width');

                    /**
                     * Sets new product data to be duplicated saving all other data
                     */

                    echo PHP_EOL . 'Duplicate SKU => ' . $childProductData['sku'] . PHP_EOL;

                    try {
                        $productDuplicateCheck = $this->productRepository->get($childProductData['sku']);
                        echo PHP_EOL . '[32mLoading ';

                        if ($productDuplicateCheck) {
                            $product->addData(
                                [
                                    'id' => $productDuplicateCheck->getId(),
                                    'sku' => $childProductData['sku'],
                                    'name' => $originalProductDuplicate->getData('orig_name'),
                                    'product_duplicate' => 1,
                                    'parent_sku' => $productSku,
                                    'visibility' => 4,
                                    'status' => $productDuplicateCheck->getStatus(),
                                    'filterable_color'=> $filterableColorOptionId,
                                    'url_key' => $product->getUrlKey() . '-' . strtolower($color),
                                    'filterable_width' => $optionWidthId,
                                    'category_ids' => $originalProductDuplicate->getCategoryIds(),
                                    'product_color_data' => $color
                                ]
                            );
                            $productDuplicateCheck->setData($product->getData());
                            echo PHP_EOL . '[32mDuplicate product present![32m' . PHP_EOL;
                            $productDuplicateCheck = $this->productRepository->save($productDuplicateCheck);
                            $this->categoryPosition->updateProductPosition($productDuplicateCheck, [['position' => $position, 'category_ids' => $product->getCategoryIds()]]);
                            $this->updateUrlRewrite($originalProductDuplicate, $productDuplicateCheck, $color);
                        }
                    } catch (\Exception $e) {
                        echo $e->getMessage();
                    }

                    echo PHP_EOL . '[32mDone![32m';
                    unset($product);
                    unset($originalProductDuplicate);
                } else {
                    echo PHP_EOL . '[31mSkipped. Because of missing child product data[31m';
                }
            }
        } catch (\Exception $e) {
            echo PHP_EOL . '[31mProduct ' . $productSku . ' Not found: ' . $e->getMessage() . ' ' . PHP_EOL;
        }
    }

    /**
     * @param array $newAttributeData
     */
    public function updateAccessories(array $newAttributeData)
    {

        foreach ($newAttributeData as $sku) {
            try {
                $product = $this->productRepository->get($sku);

                if ($product) {
                    $this->productAction->updateAttributes([$product->getId()], ['visibility' => Visibility::VISIBILITY_BOTH], 0);
                }
            } catch (\Exception $e) {}
        }
    }

    /**
     * Function gets configurable child product price and images
     *
     * @param $products
     * @param $colorCode
     * @return array
     */
    public function getChildProductsData($products, $colorCode)
    {
        $productData = [];

        foreach ($products as $item) {
            if ($item->getColor() == $colorCode) {
                $sku = explode('-', $item->getSku());
                $productData['price'] = $item['price'];
                $productData['images'] = [
                    'thumbnail' =>  $item->getThumbnail(),
                    'image' =>  $item->getImage(),
                    'small_image' =>  $item->getSmallImage(),
                ];
                $productData['filterable_color'] = $item['filterable_color'];
                $productData['sku'] = $sku[0];
            }
        }

        return $productData;
    }


    /**
     * Function gets configurable child product color id name
     *
     * @param $products
     * @param $productSku
     * @return int
     */
    public function getChildProductsColorName($products, $productSku)
    {
        foreach ($products as $item) {
            $sku = explode('-', $item->getSku());

            if ($productSku == $sku[0]) {
                return $item->getColor();
            }
        }
    }

    /**
     * Function updates new product URL paths
     *
     * @param Product $product
     * @param Product $copiedProduct
     * @param $attribute
     */
    public function updateUrlRewrite(Product $product, Product $copiedProduct, $attribute)
    {
        $connection = $this->resourceConnection->getConnection();
        $tableName = $this->resourceConnection->getTableName('url_rewrite');
        $productId = $product->getId();
        $copiedProductId = $copiedProduct->getId();
        $results = $this->pathQuery(
            $connection,
            $tableName,
            $product,
            self::PRODUCT_PATH . $productId
        );

        /**
         * Results for plain product
         */
        if ($results) {
            $connection->delete(
                $tableName,
                [
                    'request_path = ?' => $results[0]['request_path'] . '?color=' . $attribute,
                    'entity_id != ?' => $copiedProduct->getId()

                ]
            );

            $connection->delete(
                $tableName,
                [
                    'request_path = ?' => $results[0]['request_path'],
                    'entity_id != ?' => $product->getId()

                ]
            );

            $this->updatePath(
                $connection,
                $tableName,
                $results[0],
                $attribute,
                $copiedProduct,
                self::PRODUCT_PATH . $copiedProductId
            );
        }

        /**
         * Updates category Urls
         */
        foreach ($product->getCategoryIds() as $items) {
            $results = $this->pathQuery(
                $connection,
                $tableName,
                $product,
                self::PRODUCT_PATH . $productId . '/category/' . $items
            );

            if ($results) {
                $this->updatePathCategory(
                    $connection,
                    $tableName,
                    $results[0],
                    $attribute,
                    $copiedProduct,
                    self::PRODUCT_PATH . $copiedProductId . '/category/' . $items
                );
            }
        }
    }

    /**
     * Universal function to get table information based on product for URL rewrite
     *
     * @param AdapterInterface $connection
     * @param $tableName
     * @param $path
     * @return array
     */
    public function existingPathQuery(AdapterInterface $connection, $tableName, $path)
    {
        $sql = $connection->select()
            ->from($tableName)
            ->where('request_path = ?', $path);

        return $connection->fetchAll($sql);
    }

    /**
     * @param AdapterInterface $connection
     * @param $tableName
     * @param $path
     */
    public function deletePath(AdapterInterface $connection, $tableName, $path)
    {
        $connection->delete($tableName, ['request_path = ?' => $path]);
    }

    /**
     * Function gets configurable child product price and images
     *
     * @param $products
     * @param $colorLabel
     * @return array
     */
    public function getChildProductsByColorName($products, $colorLabel)
    {
        $productData = [];
        $colorCode = $this->attributeHelper->getAttributeId($colorLabel ,'color');

        foreach ($products as $item) {
            if ($item->getColor() == $colorCode) {
                $productData['price'] = $item['price'];
                $productData['images'] = [
                    'thumbnail' =>  $item->getThumbnail(),
                    'image' =>  $item->getImage(),
                    'small_image' =>  $item->getSmallImage(),
                ];
                $productData['filterable_color'] = $item['filterable_color'];
                $sku = explode('-', $item->getSku());
                $productData['sku'] = $sku[0];
            }
        }

        return $productData;
    }


    /**
     * Universal function to get table information based on product for URL rewrite
     *
     * @param AdapterInterface $connection
     * @param $tableName
     * @param Product $product
     * @param $path
     * @return array
     */
    public function pathQuery(AdapterInterface $connection, $tableName, Product $product, $path)
    {
        $sql = $connection->select()
            ->from($tableName)
            ->where('entity_id = ?', $product->getId())
            ->where('target_path = ?', $path);

        return $connection->fetchAll($sql);
    }

    /**
     * Updates product path in database
     *
     * @param AdapterInterface $connection
     * @param $tableName
     * @param $result
     * @param $attribute
     * @param Product $product
     * @param $path
     */
    public function updatePath(AdapterInterface $connection, $tableName, $result, $attribute, Product $product, $path)
    {
        $connection->update(
            $tableName,
            [
                'request_path' => $result['request_path'] . '?color=' . $attribute
            ],
            [
                'target_path = ?' => $path,
                'entity_id = ?' => $product->getId()
            ]
        );
    }

    /**
     * Updates product path in database for ca
     *
     * @param AdapterInterface $connection
     * @param $tableName
     * @param $result
     * @param $attribute
     * @param Product $product
     * @param $path
     */
    public function updatePathCategory(AdapterInterface $connection, $tableName, $result, $attribute, Product $product, $path)
    {
        $sql = $connection->select()
            ->from($tableName)
            ->where('request_path = ?', $result['request_path'] . '?color=' . $attribute)
            ->where('entity_id <> ?', $product->getId());

        $resultSql = $connection->fetchAll($sql);

        if ($resultSql) {
            $connection->delete(
                $tableName,
                [
                    'request_path = ?' => $result['request_path'] . '?color=' . $attribute,
                    'entity_id != ?' => $product->getId()
                ]
            );
        }

        $connection->update(
            $tableName,
            [
                'request_path' => $result['request_path'] . '?color=' . $attribute
            ],
            [
                'target_path = ?' => $path,
                'entity_id = ?' => $product->getId()
            ]
        );
    }

    /**
     * Fixes Url Rewrites
     *
     * @param Product $product
     */
    public function setUrlRewrites(Product $product)
    {
        $this->urlPersist->deleteByData([
            UrlRewrite::ENTITY_ID => $product->getId(),
            UrlRewrite::ENTITY_TYPE => ProductUrlRewriteGenerator::ENTITY_TYPE,
            UrlRewrite::REDIRECT_TYPE => 0,
            UrlRewrite::STORE_ID => 0
        ]);
        $this->urlPersist->replace(
            $this->productUrlRewriteGenerator->generate($product)
        );
    }

    /**
     * Returns Color attribute options
     *
     * @return array
     */
    public function getColorOptions()
    {
        $attribute = $this->eavConfig->getAttribute('catalog_product', 'color');
        $options = $attribute->getSource()->getAllOptions();

        return $options;
    }

    /**
     * Returns color based on option
     *
     * @param $options
     * @param $colorLabel
     * @return array
     */
    public function getAttributeColorId($options, $colorLabel)
    {
        foreach ($options as $option) {
            if ($option['label'] == $colorLabel) {
                return $option['value'];
            }
        }
    }

    /**
     * This function deletes old product
     *
     * @param Product $product
     */
    public function deleteOldProduct(Product $product)
    {
        try {
            $oldProduct = $this->productRepository->get($product->getSku());

            if ($oldProduct) {
                $this->productRepository->delete($oldProduct);
            }
        } catch (\Exception $e) {

        }
    }

    /**
     * @param $categoryTitle
     * @return null
     */
    public function getCategoryId($categoryTitle)
    {
        $categoryId = null;
        $collection = $this->categoryFactory->create()
            ->getCollection()
            ->addAttributeToFilter('name' ,$categoryTitle)
            ->setPageSize(1);

        if ($collection->getSize()) {
            $categoryId = $collection->getFirstItem()->getId();
        }

        return $categoryId;
    }
}
