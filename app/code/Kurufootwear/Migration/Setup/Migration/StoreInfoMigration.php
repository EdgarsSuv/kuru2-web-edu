<?php
/**
 * @vendor      Kurufootwear
 * @module      Kurufootwear_Migration
 * @author      Jelena Sorohova <info@scandiweb.com>
 * @copyright   Copyright (c) 2017 Scandiweb, Ltd (http://scandiweb.com)
 * @license     http://opensource.org/licenses/afl-3.0.php Academic Free License (AFL 3.0)
 */
namespace Kurufootwear\Migration\Setup\Migration;

use Kurufootwear\Migration\Api\UpgradeMigrationInterface;
use Magento\Framework\Setup\SetupInterface;
use Magento\Config\Model\ResourceModel\Config;
use Magento\Config\Block\System\Config\Form;
use Kurufootwear\Migration\Helper\FileHelper;

class StoreInfoMigration implements UpgradeMigrationInterface
{
    /**
     * @var Config
     */
    private $_resourceConfig;

    /**
     * @var FileHelper
     */
    private $_fileHelper;

    /**
     * StoreInfoMigration constructor.
     * @param Config $resourceConfig
     * @param FileHelper $fileHelper
     */
    public function __construct(
        Config $resourceConfig,
        FileHelper $fileHelper
    ) {
        $this->_resourceConfig = $resourceConfig;
        $this->_fileHelper = $fileHelper;
    }

    /**
     * @inheritDoc
     */
    public function apply(SetupInterface $setup = null)
    {
        $storeInfo = array(
            array(
                'name' => 'hours',
                'value' => 'Mon - Fri 7am MST - 5pm MST'
            ),
            array(
                'name' => 'street_line1',
                'value' => '4416 S Century Dr'
            ),
            array(
                'name' => 'city',
                'value' => 'Salt Lake City'
            ),
            array(
                'name' => 'region_id',
                'value' => '58'
            ),
            array(
                'name' => 'email',
                'value' => 'gurus@kurufootwear.com'
            )
        );

        foreach ($storeInfo as $info) {
            $this->_resourceConfig->saveConfig(
                'general/store_information/' . $info['name'],
                $info['value'],
                Form::SCOPE_DEFAULT,
                0
            );
        }
    }
}
