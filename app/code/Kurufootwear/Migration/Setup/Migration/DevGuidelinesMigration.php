<?php
/**
 * @vendor Kurufootwear
 * @module Kurufootwear_Migration
 * @author Viesturs Ruzans <viestursr@scandiweb.com>
 * @copyright Copyright (c) 2017 Scandiweb, Ltd (http://scandiweb.com)
 * @license http://opensource.org/licenses/afl-3.0.php Academic Free License (AFL 3.0)
 */
namespace Kurufootwear\Migration\Setup\Migration;

use Kurufootwear\Migration\Api\UpgradeMigrationInterface;
use Magento\Framework\Setup\SetupInterface;
use Kurufootwear\Migration\Helper\FileParser;
use Magento\Cms\Model\PageFactory;

class DevGuidelinesMigration implements UpgradeMigrationInterface
{
    /**
     * @var PageFactory
     */
    private $pageFactory;

    /**
     * @var FileParser
     */
    private $fileParser;

    /**
     * UpgradeData constructor.
     * @param PageFactory $pageFactory
     * @param FileParser $fileParser
     */
    public function __construct(
        PageFactory $pageFactory,
        FileParser $fileParser
    ) {
        $this->pageFactory = $pageFactory;
        $this->fileParser = $fileParser;
    }

    /**
     * @inheritDoc
     */
    public function apply(SetupInterface $setup = null)
    {
        $cmsPages = array(
            array(
                'identifier' => 'guidelines',
                'heading' => 'DEV Guidelines',
                'file_name' => 'guidelines.html'
            )
        );

        foreach ($cmsPages as $cmsPage) {
            $content = '';

            if ($cmsPage['file_name'] != '') {
                $content = $this->fileParser->getHtmlContent('pages/' . $cmsPage['file_name']);
            }

            $pageData = array(
                'title' => $cmsPage['heading'],
                'content_heading' => $cmsPage['heading'],
                'identifier' => $cmsPage['identifier'],
                'content' => $content,
                'page_layout' => '1column',
                'stores' => array(0)
            );

            $page = $this->pageFactory->create()->load($pageData['identifier'], 'identifier');

            if ($page->getId()) {
                $page->setTitle($pageData['title'])
                    ->setContent($pageData['content'])
                    ->setStores($pageData['stores'])
                    ->setPageLayout($pageData['page_layout'])
                    ->setContentHeading($pageData['content_heading'])
                    ->save();
            } else {
                $page->addData($pageData)
                    ->save();
            }
        }
    }
}
