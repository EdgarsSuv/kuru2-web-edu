<?php
/**
 * @vendor      Kurufootwear
 * @module      Kurufootwear_Migration
 * @author      Kristaps Stalidzans <info@scandiweb.com>
 * @copyright   Copyright (c) 2017 Scandiweb, Ltd (http://scandiweb.com)
 * @license     http://opensource.org/licenses/afl-3.0.php Academic Free License (AFL 3.0)
 */
namespace Kurufootwear\Migration\Setup\Migration;

use Kurufootwear\Migration\Api\UpgradeMigrationInterface;
use Kurufootwear\Migration\Helper\Configurator;
use Magento\Framework\Setup\SetupInterface;

class EnableAmastyAjax implements UpgradeMigrationInterface
{
    /**
     * @var Configurator
     */
    private $configurator;

    /**
     * EnableAmastyAjax constructor.
     * @param Configurator $configurator
     */
    public function __construct(
        Configurator $configurator
    ) {
        $this->configurator = $configurator;
    }

    /**
     * @inheritDoc
     */
    public function apply(SetupInterface $setup = null)
    {
        $this->configurator->save(
            'amshopby/general/ajax_enabled',
            '1',
            Configurator::SCOPE_WEBSITES
        );

        $this->configurator->save(
            'amshopby/general/ajax_scroll_up',
            '1',
            Configurator::SCOPE_WEBSITES
        );
    }
}
