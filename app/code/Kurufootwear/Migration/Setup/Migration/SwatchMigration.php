<?php

namespace Kurufootwear\Migration\Setup\Migration;

use Kurufootwear\Migration\Api\UpgradeMigrationInterface;
use Kurufootwear\Migration\Helper\FileParser;
use Kurufootwear\Catalog\Helper\AttributeHelper;
use Magento\Framework\Setup\SetupInterface;
use Magento\Framework\App\Filesystem\DirectoryList;
use Magento\Swatches\Model\Swatch;
use Magento\Swatches\Model\ResourceModel\Swatch\CollectionFactory as SwatchCollectionFactory;
use Magento\Store\Model\StoreRepository;

class SwatchMigration implements UpgradeMigrationInterface
{
    /** @var AttributeHelper  */
    private $attributeHelper;
    
    /** @var SwatchCollectionFactory  */
    private $swatchCollectionFactory;
    
    /** @var StoreRepository  */
    private $storeRepository;
    
    /** @var \Magento\Swatches\Helper\Media  */
    private $swatchHelper;
    
    /**
     * SwatchMigration constructor.
     *
     * @param AttributeHelper $attributeHelper
     * @param \Magento\Swatches\Helper\Media $swatchHelper
     * @param SwatchCollectionFactory $swatchCollectionFactory
     * @param StoreRepository $storeRepository
     */
    public function __construct(
        AttributeHelper $attributeHelper,
        \Magento\Swatches\Helper\Media $swatchHelper,
        SwatchCollectionFactory $swatchCollectionFactory,
        StoreRepository $storeRepository
    ) {
        $this->attributeHelper = $attributeHelper;
        $this->swatchHelper = $swatchHelper;
        $this->swatchCollectionFactory = $swatchCollectionFactory;
        $this->storeRepository = $storeRepository;
    }
    
    /**
     * @param SetupInterface|null $setup
     */
    public function apply(SetupInterface $setup = null)
    {
        $colors = $this->attributeHelper->getAllAttributeOptions('color');
        foreach ($colors as $color) {
            $label = $color->getLabel();
            $optionId = $color->getValue();
            if (empty($label) || empty($optionId)) {
                continue;
            }
            
            // Find the file
            $sourceFile = DirectoryList::APP . '/' . FileParser::PATH_TO_DATA . 'media/swatches/' . $optionId . '.jpg';
            if (file_exists($sourceFile)) {
                // Copy file to media dir
                $destFile = DirectoryList::PUB . '/' . DirectoryList::MEDIA . '/attribute/swatch';
                
                $parts = str_split($optionId);
                $swatchPath = '';
                for ($i = 0; $i < count($parts) - 1; $i++) {
                    $swatchPath .= '/' . $parts[$i];
                }
                
                $destFile .= $swatchPath;
                if (!file_exists($destFile)) {
                    mkdir($destFile, 0775, true);
                }
                
                $destFile .= '/' . $optionId . '.jpg';
                if (file_exists($destFile)) {
                    unlink($destFile);
                }
                copy($sourceFile, $destFile);
                
                // Update the color attribute
                $stores = $this->storeRepository->getList();
                foreach ($stores as $store) {
                    $swatch = $this->getSwatch($store->getId(), $optionId);
                    
                    if (!$swatch->hasData()) {
                        $swatch->setOptionId($optionId);
                        $swatch->setStoreId($store->getId());
                    }
                    
                    $swatch->setType(Swatch::SWATCH_TYPE_VISUAL_IMAGE);
                    $swatch->setValue($swatchPath . '/' . $optionId . '.jpg');
                    $swatch->save();
                }
            }
        }
    }
    
    /**
     * @param $storeId
     * @param $optionId
     *
     * @return \Magento\Framework\DataObject
     */
    private function getSwatch($storeId, $optionId)
    {
        $collection = $this->swatchCollectionFactory->create();
        $collection
            ->addStoreFilter($storeId)
            ->addFilterByOptionsIds([$optionId]);
        return $collection->getFirstItem();
    }
}
