<?php
/**
 * @vendor      Kurufootwear
 * @module      Kurufootwear_Migration
 * @author      Jelena Sorohova <info@scandiweb.com>
 * @copyright   Copyright (c) 2017 Scandiweb, Ltd (http://scandiweb.com)
 * @license     http://opensource.org/licenses/afl-3.0.php Academic Free License (AFL 3.0)
 */
namespace Kurufootwear\Migration\Setup\Migration;

use Kurufootwear\Migration\Api\UpgradeMigrationInterface;
use Magento\Framework\Setup\SetupInterface;
use Magento\Framework\Filesystem;
use Kurufootwear\Migration\Helper\FileParser;
use Magento\Cms\Model\BlockFactory;
use Kurufootwear\Migration\Helper\FileHelper;

class MinicartShippingPromoBlockMigration implements UpgradeMigrationInterface
{
    /**
     * @var Filesystem
     */
    private $filesystem;

    /**
     * @var BlockFactory
     */
    private $blockFactory;

    /**
     * @var FileParser
     */
    private $fileParser;

    /**
     * @var FileHelper
     */
    private $fileHelper;

    /**
     * MinicartShippingPromoBlockMigration constructor.
     * @param Filesystem $filesystem
     * @param BlockFactory $blockFactory
     * @param FileParser $fileParser
     * @param FileHelper $fileHelper
     */
    public function __construct(
        Filesystem $filesystem,
        BlockFactory $blockFactory,
        FileParser $fileParser,
        FileHelper $fileHelper
    ) {
        $this->filesystem = $filesystem;
        $this->blockFactory = $blockFactory;
        $this->fileParser = $fileParser;
        $this->fileHelper = $fileHelper;
    }

    /**
     * @inheritDoc
     */
    public function apply(SetupInterface $setup = null)
    {
        $this->fileHelper->copyMediaFiles(['map.svg'], 'icons');

        $cmsBlocks = [
            [
                'file_name' => 'minicart_shipping_promo_block.html',
                'title' => 'Minicart Shipping Promo Block',
                'identifier' => 'minicart_shipping_promo_block'
            ]
        ];

        foreach ($cmsBlocks as $cmsBlock) {
            $content = '';

            if ($cmsBlock['file_name'] != '') {
                $content = $this->fileParser->getHtmlContent('blocks/' . $cmsBlock['file_name']);
            }

            $blockData = [
                'title' => $cmsBlock['title'],
                'identifier' => $cmsBlock['identifier'],
                'content' => $content,
                'stores' => [0]
            ];

            $block = $this->blockFactory->create()->load($blockData['identifier'], 'identifier');

            if ($block->getId()) {
                $block->setTitle($blockData['title'])
                    ->setContent($blockData['content'])
                    ->setStores($blockData['stores'])
                    ->save();
            } else {
                $block->addData($blockData)
                    ->save();
            }
        }
    }
}
