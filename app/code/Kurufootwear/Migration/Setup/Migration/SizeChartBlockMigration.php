<?php
/**
 * @vendor      Kurufootwear
 * @module      Kurufootwear_Migration
 * @author      Kristaps Stalidzans <info@scandiweb.com>
 * @copyright   Copyright (c) 2017 Scandiweb, Ltd (http://scandiweb.com)
 * @license     http://opensource.org/licenses/afl-3.0.php Academic Free License (AFL 3.0)
 */
namespace Kurufootwear\Migration\Setup\Migration;

use Psr\Log\LoggerInterface;
use Magento\Framework\Filesystem;
use Magento\Cms\Model\BlockFactory;
use Magento\Catalog\Model\Product\Action;
use Magento\Framework\Setup\SetupInterface;
use Kurufootwear\Migration\Helper\FileParser;
use Magento\Framework\Api\SearchCriteriaBuilder;
use Magento\Catalog\Api\ProductRepositoryInterface;
use Kurufootwear\Migration\Api\UpgradeMigrationInterface;
use Magento\Catalog\Model\ResourceModel\Product\CollectionFactory as ProductCollectionFactory;

class SizeChartBlockMigration implements UpgradeMigrationInterface
{
    /**
     * @var Filesystem
     */
    private $filesystem;

    /**
     * @var BlockFactory
     */
    private $blockFactory;

    /**
     * @var FileParser
     */
    private $fileParser;

    /**
     * @var ProductCollectionFactory
     */
    private $productCollectionFactory;

    /**
     * @var SearchCriteriaBuilder
     */
    private $searchCriteriaBuilder;

    /**
     * @var ProductRepositoryInterface
     */
    private $productRepository;

    /**
     * @var Action
     */
    private $productAction;

    /**
     * @var LoggerInterface
     */
    private $logger;

    /**
     * @var int
     */
    private $blockId = 0;

    /**
     * FooterBlocksMigration constructor.
     * @param Filesystem $filesystem
     * @param BlockFactory $blockFactory
     * @param FileParser $fileParser
     * @param Action $productAction
     * @param ProductCollectionFactory $productCollectionFactory
     * @param SearchCriteriaBuilder $searchCriteriaBuilder
     * @param ProductRepositoryInterface $productRepository
     * @param LoggerInterface $logger
     */
    public function __construct(
        Filesystem $filesystem,
        BlockFactory $blockFactory,
        FileParser $fileParser,
        Action $productAction,
        ProductCollectionFactory $productCollectionFactory,
        SearchCriteriaBuilder $searchCriteriaBuilder,
        ProductRepositoryInterface $productRepository,
        LoggerInterface $logger
    ) {
        $this->filesystem = $filesystem;
        $this->blockFactory = $blockFactory;
        $this->fileParser = $fileParser;
        $this->productCollectionFactory = $productCollectionFactory;
        $this->searchCriteriaBuilder = $searchCriteriaBuilder;
        $this->productRepository = $productRepository;
        $this->productAction = $productAction;
        $this->logger = $logger;
    }

    /**
     * @inheritDoc
     */
    public function apply(SetupInterface $setup = null)
    {
        $cmsBlocks = [
            [
                'file_name' => 'size_chart.html',
                'title' => 'Size chart block',
                'identifier' => 'size_chart'
            ],
            [
                'file_name' => 'size_chart_socks.html',
                'title' => 'Size chart block for Socks PDP',
                'identifier' => 'size_chart_socks'
            ]
        ];

        foreach ($cmsBlocks as $cmsBlock) {
            $content = '';

            if ($cmsBlock['file_name'] != '') {
                $content = $this->fileParser->getHtmlContent('blocks/' . $cmsBlock['file_name']);
            }

            $blockData = [
                'title' => $cmsBlock['title'],
                'identifier' => $cmsBlock['identifier'],
                'content' => $content,
                'stores' => [0]
            ];

            $block = $this->blockFactory->create()->load($blockData['identifier'], 'identifier');

            // Check for Shoes block id / Socks add manually
            if ($cmsBlock['identifier'] === 'size_chart') {
                $this->blockId = $block->getId();
            }

            if ($block->getId()) {
                $block->setTitle($blockData['title'])
                    ->setContent($blockData['content'])
                    ->setStores($blockData['stores'])
                    ->save();
            } else {
                $block->addData($blockData)
                    ->save();
            }
        }

        $searchCriteria = $this->searchCriteriaBuilder
            ->addFilter('sku', null, 'neq')
            ->addFilter('name', null, 'neq')
            ->addFilter('type_id', 'configurable', 'eq')
            ->create();
        $productIds = [];

        try {
            $repositoryList = $this->productRepository->getList($searchCriteria);

            foreach ($repositoryList->getItems() as $repositoryItem) {
                array_push($productIds, $repositoryItem->getId());
            }

            $this->productAction->updateAttributes($productIds, ['size_guide_cms_block' => $this->blockId], 0);
        } catch (\Exception $e) {
            $this->logger->error($e->getMessage());
        }
    }
}
