<?php
/**
 * @category  Kurufootwear
 * @package   Kurufootwear\Migration
 * @author    Kristaps Stalidzāns <kristapss@scandiweb.com>
 * @copyright Copyright (c) 2017 Scandiweb, Inc (http://scandiweb.com)
 * @license   http://opensource.org/licenses/OSL-3.0 The Open Software License 3.0 (OSL-3.0)
 */
namespace Kurufootwear\Migration\Setup\Migration;

use Magento\Framework\Setup\SetupInterface;
use Kurufootwear\Migration\Helper\FileParser;
use Magento\Framework\App\Filesystem\DirectoryList;
use Kurufootwear\Migration\Api\UpgradeMigrationInterface;

class ProductDuplicationCSVMigration extends ProductDuplicationMigration implements UpgradeMigrationInterface
{
    const ACCESSORIES = [
        410009,
        410008,
        410001
    ];
    const MENS_CATEGORY_NAME = 'Men\'s Shoes';
    const WOMENS_CATEGORY_NAME = 'Women\'s Shoes';

    /**
     * @inheritDoc
     */
    public function apply(SetupInterface $setup = null)
    {
        $this->addDuplicateAttribute();
        /**
         * Changes product visibility
         */
        $this->updateAccessories(self::ACCESSORIES);
    }
}
