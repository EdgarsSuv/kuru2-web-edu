<?php
/**
 * @vendor      Kurufootwear
 * @module      Kurufootwear_Migration
 * @author      Jelena Sorohova <info@scandiweb.com>
 * @copyright   Copyright (c) 2017 Scandiweb, Ltd (http://scandiweb.com)
 * @license     http://opensource.org/licenses/afl-3.0.php Academic Free License (AFL 3.0)
 */
namespace Kurufootwear\Migration\Setup\Migration;

use Kurufootwear\Migration\Api\UpgradeMigrationInterface;
use Magento\Framework\Setup\SetupInterface;
use Kurufootwear\Migration\Helper\FileParser;
use Magento\Framework\App\TemplateTypesInterface;
use Magento\Email\Model\Template;
use Magento\Email\Model\TemplateFactory;
use Magento\Email\Model\ResourceModel\TemplateFactory as TemplateResourceFactory;

class EmailTemplatesMigration implements UpgradeMigrationInterface
{
    /**
     * @var FileParser $fileParser
     */
    private $fileParser;

    /**
     * @var TemplateFactory $templateFactory
     */
    private $templateFactory;

    /**
     * @var TemplateResourceFactory $templateResourceFactory
     */
    private $templateResourceFactory;

    /**
     * EmailTemplatesMigration constructor.
     * @param FileParser $fileParser
     * @param TemplateFactory $templateFactory
     * @param TemplateResourceFactory $templateResourceFactory
     */
    public function __construct(
        FileParser $fileParser,
        TemplateFactory $templateFactory,
        TemplateResourceFactory $templateResourceFactory
    ) {
        $this->fileParser = $fileParser;
        $this->templateFactory = $templateFactory;
        $this->templateResourceFactory = $templateResourceFactory;
    }

    /**
     * @inheritDoc
     */
    public function apply(SetupInterface $setup = null)
    {
        $emailTemplates = [
            [
                'template_file' => 'rma_update.html',
                'template_code' => 'RMA Update Email for Customer',
                'template_subject' => 'Update on Your RMA {{var request.getTextId()}}'
            ],
            [
                'template_file' => 'aw_rma_base_modified.html',
                'template_code' => 'Modified AW RMA Base template for customer',
                'template_subject' => 'Modified AW RMA Base template for customer'
            ]
        ];

        foreach ($emailTemplates as $emailTemplate) {
            $content = '';

            if ($emailTemplate['template_file'] != '') {
                $content = $this->fileParser->getHtmlContent('email/' . $emailTemplate['template_file']);
            }

            $emailData = [
                'template_text' => $content,
                'template_type' => TemplateTypesInterface::TYPE_HTML,
                'template_subject' => $emailTemplate['template_subject']
            ];

            if ($emailTemplate['template_code'] != '') {
                $this->updateTemplate($emailTemplate['template_code'], $emailData);
            }
        }
    }

    public function updateTemplate(string $templateCode, array $data = []): Template
    {
        $template = $this->templateFactory->create();
        $templateResource = $this->templateResourceFactory->create();

        $templateResource->load($template, $templateCode, 'template_code');

        if (!$template->getId()) {
            $data = array_merge(['template_code' => $templateCode], $data);
        }

        $template = $template->addData($data);
        $templateResource->save($template);

        return $template;
    }
}
