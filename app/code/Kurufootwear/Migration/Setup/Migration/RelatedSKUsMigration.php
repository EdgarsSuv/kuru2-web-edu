<?php
/**
 * @vendor      Kurufootwear
 * @module      Kurufootwear_Migration
 * @author      Kristaps Stalidzans <info@scandiweb.com>
 * @copyright   Copyright (c) 2017 Scandiweb, Ltd (http://scandiweb.com)
 * @license     http://opensource.org/licenses/afl-3.0.php Academic Free License (AFL 3.0)
 */
namespace Kurufootwear\Migration\Setup\Migration;

use Psr\Log\LoggerInterface;
use Magento\Catalog\Model\Product;
use Magento\Eav\Setup\EavSetupFactory;
use Magento\Catalog\Model\Product\Action;
use Magento\Framework\Setup\SetupInterface;
use Magento\Framework\Api\SearchCriteriaBuilder;
use Magento\Catalog\Api\ProductRepositoryInterface;
use Kurufootwear\Migration\Api\UpgradeMigrationInterface;
use Magento\Catalog\Model\ResourceModel\Product\CollectionFactory as ProductCollectionFactory;

class RelatedSKUsMigration implements UpgradeMigrationInterface
{
    const MEDIUM = 'medium';
    const WIDE = 'wide';

    /**
     * @var EavSetupFactory
     */
    protected $eavSetupFactory;

    /**
     * @var ProductCollectionFactory
     */
    private $productCollectionFactory;

    /**
     * @var SearchCriteriaBuilder
     */
    private $searchCriteriaBuilder;

    /**
     * @var ProductRepositoryInterface
     */
    private $productRepository;

    /**
     * @var Action
     */
    private $productAction;

    /**
     * @var LoggerInterface
     */
    private $logger;

    /**
     * FooterBlocksMigration constructor.
     * @param Action $productAction
     * @param ProductCollectionFactory $productCollectionFactory
     * @param SearchCriteriaBuilder $searchCriteriaBuilder
     * @param ProductRepositoryInterface $productRepository
     * @param LoggerInterface $logger
     * @param EavSetupFactory $eavSetupFactory
     */
    public function __construct(
        Action $productAction,
        ProductCollectionFactory $productCollectionFactory,
        SearchCriteriaBuilder $searchCriteriaBuilder,
        ProductRepositoryInterface $productRepository,
        LoggerInterface $logger,
        EavSetupFactory $eavSetupFactory
    ) {
        $this->productCollectionFactory = $productCollectionFactory;
        $this->searchCriteriaBuilder = $searchCriteriaBuilder;
        $this->productRepository = $productRepository;
        $this->productAction = $productAction;
        $this->logger = $logger;
        $this->eavSetupFactory = $eavSetupFactory;
    }

    /**
     * @inheritDoc
     */
    public function apply(SetupInterface $setup = null)
    {
        $this->addAttributes($setup);
    }

    /**
     * Adds new width attributes
     *
     * @param $setup
     */
    public function addAttributes($setup)
    {
        /** @var EavSetupFactory $eavSetupFactory */
        $eavSetup = $this->eavSetupFactory->create(['setup' => $setup]);
        $entityType = Product::ENTITY;
        // Create text attributes

        if (!$eavSetup->getAttribute($entityType, 'filterable_width')) {
            $eavSetup->addAttribute(
                $entityType,
                'filterable_width',
                [
                    'type' => 'varchar',
                    'label' => 'Width',
                    'input' => 'select',
                    'required' => false,
                    'user_defined' => true,
                    'searchable' => true,
                    'filterable' => true,
                    'comparable' => true,
                    'visible_in_advanced_search' => true,
                    'apply_to' => '',
                    'is_used_in_grid' => true,
                    'is_visible_in_grid' => true,
                    'is_filterable_in_grid' => true,
                    'used_in_product_listing' => true,
                    'option' =>
                        [
                            'values' =>
                                [
                                    self::MEDIUM => 'Medium',
                                    self::WIDE => 'Wide',
                                ]
                        ]
                ]
            );
        }

        $eavSetup->addAttributeToSet($entityType,
            $eavSetup->getAttributeSetId($entityType, 'Migration_Shoes'),
            'migration-shoe-settings',
            $eavSetup->getAttributeId($entityType, 'filterable_width')
        );

        // Create text attributes
        if (!$eavSetup->getAttribute($entityType, 'width_wide_sku')) {
            $eavSetup->addAttribute(
                $entityType,
                'width_wide_sku',
                [
                    'type' => 'varchar',
                    'label' => 'Width Wide SKU',
                    'input' => 'text',
                    'required' => false,
                    'user_defined' => false,
                    'searchable' => false,
                    'filterable' => false,
                    'comparable' => false,
                    'visible_on_front' => false,
                    'visible_in_advanced_search' => false,
                    'apply_to' => '',
                    'is_visible_in_grid' => false,
                    'is_filterable_in_grid' => false,
                    'used_in_product_listing' => false,
                ]
            );
        }

        $eavSetup->addAttributeToSet($entityType,
            $eavSetup->getAttributeSetId($entityType, 'Migration_Shoes'),
            'migration-shoe-settings',
            $eavSetup->getAttributeId($entityType, 'width_wide_sku')
        );

        // Create text attributes
        if (!$eavSetup->getAttribute($entityType, 'width_medium_sku')) {
            $eavSetup->addAttribute(
                $entityType,
                'width_medium_sku',
                [
                    'type' => 'varchar',
                    'label' => 'Width Medium SKU',
                    'input' => 'text',
                    'required' => false,
                    'user_defined' => false,
                    'searchable' => false,
                    'filterable' => false,
                    'comparable' => false,
                    'visible_on_front' => false,
                    'visible_in_advanced_search' => false,
                    'apply_to' => '',
                    'is_visible_in_grid' => false,
                    'is_filterable_in_grid' => false,
                    'used_in_product_listing' => false,
                ]
            );
        }

        $eavSetup->addAttributeToSet($entityType,
            $eavSetup->getAttributeSetId($entityType, 'Migration_Shoes'),
            'migration-shoe-settings',
            $eavSetup->getAttributeId($entityType, 'width_medium_sku')
        );
    }
}
