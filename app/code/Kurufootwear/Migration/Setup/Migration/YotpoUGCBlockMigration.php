<?php
/**
 * @vendor      Kurufootwear
 * @module      Kurufootwear_Migration
 * @author      Krisjanis Veinbahs <info@scandiweb.com>
 * @copyright   Copyright (c) 2017 Scandiweb, Ltd (http://scandiweb.com)
 * @license     http://opensource.org/licenses/afl-3.0.php Academic Free License (AFL 3.0)
 */
namespace Kurufootwear\Migration\Setup\Migration;

use Kurufootwear\Migration\Api\UpgradeMigrationInterface;
use Magento\Framework\Setup\SetupInterface;
use Magento\Framework\Filesystem;
use Kurufootwear\Migration\Helper\FileParser;
use Magento\Cms\Model\BlockFactory;

class YotpoUGCBlockMigration implements UpgradeMigrationInterface
{
    /**
     * @var Filesystem
     */
    private $filesystem;

    /**
     * @var BlockFactory
     */
    private $blockFactory;

    /**
     * @var FileParser
     */
    private $fileParser;

    /**
     * FooterBlocksMigration constructor.
     * @param Filesystem $filesystem
     * @param BlockFactory $blockFactory
     * @param FileParser $fileParser
     */
    public function __construct(
        Filesystem $filesystem,
        BlockFactory $blockFactory,
        FileParser $fileParser
    ) {
        $this->filesystem = $filesystem;
        $this->blockFactory = $blockFactory;
        $this->fileParser = $fileParser;
    }

    /**
     * @inheritDoc
     */
    public function apply(SetupInterface $setup = null)
    {
        $cmsBlocks = [
            [
                'file_name' => 'yotpo_ugc.html',
                'title' => 'Yotpo UGC',
                'identifier' => 'yotpo_ugc'
            ]
        ];

        foreach ($cmsBlocks as $cmsBlock) {
            $content = '';

            if ($cmsBlock['file_name'] != '') {
                $content = $this->fileParser->getHtmlContent('blocks/' . $cmsBlock['file_name']);
            }

            $blockData = [
                'title' => $cmsBlock['title'],
                'identifier' => $cmsBlock['identifier'],
                'content' => $content,
                'stores' => [0]
            ];

            $block = $this->blockFactory->create()->load($blockData['identifier'], 'identifier');

            if ($block->getId()) {
                $block->setTitle($blockData['title'])
                    ->setContent($blockData['content'])
                    ->setStores($blockData['stores'])
                    ->save();
            } else {
                $block->addData($blockData)
                    ->save();
            }
        }
    }
}
