<?php
/**
 * @vendor      Kurufootwear
 * @module      Kurufootwear_Migration
 * @author      Jelena Sorohova <info@scandiweb.com>
 * @copyright   Copyright (c) 2017 Scandiweb, Ltd (http://scandiweb.com)
 * @license     http://opensource.org/licenses/afl-3.0.php Academic Free License (AFL 3.0)
 */
namespace Kurufootwear\Migration\Setup\Migration;

use Magento\Catalog\Model\Product;
use Magento\Eav\Setup\EavSetupFactory;
use Kurufootwear\Migration\Api\UpgradeMigrationInterface;
use Magento\Framework\Setup\SetupInterface;

class SizeChartMigration implements UpgradeMigrationInterface
{
    /**
     * @var EavSetupFactory
     */
    private $_eavSetupFactory;

    /**
     * SizeChartMigration constructor.
     * @param EavSetupFactory $eavSetupFactory
     */
    public function __construct
    (
        EavSetupFactory $eavSetupFactory
    )
    {
        $this->_eavSetupFactory = $eavSetupFactory;
    }

    /**
     * @inheritDoc
     */
    public function apply(SetupInterface $setup = null)
    {
        /** @var EavSetupFactory $eavSetupFactory */
        $eavSetup = $this->_eavSetupFactory->create(['setup' => $setup]);
        $entityType = Product::ENTITY;

        $eavSetup->addAttributeToSet($entityType,
            $eavSetup->getAttributeSetId($entityType, 'Migration_Shoes'),
            'design',
            $eavSetup->getAttributeId($entityType, 'size_guide_cms_block')
        );
    }
}
