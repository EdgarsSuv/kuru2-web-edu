<?php
/**
 * @vendor Kurufootwear
 * @module Kurufootwear_Migration
 * @author Aleksejs Baranovs <info@scandiweb.com>
 * @copyright Copyright (c) 2018 KURU Footwear. All rights reserved.
 */

namespace Kurufootwear\Migration\Setup\Migration;

use Kurufootwear\Migration\Api\UpgradeMigrationInterface;
use Magento\Customer\Model\ResourceModel\Group\Collection as GroupCollection;
use Magento\Framework\Setup\SetupInterface;
use Magento\Store\Api\Data\WebsiteInterface;
use Magento\Store\Api\WebsiteRepositoryInterface;
use Mirasvit\Rewards\Model\Config;
use Mirasvit\Rewards\Model\Earning\RuleFactory as EarningRuleFactory;
use Mirasvit\Rewards\Model\ResourceModel\Earning\Rule as EarningRuleResource;

/**
 * Class SubscriptionRewardsRuleMigration
 * @package Kurufootwear\Migration\Setup\Migration
 */
class SubscriptionRewardsRuleMigration implements UpgradeMigrationInterface
{
    /**
     * New rule name
     */
    const NEWSLETTER_SUBSCRIPTION_REWARDS_RULE_NAME = 'Rewards Rule For Subscribing';

    /**
     * Rule history message
     */
    const NEWSLETTER_SUBSCRIPTION_REWARDS_HISTORY_MESSAGE = 'Thank you for subscribing! You have been awarded 200 KURU CASH REWARDS.';

    /**
     * @var GroupCollection
     */
    private $groupCollection;

    /**
     * @var WebsiteRepositoryInterface
     */
    private $websiteRepository;

    /**
     * @var EarningRuleFactory
     */
    private $earningRuleFactory;

    /**
     * @var EarningRuleResource
     */
    private $earningRuleResource;


    /**
     * DispensePointsForSubscribedCustomers constructor.
     *
     * @param GroupCollection $groupCollection
     * @param WebsiteRepositoryInterface $websiteRepository
     * @param EarningRuleFactory $earningRuleFactory
     * @param EarningRuleResource $earningRuleResource
     */
    public function __construct(
        GroupCollection $groupCollection,
        WebsiteRepositoryInterface $websiteRepository,
        EarningRuleFactory $earningRuleFactory,
        EarningRuleResource $earningRuleResource
    ) {
        $this->groupCollection = $groupCollection;
        $this->websiteRepository = $websiteRepository;
        $this->earningRuleFactory = $earningRuleFactory;
        $this->earningRuleResource = $earningRuleResource;
    }

    /**
     * Apply
     *
     * @param SetupInterface|null $setup
     */
    public function apply(SetupInterface $setup = null)
    {
        $this->createNewsletterSubscriptionRule();
    }

    /**
     * Create newsletter subscription rewards rule
     */
    private function createNewsletterSubscriptionRule()
    {
        $customerGroups = $this->getGroupIds();
        $websites = $this->getWebsiteIds();

        $earningRuleModel = $this->earningRuleFactory->create();

        $this->earningRuleResource->load($earningRuleModel, self::NEWSLETTER_SUBSCRIPTION_REWARDS_RULE_NAME, 'name');

        // Do not proceed with creation of newsletter rule if such exists
        if ($earningRuleModel->getId()) {
            return;
        }

        $earningRuleModel->setName(self::NEWSLETTER_SUBSCRIPTION_REWARDS_RULE_NAME)
            ->setType(Config::TYPE_BEHAVIOR)
            ->setIsActive(true)
            ->setBehaviorTrigger(Config::BEHAVIOR_TRIGGER_NEWSLETTER_SIGNUP)
            ->setHistoryMessage(self::NEWSLETTER_SUBSCRIPTION_REWARDS_HISTORY_MESSAGE)
            ->setEmailMessage(self::NEWSLETTER_SUBSCRIPTION_REWARDS_HISTORY_MESSAGE)
            ->setEarnPoints(200)
            ->setCustomerGroupIds($customerGroups)
            ->setWebsiteIds($websites);

        $this->earningRuleResource->save($earningRuleModel);
    }

    /**
     * Get customer groups
     *
     * @return array
     */
    private function getGroupIds()
    {
        $customerGroups = $this->groupCollection->toOptionArray();

        foreach ($customerGroups as $group => $info) {
            $customerGroups[$group] = $info['value'];
        }

        return $customerGroups;
    }

    /**
     * Get website ids
     *
     * @return WebsiteInterface[]
     */
    private function getWebsiteIds()
    {
        $websites = $this->websiteRepository->getList();

        foreach ($websites as $index => $website) {
            $websites[$index] = $website->getId();
        }

        return $websites;
    }
}
