<?php
/**
 * @category  Kurufootwear
 * @package   Kurufootwear\Migration
 * @author    Jelena Sorohova <info@scandiweb.com>
 * @copyright Copyright (c) 2017 Scandiweb, Inc (http://scandiweb.com)
 * @license   http://opensource.org/licenses/OSL-3.0 The Open Software License 3.0 (OSL-3.0)
 */
namespace Kurufootwear\Migration\Setup\Migration;

use Scandiweb\Slider\Model\SlideFactory;
use Scandiweb\Slider\Model\SliderFactory;
use Magento\Framework\Setup\SetupInterface;
use Kurufootwear\Migration\Helper\FileParser;
use Kurufootwear\Migration\Helper\FileHelper;
use Kurufootwear\Migration\Api\UpgradeMigrationInterface;

class HpSliderMigration implements UpgradeMigrationInterface
{
    /**
     * @var SliderFactory
     */
    private $sliderFactory;

    /**
     * @var FileHelper
     */
    private $fileHelper;

    /**
     * @var FileParser
     */
    private $fileParser;

    /**
     * @var SlideFactory
     */
    private $slideFactory;

    /**
     * HpSliderMigration constructor.
     * @param SliderFactory $sliderFactory
     * @param SlideFactory $slideFactory
     * @param FileParser $fileParser
     * @param FileHelper $fileHelper
     */
    public function __construct(
        SliderFactory $sliderFactory,
        SlideFactory $slideFactory,
        FileParser $fileParser,
        FileHelper $fileHelper
    ) {
        $this->sliderFactory = $sliderFactory;
        $this->fileHelper = $fileHelper;
        $this->fileParser = $fileParser;
        $this->slideFactory = $slideFactory;
    }

    /**
     * @inheritDoc
     */
    public function apply(SetupInterface $setup = null)
    {
        $this->createSlider();
        $this->createSlides();
    }

    /**
     * Creates HP slider
     */
    protected function createSlider()
    {
        $sliderData = [
            'title' => 'Homepage Slider',
            'is_active' => 1,
            'show_menu' => 1,
            'show_navigation' => 0,
            'slide_speed' => 0,
            'position' => 0,
            'animation_speed' => 0,
            'slides_to_display' => 1,
            'slides_to_scroll' => 0,
            'lazy_load' => 1,
            'slides_to_display_tablet' => 1,
            'slides_to_scroll_tablet' => 1,
            'slides_to_display_mobile' => 1,
            'slides_to_scroll_mobile' => 1
        ];

        /* @var $model /Scandiweb/Slider/Model/Slider */
        $model = $this->sliderFactory->create()->load('Homepage Slider', 'title');

        if (!$model->getId()) {
            $model->addData($sliderData)
                ->save();
        }
    }

    /**
     * Adds media files and slides
     */
    protected function createSlides()
    {
        $sliderImages = [
            'slingsuomi-bannner.jpg',
            'slingsuomi-minibannner.jpg',
            'bellatrina-banner.jpg',
            'bellatrina-minibanner.jpg',
            'chicaneboot-banner.jpg',
            'chicaneboot-minibanner.jpg',
            'kinetic-banner.jpg',
            'kinetic-minibanner.jpg',
            'hugyourheels-banner.jpg',
            'hugyourheels-minibanner.jpg'
        ];

        $this->fileHelper->copyMediaFiles($sliderImages, 'scandiweb/slider/s/l');
        $this->fileHelper->copyMediaFiles(['image-vid.png'], 'wysiwyg');

        $slidesData = [
            [
                'slider_id' => 1,
                'is_active' => 1,
                'position' => 1,
                'title' => 'Sling Suomi',
                'image' => 'scandiweb/slider/s/l/slingsuomi-bannner.jpg',
                'image_mobile' => 'scandiweb/slider/s/l/slingsuomi-minibannner.jpg',
                'slide_text' => $this->fileParser->getHtmlContent('slides/homepage-slide1.html'),
                'stores' => [1],
                'store_id' => [1],
                'slide_text_position' => 2
            ],
            [
                'slider_id' => 1,
                'is_active' => 1,
                'position' => 2,
                'title' => 'Bella Trina',
                'image' => 'scandiweb/slider/s/l/bellatrina-banner.jpg',
                'image_mobile' => 'scandiweb/slider/s/l/bellatrina-minibanner.jpg',
                'slide_text' => $this->fileParser->getHtmlContent('slides/homepage-slide2.html'),
                'stores' => [1],
                'store_id' => [1],
                'slide_text_position' => 2
            ],
            [
                'slider_id' => 1,
                'is_active' => 1,
                'position' => 3,
                'title' => 'Chicane Boot',
                'image' => 'scandiweb/slider/s/l/chicaneboot-banner.jpg',
                'image_mobile' => 'scandiweb/slider/s/l/chicaneboot-minibanner.jpg',
                'slide_text' => $this->fileParser->getHtmlContent('slides/homepage-slide3.html'),
                'stores' => [1],
                'store_id' => [1],
                'slide_text_position' => 2
            ],
            [
                'slider_id' => 1,
                'is_active' => 1,
                'position' => 4,
                'title' => 'Kinetic',
                'image' => 'scandiweb/slider/s/l/kinetic-banner.jpg',
                'image_mobile' => 'scandiweb/slider/s/l/kinetic-minibanner.jpg',
                'slide_text' => $this->fileParser->getHtmlContent('slides/homepage-slide4.html'),
                'stores' => [1],
                'store_id' => [1],
                'slide_text_position' => 2
            ],
            [
                'slider_id' => 1,
                'is_active' => 1,
                'position' => 5,
                'title' => 'Hug your heels',
                'image' => 'scandiweb/slider/s/l/hugyourheels-banner.jpg',
                'image_mobile' => 'scandiweb/slider/s/l/hugyourheels-minibanner.jpg',
                'slide_text' => $this->fileParser->getHtmlContent('slides/homepage-slide5.html'),
                'stores' => [1],
                'store_id' => [1],
                'slide_text_position' => 0
            ]
        ];

        foreach ($slidesData as $slideData) {
            $slide = $this->slideFactory->create()->load($slideData['title'], 'title');

            if ($slide->getId()) {
                $slide->setSliderId($slideData['slider_id'])
                    ->setSlideText($slideData['slide_text'])
                    ->setIsActive($slideData['is_active'])
                    ->setPosition($slideData['position'])
                    ->setImage($slideData['image'])
                    ->setImageMobile($slideData['image_mobile'])
                    ->setSlideTextPosition($slideData['slide_text_position'])
                    ->setStores($slideData['stores'])
                    ->save();
            } else {
                $slide->addData($slideData)
                    ->save();
            }
        }
    }
}
