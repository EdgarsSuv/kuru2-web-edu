<?php
/**
 * @vendor      Kurufootwear
 * @module      Kurufootwear_Migration
 * @author      Krisjanis Veinbahs <info@scandiweb.com>
 * @copyright   Copyright (c) 2018 Scandiweb, Ltd (http://scandiweb.com)
 * @license     http://opensource.org/licenses/afl-3.0.php Academic Free License (AFL 3.0)
 */
namespace Kurufootwear\Migration\Setup\Migration;

use Kurufootwear\Migration\Api\UpgradeMigrationInterface;
use Magento\Eav\Model\Entity\Attribute\ScopedAttributeInterface;
use Magento\Framework\Setup\SetupInterface;
use Magento\Catalog\Api\Data\ProductAttributeInterface;
use Kurufootwear\Shopby\Model\OptionSettingFactory;
use Magento\Eav\Model\Entity\TypeFactory as EavTypeFactory;
use Magento\Eav\Model\Entity\Attribute\SetFactory as AttributeSetFactory;
use Magento\Eav\Model\AttributeSetManagement;
use Magento\Eav\Model\AttributeManagement;
use Magento\Eav\Setup\EavSetupFactory;
use Magento\Catalog\Model\Product\Attribute\Repository as AttributeRepository;
use Magento\Swatches\Model\Swatch;

class SocksAttributeSetMigration implements UpgradeMigrationInterface
{
    /**
     * Name for new sock attribute set
     */
    const SOCKS_ATTRIBUTE_SET_NAME = 'Socks';

    /**
     * Name for new sock size attribute
     */
    const SOCKS_SIZE_ATTRIBUTE_CODE = 'socks_size';

    /**
     * Group/field in Product BE to put the socks size attribute in
     */
    const SOCKS_SIZE_PRODUCT_GROUP = 'Product Details';

    /**
     * @var EavSetupFactory
     */
    protected $eavSetupFactory;

    /**
     * @var EavTypeFactory
     */
    protected $eavTypeFactory;

    /**
     * @var AttributeSetFactory
     */
    protected $attributeSetFactory;

    /**
     * @var AttributeSetManagement
     */
    protected $attributeSetManagement;

    /**
     * @var AttributeManagement
     */
    protected $attributeManagement;

    /**
     * @var AttributeRepository
     */
    protected $attributeRepository;

    /**
     * SocksAttributeSetMigration constructor.
     * @param EavSetupFactory $eavSetupFactory
     * @param EavTypeFactory $eavTypeFactory
     * @param AttributeSetFactory $attributeSetFactory
     * @param AttributeSetManagement $attributeSetManagement
     * @param AttributeManagement $attributeManagement
     * @param AttributeRepository $attributeRepository
     */
    public function __construct(
        EavSetupFactory $eavSetupFactory,
        EavTypeFactory $eavTypeFactory,
        AttributeSetFactory $attributeSetFactory,
        AttributeSetManagement $attributeSetManagement,
        AttributeManagement $attributeManagement,
        AttributeRepository $attributeRepository
    ) {
        $this->eavSetupFactory = $eavSetupFactory;
        $this->eavTypeFactory = $eavTypeFactory;
        $this->attributeSetFactory = $attributeSetFactory;
        $this->attributeSetManagement = $attributeSetManagement;
        $this->attributeManagement = $attributeManagement;
        $this->attributeRepository = $attributeRepository;
    }

    /**
     * Create socks attribute set, which extends
     * shoes attribute set with a new socks_size attribute
     *
     * @param SetupInterface|null $setup
     */
    public function apply(SetupInterface $setup = null)
    {
        $productEntityTypeCode = ProductAttributeInterface::ENTITY_TYPE_CODE;
        $productEntityType = $this->eavTypeFactory->create()->loadByCode($productEntityTypeCode);
        $shoesAttributeSetId = $this->attributeSetFactory->create()
            ->load('Shoes', 'attribute_set_name')->getId();

        /**
         * Create/load socks attribute set model
         */
        $socksAttributeSet = $this->attributeSetFactory->create()
            ->load(self::SOCKS_ATTRIBUTE_SET_NAME, 'attribute_set_name');

        if (is_null($socksAttributeSet->getId())) {
            /**
             * If attribute set is newly created
             * set proper name and sort order and
             * save it.
             */
            $socksAttributeSet->setAttributeSetName(self::SOCKS_ATTRIBUTE_SET_NAME)
                ->setEntityTypeId($productEntityType->getId())
                ->setSortOrder(200);
            $this->attributeSetManagement->create($productEntityTypeCode, $socksAttributeSet, $shoesAttributeSetId);
        }

        /**
         * Create/Update and Assign additional socks_size
         * attribute to the new socks attribute set
         */
        $sockSizeAttribute = $this->createSocksSizeAttribute($setup);
        $this->attributeManagement->assign(
            $productEntityTypeCode,
            $socksAttributeSet->getId(),
            $socksAttributeSet->getDefaultGroupId(),
            $sockSizeAttribute->getAttributeCode(),
            $socksAttributeSet->getCollection()->count() * 10
        );
    }

    /**
     * Create a socks size attribute
     * Re-runnable -
     *   Doesn't duplicate attribute,
     *   Updates attribute data,
     *   (Re)Appends options
     * @param SetupInterface $setup
     * @return mixed
     */
    public function createSocksSizeAttribute(SetupInterface $setup)
    {
        $attributeCode = self::SOCKS_SIZE_ATTRIBUTE_CODE;
        $attributeData = [
            'type' => 'int',
            'label' => 'Socks size',
            'input' => 'select',

            'source' => 'Magento\Eav\Model\Entity\Attribute\Source\Table',
            'global' => ScopedAttributeInterface::SCOPE_GLOBAL,
            'group' => self::SOCKS_SIZE_PRODUCT_GROUP,
            'used_in_product_listing' => true,
            'visible_on_front' => true,
            'user_defined' => true,
            'filterable' => 0, // Don't use in layered navigation
            'filterable_in_search' => true,
            'used_for_promo_rules' => true,
            'is_html_allowed_on_front' => true,
            'used_for_sort_by' => true,
            'comparable' => true,
            'position' => 5,

            'visible' => true, // Visible in BE product attribute listing
            'required' => false,
            'nullable' => true,
            'length' => 255,
        ];

        $attributeOptions = [
            Swatch::SWATCH_INPUT_TYPE_KEY => Swatch::SWATCH_INPUT_TYPE_TEXT,
            'optiontext' => [
                'value' => [
                    'option_0' => ['XS'],
                    'option_1' => ['S'],
                    'option_2' => ['M'],
                    'option_3' => ['L'],
                    'option_4' => ['XL']
                ],
                'order' => [
                    'option_0' => 0,
                    'option_1' => 1,
                    'option_2' => 2,
                    'option_3' => 3,
                    'option_4' => 4
                ]
            ]
        ];

        /**
         * Create attribute.
         * Re-runnable - will create attribute once
         */
        $this->eavSetupFactory->create(['setup' => $setup])
            ->addAttribute(ProductAttributeInterface::ENTITY_TYPE_CODE, $attributeCode, $attributeData);

        /**
         * Fetch newly created attribute,
         * Update the latest attribute data,
         * (Re)Append the attribute options.
         */
        $attribute = $this->attributeRepository->get($attributeCode)
            ->addData($attributeOptions)
            ->save();

        /**
         * Set frontend label 'Size' for socks_size
         */
        $attribute->setStoreLabels([1 => 'Size'])
            ->save();

        return $attribute;
    }
}
