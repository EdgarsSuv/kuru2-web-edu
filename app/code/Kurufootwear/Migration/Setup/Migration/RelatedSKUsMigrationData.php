<?php
/**
 * @vendor      Kurufootwear
 * @module      Kurufootwear_Migration
 * @author      Kristaps Stalidzans <info@scandiweb.com>
 * @copyright   Copyright (c) 2017 Scandiweb, Ltd (http://scandiweb.com)
 * @license     http://opensource.org/licenses/afl-3.0.php Academic Free License (AFL 3.0)
 */
namespace Kurufootwear\Migration\Setup\Migration;

use Magento\Catalog\Model\CategoryFactory;
use Psr\Log\LoggerInterface;
use Magento\Eav\Setup\EavSetupFactory;
use Magento\Catalog\Model\Product\Visibility;
use Magento\Catalog\Model\Product\Action;
use Magento\Framework\Setup\SetupInterface;
use Kurufootwear\Shopby\Helper\AttributeHelper;
use Magento\Framework\Api\SearchCriteriaBuilder;
use Magento\Catalog\Api\ProductRepositoryInterface;
use Magento\Catalog\Api\CategoryRepositoryInterface;
use Kurufootwear\Migration\Api\UpgradeMigrationInterface;
use Magento\Catalog\Model\ResourceModel\Product\CollectionFactory as ProductCollectionFactory;

class RelatedSKUsMigrationData implements UpgradeMigrationInterface
{
    const MEDIUM = 'Medium';
    const WIDE = 'Wide';
    /**
     * normal => wide
     */
    const PRODUCT_SKUS = [
        '2030' => null,
        '2025' => null,
        '2024' => null,
        '2023' => null,
        '2022' => null,
        '2021' => null,
        '2020' => null,
        '2019' => '2019E',
        '2014' => null,
        '2017' => null,
        '2013' => '2013E',
        '2012' => null,
        '2011' => null,
        '2010' => null,
        '2008' => null,
        '2006D' => '2006DE',
        '2006' => '2006E',
        '2004' => null,
        '2003' => null,
        '2002D' => null,
        '2002' => null,
        '2001' => null,
        '1019' => '1019E',
        '1017' => null,
        '1016D' => null,
        '1016' => null,
        '1015' => null,
        '1014' => null,
        '1013LD' => null,
        '1013L' => '1013LE',
        '1013D' => null,
        '1013' => '1013E',
        '1012D' => null,
        '1012' => null,
        '1011D' => null,
        '1011' => null,
        '1009D' => null,
        '1009' => null,
        '1007D' => null,
        '1007' => null,
        '1006' => '1006E',
        '1006D' => null,
        '1002' => null,
        '1001' => null,
        '2013L' => '2013LE'
    ];
    const WOMENS_WIDE_SHOES_CATID = 237;
    const MENS_WIDE_SHOES_CATID = 238;
    /**
     * Yeah ids because site is migrated and its ids are also moved
     */
    const WOMENS_CAT_IDS = [
        219,
        37,
        39,
        57,
        237
    ];
    const MENS_CAT_IDS = [
        220,
        38,
        40,
        58,
        238
    ];


    /**
     * @var EavSetupFactory
     */
    protected $eavSetupFactory;

    /**
     * @var ProductCollectionFactory
     */
    private $productCollectionFactory;

    /**
     * @var SearchCriteriaBuilder
     */
    private $searchCriteriaBuilder;

    /**
     * @var ProductRepositoryInterface
     */
    private $productRepository;

    /**
     * @var Action
     */
    private $productAction;

    /**
     * @var LoggerInterface
     */
    private $logger;

    /**
     * @var AttributeHelper
     */
    private $attributeHelper;

    /**
     * @var CategoryFactory
     */
    private $categoryFactory;

    /**
     * @var CategoryRepositoryInterface
     */
    private $categoryRepository;

    /**
     * FooterBlocksMigration constructor.
     * @param Action $productAction
     * @param ProductCollectionFactory $productCollectionFactory
     * @param SearchCriteriaBuilder $searchCriteriaBuilder
     * @param ProductRepositoryInterface $productRepository
     * @param LoggerInterface $logger
     * @param EavSetupFactory $eavSetupFactory
     * @param AttributeHelper $attributeHelper
     * @param CategoryFactory $categoryFactory
     * @param CategoryRepositoryInterface $categoryRepository
     */
    public function __construct(
        Action $productAction,
        ProductCollectionFactory $productCollectionFactory,
        SearchCriteriaBuilder $searchCriteriaBuilder,
        ProductRepositoryInterface $productRepository,
        LoggerInterface $logger,
        EavSetupFactory $eavSetupFactory,
        AttributeHelper $attributeHelper,
        CategoryFactory $categoryFactory,
        CategoryRepositoryInterface $categoryRepository
    ) {
        $this->productCollectionFactory = $productCollectionFactory;
        $this->searchCriteriaBuilder = $searchCriteriaBuilder;
        $this->productRepository = $productRepository;
        $this->productAction = $productAction;
        $this->logger = $logger;
        $this->eavSetupFactory = $eavSetupFactory;
        $this->attributeHelper = $attributeHelper;
        $this->categoryFactory = $categoryFactory;
        $this->categoryRepository = $categoryRepository;
    }

    /**
     * @inheritDoc
     */
    public function apply(SetupInterface $setup = null)
    {
        $this->applyAttributeData(self::PRODUCT_SKUS);
        //$this->addWideCategories(self::WOMENS_WIDE_SHOES_CATID);
        //$this->addWideCategories(self::MENS_WIDE_SHOES_CATID);
        //$this->addInMenu(self::WOMENS_CAT_IDS);
        //$this->addInMenu(self::MENS_CAT_IDS);
    }

    /**
     * Apply attributes to product
     *
     * @param array $skuArray
     */
    public function applyAttributeData(array $skuArray)
    {
        foreach ($skuArray as $skuNormal => $skuWide) {
            $productWideId = $productNormalId = null;

            try {
                if ($skuWide) {
                    $product = $this->productRepository->get($skuWide);
                    $productWideId = $product->getId();
                    $optionId = $this->attributeHelper->getAttributeId(self::WIDE,'filterable_width');
                    $this->productAction->updateAttributes([$productWideId], ['filterable_width' => $optionId], 0);
                }

                if ($skuNormal) {
                    $product = $this->productRepository->get($skuNormal);
                    $productNormalId = $product->getId();
                    $optionId = $this->attributeHelper->getAttributeId(self::MEDIUM,'filterable_width');
                    $this->productAction->updateAttributes([$productNormalId], ['filterable_width' => $optionId], 0);
                }

                if ($productWideId & $productNormalId) {
                    $this->productAction->updateAttributes([$productNormalId], ['width_wide_sku' => $skuWide], 0);
                    $this->productAction->updateAttributes([$productWideId], ['width_medium_sku' => $skuNormal], 0);
                }
            } catch (\Exception $e) {
                echo 'Apply in attributes' . $e->getMessage()  . PHP_EOL;
                $this->logger->error($e->getMessage());
            }
        }
    }

    /**
     * Allows item to been seen on category
     *
     * @param array $data
     */
    public function addInMenu(array $data)
    {
        foreach ($data as $item) {
            try {
                $category = $this->categoryRepository->get($item, 0);
                $category->setIncludeInMenu(1);
                $this->categoryRepository->save($category);
            } catch (\Exception $e) {
                echo 'Add in menu ' . $e->getMessage() . PHP_EOL;
                $this->logger->error('Add wide categories set: ' . $e->getMessage());
            }
        }
    }

    /**
     * Sets wide product categories
     *
     * @param int $categoryId
     */
    public function addWideCategories(int $categoryId)
    {
        $category = $this->categoryFactory->create()->load($categoryId);
        $collection = $this->productCollectionFactory->create();
        $collection->addAttributeToSelect('*');
        $collection->addCategoryFilter($category);
        $collectionIds = $collection->getAllIds();

        if ($category) {
            foreach ($collectionIds as $collectionId) {
                try {
                    $product = $this->productRepository->getById($collectionId);
                    $product->setVisibility(Visibility::VISIBILITY_IN_CATALOG);
                    $product->setCategoryIds([$categoryId]);
                    $this->productRepository->save($product);
                } catch (\Exception $e) {
                    echo 'Add wide categories: ' . $product->getSku() . $e->getMessage() . PHP_EOL;
                    $this->logger->error('Add wide categories: ' . $e->getMessage());
                }
            }
        }
    }
}
