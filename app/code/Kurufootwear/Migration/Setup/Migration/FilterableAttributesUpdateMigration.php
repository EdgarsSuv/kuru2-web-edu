<?php
/**
 * @vendor      Kurufootwear
 * @module      Kurufootwear_Migration
 * @author      Jelena Sorohova <info@scandiweb.com>
 * @copyright   Copyright (c) 2017 Scandiweb, Ltd (http://scandiweb.com)
 * @license     http://opensource.org/licenses/afl-3.0.php Academic Free License (AFL 3.0)
 */
namespace Kurufootwear\Migration\Setup\Migration;

use Magento\Catalog\Model\Product;
use Magento\Eav\Setup\EavSetupFactory;
use Magento\Framework\Setup\SetupInterface;
use Kurufootwear\Migration\Api\UpgradeMigrationInterface;

class FilterableAttributesUpdateMigration implements UpgradeMigrationInterface
{
    /**
     * @var EavSetupFactory
     */
    protected $eavSetupFactory;

    /**
     * FilterableAttributesUpdateMigration constructor.
     * @param EavSetupFactory $eavSetupFactory
     */
    public function __construct(
        EavSetupFactory $eavSetupFactory
    ) {
        $this->eavSetupFactory = $eavSetupFactory;
    }

    /**
     * @inheritDoc
     */
    public function apply(SetupInterface $setup = null)
    {
        $this->updateAttributes($setup);
    }

    /**
     * Update filterable attributes positions
     *
     * @param $setup
     */
    public function updateAttributes($setup)
    {
        $attributes = [
            0 => 'category_ids',
            1 => 'benefits',
            2 => 'size',
            3 => 'womens_size',
            4 => 'filterable_width',
            5 => 'filterable_color'
        ];

        foreach ($attributes as $sortOrder => $attribute) {
            $eavSetup = $this->eavSetupFactory->create(['setup' => $setup]);

            if ($eavSetup->getAttribute(Product::ENTITY, $attribute)) {
                $eavSetup->updateAttribute(Product::ENTITY, $attribute,
                    [
                        'position' => $sortOrder,
                    ],
                    null, null);
            }
        }
    }
}
