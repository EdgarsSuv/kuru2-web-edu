<?php
/**
 * @vendor      Kurufootwear
 * @module      Kurufootwear_Migration
 * @author      Krisjanis Veinbahs <info@scandiweb.com>
 * @copyright   Copyright (c) 2017 Scandiweb, Ltd (http://scandiweb.com)
 * @license     http://opensource.org/licenses/afl-3.0.php Academic Free License (AFL 3.0)
 */
namespace Kurufootwear\Migration\Setup\Migration;

use Amasty\Shopby\Model\OptionSetting;
use Kurufootwear\Catalog\Block\Benefits;
use Kurufootwear\Catalog\Block\Benefits as BenefitsHelper;
use Kurufootwear\Migration\Api\UpgradeMigrationInterface;
use Kurufootwear\Migration\Helper\FileHelper;
use Magento\Catalog\Api\Data\ProductAttributeInterface;
use Magento\Eav\Model\Entity\Attribute\OptionManagement;
use Magento\Framework\Setup\SetupInterface;
use Kurufootwear\Shopby\Model\OptionSettingFactory;
use Amasty\Shopby\Model\ResourceModel\OptionSetting\CollectionFactory as OptionSettingCollectionFactory;

class BenefitsMigration implements UpgradeMigrationInterface
{
    /**
     * @var FileHelper
     */
    private $fileHelper;

    /**
     * @var BenefitsHelper
     */
    private $benefitsHelper;

    /**
     * @var OptionManagement
     */
    private $optionManagement;

    /**
     * @var OptionSettingCollectionFactory
     */
    private $optionSettingCollectionFactory;

    /**
     * @var OptionSettingFactory
     */
    private $optionSettingFactory;

    /**
     * Benefits data to be migrated into database,
     * which was previously hardcoded
     */
    const BENEFITS_DATA = [
        'Arch support' => [
            'image' => 'arch-support.svg',
            'description' => 'ULTIMATEINSOLES provide custom molded support for premium shock absorption and reliable arch support for an ultra comfortable ride'
        ],
        'Plantar fasciitis' => [
            'image' => 'plantar-fasciitis.svg',
            'description' => 'Proven orthotic support with the latest space-age materials, our KURUSOLE is crafted to hug your heels with every move, just as nature intended'
        ],
        'Nursing/healthcare' => [
            'image' => 'plantar-fasciitis.svg',
            'description' => 'Let KURU take care of your feet and keep you comfortable all day long, so you can focus on taking care of your patients'
        ],
        'Wide Width' => [
            'image' => 'wide-width.svg',
            'description' => 'The wide footwear feel and comfort you need, without the wide footwear restrictions you don\'t'
        ],
        'Heel pain' => [
            'image' => 'heel-pain.svg',
            'description' => 'The superior HEELKRADL literally hugs your heels as nature intended, allowing you to do more and GO longer'
        ],
        'Walking' => [
            'image' => 'walking.svg',
            'description' => 'Our KURUKLOUD foam uses advanced polymers, improved heel strike angles and a subtle rocker sole for a silky smooth stride'
        ],
        'Standing' => [
            'image' => 'standing.svg',
            'description' => 'Cool, comfortable and cushioned support to keep your feet happy and you standing tall'
        ],
        'Around the house' => [
            'image' => 'around-the-house.svg',
            'description' => 'Our KURUSOLE keeps your day-to-day movements completely comfortable and efficient'
        ],
        'Broad Toebox' => [
            'image' => 'broad-toebox.svg',
            'description' => 'More comfort, better mobility, and room to wiggle your toes'
        ],
        'Wide Toebox' => [
            'image' => 'wide-toebox.svg',
            'description' => 'More comfort, better mobility, and room to wiggle your toes'
        ],
        'Cold weather' => [
            'image' => 'cold-weather.svg',
            'description' => 'Keep your feet comfy and cozy when the mercury falls'
        ],
        'Hiking' => [
            'image' => 'hiking.svg',
            'description' => 'Get out and go, with total versatility, superior trail feel and amazing support'
        ],
        'Running' => [
            'image' => 'running.svg',
            'description' => 'The perfect blend of secure fit, solid heel hold and optimal toe box volume provides maximum running happiness'
        ],
        'Slip on' => [
            'image' => 'slip-on.svg',
            'description' => 'Ergonomically designed around the shape of your foot, giving your feet renewed comfort and support'
        ],
        'Slip resistant' => [
            'image' => 'slip-on.svg',
            'description' => 'Nature designed every foot with fat pads to cushion your every step. Stop your shoes from messing with nature. KURUSOLE™ is crafted to hug your heels with every move.'
        ],
        'Travel' => [
            'image' => 'travel.svg',
            'description' => 'Leave your footprint on the world and enjoy every adventure, with the proven comfort and support of our KURUSOLE'
        ],
        'Warm Weather' => [
            'image' => 'warm-weather.svg',
            'description' => 'Moisture wicking linings and breathable uppers allow just the right amount of air for awesome temperature regulation'
        ]
    ];

    /**
     * BenefitsMigration constructor.
     * @param FileHelper $fileHelper
     * @param BenefitsHelper $benefitsHelper
     * @param OptionManagement $optionManagement
     * @param OptionSettingCollectionFactory $optionSettingCollectionFactory
     * @param OptionSettingFactory $optionSettingFactory
     */
    public function __construct(
        FileHelper $fileHelper,
        BenefitsHelper $benefitsHelper,
        OptionManagement $optionManagement,
        OptionSettingCollectionFactory $optionSettingCollectionFactory,
        OptionSettingFactory $optionSettingFactory
    ) {
        $this->fileHelper = $fileHelper;
        $this->benefitsHelper = $benefitsHelper;
        $this->optionManagement = $optionManagement;
        $this->optionSettingCollectionFactory = $optionSettingCollectionFactory;
        $this->optionSettingFactory = $optionSettingFactory;
    }

    /**
     * De-hardcode benefits attributes
     *
     * @inheritDoc
     */
    public function apply(SetupInterface $setup = null)
    {
        $this->migrateBenefitsMedia();
        $this->migrateBenefitsData();
    }

    /**
     * Push hardcoded benefits data to Amasty SEO option settings
     */
    public function migrateBenefitsData()
    {
        $productEntityType = ProductAttributeInterface::ENTITY_TYPE_CODE;
        $benefitsOptions = $this->optionManagement->getItems($productEntityType, Benefits::BENEFITS_CODE);
        $filterCode = 'attr_' . Benefits::BENEFITS_CODE;
        $storeId = '0'; // Only migrating default (Adminhtml) values

        foreach ($benefitsOptions as $benefitsOption) {
            $benefitsOptionValue = $benefitsOption->getValue();
            $benefitsOptionLabel = $benefitsOption->getLabel();

            if (!$benefitsOptionValue) {
                continue;
            }

            if (!key_exists($benefitsOptionLabel, self::BENEFITS_DATA)) {
                continue;
            }

            $optionSettingCollection = $this->optionSettingCollectionFactory->create()
                ->addFieldToFilter('value', $benefitsOptionValue)
                ->addFieldToFilter('filter_code', $filterCode)
                ->addFieldToFilter('store_id', $storeId);

            if ($optionSettingCollection->count() > 0) {
                $optionSetting = $optionSettingCollection->getFirstItem();
            } else {
                $optionSetting = $this->optionSettingFactory->create();
            }

            $benefitsOptionData = self::BENEFITS_DATA[$benefitsOptionLabel];
            $optionSetting->setImage($benefitsOptionData['image'])
                ->setDescription($benefitsOptionData['description'])
                ->setFilterCode($filterCode)
                ->setValue($benefitsOptionValue)
                ->setStoreId($storeId)
                ->save();
        }
    }

    /**
     * Migrate benefits icons to proper media folder
     */
    public function migrateBenefitsMedia()
    {
        $benefitsIcons = array_map(function($benefitData) {
            return $benefitData['image'];
        }, self::BENEFITS_DATA);

        $this->fileHelper->copyMediaFiles($benefitsIcons, OptionSetting::IMAGES_DIR);
    }
}
