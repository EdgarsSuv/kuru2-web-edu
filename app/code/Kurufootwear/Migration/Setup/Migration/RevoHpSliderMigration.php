<?php
/**
 * @category  Kurufootwear
 * @package   Kurufootwear\Migration
 * @author    Kristaps Stalidzāns <kristapss@scandiweb.com>
 * @copyright Copyright (c) 2017 Scandiweb, Inc (http://scandiweb.com)
 * @license   http://opensource.org/licenses/OSL-3.0 The Open Software License 3.0 (OSL-3.0)
 */
namespace Kurufootwear\Migration\Setup\Migration;

use Kurufootwear\Migration\Api\UpgradeMigrationInterface;
use Magento\Framework\Setup\SetupInterface;
use Trive\Revo\Model\ProductSlider;

class RevoHpSliderMigration implements UpgradeMigrationInterface
{
    /**
     * @var ProductSlider
     */
    private $productSlider;

    /**
     * RevoHpSliderMigration constructor.
     * @param ProductSlider $productSlider
     */
    public function __construct(
        ProductSlider $productSlider
    ) {
        $this->productSlider = $productSlider;
    }

    /**
     * @inheritDoc
     */
    public function apply(SetupInterface $setup = null)
    {
        $sliderData = [
            'title' => 'Trending styles',
            'display_title' => 1,
            'status' => 1,
            'type' => 'featured',
            'grid' => 0,
            'exclude_from_cart' => 0,
            'exclude_from_checkout' => 0,
            'autoplay_speed' => 1000,
            'navigation' => 0,
            'infinite' => 1,
            'slides_to_show' => 4,
            'slides_to_scroll' => 4,
            'speed' => 500,
            'autoplay' => 0,
            'rtl' => 0,
            'breakpoint_large' => 1920,
            'large_slides_to_show' => 4,
            'large_slides_to_scroll' => 4,
            'breakpoint_medium' => 1024,
            'medium_slides_to_show' => 3,
            'medium_slides_to_scroll' => 3,
            'breakpoint_small' => 480,
            'small_slides_to_show' => 1,
            'small_slides_to_scroll' => 1,
            'display_price' => 0,
            'display_cart' => 0,
            'display_wishlist' => 0,
            'display_compare' => 0,
            'products_number' => 12,
            'enable_swatches' => 0,
            'enable_ajaxcart' => 0
        ];

        $model = $this->productSlider->load('Trending styles', 'title');

        if (!$model->getId()) {
            $model->addData($sliderData)
                ->save();
        }

    }
}
