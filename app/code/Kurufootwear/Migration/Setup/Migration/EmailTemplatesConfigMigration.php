<?php
/**
 * @vendor      Kurufootwear
 * @module      Kurufootwear_Migration
 * @author      Jelena Sorohova <info@scandiweb.com>
 * @copyright   Copyright (c) 2017 Scandiweb, Ltd (http://scandiweb.com)
 * @license     http://opensource.org/licenses/afl-3.0.php Academic Free License (AFL 3.0)
 */
namespace Kurufootwear\Migration\Setup\Migration;

use Kurufootwear\Migration\Api\UpgradeMigrationInterface;
use Magento\Config\Block\System\Config\Form;
use Magento\Config\Model\ResourceModel\Config;
use Magento\Framework\Setup\SetupInterface;

class EmailTemplatesConfigMigration implements UpgradeMigrationInterface
{
    /**
     * @var Config
     */
    private $_resourceConfig;


    /**
     * EmailTemplatesConfigMigration constructor.
     * @param Config $resourceConfig
     */
    public function __construct(
        Config $resourceConfig
    ) {
        $this->_resourceConfig = $resourceConfig;
    }

    /**
     * @inheritDoc
     */
    public function apply(SetupInterface $setup = null)
    {
        $emailConfigs = [
            [
                'path' => 'sales_email/order/template',
                'value' => 'sales_email_order_template'
            ],
            [
                'path' => 'sales_email/order/guest_template',
                'value' => 'sales_email_order_guest_template'
            ],
            [
                'path' => 'sales_email/invoice/template',
                'value' => 'sales_email_invoice_template'
            ],
            [
                'path' => 'sales_email/invoice/guest_template',
                'value' => 'sales_email_invoice_guest_template'
            ],
            [
                'path' => 'sales_email/invoice_comment/template',
                'value' => 'sales_email_invoice_comment_template'
            ],
            [
                'path' => 'sales_email/invoice_comment/guest_template',
                'value' => 'sales_email_invoice_comment_guest_template'
            ],
            [
                'path' => 'sales_email/shipment/template',
                'value' => 'sales_email_shipment_template'
            ],
            [
                'path' => 'sales_email/shipment/guest_template',
                'value' => 'sales_email_shipment_guest_template'
            ],
            [
                'path' => 'sales_email/creditmemo/template',
                'value' => 'sales_email_creditmemo_template'
            ],
            [
                'path' => 'sales_email/creditmemo/guest_template',
                'value' => 'sales_email_creditmemo_guest_template'
            ],
            [
                'path' => 'customer/create_account/email_template',
                'value' => 'customer_create_account_email_template'
            ],
            [
                'path' => 'customer/create_account/email_confirmed_template',
                'value' => 'customer_create_account_email_confirmation_template'
            ],
        ];

        foreach ($emailConfigs as $emailConfig) {
            $this->_resourceConfig->saveConfig(
                $emailConfig['path'],
                $emailConfig['value'],
                Form::SCOPE_DEFAULT,
                0
            );
        }
    }
}
