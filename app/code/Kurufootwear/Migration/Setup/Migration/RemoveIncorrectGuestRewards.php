<?php
/**
 * @category  Kurufootwear
 * @package   Kurufootwear\Migration
 * @author    Krisjanis Veinbahs <info@scandiweb.com>
 * @copyright Copyright (c) 2017 Scandiweb, Inc (http://scandiweb.com)
 * @license   http://opensource.org/licenses/OSL-3.0 The Open Software License 3.0 (OSL-3.0)
 */
namespace Kurufootwear\Migration\Setup\Migration;

use Kurufootwear\Migration\Api\UpgradeMigrationInterface;
use Magento\Customer\Api\CustomerRepositoryInterface;
use Magento\Framework\Setup\SetupInterface;
use Magento\Sales\Api\Data\OrderInterface;
use Mirasvit\Rewards\Model\ResourceModel\Transaction\CollectionFactory;

class RemoveIncorrectGuestRewards implements UpgradeMigrationInterface
{
    /**
     * @var CollectionFactory
     */
    protected $transactionCollectionFactory;

    /**
     * @var CustomerRepositoryInterface
     */
    protected $customerRepository;

    /**
     * @var OrderInterface
     */
    protected $order;

    /**
     * RemoveIncorrectGuestRewards constructor.
     * @param CollectionFactory $transactionCollectionFactory
     * @param CustomerRepositoryInterface $customerRepository
     * @param OrderInterface $order
     */
    public function __construct(
        CollectionFactory $transactionCollectionFactory,
        CustomerRepositoryInterface $customerRepository,
        OrderInterface $order
    )
    {
        $this->transactionCollectionFactory = $transactionCollectionFactory;
        $this->customerRepository = $customerRepository;
        $this->order = $order;
    }

    /**
     * This migration logic will remove "Earned ... for the order #..." transactions,
     * when the all of the following conditions are met:
     *      If mentioned order was created before customer creation
     *      If mentioned order was invoiced after customer creation
     *
     * @param SetupInterface|null $setup
     */
    public function apply(SetupInterface $setup = null)
    {
        $orderRewardTransactions = $this->transactionCollectionFactory->create()
            ->addFieldToFilter('comment', ['like' => 'Earned % points for the order #%']);

        foreach ($orderRewardTransactions as $orderRewardTransaction) {
            // Get customer creation date
            $customerId = $orderRewardTransaction->getCustomerId();
            $customer = $this->customerRepository->getById($customerId);
            $customerCreatedAt = $customer->getCreatedAt();

            // Get order creation date
            $comment = $orderRewardTransaction->getComment();
            $orderId = (int) substr($comment, strpos($comment, "#") + 1);
            $order = $this->order->loadByIncrementId($orderId);
            $orderCreatedAt = $order->getCreatedAt();

            // Get earliest order invoicing date
            $invoiceCollection = $order->getInvoiceCollection();
            $invoiceCollection->setOrder('created_at', 'ASC');
            $invoice = $invoiceCollection->getFirstItem();
            $invoiceCreatedAt = $invoice->getCreatedAt();

            // Format data properly for logic functionality
            if (!is_null($customerCreatedAt) &&
                !is_null($orderCreatedAt) &&
                !is_null($invoiceCreatedAt)) {
                $customerCreatedAt = strtotime($customerCreatedAt);
                $orderCreatedAt = strtotime($orderCreatedAt);
                $invoiceCreatedAt = strtotime($invoiceCreatedAt);
            } else {
                continue;
            }

            if ($orderCreatedAt < $customerCreatedAt &&
                $invoiceCreatedAt > $customerCreatedAt) {
                // Remove transaction
                $orderRewardTransaction->delete();
            }
        }
    }
}