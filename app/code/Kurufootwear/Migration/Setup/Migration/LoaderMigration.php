<?php
/**
 * @category  Kurufootwear
 * @package   Kurufootwear\Migration
 * @author    Kristaps Stalidzāns <kristapss@scandiweb.com>
 * @copyright Copyright (c) 2017 Scandiweb, Inc (http://scandiweb.com)
 * @license   http://opensource.org/licenses/OSL-3.0 The Open Software License 3.0 (OSL-3.0)
 */
namespace Kurufootwear\Migration\Setup\Migration;

use Magento\Framework\Setup\SetupInterface;
use Kurufootwear\Migration\Helper\FileHelper;
use Kurufootwear\Migration\Api\UpgradeMigrationInterface;

class LoaderMigration implements UpgradeMigrationInterface
{

    /**
     * @var FileHelper
     */
    private $fileHelper;

    /**
     * SliderMigration constructor.
     * @param FileHelper $fileHelper
     */
    public function __construct(
        FileHelper $fileHelper
    ) {
        $this->fileHelper = $fileHelper;
    }

    /**
     * @inheritDoc
     */
    public function apply(SetupInterface $setup = null)
    {
        $this->fileHelper->copyMediaFiles(['loader.svg'], 'wysiwyg');
    }
}
