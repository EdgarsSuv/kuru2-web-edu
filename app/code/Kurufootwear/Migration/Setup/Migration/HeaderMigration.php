<?php
/**
 * @vendor      Kurufootwear
 * @module      Kurufootwear_Migration
 * @author      Jelena Sorohova <info@scandiweb.com>
 * @copyright   Copyright (c) 2017 Scandiweb, Ltd (http://scandiweb.com)
 * @license     http://opensource.org/licenses/afl-3.0.php Academic Free License (AFL 3.0)
 */

namespace Kurufootwear\Migration\Setup\Migration;

use Kurufootwear\Migration\Api\UpgradeMigrationInterface;
use Magento\Framework\Setup\SetupInterface;
use Kurufootwear\Migration\Helper\FileParser;
use Magento\Cms\Model\PageFactory;
use Magento\Framework\ObjectManagerInterface as ObjectManager;
use \Magento\Catalog\Model\CategoryFactory;

class HeaderMigration implements UpgradeMigrationInterface
{
    /**
     * @var PageFactory
     */
    private $pageFactory;

    /**
     * @var ObjectManager
     */
    protected $_objectManager;

    /**
     * @var CategoryFactory
     */
    protected $_categoryFactory;

    /**
     * @var FileParser
     */
    private $fileParser;

    /**
     * HeaderMigration constructor.
     * @param PageFactory $pageFactory
     * @param ObjectManager $objectManager
     * @param CategoryFactory $categoryFactory
     * @param FileParser $fileParser
     */
    public function __construct(
        PageFactory $pageFactory,
        ObjectManager $objectManager,
        CategoryFactory $categoryFactory,
        FileParser $fileParser
    )
    {
        $this->pageFactory = $pageFactory;
        $this->_objectManager = $objectManager;
        $this->_categoryFactory = $categoryFactory;
        $this->fileParser = $fileParser;
    }

    /**
     * @inheritDoc
     */
    public function apply(SetupInterface $setup = null)
    {
        $this->_createHeaderMenu();
        $this->_fillHeaderMenu();
        $this->_fillHeaderSubMenu();
        $this->_fillHeaderSubSubMenu();
        $this->_deleteItems();
    }

    /**
     * Creates Scandiweb header menu
     */
    protected function _createHeaderMenu()
    {
        $this->_objectManager->create('Scandiweb\Menumanager\Model\MenuFactory')
            ->create()
            ->load('header-menu', 'identifier')
            ->setIdentifier('header-menu')
            ->setTitle('Header menu')
            ->setStores(array(0))
            ->save();
    }

    /**
     * Fills Scandiweb header menu
     */
    protected function _fillHeaderMenu()
    {
        $womensCatId = $this->_categoryFactory->create()
            ->getCollection()
            ->addAttributeToFilter('name', 'Women\'s Shoes')
            ->getFirstItem()
            ->getId();

        $mensCatId = $this->_categoryFactory->create()
            ->getCollection()
            ->addAttributeToFilter('name', 'Men\'s Shoes')
            ->getFirstItem()
            ->getId();

        $hoverMenuContent = array(
            array(
                'identifier' => 'womens',
                'title' => 'Women\'s shoes',
                'url_type' => '2',
                'category_id' => $womensCatId,
                'position' => '0',
                'css_class' => 'hover-show top-item'
            ),
            array(
                'identifier' => 'mens',
                'title' => 'Men\'s shoes',
                'url_type' => '2',
                'category_id' => $mensCatId,
                'position' => '10',
                'css_class' => 'hover-show top-item'
            ),
            array(
                'identifier' => 'innovation',
                'title' => 'Innovation',
                'url_type' => '0',
                'url' => 'innovation',
                'position' => '20',
                'css_class' => 'top-item'
            ),
            array(
                'identifier' => 'whykuru',
                'title' => 'Why KURU',
                'url_type' => '0',
                'url' => 'why-kuru',
                'position' => '30',
                'css_class' => 'top-item'
            ),
        );

        $menu = $this->_objectManager->create('Scandiweb\Menumanager\Model\MenuFactory')
            ->create()
            ->load('header-menu', 'identifier');

        foreach ($hoverMenuContent as $menuContent) {
            $this->_objectManager->create('Scandiweb\Menumanager\Model\ItemFactory')
                ->create()
                ->load($menuContent['identifier'], 'identifier')
                ->addData($menuContent)
                ->setMenuId($menu->getId())
                ->save();
        }
    }

    /**
     * Fills Scandiweb header submenu
     */
    protected function _fillHeaderSubMenu()
    {
        $womensId = $this->_objectManager->create('Scandiweb\Menumanager\Model\ItemFactory')
            ->create()
            ->load('womens', 'identifier')
            ->getId();

        $mensId = $this->_objectManager->create('Scandiweb\Menumanager\Model\ItemFactory')
            ->create()
            ->load('mens', 'identifier')
            ->getId();

        $womensCatId = $this->_categoryFactory->create()
            ->getCollection()
            ->addAttributeToFilter('name', 'Women\'s Shoes')
            ->getFirstItem()
            ->getId();

        $mensCatId = $this->_categoryFactory->create()
            ->getCollection()
            ->addAttributeToFilter('name', 'Men\'s Shoes')
            ->getFirstItem()
            ->getId();

        $headerMenuContent = array(
            array(
                'identifier' => 'womens_by_cat',
                'title' => 'Shop by category',
                'parent_id' => $womensId,
                'url_type' => '3',
                'url' => '#',
                'position' => '10',
                'css_class' => 'submenu-label'
            ),
            array(
                'identifier' => 'womens_best',
                'title' => 'Best for',
                'parent_id' => $womensId,
                'url_type' => '3',
                'url' => '#',
                'position' => '20',
                'css_class' => 'submenu-label'
            ),
            array(
                'identifier' => 'womens_by_size',
                'title' => 'Shop by size',
                'parent_id' => $womensId,
                'url_type' => '3',
                'url' => '#',
                'position' => '30',
                'css_class' => 'submenu-label'
            ),
            array(
                'identifier' => 'womens_by_color',
                'title' => 'Shop by color',
                'parent_id' => $womensId,
                'url_type' => '3',
                'url' => '#',
                'position' => '40',
                'css_class' => 'submenu-label'
            ),
            array(
                'identifier' => 'womens_view_all',
                'title' => 'View All',
                'parent_id' => $womensId,
                'url_type' => '0',
                'url' => 'womens-shoes.html',
                'position' => '5',
                'css_class' => 'submenu-label mobile-only blue'
            ),
            array(
                'identifier' => 'mens_by_cat',
                'title' => 'Shop by category',
                'parent_id' => $mensId,
                'url_type' => '3',
                'url' => '#',
                'position' => '10',
                'css_class' => 'submenu-label'
            ),
            array(
                'identifier' => 'mens_best',
                'title' => 'Best for',
                'parent_id' => $mensId,
                'url_type' => '3',
                'url' => '#',
                'position' => '20',
                'css_class' => 'submenu-label'
            ),
            array(
                'identifier' => 'mens_by_size',
                'title' => 'Shop by size',
                'parent_id' => $mensId,
                'url_type' => '3',
                'url' => '#',
                'position' => '30',
                'css_class' => 'submenu-label'
            ),
            array(
                'identifier' => 'mens_by_color',
                'title' => 'Shop by color',
                'parent_id' => $mensId,
                'url_type' => '3',
                'url' => '#',
                'position' => '40',
                'css_class' => 'submenu-label'
            ),
            array(
                'identifier' => 'mens_view_all',
                'title' => 'View All',
                'parent_id' => $mensId,
                'url_type' => '0',
                'url' => 'mens-shoes.html',
                'position' => '5',
                'css_class' => 'submenu-label mobile-only blue'
            )
        );

        $menu = $this->_objectManager->create('Scandiweb\Menumanager\Model\MenuFactory')
            ->create()
            ->load('header-menu', 'identifier');

        foreach ($headerMenuContent as $menuContent) {
            $this->_objectManager->create('Scandiweb\Menumanager\Model\ItemFactory')
                ->create()
                ->load($menuContent['identifier'], 'identifier')
                ->addData($menuContent)
                ->setMenuId($menu->getId())
                ->save();
        }
    }

    /**
     * Fills Scandiweb header submenu's submenus
     */
    protected function _fillHeaderSubSubMenu()
    {
        $wCatId = $this->_objectManager->create('Scandiweb\Menumanager\Model\ItemFactory')
            ->create()
            ->load('womens_by_cat', 'identifier')
            ->getId();

        $wBestId = $this->_objectManager->create('Scandiweb\Menumanager\Model\ItemFactory')
            ->create()
            ->load('womens_best', 'identifier')
            ->getId();

        $wSizeId = $this->_objectManager->create('Scandiweb\Menumanager\Model\ItemFactory')
            ->create()
            ->load('womens_by_size', 'identifier')
            ->getId();

        $wColorId = $this->_objectManager->create('Scandiweb\Menumanager\Model\ItemFactory')
            ->create()
            ->load('womens_by_color', 'identifier')
            ->getId();

        $mCatId = $this->_objectManager->create('Scandiweb\Menumanager\Model\ItemFactory')
            ->create()
            ->load('mens_by_cat', 'identifier')
            ->getId();

        $mBestId = $this->_objectManager->create('Scandiweb\Menumanager\Model\ItemFactory')
            ->create()
            ->load('mens_best', 'identifier')
            ->getId();

        $mSizeId = $this->_objectManager->create('Scandiweb\Menumanager\Model\ItemFactory')
            ->create()
            ->load('mens_by_size', 'identifier')
            ->getId();

        $mColorId = $this->_objectManager->create('Scandiweb\Menumanager\Model\ItemFactory')
            ->create()
            ->load('mens_by_color', 'identifier')
            ->getId();

        $womensCatId = $this->_categoryFactory->create()
            ->getCollection()
            ->addAttributeToFilter('name', 'Women\'s Shoes')
            ->getFirstItem()
            ->getId();

        $mensCatId = $this->_categoryFactory->create()
            ->getCollection()
            ->addAttributeToFilter('name', 'Men\'s Shoes')
            ->getFirstItem()
            ->getId();

        $headerMenuContent = array(
            array(
                'identifier' => 'womens_new_arrivals',
                'title' => 'New Arrivals',
                'parent_id' => $wCatId,
                'url_type' => '0',
                'url' => 'womens-shoes/womens-new-arrivals.html',
                'position' => '0',
                'css_class' => 'submenu-link'
            ),
            array(
                'identifier' => 'womens_outdoor',
                'title' => 'Outdoor Shoes',
                'parent_id' => $wCatId,
                'url_type' => '0',
                'url' => 'womens-shoes/womens-outdoor-shoes.html',
                'position' => '10',
                'css_class' => 'submenu-link'
            ),
            array(
                'identifier' => 'womens_athletic',
                'title' => 'Athletic Shoes',
                'parent_id' => $wCatId,
                'url_type' => '0',
                'url' => 'womens-shoes/womens-athletic-shoes.html',
                'position' => '20',
                'css_class' => 'submenu-link'
            ),
            array(
                'identifier' => 'womens_casual',
                'title' => 'Casual Shoes',
                'parent_id' => $wCatId,
                'url_type' => '0',
                'url' => 'womens-shoes/womens-casual-shoes.html',
                'position' => '30',
                'css_class' => 'submenu-link'
            ),
            array(
                'identifier' => 'womens_wide',
                'title' => 'Wide Shoes',
                'parent_id' => $wCatId,
                'url_type' => '0',
                'url' => 'womens-shoes/womens-wide-shoes.html',
                'position' => '40',
                'css_class' => 'submenu-link'
            ),
            array(
                'identifier' => 'womens_all',
                'title' => 'View All',
                'parent_id' => $wCatId,
                'url_type' => '2',
                'category_id' => $womensCatId,
                'position' => '50',
                'css_class' => 'submenu-link blue'
            ),
            array(
                'identifier' => 'womens_plantar',
                'title' => 'Plantar Fasciitis',
                'parent_id' => $wBestId,
                'url_type' => '0',
                'url' => 'womens-shoes.html?benefits=Plantar+fasciitis',
                'position' => '0',
                'css_class' => 'submenu-link'
            ),
            array(
                'identifier' => 'womens_arch',
                'title' => 'Arch Support',
                'parent_id' => $wBestId,
                'url_type' => '0',
                'url' => 'womens-shoes.html?benefits=Arch+support',
                'position' => '10',
                'css_class' => 'submenu-link'
            ),
            array(
                'identifier' => 'womens_nursing',
                'title' => 'Nursing / Healthcare',
                'parent_id' => $wBestId,
                'url_type' => '0',
                'url' => 'womens-shoes.html?benefits=Nursing%2Fhealthcare',
                'position' => '20',
                'css_class' => 'submenu-link'
            ),
            array(
                'identifier' => 'womens_heel',
                'title' => 'Heel Pain',
                'parent_id' => $wBestId,
                'url_type' => '0',
                'url' => 'womens-shoes.html?benefits=Heel+pain',
                'position' => '40',
                'css_class' => 'submenu-link'
            ),
            array(
                'identifier' => 'womens_walking',
                'title' => 'Walking',
                'parent_id' => $wBestId,
                'url_type' => '0',
                'url' => 'womens-shoes.html?benefits=Walking',
                'position' => '50',
                'css_class' => 'submenu-link'
            ),
            array(
                'identifier' => 'womens_standing',
                'title' => 'Standing',
                'parent_id' => $wBestId,
                'url_type' => '0',
                'url' => 'womens-shoes.html?benefits=Standing',
                'position' => '60',
                'css_class' => 'submenu-link'
            ),
            array(
                'identifier' => 'womens_more',
                'title' => 'View All',
                'parent_id' => $wBestId,
                'url_type' => '2',
                'category_id' => $womensCatId,
                'position' => '70',
                'css_class' => 'submenu-link blue'
            ),
            array(
                'identifier' => 'womens_five',
                'title' => '5',
                'parent_id' => $wSizeId,
                'url_type' => '0',
                'url' => 'womens-shoes.html?womens_size=5',
                'position' => '0',
                'css_class' => 'submenu-link size'
            ),
            array(
                'identifier' => 'womens_five_and_half',
                'title' => '5.5',
                'parent_id' => $wSizeId,
                'url_type' => '0',
                'url' => 'womens-shoes.html?womens_size=5.5',
                'position' => '10',
                'css_class' => 'submenu-link size'
            ),
            array(
                'identifier' => 'womens_six',
                'title' => '6',
                'parent_id' => $wSizeId,
                'url_type' => '0',
                'url' => 'womens-shoes.html?womens_size=6',
                'position' => '20',
                'css_class' => 'submenu-link size'
            ),
            array(
                'identifier' => 'womens_six_and_half',
                'title' => '6.5',
                'parent_id' => $wSizeId,
                'url_type' => '0',
                'url' => 'womens-shoes.html?womens_size=6.5',
                'position' => '30',
                'css_class' => 'submenu-link size'
            ),
            array(
                'identifier' => 'womens_seven',
                'title' => '7',
                'parent_id' => $wSizeId,
                'url_type' => '0',
                'url' => 'womens-shoes.html?womens_size=7',
                'position' => '40',
                'css_class' => 'submenu-link size'
            ),
            array(
                'identifier' => 'womens_seven_and_half',
                'title' => '7.5',
                'parent_id' => $wSizeId,
                'url_type' => '0',
                'url' => 'womens-shoes.html?womens_size=7.5',
                'position' => '50',
                'css_class' => 'submenu-link size'
            ),
            array(
                'identifier' => 'womens_eight',
                'title' => '8',
                'parent_id' => $wSizeId,
                'url_type' => '0',
                'url' => 'womens-shoes.html?womens_size=8',
                'position' => '60',
                'css_class' => 'submenu-link size'
            ),
            array(
                'identifier' => 'womens_eight_and_half',
                'title' => '8.5',
                'parent_id' => $wSizeId,
                'url_type' => '0',
                'url' => 'womens-shoes.html?womens_size=8.5',
                'position' => '70',
                'css_class' => 'submenu-link size'
            ),
            array(
                'identifier' => 'womens_nine',
                'title' => '9',
                'parent_id' => $wSizeId,
                'url_type' => '0',
                'url' => 'womens-shoes.html?womens_size=9',
                'position' => '80',
                'css_class' => 'submenu-link size'
            ),
            array(
                'identifier' => 'womens_nine_and_half',
                'title' => '9.5',
                'parent_id' => $wSizeId,
                'url_type' => '0',
                'url' => 'womens-shoes.html?womens_size=9.5',
                'position' => '90',
                'css_class' => 'submenu-link size'
            ),
            array(
                'identifier' => 'womens_ten',
                'title' => '10',
                'parent_id' => $wSizeId,
                'url_type' => '0',
                'url' => 'womens-shoes.html?womens_size=10',
                'position' => '100',
                'css_class' => 'submenu-link size'
            ),
            array(
                'identifier' => 'womens_ten_and_half',
                'title' => '10.5',
                'parent_id' => $wSizeId,
                'url_type' => '0',
                'url' => 'womens-shoes.html?womens_size=10.5',
                'position' => '110',
                'css_class' => 'submenu-link size'
            ),
            array(
                'identifier' => 'womens_eleven',
                'title' => '11',
                'parent_id' => $wSizeId,
                'url_type' => '0',
                'url' => 'womens-shoes.html?womens_size=11',
                'position' => '120',
                'css_class' => 'submenu-link size'
            ),
            array(
                'identifier' => 'womens_eleven_and_half',
                'title' => '11.5',
                'parent_id' => $wSizeId,
                'url_type' => '0',
                'url' => 'womens-shoes.html?womens_size=11.5',
                'position' => '130',
                'css_class' => 'submenu-link size'
            ),
            array(
                'identifier' => 'womens_twelve',
                'title' => '12',
                'parent_id' => $wSizeId,
                'url_type' => '0',
                'url' => 'womens-shoes.html?womens_size=12',
                'position' => '140',
                'css_class' => 'submenu-link size'
            ),
            array(
                'identifier' => 'womens_black',
                'title' => 'Black',
                'parent_id' => $wColorId,
                'url_type' => '0',
                'url' => 'womens-shoes.html?filterable_color=Black',
                'position' => '0',
                'css_class' => 'submenu-link color color-black'
            ),
            array(
                'identifier' => 'womens_multi',
                'title' => 'Multi',
                'parent_id' => $wColorId,
                'url_type' => '0',
                'url' => 'womens-shoes.html?filterable_color=Multi',
                'position' => '10',
                'css_class' => 'submenu-link color color-multi'
            ),
            array(
                'identifier' => 'womens_orange',
                'title' => 'Orange',
                'parent_id' => $wColorId,
                'url_type' => '0',
                'url' => 'womens-shoes.html?filterable_color=Orange',
                'position' => '20',
                'css_class' => 'submenu-link color color-orange'
            ),
            array(
                'identifier' => 'womens_yellow',
                'title' => 'Yellow',
                'parent_id' => $wColorId,
                'url_type' => '0',
                'url' => 'womens-shoes.html?filterable_color=Yellow',
                'position' => '30',
                'css_class' => 'submenu-link color color-yellow'
            ),
            array(
                'identifier' => 'womens_grey',
                'title' => 'Grey',
                'parent_id' => $wColorId,
                'url_type' => '0',
                'url' => 'womens-shoes.html?filterable_color=Grey',
                'position' => '40',
                'css_class' => 'submenu-link color color-grey'
            ),
            array(
                'identifier' => 'womens_white',
                'title' => 'White',
                'parent_id' => $wColorId,
                'url_type' => '0',
                'url' => 'womens-shoes.html?filterable_color=White',
                'position' => '50',
                'css_class' => 'submenu-link color color-white'
            ),
            array(
                'identifier' => 'womens_blue',
                'title' => 'Blue',
                'parent_id' => $wColorId,
                'url_type' => '0',
                'url' => 'womens-shoes.html?filterable_color=Blue',
                'position' => '60',
                'css_class' => 'submenu-link color color-blue'
            ),
            array(
                'identifier' => 'womens_green',
                'title' => 'Green',
                'parent_id' => $wColorId,
                'url_type' => '0',
                'url' => 'womens-shoes.html?filterable_color=Green',
                'position' => '70',
                'css_class' => 'submenu-link color color-green'
            ),
            array(
                'identifier' => 'womens_brown',
                'title' => 'Brown',
                'parent_id' => $wColorId,
                'url_type' => '0',
                'url' => 'womens-shoes.html?filterable_color=Brown',
                'position' => '80',
                'css_class' => 'submenu-link color color-brown'
            ),
            array(
                'identifier' => 'womens_red',
                'title' => 'Red',
                'parent_id' => $wColorId,
                'url_type' => '0',
                'url' => 'womens-shoes.html?filterable_color=Red',
                'position' => '90',
                'css_class' => 'submenu-link color color-red'
            ),
            array(
                'identifier' => 'womens_pink',
                'title' => 'Pink',
                'parent_id' => $wColorId,
                'url_type' => '0',
                'url' => 'womens-shoes.html?filterable_color=Pink',
                'position' => '100',
                'css_class' => 'submenu-link color color-pink'
            ),
            array(
                'identifier' => 'womens_purple',
                'title' => 'Purple',
                'parent_id' => $wColorId,
                'url_type' => '0',
                'url' => 'womens-shoes.html?filterable_color=Purple',
                'position' => '110',
                'css_class' => 'submenu-link color color-purple'
            ),

            array(
                'identifier' => 'mens_new_arrivals',
                'title' => 'New Arrivals',
                'parent_id' => $mCatId,
                'url_type' => '0',
                'url' => 'mens-shoes/mens-new-arrivals.html',
                'position' => '0',
                'css_class' => 'submenu-link'
            ),
            array(
                'identifier' => 'mens_outdoor',
                'title' => 'Outdoor Shoes',
                'parent_id' => $mCatId,
                'url_type' => '0',
                'url' => 'mens-shoes/mens-outdoor-shoes.html',
                'position' => '10',
                'css_class' => 'submenu-link'
            ),
            array(
                'identifier' => 'mens_athletic',
                'title' => 'Athletic Shoes',
                'parent_id' => $mCatId,
                'url_type' => '0',
                'url' => 'mens-shoes/mens-athletic-shoes.html',
                'position' => '20',
                'css_class' => 'submenu-link'
            ),
            array(
                'identifier' => 'mens_casual',
                'title' => 'Casual Shoes',
                'parent_id' => $mCatId,
                'url_type' => '0',
                'url' => 'mens-shoes/mens-casual-shoes.html',
                'position' => '30',
                'css_class' => 'submenu-link'
            ),
            array(
                'identifier' => 'mens_wide',
                'title' => 'Wide Shoes',
                'parent_id' => $mCatId,
                'url_type' => '0',
                'url' => 'mens-shoes/mens-wide-shoes.html',
                'position' => '40',
                'css_class' => 'submenu-link'
            ),
            array(
                'identifier' => 'mens_all',
                'title' => 'View All',
                'parent_id' => $mCatId,
                'url_type' => '2',
                'category_id' => $mensCatId,
                'position' => '50',
                'css_class' => 'submenu-link blue'
            ),
            array(
                'identifier' => 'mens_plantar',
                'title' => 'Plantar Fasciitis',
                'parent_id' => $mBestId,
                'url_type' => '0',
                'url' => 'mens-shoes.html?benefits=Plantar+fasciitis',
                'position' => '0',
                'css_class' => 'submenu-link'
            ),
            array(
                'identifier' => 'mens_arch',
                'title' => 'Arch Support',
                'parent_id' => $mBestId,
                'url_type' => '0',
                'url' => 'mens-shoes.html?benefits=Arch+support',
                'position' => '10',
                'css_class' => 'submenu-link'
            ),
            array(
                'identifier' => 'mens_nursing',
                'title' => 'Nursing / Healthcare',
                'parent_id' => $mBestId,
                'url_type' => '0',
                'url' => 'mens-shoes.html?benefits=Nursing%2Fhealthcare',
                'position' => '20',
                'css_class' => 'submenu-link'
            ),
            array(
                'identifier' => 'mens_heel',
                'title' => 'Heel Pain',
                'parent_id' => $mBestId,
                'url_type' => '0',
                'url' => 'mens-shoes.html?benefits=Heel+pain',
                'position' => '40',
                'css_class' => 'submenu-link'
            ),
            array(
                'identifier' => 'mens_walking',
                'title' => 'Walking',
                'parent_id' => $mBestId,
                'url_type' => '0',
                'url' => 'mens-shoes.html?benefits=Walking',
                'position' => '50',
                'css_class' => 'submenu-link'
            ),
            array(
                'identifier' => 'mens_standing',
                'title' => 'Standing',
                'parent_id' => $mBestId,
                'url_type' => '0',
                'url' => 'mens-shoes.html?benefits=Standing',
                'position' => '60',
                'css_class' => 'submenu-link'
            ),
            array(
                'identifier' => 'mens_more',
                'title' => 'View All',
                'parent_id' => $mBestId,
                'url_type' => '2',
                'category_id' => $mensCatId,
                'position' => '70',
                'css_class' => 'submenu-link blue'
            ),
            array(
                'identifier' => 'mens_six_and_half',
                'title' => '6.5',
                'parent_id' => $mSizeId,
                'url_type' => '0',
                'url' => 'mens-shoes.html?size=6.5',
                'position' => '0',
                'css_class' => 'submenu-link size'
            ),
            array(
                'identifier' => 'mens_seven',
                'title' => '7',
                'parent_id' => $mSizeId,
                'url_type' => '0',
                'url' => 'mens-shoes.html?size=7',
                'position' => '10',
                'css_class' => 'submenu-link size'
            ),
            array(
                'identifier' => 'mens_seven_and_half',
                'title' => '7.5',
                'parent_id' => $mSizeId,
                'url_type' => '0',
                'url' => 'mens-shoes.html?size=7.5',
                'position' => '20',
                'css_class' => 'submenu-link size'
            ),
            array(
                'identifier' => 'mens_eight',
                'title' => '8',
                'parent_id' => $mSizeId,
                'url_type' => '0',
                'url' => 'mens-shoes.html?size=8',
                'position' => '30',
                'css_class' => 'submenu-link size'
            ),
            array(
                'identifier' => 'mens_eight_and_half',
                'title' => '8.5',
                'parent_id' => $mSizeId,
                'url_type' => '0',
                'url' => 'mens-shoes.html?size=8.5',
                'position' => '40',
                'css_class' => 'submenu-link size'
            ),
            array(
                'identifier' => 'mens_nine',
                'title' => '9',
                'parent_id' => $mSizeId,
                'url_type' => '0',
                'url' => 'mens-shoes.html?size=9',
                'position' => '50',
                'css_class' => 'submenu-link size'
            ),
            array(
                'identifier' => 'mens_nine_and_half',
                'title' => '9.5',
                'parent_id' => $mSizeId,
                'url_type' => '0',
                'url' => 'mens-shoes.html?size=9.5',
                'position' => '60',
                'css_class' => 'submenu-link size'
            ),
            array(
                'identifier' => 'mens_ten',
                'title' => '10',
                'parent_id' => $mSizeId,
                'url_type' => '0',
                'url' => 'mens-shoes.html?size=10',
                'position' => '70',
                'css_class' => 'submenu-link size'
            ),
            array(
                'identifier' => 'mens_ten_and_half',
                'title' => '10.5',
                'parent_id' => $mSizeId,
                'url_type' => '0',
                'url' => 'mens-shoes.html?size=10.5',
                'position' => '80',
                'css_class' => 'submenu-link size'
            ),
            array(
                'identifier' => 'mens_eleven',
                'title' => '11',
                'parent_id' => $mSizeId,
                'url_type' => '0',
                'url' => 'mens-shoes.html?size=11',
                'position' => '90',
                'css_class' => 'submenu-link size'
            ),
            array(
                'identifier' => 'mens_eleven_and_half',
                'title' => '11.5',
                'parent_id' => $mSizeId,
                'url_type' => '0',
                'url' => 'mens-shoes.html?size=11.5',
                'position' => '100',
                'css_class' => 'submenu-link size'
            ),
            array(
                'identifier' => 'mens_twelve',
                'title' => '12',
                'parent_id' => $mSizeId,
                'url_type' => '0',
                'url' => 'mens-shoes.html?size=12',
                'position' => '110',
                'css_class' => 'submenu-link size'
            ),
            array(
                'identifier' => 'mens_twelve_and_half',
                'title' => '12.5',
                'parent_id' => $mSizeId,
                'url_type' => '0',
                'url' => 'mens-shoes.html?size=12.5',
                'position' => '120',
                'css_class' => 'submenu-link size'
            ),
            array(
                'identifier' => 'mens_thirteen',
                'title' => '13',
                'parent_id' => $mSizeId,
                'url_type' => '0',
                'url' => 'mens-shoes.html?size=13',
                'position' => '130',
                'css_class' => 'submenu-link size'
            ),
            array(
                'identifier' => 'mens_thirteen_and_half',
                'title' => '13.5',
                'parent_id' => $mSizeId,
                'url_type' => '0',
                'url' => 'mens-shoes.html?size=13.5',
                'position' => '140',
                'css_class' => 'submenu-link size'
            ),
            array(
                'identifier' => 'mens_fourteen',
                'title' => '14',
                'parent_id' => $mSizeId,
                'url_type' => '0',
                'url' => 'mens-shoes.html?size=14',
                'position' => '150',
                'css_class' => 'submenu-link size'
            ),
            array(
                'identifier' => 'mens_black',
                'title' => 'Black',
                'parent_id' => $mColorId,
                'url_type' => '0',
                'url' => 'mens-shoes.html?filterable_color=Black',
                'position' => '0',
                'css_class' => 'submenu-link color color-black'
            ),
            array(
                'identifier' => 'mens_white',
                'title' => 'White',
                'parent_id' => $mColorId,
                'url_type' => '0',
                'url' => 'mens-shoes.html?filterable_color=White',
                'position' => '10',
                'css_class' => 'submenu-link color color-white'
            ),
            array(
                'identifier' => 'mens_orange',
                'title' => 'Orange',
                'parent_id' => $mColorId,
                'url_type' => '0',
                'url' => 'mens-shoes.html?filterable_color=Orange',
                'position' => '20',
                'css_class' => 'submenu-link color color-orange'
            ),
            array(
                'identifier' => 'mens_yellow',
                'title' => 'Yellow',
                'parent_id' => $mColorId,
                'url_type' => '0',
                'url' => 'mens-shoes.html?filterable_color=Yellow',
                'position' => '30',
                'css_class' => 'submenu-link color color-yellow'
            ),
            array(
                'identifier' => 'mens_grey',
                'title' => 'Grey',
                'parent_id' => $mColorId,
                'url_type' => '0',
                'url' => 'mens-shoes.html?filterable_color=Grey',
                'position' => '40',
                'css_class' => 'submenu-link color color-grey'
            ),
            array(
                'identifier' => 'mens_blue',
                'title' => 'Blue',
                'parent_id' => $mColorId,
                'url_type' => '0',
                'url' => 'mens-shoes.html?filterable_color=Blue',
                'position' => '50',
                'css_class' => 'submenu-link color color-blue'
            ),
            array(
                'identifier' => 'mens_brown',
                'title' => 'Brown',
                'parent_id' => $mColorId,
                'url_type' => '0',
                'url' => 'mens-shoes.html?filterable_color=Brown',
                'position' => '60',
                'css_class' => 'submenu-link color color-brown'
            ),
            array(
                'identifier' => 'mens_red',
                'title' => 'Red',
                'parent_id' => $mColorId,
                'url_type' => '0',
                'url' => 'mens-shoes.html?filterable_color=Red',
                'position' => '70',
                'css_class' => 'submenu-link color color-red'
            )
        );

        $menu = $this->_objectManager->create('Scandiweb\Menumanager\Model\MenuFactory')
            ->create()
            ->load('header-menu', 'identifier');

        foreach ($headerMenuContent as $menuContent) {
            $this->_objectManager->create('Scandiweb\Menumanager\Model\ItemFactory')
                ->create()
                ->load($menuContent['identifier'], 'identifier')
                ->addData($menuContent)
                ->setMenuId($menu->getId())
                ->save();
        }
    }

    protected function _deleteItems()
    {
        $this->_objectManager->create('Scandiweb\Menumanager\Model\ItemFactory')
            ->create()
            ->load('womwens_by_cat', 'identifier')
            ->delete();

        $this->_objectManager->create('Scandiweb\Menumanager\Model\ItemFactory')
            ->create()
            ->load('technology', 'identifier')
            ->delete();

        $this->_objectManager->create('Scandiweb\Menumanager\Model\ItemFactory')
            ->create()
            ->load('kurulove', 'identifier')
            ->delete();
    }
}