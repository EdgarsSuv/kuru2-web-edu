<?php
/**
 * @category  Kurufootwear
 * @package   Kurufootwear\Migration
 * @author    Krisjanis Veinbahs <info@scandiweb.com>
 * @copyright Copyright (c) 2017 Scandiweb, Inc (http://scandiweb.com)
 * @license   http://opensource.org/licenses/OSL-3.0 The Open Software License 3.0 (OSL-3.0)
 */
namespace Kurufootwear\Migration\Setup\Migration;

use Kurufootwear\Migration\Api\UpgradeMigrationInterface;
use Kurufootwear\Rma\Model\Source\Request\Status;
use Magento\Framework\Setup\SetupInterface;
use Magento\Sales\Model\Order;
use Magento\Sales\Model\Order\StatusFactory;

class RmaOrderStatusMigration implements UpgradeMigrationInterface
{
    /**
     * @var StatusFactory
     */
    protected $orderStatusFactory;

    /**
     * RmaOrderStatusMigration constructor.
     * @param StatusFactory $orderStatusFactory
     */
    public function __construct(
        StatusFactory $orderStatusFactory
    )
    {
        $this->orderStatusFactory = $orderStatusFactory;
    }

    /**
     * Create Exchange status if it doesn't exist
     */
    public function apply(SetupInterface $setup = null)
    {
        $this->orderStatusFactory->create()
            ->setData('status', Status::STATUS_PENDING_EXCHANGE)
            ->setData('label', 'Pending Exchange')
            ->assignState(Order::STATE_HOLDED, true, true)
            ->save();
    }
}