<?php
/**
 * @vendor      Kurufootwear
 * @module      Kurufootwear_Migration
 * @author      Krisjanis Veinbahs <info@scandiweb.com>
 * @copyright   Copyright (c) 2017 Scandiweb, Ltd (http://scandiweb.com)
 * @license     http://opensource.org/licenses/afl-3.0.php Academic Free License (AFL 3.0)
 */
namespace Kurufootwear\Migration\Setup\Migration;

use Kurufootwear\Migration\Api\UpgradeMigrationInterface;
use Magento\Framework\Setup\SetupInterface;
use Magento\Framework\Filesystem;
use Kurufootwear\Migration\Helper\FileParser;
use Magento\Cms\Model\BlockFactory;
use Kurufootwear\Migration\Helper\FileHelper;

class ContactBlockMigration implements UpgradeMigrationInterface
{
    /**
     * @var Filesystem
     */
    private $filesystem;

    /**
     * @var FileHelper
     */
    private $fileHelper;

    /**
     * @var BlockFactory
     */
    private $blockFactory;

    /**
     * @var FileParser
     */
    private $fileParser;

    /**
     * FooterBlocksMigration constructor.
     * @param Filesystem $filesystem
     * @param FileHelper $fileHelper
     * @param BlockFactory $blockFactory
     * @param FileParser $fileParser
     */
    public function __construct(
        Filesystem $filesystem,
        FileHelper $fileHelper,
        BlockFactory $blockFactory,
        FileParser $fileParser
    ) {
        $this->filesystem = $filesystem;
        $this->fileHelper = $fileHelper;
        $this->blockFactory = $blockFactory;
        $this->fileParser = $fileParser;
    }

    /**
     * @inheritDoc
     */
    public function apply(SetupInterface $setup = null)
    {
        $images = [
            'icons' => [
                'clock.svg',
                'envelope.svg',
                'google_maps_marker.png',
                'phone.svg',
                'questions.svg',
            ],
            'images' => [
                'infowindow-tail'
            ],
            'logos' => [
                'logo_new.svg'
            ]
        ];

        foreach ($images as $map => $files) {
            $this->fileHelper->copyMediaFiles($files, $map);
        }

        $cmsBlocks = [
            [
                'file_name' => 'contact_info.html',
                'title' => 'Contact Us block',
                'identifier' => 'contact_info'
            ]
        ];

        foreach ($cmsBlocks as $cmsBlock) {
            $content = '';

            if ($cmsBlock['file_name'] != '') {
                $content = $this->fileParser->getHtmlContent('blocks/' . $cmsBlock['file_name']);
            }

            $blockData = [
                'title' => $cmsBlock['title'],
                'identifier' => $cmsBlock['identifier'],
                'content' => $content,
                'stores' => [0]
            ];

            $block = $this->blockFactory->create()->load($blockData['identifier'], 'identifier');

            if ($block->getId()) {
                $block->setTitle($blockData['title'])
                    ->setContent($blockData['content'])
                    ->setStores($blockData['stores'])
                    ->save();
            } else {
                $block->addData($blockData)
                    ->save();
            }
        }
    }
}
