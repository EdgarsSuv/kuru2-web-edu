<?php
/**
 * @category    Kurufootwear
 * @package     Kurufootwear\Migration
 * @author      Viesturs Ruzans <viestursr@scandiweb.com>
 * @copyright   Copyright (c) 2017 Scandiweb, Inc (http://scandiweb.com)
 * @license     http://opensource.org/licenses/OSL-3.0 The Open Software License 3.0 (OSL-3.0)
 */
use Magento\Framework\Component\ComponentRegistrar;

ComponentRegistrar::register(
    ComponentRegistrar::MODULE,
    'Kurufootwear_Migration',
    __DIR__
);
