Kurufootwear_Migration changelog
=============================

0.2.184:
- Sales points block for new PDP layout page

0.2.183:
- Change VIEW ALL menu entry copyright

0.2.182:
- Created guest order status page

0.2.181:
- Changed chat svg icon stroke color to black
- Add package svg icon

0.2.179:
- Create new Rewards rule for newsletter subscription

0.2.179:
- Update Category CMS page links

0.2.178:
- Create PDP size chart for socks

0.2.177:
- Update unsubscribe page

0.2.176:
- Update Store hours format

0.2.175:
- Update Category CMS page links

0.2.174:
- Creating new product attribute set "Socks" and socks size attribute

0.2.173:
- Benefits data migration for use in Amasty SEO fields

0.2.172:
- Update Category CMS page links

0.2.171:
- Sales points block for socks PDP page

0.2.170:
- Update Category CMS page links

0.2.169:
- Update PDP size chart

0.2.168:
- Update footer links

0.2.167:
- Update Technology CMS block Kurusole sub-title to have bold "Kuru"

0.2.166:
- Clean up incorrectly dispensed "Earned ... points for order #..."

0.2.165:
- Update Category CMS page links

0.2.162:
- Fix and populate duplicate product configurations

0.2.159:
- Update "Best for walking" page links

0.2.157:
- Update Reward Points configurations

0.2.156:
- Update "Why Kuru" CMS page icons

0.2.154:
- Update homepage slider URL's

0.2.153:
- Restructure Unsubscribe CMS page

0.2.152:
- Update Store postcode

0.2.151:
- Update Contact Info block URL's

0.2.150:
- Update Category CMS block structure, URL's

0.2.149:
- Update Category Menu URL's

0.2.148:
- Update Homepage banner URL's

0.2.147:
- Set infinite pagination size

0.2.146:
- Update Reward Points Tax configurations

0.2.145:
- Added category migration to use include in menu attribute
- Added wide products to wide category

0.2.144:
- Update GTM configurations

0.2.143:
- Bump store configuration version to do a remigration

0.2.142:
- Update Footer links block copyright

0.2.141:
- Update Category menu copyright

0.2.140:
- White shoes category add
- Benefits wide width removal

0.2.139:
- Set default postcode from '*' to nothing

0.2.138:
- Updated Category CMS blocks

0.2.137:
- Set RMA request period to 45

0.2.134-135:
- Resolved wide product issue with sizechartgit

0.2.133:
- Remigrate new benefits icons

0.2.132:
- Remigrate updated Footer CMS blocks

0.2.130:
- Updated Experts Corner CMS block

0.2.129: 
- Updated Header content

0.2.128:
- Change shopping bag related product limit to 5

0.2.127:
- Removed Wide with attribute option
- Added amasty filter settings

0.2.126:
- Updated Category CMS blocks

0.2.125:
- Updated Minicart Shipping Promo CMS block

0.2.124:
- Updated Category CMS blocks

0.2.123:
- Updated Experts Corner CMS block

0.2.122:
- Updated Infinite scroll configurations

0.2.121:
- Updated Category CMS blocks

0.2.120:
- Updated Category CMS blocks

0.2.119:
- Updated Homepage slider

0.2.118:
- Updated "Best Walking Shoes" CMS block

0.2.117:
- Added Universal Template page for CMS pages and blocks

0.2.116:
- Updated "Innovation" CMS page

0.2.115:
- Updated "Why KURU" CMS page

0.2.114:
- Remigrate Benefits icons

0.2.113:
- Updated "Why KURU" CMS page

0.2.112:
- Updated Plantar fasciitis CMS page

0.2.111:
- (Re)migrate proper RP earning rule 

0.2.110:
- Remove unnecessary payment methods in store config

0.2.109:
- Fix Amasty Infinite scroll store configuration syntax mistakes
- Fix Scandi Gtm store configuration syntax mistakes

0.2.107:
- Upgrade script for improving product duplicates

0.2.106:
- Updated Homepage slider

0.2.105:
- Added attribute for product name and color for duplicates

0.2.104:
- Added plantar fasciitis cms page (/experts-corner/foot-care/plantar-fasciitis.html)

0.2.103:
- Updated best walking shoes cms page (/experts-corner/best-walking-shoes.html)

0.2.102:
- Updated Homepage Promo CMS blocks

0.2.101:
- Updated "Lightweight Magic" CMS block

0.2.100:
- Updated "Yotpo UGC gallery" CMS block

0.2.99:
- Updated Footer content links blocks

0.2.98:
- Updated Store Configuration for Email Templates

0.2.97:
- Updated custom Email templates

0.2.96:
- Added best walking shoes cms page (/experts-corner/best-walking-shoes.html)

0.2.95:
- Migrated old shipping flatrates

0.2.94:
- Set up Amasty Multiflatrate configurations

0.2.93:
- Test image migration

0.2.92:
- Added experts corner cms page (/experts-corner)

0.2.90:
- Added migration script to create sidebar block for category cms pages

0.2.89:
- Update GTM configurations

0.2.88:
- Set custom Magic Zoom configurations

0.2.87:
- Updated "Yotpo UGC gallery" CMS block

0.2.86:
- Updated "Innovation" CMS page

0.2.85:
- Updated "Why KURU" CMS page

0.2.84:
- Enable checkout comments block configuration

0.2.83: 
- Updated Header content

0.2.82:
- Updated "Why KURU" CMS page

0.2.80: 
- Updated Header content

0.2.79:
- Added "Innovation" CMS page

0.2.78
- Added infinite scroll settings

0.2.77
- Added ProductImagesMigration

0.2.76:
- Updated "Why KURU" CMS page

0.2.75:
- Updated Technology CMS page 

0.2.74:
- Created "Why KURU" CMS page

0.2.73:
- Configure Mageworx SEO extension store settings

0.2.72:
- Updated migration script to update existing products

0.2.71:
- Updated Store Information

0.2.70: 
- Created Header Promo CMS block

0.2.69:
- Updated Header content

0.2.68:
- Updated Store Information

0.2.67:
- Updated Contact Info CMS blocks

0.2.66:
- Reset default layout/theme to category pages.

0.2.65:
- Removed Celigo related cron jobs from sotre configuration

0.2.64:
- Update Yotpo credential settings

0.2.62:
- Updated migration with correct parent sku

0.2.61:
- Updated related SKUs migration

0.2.61:
- Updated Configurable products

0.2.60:
- Add missing Broad Toebox benefit icon

0.2.59:
- Updated Yotpo Reviews CMS block on the Homepage

0.2.58:
- Added Reviews CMS page

0.2.57
- Added missing color option swatches.

0.2.56
- Added color option swatches.

0.2.55:
- Update PDP Salespoint block text

0.2.54:
- Change payment method sort order

0.2.53:
- Updated Footer delivery block

0.2.52:
- Changed Braintree payment method titles

0.2.51:
- Added configuration for Wyomind Data Feed Manager

0.2.50:
- Added Help Submenu icons to the media folder

0.2.49:
- Updated the Header menu to add View all links

0.2.48
- Added Shipping carrier FedEx configuration

0.2.47
- Added Weltpixel Maxmind configuration

0.2.46:
- Add correct reward earning/spending rules

0.2.45:
- Add several store configurations

0.2.44:
- Create custom Pending exchange order status

0.2.43:
- Update Zendesk module

0.2.42:
- Updated Footer content links block

0.2.41:
- Filled header menu

0.2.40:
- Add Kurufootwear's Yotpo API keys

0.2.39:
- Updated filterable attributes positions

0.2.38:
- Removed newsroom cms pages

0.2.37:
- Added filter attribute for duplicates 

0.2.35-36:
- Added wide product attributes and inserted data to products

0.2.33:
- Added RMA custom fields

0.2.31:
- Size chart block migration

0.2.30:
- Updated No-route CMS page

0.2.29:
- Added Technology CMS page

0.2.28:
- Split of contact us page blocks

0.2.27:
- Create a standalone Social Link CMS block

0.2.26:
- Added Contact page icons and images
- Added Contact page cms block

0.2.25:
- Updated No-route CMS page
- Added No-route related images to the media folder

0.2.24:
- Added Checkout images to the media folder

0.2.23:
- Updated Store Information

0.2.21-22:
- Create product duplicates on FE

0.2.20:
- Enabled Wishlist functionality

0.2.19:
- Loader icon migration

0.2.18:
- Benefits icon migration

0.2.17:
- Added Size Chart for Migration_Shoes

0.2.16:
- Add Kuru Technology CMS block

0.2.15:
- Add 3xSalespoints CMS block

0.2.14:
- Enable Amasty ajax page load

0.2.13:
- Footer blocks update

0.2.12:
- Updated Copyright CMS block

0.2.11:
- Added Minicart Shipping Promo CMS block

0.2.10:
- Added li element as promo block

0.2.9:
- Add wrapper to "Lightweight Magic" block

0.2.8:
- Add dark tint to Yotpo UGC gallery mid column

0.2.7:
- Set Yotpo carousel reviews to site-only
- Set Yotpo carousel review count to 3

0.2.6:
- Home page Promo CMS blocks update

0.2.5:
- Added Copyright CMS block

0.2.4:
- Create "Yotpo UGC gallery" CMS block

0.2.3:
- Added slider for hompage

0.2.2:
- Create "Yotpo site reviews carousel" CMS block

0.2.1:
- Create "Lightweight Magic" CMS block

0.1.9:
- Updated promo blocks

0.1.8:
- Added HP promo blocks

0.1.7:
- Header menu

0.1.6:
- Added Hp slider blocks

0.1.5:
- Footer blocks update

0.1.4:
- Footer blocks and images

0.1.3:
- Menumanager database table update - added column for CSS class

0.1.2:
- Update dev guidelines CMS page

0.1.1:
- Added dev guidelines CMS page

0.1.0:
- Changed theme to Kurufootwear/default
