<?php
/**
 * @category  Kurufootwear
 * @package   Kurufootwear\Migration
 * @author    Viesturs Ruzans <viestursr@scandiweb.com>
 * @copyright Copyright (c) 2017 Scandiweb, Inc (http://scandiweb.com)
 * @license   http://opensource.org/licenses/OSL-3.0 The Open Software License 3.0 (OSL-3.0)
 */

namespace Kurufootwear\Migration\Api;

use \Magento\Framework\Setup\SetupInterface;

interface UpgradeMigrationInterface
{
    /**
     * Applies migration.
     *
     * @param SetupInterface $setup
     * @return
     */
    public function apply(SetupInterface $setup = null);
}
