#!/usr/bin/env bash

export IFS=","

rm user_output.csv > /dev/null 2>&1

cat admin_users.csv | while read un fn ln e; 
do
	echo "Adding user $fn $ln";
	
	pw="kjn786KK6dbnfN8"
	
	php ../../../../../../../bin/magento admin:user:create --admin-user=$un --admin-firstname=$fn --admin-lastname=$ln --admin-email=$e --admin-password=$pw
	echo $un,$fn,$ln,$e,$pw >> user_output.csv
done
