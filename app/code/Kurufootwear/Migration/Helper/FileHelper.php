<?php
/**
 * @category  Kurufootwear
 * @package   Kurufootwear\Migration
 * @author    Kristaps Stalidzāns <kristapss@scandiweb.com>
 * @copyright Copyright (c) 2017 Scandiweb, Inc (http://scandiweb.com)
 * @license   http://opensource.org/licenses/OSL-3.0 The Open Software License 3.0 (OSL-3.0)
 */

namespace Kurufootwear\Migration\Helper;

use Magento\Framework\App\Helper\AbstractHelper;
use Magento\Framework\App\Helper\Context;
use Magento\Framework\Module\Dir\Reader as ModuleReader;
use Magento\Framework\App\Filesystem\DirectoryList;
use Magento\Framework\Filesystem;
use Magento\Framework\FilesystemFactory;

class FileHelper extends AbstractHelper
{
    const MODULE_FILES_DIR = 'files';

    /**
     * @var ModuleReader
     */
    protected $_moduleReader;

    /**
     * @var Filesystem
     */
    protected $_fileSystem;

    /**
     * @var FilesystemFactory
     */
    protected $_fileSystemFactory;

    /**
     * @param Context $context
     * @param ModuleReader $moduleReader ,
     * @param Filesystem $fileSystem
     * @param FilesystemFactory $fileSystemFactory
     */
    public function __construct(
        Context $context,
        ModuleReader $moduleReader,
        Filesystem $fileSystem,
        FilesystemFactory $fileSystemFactory
    ) {
        $this->_fileSystem = $fileSystem;
        $this->_moduleReader = $moduleReader;
        $this->_fileSystemFactory = $fileSystemFactory;
        parent::__construct($context);
    }

    /**
     * Copies an array of files from a source to a destination media directory.
     *
     * @param array $files
     * @param string $folderPath
     */
    public function copyMediaFiles($files, $folderPath = null)
    {
        $sourcePath = $this->_getSourceMediaDirectory();
        $destinationPath = $this->_getDestinationMediaDirectory($folderPath);

        $rootDirectory = $this->_fileSystem->getDirectoryWrite(DirectoryList::ROOT);

        $relativeSourcePath = str_replace($rootDirectory->getAbsolutePath(), '', $sourcePath);
        $relativeDestinationPath = str_replace($rootDirectory->getAbsolutePath(), '', $destinationPath);

        foreach ($files as $file) {
            if ($rootDirectory->isFile($relativeSourcePath . $file)) {
                $rootDirectory->copyFile($relativeSourcePath . $file, $relativeDestinationPath . $file);
            }
        }
    }

    /**
     * Delete media files
     *
     * @param $files
     * @param null $folderPath
     */
    public function deleteMediaFiles($files, $folderPath = null)
    {
        $sourcePath = $this->_getDestinationMediaDirectory($folderPath);

        $rootDirectory = $this->_fileSystem->getDirectoryWrite(DirectoryList::ROOT);

        $relativeSourcePath = str_replace($rootDirectory->getAbsolutePath(), '', $sourcePath);

        foreach ($files as $file) {
            if ($rootDirectory->isFile($relativeSourcePath . $file)) {
                $rootDirectory->delete($relativeSourcePath . $file);
            }
        }
    }

    /**
     * Gets the directory from which media files are copied.
     *
     * @return string
     */
    protected function _getSourceMediaDirectory()
    {
        return $this->_moduleReader->getModuleDir('',
                'Kurufootwear_Migration') . DIRECTORY_SEPARATOR . 'files' . DIRECTORY_SEPARATOR . 'data' . DIRECTORY_SEPARATOR . 'media' . DIRECTORY_SEPARATOR . 'images' . DIRECTORY_SEPARATOR;
    }

    /**
     * Gets the directory in which media files are copied to.
     *
     * @param string $folderPath
     *
     * @return string
     */
    protected function _getDestinationMediaDirectory($folderPath = 'wysiwyg')
    {
        if (!$folderPath) {
            return $this->_fileSystem->getDirectoryRead(DirectoryList::MEDIA)->getAbsolutePath();
        }

        return $this->_fileSystem->getDirectoryWrite(DirectoryList::MEDIA)->getAbsolutePath() . $folderPath . DIRECTORY_SEPARATOR;
    }
}
