<?php
/**
 * @category  Kurufootwear
 * @package   Kurufootwear\Migration
 * @author    Viesturs Ruzans <viestursr@scandiweb.com>
 * @copyright Copyright (c) 2017 Scandiweb, Inc (http://scandiweb.com)
 * @license   http://opensource.org/licenses/OSL-3.0 The Open Software License 3.0 (OSL-3.0)
 */

namespace Kurufootwear\Migration\Helper;

use Exception;
use Magento\Config\Model\ResourceModel\Config as ConfigResource;
use Magento\Store\Model\Store;
use Magento\Store\Model\StoreManager;

class Configurator
{
    const SCOPE_DEFAULT = 'default';
    const SCOPE_WEBSITES = 'websites';
    const SCOPE_STORES = 'stores';

    /** @var ConfigResource */
    private $configResource;

    /** @var array */
    private $scopeIds = [];

    /** @var StoreManager */
    private $storeManager;

    /**
     * ConfigHelper constructor.
     *
     * @param ConfigResource $configResource
     * @param StoreManager $storeManager
     */
    public function __construct(ConfigResource $configResource, StoreManager $storeManager)
    {
        $this->configResource = $configResource;
        $this->storeManager = $storeManager;
    }

    /**
     * @param string $path
     * @param string $scope
     * @param int $scopeId
     * @return $this
     */
    public function delete(
        string $path,
        string $scope = self::SCOPE_DEFAULT,
        $scopeId = Store::DEFAULT_STORE_ID
    ) {
        $scopeId = $this->getScopeId($scope, $scopeId);
        $this->configResource->deleteConfig($path, $scope, $scopeId);

        return $this;
    }

    /**
     * Saves or deletes the config value.
     *
     * @param array $data
     * @param string $scope
     * @param int $scopeId
     * @return $this
     */
    public function process(
        array $data,
        $scope = self::SCOPE_DEFAULT,
        $scopeId = Store::DEFAULT_STORE_ID
    ) {
        foreach ($data as $key => $value) {
            if (is_null($value)) {
                $this->delete($key, $scope, $scopeId);
            } else {
                $this->save($key, $value, $scope, $scopeId);
            }
        }

        return $this;
    }

    /**
     * Saves config value.
     *
     * @param string $path
     * @param string $value
     * @param string $scope
     * @param int $scopeId
     * @return $this
     */
    public function save(
        string $path,
        string $value,
        string $scope = self::SCOPE_DEFAULT,
        $scopeId = Store::DEFAULT_STORE_ID
    ) {
        $scopeId = $this->getScopeId($scope, $scopeId);
        $this->configResource->saveConfig($path, $value, $scope, $scopeId);

        return $this;
    }

    /**
     * @param string $scope
     * @param $scopeId
     * @return int|mixed
     * @throws Exception
     */
    private function getScopeId(string $scope, $scopeId)
    {
        if (array_key_exists($scope, $this->scopeIds)
            && array_key_exists($scopeId, $this->scopeIds[$scope])
        ) {
            return $this->scopeIds[$scope][$scopeId];
        }

        switch ($scope) {
            case self::SCOPE_WEBSITES:
                $result = $this->storeManager->getWebsite($scopeId)->getId();
                break;

            case self::SCOPE_STORES:
                $result = $this->storeManager->getStore($scopeId)->getId();
                break;

            case self::SCOPE_DEFAULT:
                $result = Store::DEFAULT_STORE_ID;
                break;

            default:
                throw new Exception(sprintf('Invalid scope %s / %s.', $scope, $scopeId));
        }

        $this->scopeIds[$scope][$scopeId] = $result;

        return $result;
    }
}
