<?php
/**
 * @category  Kurufootwear
 * @package   Kurufootwear\Migration
 * @author    Viesturs Ruzans <viestursr@scandiweb.com>
 * @copyright Copyright (c) 2017 Scandiweb, Inc (http://scandiweb.com)
 * @license   http://opensource.org/licenses/OSL-3.0 The Open Software License 3.0 (OSL-3.0)
 */

namespace Kurufootwear\Migration\Helper;

use Magento\Framework\App\Filesystem\DirectoryList;
use Magento\Framework\Filesystem;
use Magento\Framework\File\Csv as CsvProcessor;

class FileParser
{
    /**
     * Path to the data files from root magento folder
     */
    const PATH_TO_DATA = 'code/Kurufootwear/Migration/files/data/';

    /**
     * @var \Magento\Framework\Filesystem\Directory\ReadInterface
     */
    protected $rootDirectory;

    /**
     * @var CsvProcessor
     */
    protected $csvProcessor;

    /**
     * @var DirectoryList
     */
    protected $directoryList;

    /**
     * FileParser constructor.
     *
     * @param Filesystem $fileSystem
     * @param CsvProcessor $csvProcessor
     * @param DirectoryList $directoryList
     */
    public function __construct(
        Filesystem $fileSystem,
        CsvProcessor $csvProcessor,
        DirectoryList $directoryList
    ) {
        $this->rootDirectory = $fileSystem->getDirectoryRead(DirectoryList::APP);
        $this->csvProcessor = $csvProcessor;
        $this->directoryList = $directoryList;
    }

    /**
     * Get content from html file
     *
     * @param string $filePath Relative path to the html file from data/html folder
     *
     * @return string
     */
    public function getHtmlContent($filePath)
    {
        return $this->rootDirectory->readFile(
            sprintf(
                '%shtml/%s',
                self::PATH_TO_DATA,
                $filePath
            )
        );
    }

    /**
     * Return content of json file as associative array
     *
     * @param string $filePath Relative path to the json file from data/json folder
     *
     * @return array
     */
    public function getJSONContent($filePath)
    {
        $data = $this->rootDirectory->readFile(
            sprintf(
                '%sjson/%s',
                self::PATH_TO_DATA,
                $filePath
            )
        );

        return json_decode($data, true);
    }

    /**
     * Get content of csv file
     *
     * @param $filePath
     *
     * @return array
     */
    public function getCsvContent($filePath)
    {
        return $this->csvProcessor->getData(
            sprintf(
                '%s/%scsv/%s',
                $this->directoryList->getPath(DirectoryList::APP),
                self::PATH_TO_DATA,
                $filePath
            )
        );
    }
}
