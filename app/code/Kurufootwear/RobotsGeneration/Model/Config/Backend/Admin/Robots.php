<?php
/**
 * @category Kurufootwear
 * @package Kurufootwear\RobotsGeneration
 * @author Dmitriy Gallyamov <dmitrijs@scnadiweb.com>
 * @author Krisjanis Veinbahs <krisjanisv@scnadiweb.com>
 * @copyright Copyright (c) 2016 Scandiweb, Ltd (http://scandiweb.com)
 * @license http://opensource.org/licenses/afl-3.0.php Academic Free License (AFL 3.0)\
 */
namespace Kurufootwear\RobotsGeneration\Model\Config\Backend\Admin;

use Magento\Config\Model\Config\Backend\Admin\Robots as MagentoRobots;
use Magento\Framework\App\Cache\TypeListInterface;
use Magento\Framework\App\Config\ScopeConfigInterface;
use Magento\Framework\App\Filesystem\DirectoryList;
use Magento\Framework\Data\Collection\AbstractDb;
use Magento\Framework\Filesystem;
use Magento\Framework\Model\Context;
use Magento\Framework\Model\ResourceModel\AbstractResource;
use Magento\Framework\Registry;

class Robots extends MagentoRobots
{

    /**
     * @param Context $context
     * @param Registry $registry
     * @param ScopeConfigInterface $config
     * @param TypeListInterface $cacheTypeList
     * @param Filesystem $filesystem
     * @param AbstractResource $resource
     * @param AbstractDb $resourceCollection
     * @param array $data
     */
    public function __construct(
        Context $context,
        Registry $registry,
        ScopeConfigInterface $config,
        TypeListInterface $cacheTypeList,
        Filesystem $filesystem,
        AbstractResource $resource = null,
        AbstractDb $resourceCollection = null,
        array $data = []
    ) {
        parent::__construct(
            $context,
            $registry,
            $config,
            $cacheTypeList,
            $filesystem,
            $resource,
            $resourceCollection,
            $data
        );

        $this->_directory = $filesystem->getDirectoryWrite(DirectoryList::MEDIA);
        $this->_file = 'robots.txt';
    }
}