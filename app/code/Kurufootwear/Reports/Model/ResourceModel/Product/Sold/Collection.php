<?php
/**
 * @category    Kurufootwear
 * @package     Kurufootwear\Reports
 * @author      Krisjanis Veinbahs <info@scandiweb.com>
 * @copyright   Copyright (c) 2018 Scandiweb, Inc (http://scandiweb.com)
 * @license     http://opensource.org/licenses/OSL-3.0 The Open Software License 3.0 (OSL-3.0)
 */
namespace Kurufootwear\Reports\Model\ResourceModel\Product\Sold;

use Magento\Reports\Model\ResourceModel\Product\Sold\Collection as MagentoSoldProductCollection;
use Magento\Sales\Model\Order;

/**
 * Class Collection
 *
 * Overriden for more generating more
 * detailed sold product reports
 */
class Collection extends MagentoSoldProductCollection
{
    /**
     * Overriden sold product quantity function
     * with added product id's to returned structure
     *
     * @param string $from
     * @param string $to
     *
     * @return $this
     */
    public function addOrderedQty($from = '', $to = '')
    {
        $connection = $this->getConnection();
        $orderTableAliasName = $connection->quoteIdentifier('order');

        $orderJoinCondition = [
            $orderTableAliasName . '.entity_id = order_items.order_id',
            $connection->quoteInto("{$orderTableAliasName}.state <> ?", Order::STATE_CANCELED),
        ];

        if ($from != '' && $to != '') {
            $fieldName = $orderTableAliasName . '.created_at';
            $orderJoinCondition[] = $this->prepareBetweenSql($fieldName, $from, $to);
        }

        // Override - This select now includes order_items_product_id
        $this->getSelect()->reset()->from(
            ['order_items' => $this->getTable('sales_order_item')],
            [
                'ordered_qty' => 'SUM(order_items.qty_ordered)',
                'order_items_name' => 'order_items.name',
                'order_items_product_id' => 'order_items.product_id',
                'order_items_created_at' => 'order_items.created_at'
            ]
        )->joinInner(
            ['order' => $this->getTable('sales_order')],
            implode(' AND ', $orderJoinCondition),
            []
        )->group(
            'order_items.product_id'
        )->having(
            'SUM(order_items.qty_ordered) > ?',
            0
        );

        return $this;
    }
}
