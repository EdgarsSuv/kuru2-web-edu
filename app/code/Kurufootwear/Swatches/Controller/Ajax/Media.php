<?php
/**
 * @category     Kurufootwear
 * @package      Kurufootwear/Swatches
 * @author       Jim McGowen <jim@kurufootwear.com>
 * @copyright    Copyright (c)2018 KURU Footwear
 */

namespace Kurufootwear\Swatches\Controller\Ajax;

use Magento\Catalog\Api\ProductRepositoryInterface;
use Magento\Catalog\Model\ProductFactory;
use Magento\Catalog\Model\ResourceModel\Product\Collection as ProductCollection;
use Magento\Framework\App\Action\Context;
use Magento\Catalog\Model\Product;
use Magento\Framework\Controller\ResultFactory;
use Magento\Swatches\Helper\Data;
use Magento\PageCache\Model\Config;
use Magento\Framework\App\Config\ScopeConfigInterface;
use Amasty\Label\Helper\Data as AmastyLabelHelper;

class Media extends \Magento\Swatches\Controller\Ajax\Media
{
    const AMASTY_LABEL_CONTAINER_PRODUCT = 'amasty_label/display/product';
    
    /** @var ProductCollection  */
    protected $productCollection;

    /**
     * @var ProductRepositoryInterface
     */
    protected $productRepository;

    /**
     * @var ScopeConfigInterface
     */
    private $scopeConfig;
    
    /** @var AmastyLabelHelper */
    private $labelHelper;
    
    /**
     * Media constructor.
     *
     * @param Context $context
     * @param Data $swatchHelper
     * @param ProductFactory $productModelFactory
     * @param ProductCollection $productCollection
     * @param ProductRepositoryInterface $productRepository
     * @param ScopeConfigInterface $scopeConfig
     * @param AmastyLabelHelper $labelHelper
     */
    public function __construct(
        Context $context, 
        Data $swatchHelper,
        ProductFactory $productModelFactory,
        ProductCollection $productCollection,
        ProductRepositoryInterface $productRepository,
        ScopeConfigInterface $scopeConfig,
        AmastyLabelHelper $labelHelper
    ) {
        $this->productCollection = $productCollection;
        $this->productRepository = $productRepository;
        $this->scopeConfig = $scopeConfig;
        $this->labelHelper = $labelHelper;

        parent::__construct($context, $swatchHelper, $productModelFactory);
    }
    
    /**
     * @return \Magento\Framework\Controller\Result\Json
     */
    public function execute()
    {
        $product = null;
        $productMedia = [];
        if ($productId = (int)$this->getRequest()->getParam('product_id')) {
            $attributes = (array)$this->getRequest()->getParam('attributes');

            // Allow Varnish to cache these requests
            $this->_response->setPublicHeaders($this->scopeConfig->getValue(Config::XML_PAGECACHE_TTL));

            // Compatibility with Varnish invalidation
            $contentIdentifiers = $attributes;
            $contentIdentifiers['catalog_product'] = $productId;
            $tags = [];
            foreach ($contentIdentifiers as $contentIdentifier => $identifierValue) {
                $tags[] = sprintf(
                    '%s_%s',
                    $contentIdentifier,
                    $identifierValue
                );
            }
            $this->_response->setHeader('X-Magento-Tags', implode(',', $tags));

            $currentConfigurable = $this->productModelFactory->create()->load($productId);
            $parentSku = $currentConfigurable->getData('parent_sku');
            if ($parentSku) {
                try {
                    $currentConfigurable = $this->productRepository->get($parentSku);
                } catch (\Exception $e) {}
            }

            if (!empty($attributes)) {
                $product = $this->getProductVariationWithMedia($currentConfigurable, $attributes);
            }
            if ((empty($product) || (!$product->getImage() || $product->getImage() == 'no_selection'))
                && isset($currentConfigurable)
            ) {
                $product = $currentConfigurable;
            }
            $productMedia = $this->swatchHelper->getProductMediaGallery($product);
    
            // This controller is used by both the Product Detail and the Category pages
            // We only want to do the following on the Product Detail page. We only get the 
            // $additional['color'] data when this method is called from the Product Detail page.
            // Hence this check:
            $additional = $this->getRequest()->getParam('additional');
            if (!empty($additional['color'])) {
                // Exchange the base image for the first image in the gallery
                // This is because the base image should be hidden and we don't want it to show on the PDP
                $firstGalleryImage = reset($productMedia['gallery']);
                $productMedia['large'] = $firstGalleryImage['large'];
                $productMedia['medium'] = $firstGalleryImage['medium'];
                $productMedia['small'] = $firstGalleryImage['small'];
    
                // Add the Amasty product label
                $productMedia['label'] = [];
                $productMedia['label']['container'] = $this->scopeConfig->getValue(self::AMASTY_LABEL_CONTAINER_PRODUCT);
                $productMedia['label']['html'] = $this->labelHelper->renderProductLabel($product, 'product');
            }
        }
        
        /** @var \Magento\Framework\Controller\Result\Json $resultJson */
        $resultJson = $this->resultFactory->create(ResultFactory::TYPE_JSON);
        $resultJson->setData($productMedia);

        // TODO: The json has the images in the correct order (by position) but for some reason, the page displays them out of order!
        
        return $resultJson;
    }
    
    /**
     * @param Product $currentConfigurable
     * @param array $attributes
     *
     * @return $this|bool|\Magento\Catalog\Api\Data\ProductInterface|null
     */
    protected function getProductVariationWithMedia(Product $currentConfigurable, array $attributes)
    {
        $product = null;
        $layeredAttributes = [];
        $configurableAttributes = $this->swatchHelper->getAttributesFromConfigurable($currentConfigurable);
        if ($configurableAttributes) {
            $layeredAttributes = $this->getLayeredAttributesIfExists($configurableAttributes);
        }
        $resultAttributes = array_merge($layeredAttributes, $attributes);
        
        // Find the duplicate product
        $additional = $this->getRequest()->getParam('additional');
        if (!empty($additional['color'])) {
            /** @var \Magento\Catalog\Model\Product $duplicateProduct */
            $duplicateProduct = $this->productCollection
                ->addAttributeToSelect('product_color_data')
                ->addAttributeToSelect('parent_sku')
                ->addAttributeToFilter('product_color_data', $additional['color'])
                ->addAttributeToFilter('parent_sku', $currentConfigurable->getSku())
                ->addFieldToFilter('type_id', 'configurable')
                ->getFirstItem();
            if ($duplicateProduct->hasData()) {
                // We have a duplicate so we will load the gallery from it
                $product = $this->productModelFactory->create()->load($duplicateProduct->getId());
            }
        }
        
        if (!$product || !$product->hasData()) {
            // No duplicate product found so we need to fall back to the first child or variation
            $product = $this->swatchHelper->loadVariationByFallback($currentConfigurable, $resultAttributes);
            if (!$product || (!$product->getImage() || $product->getImage() == 'no_selection')) {
                $product = $this->swatchHelper->loadFirstVariationWithSwatchImage($currentConfigurable, $resultAttributes);
            }
            if (!$product) {
                $product = $this->swatchHelper->loadFirstVariationWithImage($currentConfigurable, $resultAttributes);
            }
        }
    
        return $product;
    }
}
