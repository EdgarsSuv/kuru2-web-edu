<?php
/**
 * @vendor Kurufootwear
 * @module Kurufootwear_Swatches
 * @author Aleksejs Baranovs <info@scandiweb.com>
 * @copyright Copyright (c) 2018 KURU Footwear. All rights reserved.
 */

namespace Kurufootwear\Swatches\Block\Product\Renderer;

use Scandiweb\Swatches\Block\Product\Renderer\Configurable as BaseConfigurable;

/**
 * Class Configurable
 * @package Kurufootwear\Swatches\Block\Product\Renderer
 */
class Configurable extends BaseConfigurable
{
    /**
     * Path to template file with Swatch renderer.
     */
    const SWATCH_RENDERER_TEMPLATE = 'Magento_Swatches::product/view/renderer_new_pdp.phtml';

    /**
     * Get renderer template
     *
     * @return string
     */
    protected function getRendererTemplate()
    {
        return $this->isProductHasSwatchAttribute() ?
            self::SWATCH_RENDERER_TEMPLATE : self::CONFIGURABLE_RENDERER_TEMPLATE;
    }
}
