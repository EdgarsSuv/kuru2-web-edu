<?php
/**
 * @category    Kurufootwear
 * @package     Kurufootwear\Swatches
 * @author      Aleksejs Baranovs <info@scandiweb.com>
 * @copyright   Copyright (c) 2018 Scandiweb, Inc (http://scandiweb.com)
 * @license     http://opensource.org/licenses/OSL-3.0 The Open Software License 3.0 (OSL-3.0)
 */

namespace Kurufootwear\Swatches\Helper;

/**
 * Class Media
 *
 * @package Kurufootwear\Swatches\Helper
 */
class Media extends \Magento\Swatches\Helper\Media
{
    /**
     * Generate swatch thumb and small swatch image
     *
     * @param string $imageUrl
     *
     * @return $this
     */
    public function generateSwatchVariations($imageUrl)
    {
        $absoluteImagePath = $this->mediaDirectory->getAbsolutePath($this->getAttributeSwatchPath($imageUrl));

        if (file_exists($absoluteImagePath)) {
            foreach ($this->swatchImageTypes as $swatchType) {
                $imageConfig = $this->getImageConfig();
                $swatchNamePath = $this->generateNamePath($imageConfig, $imageUrl, $swatchType);
                $image = $this->imageFactory->create($absoluteImagePath);
                $this->setupImageProperties($image);
                $image->resize($imageConfig[$swatchType]['width'], $imageConfig[$swatchType]['height']);
                $this->setupImageProperties($image, true);
                $image->save($swatchNamePath['path_for_save'], $swatchNamePath['name']);
            }
        }

        return $this;
    }
}
