/**
 * @category    Kurufootwear
 * @package     Kurufootwear\ApplePay
 * @author      Jim McGowen <jim@kurufootwear.com>
 * @copyright   Copyright (c) 2018 KURU Footwear
 */

var config = {
    paths: {
        braintreeClient: 'https://js.braintreegateway.com/web/3.34.1/js/client.min',
        braintreeApplePay: 'https://js.braintreegateway.com/web/3.34.1/js/apple-pay.min'
    }
};
