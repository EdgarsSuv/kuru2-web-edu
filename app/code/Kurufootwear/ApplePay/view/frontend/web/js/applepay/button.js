/**
 * @category    Kurufootwear
 * @package     Kurufootwear\ApplePay
 * @author      Jim McGowen <jim@kurufootwear.com>
 * @copyright   Copyright (c) 2018 KURU Footwear
 */

define(
    [
        'uiComponent',
        "knockout",
        "jquery",
        'braintreeClient',
        'braintreeApplePay',
        'mage/translate'
    ],
    function (
        Component,
        ko,
        $,
        braintree,
        applePay,
        $t
    ) {
        'use strict';

        return {
            /**
             * Key/option storage
             */
            options: {
                applePayAvailableKey: 'applePayAvailable'
            },

            /**
             * Initialize the Apple Pay button.
             * 
             * @param buttonWrap
             * @param api
             */
            init: function (buttonWrap, api) {
                var self = this;
    
                buttonWrap = $(buttonWrap);
                
                // Check if this payment method is active and device/browser is supported
                if (api.getIsActive() && this.deviceSupported()) {
                    console.info('Initializing Apple Pay button ' + buttonWrap.attr('id'));

                    // If previously ApplePay has been calculated as available, populate the wrapper immediately
                    if (localStorage.getItem(self.options.applePayAvailableKey) === "1") {
                        self.buttonWrapPopulate(buttonWrap);
                    }

                    // Init Braintree gateway API
                    braintree.create({
                        authorization: api.getClientToken()
                    }, function(clientErr, clientInstance) {
                        // Sanity check
                        if (clientErr) {
                            console.error('Error creating  Braintree client:', clientErr);
                            return;
                        }
        
                        // Init Braintree Apple Pay API
                        applePay.create({
                            client: clientInstance
                        }, function(applePayErr, applePayInstance) {
                            // Sanity check
                            if (applePayErr) {
                                console.error('Braintree ApplePay Error creating applePayInstance:', applePayErr);
                                return;
                            }
    
                            // Check for active payment method
                            self.canMakePaymentsWithActiveCard(applePayInstance);
                            
                            // Show the Apple pay button
                            // How it is shown depends on whether it in the side-cart or the cart page
                            if (buttonWrap.parent().is('li')) {
                                buttonWrap.css('display', 'block');
                            }
                            else {
                                buttonWrap.css('display', 'table-cell');
                            }
                            
                            // Create a button within the KO element, as apple pay can only be instantiated through
                            // a valid on click event (ko onclick bind interferes with this).
                            localStorage.setItem(self.options.applePayAvailableKey, "1");
                            var button = self.buttonWrapPopulate(buttonWrap);
                            
                            button.click(function(event) {
                                event.preventDefault();
                
                                // Payment request object
                                var paymentRequest = applePayInstance.createPaymentRequest(api.getPaymentRequest());
                                if (!paymentRequest) {
                                    console.error('Braintree ApplePay Unable to create paymentRequest', paymentRequest);
                                    alert($t("We're unable to take payments through Apple Pay at the moment. Please try an alternative payment method."));
                                    return;
                                }
                
                                // Init apple pay session
                                try {
                                    var session = new ApplePaySession(1, paymentRequest);
                                }
                                catch (err) {
                                    console.error('Braintree ApplePay Unable to create ApplePaySession', err);
                                    alert($t("We're unable to take payments through Apple Pay at the moment. Please try an alternative payment method."));
                                    return false;
                                }
                
                                // Handle invalid merchant
                                session.onvalidatemerchant = function(event) {
                                    applePayInstance.performValidation({
                                        validationURL: event.validationURL,
                                        displayName: api.getDisplayName()
                                    }, function(validationErr, merchantSession) {
                                        if (validationErr) {
                                            session.abort();
                                            console.error('Braintree ApplePay Error validating merchant:', validationErr);
                                            alert($t("We're unable to take payments through Apple Pay at the moment. Please try an alternative payment method."));
                                            return;
                                        }
                        
                                        session.completeMerchantValidation(merchantSession);
                                    });
                                };
                
                                // Attach payment auth event
                                session.onpaymentauthorized = function(event) {
                                    applePayInstance.tokenize({
                                        token: event.payment.token
                                    }, function(tokenizeErr, payload) {
                                        if (tokenizeErr) {
                                            console.error('Error tokenizing Apple Pay:', tokenizeErr);
                                            session.completePayment(ApplePaySession.STATUS_FAILURE);
                                            return;
                                        }
                        
                                        // Pass the nonce back to the payment method
                                        var nonce = payload.nonce;
                                        api.startPlaceOrder(nonce, event, session);
                                    });
                                };
                
                                // Attach onShippingContactSelect method
                                session.onshippingcontactselected = function(event) {
                                    return api.onShippingContactSelect(event, session);
                                };
                
                                // Attach onShippingMethodSelect method
                                session.onshippingmethodselected = function(event) {
                                    return api.onShippingMethodSelect(event, session);
                                };
                
                                session.begin();
                            });
                        });
                    });
                }
                else {
                    buttonWrap.remove();
                }
            },

            /**
             * Insert actual ApplePay click-handler button into the placeholder wrapper.
             * Return the functional button DOM element
             *
             * @param buttonWrap
             */
            buttonWrapPopulate: function(buttonWrap) {
                // Show the Apple pay button
                // How it is shown depends on whether it in the side-cart or the cart page
                if (buttonWrap.parent().is('li')) {
                    buttonWrap.css('display', 'block');
                } else {
                    buttonWrap.css('display', 'table-cell');
                }

                // Create a button within the KO element, as apple pay can only be instantiated through
                // a valid on click event (ko onclick bind interferes with this).
                var button = $('<div class="braintree-apple-pay-button">');

                // Append the button and image divs to the button wrapper
                var buttonImage = $('<div class="braintree-apple-pay-logo">');
                button.append(buttonImage);
                buttonWrap.empty().append(button);

                return button;
            },

            /**
             * Check the site is using HTTPS and apple pay is supported on this device.
             * 
             * @return boolean
             */
            deviceSupported: function () {
                if (location.protocol !== 'https:') {
                    console.warn("Braintree Apple Pay: Apple Pay requires your checkout be served over HTTPS");
                    return false;
                }

                if ((window.ApplePaySession && ApplePaySession.canMakePayments()) !== true) {
                    console.warn("Braintree ApplePay Apple Pay is not supported on this device/browser");
                    return false;
                }
    
                return true;
            },
    
            /**
             * Checks if customer has any active cards in their Apple Pay Wallet.
             * 
             * @param applePayInstance
             * @param callback
             */
            canMakePaymentsWithActiveCard: function (applePayInstance, callback) {
                var promise = ApplePaySession.canMakePaymentsWithActiveCard(applePayInstance.merchantIdentifier);
                promise.then(function (canMakePaymentsWithActiveCard) {
                    if (!canMakePaymentsWithActiveCard) {
                        console.warn("ApplePay Wallet has no active cards");
                    }
                    
                    if (typeof callback === 'function') {
                        callback(canMakePaymentsWithActiveCard);
                    }
                });
            }
        };
    }
);