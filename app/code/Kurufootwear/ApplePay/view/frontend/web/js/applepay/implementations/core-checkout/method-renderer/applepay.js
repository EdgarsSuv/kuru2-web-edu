/**
 * @category    Kurufootwear
 * @package     Kurufootwear\ApplePay
 * @author      Jim McGowen <jim@kurufootwear.com>
 * @copyright   Copyright (c) 2018 KURU Footwear
 */

define([
        'Magento_Checkout/js/view/payment/default',
        'Magento_Checkout/js/model/quote',
        'Kurufootwear_ApplePay/js/applepay/button'
    ], function (
        Component,
        quote,
        button
    ) {
        'use strict';
    
        return Component.extend({
            defaults: {
                template: 'Kurufootwear_ApplePay/applepay/core-checkout',
                paymentMethodNonce: null,
                grandTotalAmount: 0,
                deviceSupported: button.deviceSupported()
            },
            
            /**
             * Subscribe to grand totals
             */
            initObservable: function () {
                this._super();
                this.grandTotalAmount = parseFloat(quote.totals()['base_grand_total']).toFixed(2);
    
                quote.totals.subscribe(function () {
                    if (this.grandTotalAmount !== quote.totals()['base_grand_total']) {
                        this.grandTotalAmount = parseFloat(quote.totals()['base_grand_total']).toFixed(2);
                    }
                }.bind(this));
    
                return this;
            },
    
            /**
             * Apple Pay place order method
             */
            startPlaceOrder: function (nonce, event, session) {
                this.setPaymentMethodNonce(nonce);
                this.placeOrder();
    
                session.completePayment(ApplePaySession.STATUS_SUCCESS);
            },
    
            /**
             * Save nonce
             */
            setPaymentMethodNonce: function (nonce) {
                this.paymentMethodNonce = nonce;
            },
    
            /**
             * Retrieve the client token
             * @returns null|string
             */
            getClientToken: function () {
                return window.checkoutConfig.payment[this.getCode()].clientToken;
            },
    
            /**
             * Payment request data
             */
            getPaymentRequest: function () {
                return {
                    total: {
                        label: this.getDisplayName(),
                        amount: this.grandTotalAmount
                    }
                };
            },
    
            /**
             * Merchant display name
             */
            getDisplayName: function () {
                return window.checkoutConfig.payment[this.getCode()].merchantName;
            },
    
            /**
             * Get data
             * @returns {Object}
             */
            getData: function () {
                return {
                    'method': this.getCode(),
                    'additional_data': {
                        'payment_method_nonce': this.paymentMethodNonce
                    }
                };
            }
        });
    }
);