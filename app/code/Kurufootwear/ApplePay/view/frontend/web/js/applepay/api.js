/**
 * @category    Kurufootwear
 * @package     Kurufootwear\ApplePay
 * @author      Jim McGowen <jim@kurufootwear.com>
 * @copyright   Copyright (c) 2018 KURU Footwear
 */

define(
    [
        'uiComponent', 
        'mage/translate', 
        'mage/storage',
        'Magento_Checkout/js/model/quote'
    ], 
    function (
        Component, 
        $t, 
        storage,
        quote
    ) {
        'use strict';
    
        return Component.extend({
            defaults: {
                clientToken: null,
                quoteId: 0,
                displayName: null,
                actionSuccess: null,
                subTotalAmount: 0,
                grandTotalAmount: 0,
                taxAmount: 0,
                discountAmount: 0,
                isLoggedIn: false,
                storeCode: "default",
                isActive: false
            },
    
            /**
             * Set & get api token
             */
            setClientToken: function (value) {
                this.clientToken = value;
            },
            getClientToken: function () {
                return this.clientToken;
            },
    
            /**
             * Set and get quote id
             */
            setQuoteId: function (value) {
                this.quoteId = value;
            },
            getQuoteId: function () {
                return this.quoteId;
            },
    
            /**
             * Set and get display name
             */
            setDisplayName: function (value) {
                this.displayName = value;
            },
            getDisplayName: function () {
                return this.displayName;
            },
    
            /**
             * Set and get success redirection url
             */
            setActionSuccess: function (value) {
                this.actionSuccess = value;
            },
            getActionSuccess: function () {
                return this.actionSuccess;
            },
    
            /**
             * Set and get sub total
             */
            setSubTotalAmount: function (value) {
                this.subTotalAmount = parseFloat(value).toFixed(2);
            },
            getSubTotalAmount: function () {
                return parseFloat(this.subTotalAmount);
            },
    
            /**
             * Set and get grand total
             */
            setGrandTotalAmount: function (value) {
                this.grandTotalAmount = parseFloat(value).toFixed(2);
            },
            getGrandTotalAmount: function () {
                return parseFloat(this.grandTotalAmount);
            },
    
            /**
             * Set and get tax
             */
            setTaxAmount: function (value) {
                this.taxAmount = parseFloat(value).toFixed(2);
            },
            getTaxAmount: function () {
                return parseFloat(this.taxAmount);
            },
    
            /**
             * Set and get discount amount
             */
            setDiscountAmount: function (value) {
                this.discountAmount = parseFloat(value).toFixed(2);
            },
            getDiscountAmount: function () {
                return parseFloat(this.discountAmount);
            },
    
            /**
             * Set and get is logged in
             */
            setIsLoggedIn: function (value) {
                this.isLoggedIn = value;
            },
            getIsLoggedIn: function () {
                return this.isLoggedIn;
            },
    
            /**
             * Set and get store code
             */
            setStoreCode: function (value) {
                this.storeCode = value;
            },
            getStoreCode: function () {
                return this.storeCode;
            },
    
            /**
             * Set and get is active
             */
            setIsActive: function(value) {
                this.isActive = value;
            },
            getIsActive: function () {
                return this.isActive;
            },
    
            /**
             * API Urls for logged in or guest user
             */
            getApiUrl: function (uri) {
                if (this.getIsLoggedIn() === true) {
                    return "rest/" + this.getStoreCode() + "/V1/carts/mine/" + uri;
                }
                else {
                    return "rest/" + this.getStoreCode() + "/V1/guest-carts/" + this.getQuoteId() + "/" + uri;
                }
            },
    
            /**
             * Returns the payment request data.
             * 
             * @returns {{total: {label: *, amount: *}, requiredShippingContactFields: string[], requiredBillingContactFields: string[]}}
             */
            getPaymentRequest: function () {
                return {
                    total: {
                        label: this.getDisplayName(),
                        amount: this.getGrandTotalAmount()
                    },
                    requiredShippingContactFields: [
                        'postalAddress',
                        'name',
                        'email',
                        'phone'
                    ],
                    requiredBillingContactFields: [
                        'postalAddress', 
                        'name'
                    ]
                };
            },
    
            /**
             * Returns the line item data for the Apple Pay sheet.
             * 
             * @returns {*[]}
             */
            getLineItems: function() {
                var lineItems = [
                    {
                        type: 'final',
                        label: $t('Subtotal'),
                        amount: this.getSubTotalAmount()
                    }
                ];
    
                if (Math.abs(this.getDiscountAmount()) > 0) {
                    lineItems.push(
                        {
                            type: 'final',
                            label: $t('Discount'),
                            amount: this.getDiscountAmount()
                        }
                    );
                }
    
                if (this.getTaxAmount() > 0) {
                    lineItems.push(
                        {
                            type: 'final',
                            label: $t('Tax'),
                            amount: this.getTaxAmount()
                        }
                    );
                }
    
                lineItems.push(
                    {
                        type: 'final',
                        label: $t('Shipping'),
                        amount: this.shippingMethods[this.shippingMethod].amount
                    }
                );
                
                return lineItems;
            },
    
            /**
             * Retrieve shipping methods based on address
             * 
             * @param event
             * @param session
             */
            onShippingContactSelect: function (event, session) {
                var address = event.shippingContact;
                
                // Retrieve shipping methods
                storage.post(
                    this.getApiUrl("estimate-shipping-methods"),
                    JSON.stringify({
                        address: {
                            city: address.locality,
                            region: address.administrativeArea,
                            country_id: address.countryCode,
                            postcode: address.postalCode,
                            save_in_address_book :0
                        }
                    })
                )
                .done(function (r) {
                    var shippingMethods = [];
                    var method = {};
                    
                    this.shippingMethods = {};
    
                    // Stop if no shipping methods
                    if (r.length === 0) {
                        session.abort();
                        alert($t("There are no shipping methods available for you right now - please try an again or use an alternative payment method."));
                        return false;
                    }
    
                    // Format shipping methods array
                    for (var i = 0; i < r.length; i++) {
                        method = {
                            identifier: r[i].method_code,
                            label: r[i].method_title,
                            detail: r[i].carrier_title ? r[i].carrier_title : "",
                            amount: parseFloat(r[i].amount).toFixed(2)
                        };
                        shippingMethods.push(method);
                        this.shippingMethods[r[i].method_code] = r[i];
                    }
    
                    // Set the shipping method to the first option
                    this.shippingMethod = r[0].method_code;
    
                    // Update shipping info and collect totals
                    var payload = {
                        "addressInformation": {
                            "shipping_address": {
                                "email": "",
                                "telephone": "",
                                "firstname": "",
                                "lastname": "",
                                "street": [],
                                "city": address.locality,
                                "region": address.administrativeArea,
                                "region_id": null,
                                "region_code": null,
                                "country_id": address.countryCode,
                                "postcode": address.postalCode,
                                "same_as_billing": 0,
                                "customer_address_id": 0,
                                "save_in_address_book": 0
                            },
                            "shipping_method_code": this.shippingMethods[this.shippingMethod].method_code,
                            "shipping_carrier_code": this.shippingMethods[this.shippingMethod].carrier_code
                        }
                    };
                    storage.post(
                        this.getApiUrl('shipping-information'),
                        JSON.stringify(payload)
                    )
                    .done(function(response) {
                        quote.setTotals(response.totals);
                        
                        var totals = response.totals;
    
                        if (this.getSubTotalAmount() !== totals['base_subtotal']) {
                            this.setSubTotalAmount(parseFloat(totals['base_subtotal']).toFixed(2));
                        }
    
                        if (this.getGrandTotalAmount() !== totals['base_grand_total']) {
                            this.setGrandTotalAmount(parseFloat(totals['base_grand_total']).toFixed(2));
                        }
                        
                        if (this.getTaxAmount() !== totals['base_tax_amount']) {
                            this.setTaxAmount(parseFloat(totals['base_tax_amount']).toFixed(2));
                        }
    
                        if (this.getDiscountAmount() !== totals['base_discount_amount']) {
                            this.setDiscountAmount(parseFloat(totals['base_discount_amount']).toFixed(2));
                        }
                        
                        var total = {
                            label: this.getDisplayName(),
                            amount: parseFloat(this.getGrandTotalAmount() + this.shippingMethods[this.shippingMethod].amount).toFixed(2)
                        };
                        
                        var lineItems = this.getLineItems();
                        
                        session.completeShippingContactSelection(
                            ApplePaySession.STATUS_SUCCESS,
                            shippingMethods,
                            total,
                            lineItems
                        );
                    }.bind(this))
                    .fail(function (r) {
                        session.abort();
                        console.error("Braintree ApplePay Unable to update shipping information", r);
                        alert($t("We're unable to take payments through Apple Pay at the moment. Please try an alternative payment method."));
                    });
                }.bind(this))
                .fail(function (r) {
                    session.abort();
                    console.error("Braintree ApplePay Unable to find shipping methods for estimate-shipping-methods", r);
                    alert($t("We were unable to find any shipping methods for you. Please try an alternative payment method."));
                });
            },
    
            /**
             * Record which shipping method has been selected and updated totals
             */
            onShippingMethodSelect: function (event, session) {
                var shippingMethod = event.shippingMethod;
                this.shippingMethod = shippingMethod.identifier;
    
                var total = {
                    label: this.getDisplayName(),
                    amount: parseFloat(this.getGrandTotalAmount() + this.shippingMethods[this.shippingMethod].amount).toFixed(2)
                };
    
                var lineItems = this.getLineItems();
                
                session.completeShippingMethodSelection(
                    ApplePaySession.STATUS_SUCCESS,
                    total,
                    lineItems
                );
            },
    
            /**
             * Place the order
             */
            startPlaceOrder: function (nonce, event, session) {
                var shippingContact = event.payment.shippingContact;
                var billingContact = event.payment.billingContact;
                var payload = {
                    "addressInformation": {
                        "shipping_address": {
                            "email": shippingContact.emailAddress,
                            "telephone": shippingContact.phoneNumber,
                            "firstname": shippingContact.givenName,
                            "lastname": shippingContact.familyName,
                            "street": shippingContact.addressLines,
                            "city": shippingContact.locality,
                            "region": shippingContact.administrativeArea,
                            "region_id": null,
                            "region_code": null,
                            "country_id": shippingContact.countryCode,
                            "postcode": shippingContact.postalCode,
                            "same_as_billing": 0,
                            "customer_address_id": 0,
                            "save_in_address_book": 0
                        },
                        "billing_address": {
                            "email": shippingContact.emailAddress,
                            "telephone": '0000000000',
                            "firstname": billingContact.givenName,
                            "lastname": billingContact.familyName,
                            "street": billingContact.addressLines,
                            "city": billingContact.locality,
                            "region": billingContact.administrativeArea,
                            "region_id": null,
                            "region_code": null,
                            "country_id": billingContact.countryCode,
                            "postcode": billingContact.postalCode,
                            "same_as_billing": 0,
                            "customer_address_id": 0,
                            "save_in_address_book": 0
                        },
                        "shipping_method_code": this.shippingMethods[this.shippingMethod].method_code,
                        "shipping_carrier_code": this.shippingMethods[this.shippingMethod].carrier_code
                    }
                };
    
                // Set addresses
                storage.post(
                    this.getApiUrl("shipping-information"),
                    JSON.stringify(payload)
                )
                .done(function () {
                    // Submit payment information
                    storage.post(
                        this.getApiUrl("payment-information"),
                        JSON.stringify(
                            {
                                "email": shippingContact.emailAddress,
                                "paymentMethod": {
                                    "method": "braintree_applepay",
                                    "additional_data": {
                                        "payment_method_nonce": nonce
                                    }
                                }
                            }
                    ))
                    .done(function (r) {
                        document.location = this.getActionSuccess();
                        session.completePayment(ApplePaySession.STATUS_SUCCESS);
                    }.bind(this))
                    .fail(function (r) {
                        session.completePayment(ApplePaySession.STATUS_FAILURE);
                        alert($t("We were unable to take your payment over Apple Pay - please try an again or use an alternative payment method."));
                        console.error("Braintree ApplePay Unable to take payment", r);
                    });
                }.bind(this))
                .fail(function (r) {
                    session.completePayment(ApplePaySession.STATUS_FAILURE);
                    console.error("Braintree ApplePay Unable to set shipping information", r);
                    alert($t("We were unable to take your payment over Apple Pay - please try an again or use an alternative payment method."));
                });
            }
        });
    }
);
