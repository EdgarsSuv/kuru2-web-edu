/**
 * @category    Kurufootwear
 * @package     Kurufootwear\ApplePay
 * @author      Jim McGowen <jim@kurufootwear.com>
 * @copyright   Copyright (c) 2018 KURU Footwear
 */

define(
    [
        'uiComponent', 
        'Magento_Checkout/js/model/payment/renderer-list'
    ],
    function (Component, rendererList) {
        'use strict';

        rendererList.push(
            {
                type: 'braintree_applepay',
                component: 'Kurufootwear_ApplePay/js/applepay/implementations/core-checkout/method-renderer/applepay'
            }
        );

        return Component.extend({});
    }
);