<?php
/**
 * @category    Kurufootwear
 * @package     Kurufootwear\ApplePay
 * @author      Jim McGowen <jim@kurufootwear.com>
 * @copyright   Copyright (c) 2018 KURU Footwear
 */

namespace Kurufootwear\ApplePay\Block\ApplePay\Shortcut;

use Kurufootwear\ApplePay\Block\ApplePay\AbstractButton;
use Magento\Checkout\Model\Session;
use Magento\Catalog\Block\ShortcutInterface;
use Magento\Checkout\Model\DefaultConfigProvider;
use Magento\Framework\View\Element\Template\Context;
use Kurufootwear\ApplePay\Model\ApplePay\Auth;
use Kurufootwear\ApplePay\Model\ApplePay\Config;

/**
 * Class Button
 */
class Button extends AbstractButton implements ShortcutInterface
{
    const ALIAS_ELEMENT_INDEX = 'alias';

    const BUTTON_ELEMENT_INDEX = 'button_id';

    /**
     * @var DefaultConfigProvider
     */
    private $defaultConfigProvider;

    /**
     * Button constructor.
     * @param Context $context
     * @param Session $checkoutSession
     * @param Auth $auth
     * @param DefaultConfigProvider $defaultConfigProvider
     * @param Config $config
     * @param array $data
     */
    public function __construct(
        Context $context,
        Session $checkoutSession,
        Auth $auth,
        DefaultConfigProvider $defaultConfigProvider,
        Config $config,
        array $data = []
    ) {
        parent::__construct($context, $checkoutSession, $auth, $config, $data);
        $this->defaultConfigProvider = $defaultConfigProvider;
    }

    /**
     * @inheritdoc
     */
    public function getAlias()
    {
        return $this->getData(self::ALIAS_ELEMENT_INDEX);
    }

    /**
     * @return string
     */
    public function getContainerId()
    {
        return $this->getData(self::BUTTON_ELEMENT_INDEX);
    }

    /**
     * Current Quote ID for guests
     * @return int
     */
    public function getQuoteId()
    {
        $config = $this->defaultConfigProvider->getConfig();
        if (!empty($config['quoteData']['entity_id'])) {
            return $config['quoteData']['entity_id'];
        }
        
        return 0;
    }
}
