<?php
/**
 * @category    Kurufootwear
 * @package     Kurufootwear\ApplePay
 * @author      Jim McGowen <jim@kurufootwear.com>
 * @copyright   Copyright (c) 2018 KURU Footwear
 */

namespace Kurufootwear\ApplePay\Block\ApplePay;

use Magento\Braintree\Block\Info as BraintreeInfo;

/**
 * Class Info
 */
class Info extends BraintreeInfo
{
}
