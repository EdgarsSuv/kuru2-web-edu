<?php
/**
 * @category    Kurufootwear
 * @package     Kurufootwear\ApplePay
 * @author      Jim McGowen <jim@kurufootwear.com>
 * @copyright   Copyright (c) 2018 KURU Footwear
 */

namespace Kurufootwear\ApplePay\Block\ApplePay;

use Magento\Checkout\Model\Session;
use Magento\Framework\View\Element\Template;
use Magento\Framework\View\Element\Template\Context;
use Kurufootwear\ApplePay\Model\ApplePay\Auth;
use Kurufootwear\ApplePay\Model\ApplePay\Config;

/**
 * Class AbstractButton
 */
abstract class AbstractButton extends Template
{
    /**
     * @var Session
     */
    protected $checkoutSession;

    /**
     * @var Auth
     */
    protected $auth;
    
    /**
     * @var Config 
     */
    protected $config;

    /**
     * Button constructor.
     * @param Context $context
     * @param Session $checkoutSession
     * @param Auth $auth
     * @param Config $config
     * @param array $data
     */
    public function __construct(
        Context $context,
        Session $checkoutSession,
        Auth $auth,
        Config $config,
        array $data = []
    ) {
        parent::__construct($context, $data);
        $this->checkoutSession = $checkoutSession;
        $this->auth = $auth->get();
        $this->config = $config;
    }

    /**
     * @return bool
     */
    public function isActive()
    {
        return (bool)$this->config->getActive();
    }

    /**
     * Merchant name to display in popup
     * @return string
     */
    public function getMerchantName()
    {
        return $this->auth->getDisplayName();
    }

    /**
     * Braintree's API token
     * @return string|null
     */
    public function getClientToken()
    {
        return $this->auth->getClientToken();
    }

    /**
     * URL To success page
     * @return string
     */
    public function getActionSuccess()
    {
        return $this->auth->getActionSuccess();
    }

    /**
     * Is customer logged in flag
     * @return bool
     */
    public function isCustomerLoggedIn()
    {
        return $this->auth->getIsLoggedIn();
    }

    /**
     * Cart grand total
     * @return float
     */
    public function getGrandTotalAmount()
    {
        return $this->checkoutSession->getQuote()->getBaseGrandTotal();
    }

    /**
     * @return float
     */
    public function getStorecode()
    {
        return $this->auth->getStoreCode();
    }
}
