<?php
/**
 * @category    Kurufootwear
 * @package     Kurufootwear\ApplePay
 * @author      Jim McGowen <jim@kurufootwear.com>
 * @copyright   Copyright (c) 2018 KURU Footwear
 */

namespace Kurufootwear\ApplePay\Observer;

use Kurufootwear\ApplePay\Block\ApplePay\Shortcut\Button;
use Magento\Catalog\Block\ShortcutButtons;
use Magento\Framework\Event\Observer;
use Magento\Framework\Event\ObserverInterface;
use Magento\Framework\Exception\LocalizedException;

/**
 * Class AddApplePayShortcuts
 */
class AddApplePayShortcuts implements ObserverInterface
{
    /**
     * Add apple pay shortcut button
     *
     * @param Observer $observer
     * @return void
     * @throws LocalizedException
     */
    public function execute(Observer $observer)
    {
        // Remove button from catalog pages
        if ($observer->getData('is_catalog_product')) {
            return;
        }
    
        /** @var ShortcutButtons $shortcutButtons */
        /** @noinspection PhpUndefinedMethodInspection */
        $shortcutButtons = $observer->getEvent()->getContainer();
        $shortcut = $shortcutButtons->getLayout()->createBlock(Button::class);
    
        /** @noinspection PhpParamsInspection */
        $shortcutButtons->addShortcut($shortcut);
    }
}
