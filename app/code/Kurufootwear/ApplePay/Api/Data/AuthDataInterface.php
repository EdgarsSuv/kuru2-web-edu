<?php
/**
 * @category    Kurufootwear
 * @package     Kurufootwear\ApplePay
 * @author      Jim McGowen <jim@kurufootwear.com>
 * @copyright   Copyright (c) 2018 KURU Footwear
 */

namespace Kurufootwear\ApplePay\Api\Data;

/**
 * Interface AuthDataInterface
 */
interface AuthDataInterface
{
    /**
     * Braintree client token
     * @return string
     */
    public function getClientToken();

    /**
     * Merchant display name
     * @return string
     */
    public function getDisplayName();

    /**
     * URL To success page
     * @return string
     */
    public function getActionSuccess();

    /**
     * @return boolean
     */
    public function getIsLoggedIn();

    /**
     * Get current store code
     * @return string
     */
    public function getStoreCode();

    /**
     * Set Braintree client token
     * @var $value string
     * @return $this
     */
    public function setClientToken($value);

    /**
     * Set Merchant display name
     * @var $value string
     * @return $this
     */
    public function setDisplayName($value);

    /**
     * Set URL To success page
     * @var $value string
     * @return $this
     */
    public function setActionSuccess($value);

    /**
     * Set if user is logged in
     * @var $value boolean
     * @return $this
     */
    public function setIsLoggedIn($value);

    /**
     * Set current store code
     * @var $value string
     * @return $this
     */
    public function setStoreCode($value);
}
