<?php
/**
 * @category    Kurufootwear
 * @package     Kurufootwear\ApplePay
 * @author      Jim McGowen <jim@kurufootwear.com>
 * @copyright   Copyright (c) 2018 KURU Footwear
 */

namespace Kurufootwear\ApplePay\Api;

use Kurufootwear\ApplePay\Api\Data\AuthDataInterface;

/**
 * Interface AuthInterface
 */
interface AuthInterface
{
    /**
     * Returns details required to be able to submit a payment with apple pay.
     * 
     * @return AuthDataInterface
     */
    public function get();
}
