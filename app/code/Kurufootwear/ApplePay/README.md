Kurufootwear_ApplePay Readme
============================

This module adds support for Apple Pay. Concretely, it adds quick checkout buttons to the cart page and the mini-cart.

Configuration is at Stores > Configuration > Sales > Payment Methods > Apple Pay

This implementation is specifically for use with Braintree. Consequently, Braintree configuraiton settings affect this module as well. For example the environment selected in the Braintree settings is applicable to Apple Pay as well and dictate which environment payments will be placed.

See Confluence for credential info:
https://kurufootwear.atlassian.net/wiki/spaces/KURU/pages/744751105/Access+-+Apple+Pay

Braintree - Apple Pay developer documentation:
https://developers.braintreepayments.com/guides/apple-pay/overview

Apple Pay developer documentation:
https://developer.apple.com/apple-pay/implementation/
(The Braintree documentation should take precedence over Apple's documentation as there are some differences in implementation)

