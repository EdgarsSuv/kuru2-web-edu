<?php
/**
 * @category    Kurufootwear
 * @package     Kurufootwear\ApplePay
 * @author      Krisjanis Veinbahs <info@scandiweb.com>
 * @copyright   Copyright (c) 2018 KURU Footwear. All rights reserved.
 */
namespace Kurufootwear\ApplePay\Model;

use Kurufootwear\ApplePay\Model\ApplePay\Config;
use Magento\Checkout\Model\ConfigProviderInterface;

class CheckoutConfigProvider implements ConfigProviderInterface
{
    /**
     * CheckoutConfigProvider constructor.
     *
     * @param ApplePay\Config $applePayConfig
     */
    public function __construct(
        Config $applePayConfig
    ) {
        $this->applePayConfig = $applePayConfig;
    }

    /**
     * {@inheritdoc}
     */
    public function getConfig()
    {
        $config = [];
        $config['isApplepaybuttonActive'] = (bool)$this->applePayConfig->getActive();

        return $config;
    }
}
