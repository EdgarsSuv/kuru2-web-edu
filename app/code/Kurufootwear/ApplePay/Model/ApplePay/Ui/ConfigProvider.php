<?php
/**
 * @category    Kurufootwear
 * @package     Kurufootwear\ApplePay
 * @author      Jim McGowen <jim@kurufootwear.com>
 * @copyright   Copyright (c) 2018 KURU Footwear
 */

namespace Kurufootwear\ApplePay\Model\ApplePay\Ui;

use Kurufootwear\ApplePay\Model\ApplePay\Config;
use Magento\Checkout\Model\ConfigProviderInterface;
use Magento\Braintree\Model\Adapter\BraintreeAdapterFactory;
use Magento\Framework\View\Asset\Repository;
use Magento\Braintree\Gateway\Request\PaymentDataBuilder;

/**
 * Class ConfigProvider
 */
final class ConfigProvider implements ConfigProviderInterface
{
    const METHOD_CODE = 'braintree_applepay';

    /**
     * @var Config
     */
    private $config;

    /**
     * @var BraintreeAdapterFactory
     */
    private $adapterFactory;

    /**
     * @var Repository
     */
    private $assetRepo;

    /**
     * @var string
     */
    private $clientToken = '';

    /**
     * ConfigProvider constructor.
     * @param Config $config
     * @param BraintreeAdapterFactory $adapterFactory
     * @param Repository $assetRepo
     */
    public function __construct(
        Config $config,
        BraintreeAdapterFactory $adapterFactory,
        Repository $assetRepo
    ) {
        $this->config = $config;
        $this->adapterFactory = $adapterFactory;
        $this->assetRepo = $assetRepo;
    }

    /**
     * Retrieve assoc array of checkout configuration
     * 
     * Note: Don't think this is used but it's required by the interface.
     *
     * @return array
     */
    public function getConfig()
    {
        return [
            'payment' => [
                self::METHOD_CODE => [
                    'clientToken' => $this->getClientToken(),
                    'merchantName' => $this->getMerchantName()
                ]
            ]
        ];
    }

    /**
     * Returns the client token from the payment gateway.
     * Generates a new client token if necessary.
     * 
     * @return string
     */
    public function getClientToken()
    {
        if (empty($this->clientToken)) {
            $params = [];
            
            $merchantAccountId = $this->config->getMerchantAccountId();
            if (!empty($merchantAccountId)) {
                $params[PaymentDataBuilder::MERCHANT_ACCOUNT_ID] = $merchantAccountId;
            }
            
            $this->clientToken = $this->adapterFactory->create()->generate($params);
        }
        
        return $this->clientToken;
    }

    /**
     * Get merchant name
     * 
     * @return string
     */
    public function getMerchantName()
    {
        return $this->config->getMerchantName();
    }
}
