<?php
/**
 * @category    Kurufootwear
 * @package     Kurufootwear\ApplePay
 * @author      Jim McGowen <jim@kurufootwear.com>
 * @copyright   Copyright (c) 2018 KURU Footwear
 */

namespace Kurufootwear\ApplePay\Model\ApplePay;

use Kurufootwear\ApplePay\Api\AuthInterface;
use Kurufootwear\ApplePay\Api\Data\AuthDataInterface;
use Kurufootwear\ApplePay\Api\Data\AuthDataInterfaceFactory;
use Kurufootwear\ApplePay\Model\ApplePay\Ui\ConfigProvider;
use Magento\Customer\Model\Session as CustomerSession;
use Magento\Framework\UrlInterface;
use Magento\Store\Model\StoreManagerInterface;

/**
 * Class Auth
 */
class Auth implements AuthInterface
{
    /**
     * @var AuthDataInterfaceFactory
     */
    private $authDataFactory;

    /**
     * @var ConfigProvider
     */
    private $configProvider;

    /**
     * @var UrlInterface
     */
    private $url;

    /**
     * @var CustomerSession
     */
    private $customerSession;

    /**
     * @var StoreManagerInterface
     */
    private $storeManager;

    /**
     * Auth constructor.
     *
     * @param AuthDataInterfaceFactory $authDataFactory
     * @param ConfigProvider $configProvider
     * @param UrlInterface $url
     * @param CustomerSession $customerSession
     * @param StoreManagerInterface $storeManagerInterface
     */
    public function __construct(
        AuthDataInterfaceFactory $authDataFactory,
        ConfigProvider $configProvider,
        UrlInterface $url,
        CustomerSession $customerSession,
        StoreManagerInterface $storeManagerInterface
    ) {
        $this->authDataFactory = $authDataFactory;
        $this->configProvider = $configProvider;
        $this->url = $url;
        $this->customerSession = $customerSession;
        $this->storeManager = $storeManagerInterface;
    }

    /**
     * @inheritdoc
     */
    public function get()
    {
        /** @var $data AuthDataInterface */
        $data = $this->authDataFactory->create()
            ->setClientToken($this->getClientToken())
            ->setDisplayName($this->getDisplayName())
            ->setActionSuccess($this->getActionSuccess())
            ->setIsLoggedIn($this->getIsLoggedIn())
            ->setStoreCode($this->getStoreCode());

        return $data;
    }
    
    /**
     * Returns the client token from the payment gateway.
     * 
     * @return string
     */
    protected function getClientToken()
    {
        return $this->configProvider->getClientToken();
    }
    
    /**
     * Returns the merchant name to display in the Apple Pay sheet.
     * 
     * @return string
     */
    protected function getDisplayName()
    {
        return $this->configProvider->getMerchantName();
    }
    
    /**
     * Returns the url for the order success page.
     * 
     * @return string
     */
    protected function getActionSuccess()
    {
        return $this->url->getUrl('checkout/onepage/success', ['_secure' => true]);
    }
    
    /**
     * Returns true if the user is logged in, false if guest.
     * 
     * @return bool
     */
    protected function getIsLoggedIn()
    {
        return (bool)$this->customerSession->isLoggedIn();
    }
    
    /**
     * Returns the store code.
     * 
     * @return string
     */
    protected function getStoreCode()
    {
        return $this->storeManager->getStore()->getCode();
    }
}
