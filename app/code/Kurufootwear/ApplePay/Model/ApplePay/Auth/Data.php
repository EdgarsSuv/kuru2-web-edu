<?php
/**
 * @category    Kurufootwear
 * @package     Kurufootwear\ApplePay
 * @author      Jim McGowen <jim@kurufootwear.com>
 * @copyright   Copyright (c) 2018 KURU Footwear
 */

namespace Kurufootwear\ApplePay\Model\ApplePay\Auth;

use Kurufootwear\ApplePay\Api\Data\AuthDataInterface;

/**
 * Class Auth
 */
class Data implements AuthDataInterface
{
    /**
     * @var string
     */
    private $clientToken;

    /**
     * @var string
     */
    private $displayName;

    /**
     * @var string
     */
    private $actionSuccess;

    /**
     * @var bool
     */
    private $isLoggedIn;

    /**
     * @var string
     */
    private $storeCode;

    /**
     * @inheritdoc
     */
    public function getClientToken()
    {
        return $this->clientToken;
    }

    /**
     * @inheritdoc
     */
    public function getDisplayName()
    {
        return $this->displayName;
    }

    /**
     * @inheritdoc
     */
    public function getActionSuccess()
    {
        return $this->actionSuccess;
    }

    /**
     * @inheritdoc
     */
    public function getIsLoggedIn()
    {
        return $this->isLoggedIn;
    }

    /**
     * @inheritdoc
     */
    public function getStoreCode()
    {
        return $this->storeCode;
    }

    /**
     * @inheritdoc
     */
    public function setClientToken($value)
    {
        $this->clientToken = $value;
        return $this;
    }

    /**
     * @inheritdoc
     */
    public function setDisplayName($value)
    {
        $this->displayName = $value;
        return $this;
    }

    /**
     * @inheritdoc
     */
    public function setActionSuccess($value)
    {
        $this->actionSuccess = $value;
        return $this;
    }

    /**
     * @inheritdoc
     */
    public function setIsLoggedIn($value)
    {
        $this->isLoggedIn = $value;
        return $this;
    }

    /**
     * @inheritdoc
     */
    public function setStoreCode($value)
    {
        $this->storeCode = $value;
        return $this;
    }
}
