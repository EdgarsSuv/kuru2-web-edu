<?php
/**
 * @category    Kurufootwear
 * @package     Kurufootwear\ApplePay
 * @author      Jim McGowen <jim@kurufootwear.com>
 * @copyright   Copyright (c) 2018 KURU Footwear
 */

namespace Kurufootwear\ApplePay\Model\ApplePay;

use Magento\Payment\Gateway\Config\Config as GatewayConfig;

/**
 * Class Config
 */
class Config extends GatewayConfig
{
    /**
     * Get merchant name to display
     * 
     * @return string
     */
    public function getMerchantName()
    {
        return $this->getValue('merchant_name');
    }
    
    /**
     * Gets Merchant account ID.
     *
     * @return string
     */
    public function getMerchantAccountId()
    {
        return $this->getValue('merchant_account_id');
    }
    
    /**
     * Get whether or not this payment method is active.
     * 
     * @return mixed
     */
    public function getActive()
    {
        return $this->getValue('active');
    }
}
