<?php
/**
 * @category     Kurufootwear
 * @package      Kurufootwear/Mail
 * @author       Jim McGowen <jim*kurufootwear.com>
 * @copyright    Copyright (c) 2018 KURU Footwear. All rights reserved.
 */
namespace Kurufootwear\Mail\Template;

use Magento\Framework\Mail\Template\TransportBuilder as MagentoTransportBuilder;
use Zend_Mime;

class TransportBuilder extends MagentoTransportBuilder
{
    public function addAttachment(
        $body,
        $mimeType    = Zend_Mime::TYPE_OCTETSTREAM,
        $disposition = Zend_Mime::DISPOSITION_ATTACHMENT,
        $encoding    = Zend_Mime::ENCODING_BASE64,
        $filename    = null
    ) {
        $this->message->createAttachment($body, $mimeType, $disposition, $encoding, $filename);
        return $this;
    }
}
