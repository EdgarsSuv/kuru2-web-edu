<?php
/**
 * @category     Kurufootwear
 * @package      Kurufootwear/Mail
 * @author       Jim McGowen <jim*kurufootwear.com>
 * @copyright    Copyright (c) 2018 KURU Footwear. All rights reserved.
 */
use Magento\Framework\Component\ComponentRegistrar;

ComponentRegistrar::register(
    ComponentRegistrar::MODULE,
    'Kurufootwear_Mail',
    __DIR__
);
