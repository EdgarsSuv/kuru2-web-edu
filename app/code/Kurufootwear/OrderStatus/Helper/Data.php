<?php
/**
 * @vendor Kurufootwear
 * @module Kurufootwear_OrderStatus
 * @author Aleksejs Baranovs <info@scandiweb.com>
 * @copyright Copyright (c) 2018 KURU Footwear. All rights reserved.
 */

namespace Kurufootwear\OrderStatus\Helper;

use Magento\Framework\App\Helper\AbstractHelper;
use Magento\Framework\App\Helper\Context;
use Magento\Sales\Model\Order;
use Magento\Sales\Api\OrderRepositoryInterface;
use Magento\Framework\Api\SearchCriteriaBuilder;

/**
 * Class Data
 * @package Kurufootwear\OrderStatus\Helper
 */
class Data extends AbstractHelper
{
    /**
     * @var OrderRepositoryInterface
     */
    private $orderRepository;

    /**
     * @var SearchCriteriaBuilder
     */
    private $searchCriteria;

    /**
     * Data constructor.
     *
     * @param Context $context
     * @param OrderRepositoryInterface $orderRepository
     * @param SearchCriteriaBuilder $searchCriteria
     */
    public function __construct(
        Context $context,
        OrderRepositoryInterface $orderRepository,
        SearchCriteriaBuilder $searchCriteria
    ) {
        parent::__construct($context);

        $this->orderRepository = $orderRepository;
        $this->searchCriteria = $searchCriteria;
    }

    /**
     * Get order information by entered email address & real order increment ID
     *
     * @param $orderIncrementId
     * @param $email
     *
     * @return bool|Order
     */
    public function getOrderInfo($orderIncrementId, $email)
    {
        /** @var Order $order */
        $order = $this->orderRepository->getList(
            $this->searchCriteria->addFilter('increment_id', $orderIncrementId, 'eq')->create()
        )->getItems();

        $order = reset($order);

        if ($order && $order->getEntityId() && $order->getCustomerEmail() == $email) {
            return $order;
        }

        return false;
    }
}
