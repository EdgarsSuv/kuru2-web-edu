<?php
/**
 * @vendor Kurufootwear
 * @module Kurufootwear_OrderStatus
 * @author Aleksejs Baranovs <info@scandiweb.com>
 * @copyright Copyright (c) 2018 KURU Footwear. All rights reserved.
 */

namespace Kurufootwear\OrderStatus\Controller\Index;

use Kurufootwear\OrderStatus\Helper\Data;
use Magento\Framework\App\Action\Action;
use Magento\Framework\App\Action\Context;
use Magento\Framework\Controller\ResultFactory;

/**
 * Class CheckOrderStatus
 * @package Kurufootwear\OrderStatus\Controller\Index
 */
class CheckOrderStatus extends Action
{
    /**
     * @var Data
     */
    private $dataHelper;

    /**
     * CheckOrderStatus constructor.
     *
     * @param Context $context
     * @param Data $dataHelper
     */
    public function __construct(
        Context $context,
        Data $dataHelper
    ) {
        parent::__construct($context);

        $this->dataHelper = $dataHelper;
    }

    public function execute()
    {
        $data = $this->getRequest()->getPostValue();
        $resultFactory = $this->resultFactory->create(ResultFactory::TYPE_JSON);

        if ($data) {
            $postEmail = $this->getRequest()->getPost('email_address');
            $postOrderId = $this->getRequest()->getPost('increment_id');
            $email = !empty($postEmail) ? trim($postEmail) : '';
            $orderId = !empty($postOrderId) ? trim(preg_replace('/[^0-9]*/', '', $postOrderId)) : '';

            if ($this->dataHelper->getOrderInfo($orderId, $email)) {
                return $resultFactory->setData([
                    'success' => __('You will be redirected to order status page, please wait...')
                ]);
            }

            return $resultFactory->setData([
                'error' => __('This Order/Email doesn\'t exist')
            ]);
        }

        return $resultFactory->setData([
            'error' => __('Something went wrong while processing your request.')
        ]);
    }
}