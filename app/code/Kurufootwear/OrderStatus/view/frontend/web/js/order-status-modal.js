/**
 * @vendor Kurufootwear
 * @module Kurufootwear_OrderStatus
 * @author Aleksejs Baranovs <info@scandiweb.com>
 * @copyright Copyright (c) 2018 KURU Footwear. All rights reserved.
 */

define([
    'jquery',
    'ko',
    'Magento_Ui/js/modal/modal',
    'Magento_Customer/js/customer-data'
], function($, ko, modal, customerData) {
    'use strict';

    $.widget('kuru.orderstatus', {
        options: {
            actionUrl: '',
            orderStatusUrl: ''
        },

        classes: {
            orderStatusFormSelector: '.order-status-form',
            orderStatusMessageSelector: '.order-status-message',
            orderStatusLinkSelector: '#order-status-link'
        },

        /**
         * Initialize
         *
         * @private
         */
        _create: function () {
            this.bindActions();
        },

        /**
         * Bind actions
         */
        bindActions: function() {
            if (!this.checkForLastOrderExistence()) {
                this.createOrderStatusPopup();
            }

            this.subscribeLastOrderedItemsChange();
        },

        /**
         * Check for last order existence for particular customer
         *
         * @returns {boolean}
         */
        checkForLastOrderExistence: function () {
            var self = this,
                $orderStatusLinkSelector = $(self.classes.orderStatusLinkSelector),
                lastOrder = customerData.get('last-ordered-items')().order;

            if (lastOrder && lastOrder !== null) {
                $orderStatusLinkSelector.attr(
                    'href',
                    self.options.orderStatusUrl + 'order_id/' + lastOrder
                );
                $orderStatusLinkSelector.attr('class', 'order-status-trigger-customer');
                $orderStatusLinkSelector.unbind('click');
            } else {
                $orderStatusLinkSelector.attr('href', '#');
                $orderStatusLinkSelector.attr('class', 'order-status-trigger');
                $orderStatusLinkSelector.bind('click');

                return false;
            }
        },

        /**
         * Subscribe to customer data change
         */
        subscribeLastOrderedItemsChange: function () {
            var self = this;

            customerData.get('last-ordered-items').subscribe(function () {
                self.checkForLastOrderExistence();
            });
        },

        /**
         * Create order status popup
         */
        createOrderStatusPopup: function () {
            var self = this;

            modal({
                type: 'popup',
                responsive: true,
                innerScroll: true,
                trigger: '.order-status-trigger',
                buttons: [{
                    text: $.mage.__('Check order status'),
                    class: 'action submit primary',
                    click: function (event) {
                        event.preventDefault();
                        self.postOrderInfoRequest();
                    }
                }]
            }, this.element);
        },

        /**
         * Post order info request
         */
        postOrderInfoRequest: function () {
            var self = this,
                $form = $(self.classes.orderStatusFormSelector);

            if (!$form.valid()) {
                return;
            }

            // Clean messages
            $(self.classes.orderStatusMessageSelector).empty();
            $(self.classes.orderStatusMessageSelector).removeClass('error success');

            self.element.trigger('processStart');
            $.ajax({
                url: self.options.actionUrl,
                data: $form.serialize(),
                type: 'POST',
                dataType: 'json',
                success: function (data) {
                    if (data.error) {
                        $(self.classes.orderStatusMessageSelector).addClass('error');
                        $(self.classes.orderStatusMessageSelector).text(data.error);
                    } else if (data.success) {
                        $(self.classes.orderStatusMessageSelector).addClass('success');
                        $(self.classes.orderStatusMessageSelector).text(data.success);
                        $form.submit();
                    }
                    self.element.trigger('processStop');
                }
            });
        }
    });

    return $.kuru.orderstatus;
});
