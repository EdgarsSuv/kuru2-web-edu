<?php
/**
 * @vendor Kurufootwear
 * @module Kurufootwear_OrderStatus
 * @author Aleksejs Baranovs <info@scandiweb.com>
 * @copyright Copyright (c) 2018 KURU Footwear. All rights reserved.
 */

namespace Kurufootwear\OrderStatus\Block\GuestOrderStatus;

use Kurufootwear\OrderStatus\Helper\Data as OrderStatusHelper;
use Magento\Framework\EscapeHelper;
use Magento\Framework\View\Element\Template;
use Magento\Framework\View\Element\Template\Context;
use Magento\Sales\Model\Order;
use Magento\Sales\Model\Order\Address\Renderer as AddressRenderer;
use Magento\Sales\Model\Order\Address;
use Magento\Payment\Helper\Data as PaymentHelper;
use Magento\Sales\Model\Order\Item;

/**
 * Class View
 * @package Kurufootwear\OrderStatus\Block\GuestOrderStatus
 */
class View extends Template
{
    /**
     * @var OrderStatusHelper
     */
    private $orderStatusHelper;

    /**
     * @var AddressRenderer
     */
    private $addressRenderer;

    /**
     * @var PaymentHelper
     */
    private $paymentHelper;

    /**
     * @var EscapeHelper
     */
    private $escapeHelper;

    /**
     * View constructor.
     *
     * @param Context $context
     * @param OrderStatusHelper $orderStatusHelper
     * @param AddressRenderer $addressRenderer
     * @param PaymentHelper $paymentHelper
     * @param EscapeHelper $escapeHelper
     * @param array $data
     */
    public function __construct(
        Context $context,
        OrderStatusHelper $orderStatusHelper,
        AddressRenderer $addressRenderer,
        PaymentHelper $paymentHelper,
        EscapeHelper $escapeHelper,
        array $data = []
    ) {
        parent::__construct($context, $data);

        $this->orderStatusHelper = $orderStatusHelper;
        $this->addressRenderer = $addressRenderer;
        $this->paymentHelper = $paymentHelper;
        $this->escapeHelper = $escapeHelper;
    }

    /**
     * Returns order increment ID
     *
     * @return string
     */
    public function getOrderIncrementId()
    {
        $orderIncrementId = $this->getRequest()->getParam('increment_id');

        return !empty($orderIncrementId) ? trim(preg_replace('/[^0-9]*/', '', $orderIncrementId)) : '';
    }

    /**
     * Gets email
     *
     * @return string
     */
    public function getEmail()
    {
        $email = $this->getRequest()->getParam('email_address');

        return !empty($email) ? trim($email) : '';
    }

    /**
     * Escape html attr
     *
     * @param string $string
     *
     * @return string
     */
    public function escapeHtmlAttr(string $string) {
        return $this->escapeHelper->escapeHtmlAttr($string);
    }

    /**
     * Get order information by entered email address & real order increment ID
     *
     * @param $orderIncrementId
     * @param $email
     *
     * @return bool|Order
     */
    public function getOrderInfo($orderIncrementId, $email)
    {
        if ($order = $this->orderStatusHelper->getOrderInfo($orderIncrementId, $email)) {
            return $order;
        }

        return false;
    }

    /**
     * Returns string with formatted address
     *
     * @param Address $address
     *
     * @return null|string
     */
    public function getFormattedAddress(Address $address)
    {
        return $this->addressRenderer->format($address, 'html');
    }

    /**
     * Get payment info html
     *
     * @param Order $order
     *
     * @return string
     */
    public function getPaymentInfoHtml(Order $order)
    {
        return $this->paymentHelper->getInfoBlockHtml($order->getPayment(), $this->_storeManager->getStore()->getId());
    }

    /**
     * Get product size information
     *
     * @param $item
     *
     * @return bool|array
     */
    public function getSizeInformation(Item $item)
    {
        if ($attributesInfo = $item->getProductOptionByCode('attributes_info')) {
            foreach ($attributesInfo as $key => $value) {
                if (stripos($value['label'], 'size') !== false) {
                    return $attributesInfo[$key];
                }
            }
        }

        return false;
    }
}