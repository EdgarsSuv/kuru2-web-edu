<?php
/**
 * @vendor Kurufootwear
 * @module Kurufootwear_OrderStatus
 * @author Aleksejs Baranovs <info@scandiweb.com>
 * @copyright Copyright (c) 2018 KURU Footwear. All rights reserved.
 */

namespace Kurufootwear\OrderStatus\Block;

use Magento\Framework\View\Element\Template;

/**
 * Class OrderStatusModal
 * @package Kurufootwear\OrderStatus\Block
 */
class OrderStatusModal extends Template
{
    /**
     * Get modal action URL to check order existence
     *
     * @return string
     */
    public function getCheckActionUrl()
    {
        return $this->_urlBuilder->getUrl('order_status/index/checkOrderStatus');
    }

    /**
     * Get modal action URL to redirect after successful form submit
     *
     * @return string
     */
    public function getFormActionUrl()
    {
        return $this->_urlBuilder->getDirectUrl('customer-service/order-status.html');
    }

    /**
     * Get registration / login URL
     *
     * @return string
     */
    public function getRegistrationUrl()
    {
        return $this->_urlBuilder->getUrl('customer/account/login');
    }

    /**
     * Get order view URL
     *
     * @return string
     */
    public function getOrderStatusUrl()
    {
        return $this->_urlBuilder->getUrl('sales/order/view');
    }
}