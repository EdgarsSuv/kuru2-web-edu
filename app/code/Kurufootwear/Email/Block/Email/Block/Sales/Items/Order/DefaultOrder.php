<?php
/**
 * @vendor Kurufootwear
 * @module Kurufootwear_Email
 * @author Aleksejs Baranovs <info@scandiweb.com>
 * @copyright Copyright (c) 2018 KURU Footwear. All rights reserved.
 */

namespace Kurufootwear\Email\Block\Email\Block\Sales\Items\Order;

use Kurufootwear\Sales\Model\Order\Item;
use Magento\Catalog\Block\Product\ImageBuilder;
use Magento\Framework\Exception\NoSuchEntityException;
use Magento\Framework\Pricing\Helper\Data;
use Magento\Framework\View\Element\Template\Context;
use Magento\GiftMessage\Helper\Message;
use Magento\GiftMessage\Model\Message as GiftMessageModel;
use Magetrend\Email\Block\Email\Block\Sales\Items\Order\DefaultOrder as BaseDefaultOrder;
use Magento\Catalog\Api\ProductRepositoryInterface;

/**
 * Class DefaultOrder
 * @package Kurufootwear\Email\Block\Email\Block\Sales\Items\Order
 */
class DefaultOrder extends BaseDefaultOrder
{
    /**
     * @var Data
     */
    private $priceHelper;

    /**
     * @var ProductRepositoryInterface
     */
    private $productRepository;

    /**
     * @var Message
     */
    private $giftMessageHelper;

    /**
     * DefaultOrder constructor.
     *
     * @param Context $context
     * @param ImageBuilder $imageBuilder
     * @param Data $priceHelper
     * @param ProductRepositoryInterface $productRepository
     * @param Message $giftMessageHelper
     * @param array $data
     */
    public function __construct(
        Context $context,
        ImageBuilder $imageBuilder,
        Data $priceHelper,
        ProductRepositoryInterface $productRepository,
        Message $giftMessageHelper,
        array $data = []
    ) {
        parent::__construct($context, $imageBuilder, $data);

        $this->priceHelper = $priceHelper;
        $this->productRepository = $productRepository;
        $this->giftMessageHelper = $giftMessageHelper;
    }

    /**
     * Get item row price
     *
     * @param Item $item
     *
     * @return string
     */
    public function getItemRowPrice(Item $item)
    {
        return $this->priceHelper->currency($item->getRowTotal() / $item->getQtyOrdered());
    }

    /**
     * Get item image
     *
     * Override for configurable products to get the images from child products not parents
     * For simple products image will be taken from the same product
     *
     * @param $item
     *
     * @return string
     */
    public function getItemImage($item)
    {
        // Try to load the real product using item SKU (if configurable then child will be loaded, if simple then simple will be loaded)
        try {
            $product = $this->productRepository->get($item->getSku());
        } catch (NoSuchEntityException $e) {
            $this->_logger->error(sprintf('Product with sku %s does not exist, tried to load in order success email'), $item->getSku());
            $product = null;
        }

        if (!$product) {
            return $this->getProductImagePlaceholder();
        }

        return $this->imageBuilder->setProduct($product)
            ->setImageId('sendfriend_small_image')
            ->create()
            ->getImageUrl();
    }

    /**
     * Get item gift message
     *
     * @return bool|GiftMessageModel
     */
    public function getItemGiftMessage()
    {
        if (!$this->getItem()->getGiftMessageId()) {
            return false;
        }

        return $this->giftMessageHelper->getGiftMessage($this->getItem()->getGiftMessageId());
    }
}
