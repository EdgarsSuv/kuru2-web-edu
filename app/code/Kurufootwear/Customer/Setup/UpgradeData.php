<?php
/**
 * @vendor Kurufootwear
 * @module Kurufootwear_Customer
 * @author Aleksejs Baranovs <info@scandiweb.com>
 * @copyright Copyright (c) 2018 KURU Footwear. All rights reserved.
 */

namespace Kurufootwear\Customer\Setup;

use Magento\Eav\Setup\EavSetup;
use Magento\Eav\Setup\EavSetupFactory;
use Magento\Framework\Exception\LocalizedException;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\ModuleDataSetupInterface;
use Magento\Eav\Model\Config;
use Magento\Customer\Model\Customer;
use Magento\Framework\Setup\UpgradeDataInterface;

/**
 * Class UpgradeData
 * @package Kurufootwear\Customer\Setup
 */
class UpgradeData implements UpgradeDataInterface
{
    /**
     * @var EavSetupFactory
     */
    private $eavSetupFactory;

    /**
     * UpgradeData constructor.
     *
     * @param EavSetupFactory $eavSetupFactory
     * @param Config $eavConfig
     */
    public function __construct(
        EavSetupFactory $eavSetupFactory,
        Config $eavConfig
    ) {
        $this->eavSetupFactory = $eavSetupFactory;
        $this->eavConfig = $eavConfig;
    }

    /**
     * Upgrade data
     *
     * @param ModuleDataSetupInterface $setup
     * @param ModuleContextInterface $context
     *
     * @throws LocalizedException
     * @throws \Exception
     */
    public function upgrade(
        ModuleDataSetupInterface $setup,
        ModuleContextInterface $context
    ) {
        $setup->startSetup();

        if (version_compare($context->getVersion(), '0.1.1') < 0) {
            $this->addCustomerNotesAttribute($setup);
        }

        $setup->endSetup();
    }

    /**
     * Add customer notes attribute
     *
     * @param $setup
     *
     * @throws \Exception
     * @throws LocalizedException
     */
    private function addCustomerNotesAttribute($setup)
    {
        /** @var EavSetup $eavSetup */
        $eavSetup = $this->eavSetupFactory->create(['setup' => $setup]);
        $eavSetup->addAttribute(
            Customer::ENTITY,
            'customer_notes',
            [
                'type' => 'varchar',
                'label' => 'Customer Notes',
                'input' => 'text',
                'required' => false,
                'visible' => false,
                'system' => 0,
            ]
        );

        $customerNotes = $this->eavConfig->getAttribute(Customer::ENTITY, 'customer_notes');

        $customerNotes->setData(
            'used_in_forms',
            ['adminhtml_customer']
        );

        $customerNotes->getResource()->save($customerNotes);
    }
}
