<?php
/**
 * @vendor Kurufootwear
 * @module Kurufootwear_Customer
 * @author Aleksejs Baranovs <info@scandiweb.com>
 * @copyright Copyright (c) 2018 KURU Footwear. All rights reserved.
 */

namespace Kurufootwear\Customer\Block\Adminhtml\Edit\Tab\View;

use Magento\Customer\Block\Adminhtml\Edit\Tab\View\PersonalInfo;

/**
 * Class CustomerNotes
 * @package Kurufootwear\Customer\Block\Adminhtml\Edit\Tab\View
 */
class CustomerNotes extends PersonalInfo
{
    /**
     * Get customer notes
     *
     * @return bool|string
     */
    public function getCustomerNotes()
    {
        $customerNotes = $this->getCustomer()->getCustomAttribute('customer_notes');

        if (isset($customerNotes)) {
            return $customerNotes->getValue();
        }

        return false;
    }
}
