<?php
/**
 * @category    Kurufootwear
 * @package     Kurufootwear/Customer
 * @copyright   Copyright (c) 2017 Scandiweb, Inc (https://scandiweb.com)
 * @license     http://opensource.org/licenses/OSL-3.0 The Open Software License 3.0 (OSL-3.0)
 */
namespace Kurufootwear\Customer\Plugin\CustomerData;

use Magento\Customer\CustomerData\Customer;
use Magento\Customer\Helper\Session\CurrentCustomer;

class CustomerPlugin
{
    /**
     * @var CurrentCustomer
     */
    private $currentCustomerHelper;

    /**
     * CustomerPlugin constructor.
     *
     * @param CurrentCustomer $currentCustomerHelper
     */
    public function __construct(
        CurrentCustomer $currentCustomerHelper
    ){
        $this->currentCustomerHelper = $currentCustomerHelper;
    }

    /**
     * Adds additional customer information to customer section
     *
     * @param Customer $customer
     * @param $result
     *
     * @return array
     */
    public function afterGetSectionData(Customer $customer, $result)
    {
        if (!$this->currentCustomerHelper->getCustomerId()) {
            return $result;
        }

        $currentCustomer = $this->currentCustomerHelper->getCustomer();

        $additional = [
            'lastname' => $currentCustomer->getLastname(),
            'email' => $currentCustomer->getEmail(),
        ];

        return array_merge($result, $additional);
    }
}
