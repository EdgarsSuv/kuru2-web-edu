<?php
/**
 * @vendor Kurufootwear
 * @module Kurufootwear_Customr
 * @author Krisjanis Veinbahs <info@scandiweb.com>
 * @copyright Copyright (c) 2018 KURU Footwear. All rights reserved.
 */

namespace Kurufootwear\Customer\Plugin;

use Magento\Customer\Api\AccountManagementInterface;
use Magento\Customer\Api\Data\CustomerInterface;
use Magento\Framework\Exception\LocalizedException;

/**
 * Class AccountManagementInterfacePlugin
 *
 * Plug customer creation and throw exception
 * if customer name includes forbidden characters
 *
 * @package Kurufootwear\Customer\Plugin
 */
class AccountManagementInterfacePlugin {
    /**
     * Characters that shouldn't appear in a customer name
     */
    const FORBIDDEN_SUBSTRINGS = [
        ":", "/", "\\", "?", "#", "@", ",", ".",
        "http", "www", "ssh", "ftp",
    ];

    /**
     * Wrap customer registration w/ additional validations
     *
     * @param AccountManagementInterface $subject
     * @param callable $proceed
     * @param CustomerInterface $customer
     * @param $password
     * @param $redirectUrl
     * @throws LocalizedException
     * @return null
     */
    public function aroundCreateAccount(
        AccountManagementInterface $subject,
        callable $proceed,
        CustomerInterface $customer,
        $password = null,
        $redirectUrl = ''
    ){
        $this->validateName($customer);

        return $proceed($customer, $password, $redirectUrl);
    }

    /**
     * Wrap customer registration w/ additional validations
     *
     * @param AccountManagementInterface $subject
     * @param callable $proceed
     * @param CustomerInterface $customer
     * @param $hash
     * @param $redirectUrl
     * @throws LocalizedException
     * @return null
     */
    public function aroundCreateAccountWithPasswordHash(
        AccountManagementInterface $subject,
        callable $proceed,
        CustomerInterface $customer,
        $hash,
        $redirectUrl = ''
    ) {
        $this->validateName($customer);

        return $proceed($customer, $hash, $redirectUrl);
    }

    /**
     * Detect web link in customer name
     *
     * @param CustomerInterface $customer
     * @throws LocalizedException
     */
    public function validateName(CustomerInterface $customer)
    {
       if ($this->hasForbiddenCharacters($customer->getFirstname()) ||
           $this->hasForbiddenCharacters($customer->getLastname()) ||
           $this->hasForbiddenCharacters($customer->getMiddlename())) {
            throw new LocalizedException(__("Forbidden characters detected in name."));
       }
    }

    /**
     * Check whether string contains forbidden substrings/characters
     *
     * @param $text
     *
     * @return bool
     */
    private function hasForbiddenCharacters($text)
    {
        if (is_null($text)) {
            return false;
        } else {
            $text = strtolower($text);
        }

        foreach (self::FORBIDDEN_SUBSTRINGS as $forbiddenSubstring) {
            if (strpos($text, strtolower($forbiddenSubstring)) !== false) {
                return true;
            }
        }

        return false;
    }
}