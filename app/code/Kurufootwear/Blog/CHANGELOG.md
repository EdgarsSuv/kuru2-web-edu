Kurufootwear_Blog changelog
=============================

0.1.3:
- Added banner related columns to aw_blog_post table

0.1.2:
- Added thumbnail column to aw_blog_post table
