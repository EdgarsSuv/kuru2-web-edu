<?php
/**
 * @category  Kurufootwear
 * @package   Kurufootwear\Blog
 * @author    Krisjanis Veinbahs <info@scandiweb.com>
 * @copyright Copyright (c) 2017 Scandiweb, Inc (http://scandiweb.com)
 * @license   http://opensource.org/licenses/OSL-3.0 The Open Software License 3.0 (OSL-3.0)
 */

namespace Kurufootwear\Blog\Setup;

use Kurufootwear\Blog\Setup\Setup as KurufootwearSetup;
use Magento\Framework\Setup\UpgradeSchemaInterface;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\SchemaSetupInterface;
use Magento\Framework\DB\Ddl\Table;

class UpgradeSchema extends KurufootwearSetup implements UpgradeSchemaInterface
{
    /**
     * @param SchemaSetupInterface $setup
     * @param ModuleContextInterface $context
     */
    public function upgrade(

        SchemaSetupInterface $setup,
        ModuleContextInterface $context
    )
    {
        $this->_content = $context;
        $setup->startSetup();

        if ($this->getVersion('0.1.2')) {
            $this->addThumbnailColumn($setup);
        }

        if ($this->getVersion('0.1.3')) {
            $this->addBannerColumns($setup);
        }

        $setup->endSetup();
    }

    /**
     * Adds thumbnail column to aheadworks blog post table
     *
     * @param $setup
     */
    protected function addThumbnailColumn($setup)
    {
        // Get module table
        $tableName = $setup->getTable('aw_blog_post');

        // Check if the table already exists
        if ($setup->getConnection()->isTableExists($tableName)) {
            $connection = $setup->getConnection();
            $connection->addColumn(
                $tableName,
                'thumbnail',
                [
                    'type' => Table::TYPE_TEXT,
                    'length' => '2M',
                    'nullable' => true,
                    'comment' => 'Blog post thumbnail'
                ]
            );
        }
    }

    /**
     * Adds thumbnail column to aheadworks blog post table
     *
     * @param $setup
     */
    protected function addBannerColumns($setup)
    {
        // Get module table
        $tableName = $setup->getTable('aw_blog_post');

        // Check if the table already exists
        if ($setup->getConnection()->isTableExists($tableName)) {
            $connection = $setup->getConnection();
            $connection->addColumn(
                $tableName,
                'visible_in_banner',
                [
                    'type' => Table::TYPE_BOOLEAN,
                    'nullable' => true,
                    'comment' => 'Post visible in banner'
                ]
            );
            $connection->addColumn(
                $tableName,
                'white_title',
                [
                    'type' => Table::TYPE_TEXT,
                    'length' => '2M',
                    'nullable' => true,
                    'comment' => 'Banner text is white'
                ]
            );
            $connection->addColumn(
                $tableName,
                'sort_order',
                [
                    'type' => Table::TYPE_INTEGER,
                    'nullable' => true,
                    'comment' => 'Banner sort order'
                ]
            );
        }
    }
}
