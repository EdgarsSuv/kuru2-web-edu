<?php
/**
 * @category  Kurufootwear
 * @package   Kurufootwear\Blog
 * @author    Krisjanis Veinbahs <info@scandiweb.com>
 * @copyright Copyright (c) 2017 Scandiweb, Inc (http://scandiweb.com)
 * @license   http://opensource.org/licenses/OSL-3.0 The Open Software License 3.0 (OSL-3.0)
 */

namespace Kurufootwear\Blog\Setup;

use Magento\Framework\Setup\ModuleContextInterface;

abstract class Setup
{
    /**
     * @var ModuleContextInterface
     */
    protected $_content;

    /**
     * @param $version
     * @return bool
     */
    public function getVersion($version)
    {
        if (version_compare($this->_content->getVersion(), $version, '<')) {
            $this->registerVersion($version);

            return true;
        }

        return false;
    }

    /**
     * @param $version
     */
    protected function registerVersion($version)
    {
        $this->printLine("\033[35mKurufootwear Blog migration version " . $version . " \033[0m");
    }

    /**
     * @param $message
     */
    protected function printLine($message)
    {
        echo PHP_EOL . $message . PHP_EOL;
    }
}
