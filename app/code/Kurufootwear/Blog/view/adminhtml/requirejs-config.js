/**
 * @vendor Kurufootwear
 * @module Kurufootwear_Blog
 * @author Krisjanis Veinbahs <krisjanisv@scandiweb.com>
 * @copyright Copyright (c) 2017 Scandiweb. All rights reserved.
 * @license http://opensource.org/licenses/afl-3.0.php Academic Free License (AFL 3.0)
 */
var config = {
    map: {
        '*': {
            'Magento_Ui/js/form/element/file-uploader': 'Kurufootwear_Blog/js/form/element/file-uploader'
        }
    }
};
