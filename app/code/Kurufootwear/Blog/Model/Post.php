<?php
/**
 * @vendor Kurufootwear
 * @module Kurufootwear_Blog
 * @author Krisjanis Veinbahs <krisjanisv@scandiweb.com>
 * @copyright Copyright (c) 2017 Scandiweb, Ltd (http://scandiweb.com)
 * @license http://opensource.org/licenses/afl-3.0.php Academic Free License (AFL 3.0)
 */

namespace Kurufootwear\Blog\Model;

use Aheadworks\Blog\Model\Converter\Condition as ConditionConverter;
use Aheadworks\Blog\Model\Post as AheadworksPost;
use Aheadworks\Blog\Model\ResourceModel\Validator\UrlKeyIsUnique;
use Aheadworks\Blog\Model\Rule\ProductFactory;
use Magento\Framework\Data\Collection\AbstractDb;
use Magento\Framework\Model\Context;
use Magento\Framework\Model\ResourceModel\AbstractResource;
use Magento\Framework\Registry;
use Aheadworks\Blog\Api\CategoryRepositoryInterface;
use Magento\Framework\App\Filesystem\DirectoryList;
use Aheadworks\Blog\Model\Url as AheadworksUrl;

/**
 * Class Post
 * @package Kurufootwear\Blog\Model
 */
class Post extends AheadworksPost
{
    /**
     * Thumbnail column name
     */
    const THUMBNAIL = 'thumbnail';

    /**
     * Post visible in banner column name
     */
    const VISIBLE_IN_BANNER = 'visible_in_banner';

    /**
     * Is banner title white column name
     */
    const WHITE_TITLE = 'white_title';

    /**
     * Banner sort order column name
     */
    const SORT_ORDER = 'sort_order';

    /**
     * @var CategoryRepositoryInterface
     */
    protected $categoryRepository;

    /**
     * @var AheadworksUrl
     */
    protected $aheadworksUrl;

    /**
     * Post constructor.
     *
     * @param CategoryRepositoryInterface $categoryRepository
     * @param AheadworksUrl $aheadworksUrl
     * @param Context $context
     * @param Registry $registry
     * @param ProductFactory $productRuleFactory
     * @param ConditionConverter $conditionConverter
     * @param UrlKeyIsUnique $urlKeyIsUnique
     * @param null $resource
     * @param null $resourceCollection
     * @param array $data
     */
    public function __construct(
        CategoryRepositoryInterface $categoryRepository,
        AheadworksUrl $aheadworksUrl,
        Context $context,
        Registry $registry,
        ProductFactory $productRuleFactory,
        ConditionConverter $conditionConverter,
        UrlKeyIsUnique $urlKeyIsUnique,
        $resource = null,
        $resourceCollection = null,
        array $data = []
    )
    {
        $this->categoryRepository = $categoryRepository;
        $this->aheadworksUrl = $aheadworksUrl;

        parent::__construct(
            $context,
            $registry,
            $productRuleFactory,
            $conditionConverter,
            $urlKeyIsUnique,
            $resource,
            $resourceCollection,
            $data
        );
    }

    /**
     * {@inheritdoc}
     */
    public function getThumbnail()
    {
        return $this->getData(self::THUMBNAIL);
    }

    /**
     * {@inheritdoc}
     */
    public function setThumbnail($thumbnail)
    {
        return $this->setData(self::THUMBNAIL, $thumbnail);
    }


    /**
     * Retrieves visible in banner from the data array if set, or null otherwise.
     *
     * @return mixed|null
     */
    public function getVisibleInBanner()
    {
        return $this->getData(self::VISIBLE_IN_BANNER);
    }

    /**
     * Set value visible in banner
     *
     * @param mixed $value
     * @return Post $this
     */
    public function setVisibleInBanner($visibleInBanner)
    {
        return $this->setData(self::VISIBLE_IN_BANNER, $visibleInBanner);
    }

    /**
     * Retrieves white title from the data array if set, or null otherwise.
     *
     * @return mixed|null
     */
    public function getWhiteTitle()
    {
        return $this->getData(self::WHITE_TITLE);
    }

    /**
     * Set value for white title
     *
     * @param mixed $value
     * @return Post $this
     */
    public function setWhiteTitle($whiteTitle)
    {
        return $this->setData(self::WHITE_TITLE, $whiteTitle);
    }

    /**
     * Retrieves sort order from the data array if set, or null otherwise.
     *
     * @return mixed|null
     */
    public function getSortOrder()
    {
        return $this->getData(self::SORT_ORDER);
    }

    /**
     * Set value for sort order
     *
     * @param mixed $value
     * @return Post $this
     */
    public function setSortOrder($sortOrder)
    {
        return $this->setData(self::SORT_ORDER, $sortOrder);
    }

    /**
     * Retrieves post url
     *
     * @return string
     */
    public function getUrl()
    {
        $blogUrl = $this->aheadworksUrl->getBlogHomeUrl();
        $postUrl = $this->getUrlKey();

        return $blogUrl . $postUrl;
    }

    /**
     * Retrieve category by id
     *
     * @param $categoryId
     * @return \Aheadworks\Blog\Api\Data\CategoryInterface
     */
    private function getCategoryById($categoryId)
    {
        return $this->categoryRepository->get($categoryId);
    }

    /**
     * Retrieve post categories
     *
     * @return \Aheadworks\Blog\Api\Data\CategoryInterface
     */
    public function getPublicCategories()
    {
        $categoryIds = $this->getCategoryIds();
        $categories = array_map(array($this, 'getCategoryById'), $categoryIds);

        return array_filter($categories, function($category) {
            return $category->getStatus();
        });
    }
}
