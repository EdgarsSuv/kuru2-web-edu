<?php
/**
 * @vendor Kurufootwear
 * @module Kurufootwear_Blog
 * @author Krisjanis Veinbahs <krisjanisv@scandiweb.com>
 * @copyright Copyright (c) 2017 Scandiweb, Ltd (http://scandiweb.com)
 * @license http://opensource.org/licenses/afl-3.0.php Academic Free License (AFL 3.0)
 */

namespace Kurufootwear\Blog\Model;

use Aheadworks\Blog\Model\Category as AheadworksCategory;
use Aheadworks\Blog\Model\Url as AheadworksUrl;
use Aheadworks\Blog\Model\ResourceModel\Validator\UrlKeyIsUnique;
use Magento\Framework\Model\Context;
use Magento\Framework\Registry;

class Category extends AheadworksCategory
{
    /**
     * @var AheadworksUrl
     */
    protected $aheadworksUrl;

    /**
     * Category constructor.
     * @param AheadworksUrl $aheadworksUrl
     * @param Context $context
     * @param Registry $registry
     * @param UrlKeyIsUnique $urlKeyIsUnique
     * @param null $resource
     * @param null $resourceCollection
     * @param array $data
     */
    public function __construct(
        AheadworksUrl $aheadworksUrl,
        Context $context,
        Registry $registry,
        UrlKeyIsUnique $urlKeyIsUnique,
        $resource = null,
        $resourceCollection = null,
        array $data = []
    )
    {
        $this->aheadworksUrl = $aheadworksUrl;

        parent::__construct(
            $context,
            $registry,
            $urlKeyIsUnique,
            $resource,
            $resourceCollection,
            $data
        );
    }

    /**
     * Get full category url
     *
     * @return string
     */
    public function getUrl()
    {
        $blogUrl = $this->aheadworksUrl->getBlogHomeUrl();
        $categoryUrl = $this->getUrlKey();

        return $blogUrl . $categoryUrl;
    }
}