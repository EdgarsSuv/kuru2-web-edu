<?php
/**
 * @vendor Kurufootwear
 * @module Kurufootwear_Blog
 * @author Krisjanis Veinbahs <krisjanisv@scandiweb.com>
 * @copyright Copyright (c) 2017 Scandiweb, Ltd (http://scandiweb.com)
 * @license http://opensource.org/licenses/afl-3.0.php Academic Free License (AFL 3.0)
 */

namespace Kurufootwear\Blog\Model\Data;

use Aheadworks\Blog\Model\Data\Post as AheadworksPost;
use Aheadworks\Blog\Api\CategoryRepositoryInterface;
use Magento\Framework\Api\AttributeValueFactory;
use Magento\Framework\Api\ExtensionAttributesFactory;

/**
 * Class Post
 * @package Kurufootwear\Blog\Model
 */
class Post extends AheadworksPost
{
    /**
     * Thumbnail column name
     */
    const THUMBNAIL = 'thumbnail';

    /**
     * Post visible in banner column name
     */
    const VISIBLE_IN_BANNER = 'visible_in_banner';

    /**
     * Is banner title white column name
     */
    const WHITE_TITLE = 'white_title';

    /**
     * Banner sort order column name
     */
    const SORT_ORDER = 'sort_order';

    /**
     * @var CategoryRepositoryInterface
     */
    protected $categoryRepository;

    /**
     * Post constructor.
     * @param CategoryRepositoryInterface $categoryRepository
     * @param ExtensionAttributesFactory $extensionFactory
     * @param AttributeValueFactory $attributeValueFactory
     * @param array $data
     */
    public function __construct(
        CategoryRepositoryInterface $categoryRepository,
        ExtensionAttributesFactory $extensionFactory,
        AttributeValueFactory $attributeValueFactory,
        array $data = []
    )
    {
        $this->categoryRepository = $categoryRepository;

        parent::__construct(
            $extensionFactory,
            $attributeValueFactory,
            $data
        );
    }

    /**
     * Retrieves a thumbnail from the data array if set, or null otherwise.
     *
     * @return mixed|null
     */
    public function getThumbnail()
    {
        return $this->_get(self::THUMBNAIL);
    }

    /**
     * Set value for thumbnail
     *
     * @param mixed $value
     * @return Post $this
     */
    public function setThumbnail($thumbnail)
    {
        return $this->setData(self::THUMBNAIL, $thumbnail);
    }

    /**
     * Retrieves visible in banner from the data array if set, or null otherwise.
     *
     * @return mixed|null
     */
    public function getVisibleInBanner()
    {
        return $this->_get(self::VISIBLE_IN_BANNER);
    }

    /**
     * Set value visible in banner
     *
     * @param mixed $value
     * @return Post $this
     */
    public function setVisibleInBanner($visibleInBanner)
    {
        return $this->setData(self::VISIBLE_IN_BANNER, $visibleInBanner);
    }

    /**
     * Retrieves white title from the data array if set, or null otherwise.
     *
     * @return mixed|null
     */
    public function getWhiteTitle()
    {
        return $this->_get(self::WHITE_TITLE);
    }

    /**
     * Set value for white title
     *
     * @param mixed $value
     * @return Post $this
     */
    public function setWhiteTitle($whiteTitle)
    {
        return $this->setData(self::WHITE_TITLE, $whiteTitle);
    }

    /**
     * Retrieves sort order from the data array if set, or null otherwise.
     *
     * @return mixed|null
     */
    public function getSortOrder()
    {
        return $this->_get(self::SORT_ORDER);
    }

    /**
     * Set value for sort order
     *
     * @param mixed $value
     * @return Post $this
     */
    public function setSortOrder($sortOrder)
    {
        return $this->setData(self::SORT_ORDER, $sortOrder);
    }

    /**
     * Retrieve category by id
     *
     * @param $categoryId
     * @return \Aheadworks\Blog\Api\Data\CategoryInterface
     */
    private function getCategoryById($categoryId)
    {
        return $this->categoryRepository->get($categoryId);
    }

    /**
     * Retrieve post categories
     *
     * @return \Aheadworks\Blog\Api\Data\CategoryInterface
     */
    public function getPublicCategories()
    {
        $categoryIds = $this->getCategoryIds();
        $categories = array_map(array($this, 'getCategoryById'), $categoryIds);

        return array_filter($categories, function($category) {
            return $category->getStatus();
        });
    }
}
