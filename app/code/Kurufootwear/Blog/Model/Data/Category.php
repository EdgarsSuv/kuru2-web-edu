<?php
/**
 * @category  Kurufootwear
 * @package   Kurufootwear\Blog
 * @author    Krisjanis Veinbahs <krisjanisv@scandiweb.com>
 * @copyright Copyright (c) 2017 Scandiweb, Inc (http://scandiweb.com)
 * @license   http://opensource.org/licenses/OSL-3.0 The Open Software License 3.0 (OSL-3.0)
 */

namespace Kurufootwear\Blog\Model\Data;

use Aheadworks\Blog\Model\Data\Category as AheadworksCategory;
use Aheadworks\Blog\Model\Url as AheadworksUrl;
use Magento\Framework\Api\AttributeValueFactory;
use Magento\Framework\Api\ExtensionAttributesFactory;

class Category extends AheadworksCategory
{
    /**
     * @var AheadworksUrl
     */
    protected $aheadworksUrl;

    /**
     * Category constructor.
     * @param AheadworksUrl $aheadworksUrl
     * @param ExtensionAttributesFactory $extensionFactory
     * @param AttributeValueFactory $attributeValueFactory
     * @param array $data
     */
    public function __construct(
        AheadworksUrl $aheadworksUrl,
        ExtensionAttributesFactory $extensionFactory,
        AttributeValueFactory $attributeValueFactory,
        array $data = []
    )
    {
        $this->aheadworksUrl = $aheadworksUrl;

        parent::__construct(
            $extensionFactory,
            $attributeValueFactory,
            $data
        );
    }

    /**
     * Get full category url
     *
     * @return string
     */
    public function getUrl()
    {
        $blogUrl = $this->aheadworksUrl->getBlogHomeUrl();
        $categoryUrl = $this->getUrlKey();

        return $blogUrl . $categoryUrl;
    }
}
