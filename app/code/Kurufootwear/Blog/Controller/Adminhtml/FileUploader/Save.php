<?php
/**
 * @vendor Kurufootwear
 * @module Kurufootwear_Blog
 * @author Krisjanis Veinbahs <krisjanisv@scandiweb.com>
 * @copyright Copyright (c) 2017 Scandiweb. All rights reserved.
 * @license http://opensource.org/licenses/afl-3.0.php Academic Free License (AFL 3.0)
 */

namespace Kurufootwear\Blog\Controller\Adminhtml\FileUploader;

use Magento\Backend\App\Action;
use Magento\Backend\App\Action\Context;
use Magento\Framework\Controller\Result\RawFactory;
use Magento\Framework\Filesystem;
use Magento\MediaStorage\Model\File\UploaderFactory;
use Magento\Framework\App\Filesystem\DirectoryList;
use Magento\Framework\Image\AdapterFactory;

class Save extends Action
{
    /**
     * @var RawFactory
     */
    protected $rawFactory;

    /**
     * @var AdapterFactory
     */
    protected $adapterFactory;

    /**
     * @var Filesystem
     */
    protected $filesystem;

    /**
     * @var UploaderFactory
     */
    protected $uploaderFactory;

    /**
     * Save constructor.
     * @param Context $context
     * @param RawFactory $rawFactory
     * @param AdapterFactory $adapterFactory
     * @param Filesystem $filesystem
     * @param UploaderFactory $uploaderFactory
     */
    public function __construct(
        Context $context,
        RawFactory $rawFactory,
        AdapterFactory $adapterFactory,
        Filesystem $filesystem,
        UploaderFactory $uploaderFactory
    )
    {
        $this->rawFactory = $rawFactory;
        $this->adapterFactory = $adapterFactory;
        $this->filesystem = $filesystem;
        $this->uploaderFactory = $uploaderFactory;

        parent::__construct(
            $context
        );
    }

    /**
     * @inheritDoc
     */
    public function execute()
    {
        try {
            $thumbnailPath = 'blog/images/thumbnails';

            /** @var Uploader $uploader */
            $uploader = $this->_objectManager->create(
                'Magento\MediaStorage\Model\File\Uploader',
                ['fileId' => 'thumbnail']
            );

            $uploader->setAllowedExtensions(['jpg', 'jpeg', 'gif', 'png']);
            $imageAdapter = $this->adapterFactory->create();
            $uploader->addValidateCallback('image', $imageAdapter, 'validateUploadFile');
            $uploader->setAllowRenameFiles(true);
            $uploader->setFilesDispersion(true);

            $mediaDirectory = $this->filesystem->getDirectoryRead(DirectoryList::MEDIA);
            $result = $uploader->save(
                $mediaDirectory->getAbsolutePath($thumbnailPath)
            );
            $result['path'] = $thumbnailPath . $result['file'];
        } catch (\Exception $e) {
            if ($e->getCode() == 0) {
                $this->messageManager->addError($e->getMessage());
            }
        }

        /** @var \Magento\Framework\Controller\Result\Raw $response */
        $response = $this->rawFactory->create();
        $response->setHeader('Content-type', 'text/plain');
        $response->setContents(json_encode($result));

        return $response;
    }
}
