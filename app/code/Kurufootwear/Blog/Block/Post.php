<?php
/**
 * @vendor Kurufootwear
 * @module Kurufootwear_Blog
 * @author Krisjanis Veinbahs <krisjanisv@scandiweb.com>
 * @copyright Copyright (c) 2017 Scandiweb. All rights reserved.
 * @license http://opensource.org/licenses/afl-3.0.php Academic Free License (AFL 3.0)
 */

namespace Kurufootwear\Blog\Block;

use Aheadworks\Blog\Api\CategoryRepositoryInterface;
use Aheadworks\Blog\Api\PostRepositoryInterface;
use Aheadworks\Blog\Api\TagRepositoryInterface;
use Aheadworks\Blog\Block\LinkFactory;
use Aheadworks\Blog\Block\Post as AheadworksPost;
use Aheadworks\Blog\Model\Config;
use Aheadworks\Blog\Model\Template\FilterProvider;
use Aheadworks\Blog\Model\Url;
use Magento\Framework\Api\SearchCriteriaBuilder;
use Magento\Framework\View\Element\Template\Context;
use Magento\Store\Model\StoreManagerInterface;
use Magento\Framework\UrlInterface;

/**
 * Class Post
 * @package Kurufootwear\Blog\Block
 */
class Post extends AheadworksPost
{
    /**
     * Template path
     * @var string
     */
    protected $_template = 'post.phtml';

    /**
     * @var StoreManagerInterface
     */
    protected $storeManager;

    /**
     * Post constructor.
     * @param Context $context
     * @param PostRepositoryInterface $postRepository
     * @param CategoryRepositoryInterface $categoryRepository
     * @param TagRepositoryInterface $tagRepository
     * @param SearchCriteriaBuilder $searchCriteriaBuilder
     * @param Config $config
     * @param LinkFactory $linkFactory
     * @param Url $url
     * @param FilterProvider $templateFilterProvider
     * @param array $data
     */
    public function __construct(
        Context $context,
        PostRepositoryInterface $postRepository,
        CategoryRepositoryInterface $categoryRepository,
        TagRepositoryInterface $tagRepository,
        SearchCriteriaBuilder $searchCriteriaBuilder,
        Config $config,
        LinkFactory $linkFactory,
        Url $url,
        FilterProvider $templateFilterProvider,
        array $data = []
    )
    {
        $this->storeManager = $context->getStoreManager();

        parent::__construct(
            $context,
            $postRepository,
            $categoryRepository,
            $tagRepository,
            $searchCriteriaBuilder,
            $config,
            $linkFactory,
            $url,
            $templateFilterProvider,
            $data
        );
    }

    /**
     * Retrieve absolute thumbnail path
     *
     * @param $post
     * @return string
     */
    public function getFullThumbnail($post)
    {
        $thumbnailPath = $post->getThumbnail();
        $baseUrl = $this->storeManager->getStore()
            ->getBaseUrl(UrlInterface::URL_TYPE_MEDIA);

        return $baseUrl . $thumbnailPath;
    }
}
