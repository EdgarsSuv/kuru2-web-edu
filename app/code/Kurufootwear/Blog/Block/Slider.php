<?php
/**
 * @vendor Kurufootwear
 * @module Kurufootwear_Blog
 * @copyright Copyright (c) 2017 Scandiweb. All rights reserved.
 * @license http://opensource.org/licenses/afl-3.0.php Academic Free License (AFL 3.0)
 */

namespace Kurufootwear\Blog\Block;

use Aheadworks\Blog\Api\PostRepositoryInterface;
use Kurufootwear\Blog\Model\Data\Post;
use Magento\Framework\Api\SearchCriteriaBuilder;
use Magento\Framework\UrlInterface;
use Magento\Framework\View\Element\Template;
use Magento\Framework\View\Element\Template\Context;

class Slider extends Template
{
    /**
     * @var PostRepositoryInterface
     */
    protected $postRepository;

    /**
     * @var SearchCriteriaBuilder
     */
    protected $searchCriteriaBuilder;

    /**
     * @var Context
     */
    protected $context;

    /**
     * Slider constructor.
     * @param PostRepositoryInterface $postRepository
     * @param SearchCriteriaBuilder $searchCriteriaBuilder
     * @param Context $context
     * @param array $data
     */
    public function __construct(
        PostRepositoryInterface $postRepository,
        SearchCriteriaBuilder $searchCriteriaBuilder,
        Context $context,
        array $data = []
    )
    {
        $this->postRepository = $postRepository;
        $this->searchCriteriaBuilder = $searchCriteriaBuilder;
        $this->context = $context;

        parent::__construct(
            $context,
            $data
        );
    }


    /**
     * {@inheritdoc}
     */
    public function getSliderPosts()
    {
        $searchCriteria = $this->searchCriteriaBuilder
            ->addFilter(Post::VISIBLE_IN_BANNER, 1)
            ->create();
        $searchResults = $this->postRepository->getList($searchCriteria)->getItems();
        usort($searchResults, array($this, 'postSort'));

        return $searchResults;
    }

    /**
     * Compare post order value
     * @param $post1
     * @param $post2
     * @return int
     */
    private function postSort($post1, $post2)
    {
        $value1 = $post1->getSortOrder() ? $post1->getSortOrder() : -1;
        $value2 = $post2->getSortOrder() ? $post2->getSortOrder() : -1;

        return $value1 - $value2;
    }

    /**
     * @param $post
     * @return string
     */
    public function getFullThumbnail($post)
    {
        $thumbnailPath = $post->getThumbnail();
        $baseUrl = $this->context->getStoreManager()->getStore()
            ->getBaseUrl(UrlInterface::URL_TYPE_MEDIA);

        return $baseUrl . $thumbnailPath;
    }
}
