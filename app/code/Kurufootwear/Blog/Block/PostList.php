<?php
/**
 * @vendor Kurufootwear
 * @module Kurufootwear_Blog
 * @author Krisjanis Veinbahs <krisjanisv@scandiweb.com>
 * @copyright Copyright (c) 2017 Scandiweb. All rights reserved.
 * @license http://opensource.org/licenses/afl-3.0.php Academic Free License (AFL 3.0)
 */

namespace Kurufootwear\Blog\Block;

use Aheadworks\Blog\Api\CategoryRepositoryInterface;
use Aheadworks\Blog\Api\TagRepositoryInterface;
use Aheadworks\Blog\Block\Post\ListingFactory;
use Aheadworks\Blog\Block\PostList as AheadworksPostList;
use Aheadworks\Blog\Model\Config;
use Aheadworks\Blog\Model\Url;
use Magento\Framework\UrlInterface;
use Magento\Framework\View\Element\Template\Context;
use Magento\Store\Model\StoreManager;

/**
 * List of posts block
 * @package Aheadworks\Blog\Block
 */
class PostList extends AheadworksPostList
{
    /**
     * @var Context
     */
    protected $context;

    /**
     * @var StoreManager
     */
    protected $storeManager;

    /**
     * PostList constructor.
     * @param Context $context
     * @param ListingFactory $postListingFactory
     * @param CategoryRepositoryInterface $categoryRepository
     * @param TagRepositoryInterface $tagRepository
     * @param Url $url
     * @param Config $config
     * @param array $data
     */
    public function __construct(
        Context $context,
        ListingFactory $postListingFactory,
        CategoryRepositoryInterface $categoryRepository,
        TagRepositoryInterface $tagRepository,
        Url $url,
        Config $config,
        array $data = []
    )
    {
        $this->storeManager = $context->getStoreManager();

        parent::__construct(
            $context,
            $postListingFactory,
            $categoryRepository,
            $tagRepository,
            $url,
            $config,
            $data
        );
    }

    /**
     * Retrieve absolute thumbnail path
     *
     * @param $post
     * @return string
     */
    public function getFullThumbnail($post)
    {
        $thumbnailPath = $post->getThumbnail();
        $baseUrl = $this->storeManager->getStore()
            ->getBaseUrl(UrlInterface::URL_TYPE_MEDIA);

        return $baseUrl . $thumbnailPath;
    }
}
