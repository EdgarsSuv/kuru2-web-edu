<?php
/**
 * @category  Kurufootwear
 * @package   Kurufootwear\Blog
 * @author    Krisjanis Veinbahs <krisjanisv@scandiweb.com>
 * @copyright Copyright (c) 2017 Scandiweb, Inc (http://scandiweb.com)
 * @license   http://opensource.org/licenses/OSL-3.0 The Open Software License 3.0 (OSL-3.0)
 */

namespace Kurufootwear\Blog\Block\Sidebar;

use Magento\Cms\Model\Block;
use Magento\Framework\View\Element\Template;
use Magento\Cms\Model\Template\FilterProvider;
use Magento\Cms\Model\BlockFactory;
use Kurufootwear\Migration\Setup\Migration\SocialLinkBlockMigration;

class Socials extends Template
{
    /**
     * @var FilterProvider
     */
    protected $cmsFilterProvider;

    /**
     * @var BlockFactory
     */
    protected $cmsBlockFactory;

    /**
     * Socials constructor.
     * @param FilterProvider $cmsFilterProvider
     * @param Template\Context $context
     * @param array $data
     */
    public function __construct(
        FilterProvider $cmsFilterProvider,
        BlockFactory $cmsBlockFactory,
        Template\Context $context,
        array $data = []
    )
    {
        $this->cmsFilterProvider = $cmsFilterProvider;
        $this->cmsBlockFactory = $cmsBlockFactory;

        parent::__construct(
            $context,
            $data
        );
    }

    /**
     * Retrieve Social CMS Block html
     */
    public function getSocialCmsBlockHtml()
    {
        $id = SocialLinkBlockMigration::SOCIAL_LINK_BLOCK_ID;
        $block = $this->getCmsBlock($id);
        $html = $this->getCmsBlockHtml($block);

        return $html;
    }

    /**
     * Retrieves CMS block
     *
     * @return bool|Block|null
     */
    public function getCmsBlock($id)
    {
        return $this->cmsBlockFactory->create()
            ->setStoreId($this->_storeManager->getStore()->getId())
            ->load($id);
    }

    /**
     * @param Block $cmsBlock
     * @return string
     */
    public function getCmsBlockHtml(Block $cmsBlock)
    {
        return $this->cmsFilterProvider->getBlockFilter()
            ->setStoreId($this->_storeManager->getStore()->getId())
            ->filter($cmsBlock->getContent());
    }
}
