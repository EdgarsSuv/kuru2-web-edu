<?php
/**
 * @category  Kurufootwear
 * @package   Kurufootwear\Blog
 * @author    Krisjanis Veinbahs <krisjanisv@scandiweb.com>
 * @copyright Copyright (c) 2017 Scandiweb, Inc (http://scandiweb.com)
 * @license   http://opensource.org/licenses/OSL-3.0 The Open Software License 3.0 (OSL-3.0)
 */

namespace Kurufootwear\Blog\Block\Sidebar;

use Magento\Framework\View\Element\Template;
use Magento\Framework\Api\SearchCriteriaBuilder;
use Aheadworks\Blog\Model\Config;
use Aheadworks\Blog\Model\ResourceModel\CategoryRepository;
use Aheadworks\Blog\Model\Source\Category\Status as CategoryStatus;

class Categories extends Template
{

    /**
     * @var Config
     */
    protected $config;

    /**
     * @var SearchCriteriaBuilder
     */
    protected $searchCriteriaBuilder;

    /**
     * @var CategoryRepository
     */
    protected $categoryRepository;

    /**
     * Categories constructor.
     * @param Template\Context $context
     * @param Config $config
     * @param SearchCriteriaBuilder $searchCriteriaBuilder
     * @param CategoryRepository $categoryRepository
     * @param array $data
     */
    public function __construct(
        Template\Context $context,
        Config $config,
        SearchCriteriaBuilder $searchCriteriaBuilder,
        CategoryRepository $categoryRepository,
        array $data = []
    )
    {
        $this->config = $config;
        $this->searchCriteriaBuilder = $searchCriteriaBuilder;
        $this->categoryRepository = $categoryRepository;

        parent::__construct(
            $context,
            $data
        );
    }

    /**
     * Return if given category is currently opened
     *
     * @return bool
     */
    public function isCategoryCurrent($category)
    {
        $blogUrlKey = $this->config->getRouteToBlog();
        $categoryUrlKey = $category->getUrlKey();
        $path = '/' . $blogUrlKey . '/' . $categoryUrlKey;
        $currentPath = $this->getRequest()->getPathInfo();

        return $path == $currentPath;
    }

    /**
     * Get an array of all enabled blog categories
     *
     * @return \Aheadworks\Blog\Api\Data\CategoryInterface[]
     */
    public function getCategories()
    {
        $searchCriteria = $this->searchCriteriaBuilder
            ->addFilter('status', CategoryStatus::ENABLED)
            ->create();
        $searchResults = $this->categoryRepository->getList($searchCriteria)->getItems();

        return $searchResults;
    }
}
