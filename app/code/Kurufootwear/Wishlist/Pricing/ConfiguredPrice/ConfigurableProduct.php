<?php
/**
 * @vendor      Kurufootwear
 * @module      Kurufootwear_Wishlist
 * @author      Jelena Sorohova <info@scandiweb.com>
 * @copyright   Copyright (c) 2017 Scandiweb, Ltd (http://scandiweb.com)
 * @license     http://opensource.org/licenses/afl-3.0.php Academic Free License (AFL 3.0)
 */
namespace Kurufootwear\Wishlist\Pricing\ConfiguredPrice;

class ConfigurableProduct extends \Magento\Wishlist\Pricing\ConfiguredPrice\ConfigurableProduct
{
    /**
     * @inheritdoc
     */
    public function getValue()
    {
        $result = 0.;
        /** @var \Magento\Wishlist\Model\Item\Option $customOption */
        $customOption = $this->getProduct()->getCustomOption('simple_product');
        if ($customOption) {
            /** @var \Magento\Framework\Pricing\PriceInfoInterface $priceInfo */
            $priceInfo = $customOption->getProduct()->getPriceInfo();
            $result = $priceInfo->getPrice(self::PRICE_CODE)->getValue();
            return $result;
        }
        else {
            return $this->getBasePrice()->getValue();
        }
    }
}
