<?php
/**
 * @vendor      Kurufootwear
 * @module      Kurufootwear_Wishlist
 * @copyright   Copyright (c) 2018 Scandiweb, Ltd (http://scandiweb.com)
 * @license     http://opensource.org/licenses/afl-3.0.php Academic Free License (AFL 3.0)
 */
namespace Kurufootwear\Wishlist\Helper;

use Magento\Catalog\Api\ProductRepositoryInterface;
use Magento\Customer\Helper\View;
use Magento\Customer\Model\Session;
use Magento\Framework\App\Helper\Context;
use Magento\Framework\Data\Helper\PostHelper;
use Magento\Framework\Registry;
use Magento\Store\Model\ScopeInterface;
use Magento\Store\Model\StoreManagerInterface;
use Magento\Wishlist\Controller\WishlistProviderInterface;
use Magento\Wishlist\Model\WishlistFactory;

class Data extends \Magento\Wishlist\Helper\Data
{
    /**
     * @var \Magento\Customer\Model\Session
     */
    protected $_customerSession;

    /**
     * Data constructor.
     * @param Context $context
     * @param Registry $coreRegistry
     * @param Session $customerSession
     * @param WishlistFactory $wishlistFactory
     * @param StoreManagerInterface $storeManager
     * @param PostHelper $postDataHelper
     * @param View $customerViewHelper
     * @param WishlistProviderInterface $wishlistProvider
     * @param ProductRepositoryInterface $productRepository
     */
    public function __construct(
        Context $context,
        Registry $coreRegistry,
        Session $customerSession,
        WishlistFactory $wishlistFactory,
        StoreManagerInterface $storeManager,
        PostHelper $postDataHelper,
        View $customerViewHelper,
        WishlistProviderInterface $wishlistProvider,
        ProductRepositoryInterface $productRepository
    ){
        parent::__construct(
            $context,
            $coreRegistry,
            $customerSession,
            $wishlistFactory,
            $storeManager,
            $postDataHelper,
            $customerViewHelper,
            $wishlistProvider,
            $productRepository
        );
    }

    /**
     * Calculate count of wishlist items and put value to customer session.
     * Method called after wishlist modifications and trigger 'wishlist_items_renewed' event.
     * Depends from configuration.
     * Added check first to check if collection for redundantcy is there then do the calculations
     * @return $this
     */
    public function calculate()
    {
        $count = 0;
        $wishlistCollection = $this->getWishlistItemCollection();
        if ($this->getCustomer() && $wishlistCollection) {
            $collection = $wishlistCollection->setInStockFilter(true);
            if ($this->scopeConfig->getValue(
                self::XML_PATH_WISHLIST_LINK_USE_QTY,
                ScopeInterface::SCOPE_STORE
            )
            ) {
                $count = $collection->getItemsQty();
            } else {
                $count = $collection->getSize();
            }
            $this->_customerSession->setWishlistDisplayType(
                $this->scopeConfig->getValue(
                    self::XML_PATH_WISHLIST_LINK_USE_QTY,
                    ScopeInterface::SCOPE_STORE
                )
            );
            $this->_customerSession->setDisplayOutOfStockProducts(
                $this->scopeConfig->getValue(
                    self::XML_PATH_CATALOGINVENTORY_SHOW_OUT_OF_STOCK,
                    ScopeInterface::SCOPE_STORE
                )
            );
        }
        $this->_customerSession->setWishlistItemCount($count);
        $this->_eventManager->dispatch('wishlist_items_renewed');
        return $this;
    }
}
