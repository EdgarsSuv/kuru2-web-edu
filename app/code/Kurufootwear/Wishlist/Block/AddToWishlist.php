<?php
/**
 * @vendor      Kurufootwear
 * @module      Kurufootwear_Wishlist
 * @copyright   Copyright (c) 2018 Scandiweb, Ltd (http://scandiweb.com)
 * @license     http://opensource.org/licenses/afl-3.0.php Academic Free License (AFL 3.0)
 */
namespace Kurufootwear\Wishlist\Block;

use Magento\Framework\View\Element\Template\Context;

class AddToWishlist extends \Magento\Wishlist\Block\AddToWishlist
{
    /**
     * Product types
     *
     * @var array|null
     */
    private $productTypes;

    /**
     * AddToWishlist constructor.
     * @param Context $context
     * @param array $data
     */
    public function __construct(
        Context $context,
        array $data = []
    )
    {
        parent::__construct($context, $data);
    }

    /**
     * Returns an array of product types
     *
     * @return array|null
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    private function getProductTypes()
    {
        if ($this->productTypes === null) {
            $this->productTypes = [];
            $block = $this->getLayout()->getBlock('category.products.list');
            if ($block) {
                $productCollection = $block->getLoadedProductCollection();

                /** @var $product \Magento\Catalog\Model\Product */
                foreach ($productCollection as $product) {
                    $productTypes[] = $product->getTypeId();
                }

                if (isset($productTypes)) {
                    $this->productTypes = array_unique($productTypes);
                }
            }
        }
        return $this->productTypes;
    }

    /**
     * {@inheritdoc}
     */
    protected function _toHtml()
    {
        $this->setModuleName($this->extractModuleName('Magento\Wishlist\Block\AddToWishlist'));

        if (!$this->getProductTypes()) {
            return '';
        }
        return parent::_toHtml();
    }
}
