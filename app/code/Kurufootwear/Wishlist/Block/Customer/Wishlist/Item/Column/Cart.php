<?php
/**
 * @category    Kurufootwear
 * @package     Kurufootwear\Wishlist
 * @author      Aleksejs Baranovs <info@scandiweb.com>
 * @copyright   Copyright (c) 2018 Scandiweb, Inc (http://scandiweb.com)
 * @license     http://opensource.org/licenses/OSL-3.0 The Open Software License 3.0 (OSL-3.0)
 */

namespace Kurufootwear\Wishlist\Block\Customer\Wishlist\Item\Column;

use Magento\Catalog\Block\Product\Context;
use Magento\Framework\App\Http\Context as HttpContext;
use Magento\Wishlist\Block\Customer\Wishlist\Item\Column\Cart as BaseCart;
use Magento\Wishlist\Model\Item;
use Magento\Catalog\Helper\Product\Configuration;

/**
 * Class Cart
 * @package Kurufootwear\Wishlist\Block\Customer\Wishlist\Item\Column
 */
class Cart extends BaseCart
{
    /**
     * @var Configuration
     */
    private $configuration;

    /**
     * Cart constructor.
     *
     * @param Context $context
     * @param HttpContext $httpContext
     * @param array $data
     * @param Configuration $configuration
     */
    public function __construct(
        Context $context,
        HttpContext $httpContext,
        array $data = [],
        Configuration $configuration
    ) {
        parent::__construct($context, $httpContext, $data);

        $this->configuration = $configuration;
    }

    /**
     * Check if product is salable
     *
     * @param Item $item
     * @return bool
     */
    public function checkSalableProduct(Item $item)
    {
        $simpleProduct = $item->getOptionByCode('simple_product');

        // Workaround to check if isset simple product or all required options (color and size)
        if (!isset($simpleProduct) || count($this->configuration->getOptions($item)) <= 1) {
            return $item->getProduct()->getIsSalable();
        }

        return $simpleProduct->getProduct()->getIsSalable();
    }
}
