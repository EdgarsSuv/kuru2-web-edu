<?php
/**
 * @category    Kurufootwear
 * @package     Kurufootwear\Rma
 * @author      Jim McGowen <jim@kurufootwear.com>
 * @copyright   Copyright (c) 2018 KURU Footwear. All rights reserved.
 */

namespace Kurufootwear\Rewards\Model;

/**
 * Class Purchase
 * @package Kurufootwear\Rewards\Model
 */
class Purchase extends \Mirasvit\Rewards\Model\Purchase
{
    /**
     * Overridden to look at the coupon code as well.
     *
     * @param \Magento\Quote\Model\Quote $quote
     *
     * @return bool
     */
    protected function hasChanges($quote)
    {
        $cachedQuote = $this->customerSession->getRWCachedQuote();
        $data = [];
        $data[] = (int) $quote->getCustomerId();//we need this, because if customer do 'reorder', customer id maybe = 0.
        $data[] = (double) $quote->getData('grandtotal');
        $data[] = (double) $quote->getData('subtotal');
        $data[] = (double) $quote->getData('subtotal_with_discount');
        $data[] = (string) $quote->getData('coupon_code');
        $data[] = count($quote->getAllItems());
        
        if ($shipping = $quote->getShippingAddress()) {
            $data[] = $shipping->getData('shipping_method');
            $data[] = (double) $shipping->getData('shipping_amount');
        }
        if ($payment = $quote->getPayment()) {
            if ($payment->hasMethodInstance()) {
                $data[] = $payment->getMethodInstance()->getCode();
            }
        }
        
        $newCachedQuote = implode('|', $data);
        
        if ($cachedQuote != $newCachedQuote) {
            $this->customerSession->setRWCachedQuote($newCachedQuote);
            return true;
        }
        
        return false;
    }

    /**
     * Validate points not being set
     * to a larger than possible value
     *
     * @param $points
     * @throws \Magento\Framework\Exception\LocalizedException
     *
     * @return $this
     */
    public function setSpendPoints($points)
    {
        /**
         * Frontend reward point validation
         */
        try {
            $isBackend = $this->_appState->getAreaCode() === 'adminhtml';
        } catch (\Exception $exception) {
            $isBackend = true;
        }

        if (!$isBackend) {
            $maxPoints = $this->getMaxPointsNumberToSpent();

            if ($points > $maxPoints) {
                $points = $maxPoints;
            }
        }

        $this->setData('spend_points', $points);
        return $this;
    }
}