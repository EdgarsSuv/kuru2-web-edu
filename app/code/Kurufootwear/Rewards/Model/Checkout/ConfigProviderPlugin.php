<?php
/**
 * @vendor      Kurufootwear
 * @module      Kurufootwear_Rewards
 * @author      Krisjanis Veinbahs <info@scandiweb.com>
 * @copyright   Copyright (c) 2017 Scandiweb. All rights reserved.
 * @license     http://opensource.org/licenses/afl-3.0.php Academic Free License (AFL 3.0)
 */
namespace Kurufootwear\Rewards\Model\Checkout;

use Magento\Checkout\Model\DefaultConfigProvider;
use Mirasvit\Rewards\Helper\Data;
use Mirasvit\Rewards\Helper\Purchase;

/**
 * Class ConfigProviderPlugin
 * @package Kurufootwear\Rewards\Model\Checkout
 */
class ConfigProviderPlugin
{
    /**
     * ConfigProviderPlugin constructor.
     * @param Purchase $rewardsPurchase
     * @param Data $rewardsData
     */
    public function __construct(
        Purchase $rewardsPurchase,
        Data $rewardsData
    ) {
        $this->rewardsPurchase = $rewardsPurchase;
        $this->rewardsData     = $rewardsData;
    }

    /**
     * @param DefaultConfigProvider $subject
     * @param array $result
     *
     * @return array
     * @SuppressWarnings(PHPMD.UnusedFormalParameter)
     */
    public function afterGetConfig(DefaultConfigProvider $subject, array $result)
    {
        if (($purchase = $this->rewardsPurchase->getPurchase()) && ($earnPoints = $purchase->getEarnPoints())) {
            $result['chechoutRewardsPoints'] = $this->rewardsData->formatPoints($earnPoints);
        }

        return $result;
    }
}
