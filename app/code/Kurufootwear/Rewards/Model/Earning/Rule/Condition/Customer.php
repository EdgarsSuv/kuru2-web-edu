<?php
/**
 * @category     Kurufootwear
 * @package      Kurufootwear/Rewards
 * @author       Krisjanis Veinbahs <krisjanisv@scandiweb.com>
 * @copyright    Copyright (c) 2018 KURU Footwear. All rights reserved.
 */
namespace Kurufootwear\Rewards\Model\Earning\Rule\Condition;

use Magento\Newsletter\Model\SubscriberFactory;

/**
 * Class Customer
 *
 * Overridden to restrict referral bonus to only trigger after first invoiced order
 *
 * @package Kurufootwear\Rewards\Model\Earning\Rule\Condition
 */
class Customer extends \Mirasvit\Rewards\Model\Earning\Rule\Condition\Customer
{
    /**
     * Overridden to restrict referral bonus to only trigger after first invoiced order
     *
     * @param \Magento\Framework\DataObject $customer - can be registered customer and guest(!)
     *
     * @return bool
     */
    protected function validateCustomer(\Magento\Framework\DataObject $customer)
    {
        $attrCode = $this->getAttribute();

        $subscriber = $this->subscriberFactory->create()->loadByEmail($customer->getEmail());

        $reviews = $this->reviewCollectionFactory->create()
            ->addFieldToFilter('customer_id', $customer->getId());
        $reviewsCount = $reviews->count();

        $lifetimeSales = $this->getSumOfOrdersByCustomer($customer);
        $numberOfOrders = $this->getNumberOfOrdersByCustomer($customer);

        /**
         * Condition:
         * - Calculating a referral rule (referred_customer (aka referral user) exists)
         * - Currently validating orders_number
         *
         * Override:
         * - Get orders_number of/from referred customer (referral user)
         *
         * Override added for the following reason:
         * - We want to limit the referral bonus for the referral owner
         *   to only trigger for the first order from the referral user,
         *   this could be done by adding a condition "referral user has 1 order",
         *   but currently that condition has the referral owner order amount
         *
         */
        if ($attrCode === 'orders_number' && $customer->hasReferredCustomer()) {
            $numberOfOrders = $this->getNumberOfOrdersByCustomer($customer->getReferredCustomer());
        }

        $customer
            ->setData(self::OPTION_IS_SUBSCRIBER, $subscriber->getId() ? 1 : 0)
            ->setData(self::OPTION_REVIEWS_NUMBER, $reviewsCount)
            ->setData(self::OPTION_ORDERS_SUM, $lifetimeSales)
            ->setData(self::OPTION_ORDERS_NUMBER, $numberOfOrders);

        $value = $customer->getData($attrCode);

        return $this->validateAttribute($value);
    }
}
