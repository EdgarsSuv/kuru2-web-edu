<?php
/**
 * @category    Kurufootwear
 * @package     Kurufootwear\Rewards
 * @author      Aleksejs Baranovs <info@scandiweb.com>
 * @copyright   Copyright (c) 2018 KURU Footwear. All rights reserved.
 */

namespace Kurufootwear\Rewards\Observer;

use Exception;
use Magento\Customer\Model\Session;
use Magento\Framework\Event\Observer;
use Magento\Framework\Exception\LocalizedException;
use Mirasvit\Rewards\Helper\Purchase;
use Mirasvit\Rewards\Helper\Referral;
use Mirasvit\Rewards\Model\Config;
use Mirasvit\Rewards\Observer\AdminOrderQuoteAfterSave as BaseAdminOrderQuoteAfterSave;
use Psr\Log\LoggerInterface;

/**
 * Class AdminOrderQuoteAfterSave
 * @package Kurufootwear\Rewards\Observer
 */
class AdminOrderQuoteAfterSave extends BaseAdminOrderQuoteAfterSave
{
    /**
     * @var LoggerInterface
     */
    private $logger;

    /**
     * AdminOrderQuoteAfterSave constructor.
     *
     * @param Session $customerSession
     * @param Purchase $rewardsPurchase
     * @param Referral $rewardsReferral
     * @param Config $config
     * @param LoggerInterface $logger
     */
    public function __construct(
        Session $customerSession,
        Purchase $rewardsPurchase,
        Referral $rewardsReferral,
        Config $config,
        LoggerInterface $logger
    ) {
        parent::__construct($customerSession, $rewardsPurchase, $rewardsReferral, $config);

        $this->logger = $logger;
    }

    /**
     * Execute observer
     * Override to add logger
     *
     * @param Observer $observer
     *
     * @return void
     *
     * @throws LocalizedException
     */
    public function execute(Observer $observer)
    {
        if ($this->config->getQuoteSaveFlag()) {
            $this->logger->error('Quote save flag was true while saving the order from BE');

            return;
        }
        if (!$quote = $observer->getQuote()) {
            $this->logger->error('No quote provided while saving the order from BE');

            return;
        }

        try {
            $this->config->setQuoteSaveFlag(true);
            $this->refreshPoints($quote);
            $this->config->setQuoteSaveFlag(false);
        } catch (Exception $e) {
            $this->logger->critical('Critical error while saving the order from BE: ' . $e->getMessage());
        }
    }
}
