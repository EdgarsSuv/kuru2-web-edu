<?php
/**
 * @category    Kurufootwear
 * @package     Kurufootwear\Rewards
 * @author      Krisjanis Veinbahs <krisjanisv@scandiweb.com>
 * @copyright   Copyright (c) 2017 KURU Footwear. All rights reserved.
 */

namespace Kurufootwear\Rewards\Observer;

use Mirasvit\Rewards\Observer\OrderCheckoutSuccess as MirasvitOrderCheckoutSuccess;

class OrderCheckoutSuccess extends MirasvitOrderCheckoutSuccess
{
    /**
     * @param \Magento\Sales\Model\Order $order
     * @return void
     */
    public function addPointsNotifications($order)
    {
        if (!$order->getCustomerId()) {
            return;
        }

        $quote = $this->quoteCollectionFactory->create()
            ->addFieldToFilter('entity_id', $order->getQuoteId())
            ->getFirstItem(); //we need this for correct work if we create orders via backend
        $totalEarnedPoints = $this->rewardsBalanceEarn->getPointsEarned($quote);
        $purchase = $this->rewardsPurchase->getByOrder($order);
        $totalSpendPoints = $purchase->getPointsNumber();

        if ($totalEarnedPoints && $totalSpendPoints) {
            $this->addNotificationMessage(__('You earned %1 and spent %2 for this order.',
                $this->rewardsData->formatPoints($totalEarnedPoints),
                $this->rewardsData->formatPoints($totalSpendPoints)));
        } elseif ($totalSpendPoints) {
            $this->addNotificationMessage(__('You spent %1 for this order.',
                $this->rewardsData->formatPoints($totalSpendPoints)));
        } elseif ($totalEarnedPoints) {
            $this->addNotificationMessage(__('You earned %1 for this order.',
                $this->rewardsData->formatPoints($totalEarnedPoints)));
        }
    }

    /**
     * @param string $message
     *
     * @return void
     */
    private function addNotificationMessage($message)
    {
        $this->messageManager->addSuccessMessage($message);
    }
}