<?php
/**
 * @category    Kurufootwear
 * @package     Kurufootwear\Rewards
 * @author      Krisjanis Veinbahs <krisjanisv@scandiweb.com>
 * @copyright   Copyright (c) 2018 KURU Footwear. All rights reserved.
 */
namespace Kurufootwear\Rewards\Observer;

use Exception;
use Magento\Checkout\Model\CartFactory;
use Magento\Checkout\Model\Type\Onepage;
use Magento\Customer\Model\SessionFactory;
use Magento\Framework\Data\Collection\AbstractDb;
use Magento\Framework\Event\Observer;
use Magento\Framework\Message\ManagerInterface;
use Magento\Framework\Model\Context;
use Magento\Framework\Model\ResourceModel\AbstractResource;
use Magento\Framework\Registry;
use Magento\Quote\Model\ResourceModel\Quote\CollectionFactory;
use Magento\Sales\Api\OrderRepositoryInterface;
use Magento\Sales\Model\Order;
use Magento\Sales\Model\OrderFactory;
use Mirasvit\Rewards\Helper\Balance\Earn;
use Mirasvit\Rewards\Helper\Balance\Order as OrderHelper;
use Mirasvit\Rewards\Helper\Behavior;
use Mirasvit\Rewards\Helper\Data;
use Mirasvit\Rewards\Helper\Purchase as PurchaseHelper;
use Mirasvit\Rewards\Helper\Referral;
use Mirasvit\Rewards\Model\Config;
use Mirasvit\Rewards\Model\Purchase;
use Mirasvit\Rewards\Observer\OrderPlaceAfter as MirasvitOrderPlaceAfter;

class OrderPlaceAfter extends MirasvitOrderPlaceAfter
{
    /**
     * @var OrderRepositoryInterface
     */
    protected $orderRepository;

    /**
     * OrderPlaceAfter constructor.
     * @param SessionFactory $sessionFactory
     * @param CartFactory $cartFactory
     * @param OrderFactory $orderFactory
     * @param CollectionFactory $quoteCollectionFactory
     * @param Config $config
     * @param Onepage $typeOnepage
     * @param PurchaseHelper $rewardsPurchase
     * @param Referral $rewardsReferral
     * @param OrderHelper $rewardsBalanceOrder
     * @param Earn $rewardsBalanceEarn
     * @param Data $rewardsData
     * @param Behavior $rewardsBehavior
     * @param Registry $registry
     * @param ManagerInterface $messageManager
     * @param Context $context
     * @param OrderRepositoryInterface $orderRepository
     * @param AbstractResource|null $resource
     * @param AbstractDb|null $resourceCollection
     */
    public function __construct(
        SessionFactory $sessionFactory,
        CartFactory $cartFactory,
        OrderFactory $orderFactory,
        CollectionFactory $quoteCollectionFactory,
        Config $config,
        Onepage $typeOnepage,
        PurchaseHelper $rewardsPurchase,
        Referral $rewardsReferral,
        OrderHelper $rewardsBalanceOrder,
        Earn $rewardsBalanceEarn,
        Data $rewardsData,
        Behavior $rewardsBehavior,
        Registry $registry,
        ManagerInterface $messageManager,
        Context $context,
        OrderRepositoryInterface $orderRepository,
        AbstractResource $resource = null,
        AbstractDb $resourceCollection = null
    ) {
        parent::__construct(
            $sessionFactory,
            $cartFactory,
            $orderFactory,
            $quoteCollectionFactory,
            $config,
            $typeOnepage,
            $rewardsPurchase,
            $rewardsReferral,
            $rewardsBalanceOrder,
            $rewardsBalanceEarn,
            $rewardsData,
            $rewardsBehavior,
            $registry,
            $messageManager,
            $context,
            $resource,
            $resourceCollection
        );

        $this->orderRepository = $orderRepository;
    }

    /**
     * @param Observer $observer
     *
     * @return void
     *
     * @SuppressWarnings(PHPMD.UnusedFormalParameter)
     */
    public function execute(Observer $observer)
    {
        /** @var Order $order */
        $order = $observer->getEvent()->getOrder();

        /** @var Purchase $purchase */
        $purchase = $this->rewardsPurchase->getByQuote($order->getQuoteId());
        $this->refreshPoints($purchase->getQuote());

        if ($order && $order->getId() && $this->_isOrderPaidNow() && $order->getCustomerId()) {
            try {
                $this->rewardsBalanceOrder->spendOrderPoints($order);
            } catch (Exception $e) {
                /**
                 * Impossible amount of reward points were spent
                 * Set order to "On hold" with comment
                 */
                $order->hold();
                $order->addStatusHistoryComment(
                    __("Order resulted in negative reward points, please manually review.")->render()
                );
                $order->setIsCustomerNotified(false);
                $this->orderRepository->save($order);
            }
        }
    }
}
