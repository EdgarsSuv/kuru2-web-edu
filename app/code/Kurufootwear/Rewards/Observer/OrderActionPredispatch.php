<?php

namespace Kurufootwear\Rewards\Observer;

class OrderActionPredispatch extends \Mirasvit\Rewards\Observer\OrderActionPredispatch
{
    /**
     * Overridden because calling refreshPoints for every request to checkout
     * causes sporadic problems. Sometimes when the quote changes, the points 
     * are recalculated before the quote has had a chance to finish changing.
     * 
     * @param \Magento\Framework\Event\Observer $observer
     *
     * @return void
     *
     * @SuppressWarnings(PHPMD.UnusedFormalParameter)
     */
    public function execute(\Magento\Framework\Event\Observer $observer)
    {
        return;
    }
}
