<?php
/**
 * @vendor      Kurufootwear
 * @module      Kurufootwear_Rewards
 * @author      Krisjanis Veinbahs <info@scandiweb.com>
 * @copyright   Copyright (c) 2017 Scandiweb. All rights reserved.
 * @license     http://opensource.org/licenses/afl-3.0.php Academic Free License (AFL 3.0)
 */
namespace Kurufootwear\Rewards\Controller\Checkout;

use Mirasvit\Rewards\Controller\Checkout\UpdatePaymentMethodPost as MirasvitUpdatePaymentMethodPost;
use Magento\Framework\Controller\ResultFactory;

class UpdatePaymentMethodPost extends MirasvitUpdatePaymentMethodPost
{
    /**
     * @return \Magento\Framework\Controller\Result\Redirect
     * @SuppressWarnings(PHPMD.ExitExpression)
     */
    public function execute()
    {
        $this->resultFactory->create(ResultFactory::TYPE_PAGE);

        $paymentMethod = $this->getRequest()->getParam('payment', '');

        if (is_array($paymentMethod) && array_key_exists('method', $paymentMethod)) {
            $response = $this->rewardsCheckout->updatePaymentMethod($paymentMethod['method']);
        } else if (!is_array($paymentMethod)) {
            $response = $this->rewardsCheckout->updatePaymentMethod($paymentMethod);
        } else {
            $response = $this->rewardsCheckout->updatePaymentMethod('');
        }

        if ($this->getRequest()->isXmlHttpRequest()) {
            echo json_encode($response);
            exit;
        }

        return $this->_goBack();
    }
}
