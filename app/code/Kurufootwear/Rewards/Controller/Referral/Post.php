<?php
/**
 * @vendor      Kurufootwear
 * @module      Kurufootwear_Rewards
 * @author      Kristaps Stalidzans <info@scandiweb.com>
 * @copyright   Copyright (c) 2017 Scandiweb. All rights reserved.
 * @license     http://opensource.org/licenses/afl-3.0.php Academic Free License (AFL 3.0)
 */
namespace Kurufootwear\Rewards\Controller\Referral;

use Magento\Framework\Controller\ResultFactory;

class Post extends \Mirasvit\Rewards\Controller\Referral\Post
{
    /**
     * @return void
     */
    public function execute()
    {
        $this->resultFactory->create(ResultFactory::TYPE_REDIRECT);

        $customer = $this->customer->getCurrentCustomer();
        $emails = $this->getRequest()->getParam('email');
        $invitations = explode(',', $emails);

        if ($this->getRequest()->getParam('message')) {
            $message = $this->getRequest()->getParam('message');
        } else {
            $message = '';
        }

        $rejectedEmails = $this->rewardsReferral->frontendPost($customer, $invitations, $message);

        if (count($rejectedEmails)) {
            foreach ($rejectedEmails as $email) {
                $this->messageManager->addNoticeMessage(
                    __('Customer with email %1 has been already invited to our store', $email)
                );
            }
        }

        if (count($rejectedEmails) < count($invitations)) {
            $this->messageManager->addSuccessMessage(__('Your invitations were sent. Thanks!'));
        }

        $this->_redirect('*/*/');
    }
}
