<?php
/**
 * @vendor      Kurufootwear
 * @module      Kurufootwear_Rewards
 * @author      Kristaps Stalidzans <info@scandiweb.com>
 * @copyright   Copyright (c) 2017 Scandiweb. All rights reserved.
 * @license     http://opensource.org/licenses/afl-3.0.php Academic Free License (AFL 3.0)
 */
namespace Kurufootwear\Rewards\Block\Account;

use Mirasvit\Rewards\Model\Transaction;
use Mirasvit\Rewards\Model\ResourceModel\Transaction\Collection;

class Listing extends \Mirasvit\Rewards\Block\Account\Listing
{
    const POSITIVE = 'gteq';
    const NEGATIVE = 'lteq';

    /**
     * @var Collection
     */
    protected $_collectionPositive;

    /**
     * @var Collection
     */
    protected $_collectionNegative;

    /**
     * @return Collection|Transaction[]
     */
    public function getTransactionCollectionPositive()
    {
        if (!$this->_collectionPositive) {
            $this->_collectionPositive = $this->transactionCollectionFactory->create()
                ->addFieldToFilter('customer_id', $this->getCustomer()->getId())
                ->addFieldToFilter('amount', [[self::POSITIVE => 0]])
                ->setOrder('created_at', 'desc');
        }

        return $this->_collectionPositive;
    }

    /**
     * @return Collection|Transaction[]
     */
    public function getTransactionCollectionNegative()
    {
        if (!$this->_collectionNegative) {
            $this->_collectionNegative = $this->transactionCollectionFactory->create()
                ->addFieldToFilter('customer_id', $this->getCustomer()->getId())
                ->addFieldToFilter('amount', [[self::NEGATIVE => 0]])
                ->setOrder('created_at', 'desc');
        }

        return $this->_collectionNegative;
    }

    /**
     * Returns collection for data array
     *
     * @return array
     */
    public function getTransactionCollectionArray()
    {
       $collections = [
           'Earning' => $this->getTransactionCollectionPositive(),
           'Spendings' => $this->getTransactionCollectionNegative()
       ];

        return $collections;
    }

    /**
     * This is needed to take the correct templates
     *
     * @return string
     */
    protected function _toHtml()
    {
        $this->setModuleName($this->extractModuleName('Mirasvit\Rewards\Block\Account\Listing'));

        return parent::_toHtml();
    }
}
