<?php
/**
 * @vendor      Kurufootwear
 * @module      Kurufootwear_Rewards
 * @author      Kristaps Stalidzans <info@scandiweb.com>
 * @copyright   Copyright (c) 2017 Scandiweb. All rights reserved.
 * @license     http://opensource.org/licenses/afl-3.0.php Academic Free License (AFL 3.0)
 */
namespace Kurufootwear\Rewards\Block\Referral;

class Listing extends \Mirasvit\Rewards\Block\Referral\Listing
{

    /**
     * Returns referral code for client
     *
     * @return string
     */
    public function getReferralCode()
    {
        return $this->helper->getReferralLinkId();
    }

    /**
     * Returns Customer email
     *
     * @return string
     */
    public function getCustomerEmail()
    {
        return $this->customerSession->getCustomer()->getEmail();
    }

    /**
     * This is needed to take the correct templates
     *
     * @return string
     */
    protected function _toHtml()
    {
        $this->setModuleName($this->extractModuleName('Mirasvit\Rewards\Block\Referral\Listing'));

        return parent::_toHtml();
    }
}
