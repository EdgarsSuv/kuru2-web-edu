<?php
/**
 * @vendor      Kurufootwear
 * @module      Kurufootwear_Rewards
 * @author      Krisjanis Veinbahs <krisjanisv@scandiweb.com>
 * @copyright   Copyright (c) 2017 Scandiweb. All rights reserved.
 * @license     http://opensource.org/licenses/afl-3.0.php Academic Free License (AFL 3.0)
 */

namespace Kurufootwear\Rewards\Block\Checkout\Cart;

use Mirasvit\Rewards\Block\Checkout\Cart\Tooltip as MirasvitTooltip;

class Tooltip extends MirasvitTooltip
{
    /**
     * @return $this
     */
    protected function _prepareLayout()
    {
        /**
         * Don't print Mirasvit Reward points messages,
         * because they're not responsive to cart changes
         */
        return $this;
    }
}
