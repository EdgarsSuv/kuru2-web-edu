<?php
/**
 * @vendor      Kurufootwear
 * @module      Kurufootwear_Rewards
 * @author      Krisjanis Veinbahs <info@scandiweb.com>
 * @copyright   Copyright (c) 2018 KURU Footwear. All rights reserved.
 */
namespace Kurufootwear\Rewards\Helper;

use Magento\Email\Model\TemplateFactory;
use Magento\Framework\App\Helper\Context;
use Magento\Framework\App\ResourceConnection;
use Magento\Framework\App\State as AppState;
use Magento\Framework\Exception\MailException;
use Magento\Framework\Filesystem;
use Magento\Framework\Mail\Template\TransportBuilder;
use Magento\Framework\Translate\Inline\StateInterface;
use Magento\Framework\View\Asset\Repository;
use Magento\Store\Model\StoreManagerInterface;
use Mirasvit\Rewards\Helper\Data;
use Mirasvit\Rewards\Helper\Mail as MirasvitMail;
use Mirasvit\Rewards\Helper\ReferralFactory as ReferralHelperFactory;
use Mirasvit\Rewards\Model\Config;
use Mirasvit\Rewards\Model\Referral;

/**
 * Class Mail
 *
 * Overridden to use the same referral URL in referral invite emails
 * as the one copy-able in customer pages for easier maintenance.
 *
 * @package Kurufootwear\Rewards\Helper
 */
class Mail extends MirasvitMail
{
    /**
     * @var ReferralHelperFactory
     */
    protected $referralHelperFactory;

    /**
     * Mail constructor.
     * @param AppState $appState
     * @param TemplateFactory $emailTemplateFactory
     * @param TransportBuilder $transportBuilder
     * @param Config $config
     * @param Data $rewardsData
     * @param StoreManagerInterface $storeManager
     * @param Repository $assetRepo
     * @param Filesystem $filesystem
     * @param StateInterface $inlineTranslation
     * @param ResourceConnection $resource
     * @param Context $context
     * @param ReferralHelperFactory $referralHelperFactory
     */
    public function __construct(
        AppState $appState,
        TemplateFactory $emailTemplateFactory,
        TransportBuilder $transportBuilder,
        Config $config,
        Data $rewardsData,
        StoreManagerInterface $storeManager,
        Repository $assetRepo,
        Filesystem $filesystem,
        StateInterface $inlineTranslation,
        ResourceConnection $resource,
        Context $context,
        ReferralHelperFactory $referralHelperFactory
    ) {
        $this->referralHelperFactory = $referralHelperFactory;

        parent::__construct(
            $appState,
            $emailTemplateFactory,
            $transportBuilder,
            $config,
            $rewardsData,
            $storeManager,
            $assetRepo,
            $filesystem,
            $inlineTranslation,
            $resource,
            $context
        );
    }

    /**
     * Override Mirasvit referral invite mail to use
     * the same referral URL as listed in customer pages not
     * the custom "invite" URL. There's no reason to maintain two
     * referral controllers as the functionality should be the same.
     *
     * @param Referral $referral
     * @param string $message
     * @throws MailException
     *
     * @return bool
     */
    public function sendReferralInvitationEmail($referral, $message) : bool
    {
        $store = $referral ? $referral->getData('store_id') : null;
        $templateName = $this->getConfig()->getReferralInvitationEmailTemplate($store);
        if ($templateName === 'none') {
            return false;
        }
        $recipientEmail = $referral->getEmail();
        $recipientName  = $referral->getName();
        $storeId  = $referral->getStoreId();
        $customer = $referral->getCustomer();
        $referralUrlPath = 'r/' . $this->getReferralHelper()->getReferralLinkId();
        $referralUrl = $this->_urlBuilder->getUrl($referralUrlPath);
        $variables = [
            'customer'       => $customer,
            'name'           => $referral->getName(),
            'message'        => $message,
            'invitation_url' => $referralUrl,
        ];
        $senderName = $this->context->getScopeConfig()->getValue("trans_email/ident_{$this->getSender()}/name");
        $senderEmail = $this->context->getScopeConfig()->getValue("trans_email/ident_{$this->getSender()}/email");

        return $this->send($templateName, $senderName, $senderEmail, $recipientEmail, $recipientName, $variables, $storeId);
    }

    /**
     * Referral helper must be instantiated on-demand to avoid circular
     * dependency exceptions at time of construct'ion
     *
     * @return mixed
     */
    public function getReferralHelper()
    {
        return $this->referralHelperFactory->create();
    }
}