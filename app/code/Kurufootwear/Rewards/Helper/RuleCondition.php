<?php
/**
 * @category    Kurufootwear
 * @package     Kurufootwear\Rma
 * @author      Jim McGowen <jim@kurufootwear.com>
 * @copyright   Copyright (c) 2018 KURU Footwear. All rights reserved.
 */

namespace Kurufootwear\Rewards\Helper;

use Magento\Framework\App\ObjectManager;
use Magento\Checkout\Model\Session;

/**
 * Class RuleCondition
 * @package Kurufootwear\Rewards\Helper
 */
class RuleCondition
{
    /** @var \Magento\Checkout\Model\Session */
    public static $session;
    
    /**
     * Static function for use in the backend reward point rules (Marketing > Reward Points).
     * Returns the discount code stored in the session or the quote if there is one. Use with 
     * a PHP Condition like so:
     * 
     *      "xxx" == \Kurufootwear\Rewards\Helper\RuleCondition::discountCode()
     * 
     * where "xxx" is the discount code created normally (Marketing > Cart Price Rules).
     * 
     * Relies on (but does not require) the Kurufootwear_DiscountCode module.
     * 
     * @return string
     */
    public static function discountCode()
    {
        if (self::$session === null) {
            // Have to use object manager directly since this is static
            self::$session = ObjectManager::getInstance()->get(Session::class);
        }
        
        $code = self::$session->getCouponCode();
        if (empty($code)) {
            $code = self::$session->getQuote()->getCouponCode();
        }
        
        return $code;
    }
}
