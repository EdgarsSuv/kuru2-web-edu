<?php
/**
 * @vendor      Kurufootwear
 * @module      Kurufootwear_Rewards
 * @author      Krisjanis Veinbahs <info@scandiweb.com>
 * @copyright   Copyright (c) 2018 KURU Footwear. All rights reserved.
 */
namespace Kurufootwear\Rewards\Helper;

use Magento\Customer\Model\Customer;
use Magento\Customer\Model\CustomerFactory;
use Magento\Customer\Model\ResourceModel\Customer as CustomerResource;
use Magento\Customer\Model\SessionFactory as CustomerSessionFactory;
use Magento\Framework\App\Helper\Context;
use Magento\Framework\App\ResourceConnection;
use Magento\Framework\App\State as AppState;
use Magento\Framework\Exception\LocalizedException;
use Magento\Framework\Exception\SessionException;
use Magento\Framework\Message\ManagerInterface;
use Magento\Framework\App\ObjectManager;
use Magento\Framework\Session\SessionManagerInterface;
use Magento\Framework\Stdlib\DateTime\DateTime;
use Magento\Store\Model\StoreManagerInterface;
use Mirasvit\Rewards\Helper\Balance;
use Mirasvit\Rewards\Helper\Behavior as MirasvitBehavior;
use Mirasvit\Rewards\Helper\Data;
use Mirasvit\Rewards\Model\Config;
use Mirasvit\Rewards\Model\Earning\Rule\QueueFactory;
use Mirasvit\Rewards\Model\ResourceModel\Earning\Rule\CollectionFactory as EarningRuleCollectionFactory;
use Mirasvit\Rewards\Model\ResourceModel\Transaction\CollectionFactory;
use Magento\Framework\DataObjectFactory;

/**
 * Class Behavior
 * Overridden to dispense points on registration with referral
 *
 * @package Kurufootwear\Rewards\Helper
 */
class Behavior extends MirasvitBehavior
{
    /**
     * @var CustomerSessionFactory
     */
    private $customerSessionFactory;

    /**
     * @var AppState
     */
    protected $appState;

    /**
     * @var CustomerResource
     */
    private $customerResource;

    /**
     * @var CustomerFactory
     */
    protected $customerFactory;

    /**
     * @var Balance
     */
    protected $rewardsBalance;

    /**
     * @var Config
     */
    protected $config;

    /**
     * Behavior constructor.
     *
     * @param AppState $appState
     * @param CustomerFactory $customerFactory
     * @param CollectionFactory $transactionCollectionFactory
     * @param EarningRuleCollectionFactory $earningRuleCollectionFactory
     * @param QueueFactory $earningRuleQueueFactory
     * @param ResourceConnection $resource
     * @param Balance $rewardsBalance
     * @param Data $rewardsData
     * @param Config $config
     * @param Context $context
     * @param DateTime $date
     * @param StoreManagerInterface $storeManager
     * @param ManagerInterface $messageManager
     * @param CustomerResource $customerResource
     * @param CustomerSessionFactory $customerSessionFactory
     */
    public function __construct(
        AppState $appState,
        CustomerFactory $customerFactory,
        CollectionFactory $transactionCollectionFactory,
        EarningRuleCollectionFactory $earningRuleCollectionFactory,
        QueueFactory $earningRuleQueueFactory,
        ResourceConnection $resource,
        Balance $rewardsBalance,
        Data $rewardsData,
        Config $config,
        Context $context,
        DateTime $date,
        StoreManagerInterface $storeManager,
        ManagerInterface $messageManager,
        CustomerResource $customerResource,
        CustomerSessionFactory $customerSessionFactory
    ) {
        $this->customerSessionFactory = $customerSessionFactory;
        $this->appState = $appState;
        $this->customerResource = $customerResource;
        $this->customerFactory = $customerFactory;
        $this->config = $config;
        $this->rewardsBalance = $rewardsBalance;

        parent::__construct(
            $appState,
            $customerFactory,
            $transactionCollectionFactory,
            $earningRuleCollectionFactory,
            $earningRuleQueueFactory,
            $resource,
            $rewardsBalance,
            $rewardsData,
            $config,
            $context,
            $date,
            $storeManager,
            $messageManager
        );
    }

    /**
     * Overridden to dispense points on registration with referral
     *
     * @param string $ruleType
     * @param bool $customerId
     * @param bool $websiteId
     * @param bool $code
     * @param array $options
     *
     * @throws \Magento\Framework\Exception\LocalizedException
     *
     * @return bool
     */
    public function processRule($ruleType, $customerId = false, $websiteId = false, $code = false, $options = [])
    {
        if ($code) {
            $code = $ruleType.'-'.$code;
        } else {
            $code = $ruleType;
        }

        if (!$customer = $this->getCustomer($customerId)) {
            return false;
        }

        if (!$this->checkIsAllowToProcessRule($customer->getId(), $code)) {
            return false;
        }

        $rules = $this->getRules($ruleType, $customer, $websiteId, $code);

        $lastTransaction = false;
        foreach ($rules as $rule) {
            /* @var Rule $rule */
            $rule->afterLoad();

            if (isset($options['referred_customer'])) {
                $customer->setReferredCustomer($options['referred_customer']);
            }

            /**
             * Overridden - Extend validation for registration with referral
             */
            $referralException = $this->isReferralRegistrationException($ruleType);
            if (!$rule->validate($customer) && !$referralException) {
                continue;
            }

            $total = $rule->getEarnPoints();
            if (isset($options['order'])) {
                /** @var Order $order */
                $order = $options['order'];
                switch ($rule->getEarningStyle()) {
                    case Config::EARNING_STYLE_AMOUNT_SPENT:
                        if ($this->config->getGeneralIsIncludeTaxEarning()) {
                            $subtotal = $order->getGrandTotal();
                        } else {
                            $subtotal = $order->getSubtotal();
                        }
                        $steps = (int) ($subtotal / $rule->getMonetaryStep());
                        $amount = $steps * $rule->getEarnPoints();
                        if ($rule->getPointsLimit() && $amount > $rule->getPointsLimit()) {
                            $amount = $rule->getPointsLimit();
                        }
                        $total = $amount;
                        break;
                    case Config::EARNING_STYLE_QTY_SPENT:
                        $steps = (int) ($order->getTotalQtyOrdered() / $rule->getQtyStep());
                        $amount = $steps * $rule->getEarnPoints();
                        if ($rule->getPointsLimit() && $amount > $rule->getPointsLimit()) {
                            $amount = $rule->getPointsLimit();
                        }
                        $total = $amount;
                        break;
                }
            }

            if (!$this->isInLimit($rule, $customer->getId(), $total)) {
                continue;
            }

            $lastTransaction = $this->rewardsBalance->changePointsBalance(
                $customer,
                $total,
                $rule->getHistoryMessage(),
                $code.'-'.$rule->getId(),
                true,
                $rule->getEmailMessage()
            );

            /**
             * Overridden - Don't emit message after registration
             */
            if ($lastTransaction && !$referralException) {
                $this->addSuccessMessage($rule->getEarnPoints(), $ruleType);
            }

            if ($rule->getIsStopProcessing()) {
                break;
            }
        }

        return $lastTransaction;
    }

    /**
     * Detect when a user is registering with a referral link
     * The default behavior invalidates this scenario.
     *
     * @param $ruleType
     *
     * @return bool
     */
    public function isReferralRegistrationException($ruleType)
    {
        // Should use ObjectManager to prevent area code CLI error during setup:upgrade
        $objectManager = ObjectManager::getInstance();
        $session = $objectManager->get(SessionManagerInterface::class);
        $referralId = (int) $session->getReferral();
        $isRegistration = $ruleType === Config::REFERRAL_STATUS_SIGNUP;

        return $referralId && $isRegistration;
    }

    /**
     * Get customer
     * Override to check for loaded customer existence
     *
     * @param object|int $customerId
     *
     * @return bool|Customer
     */
    protected function getCustomer($customerId)
    {
        if (\is_object($customerId)) {
            $customerId = $customerId->getId();
        }

        try {
            // If no customer ID try to get it from customer session
            if (!$customerId && $this->appState->getAreaCode() === 'frontend') {
                // Should use factory or ObjectManager to prevent area code CLI error
                $customerId = $this->customerSessionFactory->create()->getCustomerId();
            }
        } catch (SessionException $e) {
            $this->_logger->error('Session exception when trying to get customer session on frontend' . $e->getMessage());
        } catch (LocalizedException $e) {
            $this->_logger->error('Area code is not set when trying to get customer session on frontend' . $e->getMessage());
        }

        if (!$customerId || is_null($customerId)) {
            return false;
        }

        $customerModel = $this->customerFactory->create();

        // Try to load customer using customer resource model
        $this->customerResource->load($customerModel, $customerId, 'entity_id');

        // If no customer return false and do not proceed with changing points balance
        if (!$customerModel->getId()) {
            return false;
        }

        return $customerModel;
    }
}