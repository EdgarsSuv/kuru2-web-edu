<?php
/**
 * @vendor      Kurufootwear
 * @module      Kurufootwear_Rewards
 * @author      Kristaps Stalidzans <info@scandiweb.com>
 * @copyright   Copyright (c) 2017 Scandiweb. All rights reserved.
 * @license     http://opensource.org/licenses/afl-3.0.php Academic Free License (AFL 3.0)
 */
namespace Kurufootwear\Rewards\Helper;

class Referral extends \Mirasvit\Rewards\Helper\Referral
{
    /**
     * @param \Magento\Customer\Model\Customer $customer
     * @param array                            $invitations
     * @param string                           $message
     * @return array
     */
    public function frontendPost($customer, $invitations, $message)
    {
        $rejectedEmails = [];
        foreach ($invitations as $email) {
            $referrals = $this->referralCollectionFactory->create()
                ->addFieldToFilter('email', $email);
            if ($referrals->count()) {
                $rejectedEmails[] = $email;
                continue;
            }

            $message = nl2br(strip_tags($message));

            /** @var  \Mirasvit\Rewards\Model\Referral $referral */
            $referral = $this->referralFactory->create()
                ->setName('Guest')
                ->setEmail($email)
                ->setCustomerId($customer->getId())
                ->setStoreId($this->storeManager->getStore()->getId())
                ->save();
            $referral->sendInvitation($message);
        }

        return $rejectedEmails;
    }
}
