<?php
/**
 * @category     Kurufootwear
 * @package      Kurufootwear/Balance
 * @author       Krisjanis Veinbahs <krisjanisv@scandiweb.com>
 * @copyright    Copyright (c) 2018 KURU Footwear
 */

namespace Kurufootwear\Rewards\Helper\Balance;

use Magento\Store\Model\StoreFactory;
use Mirasvit\Rewards\Helper\Balance\Order as MirasvitOrderBalance;
use Mirasvit\Rewards\Model\ResourceModel\Purchase\CollectionFactory;

/**
 * Class Order
 *
 * Overridden to make points cancelling more fail-safe
 *
 * @package Kurufootwear\Rewards\Helper\Balance
 */
class Order extends MirasvitOrderBalance
{
    /**
     * Cancels earned points.
     *
     * Overridden to handle balance going into negatives
     *
     * @param \Magento\Sales\Model\Order $order
     * @param \Magento\Sales\Model\Order\Creditmemo $creditMemo
     *
     * @return bool
     */
    public function cancelEarnedPoints($order, $creditMemo)
    {
        if (!$earnedTransaction = $this->getEarnedPointsTransaction($order)) {
            return false;
        }
        $proportion = $creditMemo->getSubtotal() / $order->getSubtotal();
        if ($proportion > 1) {
            $proportion = 1;
        }
        $creditMemoId = $order->getCreditmemosCollection()->count();
        $totalPoints = round($earnedTransaction->getAmount() * $proportion);

        /**
         * Override when cancelling points would go into negatives
         *
         * Handle it either by only setting balance to zero
         * or if balance is already negative by doing nothing
         */
        $customerId = $order->getCustomerId();
        if ($customerId && $balance = $this->rewardsBalance->getBalancePoints($customerId)) {
            $balanceChange = $balance - $totalPoints;

            if ($balance < 0) {
                $this->_logger->warning(sprintf(
                    __("There's a customer with negative balance trying to go even more into negatives. Ignoring transaction. Customer #%s, order #%s"),
                    $customerId,
                    $order->getId()
                ));

                return false;
            }

            if ($balance > 0 && $balanceChange < 0) {
                /**
                 * Balance is positive, but trying to go into negatives
                 * Customer has probably already spent some of the earned points
                 * Remove only as many points to get to zero balance.
                 */
                $this->_logger->warning(sprintf(
                    __("Some earned points have already been spent, can't remove anymore. Altering transaction. Customer #%s, order #%s"),
                    $customerId,
                    $order->getId()
                ));
                $totalPoints = $balance;
            }
        }

        $this->rewardsData->setCurrentStore($this->getStoreByOrder($order));
        $this->rewardsBalance->changePointsBalance($order->getCustomerId(), -$totalPoints,
            $this->translateComment(
                $order->getStore()->getId(),
                'Cancel earned %1 for the order #%2.',
                $this->rewardsData->formatPoints($totalPoints),
                $order->getIncrementId()
            ),
            'order_earn_cancel-'.$order->getId().'-'.$creditMemoId, false);
    }
}