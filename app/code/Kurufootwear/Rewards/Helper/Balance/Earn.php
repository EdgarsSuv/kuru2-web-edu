<?php
/**
 * @category     Kurufootwear
 * @package      Kurufootwear/Label
 * @author       Jim McGowen <jim@kurufootwear.com>
 * @copyright    Copyright (c)2018 KURU Footwear
 */

namespace Kurufootwear\Rewards\Helper\Balance;

use Magento\CatalogInventory\Api\StockRegistryInterface;
use Magento\Checkout\Model\CartFactory;
use Magento\Catalog\Model\ProductFactory;
use Magento\Quote\Model\Quote;
use Mirasvit\Rewards\Model\ResourceModel\Earning\Rule\CollectionFactory as RuleCollectionFactory;
use Mirasvit\Rewards\Model\Config;
use Magento\Catalog\Helper\Data as CatalogData;
use Magento\Store\Model\StoreManagerInterface;
use Magento\Customer\Model\CustomerFactory;
use Magento\Customer\Model\Session;
use Magento\Framework\App\Helper\Context;
use Magento\Catalog\Api\ProductRepositoryInterface;

/**
 * @SuppressWarnings(PHPMD.CouplingBetweenObjects)
 */
class Earn extends \Mirasvit\Rewards\Helper\Balance\Earn
{
    protected $productRepository;
    
    /**
     * Earn constructor.
     * 
     * Overridden to add ProductRepositoryInterface.
     *
     * @param StockRegistryInterface $stockRegistry
     * @param CartFactory $cartFactory
     * @param ProductFactory $productFactory
     * @param RuleCollectionFactory $earningRuleCollectionFactory
     * @param Config $config
     * @param CatalogData $catalogData
     * @param StoreManagerInterface $storeManager
     * @param CustomerFactory $customerFactory
     * @param Session $customerSession
     * @param Context $context
     * @param ProductRepositoryInterface $productRepository
     */
    public function __construct(
        StockRegistryInterface $stockRegistry,
        CartFactory $cartFactory,
        ProductFactory $productFactory,
        RuleCollectionFactory $earningRuleCollectionFactory,
        Config $config,
        CatalogData $catalogData,
        StoreManagerInterface $storeManager,
        CustomerFactory $customerFactory,
        Session $customerSession,
        Context $context,
        ProductRepositoryInterface $productRepository
    ) {
        $this->productRepository = $productRepository;
        
        parent::__construct(
            $stockRegistry, 
            $cartFactory, 
            $productFactory, 
            $earningRuleCollectionFactory,
            $config,
            $catalogData,
            $storeManager,
            $customerFactory,
            $customerSession,
            $context
        );
    }
    
    /**
     * Overridden to use parent products from the quote (instead of 
     * grandparents).
     * 
     * @param Quote $quote
     *
     * @return float|int
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     */
    public function getPointsEarned($quote)
    {
        $totalPoints = 0;
        /** @var \Magento\Quote\Model\Quote\Item $item */
        foreach ($quote->getAllItems() as $item) {
            if ($item->getParentItemId() && $item->getProduct()->getTypeID() == 'simple') {
                continue;
            }
    
            // Get the parent product from the simple
            $simple = $this->productRepository->get($item->getSku());
            $product = $simple->getParent();

            // Do not proceed with getting product points using parent product, use simple instead
            if (!$product) {
                $product = $simple;
            }

            $productPoints = $this->getProductPoints(
                    $product,
                    $quote->getCustomerGroupId(),
                    $quote->getStore()->getWebsiteId(),
                    $item
                );

            $totalPoints += $productPoints;
        }

        $totalPoints += $this->getCartPoints($quote);

        return $this->roundPoints($totalPoints);
    }
}
