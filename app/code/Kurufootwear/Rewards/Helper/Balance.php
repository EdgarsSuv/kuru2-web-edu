<?php
/**
 * @category     Kurufootwear
 * @package      Kurufootwear/Balance
 * @author       Krisjanis Veinbahs <krisjanisv@scandiweb.com>
 * @copyright    Copyright (c) 2018 KURU Footwear
 */

namespace Kurufootwear\Rewards\Helper;

use Exception;
use Magento\Customer\Model\Customer;
use Magento\Framework\App\Helper\Context;
use Magento\Framework\App\ResourceConnection;
use Mirasvit\Rewards\Helper\Balance as MirasvitBalance;
use Mirasvit\Rewards\Helper\Mail;
use Mirasvit\Rewards\Model\ResourceModel\Transaction\CollectionFactory;
use Mirasvit\Rewards\Model\TransactionFactory;
use Magento\Framework\App\State as AppState;

/**
 * Class Balance
 *
 *
 *
 * @package Kurufootwear\Rewards\Helper
 */
class Balance extends MirasvitBalance
{
    /**
     * @var TransactionFactory
     */
    protected $transactionFactory;

    /**
     * @var CollectionFactory
     */
    protected $transactionCollectionFactory;

    /**
     * @var ResourceConnection
     */
    protected $resource;

    /**
     * @var Mail
     */
    protected $rewardsMail;

    /**
     * @var Context
     */
    protected $context;

    /**
     * @var AppState
     */
    protected $appState;

    /**
     * Balance constructor.
     * @param TransactionFactory $transactionFactory
     * @param CollectionFactory $transactionCollectionFactory
     * @param ResourceConnection $resource
     * @param Mail $rewardsMail
     * @param Context $context
     * @param AppState $appState
     */
    public function __construct(
        TransactionFactory $transactionFactory,
        CollectionFactory $transactionCollectionFactory,
        ResourceConnection $resource,
        Mail $rewardsMail,
        Context $context,
        AppState $appState
    ) {
        $this->transactionFactory = $transactionFactory;
        $this->transactionCollectionFactory = $transactionCollectionFactory;
        $this->resource = $resource;
        $this->rewardsMail = $rewardsMail;
        $this->context = $context;
        $this->appState = $appState;

        parent::__construct(
            $transactionFactory,
            $transactionCollectionFactory,
            $resource,
            $rewardsMail,
            $context
        );
    }

    /**
     * Change the number of points on the customer balance
     * Overridden to throw exception or log occurrence when making balance negative
     *
     * @param Customer $customer
     * @param int $pointsNum
     * @param string $historyMessage
     * @param bool $code
     * @param bool $notifyByEmail
     * @param bool $emailMessage
     *
     * @return bool
     * @throws Exception
     */
    public function changePointsBalance(
        $customer, $pointsNum, $historyMessage, $code = false, $notifyByEmail = false, $emailMessage = false
    ) {
        if (is_object($customer)) {
            $customer = $customer->getId();
        }

        /**
         * Validate code uniqueness
         */
        if ($code) {
            $collection = $this->transactionCollectionFactory->create()
                ->addFieldToFilter('customer_id', $customer)
                ->addFieldToFilter('code', $code);

            if ($collection->count()) {
                return false;
            }
        }

        /**
         * If balance change requested from frontend, validate next balance won't be negative
         * (We don't want customers having negative points)
         */
        try {
            $isBackend = $this->appState->getAreaCode() === 'adminhtml';
        } catch (\Exception $exception) {
            $isBackend = true;
        }
        $balance = $this->getBalancePoints($customer);

        if ($pointsNum < 0 && $balance + $pointsNum < 0) {
            if ($isBackend) {
                $warnMessage = sprintf(
                    __("Customer rewards balance changed into negatives in BE. Customer #%s.\nStack trace: %s."),
                    $customer,
                    print_r(debug_backtrace(), true)
                );
                $this->_logger->warning($warnMessage);
            } else {
                $warnMessage = sprintf(
                    __("FE tried to change points balance into negatives. Customer #%s.\nStack trace: %s."),
                    $customer,
                    print_r(debug_backtrace(), true)
                );
                $this->_logger->warning($warnMessage);


                throw new Exception("Can't change points balance into a negative value.");
            }
        }

        /**
         * Generate and enact on transaction
         */
        $transaction = $this->transactionFactory->create()
            ->setCustomerId($customer)
            ->setAmount($pointsNum);
        if ($code) {
            $transaction->setCode($code);
        }
        $historyMessage = $this->rewardsMail->parseVariables($historyMessage, $transaction);
        $transaction->setComment($historyMessage);
        $transaction->save();
        if ($notifyByEmail) {
            $this->rewardsMail->sendNotificationBalanceUpdateEmail($transaction, $emailMessage);
        }

        return $transaction;
    }
}
