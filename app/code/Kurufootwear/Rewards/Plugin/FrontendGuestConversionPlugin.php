<?php
/**
 * @category    Kurufootwear
 * @package     Kurufootwear\Rewards
 * @author      Krisjanis Veinbahs <krisjanisv@scandiweb.com>
 * @copyright   Copyright (c) 2017 KURU Footwear. All rights reserved.
 */

namespace Kurufootwear\Rewards\Plugin;

use Magento\Customer\Api\CustomerRepositoryInterface;
use Magento\Framework\Exception\NoSuchEntityException;
use Magento\Framework\Message\ManagerInterface;
use Mirasvit\Rewards\Helper\Balance;
use Mirasvit\Rewards\Helper\Purchase;
use CommerceExtensions\GuestToReg\Observer\SubmitAllAfter;
use Magento\Framework\Event\Observer as EventObserver;
use Closure;
use Mirasvit\Rewards\Model\ResourceModel\Transaction\CollectionFactory;

/**
 * Class FrontendGuestConversionPlugin
 * (Wraps CommerceExtension's FE guest conversion method)
 *
 * Dispense points when guests are converted to registered customers
 * on finished successful order event in FE.
 *
 * @package Kurufootwear\Rewards\Plugin
 */
class FrontendGuestConversionPlugin
{
    /**
     * @var Purchase
     */
    protected $rewardsPurchase;

    /**
     * @var Balance
     */
    protected $rewardsPointsHelper;

    /**
     * @var ManagerInterface
     */
    protected $messageManager;

    /**
     * @var CustomerRepositoryInterface
     */
    protected $customerRepository;

    /**
     * @var CollectionFactory
     */
    protected $transactionCollectionFactory;

    /**
     * GuestOrderSuccess constructor.
     * @param Purchase $rewardsPurchase
     * @param Balance $rewardsPointsHelper
     * @param ManagerInterface $messageManager
     */
    public function __construct(
        Purchase $rewardsPurchase,
        Balance $rewardsPointsHelper,
        ManagerInterface $messageManager,
        CustomerRepositoryInterface $customerRepository,
        CollectionFactory $transactionCollectionFactory
    )
    {
        $this->rewardsPurchase = $rewardsPurchase;
        $this->rewardsPointsHelper = $rewardsPointsHelper;
        $this->messageManager = $messageManager;
        $this->customerRepository = $customerRepository;
        $this->transactionCollectionFactory = $transactionCollectionFactory;
    }

    /**
     * Plugin to issue rewards points after converting a guest customer
     * to a registered customer after a newly created frontend order.
     *
     * @param SubmitAllAfter $subject
     * @param Closure $proceed
     * @param EventObserver $orderSuccessObserver
     * @return mixed
     */
    public function aroundExecute(
        SubmitAllAfter $subject,
        Closure $proceed,
        EventObserver $orderSuccessObserver
    )
    {
        // First run the guest conversion process
        $conversionResult = $proceed($orderSuccessObserver);

        // Try to retrieve the new customer and created quote/order
        $quote = $orderSuccessObserver->getEvent()->getQuote();
        $order = $orderSuccessObserver->getEvent()->getOrder(); // May be null

        /**
         * Check if points can/should be dispensed
         */

        // Assert customer was converted (created & existing)
        $convertedCustomer = $this->convertedCustomer($quote);
        if (!$convertedCustomer) {
            return $conversionResult;
        }

        // Assert order was successful (not null)
        if (is_null($order)) {
            return $conversionResult;
        }

        // Assert points have been dispensed before conversion by Mirasvit internally
        if (!$this->mirasvitDispensedPoints($order)) {
            return $conversionResult;
        }

        // Assert that points haven't been dispensed already previously by guest reward plugins
        if ($this->guestPluginDispensedPoints($convertedCustomer)) {
            return $conversionResult;
        }

        /**
         * Dispense points for the order
         */
        $rewardsPurchase = $this->rewardsPurchase->getByQuote($quote);
        $this->rewardsPointsHelper->changePointsBalance(
            $convertedCustomer,
            $rewardsPurchase->getEarnPoints(),
            BackendGuestConversionPlugin::GUEST_POINTS_MESSAGE
        );

        return $conversionResult;
    }

    /**
     * Assert a registered customer was created from the guest purchase
     *
     * The customer could be not created if:
     *  Conversion function failed for some reason
     *  Guest customer conversion on successful order creation is disabled
     *
     *  The configuration in question:
     *      Path - BE -> Stores -> Configuration -> Commerce Extensions -> Guest To Customers
     *      Name - "Enable Automatic Guest to Customer Account At Checkout"
     *
     * @param $quote
     * @return bool|\Magento\Customer\Api\Data\CustomerInterface
     */
    protected function convertedCustomer($quote)
    {
        try {
            $customer = $this->customerRepository->get($quote->getCustomerEmail());
        } catch (NoSuchEntityException $entityException) {
            return false;
        }

        if (is_null($customer->getId())) {
            $quoteId = $quote->getId();
            $warningMessage = "Couldn't issue points to new converted customer (frontend order success), " .
                "because the conversion itself failed. " .
                "For reference, quote id - ${quoteId}.";
            $this->messageManager->addWarning($warningMessage);

            return false;
        }

        return $customer;
    }

    /**
     * Assert the order already had points dispensed before conversion.
     * (If the points were dispensed before the conversion, they need to be reassigned to the newly created customer)
     *
     * Note: This function supposes points have been dispensed if there's a paid invoice,
     *      however, Mirasvit configurations also allow auto-dispensing points on order shipping event (not handled)
     *
     * @param $order
     * @return bool
     */
    protected function mirasvitDispensedPoints($order)
    {
        // Check if there's a paid invoice
        $orderInvoiceCollection = $order->getInvoiceCollection();
        $isPaid = false;

        foreach ($orderInvoiceCollection as $invoice) {
            if ($invoice->getState() == \Magento\Sales\Model\Order\Invoice::STATE_PAID) {
                $isPaid = true;
            }
        }

        return $isPaid;
    }

    /**
     * Check if the guest conversion reward plugins have already dispensed points
     *
     * @param $customer
     * @return bool
     */
    protected function guestPluginDispensedPoints($customer)
    {
        $frontendConversionRewardTransactions = $this->transactionCollectionFactory->create()
            ->addFieldToFilter('customer_id', $customer->getId())
            ->addFieldToFilter('comment', BackendGuestConversionPlugin::GUEST_POINTS_MESSAGE);

        return $frontendConversionRewardTransactions->count() > 0;
    }

}