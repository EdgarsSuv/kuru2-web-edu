<?php
/**
 * @vendor      Kurufootwear
 * @module      Kurufootwear_Rewards
 * @author      Krisjanis Veinbahs <info@scandiweb.com>
 * @copyright   Copyright (c) 2017 Scandiweb. All rights reserved.
 * @license     http://opensource.org/licenses/afl-3.0.php Academic Free License (AFL 3.0)
 */
namespace Kurufootwear\Rewards\Plugin;

use Magento\Framework\Model\AbstractModel;
use Mirasvit\Rewards\Model\Earning\Rule\Condition\Referred\Customer;

/**
 * Class CustomerPlugin
 *
 * Override to fix Mirasvit customer referral default condition value
 *
 * @package Kurufootwear\Rewards\Plugin
 */
class CustomerPlugin
{
    /**
     * Override to fix Mirasvit customer referral default condition value
     *
     * @param Customer $subject
     * @param callable $proceed
     * @param AbstractModel $object
     *
     * @return bool
     */
    public function aroundValidate(
        Customer $subject,
        callable $proceed,
        AbstractModel $object
    ) {
        /**
         * If the user hasn't used a referral link,
         * the validation defaults to true, when it shouldn't
         */
        if (!$object->getReferredCustomer()) {
            return false;
        }

        return $proceed($object);
    }
}