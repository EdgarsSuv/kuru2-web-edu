<?php
/**
 * @vendor      Kurufootwear
 * @module      Kurufootwear_Rewards
 * @author      Krisjanis Veinbahs <info@scandiweb.com>
 * @copyright   Copyright (c) 2017 Scandiweb. All rights reserved.
 * @license     http://opensource.org/licenses/afl-3.0.php Academic Free License (AFL 3.0)
 */
namespace Kurufootwear\Rewards\Plugin;

use Magento\Customer\Api\Data\CustomerInterface;
use Magento\Framework\Exception\NoSuchEntityException;
use CommerceExtensions\GuestToReg\Controller\Adminhtml\GuestToRegForm\massConvert;
use Magento\Customer\Api\CustomerRepositoryInterface;
use Magento\Framework\Message\ManagerInterface;
use Magento\Sales\Model\OrderRepository;
use Mirasvit\Rewards\Helper\Balance;
use Mirasvit\Rewards\Helper\Purchase;
use Closure;
use Mirasvit\Rewards\Model\ResourceModel\Transaction\CollectionFactory;
use Psr\Log\LoggerInterface;
use Zend_Db_Statement_Exception;

/**
 * Class BackendGuestConversionPlugin
 * (Wraps CommerceExtension's BE guest conversion method)
 *
 * Dispense points when guests are converted to registered customers
 * on the "Guests to Registered customers" backend page
 *
 * @package Kurufootwear\Rewards\Plugin
 */
class BackendGuestConversionPlugin
{
    /**
     * @var OrderRepository
     */
    protected $orderRepository;

    /**
     * @var Purchase
     */
    protected $rewardsPurchase;

    /**
     * @var Balance
     */
    protected $rewardsPointsHelper;

    /**
     * @var ManagerInterface
     */
    protected $messageManager;

    /**
     * @var CustomerRepositoryInterface
     */
    protected $customerRepository;

    /**
     * @var CollectionFactory
     */
    protected $transactionCollectionFactory;

    /**
     * @var LoggerInterface
     */
    protected $logger;

    /**
     * Rewards message attached to the transaction on
     * guest to registered customer transformation event
     */
    const GUEST_POINTS_MESSAGE = 'Points issued for previous purchases as guest';

    /**
     * BackendGuestConversionPlugin constructor.
     *
     * @param OrderRepository $orderRepository
     * @param Purchase $rewardsPurchase
     * @param Balance $rewardsPointsHelper
     * @param ManagerInterface $messageManager
     * @param CustomerRepositoryInterface $customerRepository
     * @param CollectionFactory $transactionCollectionFactory
     * @param LoggerInterface $logger
     */
    public function __construct(
        OrderRepository $orderRepository,
        Purchase $rewardsPurchase,
        Balance $rewardsPointsHelper,
        ManagerInterface $messageManager,
        CustomerRepositoryInterface $customerRepository,
        CollectionFactory $transactionCollectionFactory,
        LoggerInterface $logger
    )
    {
        $this->orderRepository = $orderRepository;
        $this->rewardsPurchase = $rewardsPurchase;
        $this->rewardsPointsHelper = $rewardsPointsHelper;
        $this->messageManager = $messageManager;
        $this->customerRepository = $customerRepository;
        $this->transactionCollectionFactory = $transactionCollectionFactory;
        $this->logger = $logger;
    }

    /**
     * Plugin to issue rewards points after converting a guest customer
     * to a registered customer through admin panel.
     *
     * @param massConvert $subject
     * @param Closure $proceed
     * @param null $orderId
     * @param null $groupId
     * @param bool $isMass
     * @return massConvert
     */
    public function aroundConvertAction(
        massConvert $subject,
        Closure $proceed,
        $orderId = null,
        $groupId = null,
        $isMass = false
    )
    {
        // First run the guest conversion process
        $conversionResult = $proceed($orderId, $groupId, $isMass);

        $order = $this->orderRepository->get($orderId);

        /**
         * Check if points can/should be dispensed
         */

        // Assert customer was converted (created & existing)
        $convertedCustomer = $this->convertedCustomer($order);
        if (!$convertedCustomer) {
            return $conversionResult;
        }

        // Assert points have been dispensed before conversion by Mirasvit internally
        if (!$this->mirasvitDispensedPoints($order)) {
            return $conversionResult;
        }

        // Assert that points haven't been dispensed already previously by guest reward plugins
        if ($this->guestPluginDispensedPoints($convertedCustomer)) {
            return $conversionResult;
        }

        /**
         * Dispense points for the order
         */
        try {
            /**
             * Soft typehinting interfaces as implementations because of IDE
             * being unhappy about Mirasvit type requirements
             *
             * @var \Magento\Customer\Model\Customer $convertedCustomer
             * @var \Magento\Sales\Model\Order $order
             */
            $rewardsPurchase = $this->rewardsPurchase->getByOrder($order);

            $this->rewardsPointsHelper->changePointsBalance(
                $convertedCustomer,
                $rewardsPurchase->getEarnPoints(),
                self::GUEST_POINTS_MESSAGE
            );
        } catch (Zend_Db_Statement_Exception $databaseStatementException) {
            $this->logger->error(
                sprintf(
                    "Failed to dispense points in guest to registered conversion, order #%s.\nException: %s",
                    $orderId,
                    $databaseStatementException
                )
            );
        }

        return $conversionResult;
    }

    /**
     * Assert a registered customer was created from the guest purchase
     * (False only if conversion itself failed)
     *
     * @param $order
     * @throws \Magento\Framework\Exception\LocalizedException
     *
     * @return bool|CustomerInterface
     */
    protected function convertedCustomer($order)
    {
        try {
            $customer = $this->customerRepository->get($order->getCustomerEmail());
        } catch (NoSuchEntityException $entityException) {
            return false;
        }

        if (is_null($customer->getId())) {
            $orderId = $order->getId();
            $warningMessage = "Couldn't issue points to new converted customer (BE guest conversion), " .
                "because the conversion itself failed. " .
                "For reference, order id - ${orderId}.";
            $this->messageManager->addWarning($warningMessage);

            return false;
        }

        return $customer;
    }

    /**
     * Assert the order already had points dispensed before conversion.
     * (If the points were dispensed before the conversion, they need to be reassigned to the newly created customer)
     *
     * Note: This function supposes points have been dispensed if there's a paid invoice,
     *      however, Mirasvit configurations also allow auto-dispensing points on order shipping event (not handled)
     *
     * @param $order
     * @return bool
     */
    protected function mirasvitDispensedPoints($order)
    {
        // Check if there's a paid invoice
        $orderInvoiceCollection = $order->getInvoiceCollection();
        $isPaid = false;

        foreach ($orderInvoiceCollection as $invoice) {
            if ($invoice->getState() == \Magento\Sales\Model\Order\Invoice::STATE_PAID) {
                $isPaid = true;
            }
        }

        return $isPaid;
    }

    /**
     * Check if the guest conversion reward plugins have already dispensed points
     *
     * @param $customer
     * @return bool
     */
    protected function guestPluginDispensedPoints($customer)
    {
        $frontendConversionRewardTransactions = $this->transactionCollectionFactory->create()
            ->addFieldToFilter('customer_id', $customer->getId())
            ->addFieldToFilter('comment', self::GUEST_POINTS_MESSAGE);

        return $frontendConversionRewardTransactions->count() > 0;
    }
}