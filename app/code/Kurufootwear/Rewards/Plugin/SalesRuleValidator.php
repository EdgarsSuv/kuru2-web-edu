<?php
/**
 * @vendor Kurufootwear
 * @module Kurufootwear_Rewards
 * @author Aleksejs Baranovs <info@scandiweb.com>
 * @copyright Copyright (c) 2018 KURU Footwear. All rights reserved.
 */

namespace Kurufootwear\Rewards\Plugin;

use Mirasvit\Rewards\Plugin\SalesRuleValidator as BaseSalesRuleValidator;
use Magento\Quote\Model\Quote\Item\AbstractItem;
use Magento\SalesRule\Model\Validator;

/**
 * Class SalesRuleValidator
 * @package Kurufootwear\Rewards\Plugin
 */
class SalesRuleValidator extends BaseSalesRuleValidator
{
    /**
     * Around sales rule validator process
     *
     * @param Validator     $validator
     * @param callable     $proceed
     * @param AbstractItem  $item
     *
     * @return string
     */
    public function aroundProcess(Validator $validator, $proceed, AbstractItem $item)
    {
        $returnValue = $proceed($item);

        if ($returnValue && $this->config->getCalculateTotalFlag()) {
            $this->process($item);
        }

        return $returnValue;
    }

    /**
     * Process mirasvit reward rule
     * Override to calculate quote total not from the address entity, but counting all products row total with tax
     *
     * @param AbstractItem $item
     *
     * @return $this
     */
    private function process(AbstractItem $item)
    {
        $quote = $item->getQuote();
        $address = $item->getAddress();

        if (!$this->canProcess($item)) {
            return $this;
        }

        $purchase = $this->rewardsPurchase->getByQuote($quote);
        $spendAmount = $purchase->getSpendAmount();

        //price with TAX
        $itemTotalPrice = $item->getData('row_total_incl_tax') + $item->getData('weee_tax_applied_row_amount');

        // Override to calculate quote total not from the address entity, but counting all products row total with tax
        $total = 0;

        foreach ($quote->getAllVisibleItems() as $purchasedItem) {
            $total += $purchasedItem->getRowTotalInclTax() + $purchasedItem->getWeeeTaxAppliedRowAmount();
        }

        //protection from division on zero
        if ($itemTotalPrice == 0 || $total == 0) {
            return $this;
        }

        //now we need to check: should we make order grand total = 0.01 or not.
        if (!$this->config->getGeneralIsAllowZeroOrders()) {
            $priceIncludesTax = $this->taxData->priceIncludesTax($quote->getStore());
            $addr = $quote->getShippingAddress();
            if ($priceIncludesTax) {
                $grandTotal = $addr->getBaseSubtotalInclTax() + $addr->getBaseShippingInclTax();
            } else {
                $grandTotal = $addr->getBaseSubtotal() + $addr->getBaseShippingAmount();
            }
            if ($grandTotal == $spendAmount) {
                $spendAmount -= 0.01;
            }
        }

        $discount = $itemTotalPrice / $total * $spendAmount;

        //price with TAX
        $baseItemTotalPrice = $item->getData('base_row_total_incl_tax');
        //price with TAX
        $baseTotal = $address->getBaseSubtotalInclTax();
        if (!$baseTotal) {
            $baseTotal = $total;
        }

        $baseSpendAmount = $spendAmount * $baseItemTotalPrice / $itemTotalPrice;
        $baseDiscount = $baseItemTotalPrice / $baseTotal * $baseSpendAmount;

        $item->setDiscountAmount($discount + $item->getDiscountAmount());
        $item->setBaseDiscountAmount($baseDiscount + $item->getBaseDiscountAmount());

        return $this;
    }

    /**
     * Check for if the item can be processed
     * Copied from the Mirasvit plugin because of the private method
     *
     * @param AbstractItem $item
     *
     * @return bool
     */
    private function canProcess(AbstractItem $item)
    {
        $quote = $item->getQuote();
        $address = $item->getAddress();

        if ($this->rewardsData->isMultiship($address)) {
            return false;
        }

        if (!$quote->getId()) {
            return false;
        }

        $purchase = $this->rewardsPurchase->getByQuote($quote);

        if (!$purchase->getSpendAmount()) {
            return false;
        }

        $spendAmount = $purchase->getSpendAmount();

        if ($spendAmount == 0) {
            return false;
        }

        return true;
    }
}
