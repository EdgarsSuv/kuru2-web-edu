<?php
/**
 * @vendor      Kurufootwear
 * @module      Kurufootwear_Rewards
 * @author      Krisjanis Veinbahs <info@scandiweb.com>
 * @copyright   Copyright (c) 2018 Scandiweb. All rights reserved.
 */
namespace Kurufootwear\Rewards\Plugin;

use Magento\Customer\Model\Context;
use Magento\Framework\App\Http\Context as HttpContext;
use Magento\Framework\App\Response\RedirectInterface;
use Magento\Framework\Message\ManagerInterface as MessageManagerInterface;
use Magento\Framework\Session\SessionManagerInterface;
use Mirasvit\Rewards\Controller\Referral\ReferralVisit;

class ReferralVisitPlugin
{
    /**
     * @var SessionManagerInterface
     */
    private $session;

    /**
     * @var MessageManagerInterface
     */
    private $messageManager;

    /**
     * @var RedirectInterface
     */
    private $redirect;

    /**
     * @var HttpContext
     */
    private $httpContext;

    /**
     * ReferralVisitPlugin constructor.
     * @param SessionManagerInterface $session
     * @param MessageManagerInterface $messageManager
     * @param RedirectInterface $redirect
     */
    public function __construct(
        SessionManagerInterface $session,
        MessageManagerInterface $messageManager,
        RedirectInterface $redirect,
        HttpContext $httpContext
    ) {
        $this->session = $session;
        $this->messageManager = $messageManager;
        $this->redirect = $redirect;
        $this->httpContext = $httpContext;
    }

    /**
     * @param ReferralVisit $subject
     * @param $result
     * @return mixed
     */
    public function afterExecute(
        ReferralVisit $subject,
        $result
    ) {
        $isLoggedIn = $this->httpContext->getValue(Context::CONTEXT_AUTH);
        $isReferral = $this->session->getReferral();

        if ($isReferral && !$isLoggedIn) {
            $this->messageManager->addSuccessMessage('To receive referral points, please register.');
            $this->redirect->redirect($subject->getResponse(), 'customer/account/create');

            return $subject->getResponse();
        }

        return $result;
    }
}