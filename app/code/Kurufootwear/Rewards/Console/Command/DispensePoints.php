<?php
/**
 * @vendor Kurufootwear
 * @module Kurufootwear_Rewards
 * @author Aleksejs Baranovs <info@scandiweb.com>
 * @copyright Copyright (c) 2018 KURU Footwear. All rights reserved.
 */

namespace Kurufootwear\Rewards\Console\Command;

use Exception;
use Magento\Framework\App\Area;
use Magento\Framework\Exception\LocalizedException;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Mirasvit\Rewards\Helper\Behavior;
use Magento\Newsletter\Model\ResourceModel\Subscriber\CollectionFactory as SubscriberCollection;
use Mirasvit\Rewards\Model\Config;
use Magento\Framework\App\State;

/**
 * Class DispensePoints
 * @package Kurufootwear\Rewards\Console\Command
 */
class DispensePoints extends Command
{
    /**
     * @var SubscriberCollection
     */
    private $subscriberCollection;

    /**
     * @var Behavior
     */
    private $rewardsBehavior;

    /**
     * @var State
     */
    private $appState;

    /**
     * DispensePoints constructor.
     *
     * @param Behavior $rewardsBehavior
     * @param SubscriberCollection $subscriberCollection
     * @param State $appState
     */
    public function __construct(
        Behavior $rewardsBehavior,
        SubscriberCollection $subscriberCollection,
        State $appState
    ) {
        $this->subscriberCollection = $subscriberCollection;
        $this->rewardsBehavior = $rewardsBehavior;
        $this->appState = $appState;

        parent::__construct();
    }

    /**
     * {@inheritdoc}
     */
    protected function configure()
    {
        $this->setName('kuru:dispense:points')
            ->setDescription('Dispense reward points for subscribed customers');

        parent::configure();
    }

    /**
     * Execute CLI task
     *
     * @param InputInterface $input
     * @param OutputInterface $output
     *
     * @return void
     */
    public function execute(InputInterface $input, OutputInterface $output)
    {
        try {
            $output->writeln('Started dispensing points');

            $this->dispensePointsForSubscribedCustomers();

            $output->writeln('Finished dispensing points');
        } catch (Exception $e) {
            echo $e->getMessage();
        } catch (LocalizedException $e) {
            echo  $e->getMessage();
        }
    }

    /**
     * Dispense points for already subscribed customers
     */
    private function dispensePointsForSubscribedCustomers()
    {
        try {
            $this->appState->getAreaCode();
        } catch (LocalizedException $e) {
            $this->appState->setAreaCode(Area::AREA_ADMINHTML);
        }

        $subscriberCollection = $this->subscriberCollection->create()
            ->useOnlyCustomers()
            ->useOnlySubscribed();

        foreach ($subscriberCollection as $subscriber) {
            $this->rewardsBehavior->processRule(Config::BEHAVIOR_TRIGGER_NEWSLETTER_SIGNUP, $subscriber->getCustomerId());
        }
    }
}
