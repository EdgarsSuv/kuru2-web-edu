<?php
/**
 * @vendor Kurufootwear
 * @module Kurufootwear_Upsell
 * @author Krisjanis Veinbahs <info@scandiweb.com>
 * @copyright Copyright (c) 2018 KURU Footwear. All rights reserved.
 */
namespace Kurufootwear\Upsell\Helper;

use Magento\Checkout\Helper\Cart as CartHelper;
use Magento\Framework\App\Helper\AbstractHelper;
use Magento\Framework\App\Helper\Context;
use Magento\Framework\Data\Helper\PostHelper;

/**
 * Class ProductPostHelper
 *
 * @package Kurufootwear\Upsell\Helper
 */
class ProductPostHelper extends AbstractHelper
{
    /**
     * @var PostHelper
     */
    protected $postHelper;

    /**
     * @var CartHelper
     */
    protected $cartHelper;

    /**
     * ProductPostHelper constructor.
     *
     * @param Context $context
     * @param PostHelper $postHelper
     * @param CartHelper $cartHelper
     */
    public function __construct(
        Context $context,
        PostHelper $postHelper,
        CartHelper $cartHelper
    ) {
        $this->postHelper = $postHelper;
        $this->cartHelper = $cartHelper;

        parent::__construct($context);
    }

    /**
     * Generate serialized POST payload for adding product to cart
     * Extended for configurable product preselection support
     *
     * For reference see payload usage here:
     * - app/design/frontend/Kurufootwear/default/Magento_Catalog/templates/product/list/items.phtml:220
     * -
     *
     * @param string $url - This is expected to be similiar to /checkout/cart/add/product/XXX/
     * @param $product
     *
     * @return string
     */
    public function getPostData($url, $product)
    {
        $postData = [
            'entity_id' => $product->getEntityId(),
            'product' => $product->getEntityId()
        ];

        // Simple configuration product id
        $configurationProduct = $product->getCustomOption('selected_configurable');
        if ($configurationProduct) {
            $postData['selected_configurable_option'] = $configurationProduct->getValue()->getId();
        }

        // Simple configuration product options
        $productConfigurations = $product->getCustomOption('super_attributes');
        if ($productConfigurations) {
            /**
             * If this is a preselected product, recreate add to cart url
             * by default items.phtml creates a redirect url for configurables
             * not add to cart, because preconfigurations aren't supported
             */
            $url = $this->cartHelper->getAddUrl($product);

            foreach ($productConfigurations->getValue() as $attributeId => $valueId) {
                $attributeParam = sprintf(
                    'super_attribute[%s]',
                    $attributeId
                );

                $postData[$attributeParam] = $valueId;
            }
        }

        return $this->postHelper->getPostData($url, $postData);
    }
}
