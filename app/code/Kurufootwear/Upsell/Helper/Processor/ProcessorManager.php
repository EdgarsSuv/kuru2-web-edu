<?php
/**
 * @vendor Kurufootwear
 * @module Kurufootwear_Upsell
 * @author Krisjanis Veinbahs <info@scandiweb.com>
 * @copyright Copyright (c) 2018 KURU Footwear. All rights reserved.
 */
namespace Kurufootwear\Upsell\Helper\Processor;

use Kurufootwear\Upsell\Model\UpsellProduct;
use Magento\Catalog\Api\Data\ProductInterface;

class ProcessorManager
{
    /**
     * @var UpsellProduct
     */
    protected $upsellProduct;

    /**
     * Array of self-registering processors
     *
     * @var array
     */
    protected $registeredProcessors;

    /**
     * Cached values of product processing
     *
     * @var array [ $cacheId => null|ProductInterface|ProductInterface[] ]
     */
    private $upsellProductCache = [];

    /**
     * String constant for detecting missing cache
     * (because null|ProductInterface|ProductInterface[] are valid cached values)
     */
    const UNCACHED_PRODUCT = 'uncached_product';

    /**
     * RuleManager constructor.
     *
     * @param UpsellProduct $upsellProduct
     * @param array $registeredProcessors
     */
    public function __construct(
        UpsellProduct $upsellProduct,
        array $registeredProcessors = []
    ) {
        $this->upsellProduct = $upsellProduct;
        $this->sortProcessors($registeredProcessors);
        $this->registeredProcessors = $registeredProcessors;
    }

    /**
     * Run all processors against the upsellable product
     * according to their internally declared orders
     * to get a processed, presentable product for upselling
     *
     * @param $upsellProduct
     *
     * @return null | ProductInterface | ProductInterface[]
     */
    public function processProduct($upsellProduct)
    {
        $processedProduct = null;

        $cachedUpsellValue = $this->getProductCache($upsellProduct);
        if ($cachedUpsellValue !== static::UNCACHED_PRODUCT) {
            $processedProduct = $cachedUpsellValue;
        } else {
            $processors = $this->getProcessors();

            foreach ($processors as $processor) {
                /* @var ProcessorInterface $processor */
                $currentProcessedProduct = $processor->getProcessedProduct($upsellProduct);

                if (!is_null($currentProcessedProduct)) {
                    $processedProduct = $currentProcessedProduct;
                    break;
                }
            }

            $this->setProductCache($upsellProduct, $processedProduct);
        }

        return $processedProduct;
    }

    /**
     * Return all processed upsell products
     *
     * @return ProductInterface[]
     */
    public function getProcessedUpsellProducts()
    {
        $upsellProducts = $this->upsellProduct->getUpsellableProducts();
        $processedProducts = [];

        foreach ($upsellProducts as $upsellProduct) {
            $processedProduct = $this->processProduct($upsellProduct);

            if (is_array($processedProduct)) {
                $processedProducts = array_merge(
                    $processedProducts,
                    $processedProduct
                );
            } else if (!is_null($processedProduct)) {
                $processedProducts[] = $processedProduct;
            }
        }

        return $processedProducts;
    }

    /**
     * Generate cache id for upsell product
     *
     * @param $upsellProduct
     * @return string
     */
    private function getCacheId($upsellProduct)
    {
        $serialized = serialize($upsellProduct);
        $unwhitespaced = str_replace(' ', '-', $serialized);
        $normalised = preg_replace('/[^A-Za-z0-9\-]/', '', $unwhitespaced);

        return $normalised;
    }

    /**
     * Save upsell product processed value
     *
     * @param $upsellProduct
     * @param $processedValue
     */
    private function setProductCache($upsellProduct, $processedValue)
    {
        $cacheId = $this->getCacheId($upsellProduct);
        $this->upsellProductCache[$cacheId] = $processedValue;
    }

    /**
     * Get upsell product processed value
     *
     * @param $upsellProduct
     *
     * @return mixed|null
     */
    private function getProductCache($upsellProduct)
    {
        $cacheId = $this->getCacheId($upsellProduct);

        return array_key_exists($cacheId, $this->upsellProductCache) ?
            $this->upsellProductCache[$cacheId] :
            static::UNCACHED_PRODUCT;
    }

    /**
     * Return all registered processors
     * They should already be sorted by
     *
     * @return array
     */
    public function getProcessors()
    {
        return $this->registeredProcessors;
    }

    /**
     * On manager initialisation sort the processors by order
     *
     * @param array $processors
     *
     * @return bool
     */
    protected function sortProcessors(array &$processors)
    {
        return usort($processors, function($processorOne, $processorTwo) {
            /**
             * @var ProcessorInterface $processorOne
             * @var ProcessorInterface $processorTwo
             */
            return $processorOne->getOrder() - $processorTwo->getOrder();
        });
    }
}