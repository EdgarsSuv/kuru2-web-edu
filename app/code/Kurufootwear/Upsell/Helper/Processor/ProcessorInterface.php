<?php
/**
 * @vendor Kurufootwear
 * @module Kurufootwear_Upsell
 * @author Krisjanis Veinbahs <info@scandiweb.com>
 * @copyright Copyright (c) 2018 KURU Footwear. All rights reserved.
 */
namespace Kurufootwear\Upsell\Helper\Processor;

use Magento\Catalog\Api\Data\ProductInterface;

interface ProcessorInterface
{
    /**
     * For order in which the processor will be run
     *
     * Note - Smaller order gets applied first
     */
    const PROCESSOR_ORDER = 50;

    /**
     * Process product that is available for upselling
     *
     * @param $upsellableProduct
     *
     * @return null | ProductInterface | ProductInterface[]
     */
    public function getProcessedProduct($upsellableProduct);

    /**
     * Get processor runtime order
     *
     * @return int
     */
    public function getOrder();
}