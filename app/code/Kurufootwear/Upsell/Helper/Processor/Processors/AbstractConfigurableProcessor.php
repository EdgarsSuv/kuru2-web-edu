<?php
/**
 * @vendor Kurufootwear
 * @module Kurufootwear_Upsell
 * @author Krisjanis Veinbahs <info@scandiweb.com>
 * @copyright Copyright (c) 2018 KURU Footwear. All rights reserved.
 */
namespace Kurufootwear\Upsell\Helper\Processor\Processors;

use Exception;
use Magento\Catalog\Model\Product\Attribute\Source\Status;
use Magento\Catalog\Model\Product\Visibility;

abstract class AbstractConfigurableProcessor extends AbstractProcessor
{
    /**
     * Fetch configuration (simple) product id from
     * configurable product and preselected options
     *
     * @param $product
     * @param $options
     *
     * @return $product
     */
    protected function getConfigurationProduct($product, $options)
    {
        $matchedConfigurationProduct = null;
        $configurationProducts = $product->getTypeInstance()->getUsedProducts($product);

        foreach ($configurationProducts as $configurationProduct) {
            $configurationFound = true;
            foreach ($options as $attributeCode => $optionValue) {
                // If any option is different, this isn't a matching product
                if ($configurationProduct->getAttributeText($attributeCode) !== $optionValue) {
                    $configurationFound = false;
                    break;
                }
            }

            if ($configurationFound) {
                $matchedConfigurationProduct = $configurationProduct;
                break;
            }
        }

        return $matchedConfigurationProduct;
    }

    /**
     * Helper for pre-configuring products
     *
     * Actual pre-configuration isn't supported by core M2,
     * this just saves custom data, which is later used
     * in a patched version of the crossell block
     *
     * Note - This data is later used in \Kurufootwear\Upsell\Helper\ProductPostHelper
     *
     * @param $configurableProduct
     * @param $options - [ $attributeCode => $attributeOptionValue ]
     *
     * @return $product
     */
    protected function saveConfigurationOptions($configurableProduct, $options)
    {
        // Save simple configuration id
        $configurationProduct = $this->getConfigurationProduct($configurableProduct, $options);
        $configurableProduct->addCustomOption('selected_configurable', $configurationProduct);

        // Save selected options
        $superAttributes = [];
        foreach ($options as $attributeCode => $attributeOptionValue) {
            $attribute = $configurableProduct->getResource()->getAttribute($attributeCode);

            $attributeId = $attribute->getId();
            $attributeOptionId = $configurationProduct->getData($attributeCode);

            $superAttributes[$attributeId] = $attributeOptionId;
        }
        $configurableProduct->addCustomOption('super_attributes', $superAttributes);
        $configurableProduct->addCustomOption('request_options', [
            'super_attribute' => $superAttributes
        ]);

        // Clone, because the product is a reference and the next save
        // will override the currently saved configuration options
        return clone $configurableProduct;
    }

    /**
     * Validate the configurable product with options
     *
     * @param $configurableProduct
     * @param $options
     *
     * @return null
     */
    protected function validateProduct($configurableProduct, $options)
    {
        $configurationProduct = $this->getConfigurationProduct($configurableProduct, $options);

        $productStock = $this->stockRegistry->getStockItem($configurationProduct->getId());
        if ($productStock->getQty() == 0) {
            $this->logger->critical(
                sprintf(
                    __("Kuru Upsell: Matched configuration product with id #%s is out of stock."),
                    $configurationProduct->getId()
                )
            );

            return false;
        }
        if ((int)$configurationProduct->getStatus() !== Status::STATUS_ENABLED) {
            $this->logger->critical(
                sprintf(
                    __("Kuru Upsell: Matched configuration product with id #%s is not enabled."),
                    $configurationProduct->getId()
                )
            );

            return false;
        }

        return true;
    }

    /**
     * Return a configurable product with preselected options
     * If some attribute option is missing, pick first matching option
     *
     * @param $product
     * @param $options
     *
     * @return $product|null
     */
    protected function getFirstOptionMatchProduct($product, $options)
    {
        try {
            $configurableProduct = $this->productRepository->getById($product->getProductId());
        } catch (Exception $exception) {
            return null;
        }

        $configurableAttributes = $configurableProduct->getTypeInstance(true)
            ->getConfigurableAttributes($configurableProduct);

        /**
         * Check all required attributes and fill in missing attribute options
         */
        foreach ($configurableAttributes as $configurableAttribute) {
            $attributeCode = $configurableAttribute->getProductAttribute()->getAttributeCode();

            if (!array_key_exists($attributeCode, $options)) {
                $attributeOptions = $configurableAttribute->getOptions();
                $attributeFirstOptionLabel = $attributeOptions[0]['label'];
                $options[$attributeCode] = $attributeFirstOptionLabel;
            }
        }

        /**
         * Return product with saved preselection configurations
         */
        if ($this->validateProduct($configurableProduct, $options)) {
            return $this->saveConfigurationOptions($configurableProduct, $options);
        } else {
            return null;
        }
    }

    /**
     * Return a configurable product with preselected options
     *
     * If some attribute option is missing, return multiple products:
     * One product for each possible attribute option
     *
     * @param $product
     * @param $preselectionOptions
     *
     * @return null
     */
    protected function getAllOptionMatchProducts($product, $preselectionOptions)
    {
        try {
            $configurableProduct = $this->productRepository->getById($product->getProductId());
        } catch (Exception $exception) {
            return null;
        }

        $preselectionMatches = [$preselectionOptions];
        $configurableAttributes = $configurableProduct->getTypeInstance(true)
            ->getConfigurableAttributes($configurableProduct);

        /**
         * Check all required attributes and fill in missing attribute options
         */
        foreach ($configurableAttributes as $configurableAttribute) {
            $attributeCode = $configurableAttribute->getProductAttribute()->getAttributeCode();

            if (!array_key_exists($attributeCode, $preselectionOptions)) {
                $attributeOptions = $configurableAttribute->getOptions();
                $newPreselectionMatches = [];

                foreach ($attributeOptions as $attributeOption) {
                    foreach ($preselectionMatches as $preselectionMatch) {
                        $newMatch = $preselectionMatch;
                        $newMatch[$attributeCode] = $attributeOption['label'];

                        $newPreselectionMatches[] = $newMatch;
                    }
                }

                $preselectionMatches = $newPreselectionMatches;
            }
        }

        /**
         * Return products with saved preselection configurations
         */
        $products = [];
        foreach ($preselectionMatches as $preselectionMatch) {
            if ($this->validateProduct($configurableProduct, $preselectionMatch)) {
                $products[] = $this->saveConfigurationOptions($configurableProduct, $preselectionMatch);
            }
        }

        return count($products) === 1 ? $products[0] : $products;
    }
}