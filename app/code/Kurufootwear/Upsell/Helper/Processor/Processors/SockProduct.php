<?php
/**
 * @vendor Kurufootwear
 * @module Kurufootwear_Upsell
 * @author Krisjanis Veinbahs <info@scandiweb.com>
 * @copyright Copyright (c) 2018 KURU Footwear. All rights reserved.
 */
namespace Kurufootwear\Upsell\Helper\Processor\Processors;

use Exception;
use Magento\Catalog\Api\Data\ProductInterface;
use Magento\Catalog\Api\ProductRepositoryInterface;
use Magento\CatalogInventory\Api\StockRegistryInterface;
use Magento\Checkout\Model\Session as CheckoutSession;
use Magento\Eav\Model\ResourceModel\Entity\Attribute\Set\CollectionFactory;
use Psr\Log\LoggerInterface;

class SockProduct extends AbstractConfigurableProcessor
{
    /**
     * @var CheckoutSession
     */
    protected $checkoutSession;

    /**
     * @var CollectionFactory
     */
    protected $attributeSetCollection;

    /**
     * Order in which this processor will be run
     */
    const PROCESSOR_ORDER = 70;

    /**
     * SizeMatcher constructor.
     *
     * @param CheckoutSession $checkoutSession
     * @param ProductRepositoryInterface $productRepository
     * @param StockRegistryInterface $stockRegistry
     * @param CollectionFactory $attributeSetCollection
     * @param LoggerInterface $logger
     */
    public function __construct(
        CheckoutSession $checkoutSession,
        ProductRepositoryInterface $productRepository,
        StockRegistryInterface $stockRegistry,
        CollectionFactory $attributeSetCollection,
        LoggerInterface $logger
    ) {
        $this->checkoutSession = $checkoutSession;
        $this->attributeSetCollection = $attributeSetCollection;

        parent::__construct(
            $productRepository,
            $stockRegistry,
            $logger
        );
    }

    /**
     * Process product that is available for upselling
     *
     * This processor was written to upsell a preselected sock
     *
     * @param $upsellableProduct
     *
     * @return null | ProductInterface | ProductInterface[]
     */
    public function getProcessedProduct($upsellableProduct)
    {
        if (!$this->isSockProduct($upsellableProduct)) {
            return null;
        }

        /**
         * Upsellable sock is calculated by taking the size attribute of the cart product
         * (first one available from the matching table, if none available, then no sock)
         * and matching it with the following table
         */
        $sockAlreadyUpsold = false;
        $matchingSockSize = null;
        $sizeMatchingTables = $this->getSizeMatchingTables();
        $quoteItems = $this->checkoutSession->getQuote()->getAllItems();

        foreach ($quoteItems as $quoteItem) {
            $shoeSizeValue = null;
            $sizeMatchingTable = null;

            if ($quoteItem->getProduct()->getAttributeSetId() === $this->getSockAttributeSetId()) {
                $sockAlreadyUpsold = true;
                break;
            }

            if ($quoteItem->getProduct()->getTypeId() !== 'simple') {
                // To match the sock size we use only simple products
                // And check their selected options
                continue;
            }

            /**
             * Check if quote item has a size attribute that we can match to a sock size
             * Note: For an attribute value to be available here, edit etc/catalog_attributes.xml
             */
            foreach ($sizeMatchingTables as $availableSizeAttribute => $availableSizeMatchingTable) {
                $shoeSizeValue = $quoteItem->getProduct()->getAttributeText($availableSizeAttribute);

                if (!empty($shoeSizeValue)) {
                    $sizeMatchingTable = $availableSizeMatchingTable;
                    break;
                }
            }

            if (empty($shoeSizeValue)) {
                $this->logger->critical(
                    sprintf(
                        __(
                            "Kuru Upsell: Tried to match sock with id #%s, but couldn't get its selected shoe size. " .
                            "Is 'Used in product listing' enabled for all size attributes? " .
                            "Does the product size attribute exist in the matching tables?"
                        ),
                        $quoteItem->getProduct()->getId()
                    )
                );

                continue;
            } else if (empty($sizeMatchingTable)) {
                $this->logger->critical(
                    sprintf(
                        __(
                            "Kuru Upsell: Tried to match sock with id #%s, found the size, " .
                            "but the size matching table is missing."
                        ),
                        $quoteItem->getProduct()->getId()
                    )
                );

                continue;
            } else if (!array_key_exists($shoeSizeValue, $sizeMatchingTable)) {
                $this->logger->critical(
                    sprintf(
                        __(
                            "Kuru Upsell: Tried to match sock with id #%s, found the size, found a size matching table, " .
                            "but the specific product (shoe) size in the matching table is missing."
                        ),
                        $quoteItem->getProduct()->getId()
                    )
                );

                continue;
            } else {
                $currentCycleMatchingSockSize = $sizeMatchingTable[$shoeSizeValue];

                /**
                 * If no sock size yet picked, but one matches, pick it
                 * If a size already picked, but another matches, pick it if it's larger
                 */
                if (is_null($matchingSockSize) ||
                    $this->isLargerSocksSize($currentCycleMatchingSockSize, $matchingSockSize)) {
                    $matchingSockSize = $currentCycleMatchingSockSize;
                }
            }
        }

        if ($sockAlreadyUpsold || is_null($matchingSockSize)) {
            return null;
        } else {
            return $this->getConfiguredSock($upsellableProduct, $matchingSockSize);
        }
    }

    /**
     * Detect upsellable sock
     *
     * @param $upsellableProduct
     *
     * @return bool
     */
    protected function isSockProduct($upsellableProduct)
    {
        try {
            $product = $this->productRepository->getById($upsellableProduct->getProductId());

            return $product->getAttributeSetId() == $this->getSockAttributeSetId();
        } catch (Exception $exception) {
            return false;
        }
    }

    /**
     * Get sock attribute set id
     */
    protected function getSockAttributeSetId()
    {
        $attributeCollection = $this->attributeSetCollection->create()->addFieldToSelect(
            '*'
        )->addFieldToFilter(
            'attribute_set_name',
            'Socks'
        )->load();

        return count($attributeCollection) > 0 ?
            $attributeCollection->getFirstItem()->getAttributeSetId() : null;
    }

    /**
     * Return configurable product with sock size preselected
     *
     * @param $upsellableProduct - This should always be a configurable sock
     * @param $matchingSockSize
     *
     * @return null | ProductInterface | ProductInterface[]
     */
    protected function getConfiguredSock($upsellableProduct, $matchingSockSize)
    {
        $preconfiguredOptions = [
            'socks_size' => $matchingSockSize
        ];

        return $this->getAllOptionMatchProducts($upsellableProduct, $preconfiguredOptions);
    }

    /**
     * Table for matching shoe size attributes to sock size
     *
     * @return array
     */
    protected function getSizeMatchingTables()
    {
        return [
            'womens_size' => [
                '5' => 'XS',
                '5.5' => 'XS',
                '6' => 'S',
                '6.5' => 'S',
                '7' => 'S',
                '7.5' => 'S',
                '8' => 'S',
                '8.5' => 'S',
                '9' => 'M',
                '9.5' => 'M',
                '10' => 'M',
                '10.5' => 'M',
                '11' => 'M',
                '11.5' => 'M',
                '12' => 'L'
            ],
            'size' => [
                '6' => 'S',
                '6.5' => 'S',
                '7' => 'S',
                '7.5' => 'M',
                '8' => 'M',
                '8.5' => 'M',
                '9' => 'M',
                '9.5' => 'M',
                '10' => 'M',
                '10.5' => 'L',
                '11' => 'L',
                '11.5' => 'L',
                '12' => 'L',
                '12.5' => 'XL',
                '13' => 'XL',
                '13.5' => 'XL',
                '14' => 'XL',
            ]
        ];
    }

    /**
     * Compare socks sizes
     *
     * @param $socksSizeOne
     * @param $socksSizeTwo
     *
     * @return bool
     */
    protected function isLargerSocksSize($socksSizeOne, $socksSizeTwo)
    {
        return $this->translateSocksSizeToInt($socksSizeOne) > $this->translateSocksSizeToInt($socksSizeTwo);
    }

    /**
     * Translate socks size to equivalent integer
     *
     * @param $size
     *
     * @return mixed|null
     */
    protected function translateSocksSizeToInt($size)
    {
        $translations = [
            'XS' => 1,
            'S' => 2,
            'M' => 3,
            'L' => 4,
            'XL' => 5
        ];

        if (array_key_exists($size, $translations)) {
            return $translations[$size];
        } else {
            return null;
        }
    }
}