<?php
/**
 * @vendor Kurufootwear
 * @module Kurufootwear_Upsell
 * @author Krisjanis Veinbahs <info@scandiweb.com>
 * @copyright Copyright (c) 2018 KURU Footwear. All rights reserved.
 */
namespace Kurufootwear\Upsell\Helper\Processor\Processors;

use Exception;
use Magento\Catalog\Api\Data\ProductInterface;

class SimpleProduct extends AbstractProcessor
{
    /**
     * Relative number in which this rule will be applied
     */
    const PROCESSOR_ORDER = 30;

    /**
     * Process product that is available for upselling
     *
     * This rule was written to handle simple products, they don't need preselections
     * and if are up for upselling, just get returned directly
     *
     * @param $upsellableProduct
     *
     * @return null | ProductInterface | ProductInterface[]
     */
    public function getProcessedProduct($upsellableProduct)
    {
        try {
            $processedProduct = $this->productRepository->getById(
                $upsellableProduct->getProductId()
            );
        } catch (Exception $exception) {
            return null;
        }

        if ($processedProduct->getTypeId() === 'simple') {
            return $processedProduct;
        } else {
            return null;
        }
    }
}