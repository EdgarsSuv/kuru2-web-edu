<?php
/**
 * @vendor Kurufootwear
 * @module Kurufootwear_Upsell
 * @author Krisjanis Veinbahs <info@scandiweb.com>
 * @copyright Copyright (c) 2018 KURU Footwear. All rights reserved.
 */
namespace Kurufootwear\Upsell\Observer;

use Amasty\Promo\Helper\Cart;
use Amasty\Promo\Helper\Item;
use Amasty\Promo\Model\Registry;
use Exception;
use Magento\Framework\Event\Observer;
use Magento\Framework\Event\ObserverInterface;
use Magento\Framework\Registry as MagentoRegistry;

/**
 * Overridden to not fall into an infinite loop with configurable products
 */
class CollectTotalsAfterObserver implements ObserverInterface
{
    /**
     * Core registry
     *
     * @var MagentoRegistry
     */
    private $_coreRegistry;

    /**
     * @var Cart
     */
    private $promoCartHelper;

    /**
     * @var Item
     */
    private $promoItemHelper;

    /**
     * @var Registry
     */
    private $promoRegistry;

    /**
     * CollectTotalsAfterObserver constructor.
     *
     * @param MagentoRegistry $registry
     * @param Cart $promoCartHelper
     * @param Item $promoItemHelper
     * @param Registry $promoRegistry
     */
    public function __construct(
        MagentoRegistry $registry,
        Cart $promoCartHelper,
        Item $promoItemHelper,
        Registry $promoRegistry
    ) {
        $this->_coreRegistry = $registry;
        $this->promoCartHelper = $promoCartHelper;
        $this->promoItemHelper = $promoItemHelper;
        $this->promoRegistry = $promoRegistry;
    }

    /**
     * Override of Amasty_Promo CollectTotalsAfterObserver
     * to avoid an infinite loop on configurable "upselling"
     *
     * @param Observer $observer
     *
     * @throws Exception
     */
    public function execute(Observer $observer)
    {
        $allowedItems = $this->promoRegistry->getPromoItems();
        $toAdd = $this->_coreRegistry->registry('ampromo_to_add');
        /** @var \Magento\Quote\Model\Quote $quote */
        $quote = $observer->getQuote();

        if (is_array($toAdd)) {
            foreach ($toAdd as $item) {
                $this->promoCartHelper->addProduct(
                    $item['product'],
                    $item['qty'],
                    false,
                    [],
                    $item['discount'],
                    isset($item['discount']) && !empty($item['discount']) ? $item['discount']['minimal_price'] : null,
                    $quote
                );
            }
        }
        $this->_coreRegistry->unregister('ampromo_to_add');

        foreach ($quote->getAllItems() as $item) {
            if ($this->promoItemHelper->isPromoItem($item)) {
                if ($item->getParentItemId()) {
                    continue;
                }

                $sku = $item->getProduct()->getData('sku');

                $ruleId = $this->promoItemHelper->getRuleId($item);

                if (isset($allowedItems['_groups'][$ruleId])) { // Add one of
                    if ($allowedItems['_groups'][$ruleId]['qty'] <= 0) {
                        $quote->removeItem($item->getId());
                    } elseif ($item->getQty() > $allowedItems['_groups'][$ruleId]['qty']) {
                        $item->setQty($allowedItems['_groups'][$ruleId]['qty']);
                    }

                    $allowedItems['_groups'][$ruleId]['qty'] -= $item->getQty();
                } elseif (isset($allowedItems[$sku])) { // Add all of
                    if ($allowedItems[$sku]['qty'] <= 0) {
                        $quote->removeItem($item->getId());
                    } elseif ($item->getQty() > $allowedItems[$sku]['qty']) {
                        $item->setQty($allowedItems[$sku]['qty']);
                    }

                    $allowedItems[$sku]['qty'] -= $item->getQty();
                }
                // Override - remove 'else' condition with infinite totals re-calculation
            }
        }

        $this->promoCartHelper->updateQuoteTotalQty(
            false,
            $quote
        );
    }

    /**
     * Check if a product id is one of allowed items child ids
     *
     * @param $allowedItems
     * @param $productId
     *
     * @return string
     */
    public function isAllowedChildId($allowedItems, $productId)
    {
        foreach ($allowedItems as $sku => $allowedItem) {
            if (!isset($allowedItem['product'])) {
                continue;
            }

            $allowedSelectedConfigurable = $allowedItem['product']->getCustomOption('selected_configurable');
            if (!$allowedSelectedConfigurable) {
                continue;
            }

            if ($productId == $allowedSelectedConfigurable->getValue()->getId()) {
                return (string)$sku;
            }
        }

        return false;
    }
}
