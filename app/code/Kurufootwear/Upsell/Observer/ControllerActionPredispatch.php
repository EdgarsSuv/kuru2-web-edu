<?php
/**
 * @vendor Kurufootwear
 * @module Kurufootwear_Upsell
 * @author Krisjanis Veinbahs <info@scandiweb.com>
 * @copyright Copyright (c) 2018 KURU Footwear. All rights reserved.
 */
namespace Kurufootwear\Upsell\Observer;

use Amasty\Promo\Helper\Cart;
use Amasty\Promo\Model\Registry;
use Exception;
use Kurufootwear\Upsell\Helper\Processor\ProcessorManager;
use Kurufootwear\Upsell\Model\UpsellProduct;
use Magento\Catalog\Api\ProductRepositoryInterface;
use Magento\Checkout\Model\Session;
use Magento\Framework\App\RequestInterface;
use Magento\Framework\Event\ObserverInterface;
use Magento\Framework\Exception\NoSuchEntityException;
use Psr\Log\LoggerInterface;

/**
 * Class ControllerActionPredispatch
 *
 * Observer on page load to preselect and upsell
 * configurable gift products in checkout pages
 *
 * @package Kurufootwear\Upsell\Observer
 */
class ControllerActionPredispatch implements ObserverInterface
{
    /**
     * @var Cart
     */
    private $promoCartHelper;

    /**
     * @var Registry
     */
    private $promoRegistry;

    /**
     * @var Session
     */
    private $checkoutSession;

    /**
     * @var ProcessorManager
     */
    private $processorManager;

    /**
     * @var UpsellProduct
     */
    private $upsellProduct;

    /**
     * @var ProductRepositoryInterface
     */
    private $productRepository;

    /**
     * @var LoggerInterface
     */
    private $logger;

    /**
     * @var RequestInterface
     */
    private $request;

    /**
     * CollectTotalsAfterObserver constructor.
     *
     * @param Cart $promoCartHelper
     * @param Registry $promoRegistry
     * @param Session $checkoutSession
     * @param ProcessorManager $processorManager
     * @param UpsellProduct $upsellProduct
     * @param ProductRepositoryInterface $productRepository
     * @param LoggerInterface $logger
     * @param RequestInterface $request
     */
    public function __construct(
        Cart $promoCartHelper,
        Registry $promoRegistry,
        Session $checkoutSession,
        ProcessorManager $processorManager,
        UpsellProduct $upsellProduct,
        ProductRepositoryInterface $productRepository,
        LoggerInterface $logger,
        RequestInterface $request
    ) {
        $this->promoCartHelper = $promoCartHelper;
        $this->promoRegistry = $promoRegistry;
        $this->checkoutSession = $checkoutSession;
        $this->processorManager = $processorManager;
        $this->upsellProduct = $upsellProduct;
        $this->productRepository = $productRepository;
        $this->logger = $logger;
        $this->request = $request;
    }

    /**
     * Auto-add configurable, pre-selectable products (e.g. socks) in checkout pages
     * Essentially extended functionality for Amasty_Promo
     */
    public function execute(\Magento\Framework\Event\Observer $observer)
    {
        $autoSellTriggerActions = [
            'checkout_cart_index',
            'checkout_index_index'
        ];

        if (in_array($this->request->getFullActionName(), $autoSellTriggerActions)) {
            $this->autosellConfigurables();
        }
    }

    /**
     * Auto add products to cart that can be preselected
     */
    public function autosellConfigurables()
    {
        $autoSellableProducts = $this->checkoutSession->getData('configurable_autosellable');
        $initialAutoSoldProducts = $this->checkoutSession->getData('configurable_autosold');
        $autoSoldProducts = $initialAutoSoldProducts;

        if (is_null($autoSoldProducts)) {
            $autoSoldProducts = [];
        }

        if (is_null($autoSellableProducts)) {
            return;
        }

        $cartHasChanged = false;
        foreach ($autoSellableProducts as $productData) {
            $productId = $productData['product_id'];
            $ruleId = $productData['rule_id'];
            $quantity = $productData['quantity'];

            // Skip products that have already once been upsold
            if (in_array($productId, $autoSoldProducts)) {
                continue;
            }

            $added = $this->autoSellConfigurable($productId, $ruleId, $quantity);

            if ($added) {
                $cartHasChanged = true;
                $autoSoldProducts[] = $productId;
            }
        }

        if ($cartHasChanged) {
            $this->checkoutSession->setData('configurable_autosold', $autoSoldProducts);

            try {
                $this->promoCartHelper->updateQuoteTotalQty(true);
            } catch (Exception $exception) {
                // Failed to auto-sell products -> revert state and log error
                $this->checkoutSession->setData('configurable_autosold', $initialAutoSoldProducts);
                $this->logger->error(
                    "Failed to preconfigure and gift configurable product.\n" . $exception->getMessage()
                );
            }
        }
    }

    /**
     * Attempt pre-selecting configurable options and adding to cart
     *
     * @param $productId
     * @param $ruleId
     * @param $quantity
     *
     * @return bool
     */
    private function autoSellConfigurable($productId, $ruleId, $quantity) {
        $upsellProduct = $this->upsellProduct->mockUpsellProduct($productId);
        $processedUpsellProduct = $this->processorManager->processProduct($upsellProduct);

        if (is_null($processedUpsellProduct)) {
            return false;
        }

        if (is_array($processedUpsellProduct)) {
            // Auto-preselect any configuration randomly
            $processedUpsellProduct = $processedUpsellProduct[array_rand($processedUpsellProduct)];
        }

        $productId = $processedUpsellProduct->getId();
        $requestOptions = $processedUpsellProduct->getCustomOption('request_options')->getValue();

        return $this->mockAmastyConfigurationAdd($productId, $requestOptions, $ruleId, $quantity);
    }

    /**
     * Use native Amasty_Promo cart helper function
     * for adding configurable products to cart
     *
     * Same functionality as in amasty_promo_cart_add controller
     *
     * @param $productId
     * @param $requestOptions
     * @param $ruleId
     * @param $quantity
     *
     * @return bool
     */
    private function mockAmastyConfigurationAdd($productId, $requestOptions, $ruleId, $quantity)
    {
        try {
            $product = $this->productRepository->getById($productId);
        } catch (NoSuchEntityException $noSuchEntityException) {
            $this->logger->error(sprintf(
                "Trying to upsell/preselect non-existent product '%s'.\n%s",
                $productId,
                $noSuchEntityException->getMessage()
            ));

            return false;
        }

        // Fake custom options from Upsell module can break cart save
        $product->setCustomOptions([]);
        $limits = $this->promoRegistry->getLimits();
        $sku = $product->getSku();
        $addAllRule = isset($limits[$sku]) && $limits[$sku] > 0;
        $discount = isset($limits[$sku]) ? $limits[$sku]['discount']['discount_item'] : null;
        $minimalPrice = isset($limits[$sku]) ? $limits[$sku]['discount']['minimal_price'] : null;

        if (!$addAllRule && is_array($limits['_groups'])) {
            foreach ($limits['_groups'] as $limitRuleId => $limitRule) {
                $discount = $limitRule['discount']['discount_item'];
                $minimalPrice = $limitRule['discount']['minimal_price'];
            }
        }

        $this->promoCartHelper->addProduct(
            $product,
            $quantity,
            $ruleId,
            $requestOptions,
            $discount,
            $minimalPrice
        );

        return true;
    }
}
