<?php
/**
 * @vendor Kurufootwear
 * @module Kurufootwear_Upsell
 * @author Krisjanis Veinbahs <info@scandiweb.com>
 * @copyright Copyright (c) 2018 KURU Footwear. All rights reserved.
 */
namespace Kurufootwear\Upsell\Block\Adminhtml\Form\Field;

use Exception;
use Magento\Config\Block\System\Config\Form\Field\FieldArray\AbstractFieldArray;
use Magento\Framework\DataObject;
use Magento\Framework\View\Element\BlockInterface;

/**
 * Class UpsellProductsConfiguration
 *
 * @package Kurufootwear\Upsell\Block\Adminhtml\Form\Field
 */
class UpsellProductsConfiguration extends AbstractFieldArray
{
    /**
     * @var Boolean
     */
    protected $boolean;

    /**
     * Prepare to render
     *
     * @return void
     */
    protected function _prepareToRender()
    {
        $this->addColumn(
            'product_id',
            [
                'label'     => __('Product id'),
                'style' => 'width: 150px'
            ]
        );
        $this->addColumn(
            'active',
            [
                'label' => __('Is active'),
                'renderer'  => $this->getBoolean()
            ]
        );
        $this->_addAfter = false;
        $this->_addButtonLabel = __('Add Extra Label');
    }

    /**
     * Get and cache boolean renderer
     *
     * @return BlockInterface|null
     */
    protected function getBoolean()
    {
        if (!$this->boolean) {
            try {
                $this->boolean = $this->getLayout()->createBlock(
                    Boolean::class,
                    '',
                    ['data' => ['is_render_to_js_template' => true]]
                );
            } catch (Exception $exception) {
                return null;
            }
        }

        return $this->boolean;
    }

    /**
     * Prepare existing row data object
     *
     * @param DataObject $row
     *
     * @return void
     */
    protected function _prepareArrayRow(DataObject $row)
    {
        $optionExtraAttr = [];

        $isActive = (int)$row->getData('active');
        $optionHash = $this->getBoolean()->calcOptionHash($isActive);

        $hashedOptionId = sprintf(
            'option_%s',
            $optionHash
        );

        $optionExtraAttr[$hashedOptionId] = 'selected="selected"';
        $row->setData(
            'option_extra_attrs',
            $optionExtraAttr
        );
    }
}
