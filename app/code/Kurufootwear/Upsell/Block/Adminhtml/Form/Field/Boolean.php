<?php
/**
 * @vendor Kurufootwear
 * @module Kurufootwear_Upsell
 * @author Krisjanis Veinbahs <info@scandiweb.com>
 * @copyright Copyright (c) 2018 KURU Footwear. All rights reserved.
 */
namespace Kurufootwear\Upsell\Block\Adminhtml\Form\Field;

use Magento\Framework\View\Element\Html\Select;
use Magento\Framework\View\Element\Context;
use Magento\Eav\Model\ResourceModel\Entity\Attribute\Set\CollectionFactory;

class Boolean extends Select
{
    /**
     * Return specific option value or the option list
     *
     * @param int $option
     *
     * @return array|string
     */
    protected function availableOptions($option = null)
    {
        $options = [
            '0' => 'No',
            '1' => 'Yes',
        ];

        if (!is_null($option)) {
            return isset($options[$option]) ? $options[$option] : null;
        }

        return array_unique($options, SORT_REGULAR);
    }

    /**
     * This function is required as per a non-existing RendererInterface
     * Its used to give the renderer ($this) the name of the currently rendered attribute
     *
     * See - \Magento\Config\Block\System\Config\Form\Field\FieldArray\AbstractFieldArray::renderCellTemplate
     *
     * @param string $value
     * @return $this
     */
    public function setInputName($value)
    {
        return $this->setName($value);
    }

    /**
     * Render block HTML
     *
     * @return string
     */
    public function _toHtml()
    {
        if (!$this->getOptions()) {
            foreach ($this->availableOptions() as $optionId => $optionLabel) {
                $this->addOption($optionId, $optionLabel);
            }
        }

        return parent::_toHtml();
    }
}
