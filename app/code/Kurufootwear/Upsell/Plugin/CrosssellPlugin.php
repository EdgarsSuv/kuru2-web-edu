<?php
/**
 * @vendor Kurufootwear
 * @module Kurufootwear_Upsell
 * @author Krisjanis Veinbahs <info@scandiweb.com>
 * @copyright Copyright (c) 2018 KURU Footwear. All rights reserved.
 */
namespace Kurufootwear\Upsell\Plugin;

use Exception;
use Kurufootwear\Upsell\Helper\Processor\ProcessorManager;
use Kurufootwear\Upsell\Model\UpsellProduct;
use Magento\Checkout\Block\Cart\Crosssell;
use Psr\Log\LoggerInterface;

class CrosssellPlugin
{
    /**
     * @var ProcessorManager
     */
    protected $processorManager;

    /**
     * @var UpsellProduct
     */
    protected $upsellProduct;

    /**
     * @var LoggerInterface
     */
    protected $logger;

    /**
     * CrosssellPlugin constructor.
     *
     * @param ProcessorManager $processorManager
     * @param UpsellProduct $upsellProduct
     * @param LoggerInterface $logger
     */
    public function __construct(
        ProcessorManager $processorManager,
        UpsellProduct $upsellProduct,
        LoggerInterface $logger
    ) {
        $this->processorManager = $processorManager;
        $this->upsellProduct = $upsellProduct;
        $this->logger = $logger;
    }

    /**
     * Add more products to crosssell
     *
     * @param Crosssell $subject
     * @param callable $proceed
     * @throws \RuntimeException
     *
     * @return array
     */
    public function aroundGetItems(
        Crosssell $subject,
        callable $proceed
    ) {
        $itemsGenerated = $subject->getData('items');
        $upsellActive = $this->upsellProduct->getIsActive();

        $items = $proceed();

        if (!$itemsGenerated && $upsellActive) {
            try {
                $upsellProducts = $this->processorManager->getProcessedUpsellProducts();
                $items = array_merge($items, $upsellProducts);
                $subject->setData('items', $items);
            } catch (Exception $exception) {
                $this->logger->error(sprintf(
                    __("Generating upsell products failed.\n%s"),
                    $exception->getMessage()
                ));
            }
        }

        return $items;
    }
}