<?php
/**
 * @vendor Kurufootwear
 * @module Kurufootwear_Upsell
 * @author Krisjanis Veinbahs <info@scandiweb.com>
 * @copyright Copyright (c) 2018 KURU Footwear. All rights reserved.
 */
namespace Kurufootwear\Upsell\Model;

use Amasty\Promo\Helper\Config;
use Amasty\Promo\Helper\Item;
use Amasty\Promo\Helper\Messages;
use Amasty\Promo\Model\Registry;
use Magento\Catalog\Model\ProductFactory;
use Magento\Checkout\Model\Session;
use Magento\Framework\App\Config\ScopeConfigInterface;
use Magento\Store\Model\Store;
use Magento\Store\Model\StoreManagerInterface;

class PromoRegistry extends Registry
{
    /**
     * @var Config
     */
    private $config;

    /**
     * @var Store
     */
    private $store;

    /**
     * PromoRegistry constructor.
     * @param Session $resourceSession
     * @param ScopeConfigInterface $scopeConfig
     * @param ProductFactory $productFactory
     * @param StoreManagerInterface $storeManager
     * @param Item $promoItemHelper
     * @param Messages $promoMessagesHelper
     * @param Config $config
     * @param Store $store
     */
    public function __construct(
        Session $resourceSession,
        ScopeConfigInterface $scopeConfig,
        ProductFactory $productFactory,
        StoreManagerInterface $storeManager,
        Item $promoItemHelper,
        Messages $promoMessagesHelper,
        Config $config,
        Store $store
    ) {
        $this->config = $config;
        $this->store = $store;

        parent::__construct(
            $resourceSession,
            $scopeConfig,
            $productFactory,
            $storeManager,
            $promoItemHelper,
            $promoMessagesHelper,
            $config,
            $store
        );
    }

    /**
     * @param $sku
     * @param $qty
     * @param $ruleId
     * @param $discountData
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function addPromoItem($sku, $qty, $ruleId, $discountData)
    {
        if ($this->_locked) {
            return;
        }

        if (!$this->_hasItems) {
            $this->reset();
        }

        $discountData = $this->getCurrencyDiscount($discountData);

        $this->_hasItems = true;
        $items = $this->getPromoItems();
        $autoAdd = false;

        $addAutomatically = $this->config->getScopeValue("general/auto_add");
        $isFullDiscount = ($discountData['discount_item'] === "100%"
                || $discountData['discount_item'] === null
                || $discountData['discount_item'] === "")
            && !$discountData['minimal_price'];

        if (is_array($sku) && count($sku) == 1) {
            $sku = $sku[0];
        }

        if (!is_array($sku)) {
            if ($addAutomatically && $isFullDiscount) {
                $collection = $this->_productFactory->create()->getCollection()
                    ->addAttributeToSelect(['name', 'status', 'required_options'])
                    ->addAttributeToFilter('sku', $sku)
                    ->setPage(1, 1);

                $product = $collection->getFirstItem();

                $currentWebsiteId = $this->_storeManager->getWebsite()->getId();
                if (!is_array($product->getWebsiteIds())
                    || !in_array($currentWebsiteId, $product->getWebsiteIds())
                ) {
                    // Ignore products from other websites
                    return;
                }

                if (!$product || !$product->isInStock() || !$product->isSalable()) {
                    $this->promoMessagesHelper->addAvailabilityError($product);
                } else {
                    $shouldAutoAdd = in_array($product->getTypeId(), $this->autoAddTypes);
                    if ($shouldAutoAdd && !$product->getTypeInstance(true)->hasRequiredOptions($product)) {
                        $autoAdd = true;
                    } else {
                        /**
                         * Override:
                         *  Store auto-sellable and configurable products in the checkout session
                         *  to later attempt pre-selecting and auto-adding in a separate plugin
                         */
                        $this->addConfigurableAutosell($product->getId(), $ruleId, $qty);
                    }
                }
            }

            if (isset($items[$sku])) {
                $items[$sku]['qty'] += $qty;
            } else {
                $items[$sku] = [
                    'sku'      => $sku,
                    'qty'      => $qty,
                    'auto_add' => $autoAdd,
                    'discount' => $discountData
                ];
            }
        } else {
            $items['_groups'][$ruleId] = [
                'sku' => $sku,
                'qty' => $qty,
                'discount' => $discountData
            ];
        }

        $this->_checkoutSession->setAmpromoItems($items);
    }

    /**
     * Re-declared/copied, because this method is private
     * and is required for addPromoItem override
     *
     * @param $discountData
     *
     * @return mixed
     */
    private function getCurrencyDiscount($discountData)
    {
        preg_match('/^-*\d+.*\d*$/', $discountData['discount_item'], $discount);
        if (isset($discount[0]) && is_numeric($discount[0])) {
            $discountData['discount_item'] = $discount[0] * $this->store->getCurrentCurrencyRate();
        }

        return $discountData;
    }

    /**
     * Store configurable products that can be auto-sold in checkout session
     * (By default Amasty doesn't automate add-to-cart'ing for configurable products)
     *
     * @param $productId
     * @param $ruleId
     * @param $quantity
     */
    private function addConfigurableAutosell($productId, $ruleId, $quantity)
    {
        $configAutosells = $this->_checkoutSession->getData('configurable_autosellable');

        if (!$configAutosells) {
            $configAutosells = [];
        }

        if (!in_array($productId, $configAutosells)) {
            $configAutosells[] = [
                'product_id' => $productId,
                'rule_id' => $ruleId,
                'quantity' => $quantity
            ];
        }

        $this->_checkoutSession->setData('configurable_autosellable', $configAutosells);
    }
}
