<?php
/**
 * @vendor Kurufootwear
 * @module Kurufootwear_Upsell
 * @author Krisjanis Veinbahs <info@scandiweb.com>
 * @copyright Copyright (c) 2018 KURU Footwear. All rights reserved.
 */
namespace Kurufootwear\Upsell\Model;

/**
 * Class JsonEncode
 *
 * Used for serializing data to json string and unserializing json string to data.
 * There's a core class for this in M2.2, which will have to be used
 *
 * @package Kurufootwear\Upsell\Model
 */
class JsonEncode
{
    /**
     * Json encode supported data-types
     *
     * @param array|bool|float|int|null|string $data
     *
     * @return string
     */
    public function serialize($data)
    {
        return json_encode($data);
    }

    /**
     * Json decode supported data-types
     *
     * @param string $string
     *
     * @return mixed
     */
    public function unserialize($string)
    {
        return json_decode($string, true);
    }
}
