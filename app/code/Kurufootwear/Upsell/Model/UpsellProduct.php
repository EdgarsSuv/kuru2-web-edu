<?php
/**
 * @vendor Kurufootwear
 * @module Kurufootwear_Upsell
 * @author Krisjanis Veinbahs <info@scandiweb.com>
 * @copyright Copyright (c) 2018 KURU Footwear. All rights reserved.
 */
namespace Kurufootwear\Upsell\Model;

use Magento\Framework\DataObject;
use Magento\Framework\App\Config\ScopeConfigInterface;
use Magento\Framework\DataObjectFactory;

class UpsellProduct
{
    /**
     * @var ScopeConfigInterface
     */
    protected $scopeConfig;

    /**
     * @var JsonEncode
     */
    protected $jsonEncode;

    /**
     * @var DataObjectFactory
     */
    protected $dataObjectFactory;

    /**
     * XML Store config paths
     */
    const XML_PATH_UPSELL_ACTIVE = 'kurufootwear/upsell/active';
    const XML_PATH_UPSELL_PRODUCTS = 'kurufootwear/upsell/products';

    /**
     * UpsellProduct constructor.
     *
     * @param ScopeConfigInterface $scopeConfig
     * @param JsonEncode $jsonEncode
     * @param DataObjectFactory $dataObjectFactory
     */
    public function __construct(
        ScopeConfigInterface $scopeConfig,
        JsonEncode $jsonEncode,
        DataObjectFactory $dataObjectFactory
    ) {
        $this->scopeConfig = $scopeConfig;
        $this->jsonEncode = $jsonEncode;
        $this->dataObjectFactory = $dataObjectFactory;
    }

    /**
     * Return whether this module is running
     *
     * Note: If it is you better go catch it!
     *
     * @return mixed
     */
    public function getIsActive()
    {
        return $this->scopeConfig->getValue(static::XML_PATH_UPSELL_ACTIVE);
    }

    /**
     * Get upsellable products
     *
     * @return array
     */
    public function getUpsellableProducts()
    {
        $serializedProducts = $this->scopeConfig->getValue(static::XML_PATH_UPSELL_PRODUCTS);
        $products = $this->jsonEncode->unserialize($serializedProducts);

        if (is_array($products)) {
            return array_map(function($upsellProduct) {
                return $this->dataObjectFactory->create()->setData($upsellProduct);
            }, $products);
        } else {
            return [];
        }
    }

    /**
     * Create an Upsell Product object that will be compatible with all the
     * Upsell processor manipulations (e.g. pre-selections).
     *
     * @param int $productId
     * @param bool $isActive
     *
     * @return DataObject
     */
    public function mockUpsellProduct(int $productId, bool $isActive = true)
    {
       return $this->dataObjectFactory->create()->setData([
           'product_id' => $productId,
           'active' => $isActive
       ]);
    }
}