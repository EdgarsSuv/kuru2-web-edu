<?php
/**
 * @vendor Kurufootwear
 * @module Kurufootwear_Upsell
 * @author Krisjanis Veinbahs <info@scandiweb.com>
 * @copyright Copyright (c) 2018 KURU Footwear. All rights reserved.
 */
namespace Kurufootwear\Upsell\Model\Adminhtml\System\Config;

use Kurufootwear\Upsell\Model\JsonEncode;
use Magento\Framework\App\Cache\TypeListInterface;
use Magento\Framework\App\Config\ScopeConfigInterface;
use Magento\Framework\App\Config\Value;
use Magento\Framework\Data\Collection\AbstractDb;
use Magento\Framework\Model\Context;
use Magento\Framework\Model\ResourceModel\AbstractResource;
use Magento\Framework\Registry;

class UpsellProducts extends Value
{
    /**
     * @var JsonEncode
     */
    protected $jsonEncode;

    /**
     * UpsellProducts constructor.
     *
     * @param Context $context
     * @param Registry $registry
     * @param ScopeConfigInterface $config
     * @param TypeListInterface $cacheTypeList
     * @param JsonEncode $jsonEncode
     * @param AbstractResource|null $resource
     * @param AbstractDb|null $resourceCollection
     * @param array $data
     */
    public function __construct(
        Context $context,
        Registry $registry,
        ScopeConfigInterface $config,
        TypeListInterface $cacheTypeList,
        JsonEncode $jsonEncode,
        AbstractResource $resource = null,
        AbstractDb $resourceCollection = null,
        array $data = []
    ) {
        $this->jsonEncode = $jsonEncode;

        parent::__construct(
            $context,
            $registry,
            $config,
            $cacheTypeList,
            $resource,
            $resourceCollection,
            $data
        );
    }

    /**
     * Process data after load
     *
     * @return $this
     */
    public function afterLoad()
    {
        $value = $this->jsonEncode->unserialize($this->getValue());
        $this->setValue($value);

        return $this;
    }

    /**
     * Prepare data before save
     *
     * @return $this
     */
    public function beforeSave()
    {
        parent::beforeSave();

        $value = $this->getValue();
        unset($value['__empty']);
        $value = $this->jsonEncode->serialize($value);
        $this->setValue($value);

        return $this;
    }
}
