<?php
/**
 * @category    Kurufootwear
 * @package     Kurufootwear\Yotpo
 * @author      Jim McGowen <jim@kurufootwear.com>
 * @copyright   Copyright (c) 2018 KURU Footwear. All rights reserved.
 */

namespace Kurufootwear\Yotpo\Setup;

use Magento\Framework\Setup\UpgradeDataInterface;
use Magento\Framework\Setup\ModuleDataSetupInterface;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\App\Config\ConfigResource\ConfigInterface as config;

/**
 * Class UpgradeData
 * @package Kurufootwear\Yotpo\Setup
 */
class UpgradeData implements UpgradeDataInterface
{
    private $config;
    
    /**
     * UpgradeData constructor.
     *
     * @param Config $config
     */
    public function __construct(
        Config $config
    ) {
        $this->config = $config;
    }
    
    /**
     * @param ModuleDataSetupInterface $setup
     * @param ModuleContextInterface $context
     */
    public function upgrade(ModuleDataSetupInterface $setup, ModuleContextInterface $context)
    {
        $setup->startSetup();
    
        if (version_compare($context->getVersion(), '1.0.0', '<')) {
            $this->removeBadYotpoConfig($setup);
        }
    
        $setup->endSetup();
    }
    
    /**
     * Remove duplicate or unused config values to avoid ambiguity.
     * 
     * @param ModuleDataSetupInterface $setup
     */
    private function removeBadYotpoConfig(ModuleDataSetupInterface $setup)
    {
        $this->config->deleteConfig('yotpo/yotpo_general_group/yotpo_appkey', 'stores', 1);
        $this->config->deleteConfig('yotpo/yotpo_general_group/yotpo_secret', 'stores', 1);
        $this->config->deleteConfig('yotpo/yotpo_general_group/custom_order_status', 'stores', 1);
        $this->config->deleteConfig('yotpo/yotpo_general_group/disable_default_widget_position', 'stores', 1);
    }
}
