<?php
/**
 * @category    Kurufootwear
 * @package     Kurufootwear\Yotpo
 * @author      Jim McGowen <jim@kurufootwear.com>
 * @copyright   Copyright (c) 2018 KURU Footwear. All rights reserved.
 */

namespace Kurufootwear\Yotpo\Helper;

use Scandiweb\Yotpo\Helper\Data as YotpoHelper;

/**
 * Class YotpoApiClient
 * 
 * Wrapper for the Yotpo API
 * 
 * @package Kurufootwear\Yotpo\Helper
 */
class YotpoApiClient
{
    /**
     * Base URI for Yotpo API
     */
    const BASE_REST_URI = 'https://api.yotpo.com';
    
    /** @var string */
    private $token;
    
    /** @var string */
    private $appKey;
    
    /** @var YotpoHelper */
    private $yotpoHelper;
    
    /**
     * YotpoApiClient constructor.
     *
     * @param YotpoHelper $yotpoHelper
     */
    public function __construct(
        YotpoHelper $yotpoHelper
    ) {
        $this->yotpoHelper = $yotpoHelper;
    }
    
    /**
     * Retrieves products from Yotpo.
     * 
     * @param int $count
     *
     * @return array|bool
     * @throws \Exception
     */
    public function getProducts($count = 3000)
    {
        $this->initClient();
        
        $url = self::BASE_REST_URI . '/v1/apps/' . $this->appKey . '/products?count=' . $count . '&utoken=' . $this->token;
    
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array(
            'Content-Type: application/json'
        ));
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $response  = curl_exec($ch);
        curl_close($ch);
    
        $response = @json_decode($response, true);
        if (empty($response['status']['code']) || $response['status']['code'] != 200 || empty($response['products'])) {
            throw new \Exception("Error getting products from Yotpo: " . var_export($response, true));
        }

        return $response['products'];
    }
    
    /**
     * Wrapper for the Yotpo mass_update API endpoint. This will update 
     * the values for an existing product.
     * 
     * Data should be an array with the product entity_id as the index.
     * Minimum required values are "url" and "name"
     * 
     * Example:
     * [
     *   "437" => [
     *     "url" => "https://kurufootwear.com/blah",
     *     "name" => "Quantum",
     *     ...
     *   ],
     *   ...
     * ]
     * 
     * @param $products
     *
     * @throws \Exception
     */
    public function massUpdate($products)
    {
        $chunks = array_chunk($products, 200, true);
        foreach ($chunks as $chunk) {
            $this->initClient();
    
            $data = [
                'utoken' => $this->token,
                'products' => $chunk
            ];
            $data = json_encode($data);
    
            $url = self::BASE_REST_URI . '/apps/' . $this->appKey . '/products/mass_update';
    
            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, $url);
            curl_setopt($ch, CURLOPT_HTTPHEADER, array(
                    'Content-Type: application/json',
                    'Content-Length: ' . strlen($data)
                )
            );
            curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'PUT');
            curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            $response = curl_exec($ch);
            curl_close($ch);
    
            $response = @json_decode($response, true);
            if (empty($response['code']) || $response['code'] != 200) {
                throw new \Exception("Error updating products in Yotpo: " . var_export($response, true));
            }
        }
    }
    
    /**
     * Wrapper for the Yotpo mass_create API endpoint. This ill create 
     * a new product in Yotpo with the given ID.
     *
     * Data should be an array with the product entity_id as the index.
     * Minimum required values are "url" and "name"
     *
     * Example:
     * [
     *   "437" => [
     *     "url" => "https://kurufootwear.com/blah",
     *     "name" => "Quantum",
     *     ...
     *   ],
     *   ...
     * ]
     * 
     * @param $products
     *
     * @throws \Exception
     */
    public function massCreate($products)
    {
        $chunks = array_chunk($products, 200, true);
        foreach ($chunks as $chunk) {
    
            $this->initClient();
    
            $data = [
                'utoken' => $this->token,
                'products' => $chunk
            ];
            $data = json_encode($data);
    
            $url = self::BASE_REST_URI . '/apps/' . $this->appKey . '/products/mass_create';
    
            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, $url);
            curl_setopt($ch, CURLOPT_HTTPHEADER, array(
                    'Content-Type: application/json',
                    'Content-Length: ' . strlen($data)
                )
            );
            curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'POST');
            curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            $response = curl_exec($ch);
            curl_close($ch);
    
            $response = @json_decode($response, true);
            if (empty($response['code']) || $response['code'] != 200) {
                throw new \Exception("Error creating products in Yotpo: " . var_export($response, true));
            }
        }
    }
    
    /**
     * Retrieve all existing product groups.
     * 
     * @return mixed
     * @throws \Exception
     */
    public function getAllProductGroups()
    {
        $this->initClient();
    
        $url = self::BASE_REST_URI . '/v1//apps/' . $this->appKey . '/products_groups' . '?utoken=' . $this->token;;
    
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'GET');
        curl_setopt($ch, CURLOPT_HTTPHEADER, array(
            'Content-Type: application/json'
        ));
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $response  = curl_exec($ch);
        curl_close($ch);
    
        $response = @json_decode($response, true);
        if (empty($response['status']['code']) || $response['status']['code'] != 200 || !isset($response['response']['products_groups'])) {
            throw new \Exception("Error getting product groups from Yotpo: " . var_export($response, true));
        }
    
        return $response['response']['products_groups'];
    }
    
    /**
     * Create a product group.
     * 
     * @param string $groupName
     *
     * @throws \Exception
     */
    public function createProductGroup($groupName)
    {
        $this->initClient();
    
        $data = [
            'utoken' => $this->token,
            'group_name' => (string)$groupName
        ];
        $data = json_encode($data);
    
        $url = self::BASE_REST_URI . '/v1/apps/' . $this->appKey . '/products_groups';
    
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array(
                'Content-Type: application/json',
                'Content-Length: ' . strlen($data)
            )
        );
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'POST');
        curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $response = curl_exec($ch);
        curl_close($ch);
    
        $response = @json_decode($response, true);
        if (empty($response['status']['code']) || $response['status']['code'] != 200) {
            throw new \Exception("Error creating product group in Yotpo: " . var_export($response, true));
        }
    }
    
    /**
     * Adds products to a group.
     * 
     * @param $groupName
     * @param $productIdsToAdd
     * @param $productIdsToRemove
     *
     * @throws \Exception
     */
    public function modifyGroupProducts($groupName, $productIdsToAdd, $productIdsToRemove = [])
    {
        $this->initClient();
    
        $data = [
            'utoken' => $this->token,
            'product_ids_to_add' => $productIdsToAdd,
            'product_ids_to_remove' => $productIdsToRemove
        ];
        $data = json_encode($data);
    
        $url = self::BASE_REST_URI . '/v1/apps/' . $this->appKey . '/products_groups/' . urlencode($groupName);
    
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array(
                'Content-Type: application/json',
                'Content-Length: ' . strlen($data))
        );
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'PUT');
        curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $response  = curl_exec($ch);
        curl_close($ch);
    
        $response = @json_decode($response, true);
        if (empty($response['status']['code']) || $response['status']['code'] != 200) {
            throw new \Exception("Error updating products in Yotpo: " . var_export($response, true));
        }
    }
    
    /**
     * Retrieves the details for a product group.
     * 
     * @param $groupName
     *
     * @return mixed
     * @throws \Exception
     */
    public function getProductGroup($groupName)
    {
        $this->initClient();
    
        $url = self::BASE_REST_URI . '/v1//apps/' . $this->appKey . '/products_groups/' . urlencode($groupName) . '?utoken=' . $this->token;;
    
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'GET');
        curl_setopt($ch, CURLOPT_HTTPHEADER, array(
            'Content-Type: application/json'
        ));
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $response  = curl_exec($ch);
        curl_close($ch);
    
        $response = @json_decode($response, true);
        if (empty($response['status']['code']) || $response['status']['code'] != 200 || !isset($response['response']['products_group'])) {
            throw new \Exception("Error getting product groups from Yotpo: " . var_export($response, true));
        }
    
        return $response['response']['products_group'];
    }
    
    /**
     * Authorizes the Yotpo API and stores the auth token.
     * 
     * @throws \Exception
     */
    private function initClient()
    {
        /** @var \Zend_Http_Response $response */
        $response = null;
        
        if (empty($this->token)) {
            $this->appKey = $this->yotpoHelper->getYotpoAppKey();
            
            $authData = json_encode([
                'client_id' => $this->appKey,
                'client_secret' => $this->yotpoHelper->getYotpoAppSecret(),
                'grant_type' => 'client_credentials'
            ]);
    
            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, self::BASE_REST_URI . '/oauth/token');
            curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json'));
            curl_setopt($ch, CURLOPT_POST, 1);
            curl_setopt($ch, CURLOPT_POSTFIELDS, $authData);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            $response  = curl_exec($ch);
            curl_close($ch);
    
            $response = @json_decode($response, true);
            if (empty($response['access_token'])) {
                throw new \Exception("OAth error from Yotpo: " . var_export($response, true));
            }
            
            $this->token = $response['access_token'];
        }
    }
}
