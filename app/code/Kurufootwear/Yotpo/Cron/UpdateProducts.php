<?php
/**
 * @category    Kurufootwear
 * @package     Kurufootwear\Yotpo
 * @author      Jim McGowen <jim@kurufootwear.com>
 * @copyright   Copyright (c) 2018 KURU Footwear. All rights reserved.
 */
namespace Kurufootwear\Yotpo\Cron;

use Kurufootwear\Catalog\Model\ProductExtended;
use Magento\Framework\App\State;
use Magento\Framework\Logger\Monolog;
use Magento\Framework\App\Config\ScopeConfigInterface;
use Magento\Framework\Xml\Parser as XmlParser;
use Magento\Framework\App\Filesystem\DirectoryList;
use Kurufootwear\Yotpo\Helper\YotpoApiClient;
use Magento\Catalog\Model\ResourceModel\Product\CollectionFactory as ProductCollectionFactory;

/**
 * Class UpdateProducts
 * 
 * Syncs Yotpo products with the Google product feed using the Yotpo API.
 * 
 * @package Kurufootwear\Yotpo\Cron
 */
class UpdateProducts
{
    /** @var Monolog */
    protected $logger;

    /** @var State */
    protected $state;
    
    /** @var ScopeConfigInterface */
    protected $config;
    
    /** @var XmlParser */
    protected $xmlParser;
    
    /** @var DirectoryList */
    protected $directoryList;
    
    /** @var YotpoApiClient */
    protected $yotpoApiClient;
    
    /** @var ProductCollectionFactory */
    protected $productCollectionFactory;
    
    /**
     * UpdateProducts constructor.
     *
     * @param Monolog $logger
     * @param ScopeConfigInterface $config
     * @param State $state
     * @param XmlParser $xmlParser
     * @param DirectoryList $directoryList
     * @param YotpoApiClient $yotpoApiClient
     * @param ProductCollectionFactory $productCollectionFactory
     */
    public function __construct (
        Monolog $logger,
        ScopeConfigInterface $config,
        State $state,
        XmlParser $xmlParser,
        DirectoryList $directoryList,
        YotpoApiClient $yotpoApiClient,
        ProductCollectionFactory $productCollectionFactory
    ) {
        $this->logger = $logger;
        $this->config = $config;
        $this->state = $state;
        $this->xmlParser = $xmlParser;
        $this->directoryList = $directoryList;
        $this->yotpoApiClient = $yotpoApiClient;
        $this->productCollectionFactory = $productCollectionFactory;
    }
    
    /**
     * Entry point for the cron job.
     */
    public function execute()
    {
        $this->logger->addInfo('Yotpo cron: Starting Yotpo product sync...');
        
        try {
            // Read the feed
            $feedsDir = $this->directoryList->getRoot() . '/feeds';
            $gFeed = $this->xmlParser->load($feedsDir . '/GoogleShopping.xml');
            $gFeed = $gFeed->xmlToArray();
    
            // Read Yotpo data
            $yotpoProducts = $this->yotpoApiClient->getProducts();
    
            // Gather Yotpo product IDs for easy searching later
            $yotpoProductIds = [];
            foreach ($yotpoProducts as $yotpoProduct) {
                $yotpoProductIds[] = $yotpoProduct['external_product_id'];
            }
    
            $feedItemIds = [];
            $yotpoUpdate = [];
            $yotpoCreate = [];
            $yotpoProductGroups = [];
    
            // Iterate over feed items and add to yotpo update or create
            $gItems = $gFeed['rss']['_value']['channel']['item'];
            foreach ($gItems as $item) {
                // Using a collection here instead of the repository because it's WAY faster
                /** @var ProductExtended $product */
                $product = $this->productCollectionFactory
                    ->create()
                    ->addAttributeToSelect('parent_sku')
                    ->addFilter('sku', $item['g:mpn'])
                    ->load()
                    ->getFirstItem();
                
                if ($product->hasData()) {
                    // Already in Yotpo products?
                    if (in_array($product['entity_id'], $yotpoProductIds)) {
                        $yotpoUpdate[$product['entity_id']] = $this->feedToYotpo($product['entity_id'], $item);
                    }
                    else {
                        $yotpoCreate[$product['entity_id']] = $this->feedToYotpo($product['entity_id'], $item);
                    }
                    
                    $grandParent = $product->getGrandParent();
                    if (!empty($grandParent) && $grandParent->hasData()) {
                        $yotpoProductGroups[$grandParent->getId()][] = $product->getId();
                    }
                    else {
                        $this->logger->addWarning('Yotpo cron: Could not determine product group (grandparent) for sku ' . $product->getSku());
                    }
                }
                else {
                    $this->logger->addWarning('Yotpo cron: No product found for SKU ' . $item['g:mpn']);
                }
        
                $feedItemIds[] = $product['entity_id'];
            }
    
            // Update
            try {
                $this->logger->addInfo('Yotpo cron: Updating ' . count($yotpoUpdate) . ' products');
                $this->yotpoApiClient->massUpdate($yotpoUpdate);
            }
            catch (\Exception $e) {
                $this->logger->addError('Yotpo cron: Error updating products: ' . $e->getMessage());
            }
    
            // Create
            try {
                $this->logger->addInfo('Yotpo cron: Creating ' . count($yotpoCreate) . ' products');
                $this->yotpoApiClient->massCreate($yotpoCreate);
            }
            catch (\Exception $e) {
                $this->logger->addError('Yotpo cron: Error creating products: ' . $e->getMessage());
            }
            
            // Product groups
            $this->logger->addInfo('Yotpo cron: Creating and updating product groups');
            $existingProductGroupNames = [];
            $existingProductGroups = $this->yotpoApiClient->getAllProductGroups();
            foreach ($existingProductGroups as $group) {
                $existingProductGroupNames[] = $group['display_name'];
            }
            foreach ($yotpoProductGroups as $groupName => $productIds) {
                if (!in_array($groupName, $existingProductGroupNames)) {
                    try {
                        $this->yotpoApiClient->createProductGroup($groupName);
                    }
                    catch (\Exception $e) {
                        $this->logger->addError('Yotpo cron: Error creating product group ' . $groupName . ': ' . $e->getMessage());
                        continue;
                    }
                }
                
                try {
                    $productGroup = $this->yotpoApiClient->getProductGroup($groupName);
                    $productsAlreadyInGroup = [];
                    foreach ($productGroup['products_apps'] as $product) {
                        $productsAlreadyInGroup[] = $product['sku'];
                    }
                    
                    $productsToAdd = array_values(array_diff($productIds, $productsAlreadyInGroup));
                    $productToRemove = array_values(array_diff($productsAlreadyInGroup, $productIds));
                    
                    $this->yotpoApiClient->modifyGroupProducts($groupName, $productsToAdd, $productToRemove);
                }
                catch (\Exception $e) {
                    $this->logger->addError('Yotpo cron: Error modifying product group ' . $groupName . ': ' . $e->getMessage());
                }
            }
            
            $this->logger->addInfo('Yotpo cron: Finished Yotpo product sync');
        }
        catch (\Exception $e) {
            $this->logger->addCritical("Yotpo cron: Yotpo cron error: " . $e->getMessage());
        }
    }
    
    /**
     * Convert a Google feed item to a Yotpo product.
     * 
     * @param int $productId
     * @param array $feedItem
     *
     * @return array
     */
    private function feedToYotpo($productId, $feedItem)
    {
        $yotpoItem = [
            'currency' => 'USD',
            'specs' => [
                'upc' => ''
            ],
            'blacklisted' => false,
            'product_tags' => ''
        ];
        foreach ($feedItem as $key => $value) {
            switch($key) {
                case 'g:link':
                    $yotpoItem['url'] = $value;
                    break;
                case 'title':
                    $yotpoItem['name'] = $value;
                    break;
                case 'description':
                    $yotpoItem['description'] = $value;
                    break;
                case 'g:image_link':
                    $yotpoItem['image_url'] = $value;
                    break;
                case 'g:price':
                    $yotpoItem['price'] = trim(str_replace('USD', '', $value));
                    break;
                case 'g:mpn':
                    $yotpoItem['specs']['mpn'] = $value;
                    break;
                case 'g:brand':
                    $yotpoItem['specs']['brand'] = $value;
                    break;
                case 'g:item_group_id':
                    $yotpoItem['specs']['external_sku'] = $value;
                    break;
            }
        }
        
        $yotpoItem = $this->addProductTag($productId, $yotpoItem);
        
        return $yotpoItem;
    }
    
    /**
     * Sets the product_tags value for accessory products.
     * 
     * @param int $productId
     * @param array $yotpoItem
     *
     * @return mixed
     */
    private function addProductTag($productId, $yotpoItem)
    {
        $accessoryIds = [455, 2599, 2984];
        
        if (in_array($productId, $accessoryIds)) {
            $yotpoItem['product_tags'] = 'accessories';
        }
        
        return $yotpoItem;
    }
}
