<?php
/**
 * @vendor Kurufootwear
 * @module Kurufootwear_Revo
 * @author Kristaps Stalidzāns <info@scandiweb.com>
 * @copyright Copyright (c) 2017 Scandiweb, Ltd (http://scandiweb.com)
 * @license http://opensource.org/licenses/afl-3.0.php Academic Free License (AFL 3.0)
 */
namespace Kurufootwear\Revo\Block\Slider;

use Magento\Catalog\Block\Product\Context;
use Magento\Catalog\Model\Product\Visibility;
use Magento\Catalog\Model\ResourceModel\Product\CollectionFactory;
use Magento\Framework\Stdlib\DateTime\DateTime;
use Magento\Framework\Url\Helper\Data;
use Magento\Reports\Model\Event\TypeFactory;
use Magento\Reports\Model\ResourceModel\Product\CollectionFactory as ProductCollection;
use Trive\Revo\Model\ProductSliderFactory;
use Magento\Catalog\Api\ProductRepositoryInterface;
use Magento\Catalog\Model\CategoryRepository;

class Items extends \Trive\Revo\Block\Slider\Items
{
    /**
     * Product slider template
     */
    protected $_template = 'Kurufootwear_Revo::slider/items.phtml';
    /**
     * @var ProductRepositoryInterface
     */
    private $productRepository;

    /**
     * @var CategoryRepository
     */
    protected $categoryRepository;

    /**
     * Items constructor.
     * @param Context $context
     * @param CollectionFactory $productsCollectionFactory
     * @param Visibility $catalogProductVisibility
     * @param ProductCollection $reportsCollectionFactory
     * @param ProductSliderFactory $productSliderFactory
     * @param DateTime $dateTime
     * @param TypeFactory $eventTypeFactory
     * @param Data $urlHelper
     * @param ProductRepositoryInterface $productRepository
     * @param CategoryRepository $categoryRepository
     * @param array $data
     */
    public function __construct(
        Context $context,
        CollectionFactory $productsCollectionFactory,
        Visibility $catalogProductVisibility,
        ProductCollection $reportsCollectionFactory,
        ProductSliderFactory $productSliderFactory,
        DateTime $dateTime,
        TypeFactory $eventTypeFactory,
        Data $urlHelper,
        ProductRepositoryInterface $productRepository,
        CategoryRepository $categoryRepository,
        array $data = []
    ) {
        $this->productRepository = $productRepository;
        $this->categoryRepository = $categoryRepository;

        parent::__construct(
            $context,
            $productsCollectionFactory,
            $catalogProductVisibility,
            $reportsCollectionFactory,
            $productSliderFactory,
            $dateTime,
            $eventTypeFactory,
            $urlHelper,
            $data
        );
    }

    /**
     * Returns product color
     *
     * @param $item
     * @return mixed
     */
    public function getProductColor($item)
    {
        $productColor = $item->getData('fecolor');
        $productColorConfigurable = $item->getData('product_color_data');
        $color = null;

        if ($productColor != null) {
            $color = $productColor;
        } else if ($productColorConfigurable != null) {
            $color = $productColorConfigurable;
        }

        return $color;
    }

    /**
     * Returns product id if prent sku is present then gets its id for yotpo
     *
     * @param $item
     * @return int|null
     */
    public function getProductId($item)
    {
        $productId = $item->getId();
        $productParentSku = $item->getData('parent_sku');

        if ($productParentSku != null) {
            try {
                $productId = $this->productRepository->get($productParentSku)->getId();
            } catch (\Exception $e) {

            }
        }

        return $productId;
    }

    /**
     * Returns the product category name
     *
     * @param $categoryIds
     * @return string
     */
    public function getProductCategoryName($categoryIds)
    {
        $categoryName = '';

        foreach($categoryIds as $categoryId) {
            $category = $this->categoryRepository->get($categoryId);
            $urlKey = $category->getUrlKey();

            if ($urlKey == 'womens-shoes') {
                $categoryName = 'Women\'s';
            } else if ($urlKey == 'mens-shoes') {
                $categoryName = 'Men\'s';
            }
        }

        return $categoryName;
    }

    /**
     * @param \Magento\Catalog\Model\Product $product
     * @param array $additional
     * @return string
     */
    public function getProductUrl($product, $additional = [])
    {
        if ($this->hasProductUrl($product)) {
            if (!isset($additional['_escape'])) {
                $additional['_escape'] = true;
            }

            $parentSku = $product->getData('parent_sku');
            $productDuplicate = $product->getData('product_duplicate');
            $productColorData = $product->getData('product_color_data');
            $url = $product->getUrlModel()->getUrl($product, $additional);

            /**
             * Checks if parent product is present with color data un duplicate check
             */
            if ($parentSku && $productColorData && !is_null($productDuplicate)) {
                try {
                    $product = $this->productRepository->get($parentSku);
                    $url = $product->getUrlModel()->getUrl($product, $additional) . '?color=' . $productColorData;
                } catch (\Exception $e) {}
            }

            return $url;
        }

        return '#';
    }

    /**
     * This is needed to take the correct templates
     *
     * @return string
     */
    protected function _toHtml()
    {
        $this->setModuleName($this->extractModuleName('Trive\Revo\Block\Slider\Items'));

        return parent::_toHtml();
    }
}
