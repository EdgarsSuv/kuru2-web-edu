<?php
/**
 * @vendor Kurufootwear
 * @module Kurufootwear_Revo
 * @author Kristaps Stalidzāns <info@scandiweb.com>
 * @copyright Copyright (c) 2017 Scandiweb, Ltd (http://scandiweb.com)
 * @license http://opensource.org/licenses/afl-3.0.php Academic Free License (AFL 3.0)
 */
namespace Kurufootwear\Revo\Block\Adminhtml\Slider\Edit\Tab;

use \Magento\Backend\Block\Widget\Grid\Extended as Extended;

class Products extends \Trive\Revo\Block\Adminhtml\Slider\Edit\Tab\Products
{
    /**
     * Returns unfiltered product collection
     * @return \Magento\Backend\Block\Widget\Grid
     */
    protected function _prepareCollection()
    {
        if ($this->getSlider()->getSliderId()) {
            $this->setDefaultFilter(['in_slider' => 1]);
        }

        $collection = $this->_productFactory->create()->getCollection();
        $collection->addAttributeToSelect('name')
            ->addAttributeToSelect('sku')
            ->addAttributeToSelect('price')
            ->joinField(
                'position',
                'trive_revo_products',
                'position',
                'product_id=entity_id',
                'slider_id=' . (int)$this->getRequest()->getParam('id', 0),
                'left'
            );

        $this->setCollection($collection);
        $this->getCollection()->addWebsiteNamesToResult();

        return Extended::_prepareCollection();
    }
}
