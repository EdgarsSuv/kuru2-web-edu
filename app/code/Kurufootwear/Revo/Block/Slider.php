<?php
/**
 * @vendor Kurufootwear
 * @module Kurufootwear_Revo
 * @author Kristaps Stalidzāns <info@scandiweb.com>
 * @copyright Copyright (c) 2017 Scandiweb, Ltd (http://scandiweb.com)
 * @license http://opensource.org/licenses/afl-3.0.php Academic Free License (AFL 3.0)
 */
namespace Kurufootwear\Revo\Block;

class Slider extends \Trive\Revo\Block\Slider
{
    /**
     *  Add child sliders block
     *
     * @param \Trive\Revo\Model\ResourceModel\ProductSlider\Collection $sliderCollection
     *
     * @return $this
     */
    public function setSlider($sliderCollection)
    {
        foreach ($sliderCollection as $slider):
            $this->_coreRegistry->unregister('slider_id');
            $this->_coreRegistry->register('slider_id',$slider->getId());
            $this->_coreRegistry->unregister('enable_swatches');

            if ($slider->getEnableSwatches()){
                $this->_coreRegistry->register('enable_swatches',1);
                $this->swatchesVld = true;
            }

            if ($slider->getEnableAjaxcart()){
                $this->ajaxcartVld = true;
            }

            $this->append($this->getLayout()
                ->createBlock('\Kurufootwear\Revo\Block\Slider\Items')
                ->setSlider($slider));

        endforeach;

        $this->addSwatchesCss();
        $this->addAjaxCartJs();

        return $this;
    }
}
