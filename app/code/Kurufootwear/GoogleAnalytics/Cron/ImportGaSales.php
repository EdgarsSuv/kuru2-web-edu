<?php
/**
 * @category    Kurufootwear
 * @package     Kurufootwear\GoogleAnalytics
 * @author      Jim McGowen <jim@kurufootwear.com>
 * @copyright   Copyright (c) 2018 KURU Footwear. All rights reserved.
 */
namespace Kurufootwear\GoogleAnalytics\Cron;

use Magento\Framework\App\State;
use Magento\Framework\Logger\Monolog;
use Magento\Framework\App\Config\ScopeConfigInterface;
use Kurufootwear\GoogleAnalytics\Model\GaSalesRepository;
use Google_Client;
use Google_Service_AnalyticsReporting;
use Google_Service_AnalyticsReporting_GetReportsRequest;
use Google_Service_AnalyticsReporting_DateRange;
use Google_Service_AnalyticsReporting_Metric;
use Google_Service_AnalyticsReporting_Dimension;
use Google_Service_AnalyticsReporting_OrderBy;
use Google_Service_AnalyticsReporting_ReportRequest;
use Google_Service_AnalyticsReporting_GetReportsResponse;
use Magento\Framework\Exception\LocalizedException;

/**
 * Class ImportGaSales
 * 
 * Cron task for getting sales data from the Google Analytics API and 
 * importing it into the DB.
 * 
 * @package Kurufootwear\GoogleAnalytics\Cron
 */
class ImportGaSales
{
    protected $gaSalesRepository;
    
    /** @var Monolog */
    protected $logger;

    /** @var State */
    protected $state;
    
    /** @var ScopeConfigInterface */
    protected $config;
    
    /** @var \Google_Service_AnalyticsReporting */
    protected $analytics;
    
    /** @var \Google_Service_AnalyticsReporting_ReportRequest */
    protected $request;
    
    private $retries = 0;
    
    /**
     * ImportGaSales constructor.
     *
     * @param GaSalesRepository $gaSalesRepository
     * @param Monolog $logger
     * @param ScopeConfigInterface $config
     * @param State $state
     */
    public function __construct (
        GaSalesRepository $gaSalesRepository,
        Monolog $logger,
        ScopeConfigInterface $config,
        State $state
    ) {
        $this->gaSalesRepository = $gaSalesRepository;
        $this->logger = $logger;
        $this->config = $config;
        $this->state = $state;
    }

    /**
     * Entry point for the cron job.
     * 
     * On the first run it will import all historical data. This will take several hours.
     * After that it will retrieve data from the previous day (yesterday) therefore the 
     * cron should be scheduled to run once a day.
     * 
     * @throws \Exception
     */
    public function execute()
    {
        // This is needed because the initial import will take several hours
        $executionTime = ini_get('max_execution_time');
        ini_set('max_execution_time', 0);
        
        $gaSales = $this->gaSalesRepository->getMostRecent();
        if (count($gaSales->getItems()) == 0) {
            // On first run we want to import all historical sales date
            $endDate = date("Y-m-d", strtotime('-1 days'));
            $this->run('2011-01-01', $endDate);
        }
        else {
            $this->run();
        }
    
        ini_set('max_execution_time', $executionTime);
    }
    
    /**
     * Retrieves and imports sales data from Google Analytics.
     * 
     * If $startDay or $endDay are null then reports will be run
     * for the previous day (yesterday).
     * 
     * @param null $startDay
     * @param null $endDay
     */
    public function run($startDay = null, $endDay = null)
    {
        try {
            $this->logger->addInfo('Starting Google API sales import.');
            $this->initAnalytics();
            $this->initRequest($startDay, $endDay);
            
            /** @var \Google_Service_AnalyticsReporting_Report $report */
            $report = $this->doRequest();
            $this->logger->addInfo('  Got ' . $report->getData()->getRowCount() . ' rows');
            $this->importDataBulk($report);
            
            while (!empty($report->getNextPageToken())) {
                $this->logger->addInfo('  Next page, offset = ' . $report->getNextPageToken());
                $this->request->setPageToken($report->getNextPageToken());
                $report = $this->doRequest();
                $this->importDataBulk($report);
            }
            
            $this->logger->addInfo('Finished Google API sales import.');
        }
        catch (\Exception $e) {
            $this->logger->addCritical($e->getMessage());
        }
    }
    
    /**
     * Imports data from the GA report into the DB.
     * 
     * @param \Google_Service_AnalyticsReporting_Report $report
     * @throws LocalizedException
     */
    private function importDataBulk($report)
    {
        $data = [];
        $header = $report->getColumnHeader();
        $dimensionHeaders = $header->getDimensions();
        $rows = $report->getData()->getRows();
        foreach ($rows as $row) {
            $dimensions = $row->getDimensions();
            $rowData = [];
            for ($i = 0; $i < count($dimensionHeaders) && $i < count($dimensions); $i++) {
                switch ($dimensionHeaders[$i]) {
                    case 'ga:source' :
                        $rowData['source'] = $dimensions[$i];
                        break;
                    case 'ga:campaign':
                        $rowData['campaign'] = $dimensions[$i];
                        break;
                    case 'ga:medium':
                        $rowData['medium'] = $dimensions[$i];
                        break;
                    case 'ga:transactionId':
                        $rowData['order_number'] = $dimensions[$i];
                        break;
                    case 'ga:channelGrouping':
                        $rowData['channel_grouping'] = $dimensions[$i];
                        break;
                    case 'ga:date':
                        $rowData['date'] = substr($dimensions[$i], 0, 4) . '-' . 
                                           substr($dimensions[$i], 4, 2) . '-' . 
                                           substr($dimensions[$i], 6, 2);
                        break;
                }
            }
        
            $data[] = $rowData;
        }
        
        $this->gaSalesRepository->insertMultiple($data);
    }
    
    /**
     * Retrieves the sales reports from the Google Analytics API.
     * 
     * @return Google_Service_AnalyticsReporting_GetReportsResponse
     * @throws \Google_Exception
     */
    private function doRequest()
    {
        $result = null;
        
        try {
            $body = new Google_Service_AnalyticsReporting_GetReportsRequest();
            $body->setReportRequests(array($this->request));
    
            /** @var \Google_Service_AnalyticsReporting_GetReportsResponse $result */
            $result = $this->analytics->reports->batchGet($body);
        }
        catch (\Google_Exception $e) {
            if ($e->getCode() == 503 && $this->retries < 3) {
                $this->logger->addWarning('  Got 503 error, retrying in ' . ++$this->retries . ' seconds...');
                sleep($this->retries);
                return $this->doRequest();
            }
            else {
                throw $e;
            }
        }
    
        $this->retries = 0;
        return $result->getReports()[0];
    }
    
    /**
     * Initializes the request for the Google Analytics API.
     * If $startDay or $endDay are null then $startDay and $endDay will be yesterday.
     * 
     * @param null $startDay
     * @param null $endDay
     */
    private function initRequest($startDay = null, $endDay = null)
    {
        if ($startDay === null || $endDay === null) {
            $startDay = date("Y-m-d", strtotime('-1 days'));
            $endDay = $startDay;
        }
        else {
            $startDay = date("Y-m-d", strtotime($startDay));
            $endDay = date("Y-m-d", strtotime($endDay));
        }
        
        $this->logger->addInfo('  Date range: ' . $startDay . ' to ' . $endDay);
        
        $dateRange = new Google_Service_AnalyticsReporting_DateRange();
        $dateRange->setStartDate($startDay);
        $dateRange->setEndDate($endDay);
    
        $sessions = new Google_Service_AnalyticsReporting_Metric();
        $sessions->setExpression("ga:sessions");
        $sessions->setAlias("sessions");
    
        $transaction = new Google_Service_AnalyticsReporting_Metric();
        $transaction->setExpression("ga:transactionRevenue");
        $transaction->setAlias("Revenue");
    
        $transactionID = new Google_Service_AnalyticsReporting_Dimension();
        $transactionID->setName("ga:transactionId");
    
        $source = new Google_Service_AnalyticsReporting_Dimension();
        $source->setName("ga:source");
    
        $campaign = new Google_Service_AnalyticsReporting_Dimension();
        $campaign->setName("ga:campaign");
    
        $medium = new Google_Service_AnalyticsReporting_Dimension();
        $medium->setName("ga:medium");
    
        $channelGrouping = new Google_Service_AnalyticsReporting_Dimension();
        $channelGrouping->setName("ga:channelGrouping");
        
        $date = new Google_Service_AnalyticsReporting_Dimension();
        $date->setName('ga:date');
        
        $orderByDate = new Google_Service_AnalyticsReporting_OrderBy();
        $orderByDate->setFieldName('ga:date');
        $orderByDate->setSortOrder('ASCENDING');
    
        // Create the ReportRequest object.
        $this->request = new Google_Service_AnalyticsReporting_ReportRequest();
        $viewId = $this->config->getValue('kurufootwear_googleanalytics/view_id');
        $this->request->setViewId($viewId);
        $this->request->setDateRanges($dateRange);
        $this->request->setDimensions([
            $source,
            $campaign,
            $medium,
            $transactionID,
            $channelGrouping,
            $date
        ]);
        $this->request->setMetrics([
            $sessions,
            $transaction
        ]);
        $this->request->setOrderBys([
            $orderByDate
        ]);
    }
    
    /**
     * Initializes the the Google API for analytics. 
     * 
     * @throws \Google_Exception
     */
    private function initAnalytics()
    {
        $client = new Google_Client();
        $client->setApplicationName("KURU Analytics Reporting");
        $authConfig = json_decode($this->config->getValue('kurufootwear_googleanalytics/json_config'), true);
        if (empty($authConfig)) {
            throw new \Google_Exception("Abort: Missing Google Analytics configuration.");
        }
        $client->setAuthConfig($authConfig);
        $client->setScopes(['https://www.googleapis.com/auth/analytics.readonly']);
        $this->analytics = new Google_Service_AnalyticsReporting($client);
    }
}
