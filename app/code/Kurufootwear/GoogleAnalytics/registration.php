<?php
/**
 * @vendor      Kurufootwear
 * @module      Kurufootwear\GoogleAnalytics
 * @author      Jim McGowen <jim@kurufootwear.com>
 * @copyright   Copyright (c) 2018 KURU Footwear. All rights reserved.
 */
\Magento\Framework\Component\ComponentRegistrar::register(
    \Magento\Framework\Component\ComponentRegistrar::MODULE,
    'Kurufootwear_GoogleAnalytics',
    __DIR__
);
