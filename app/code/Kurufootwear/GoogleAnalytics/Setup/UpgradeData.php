<?php
/**
 * @category    Kurufootwear
 * @package     Kurufootwear\GoogleAnalytics
 * @author      Jim McGowen <jim@kurufootwear.com>
 * @copyright   Copyright (c) 2018 KURU Footwear. All rights reserved.
 */
namespace Kurufootwear\GoogleAnalytics\Setup;

use Magento\Framework\Setup\UpgradeDataInterface;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\ModuleDataSetupInterface;

/**
 * Class UpgradeData
 *
 * @package Kurufootwear\GoogleAnalytics\Setup
 */
class UpgradeData implements UpgradeDataInterface
{
    const GA_SALES_TABLE_NAME = 'kuru_ga_sales';
    
    /**
     * @param ModuleDataSetupInterface $setup
     * @param ModuleContextInterface $context
     */
    public function upgrade(ModuleDataSetupInterface $setup, ModuleContextInterface $context)
    {
        $setup->startSetup();
    
        if (version_compare($context->getVersion(), '1.0.1', '<')) {
            $this->truncateGaSalesTable($setup);
        }
    
        $setup->endSetup();
    }
    
    /**
     * Truncate the kuru_ga_sales table so the initial import is triggered.
     * 
     * @param ModuleDataSetupInterface $setup
     */
    private function truncateGaSalesTable(ModuleDataSetupInterface $setup)
    {
        $tableName = $setup->getTable(self::GA_SALES_TABLE_NAME);
        if ($setup->getConnection()->isTableExists($tableName)) {
            $setup->getConnection()->truncateTable($tableName);
        }
    }
}
