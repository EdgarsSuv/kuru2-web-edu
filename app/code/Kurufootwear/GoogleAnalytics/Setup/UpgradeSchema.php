<?php
/**
 * @category    Kurufootwear
 * @package     Kurufootwear\GoogleAnalytics
 * @author      Jim McGowen <jim@kurufootwear.com>
 * @copyright   Copyright (c) 2018 KURU Footwear. All rights reserved.
 */
namespace Kurufootwear\GoogleAnalytics\Setup;

use Magento\Framework\Setup\UpgradeSchemaInterface;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\SchemaSetupInterface;

/**
 * Class UpgradeSchema
 *
 * @package Kurufootwear\GoogleAnalytics\Setup
 */
class UpgradeSchema implements UpgradeSchemaInterface
{
    const GA_SALES_TABLE_NAME = 'kuru_ga_sales';
    
    /**
     * @param SchemaSetupInterface $setup
     * @param ModuleContextInterface $context
     */
    public function upgrade(SchemaSetupInterface $setup, ModuleContextInterface $context)
    {
        $setup->startSetup();
    
        if (version_compare($context->getVersion(), '1.0.2', '<')) {
            $this->removeUniqueOrderIndex($setup);
        }
    
        $setup->endSetup();
    }
    
    /**
     * Remove the unique order number index.
     * 
     * @param SchemaSetupInterface $setup
     */
    private function removeUniqueOrderIndex(SchemaSetupInterface $setup)
    {
        $tableName = $setup->getTable(self::GA_SALES_TABLE_NAME);
        if ($setup->getConnection()->isTableExists($tableName)) {
            // Could go by either name it seems
            $setup->getConnection()->dropIndex($tableName, 'order_number_UNIQUE');
            $setup->getConnection()->dropIndex($tableName, 'KURU_GA_SALES_ORDER_NUMBER_UNIQUE');
        }
    }
}
