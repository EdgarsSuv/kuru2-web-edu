<?php
/**
 * @category    Kurufootwear
 * @package     Kurufootwear\GoogleAnalytics
 * @author      Jim McGowen <jim@kurufootwear.com>
 * @copyright   Copyright (c) 2018 KURU Footwear. All rights reserved.
 */
namespace Kurufootwear\GoogleAnalytics\Setup;

use Magento\Framework\Setup\InstallSchemaInterface;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\SchemaSetupInterface;
use Magento\Framework\DB\Ddl\Table;

/**
 * Class InstallSchema
 *
 * @package Kurufootwear\GoogleAnalytics\Setup
 */
class InstallSchema implements InstallSchemaInterface
{
    const GA_SALES_TABLE_NAME = 'kuru_ga_sales';
    
    /**
     * @param SchemaSetupInterface $setup
     * @param ModuleContextInterface $context
     *
     * @throws \Zend_Db_Exception
     */
    public function install(SchemaSetupInterface $setup, ModuleContextInterface $context)
    {
        $setup->startSetup();
        
        $this->addGaSalesTable($setup);
    
        $setup->endSetup();
    }
    
    /**
     * Create the kuru_ga_sales table.
     * 
     * @param SchemaSetupInterface $setup
     *
     * @throws \Zend_Db_Exception
     */
    private function addGaSalesTable(SchemaSetupInterface $setup)
    {
        $tableName = $setup->getTable(self::GA_SALES_TABLE_NAME);
        if ($setup->getConnection()->isTableExists($tableName) != true) {
            $table = $setup->getConnection()
                ->newTable($tableName)
                ->addColumn(
                    'id',
                    Table::TYPE_INTEGER,
                    null,
                    ['identity' => true, 'unsigned' => true, 'nullable' => false, 'primary' => true],
                    'Record ID'
                )
                ->addColumn(
                    'order_number',
                        Table::TYPE_TEXT,
                    10,
                    ['nullable' => false],
                    'Order Number'
                )
                ->addColumn(
                    'date',
                    Table::TYPE_DATE,
                    null,
                    ['nullable' => false],
                    'Order Date'
                )
                ->addColumn(
                    'source',
                    Table::TYPE_TEXT,
                    128,
                    ['nullable' => false],
                    'Source'
                )
                ->addColumn(
                    'medium',
                    Table::TYPE_TEXT,
                    128,
                    ['nullable' => false],
                    'Medium'
                )
                ->addColumn(
                    'campaign',
                    Table::TYPE_TEXT,
                    128,
                    ['nullable' => false],
                    'Campaign'
                )
                ->addColumn(
                    'channel_grouping',
                    Table::TYPE_TEXT,
                    128,
                    ['nullable' => false],
                    'Channel Grouping'
                )
                ->addIndex(
                    $setup->getIdxName(
                        self::GA_SALES_TABLE_NAME,
                        ['order_number_UNIQUE'],
                        \Magento\Framework\DB\Adapter\AdapterInterface::INDEX_TYPE_UNIQUE
                    ),
                    ['order_number'],
                    ['type' => \Magento\Framework\DB\Adapter\AdapterInterface::INDEX_TYPE_UNIQUE]
                )
                ->setComment('Google Analytics Sales Table')
                ->setOption('type', 'InnoDB')
                ->setOption('charset', 'utf8');
            $setup->getConnection()->createTable($table);
        }
    }
}
