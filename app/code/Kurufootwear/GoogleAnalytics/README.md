This module relies on the Google API PHP library.
However, there is a conflict between the latest library and Magento 2.1.
Magento 2.1 requires monolog v2.16 and the Google API library requires monolog 2.17.
To get around this problem I have forked the Google API Library and changed the 
monolog requirement to 1.16. When we update Magento to 2.2 we can go back to the
main repository. ie. change composer.json from 

`"google/apiclient": "dev-master"`

to

`"google/apiclient": "^2.0"`
