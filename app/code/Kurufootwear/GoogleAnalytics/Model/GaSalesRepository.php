<?php
/**
 * @category    Kurufootwear
 * @package     Kurufootwear\GoogleAnalytics
 * @author      Jim McGowen <jim@kurufootwear.com>
 * @copyright   Copyright (c) 2018 KURU Footwear. All rights reserved.
 */

namespace Kurufootwear\GoogleAnalytics\Model;

use Kurufootwear\Framework\Model\AbstractRepository;
use Magento\Framework\Api\SearchResultsInterfaceFactory;
use Magento\Framework\ObjectManagerInterface;
use Magento\Framework\Api\SearchCriteriaBuilder;
use Magento\Framework\Api\SortOrder;
use Magento\Framework\Api\SortOrderFactory;

/**
 * Class GaSalesRepository
 * @package Kurufootwear\GoogleAnalytics\Model
 */
class GaSalesRepository extends AbstractRepository
{
    /** @var GaSalesFactory  */
    protected $gaSalesFactory;
    
    /** @var SearchCriteriaBuilder  */
    protected $searchCriteriaBuilder;
    
    /** @var SortOrderFactory  */
    protected $sortOrderFactory;
    
    /**
     * GaSalesRepository constructor.
     *
     * @param ObjectManagerInterface $objectManager
     * @param SearchResultsInterfaceFactory $searchResultsFactory
     * @param GaSalesFactory $gaSalesFactory
     * @param SearchCriteriaBuilder $searchCriteria
     * @param SortOrderFactory $sortOrderFactory
     */
    public function __construct(
        ObjectManagerInterface $objectManager, 
        SearchResultsInterfaceFactory $searchResultsFactory,
        GaSalesFactory $gaSalesFactory,
        SearchCriteriaBuilder $searchCriteria,
        SortOrderFactory $sortOrderFactory
    ) {
        $this->gaSalesFactory = $gaSalesFactory;
        $this->searchCriteriaBuilder = $searchCriteria;
        $this->sortOrderFactory = $sortOrderFactory;
        parent::__construct($objectManager, $searchResultsFactory);
    }
    
    /**
     * GaSalesRepository internal constructor.
     */
    public function _construct() {
        $this->_init('Kurufootwear\GoogleAnalytics\Model\GaSales', 'Kurufootwear\GoogleAnalytics\Model\ResourceModel\GaSales');
    }
    
    /**
     * Inserts multiple records into the DB in bulk.
     *
     * @param $data Column-value pairs or array of Column-value pairs.
     *
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function insertMultiple($data)
    {
        $tableName = $this->getResource()->getMainTable();
        $this->getResource()->getConnection()->insertMultiple($tableName, $data);
    }
    
    /**
     * Loads a GaSales object given an order number (increment id).
     * 
     * @param $orderNumber
     *
     * @return GaSales
     */
    public function getByOrderNumber($orderNumber)
    {
        /** @var GaSales $gaSales */
        $gaSales = $this->gaSalesFactory->create();
        $this->getResource()->load($gaSales, $orderNumber, 'order_number');
        return $gaSales;
    }
    
    /**
     * Retrieves the most recent record by id.
     * 
     * @return \Magento\Framework\Api\SearchResultsInterface
     */
    public function getMostRecent()
    {
        /** @var SortOrder $sortOrder */
        $sortOrder = $this->sortOrderFactory->create();
        $sortOrder->setField('id');
        $sortOrder->setDirection(SortOrder::SORT_DESC);
        
        $searchCriteria = $this->searchCriteriaBuilder
            ->setSortOrders([$sortOrder])
            ->setPageSize(1)
            ->create();
        
        $gaSales = $this->getList($searchCriteria);
        
        return $gaSales;
    }
}
