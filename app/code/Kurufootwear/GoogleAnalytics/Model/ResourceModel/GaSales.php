<?php
/**
 * @category    Kurufootwear
 * @package     Kurufootwear\GoogleAnalytics
 * @author      Jim McGowen <jim@kurufootwear.com>
 * @copyright   Copyright (c) 2018 KURU Footwear. All rights reserved.
 */

namespace Kurufootwear\GoogleAnalytics\Model\ResourceModel;

use Magento\Framework\Model\ResourceModel\Db\AbstractDb;

/**
 * Class GaSales
 * @package Kurufootwear\GoogleAnalytics\Model\ResourceModel
 */
class GaSales extends AbstractDb
{
    /**
     * Constructor.
     */
    public function _construct()
    {
        $this->_init('kuru_ga_sales', 'id');
    }
}
