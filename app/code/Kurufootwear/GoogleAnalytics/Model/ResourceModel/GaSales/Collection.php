<?php
/**
 * @category    Kurufootwear
 * @package     Kurufootwear\GoogleAnalytics
 * @author      Jim McGowen <jim@kurufootwear.com>
 * @copyright   Copyright (c) 2018 KURU Footwear. All rights reserved.
 */

namespace Kurufootwear\GoogleAnalytics\Model\ResourceModel\GaSales;

use Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection;

/**
 * Class Collection
 * @package Kurufootwear\GoogleAnalytics\Model\ResourceModel\GaSales
 */
class Collection extends AbstractCollection
{
    /**
     * Constructor
     */
    protected function _construct()
    {
        $this->_init('Kurufootwear\GoogleAnalytics\Model\GaSales','Kurufootwear\GoogleAnalytics\Model\ResourceModel\GaSales');
    }
}
