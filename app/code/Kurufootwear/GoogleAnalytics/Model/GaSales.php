<?php
/**
 * @category    Kurufootwear
 * @package     Kurufootwear\GoogleAnalytics
 * @author      Jim McGowen <jim@kurufootwear.com>
 * @copyright   Copyright (c) 2018 KURU Footwear. All rights reserved.
 */
namespace Kurufootwear\GoogleAnalytics\Model;

use Magento\Framework\Model\AbstractModel;

/**
 * Class GaSales
 * @package Kurufootwear\GoogleAnalytics\Model
 */
class GaSales extends AbstractModel
{
    /**
     * Constructor
     */
    public function _construct()
    {
        $this->_init('Kurufootwear\GoogleAnalytics\Model\ResourceModel\GaSales');
    }
}
