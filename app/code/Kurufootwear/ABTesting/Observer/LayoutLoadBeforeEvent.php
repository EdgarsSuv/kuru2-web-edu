<?php
/**
 * @category    Kurufootwear
 * @package     Kurufootwear\ABTesting
 * @author      Jim McGowewn <jim@kurufootwear.com>
 * @copyright   Copyright (c) 2017 KURU Footwear
 */

namespace Kurufootwear\ABTesting\Observer;

use Magento\Framework\Event\ObserverInterface;
use Magento\Framework\App\RequestInterface;

/**
 * Observer for the layout_load_before event
 *
 * This adds layout file handles for a/b testing. It checks the URL param 'v' for a set of 1 or more integers
 * representing individual changes in the default layout and corresponding to additional layout files. The 
 * naming convention for the addtional layout files is 
 * 
 * controller_action_ab_[int]+[int]+...
 * 
 * where [int] is any integer value
 * 
 * For example if a product URL is
 *
 * https://www.domain.com/product-x.html?v=1+3
 *
 * layout file handles will be added for catalog_product_view_ab_1.xml and catalog_product_view_ab_3.xml.
 * Each of these files would contain a change to the base layout catalog_product_view.xml. Any combination
 * of changes can be specified by adjusting the 'v' parameter in the URL, such as
 *
 * https://www.domain.com/product-x.html?v=1
 * https://www.domain.com/product-x.html?v=1+2
 * https://www.domain.com/product-x.html?v=1+3
 * https://www.domain.com/product-x.html?v=2+3
 * https://www.domain.com/product-x.html?v=3
 *
 * etc.
 *
 * @param \Magento\Framework\Event\Observer $observer
 */
class LayoutLoadBeforeEvent implements ObserverInterface
{
    const AB_QUERY_PARAM_DELIMITER = ' ';
    
    protected $request;
    
    public function __construct(
        RequestInterface $request
    )
    {
        $this->request = $request;
    }
    
    public function execute(\Magento\Framework\Event\Observer $observer)
    {
        $queryParam = $this->request->getParam('v');
        if (!empty($queryParam)) {
            /** @var \Magento\Framework\View\Layout $layout */
            $layout = $observer->getData('layout');
            $changes = explode(self::AB_QUERY_PARAM_DELIMITER, $queryParam);
            foreach ($changes as $change) {
                $handle = $observer->getFullActionName() . '_ab_' . $change;
                $layout->getUpdate()->addHandle($handle);
            }
        }
    }
}
