<?php
/**
 * @category    Kurufootwear
 * @package     Kurufootwear\ABTesting
 * @author      Jim McGowewn <jim@kurufootwear.com>
 * @copyright   Copyright (c) 2017 KURU Footwear
 */
\Magento\Framework\Component\ComponentRegistrar::register(
    \Magento\Framework\Component\ComponentRegistrar::MODULE,
    'Kurufootwear_ABTesting',
    __DIR__
);
