<?php
/**
 * @vendor Kurufootwear
 * @module Kurufootwear_Sales
 * @author Aleksejs Baranovs <info@scandiweb.com>
 * @copyright Copyright (c) 2018 KURU Footwear. All rights reserved.
 */

namespace Kurufootwear\Sales\Plugin;

use Closure;
use Kurufootwear\Payment\Logger\RefundErrorLogger;
use Magento\Payment\Gateway\Command\CommandException;
use Magento\Sales\Model\Order\Payment;

/**
 * Class PaymentPlugin
 * @package Kurufootwear\Sales\Plugin
 */
class PaymentPlugin
{
    /**
     * @var RefundErrorLogger
     */
    private $refundErrorLogger;

    /**
     * PaymentPlugin constructor.
     *
     * @param RefundErrorLogger $refundErrorLogger
     */
    public function __construct(
        RefundErrorLogger $refundErrorLogger
    ) {
        $this->refundErrorLogger = $refundErrorLogger;
    }

    /**
     * Around refund
     *
     * @param Payment $subject
     * @param Closure $proceed
     * @param $creditmemo
     *
     * @throws CommandException
     */
    public function aroundRefund(
        Payment $subject,
        Closure $proceed,
        $creditmemo
    ) {
        try {
            $proceed($creditmemo);
        } catch (CommandException $e) {
            $this->refundErrorLogger->error(
                sprintf('%s Order ID -> %s', $e->getMessage(), $creditmemo->getOrderId())
            );

            throw $e;
        }
    }
}
