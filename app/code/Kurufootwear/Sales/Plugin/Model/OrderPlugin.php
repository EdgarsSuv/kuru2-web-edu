<?php
/**
 * @vendor      Kurufootwear
 * @module      Kurufootwear_Catalog
 * @author      Krisjanis Veinbahs <krisjanisv@scandiweb.com>
 * @copyright   Copyright (c) 2018 Scandiweb, Inc (http://scandiweb.com)
 * @license     http://opensource.org/licenses/OSL-3.0 The Open Software License 3.0 (OSL-3.0)
 */
namespace Kurufootwear\Sales\Plugin\Model;

use Magento\Sales\Model\Order;
use Magento\Framework\App\State as AppState;

/**
 * Plugin for renaming "Complete" orders to
 * "Shipped" orders in frontend only
 *
 * Class ProductSavePlugin
 * @package Kurufootwear\Catalog\Plugin
 */
class OrderPlugin
{
    /**
     * @var AppState
     */
    private $appState;

    /**
     * OrderPlugin constructor.
     * @param AppState $appState
     */
    public function __construct(
        AppState $appState
    )
    {
        $this->appState = $appState;
    }

    /**
     * Replace "Complete" label with "Shipped" label in frontend only
     *
     * @param Order $subject
     * @param $currentStatusLabel
     * @return string
     */
    public function afterGetStatusLabel(Order $subject, $currentStatusLabel)
    {
        $currentStatus = $subject->getStatus();
        $completeStatus = 'complete';
        $shippedStatusLabel = 'Shipped';

        $currentAppState = $this->appState->getAreaCode();
        $adminAppState = \Magento\Framework\App\Area::AREA_ADMINHTML;
        $isNotBackend = $currentAppState != $adminAppState;

        if ($isNotBackend && $currentStatus === $completeStatus) {
            return $shippedStatusLabel;
        } else {
            return $currentStatusLabel;
        }
    }
}
