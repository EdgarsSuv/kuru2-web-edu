<?php
/**
 * @vendor Kurufootwear
 * @module Kurufootwear_Sales
 * @author Aleksejs Baranovs <info@scandiweb.com>
 * @copyright Copyright (c) 2018 KURU Footwear. All rights reserved.
 */

namespace Kurufootwear\Sales\CustomerData;

use Magento\Sales\CustomerData\LastOrderedItems as BaseLastOrderedItems;

/**
 * Class LastOrderedItems
 * @package Kurufootwear\Sales\CustomerData
 */
class LastOrderedItems extends BaseLastOrderedItems
{
    /**
     * Get section data
     * Override to put in customer data last order id
     */
    public function getSectionData()
    {
        return [
            'order' => is_null($this->getLastOrder()) ? null : $this->getLastOrder()->getId(),
            'items' => $this->getItems()
        ];
    }
}
