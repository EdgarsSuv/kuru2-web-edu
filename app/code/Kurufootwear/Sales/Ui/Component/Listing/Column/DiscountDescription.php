<?php
/**
 * @category    Kurufootwear
 * @package     Kurufootwear\Sales
 * @author      Jim McGowen <jim@kurufootwear.com>
 * @copyright   Copyright (c) 2018 KURU Footwear
 */

namespace Kurufootwear\Sales\Ui\Component\Listing\Column;

use \Magento\Sales\Api\OrderRepositoryInterface;
use \Magento\Framework\View\Element\UiComponent\ContextInterface;
use \Magento\Framework\View\Element\UiComponentFactory;
use \Magento\Ui\Component\Listing\Columns\Column;
use \Magento\Framework\Api\SearchCriteriaBuilder;
use Magento\Framework\Pricing\Helper\Data as PricingHelper;

/**
 * Class DiscountDescription
 * @package Kurufootwear\Sales\Ui\Component\Listing\Column
 */
class DiscountDescription extends Column
{
    /** @var OrderRepositoryInterface  */
    protected $orderRepository;
    
    /** @var SearchCriteriaBuilder  */
    protected $searchCriteria;
    
    /** @var PricingHelper  */
    protected $pricingHelper;
    
    /**
     * DiscountDescription constructor.
     *
     * @param ContextInterface $context
     * @param UiComponentFactory $uiComponentFactory
     * @param OrderRepositoryInterface $orderRepository
     * @param SearchCriteriaBuilder $criteria
     * @param PricingHelper $pricingHelper
     * @param array $components
     * @param array $data
     */
    public function __construct(
        ContextInterface $context, 
        UiComponentFactory $uiComponentFactory, 
        OrderRepositoryInterface $orderRepository,
        SearchCriteriaBuilder $criteria,
        PricingHelper $pricingHelper,
        array $components = [], 
        array $data = []
    ) {
        $this->orderRepository = $orderRepository;
        $this->searchCriteria  = $criteria;
        $this->pricingHelper = $pricingHelper;
        parent::__construct($context, $uiComponentFactory, $components, $data);
    }
    
    /**
     * @param array $dataSource
     *
     * @return array
     */
    public function prepareDataSource(array $dataSource)
    {
        if (isset($dataSource['data']['items'])) {
            foreach ($dataSource['data']['items'] as & $item) {
        
                // The discount description has to be taken from the order instead of the credit memo
                // because it is not copied to the credit memo (even though there is a field for it).
                $order = $this->orderRepository->get($item["order_id"]);
    
                $item['discount_description'] = $order->getDiscountDescription();
            }
        }
        
        return $dataSource;
    }
}
