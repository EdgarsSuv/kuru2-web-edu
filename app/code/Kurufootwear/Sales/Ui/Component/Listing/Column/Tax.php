<?php
/**
 * @category    Kurufootwear
 * @package     Kurufootwear\Sales
 * @author      Jim McGowen <jim@kurufootwear.com>
 * @copyright   Copyright (c) 2018 KURU Footwear
 */

namespace Kurufootwear\Sales\Ui\Component\Listing\Column;

use \Magento\Sales\Api\CreditmemoRepositoryInterface;
use \Magento\Framework\View\Element\UiComponent\ContextInterface;
use \Magento\Framework\View\Element\UiComponentFactory;
use \Magento\Sales\Ui\Component\Listing\Column\Price;
use \Magento\Framework\Api\SearchCriteriaBuilder;
use Magento\Framework\Pricing\PriceCurrencyInterface;

/**
 * Class Tax
 * @package Kurufootwear\Sales\Ui\Component\Listing\Column
 */
class Tax extends Price
{
    /** @var CreditmemoRepositoryInterface  */
    protected $creditmemoRepository;
    
    /**
     * Tax constructor.
     *
     * @param ContextInterface $context
     * @param UiComponentFactory $uiComponentFactory
     * @param PriceCurrencyInterface $priceFormatter
     * @param CreditmemoRepositoryInterface $creditmemoRepository
     * @param array $components
     * @param array $data
     */
    public function __construct(
        ContextInterface $context,
        UiComponentFactory $uiComponentFactory,
        PriceCurrencyInterface $priceFormatter,
        CreditmemoRepositoryInterface $creditmemoRepository,
        array $components = [],
        array $data = []
    ) {
        $this->creditmemoRepository = $creditmemoRepository;
        parent::__construct($context, $uiComponentFactory, $priceFormatter, $components, $data);
    }
    
    /**
     * @param array $dataSource
     *
     * @return array
     */
    public function prepareDataSource(array $dataSource)
    {
        if (isset($dataSource['data']['items'])) {
            foreach ($dataSource['data']['items'] as &$item) {
        
                $creditMemo = $this->creditmemoRepository->get($item['entity_id']);
                
                $item['tax_amount'] = $creditMemo->getTaxAmount();
            }
        }
        
        return parent::prepareDataSource($dataSource);
    }
}
