<?php
/**
 * @category    Kurufootwear
 * @package     Kurufootwear\Sales
 * @author      Jim McGowen <jim@kurufootwear.com>
 * @copyright   Copyright (c) 2018 KURU Footwear
 */

namespace Kurufootwear\Sales\Ui\Component\Listing\Column;

use \Magento\Sales\Api\CreditmemoRepositoryInterface;
use \Magento\Framework\View\Element\UiComponent\ContextInterface;
use \Magento\Framework\View\Element\UiComponentFactory;
use \Magento\Ui\Component\Listing\Columns\Column;
use \Magento\Framework\Api\SearchCriteriaBuilder;
use Magento\Framework\Pricing\Helper\Data as PricingHelper;

/**
 * Class Discount
 * @package Kurufootwear\Sales\Ui\Component\Listing\Column
 */
class Discount extends Column
{
    /** @var CreditmemoRepositoryInterface  */
    protected $creditmemoRepository;
    
    /** @var SearchCriteriaBuilder  */
    protected $searchCriteria;
    
    /** @var PricingHelper  */
    protected $pricingHelper;
    
    /**
     * Discount constructor.
     *
     * @param ContextInterface $context
     * @param UiComponentFactory $uiComponentFactory
     * @param CreditmemoRepositoryInterface $creditmemoRepository
     * @param SearchCriteriaBuilder $criteria
     * @param PricingHelper $pricingHelper
     * @param array $components
     * @param array $data
     */
    public function __construct(
        ContextInterface $context, 
        UiComponentFactory $uiComponentFactory,
        CreditmemoRepositoryInterface $creditmemoRepository,
        SearchCriteriaBuilder $criteria,
        PricingHelper $pricingHelper,
        array $components = [], 
        array $data = []
    ) {
        $this->creditmemoRepository = $creditmemoRepository;
        $this->searchCriteria  = $criteria;
        $this->pricingHelper = $pricingHelper;
        parent::__construct($context, $uiComponentFactory, $components, $data);
    }
    
    /**
     * @param array $dataSource
     *
     * @return array
     */
    public function prepareDataSource(array $dataSource)
    {
        if (isset($dataSource['data']['items'])) {
            foreach ($dataSource['data']['items'] as & $item) {
    
                $creditMemo = $this->creditmemoRepository->get($item['entity_id']);
                
                $item['discount_amount'] = $this->pricingHelper->currency(abs($creditMemo->getDiscountAmount()), true, false);
            }
        }
        
        return $dataSource;
    }
}
