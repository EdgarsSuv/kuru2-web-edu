<?php
/**
 * @category    Kurufootwear
 * @package     Kurufootwear\Sales
 * @author      Jim McGowen <jim@kurufootwear.com>
 * @copyright   Copyright (c) 2018 KURU Footwear
 */

namespace Kurufootwear\Sales\Model\ResourceModel\Order\Creditmemo\Grid;

use Magento\Framework\DB\Select;
use Magento\Sales\Model\ResourceModel\Order\Creditmemo\Grid\Collection as GridCollection;

/**
 * Class Collection
 * 
 * Overridden to add the columns tax_amount, discount_amount, and discount_description 
 * to the credit memos grid.
 * 
 * @package Kurufootwear\Sales\Model\ResourceModel\Order\Creditmemo\Grid
 */
class Collection extends GridCollection
{
    /**
     * Overridden to join tables sales_creditmemo and sales_order to the sales_creditmemo_grid table.
     */
    protected function _renderFiltersBefore()
    {
        $joinTable = $this->getTable('sales_creditmemo');
        $this->getSelect()->JoinInner($joinTable . ' AS cm','main_table.entity_id = cm.entity_id', array('tax_amount', 'discount_amount'));
        
        $joinTable = $this->getTable('sales_order');
        $this->getSelect()->joinInner($joinTable . ' AS so','main_table.order_id = so.entity_id', array('discount_description'));
        
        $test = $this->getSelect()->__toString();
        parent::_renderFiltersBefore();
    }
    
    /**
     * Removes ambiguity for new column names.
     * 
     * @param array|string $field
     * @param null $condition
     *
     * @return $this | GridCollection
     */
    public function addFieldToFilter($field, $condition = null)
    {
        if ($field == 'discount_amount') {
            // M1 orders have negative values for discount amount, M2 orders have positive values
            // So we need to change the where clause to use ABS function.
            $resultCondition = $this->_translateCondition($field, $condition);
            $resultCondition = str_replace('`discount_amount`', 'ABS(`cm`.`discount_amount`)', $resultCondition);
            $this->getSelect()->where($resultCondition, null, Select::TYPE_CONDITION);
            
            return $this;
        }
        else if ($field == 'discount_description') {
            $field = 'so.discount_description';
        }
        else if ($field == 'tax_amount') {
            $field = 'cm.tax_amount';
        }
        else {
            $field = "main_table.$field";
        }
        
        return parent::addFieldToFilter($field, $condition);
    }
    
    /**
     * Removes ambiguity for new column names.
     * 
     * @param string $field
     * @param string $direction
     *
     * @return $this | GridCollection
     */
    public function setOrder($field, $direction = self::SORT_ORDER_DESC)
    {
        if ($field == 'discount_amount') {
            $field = 'cm.discount_amount';
        }
        else if ($field == 'discount_description') {
            $field = 'so.discount_description';
        }
        else if ($field == 'tax_amount') {
            $field = 'cm.tax_amount';
        }
        else {
            $field = "main_table.$field";
        }
        
        return parent::setOrder($field, $direction);
    }
}
