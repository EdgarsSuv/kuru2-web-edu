<?php
/**
 * @vendor Kurufootwear
 * @module Kurufootwear_Sales
 * @author Krisjanis Veinbahs <krisjanisv@scandiweb.com>
 * @copyright Copyright (c) 2017 Scandiweb. All rights reserved.
 * @license http://opensource.org/licenses/afl-3.0.php Academic Free License (AFL 3.0)
 */

namespace Kurufootwear\Sales\Model\Order;

use Magento\Sales\Model\Order\Item as MagentoItem;
use Magento\Framework\UrlInterface;

class Item extends MagentoItem
{
    /**
     * Try to get product item image url
     *
     * @param $sku
     * @return string
     */
    public function getImageUrl($sku)
    {
        if ($sku) {
            try {
                $product = $this->productRepository->get($sku);
            } catch (\Exception $e) {
                $product = $this->getProduct();
            }
        } else {
            $product = $this->getProduct();
        }

        /** @var \Magento\Store\Model\Store $store */
        $store = $this->_storeManager->getStore();
        $mediaUrl = $store->getBaseUrl(UrlInterface::URL_TYPE_MEDIA);
        $imagePath = 'catalog/product' . $product->getImage();

        return $mediaUrl . $imagePath;
    }

    /**
     * @param $key
     * @return mixed
     */
    public function getProductData($key)
    {
        if ($this->getParentItem()) {
            $product = $this->getParentItem()->getProduct();
        } else {
            $product = $this->getProduct();
        }

        return $product->getData($key);
    }
}
