<?php
/**
 * @vendor      Kurufootwear
 * @module      Kurufootwear_Sales
 * @author      Aleksejs Baranovs <info@scandiweb.com>
 * @copyright   Copyright (c) 2018 Scandiweb, Ltd (http://scandiweb.com)
 * @license     http://opensource.org/licenses/afl-3.0.php Academic Free License (AFL 3.0)
 */

namespace Kurufootwear\Sales\Block\Order;

use Kurufootwear\Sales\Block\Order\History as OrderItemHistory;
use Magento\Framework\Registry;
use Magento\Framework\View\Element\Template\Context;
use Magento\Sales\Block\Order\Items as BaseItems;

/**
 * Class Items
 * @package Kurufootwear\Sales\Block\Order
 */
class Items extends BaseItems
{
    /**
     * @var OrderItemHistory
     */
    protected $orderItemHistory;

    /**
     * Items constructor.
     * @param OrderItemHistory $orderItemHistory
     * @param Context $context
     * @param Registry $registry
     * @param array $data
     * @param null $itemCollectionFactory
     */
    public function __construct(
        OrderItemHistory $orderItemHistory,
        Context $context,
        Registry $registry,
        array $data = [],
        $itemCollectionFactory = null
    ) {
        $this->orderItemHistory = $orderItemHistory;

        parent::__construct(
            $context,
            $registry,
            $data,
            $itemCollectionFactory
        );
    }

    /**
     * Get product size information
     *
     * @param $item
     *
     * @return bool|array
     */
    public function getSizeInformation($item)
    {
        if ($attributesInfo = $item->getProductOptionByCode('attributes_info')) {
            foreach ($attributesInfo as $key => $value) {
                if (stripos($value['label'], 'size') !== false) {
                    return $attributesInfo[$key];
                }
            }
        }

        return false;
    }

    /**
     * Return not allowed if already in returned exchange
     * @return bool
     */
    public function isReturnAllowed()
    {
        $order = $this->getOrder();

        return !$this->orderItemHistory->returnFinished($order);
    }

    /**
     * Get order return url
     * @return string
     */
    public function getReturnUrl()
    {
        return $this->getUrl(
            'aw_rma/customer/new',
            [
                '_query' => [
                    'order_id' => $this->getOrder()->getId()
                ]
            ]
        );
    }
}
