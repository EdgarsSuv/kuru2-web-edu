<?php
/**
 * @vendor Kurufootwear
 * @module Kurufootwear_Rma
 * @author Krisjanis Veinbahs <krisjanisv@scandiweb.com>
 * @copyright Copyright (c) 2017 Scandiweb. All rights reserved.
 * @license http://opensource.org/licenses/afl-3.0.php Academic Free License (AFL 3.0)
 */

namespace Kurufootwear\Sales\Block\Order;

use Kurufootwear\Sales\Model\Order\Item;
use Magento\Customer\Model\Session;
use Magento\Framework\View\Element\Template\Context;
use Magento\Sales\Block\Order\History as MagentoHistory;
use Magento\Sales\Model\Order;
use Magento\Sales\Model\Order\Config;
use Magento\Sales\Model\ResourceModel\Order\CollectionFactory;

class History extends MagentoHistory
{
    /**
     * History constructor.
     * @param Context $context
     * @param CollectionFactory $orderCollectionFactory
     * @param Session $customerSession
     * @param Config $orderConfig
     * @param array $data
     */
    public function __construct(
        Context $context,
        CollectionFactory $orderCollectionFactory,
        Session $customerSession,
        Config $orderConfig,
        array $data = []
    )
    {
        parent::__construct(
            $context,
            $orderCollectionFactory,
            $customerSession,
            $orderConfig,
            $data
        );
    }

    /**
     * Get RMA Url
     *
     * @param $orderId
     *
     * @return string
     */
    public function getRmaUrl($orderId)
    {
        return $this->getUrl('aw_rma/customer/new', ['_query' => ['order_id' => $orderId]]);
    }

    /**
     * @param Order $order
     * @return bool
     */
    public function returnFinished($order)
    {
        /**
         * Check only non-configurable items
         * as only they can be returned
         */
        foreach ($order->getItemsCollection() as $item) {
            if ($item->getProductType() == 'simple') {
                $itemRenderer = $this->getItemRenderer($item);
                $maxItems = $itemRenderer->getItemMaxCount($item);
                $allowedForOrder = $itemRenderer->isAllowedForOrder($order);
                $itemFinished = !($maxItems && $allowedForOrder);

                if (!$itemFinished) {
                    return false;
                }
            }
        }

        return true;
    }

    /**
     * Returns first order sku
     * @param $order
     * @return mixed
     */
    public function getFirstOrderSku($order)
    {
        foreach ($order->getItemsCollection() as $item) {
            if ($item->getProductType() == 'simple') {
                $sku = $item->getSku();
                $skuWithoutSize = explode('-', $sku);

                if (is_array($skuWithoutSize)) {
                    return $skuWithoutSize[0];
                } else {
                    return $sku;
                }
            }
        }
    }

    /**
     * @param Item $orderItem
     * @return mixed
     */
    protected function getItemRenderer($orderItem)
    {
        return $this->getLayout()
            ->createBlock('Aheadworks\Rma\Block\Customer\Request\NewRequest\Step\SelectOrder\Items\Renderer')
            ->setItem($orderItem);
    }

    /**
     * This is needed to take the correct templates
     *
     * @return string
     */
    protected function _toHtml()
    {
        $this->setModuleName($this->extractModuleName('Magento\Sales\Block\Order\History'));

        return parent::_toHtml();
    }
}