<?php

namespace Kurufootwear\Sales\Block\Search;

use Magento\Backend\Block\Template\Context;
use Magento\Sales\Model\ResourceModel\Order\Status\History\CollectionFactory as OrderStatusHistoryCollectionFactory;

class Search extends \Magento\Backend\Block\Template
{
    private $context;
    
    private  $orderStatusHistoryCollectionFactory;
    
    private $results = [];
    
    public function __construct(
        OrderStatusHistoryCollectionFactory $orderStatusHistoryCollectionFactory,
        Context $context, 
        array $data = [])
    {
        $this->context = $context;
        
        $this->orderStatusHistoryCollectionFactory = $orderStatusHistoryCollectionFactory;
        
        $term = $this->getSearchTerm();
        if (!empty(trim($term))) {
            /** @var \Magento\Sales\Model\ResourceModel\Order\Status\History\Collection $collection */
            $collection = $this->orderStatusHistoryCollectionFactory->create()
                ->addFieldToFilter('comment', array('like' => "%$term%"))
                ->setPageSize(10)
                ->setCurPage(1);
            
            $collection->getSelect()->joinLeft('sales_order', 'sales_order.entity_id=main_table.parent_id', array('entity_id AS order_id', 'increment_id'))
                ->joinLeft('aw_rma_request', 'aw_rma_request.order_id=main_table.parent_id', array('id AS rma_id'));
            
            $sql = $collection->getSelectSql()->__toString();
            foreach ($collection as $item) {
                $this->results[] = $item->getData();
            }
        }
        
        parent::__construct($context, $data);
    }
    
    public function getSearchTerm()
    {
        $term = $this->context->getRequest()->getParam('search_term');
        return htmlentities($term);
    }
    
    public function getResults()
    {
        return $this->results;
    }
    
    public function getOrderUrl($orderId)
    {
        return $this->getUrl('sales/order/view', ['order_id' => $orderId]);
    }
    
    public function getRmaUrl($rmaId)
    {
        return $this->getUrl('aw_rma_admin/rma/edit', ['id' => $rmaId]);
    }
}
