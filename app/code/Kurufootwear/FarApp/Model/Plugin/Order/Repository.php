<?php
/**
 * @vendor Kurufootwear
 * @module Kurufootwear_FarApp
 * @author Jim McGowen <jim@kurufootwear.com>
 * @copyright Copyright (c) 2017 KURU Footwear. All rights reserved.
 */
namespace Kurufootwear\FarApp\Model\Plugin\Order;

use Magento\Sales\Api\OrderRepositoryInterface;
use Magento\Sales\Api\Data\OrderInterface;
use WeltPixel\Maxmind\Model\MaxmindFactory;
use Magento\Sales\Api\Data\OrderExtensionFactory;
use \Magento\Sales\Api\Data\OrderSearchResultInterface;

/**
 * Class Repository
 * @package Kurufootwear\FarApp\Model\Plugin\Order
 */
class Repository
{
    /** @var MaxmindFactory  */
    protected $maxmindFactory;
    
    /** @var  OrderExtensionFactory */
    protected $orderExtensionFactory;
    
    /**
     * Repository constructor.
     *
     * @param MaxmindFactory $maxmindFactory
     * @param OrderExtensionFactory $orderExtensionFactory
     */
    public function __construct(
        MaxmindFactory $maxmindFactory,
        OrderExtensionFactory $orderExtensionFactory
    )
    {
        $this->maxmindFactory = $maxmindFactory;
        $this->orderExtensionFactory = $orderExtensionFactory;
    }
    
    /**
     * @param OrderRepositoryInterface $subject
     * @param OrderInterface $order
     *
     * @return OrderInterface
     */
    public function afterGet(OrderRepositoryInterface $subject, OrderInterface $order)
    {
        $this->addFraudScoreToOrder($order);
        
        return $order;
    }
    
    /**
     * @param OrderRepositoryInterface $subject
     * @param OrderSearchResultInterface $searchResult
     *
     * @return OrderSearchResultInterface
     */
    public function afterGetList(OrderRepositoryInterface $subject, OrderSearchResultInterface $searchResult)
    {
        foreach ($searchResult as $order) {
            $this->addFraudScoreToOrder($order);
        }
        
        return $searchResult;
    }
    
    /**
     * Adds fraud_score to the order extension attributes.
     * 
     * @param $order
     */
    private function addFraudScoreToOrder($order)
    {
        $extensionAttributes = $order->getExtensionAttributes();
        if (null === $extensionAttributes) {
            $extensionAttributes = $this->orderExtensionFactory->create();
        }
    
        /** @var \WeltPixel\Maxmind\Model\Maxmind $maxmind */
        $maxmind = $this->maxmindFactory->create()->loadMaxmindByOrderId($order->getId());
        if ($maxmind->hasData()) {
            $fraudScore = $maxmind->getFraudScore();
            $extensionAttributes->setFraudScore($fraudScore);
        }
        else {
            $extensionAttributes->setFraudScore('');
        }
    
        $order->setExtensionAttributes($extensionAttributes);
    }
}
