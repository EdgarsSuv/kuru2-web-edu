<?php
/**
 * @vendor Kurufootwear
 * @module Kurufootwear_FarApp
 * @author Jim McGowen <jim@kurufootwear.com>
 * @copyright Copyright (c) 2017 KURU Footwear. All rights reserved.
 */
namespace Kurufootwear\FarApp\Model\Plugin\CreditMemo;

use Magento\Sales\Api\CreditmemoRepositoryInterface;
use Magento\Sales\Api\Data\CreditmemoInterface;
use Magento\Sales\Api\Data\CreditmemoItemExtensionFactory;
use Magento\Sales\Api\Data\CreditmemoSearchResultInterface;
use Aheadworks\Rma\Model\ResourceModel\RequestItem\CollectionFactory as RmaRequestItemCollectionFactory;
use Kurufootwear\NetSuite\Helper\Inventory;
use Kurufootwear\Rma\Model\InspectionFactory;

/**
 * Class Repository
 * @package Kurufootwear\FarApp\Model\Plugin\CreditMemo
 */
class Repository
{
    /** @var CreditmemoItemExtensionFactory  */
    protected $creditmemoItemExtensionFactory;
    
    /** @var RmaRequestItemCollectionFactory  */
    protected $rmaRequestItemCollectionFactory;
    
    /** @var InspectionFactory  */
    protected $inspectionFactory;
    
    /**
     * Repository constructor.
     *
     * @param CreditmemoItemExtensionFactory $CreditmemoItemExtensionFactory
     * @param RmaRequestItemCollectionFactory $rmaRequestItemCollectionFactory
     * @param InspectionFactory $inspectionFactory
     */
    public function __construct(
        CreditmemoItemExtensionFactory $CreditmemoItemExtensionFactory,
        RmaRequestItemCollectionFactory $rmaRequestItemCollectionFactory,
        InspectionFactory $inspectionFactory
    )
    {
        $this->creditmemoItemExtensionFactory = $CreditmemoItemExtensionFactory;
        $this->rmaRequestItemCollectionFactory = $rmaRequestItemCollectionFactory;
        $this->inspectionFactory = $inspectionFactory;
    }
    
    /**
     * @param CreditmemoRepositoryInterface $subject
     * @param CreditmemoInterface $creditmemo
     *
     * @return CreditmemoInterface
     */
    public function afterGet(CreditmemoRepositoryInterface $subject, CreditmemoInterface $creditmemo)
    {
        $this->addBinAndLocationToItems($creditmemo);
        
        return $creditmemo;
    }
    
    /**
     * @param CreditmemoRepositoryInterface $subject
     * @param CreditmemoSearchResultInterface $searchResult
     *
     * @return CreditmemoSearchResultInterface
     */
    public function afterGetList(CreditmemoRepositoryInterface $subject, CreditmemoSearchResultInterface $searchResult)
    {
        foreach ($searchResult as $creditmemo) {
            $this->addBinAndLocationToItems($creditmemo);
        }
        
        return $searchResult;
    }
    
    /**
     * Adds "bin" and "inventory" extension attributes to the API response.
     * 
     * @param CreditmemoInterface $creditmemo
     */
    private function addBinAndLocationToItems(CreditmemoInterface $creditmemo)
    {
        // Note: this returns both simple and configurables and there doesn't seem to 
        // be any way to tell them apart with CreditmemoInterface :/
        /** @var \Magento\Sales\Api\Data\CreditmemoItemInterface[] $creditMemoItems */
        $creditMemoItems = $creditmemo->getItems();
        
        // Make note of duplicate items (See KURU2-1202)
        $itemSkuCount = [];
        foreach ($creditMemoItems as $creditMemoItem) {
            if (!isset($itemSkuCount[$creditMemoItem->getSku()])) {
                $itemSkuCount[$creditMemoItem->getSku()] = 1;
            }
            else {
                $itemSkuCount[$creditMemoItem->getSku()]++;
            }
        }
        
        foreach ($creditMemoItems as $creditMemoItem) {
            
            // Check if this is a long format item (See KURU2-1202)
            if (!empty($creditMemoItem->getRowTotal())) {
                // Look for a an accompanying short format item
                if ($itemSkuCount[$creditMemoItem->getSku()] > 1) {
                    // Wait for the short format to add extension_attributes
                    continue;
                }
            }
            
            $extensionAttributes = $creditMemoItem->getExtensionAttributes();
            if (null === $extensionAttributes) {
                $extensionAttributes = $this->creditmemoItemExtensionFactory->create();
            }
    
            $extensionAttributes->setHasRma(false);
            $extensionAttributes->setInspected(false);
            $extensionAttributes->setQty((int)$creditMemoItem->getQty());
            $extensionAttributes->setBin('UT');
            $extensionAttributes->setLocation(Inventory::INV_LOCATION_COMBINED_WAREHOUSE);
    
            /** @var \Aheadworks\Rma\Model\ResourceModel\RequestItem\Collection $rmaItems */
            $rmaItem = $this->rmaRequestItemCollectionFactory->create()
                ->addFieldToFilter('item_id', $creditMemoItem->getOrderItemId())
                ->getFirstItem();
            
            if ($rmaItem->hasData()) {
                // Defaults
                $extensionAttributes->setHasRma(true);
                $inspection = $this->inspectionFactory->create()->loadByRequestId($rmaItem->getRequestId());
                if ($inspection->hasData() && !empty($inspection->getInspectedDate())) {
                    $extensionAttributes->setInspected(true);
                    $receivedItems = $inspection->getReceivedItems();
                    if (!empty($receivedItems)) {
                        foreach ($receivedItems as $itemID => $receivedItem) {
                            if ($itemID == $creditMemoItem->getOrderItemId()) {
                                $extensionAttributes->setQty($receivedItem['qty']);
                                
                                if ($receivedItem['grade'] == 'A') {
                                    $extensionAttributes->setBin('UT');
                                    $extensionAttributes->setLocation(Inventory::INV_LOCATION_COMBINED_WAREHOUSE);
                                }
                                else {
                                    $extensionAttributes->setBin('UT-B');
                                    $extensionAttributes->setLocation(Inventory::INV_LOCATION_COMBINED_QUARANTINE);
                                }
                                break;
                            }
                        }
                    }
                }
            }
    
            $creditMemoItem->setExtensionAttributes($extensionAttributes);
        }
    }
}
