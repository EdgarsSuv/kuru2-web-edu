<?php
/**
 * @category    Kurufootwear
 * @package     Kurufootwear\Zopim
 * @author      Aleksejs Baranovs <info@scandiweb.com>
 * @copyright   Copyright (c) 2018 KURU Footwear. All rights reserved.
 */

namespace Kurufootwear\Zopim\Helper;

use Magento\Framework\App\Helper\AbstractHelper;

/**
 * Class Data
 * @package Kurufootwear\Zopim\Helper
 */
class Data extends AbstractHelper
{
    /**
     * Help bar offline text path
     */
    const HELP_BAR_OFFLINE_TEXT = 'kurufootwear_zopim/zopim_configuration/help_bar_offline_text';

    /**
     * Help bar offline text path
     */
    const HELP_BAR_ONLINE_TEXT = 'kurufootwear_zopim/zopim_configuration/help_bar_online_text';

    /**
     * Get help bar offline text
     *
     * @return string
     */
    public function getHelpBarOfflineText()
    {
        return $this->scopeConfig->getValue(self::HELP_BAR_OFFLINE_TEXT);
    }

    /**
     * Get help bar online text
     *
     * @return string
     */
    public function getHelpBarOnlineText()
    {
        return $this->scopeConfig->getValue(self::HELP_BAR_ONLINE_TEXT);
    }
}