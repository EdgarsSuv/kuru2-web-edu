/**
 * @category    Kurufootwear
 * @package     Kurufootwear\Zopim
 * @author      Aleksejs Baranovs <info@scandiweb.com>
 * @copyright   Copyright (c) 2018 KURU Footwear. All rights reserved.
 */

define([
    'jquery',
    'Magento_Ui/js/lib/view/utils/dom-observer'
], function($, domObserver) {
    'use strict';

    $.widget('kuru.chat', {
        desktopBreakpoint: 1024,

        options: {
            helpBarOfflineText: null,
            helpBarOnlineText: null
        },

        classes: {
            footerSelector: '.footer-bottom',
            zopimHelpBarTextSelector: '.zopim-help-bar-text',
            chatTogglerSelector: '.kuru-chat-toggler-help',
            zendeskWidgetSelector: '.zEWidget-webWidget',
            zendeskWidgetInternalCloseSelector: '.src-component-button-ButtonNav-button',
            zendeskButtonSelector: '.zEWidget-launcher',
            zendeskButtonInternalOpenSelector: '#Embed',
            zendeskButtonEnabledClass: 'zEWidget-launcher--active',

            listenerClass: '.zEWidget-launcher'
        },

        // Flag to check if actions were bind
        actionsBind: false,

        /**
         * Initialize
         *
         * @private
         */
        _create: function () {
            this.observeZendeskInitialization();
        },

        /**
         * Observe Zendesk widget creation in DOM
         */
        observeZendeskInitialization: function () {
            var self = this;

            domObserver.get(self.classes.listenerClass, function () {
                // Bind actions when zopim wrapper appears
                self.bindActions();
                self.actionsBind = true;
            });
        },

        /**
         * Bind actions
         */
        bindActions: function () {
            var self = this;

            if (!self.actionsBind) {
                domObserver.off(self.classes.listenerClass);
                self.bindChatTogglerClick();
                self.stickZopimContainers();
            }
        },

        /**
         * Is button currently shown
         */
        isButtonActive: function() {
            var self = this;
            var launcherButton = $(self.classes.zendeskButtonSelector);
            var buttonEnabledClass = self.classes.zendeskButtonEnabledClass;

            return launcherButton.hasClass(buttonEnabledClass);
        },

        /**
         * Bind chat toggler click
         */
        bindChatTogglerClick: function () {
            var self = this;

            $(self.classes.chatTogglerSelector).click(function (e) {
                e.preventDefault();

                if (self.isButtonActive()) {
                    self.activateWidget();
                } else {
                    self.activateButton();
                }
            }.bind(self));
        },

        /**
         * Show Zendesk Chat Button
         */
        activateButton: function() {
            var self = this;
            window.zE.show();

            setTimeout(function() {
                self.recheckFixedZendesk();
            }, 50);
        },

        /**
         * Show Zendesk Chat Widget
         */
        activateWidget: function() {
            var self = this;
            window.zE.activate();

            setTimeout(function() {
                self.recheckFixedZendesk();
            }, 50);
        },

        /**
         * Stick zopim containers
         */
        stickZopimContainers: function () {
            var self = this;

            if ($(self.classes.footerSelector).length <= 0) {
                return;
            }

            // Initial check if livechat is loaded overlapping the footer
            self.recheckFixedZendesk();
            $(document).scroll(function () {
                self.recheckFixedZendesk();
            });

            $(self.classes.zendeskButtonSelector).contents()
                .find(self.classes.zendeskButtonInternalOpenSelector).click(function() {
                setTimeout(function() {
                    self.recheckFixedZendesk();
                }, 50);
            });

            $(self.classes.zendeskWidgetSelector).contents()
                .find(self.classes.zendeskWidgetInternalCloseSelector).click(function() {
                setTimeout(function() {
                    self.recheckFixedZendesk();
                }, 50);
            });

            /* Workaround for checkout pages to observer body height change */
            if ($('body').hasClass('checkout-index-index')) {
                var mutationObserver = new MutationObserver(function(mutations) {
                    if ($(document).scrollTop() + window.innerHeight < $(self.classes.footerSelector).offset().top) {
                        self.recheckFixedZendesk();

                        try {
                            if ($zopim.livechat.window.getDisplay()) {
                                self.recheckFixedZendesk();
                            }
                        } catch (error) {
                            // Zopim hasn't loaded yet
                        }

                        mutationObserver.disconnect();
                    }
                });

                mutationObserver.observe(document.documentElement, {
                    childList: true,
                    subtree: true
                });
            }
        },

        /**
         * Recheck and fix - whether all Zendesk elements
         * are in proper sticky positions
         */
        recheckFixedZendesk: function() {
            var self = this;

            self.stopFixedOnFooter($(self.classes.zendeskButtonSelector));
            self.stopFixedOnFooter($(self.classes.zendeskWidgetSelector));
        },

        /**
         * Stop fixed zopim div on footer
         *
         * @param el
         */
        stopFixedOnFooter: function (el) {
            var self = this;

            if ($(window).width() >= self.desktopBreakpoint) {
                var zopimBottomOffset = el.offset().top + el.height() + parseInt(el.css("marginBottom")),
                    footerOffsetTop = $(self.classes.footerSelector).offset().top,
                    footerHeight = $(self.classes.footerSelector).height();

                if (zopimBottomOffset >= footerOffsetTop) {
                    el.css({
                        'position': 'absolute',
                        'bottom': footerHeight,
                        'transition-duration': '0ms'
                    });
                }

                if ($(document).scrollTop() + window.innerHeight < footerOffsetTop) {
                    el.css({
                        'position': 'fixed',
                        'bottom': '0',
                        'transition-duration': '0ms'
                    });
                }
            }
        }
    });

    return $.kuru.chat;
});
