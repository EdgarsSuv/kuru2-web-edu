<?php
/**
 * @category    Kurufootwear
 * @package     Kurufootwear\Zopim
 * @author      Aleksejs Baranovs <info@scandiweb.com>
 * @copyright   Copyright (c) 2018 KURU Footwear. All rights reserved.
 */

namespace Kurufootwear\Zopim\Block;

use Kurufootwear\Zopim\Helper\Data;
use Magento\Framework\View\Element\Template;
use Magento\Framework\View\Element\Template\Context;

/**
 * Class Chat
 * @package Kurufootwear\Zopim\Block
 */
class Chat extends Template
{
    /**
     * Zopim chat status
     */
    const ZOPIM_CHAT_STATUS = 'kurufootwear_zopim/zopim_configuration/zopim_status';

    /**
     * @var Data
     */
    private $helper;

    /**
     * Chat constructor.
     *
     * @param Context $context
     * @param Data $helper
     * @param array $data
     */
    public function __construct(
        Context $context,
        Data $helper,
        array $data = []
    ) {
        parent::__construct($context, $data);
        $this->helper = $helper;
    }

    /**
     * Get help bar offline text
     *
     * @return string
     */
    public function getHelpBarOfflineText()
    {
        return $this->helper->getHelpBarOfflineText();
    }

    /**
     * Get help bar online text
     *
     * @return string
     */
    public function getHelpBarOnlineText()
    {
        return $this->helper->getHelpBarOnlineText();
    }

    /**
     * Check if Zopim is enabled
     *
     * @return bool
     */
    public function isEnabled()
    {
        return $this->_scopeConfig->getValue(self::ZOPIM_CHAT_STATUS);
    }
}
