<?php
/**
 * @category    Kurufootwear
 * @package     Kurufootwear\Zopim
 * @author      Aleksejs Baranovs <info@scandiweb.com>
 * @copyright   Copyright (c) 2018 KURU Footwear. All rights reserved.
 */

use Magento\Framework\Component\ComponentRegistrar;

ComponentRegistrar::register(
    ComponentRegistrar::MODULE,
    'Kurufootwear_Zopim',
    __DIR__
);