<?php
/**
 * @vendor Kurufootwear
 * @module Kurufootwear_Checkout
 * @author Krisjanis Veinbahs <krisjanisv@scandiweb.com>
 * @copyright Copyright (c) 2017 Scandiweb. All rights reserved.
 * @license http://opensource.org/licenses/afl-3.0.php Academic Free License (AFL 3.0)
 */

namespace Kurufootwear\Checkout\Plugin\Amasty\ShippingTableRates\Block\Onepage;

use Amasty\ShippingTableRates\Block\Onepage\LayoutProcessor;
use Magento\Framework\Module\Manager;

class LayoutProcessorPlugin
{
    /**
     * @var Manager
     */
    protected $moduleManager;

    /**
     * LayoutProcessorPlugin constructor.
     * @param Manager $moduleManager
     */
    public function __construct(
        Manager $moduleManager
    )
    {
        $this->moduleManager = $moduleManager;
    }

    /**
     * @param LayoutProcessor $subject
     * @param $result
     * @return mixed
     */
    public function afterProcess(LayoutProcessor $subject, $result)
    {
        if (!$this->moduleManager->isEnabled('Magestore_OneStepCheckout')) {
            $result['components']['checkout']['children']['steps']['children']['shipping-step']['children']['shippingAddress']['template'] = 'Magento_Checkout/amasty-table-rates-rewrite/shipping';
        }

        return $result;
    }
}
