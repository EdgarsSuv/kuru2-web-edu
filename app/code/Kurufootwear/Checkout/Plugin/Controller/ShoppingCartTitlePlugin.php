<?php
/**
 * @vendor      Kurufootwear
 * @module      Kurufootwear_Checkout
 * @author      Jelena Sorohova <info@scandiweb.com>
 * @copyright   Copyright (c) 2017 Scandiweb, Ltd (http://scandiweb.com)
 * @license     http://opensource.org/licenses/afl-3.0.php Academic Free License (AFL 3.0)
 */

namespace Kurufootwear\Checkout\Plugin\Controller;

use Magento\Framework\View\Result\PageFactory;

class ShoppingCartTitlePlugin
{
    /**
     * @var PageFactory
     */
    protected $_resultPageFactory;

    public function __construct(
        PageFactory $resultPageFactory
    )
    {
        $this->_resultPageFactory = $resultPageFactory;
    }

    /**
     * @return \Magento\Framework\View\Result\Page
     */
    public function afterExecute()
    {
        $resultPage = $this->_resultPageFactory->create();
        $resultPage->getConfig()->getTitle()->set(__('My Shopping Bag'));
        return $resultPage;
    }
}
