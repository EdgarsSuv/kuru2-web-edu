<?php

/**
 * @vendor Kurufootwear
 * @module Kurufootwear_Checkout
 * @author Krisjanis Veinbahs <krisjanisv@scandiweb.com>
 * @copyright Copyright (c) 2017 Scandiweb. All rights reserved.
 * @license http://opensource.org/licenses/afl-3.0.php Academic Free License (AFL 3.0)
 */

namespace Kurufootwear\Checkout\Plugin\Checkout\Block\Checkout\AttributeMerger;

use Magento\Checkout\Block\Checkout\AttributeMerger;

class Plugin {

    /**
     * Add street field placeholders to checkout attributes
     *
     * @param AttributeMerger $subject
     * @param $result
     * @return mixed
     */
    public function afterMerge(AttributeMerger $subject, $result)
    {
        if (array_key_exists('street', $result)) {
            $result['street']['children'][0]['placeholder'] = __('Enter your address');
            $result['street']['children'][1]['placeholder'] = __('Enter your address (optional)');
        }
        return $result;
    }
}