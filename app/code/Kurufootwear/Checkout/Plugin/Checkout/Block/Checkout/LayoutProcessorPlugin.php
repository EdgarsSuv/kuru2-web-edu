<?php
/**
 * @vendor      Kurufootwear
 * @module      Kurufootwear_Checkout
 * @author      Jelena Sorohova <info@scandiweb.com>
 * @copyright   Copyright (c) 2017 Scandiweb. All rights reserved.
 * @license     http://opensource.org/licenses/afl-3.0.php Academic Free License (AFL 3.0)
 */

namespace Kurufootwear\Checkout\Plugin\Checkout\Block\Checkout;

use Closure;
use Magento\Checkout\Block\Checkout\LayoutProcessor;

class LayoutProcessorPlugin
{
    /**
     * @param \Magento\Checkout\Block\Checkout\LayoutProcessor $subject
     * @param array $jsLayout
     * @return array
     */
    public function afterProcess(
        \Magento\Checkout\Block\Checkout\LayoutProcessor $subject,
        array  $jsLayout
    ) {
        $configuration = $jsLayout['components']['checkout']['children']['steps']['children']['billing-step']['children']['payment']['children']['payments-list']['children'];
        foreach ($configuration as $paymentGroup => $groupConfig) {
            if (isset($groupConfig['component']) AND $groupConfig['component'] === 'Magento_Checkout/js/view/billing-address') {

                $jsLayout['components']['checkout']['children']['steps']['children']['billing-step']['children']
                ['payment']['children']['payments-list']['children'][$paymentGroup]['children']['form-fields']['children']['fax'] = [
                    'visible' => false
                ];

                $jsLayout['components']['checkout']['children']['steps']['children']['billing-step']['children']['payment']['children']
                ['payments-list']['children'][$paymentGroup]['children']['form-fields']['children']['telephone']['validation']['validate-number'] = true;
                $jsLayout['components']['checkout']['children']['steps']['children']['billing-step']['children']['payment']['children']
                ['payments-list']['children'][$paymentGroup]['children']['form-fields']['children']['telephone']['component'] = 'Magento_Checkout/js/form/element/telephone';
            }
        }

        return $jsLayout;
    }

    /**
     * Set necessary components to use input field mask
     *
     * @param LayoutProcessor $subject
     * @param Closure $proceed
     * @param $jsLayout
     *
     * @return mixed
     */
    public function aroundProcess(LayoutProcessor $subject, Closure $proceed, $jsLayout)
    {
        $result = $proceed($jsLayout);

        $result['components']['checkout']['children']['steps']['children']['shipping-step']['children']['shippingAddress']['children']['shipping-address-fieldset']['children']
        ['telephone']['component'] = 'Magento_Checkout/js/form/element/telephone';
        $result['components']['checkout']['children']['steps']['children']['shipping-step']['children']['shippingAddress']['children']['shipping-address-fieldset']['children']
        ['telephone']['validation']['validate-number'] = true;

        return $result;
    }
}