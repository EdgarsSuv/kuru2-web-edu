<?php
/**
 * @vendor      Kurufootwear
 * @module      Kurufootwear_Checkout
 * @author      Jelena Sorohova <info@scandiweb.com>
 * @copyright   Copyright (c) 2017 Scandiweb, Ltd (http://scandiweb.com)
 * @license     http://opensource.org/licenses/afl-3.0.php Academic Free License (AFL 3.0)
 */

namespace Kurufootwear\Checkout\Plugin\Block;

use Magento\Checkout\Block\Cart\Sidebar;
use Magento\Cms\Block\Block;
use Magento\Framework\View\LayoutInterface;

class CartSidebarShippingPromoPlugin
{

    /**
     * @var LayoutInterface
     */
    protected $layout;

    /**
     * CartSidebarShippingPromoPlugin constructor.
     * @param LayoutInterface $layout
     */
    public function __construct(LayoutInterface $layout)
    {
        $this->layout = $layout;
    }

    /**
     * @param Sidebar $subject
     * @param array $config
     * @return array
     */
    public function afterGetConfig($subject, $config)
    {
        return array_merge($config, [
            'shipping_promo_block' => $this->layout->createBlock(Block::class)->setBlockId('minicart_shipping_promo_block')->toHtml(),
        ]);
    }

}