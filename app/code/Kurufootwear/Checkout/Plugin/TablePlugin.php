<?php
/**
 * @vendor Kurufootwear
 * @module Kurufootwear_Checkout
 * @author Krisjanis Veinbahs <krisjanisv@scandiweb.com>
 * @copyright Copyright (c) 2017 Scandiweb. All rights reserved.
 * @license http://opensource.org/licenses/afl-3.0.php Academic Free License (AFL 3.0)
 */
namespace Kurufootwear\Checkout\Plugin;

use Amasty\ShippingTableRates\Model\Carrier\Table;
use Magento\Quote\Model\Quote\Address\RateRequest;
use Magento\Shipping\Model\Rate\ResultFactory;

/**
 * Class TablePlugin
 *
 * A plugin to remove expedited rates for Alaska and Hawaii
 *
 * @package Kurufootwear\Checkout\Plugin
 */
class TablePlugin
{
    /**
     * @var ResultFactory
     */
    protected $rateResultFactory;

    /**
     * LayoutProcessorPlugin constructor.
     * @param ResultFactory $rateResultFactory
     */
    public function __construct(
        ResultFactory $rateResultFactory
    ) {
        $this->rateResultFactory = $rateResultFactory;
    }

    /**
     * If shipping to Alaska or Hawaii, don't return Amasty Expedited rates
     * aka return empty rate result from Amasty collectRates
     *
     * @param Table $subject
     * @param callable $proceed
     * @param RateRequest $request
     *
     * @return $result
     */
    public function aroundCollectRates(
        Table $subject,
        callable $proceed,
        RateRequest $request
    ) {
        $result = $proceed($request);
        $regionCode = $request->getDestRegionCode();

        if ($regionCode === "AK" || $regionCode === "HI") {
            $result = $this->rateResultFactory->create();
        }

        return $result;
    }
}
