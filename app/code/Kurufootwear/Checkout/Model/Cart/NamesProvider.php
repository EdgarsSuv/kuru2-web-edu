<?php
/**
 * @vendor      Kurufootwear
 * @module      Kurufootwear_Checkout
 * @author      Krisjanis Veinbahs <info@scandiweb.com>
 * @copyright   Copyright (c) 2018 Scandiweb, Ltd (http://scandiweb.com)
 * @license     http://opensource.org/licenses/afl-3.0.php Academic Free License (AFL 3.0)
 */
namespace Kurufootwear\Checkout\Model\Cart;

use Magento\Checkout\CustomerData\ItemPoolInterface;
use Magento\Quote\Api\CartItemRepositoryInterface;
use Magento\Quote\Model\Quote\Item;

class NamesProvider
{
    /**
     * @var CartItemRepositoryInterface
     */
    protected $itemRepository;

    /**
     * @var ItemPoolInterface
     */
    protected $itemPool;

    /**
     * NamesProvider constructor.
     *
     * @param CartItemRepositoryInterface $itemRepository
     * @param ItemPoolInterface $itemPool
     */
    public function __construct(
        CartItemRepositoryInterface $itemRepository,
        ItemPoolInterface $itemPool
    ) {
        $this->itemRepository = $itemRepository;
        $this->itemPool = $itemPool;
    }

    /**
     * Get item names from ItemPoolInterface
     *
     * @param $cartId
     * @return array
     */
    public function getNames($cartId)
    {
        $items = $this->itemRepository->getList($cartId);
        $itemData = [];

        /** @var Item $cartItem */
        foreach ($items as $cartItem) {
            $allData = $this->itemPool->getItemData($cartItem);
            $itemData[$cartItem->getItemId()] = $allData['product_name'];
        }

        return $itemData;
    }
}
