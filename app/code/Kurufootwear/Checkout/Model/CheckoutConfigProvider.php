<?php
/**
 * @category    Kurufootwear
 * @package     Kurufootwear\Checkout
 * @author      Kristaps Stalidzans <info@scandiweb.com>
 * @copyright   Copyright (c) 2017 Scandiweb, Inc (http://scandiweb.com)
 * @license     http://opensource.org/licenses/OSL-3.0 The Open Software License 3.0 (OSL-3.0)
 */
namespace Kurufootwear\Checkout\Model;

use Kurufootwear\Rma\Model\CustomerExchangeFactory;
use Kurufootwear\Rma\Model\ResourceModel\CustomerExchange\Collection;
use Kurufootwear\Checkout\Model\Cart\NamesProvider;
use Magento\Checkout\Model\Session as CheckoutSession;
use Magento\Customer\Model\Session;
use Magento\Framework\View\LayoutInterface;
use Magento\Checkout\Model\ConfigProviderInterface;
use Magento\Framework\App\Request\Http;

class CheckoutConfigProvider implements ConfigProviderInterface
{
    /**
     * @var LayoutInterface
     */
    protected $layout;

    /**
     * @var CustomerExchangeFactory
     */
    protected $customerExchangeFactory;

    /**
     * @var Session
     */
    protected $customerSession;

    /**
     * @var Http
     */
    protected $request;

    /**
     * @var NamesProvider
     */
    protected $namesProvider;

    /**
     * @var CheckoutSession
     */
    protected $checkoutSession;

    /**
     * CheckoutConfigProvider constructor.
     *
     * @param LayoutInterface $layout
     * @param CustomerExchangeFactory $customerExchangeFactory
     * @param Session $customerSession
     * @param CheckoutSession $checkoutSession
     * @param Http $request
     * @param NamesProvider $namesProvider
     */
    public function __construct(
        LayoutInterface $layout,
        CustomerExchangeFactory $customerExchangeFactory,
        Session $customerSession,
        CheckoutSession $checkoutSession,
        Http $request,
        NamesProvider $namesProvider
    ) {
        $this->layout = $layout;
        $this->customerExchangeFactory = $customerExchangeFactory;
        $this->customerSession = $customerSession;
        $this->request = $request;
        $this->namesProvider = $namesProvider;
        $this->checkoutSession = $checkoutSession;
    }

    /**
     * {@inheritdoc}
     */
    public function getConfig()
    {
        $quoteId = $this->checkoutSession->getQuote()->getId();
        $config = [];
        $config['shipping_promo_block'] = $this->layout->createBlock('Magento\Cms\Block\Block')->setBlockId('minicart_shipping_promo_block')->toHtml();
        $config['exchange_points_available'] = $this->exchangePointsAvailable();
        $config['is_checkout_page'] = $this->isCheckoutPage();
        $config['namesData'] = $this->namesProvider->getNames($quoteId);

        return $config;
    }

    /**
     * Return whether exchange points are available
     * ! Supposing only one exchange can be available
     * @return int
     */
    public function exchangePointsAvailable()
    {
        if ($customerId = $this->customerSession->getCustomerId()) {
            /**
             * @var Collection $exchangeCollection
             */
            $exchange = $this->customerExchangeFactory->create();
            $exchangeCollection = $exchange->getCollection()
                ->addFieldToFilter('customer_id', ['eq' => $customerId])
                ->addFieldToFilter('pending_order_id', ['null' => true])
                ->addFieldToFilter('rma_complete', ['eq' => false]);

            if ($exchangeCollection->count() > 0) {
                $first = $exchangeCollection->getFirstItem();
                return $first->getData('points_issued');
            } else {
                return 0;
            }
        } else {
            return 0;
        }
    }

    /**
     * Check whether current page is checkout
     * (Used to differ shopping bag from checkout page in FE)
     *
     * @return bool
     */
    public function isCheckoutPage()
    {
        $route = $this->request->getRouteName();
        $controller = $this->request->getControllerName();

        return $route == 'checkout' && $controller == 'index';
    }
}
