<?php

/**
 * @vendor Kurufootwear
 * @module Kurufootwear_Checkout
 * @author Krisjanis Veinbahs <krisjanisv@scandiweb.com>
 * @copyright Copyright (c) 2017 Scandiweb. All rights reserved.
 * @license http://opensource.org/licenses/afl-3.0.php Academic Free License (AFL 3.0)
 */

namespace Kurufootwear\Checkout\Block\Checkout\Cart;

use Kurufootwear\Checkout\Model\CheckoutConfigProvider;
use Kurufootwear\Checkout\Model\CheckoutConfigProvider as KuruCheckoutConfigProvider;
use Kurufootwear\Rma\Model\CustomerExchangeFactory;
use Magento\Checkout\Model\CompositeConfigProvider;
use Magento\Checkout\Model\Session as CheckoutSession;
use Magento\Customer\Model\Session;
use Magento\Framework\App\RequestInterface;
use Magento\Framework\Message\ManagerInterface;
use Magento\Framework\View\Element\Template\Context;
use Mirasvit\Rewards\Helper\Purchase;

class RewardsPoints extends \Mirasvit\Rewards\Block\Checkout\Cart\RewardsPoints
{
    /**
     * @var CheckoutConfigProvider
     */
    protected $checkoutConfig;

    /**
     * @var ManagerInterface
     */
    protected $messageManager;

    /**
     * @var Purchase
     */
    protected $rewardsPurchase;

    /**
     * @var
     */
    protected $kuruCheckoutConfig;

    /**
     * @var RequestInterface
     */
    protected $request;

    /**
     * RewardsPoints constructor.
     * @param Purchase $rewardsPurchase
     * @param Context $context
     * @param Session $customerSession
     * @param CheckoutSession $checkoutSession
     * @param CompositeConfigProvider $configProvider
     * @param KuruCheckoutConfigProvider $checkoutConfig
     * @param ManagerInterface $messageManager
     * @param KuruCheckoutConfigProvider $checkoutConfigProvider
     * @param array $layoutProcessors
     * @param array $data
     */
    public function __construct(
        Purchase $rewardsPurchase,
        Context $context,
        Session $customerSession,
        CheckoutSession $checkoutSession,
        CompositeConfigProvider $configProvider,
        CheckoutConfigProvider $checkoutConfig,
        ManagerInterface $messageManager,
        KuruCheckoutConfigProvider $checkoutConfigProvider,
        array $layoutProcessors = [],
        array $data = []
    )
    {
        $this->request = $context->getRequest();
        $this->checkoutConfig = $checkoutConfig;
        $this->messageManager = $messageManager;
        $this->rewardsPurchase = $rewardsPurchase;
        $this->kuruCheckoutConfig = $checkoutConfigProvider;

        if ($this->isShoppingBag()) {
            if ($exchangePoints = $this->kuruCheckoutConfig->exchangePointsAvailable()) {
                $this->setPoints($exchangePoints);
            } else {
                $this->resetPoints();
            }
        }

        parent::__construct(
            $rewardsPurchase,
            $context,
            $customerSession,
            $checkoutSession,
            $configProvider,
            $layoutProcessors,
            $data
        );
    }

    /**
     * Return whether user is logged in
     *
     * @return bool
     */
    public function isLoggedIn()
    {
        $logged = $this->_customerSession->isLoggedIn();

        return $logged;
    }

    /**
     * TODO - Return yes when block rendered in shopping bag
     * @return bool
     */
    public function isShoppingBag()
    {
        $routeName        = $this->request->getRouteName();
        $controllerName   = $this->request->getControllerName();
        $actionName       = $this->request->getActionName();

        $currentPath = $routeName . '/' . $controllerName . '/' . $actionName;
        $cartPath = 'checkout/cart/index';

        return $currentPath == $cartPath;
    }

    /**
     * Return whether customer has available
     * rma exchange points
     * @return bool
     */
    public function exchangePointsAvailable()
    {
        return $this->checkoutConfig->exchangePointsAvailable();
    }

    /**
     * Set current purchase reward points to zero
     */
    public function resetPoints()
    {
        $this->setPoints(0);
    }

    /**
     * Set used rewards points
     * for current existing purchase
     * registered customers only
     * @param $points
     * @return bool
     */
    public function setPoints($points)
    {
        /**
         * Sanity check for purchase, quote and customerId
         */
        $purchase = $this->rewardsPurchase->getPurchase();
        if (!$purchase || !$purchase->getQuote()->getCustomerId()) {
            return;
        }

        /**
         * Validate points not being set
         * to a larger than possible value
         */
        $purchase->refreshPointsNumber();
        $maxPoints = $purchase->getMaxPointsNumberToSpent();
        if ($points > $maxPoints) {
            $points = $maxPoints;
        }

        $purchase->setSpendPoints($points);
        $purchase->save();
    }

    /**
     * Emit notification message
     * @param string $text
     */
    public function notify($text)
    {
        $this->messageManager->addNoticeMessage($text);
    }

    /**
     * This is needed to take the correct templates
     *
     * @return string
     */
    protected function _toHtml()
    {
        $this->setModuleName($this->extractModuleName('Mirasvit\Rewards\Block\Checkout\Cart\RewardsPoints'));

        return parent::_toHtml();
    }
}
