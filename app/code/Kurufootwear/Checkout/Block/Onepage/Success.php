<?php
/**
 * @category     Kurufootwear
 * @package      Kurufootwear/Checkout
 * @copyright    Copyright (c) 2017 Scandiweb, Inc (http://scandiweb.com)
 * @license      http://opensource.org/licenses/OSL-3.0 The Open Software License 3.0 (OSL-3.0)
 */

namespace Kurufootwear\Checkout\Block\Onepage;

use Magento\Checkout\Block\Onepage\Success as MagentoSuccess;
use Magento\Framework\App\Http\Context as HttpContext;
use Magento\Framework\View\Element\Template\Context as TemplateContext;
use Magento\Checkout\Model\Session as CheckoutSession;
use Magento\Customer\Model\Session as CustomerSession;
use Magento\Sales\Model\Order;
use Magento\Sales\Model\Order\Config;

/**
 * One page checkout success page
 */
class Success extends MagentoSuccess
{
    /**
     * @var CustomerSession
     */
    protected $customerSession;

    /**
     * @param CustomerSession $customerSession
     * @param TemplateContext $context
     * @param CheckoutSession $checkoutSession
     * @param Config $orderConfig
     * @param HttpContext $httpContext
     * @param array $data
     */
    public function __construct(
        CustomerSession $customerSession,
        TemplateContext $context,
        CheckoutSession $checkoutSession,
        Config $orderConfig,
        HttpContext $httpContext,
        array $data = []
    )
    {
        $this->customerSession = $customerSession;

        parent::__construct(
            $context,
            $checkoutSession,
            $orderConfig,
            $httpContext,
            $data
        );
    }

    /**
     * Check if customer is logged in
     *
     * @return bool
     */
    public function isLoggedIn()
    {
        return $this->customerSession->isLoggedIn();
    }

    /**
     * This is needed to take the correct templates
     *
     * @return string
     */
    protected function _toHtml()
    {
        $this->setModuleName($this->extractModuleName('Magento\Checkout\Block\Onepage\Success'));

        return parent::_toHtml();
    }

    /**
     * Get guest order view url
     *
     * @return string
     */
    public function getGuestOrderViewUrl()
    {
        return $this->getUrl('', [
            '_query' => [
                'increment_id' => $this->getOrderId(),
                'email_address' => $this->_checkoutSession->getLastRealOrder()->getCustomerEmail()
            ],
            '_direct' => 'customer-service/order-status.html'
        ]);
    }
}
