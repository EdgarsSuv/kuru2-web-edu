<?php
/**
 * @vendor Kurufootwear
 * @module Kurufootwear_Checkout
 * @author Aleksejs Baranovs <info@scandiweb.com>
 * @copyright Copyright (c) 2018 KURU Footwear. All rights reserved.
 */

namespace Kurufootwear\Checkout\Block\Cart;

use Magento\Checkout\Block\Cart\Totals as BaseTotals;
use Magento\Checkout\Model\Session as CheckoutSession;
use Magento\Customer\Model\Session;
use Magento\Framework\Pricing\PriceCurrencyInterface;
use Magento\Framework\View\Element\Template\Context;
use Magento\Sales\Model\Config;

/**
 * Class Totals
 * Override to include new functionality
 * 
 * @package Kurufootwear\Checkout\Block\Cart
 */
class Totals extends BaseTotals
{
    /**
     * @var PriceCurrencyInterface
     */
    private $priceCurrency;

    /**
     * Totals constructor.
     *
     * @param Context $context
     * @param Session $customerSession
     * @param CheckoutSession $checkoutSession
     * @param Config $salesConfig
     * @param PriceCurrencyInterface $priceCurrency
     * @param array $layoutProcessors
     * @param array $data
     */
    public function __construct(
        Context $context,
        Session $customerSession,
        CheckoutSession $checkoutSession,
        Config $salesConfig,
        PriceCurrencyInterface $priceCurrency,
        array $layoutProcessors = [],
        array $data = []
    ) {
        parent::__construct($context, $customerSession, $checkoutSession, $salesConfig, $layoutProcessors, $data);

        $this->priceCurrency = $priceCurrency;
    }

    /**
     * Get formatted price amount
     *
     * @param $price
     *
     * @return string
     */
    public function getFormattedPrice($price)
    {
        return $this->priceCurrency->convertAndFormat($price, false);
    }
}
