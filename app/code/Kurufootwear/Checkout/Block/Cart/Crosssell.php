<?php
/**
 * @vendor Kurufootwear
 * @module Kurufootwear_Upsell
 * @author Krisjanis Veinbahs <info@scandiweb.com>
 * @copyright Copyright (c) 2018 KURU Footwear. All rights reserved.
 */
namespace Kurufootwear\Checkout\Block\Cart;

use Kurufootwear\Upsell\Helper\ProductPostHelper;
use Magento\Catalog\Block\Product\Context;
use Magento\Catalog\Model\Product\LinkFactory;
use Magento\Catalog\Model\Product\Visibility;
use Magento\CatalogInventory\Helper\Stock as StockHelper;
use Magento\Checkout\Block\Cart\Crosssell as MagentoCrosssell;
use Magento\Checkout\Model\Session;
use Magento\Quote\Model\Quote\Item\RelatedProducts;
use Magento\Catalog\Model\Product;
use Magento\Catalog\Pricing\Price\FinalPrice;
use Magento\Framework\Pricing\Render;

/**
 * Class Crosssell
 *
 * @package Kurufootwear\Checkout\Block\Cart
 */
class Crosssell extends MagentoCrosssell
{
    /**
     * @var ProductPostHelper
     */
    protected $productPostHelper;

    /**
     * Crosssell constructor.
     *
     * @param Context $context
     * @param ProductPostHelper $productPostHelper
     * @param Session $checkoutSession
     * @param Visibility $productVisibility
     * @param LinkFactory $productLinkFactory
     * @param RelatedProducts $itemRelationsList
     * @param StockHelper $stockHelper
     * @param array $data
     */
    public function __construct(
        Context $context,
        ProductPostHelper $productPostHelper,
        Session $checkoutSession,
        Visibility $productVisibility,
        LinkFactory $productLinkFactory,
        RelatedProducts $itemRelationsList,
        StockHelper $stockHelper,
        array $data = []
    ) {
        $this->productPostHelper = $productPostHelper;

        parent::__construct(
            $context,
            $checkoutSession,
            $productVisibility,
            $productLinkFactory,
            $itemRelationsList,
            $stockHelper,
            $data
        );
    }

    /**
     * Block scope wrapper for product add-to-cart
     * post payload helper
     *
     * @param $postUrl
     * @param $product
     *
     * @return string (serialized post payload array)
     */
    public function getProductPostData($postUrl, $product)
    {
        return $this->productPostHelper->getPostData($postUrl, $product);
    }

    /**
     * Return HTML block with price
     *
     * @param Product $product
     *
     * @return string
     */
    public function getProductPrice(Product $product)
    {
        return $this->getProductPriceHtml(
            $product,
            FinalPrice::PRICE_CODE,
            Render::ZONE_ITEM_LIST,
            ['only_new_price' => true]
        );
    }
}