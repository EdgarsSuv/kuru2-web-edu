<?php
/**
 * @category    Kurufootwear
 * @package     Kurufootwear\Checkout
 * @author      Kristaps Stalidzans <info@scandiweb.com>
 * @copyright   Copyright (c) 2017 Scandiweb, Inc (http://scandiweb.com)
 * @license     http://opensource.org/licenses/OSL-3.0 The Open Software License 3.0 (OSL-3.0)
 */
namespace Kurufootwear\Checkout\Logger;

class CheckoutPlaceOrderLogger extends \Monolog\Logger
{
}
