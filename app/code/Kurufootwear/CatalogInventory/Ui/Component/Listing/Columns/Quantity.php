<?php
/**
 * @category    Kurufootwear
 * @package     Kurufootwear\CatalogInventory
 * @author      Aleksejs Baranovs <info@scandiweb.com>
 * @copyright   Copyright (c) 2018 Scandiweb, Inc (http://scandiweb.com)
 * @license     http://opensource.org/licenses/OSL-3.0 The Open Software License 3.0 (OSL-3.0)
 */

namespace Kurufootwear\CatalogInventory\Ui\Component\Listing\Columns;

use Magento\Ui\Component\Listing\Columns\Column;

/**
 * Class Quantity
 * @package Kurufootwear\CatalogInventory\Ui\Component\Listing\Columns
 */
class Quantity extends Column
{
    /**
     * Prepare data source
     *
     * @param array $dataSource
     *
     * @return array
     */
    public function prepareDataSource(array $dataSource)
    {
        if (isset($dataSource['data']['items'])) {
            $fieldName = $this->getData('name');
            foreach ($dataSource['data']['items'] as &$item) {
                if (isset($item[$fieldName])) {
                    $item[$fieldName] = (int)$item[$fieldName];
                }
            }
        }

        return $dataSource;
    }
}
