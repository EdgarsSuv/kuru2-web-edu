<?php
/**
 * @category    Kurufootwear
 * @package     Kurufootwear\Catalog
 * @author      Kristaps Stalidzans <info@scandiweb.com>
 * @copyright   Copyright (c) 2017 Scandiweb, Inc (http://scandiweb.com)
 * @license     http://opensource.org/licenses/OSL-3.0 The Open Software License 3.0 (OSL-3.0)
 */
namespace Kurufootwear\Catalog\Model;

use Magento\Catalog\Model\ProductFactory;
use Magento\Framework\App\RequestInterface;

class UrlParser
{
    const WOMENS_SIZE = 'womens_size';
    const MENS_SIZE = 'size';
    const CONTROLLER = 'category_view';

    /**
     * @var RequestInterface
     */
    protected $request;

    /**
     * @var ProductFactory
     */
    protected $attributeLoading;

    /**
     * Flag attribute to check if attribute is there in query
     *
     * @var string
     */
    private $flag = null;

    /**
     * Size index for query
     *
     * @var null
     */
    private $sizeIndex = null;

    /**
     * Size label
     *
     * @var string
     */
    private $sizeLabel = null;

    /**
     * Size Url
     *
     * @var null
     */
    private $sizeUrl = null;

    /**
     * Controller check
     *
     * @var null
     */
    private $controller = null;

    /**
     * UrlParser constructor.
     * @param ProductFactory $attributeLoading
     * @param RequestInterface $request
     */
    public function __construct(
        ProductFactory $attributeLoading,
        RequestInterface $request
    )
    {
        $this->request = $request;
        $this->attributeLoading = $attributeLoading;
        $this->controller = $this->getRequest()->getControllerName() . '_' .$this->getRequest()->getActionName();
    }

    /**
     * Get request
     *
     * @return RequestInterface
     */
    public function getRequest()
    {
        return $this->request;
    }

    /**
     * Depending on page request it gets request string that is made with
     * Amasty then based on option value gets the size label that product
     * view javascript uses to preselect product size.
     *
     * @return null|string
     */
    public function getSize()
    {
        /**
         * Checks controller and allows to run it only in category view
         *
         * Speed optimizer to do not repeat itself if url is found
         */
        if ($this->controller !== self::CONTROLLER) {
            return null;
        } else if (!is_null($this->sizeUrl)) {
            return $this->sizeUrl;
        }

        $urlData = $this->getRequest()->getUri()->getQuery();
        $query = explode('&', $urlData);

        /**
         * Gets Size query index in array
         */
        if (is_array($query) && is_null($this->sizeIndex)) {
            $this->sizeIndex = $this->getSizeQuery($query, self::WOMENS_SIZE);

           if (is_null($this->sizeIndex)) {
                $this->sizeIndex = $this->getSizeQuery($query, self::MENS_SIZE);
            }
        }

        /**
         * Gets Size label
         */
        if (is_null($this->sizeUrl) && !is_null($this->sizeIndex) && !is_null($this->flag)
            && strpos($query[$this->sizeIndex], $this->flag) !== false) {
            $sizeId = str_replace($this->flag . '=', '', $query[$this->sizeIndex]);
            $this->sizeLabel = $this->getAttributeLabel((int)$sizeId, $this->flag);
        }

        /**
         * Creates the string
         */
        if ($this->sizeLabel) {
            $this->sizeUrl = '&' . $this->flag  . '=' . $this->sizeLabel;

            return $this->sizeUrl;
        }
    }

    /**
     * Returns Size index in query string
     *
     * @param array $queries
     * @param string $sizeAttribute
     * @return int|string
     */
    public function getSizeQuery(array $queries, string $sizeAttribute)
    {
        foreach($queries as $index => $string) {
            if (strpos($string, $sizeAttribute) !== false) {
                $this->flag = $sizeAttribute;

                return $index;
            }
        }
    }

    /**
     * Returns Option Label based on its id and attribute
     *
     * @param $optionId
     * @param $attribute
     * @return bool|string
     */
    public function getAttributeLabel($optionId , $attribute)
    {
        $productResource = $this->attributeLoading->create();
        $label = $productResource->getResource()->getAttribute($attribute);
        $attributeLabel = null;

        if ($label->usesSource()) {
            $attributeLabel = $label->getSource()->getOptionText($optionId);
        }

        if (!$attributeLabel) {
            $attributeLabel = $optionId;
        }

        return $attributeLabel;
    }
}
