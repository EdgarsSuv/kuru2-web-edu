<?php
/**
 * @vendor Kurufootwear
 * @module Kurufootwear_Catalog
 * @author Krisjanis Veinbahs <info@scandiweb.com>
 * @copyright Copyright (c) 2018 Scandiweb. All rights reserved.
 * @license http://opensource.org/licenses/afl-3.0.php Academic Free License (AFL 3.0)
 */
namespace Kurufootwear\Catalog\Model;

class Config extends \Magento\Catalog\Model\Config
{
    /**
     * @return array
     */
    public function getAttributeUsedForSortByArray()
    {
        $options =  [
            'position' => __('Featured Products'),
            'bestsellers' => __('Bestsellers'),
            'newest' => __('Newest Arrivals'),
            'price-low' => __('Price Low to High'),
            'price-high' => __('Price High To Low')
        ];

        return $options;
    }
}
