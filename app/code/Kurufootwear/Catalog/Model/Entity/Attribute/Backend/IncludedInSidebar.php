<?php
/**
 * @vendor      Kurufootwear
 * @module      Kurufootwear_Catalog
 * @author      Kristap Stalidzans <info@scandiweb.com>
 * @copyright   Copyright (c) 2017 Scandiweb, Ltd (http://scandiweb.com)
 * @license     http://opensource.org/licenses/afl-3.0.php Academic Free License (AFL 3.0)
 */

namespace Kurufootwear\Catalog\Model\Entity\Attribute\Backend;

use Magento\Eav\Model\Entity\Attribute\Backend\AbstractBackend;
use Magento\Framework\DataObject;
use Magento\Store\Model\StoreManagerInterface;

class IncludedInSidebar extends AbstractBackend
{
    const ATTRIBUTE = 'include_in_sidebar';

    /**
     * @var StoreManagerInterface
     */
    protected $storeManager;

    /**
     * CroppedImage constructor.
     *
     * @param StoreManagerInterface $storeManager
     */
    public function __construct(
        StoreManagerInterface $storeManager
    ) {
        $this->storeManager = $storeManager;
    }

    /**
     * Prepares attribute for saving
     * Removes old image
     *
     * @param DataObject $object
     * @return $this
     */
    public function beforeSave($object)
    {
        if (!$object->hasData(self::ATTRIBUTE)) {
            return $this;
        }

        /** @var array $data */
        $data = $object->getData(self::ATTRIBUTE, 0);

        $object->setData(self::ATTRIBUTE, $data['name']);

        return $this;
    }

}
