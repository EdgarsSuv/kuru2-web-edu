<?php
/**
 * @vendor Kurufootwear
 * @module Kurufootwear_Catalog
 * @author Kristaps Stalidzans <info@scandiweb.com>
 * @copyright Copyright (c) 2017 Scandiweb. All rights reserved.
 * @license http://opensource.org/licenses/afl-3.0.php Academic Free License (AFL 3.0)
 */
namespace Kurufootwear\Catalog\Model\Product;

use Magento\Catalog\Model\Product;
use Magento\Catalog\Model\ResourceModel\Product as ResourceProduct;
use Magento\Catalog\Model\ResourceModel\Url as ResourceUrl;
use Magento\CatalogUrlRewrite\Model\ProductUrlRewriteGenerator;
use Magento\Framework\App\Config\ScopeConfigInterface;
use Magento\Framework\DataObject;
use Magento\Framework\DataObjectFactory;
use Magento\Framework\Filter\FilterManager;
use Magento\Framework\Registry;
use Magento\Framework\Session\SidResolverInterface;
use Magento\Framework\UrlFactory;
use Magento\Store\Model\StoreManagerInterface;
use Magento\UrlRewrite\Model\UrlFinderInterface;
use Magento\UrlRewrite\Service\V1\Data\UrlRewrite;

class Url extends \Magento\Catalog\Model\Product\Url
{
    /**
     * "Should categories be in product url's" config path
     */
    const XML_PATH_PRODUCT_URL_USE_CATEGORY = 'catalog/seo/product_use_categories';

    /**
     * @var ResourceProduct
     */
    private $resourceProduct;

    /**
     * @var ScopeConfigInterface
     */
    private $scopeConfig;

    /**
     * @var DataObjectFactory
     */
    private $dataObjectFactory;

    /**
     * @var ResourceUrl
     */
    private $resourceUrl;

    /**
     * @var Registry
     */
    private $registry;

    /**
     * Url constructor.
     * @param UrlFactory $urlFactory
     * @param StoreManagerInterface $storeManager
     * @param FilterManager $filter
     * @param SidResolverInterface $sidResolver
     * @param UrlFinderInterface $urlFinder
     * @param ResourceProduct $resourceProduct
     * @param ScopeConfigInterface $scopeConfig
     * @param DataObjectFactory $dataObjectFactory
     * @param ResourceUrl $resourceUrl
     * @param Registry $registry
     * @param array $data
     */
    public function __construct(
        UrlFactory $urlFactory,
        StoreManagerInterface $storeManager,
        FilterManager $filter,
        SidResolverInterface $sidResolver,
        UrlFinderInterface $urlFinder,
        ResourceProduct $resourceProduct,
        ScopeConfigInterface $scopeConfig,
        DataObjectFactory $dataObjectFactory,
        ResourceUrl $resourceUrl,
        Registry $registry,
        array $data = []
    ) {
        $this->resourceProduct = $resourceProduct;
        $this->scopeConfig = $scopeConfig;
        $this->dataObjectFactory = $dataObjectFactory;
        $this->resourceUrl = $resourceUrl;
        $this->registry = $registry;

        parent::__construct(
            $urlFactory,
            $storeManager,
            $filter,
            $sidResolver,
            $urlFinder,
            $data
        );
    }

    /**
     * Retrieve Product URL
     *
     * Differs from getUrl with 'color', 'size' and '_nosid'
     *
     * @param Product $product
     * @param bool $useSid forced SID mode
     * @param $sizeParams
     *
     * @return string
     */
    public function getProductUrl($product, $useSid = null, array $sizeParams = [])
    {
        $url = null;
        if ($useSid === null) {
            $useSid = $this->sidResolver->getUseSessionInUrl();
        }

        $params = [];
        if (!$useSid) {
            $params['_nosid'] = true;
        }

        $parentSku = $product->getParentSku();
        $parentId = $product->getParentId();
        $storeId = $product->getStoreId();
        $doNotUseCategoryId = $product->getDoNotUseCategoryId();
        $useRewrites = !empty($product->getUrlDataObject());
        $productColorData = $product->getProductColorData() ?:
            $product->getAttributeText('color');

        /**
         * Check if parent product is present with color data un duplicate check
         */
        if (($parentSku || $parentId) && $productColorData) {
            $params['_query'] = array_merge(
                [
                    'color' => $productColorData
                ],
                $sizeParams
            );

            if ($parentSku) {
                $url = $this->getUrlBySku($parentSku, $storeId, $doNotUseCategoryId, $useRewrites, $params);
            } else if ($parentId) {
                $url = $this->getUrlById($parentId, $storeId, $doNotUseCategoryId, $useRewrites, $params);
            }
        } else {
            $url = $this->getUrl($product, $params);
        }

        return $url;
    }

    /**
     * Retrieve Product URL using UrlDataObject
     *
     * @param Product $product
     * @param array $params
     *
     * @return string
     */
    public function getUrl(Product $product, $params = []) : string
    {
        return $this->buildUrl($product, $params);
    }

    /**
     * Get only the required data from product by sku
     * and generate, return its url
     *
     * @param $productSku
     * @param $storeId
     * @param bool $doNotUseCategoryId
     * @param bool $useRewrites
     * @param array $params
     *
     * @return string
     */
    public function getUrlBySku($productSku, $storeId, $doNotUseCategoryId = null, $useRewrites = false, $params = []) : string
    {
        $productId = $this->resourceProduct->getIdBySku($productSku);

        return $this->getUrlById($productId, $storeId, $doNotUseCategoryId, $useRewrites, $params);
    }

    /**
     * Get only the required data from product by id
     * and generate, return its url
     *
     * @param $productId
     * @param $storeId
     * @param bool $doNotUseCategoryId
     * @param bool $useRewrites
     * @param array $params
     *
     * @return string
     */
    public function getUrlById($productId, $storeId, $doNotUseCategoryId = null, $useRewrites = false, $params = []) : string
    {
        $productMock = $this->dataObjectFactory->create();
        $productMock->setId($productId);

        // UrlKey
        $urlKey = $this->resourceProduct->getAttributeRawValue($productId, 'url_key', $storeId);
        if ($urlKey) {
            $productMock->setUrlKey($urlKey);
            $productMock->setRequestPath($urlKey . '.html');
        }

        // UrlDataObject
        $urlData = $this->resourceUrl->getRewriteByProductStore([(int)$productId => (int)$storeId]);
        $urlDatum = reset($urlData);
        $urlDataObject = $urlDatum ? $this->dataObjectFactory->create()->setData($urlDatum) : null;
        if (!empty($urlDataObject) && $useRewrites) {
            $productMock->setUrlDataObject($urlDataObject);
        }

        // CategoryId
        $category = $this->registry->registry('current_category');
        if ($category) {
            $productMock->setCategoryId($category->getId());
        }

        // DoNotUseCategoryId
        $productMock->setDoNotUseCategoryId($doNotUseCategoryId);

        return $this->buildUrl($productMock, $params);
    }

    /**
     * Retrieve Product URL using UrlDataObject
     *
     * @param DataObject $productMock
     * / Required product data
     * - Id
     * - StoreId
     * - CategoryId
     * - DoNotUseCategoryId
     * - UrlDataObject
     * - RequestPath
     * - UrlKey
     * @param array $params
     *
     * @SuppressWarnings(PHPMD.CyclomaticComplexity)
     * @SuppressWarnings(PHPMD.NPathComplexity)
     *
     * @return string
     */
    public function buildUrl(DataObject $productMock, $params = []) : string
    {
        $routePath = '';
        $routeParams = $params;
        $storeId = $productMock->getStoreId();
        $categoryId = $productMock->getCategoryId();

        /**
         * Check whether category should be incorporated in URL
         */
        $doNotUseCategoryId = $productMock->getDoNotUseCategoryId();
        $useCategoryUrlConfig = $this->scopeConfig->getValue(
            \Magento\Catalog\Helper\Product::XML_PATH_PRODUCT_URL_USE_CATEGORY,
            \Magento\Store\Model\ScopeInterface::SCOPE_STORE,
            $storeId
        );
        $ignoreCategoryParameter = isset($params['_ignore_category']);
        $useCategoryUrl = $useCategoryUrlConfig && !$ignoreCategoryParameter && $categoryId && !$doNotUseCategoryId;
        if (!$useCategoryUrl) {
            $categoryId = null;
        }

        /**
         * Url generation
         */
        if ($productMock->hasUrlDataObject()) {
            $requestPath = $productMock->getUrlDataObject()->getUrlRewrite();
            $routeParams['_scope'] = $productMock->getUrlDataObject()->getStoreId();
        } else {
            $requestPath = $productMock->getRequestPath();
            if (empty($requestPath) && $requestPath !== false) {
                $filterData = [
                    UrlRewrite::ENTITY_ID => $productMock->getId(),
                    UrlRewrite::ENTITY_TYPE => ProductUrlRewriteGenerator::ENTITY_TYPE,
                    UrlRewrite::STORE_ID => $storeId,
                ];
                if ($categoryId) {
                    $filterData[UrlRewrite::METADATA]['category_id'] = $categoryId;
                }
                $rewrite = $this->urlFinder->findOneByData($filterData);
                if ($rewrite) {
                    $requestPath = $rewrite->getRequestPath();
                    $productMock->setRequestPath($requestPath);
                } else {
                    $productMock->setRequestPath(false);
                }
            }
        }

        if (isset($routeParams['_scope'])) {
            $storeId = $this->storeManager->getStore($routeParams['_scope'])->getId();
        }

        if ($storeId !== (int)$this->storeManager->getStore()->getId()) {
            $routeParams['_scope_to_url'] = true;
        }

        if (!empty($requestPath)) {
            $routeParams['_direct'] = $requestPath;
        } else {
            $routePath = 'catalog/product/view';
            $routeParams['id'] = $productMock->getId();
            $routeParams['s'] = $productMock->getUrlKey();
            if ($categoryId) {
                $routeParams['category'] = $categoryId;
            }
        }

        // reset cached URL instance GET query params
        if (!isset($routeParams['_query'])) {
            $routeParams['_query'] = [];
        }

        return $this->getUrlInstance()->setScope($storeId)->getUrl($routePath, $routeParams);
    }

    /**
     * Retrieve URL Instance
     *
     * @return \Magento\Framework\UrlInterface
     */
    private function getUrlInstance()
    {
        return $this->urlFactory->create();
    }
}
