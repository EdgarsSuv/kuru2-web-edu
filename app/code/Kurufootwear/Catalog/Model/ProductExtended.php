<?php
/**
 * @category    Kurufootwear
 * @package     Kurufootwear\Catalog
 * @author      Kristaps Stalidzans <info@scandiweb.com>
 * @copyright   Copyright (c) 2017 Scandiweb, Inc (http://scandiweb.com)
 * @license     http://opensource.org/licenses/OSL-3.0 The Open Software License 3.0 (OSL-3.0)
 */
namespace Kurufootwear\Catalog\Model;

use Kurufootwear\Catalog\Helper\Product\TypeHelper;
use Magento\Catalog\Model\ResourceModel\Product\CollectionFactory as ProductCollectionFactory;
use Kurufootwear\Catalog\Model\Product\Url;
use Magento\Catalog\Api\Data\ProductLinkExtensionFactory;
use Magento\Catalog\Api\Data\ProductLinkInterfaceFactory;
use Magento\Catalog\Api\ProductAttributeRepositoryInterface;
use Magento\CatalogInventory\Api\Data\StockItemInterfaceFactory;
use Magento\Framework\Api\DataObjectHelper;
use Magento\Framework\Api\ExtensionAttribute\JoinProcessorInterface;
use Magento\Framework\Api\ExtensionAttributesFactory;
use Magento\Framework\Data\CollectionFactory;
use Magento\Framework\Filesystem;
use Magento\Framework\Indexer\IndexerRegistry;
use Magento\Framework\Model\Context;
use Magento\Framework\Module\Manager;
use Magento\Framework\Registry;
use Magento\Store\Model\StoreManagerInterface;
use Magento\Catalog\Model\Product;
use Magento\Catalog\Api\CategoryRepositoryInterface;
use Magento\Framework\Api\AttributeValueFactory;
use Magento\Catalog\Model\Product\Attribute\Backend\Media\EntryConverterPool;
use Magento\Catalog\Model\ResourceModel;
use Magento\Catalog\Model\Indexer;
use Magento\Catalog\Model\ProductLink;
use Magento\ConfigurableProduct\Api\LinkManagementInterface;

class ProductExtended extends \Magento\Catalog\Model\Product
{
    /**
     * @var UrlParser
     */
    private $urlParser;

    /**
     * @var Url
     */
    private $kuruUrl;
    
    /**
     * @var LinkManagementInterface
     */
    private $linkManagement;

    /**
     * @var ProductCollectionFactory
     */
    private $productCollectionFactory;

    /**
     * @var TypeHelper
     */
    private $typeHelper;

    /**
     * ProductExtended constructor.
     * @param Context $context
     * @param Registry $registry
     * @param ExtensionAttributesFactory $extensionFactory
     * @param AttributeValueFactory $customAttributeFactory
     * @param StoreManagerInterface $storeManager
     * @param ProductAttributeRepositoryInterface $metadataService
     * @param Product\Url $url
     * @param Product\Link $productLink
     * @param Product\Configuration\Item\OptionFactory $itemOptionFactory
     * @param StockItemInterfaceFactory $stockItemFactory
     * @param Product\OptionFactory $catalogProductOptionFactory
     * @param Product\Visibility $catalogProductVisibility
     * @param Product\Attribute\Source\Status $catalogProductStatus
     * @param Product\Media\Config $catalogProductMediaConfig
     * @param Product\Type $catalogProductType
     * @param Manager $moduleManager
     * @param \Magento\Catalog\Helper\Product $catalogProduct
     * @param ResourceModel\Product $resource
     * @param ResourceModel\Product\Collection $resourceCollection
     * @param CollectionFactory $collectionFactory
     * @param Filesystem $filesystem
     * @param IndexerRegistry $indexerRegistry
     * @param Indexer\Product\Flat\Processor $productFlatIndexerProcessor
     * @param Indexer\Product\Price\Processor $productPriceIndexerProcessor
     * @param Indexer\Product\Eav\Processor $productEavIndexerProcessor
     * @param CategoryRepositoryInterface $categoryRepository
     * @param Product\Image\CacheFactory $imageCacheFactory
     * @param ProductLink\CollectionProvider $entityCollectionProvider
     * @param Product\LinkTypeProvider $linkTypeProvider
     * @param ProductLinkInterfaceFactory $productLinkFactory
     * @param ProductLinkExtensionFactory $productLinkExtensionFactory
     * @param EntryConverterPool $mediaGalleryEntryConverterPool
     * @param DataObjectHelper $dataObjectHelper
     * @param JoinProcessorInterface $joinProcessor
     * @param UrlParser $urlParser
     * @param Url $kuruUrl
     * @param LinkManagementInterface $linkManagement
     * @param ProductCollectionFactory $productCollectionFactory
     * @param TypeHelper $typeHelper
     * @param array $data
     */
    public function __construct(
        Context $context,
        Registry $registry,
        ExtensionAttributesFactory $extensionFactory,
        AttributeValueFactory $customAttributeFactory,
        StoreManagerInterface $storeManager,
        ProductAttributeRepositoryInterface $metadataService,
        Product\Url $url,
        Product\Link $productLink,
        Product\Configuration\Item\OptionFactory $itemOptionFactory,
        StockItemInterfaceFactory $stockItemFactory,
        Product\OptionFactory $catalogProductOptionFactory,
        Product\Visibility $catalogProductVisibility,
        Product\Attribute\Source\Status $catalogProductStatus,
        Product\Media\Config $catalogProductMediaConfig,
        Product\Type $catalogProductType,
        Manager $moduleManager,
        \Magento\Catalog\Helper\Product $catalogProduct,
        ResourceModel\Product $resource,
        ResourceModel\Product\Collection $resourceCollection,
        CollectionFactory $collectionFactory,
        Filesystem $filesystem,
        IndexerRegistry $indexerRegistry,
        Indexer\Product\Flat\Processor $productFlatIndexerProcessor,
        Indexer\Product\Price\Processor $productPriceIndexerProcessor,
        Indexer\Product\Eav\Processor $productEavIndexerProcessor,
        CategoryRepositoryInterface $categoryRepository,
        Product\Image\CacheFactory $imageCacheFactory,
        ProductLink\CollectionProvider $entityCollectionProvider,
        Product\LinkTypeProvider $linkTypeProvider,
        ProductLinkInterfaceFactory $productLinkFactory,
        ProductLinkExtensionFactory $productLinkExtensionFactory,
        EntryConverterPool $mediaGalleryEntryConverterPool,
        DataObjectHelper $dataObjectHelper,
        JoinProcessorInterface $joinProcessor,
        UrlParser $urlParser,
        Url $kuruUrl,
        LinkManagementInterface $linkManagement,
        ProductCollectionFactory $productCollectionFactory,
        TypeHelper $typeHelper,
        array $data = []
    ) {
        $this->urlParser = $urlParser;
        $this->kuruUrl = $kuruUrl;
        $this->linkManagement = $linkManagement;
        $this->productCollectionFactory = $productCollectionFactory;
        $this->typeHelper = $typeHelper;

        parent::__construct(
            $context,
            $registry,
            $extensionFactory,
            $customAttributeFactory,
            $storeManager,
            $metadataService,
            $url,
            $productLink,
            $itemOptionFactory,
            $stockItemFactory,
            $catalogProductOptionFactory,
            $catalogProductVisibility,
            $catalogProductStatus,
            $catalogProductMediaConfig,
            $catalogProductType,
            $moduleManager,
            $catalogProduct,
            $resource,
            $resourceCollection,
            $collectionFactory,
            $filesystem,
            $indexerRegistry,
            $productFlatIndexerProcessor,
            $productPriceIndexerProcessor,
            $productEavIndexerProcessor,
            $categoryRepository,
            $imageCacheFactory,
            $entityCollectionProvider,
            $linkTypeProvider,
            $productLinkFactory,
            $productLinkExtensionFactory,
            $mediaGalleryEntryConverterPool,
            $dataObjectHelper,
            $joinProcessor,
            $data
        );
    }



    /**
     * Retrieve Product URL
     *
     * @param  bool $useSid
     * @return string
     */
    public function getProductUrl($useSid = null)
    {
        $size = $this->urlParser->getSize();
        $returnUrl = $this->kuruUrl->getProductUrl($this, $useSid);

        if ($size) {
            $returnUrl .= $size;
        }

        return $returnUrl;
    }
    
    /**
     * This function returns the parent of this product if there is one.
     * It assumes KURU's grandparent > parent > child relationship and relies on the parent_sku property.
     * 
     * If this is a grandparent it will return the parent for the given color
     * $colorAttributeId is required in this case.
     * 
     * If this is a parent it will simply return itself.
     * 
     * If this is a child product it will return the parent.
     * $colorAttributeId is NOT required in this case.
     * 
     * @param $colorAttributeId
     * @return \Magento\Framework\DataObject
     */
    public function getParent($colorAttributeId = null)
    {
        /** @var \Kurufootwear\Catalog\Model\ProductExtended */
        $parent = null;

        if (empty($this->getParentSku()) && $this->getTypeId() == 'configurable') {
            // $this is a grandparent
            assert(!empty($colorAttributeId));
            if (!empty($colorAttributeId)) {
                // $this is a grandparent.
                // Parent shoes do not have the color attribute set.
                // So we need to find all the parents of this grandparent
                // then for each parent get the first child (simple) and
                // check the color attribute
                $parents = $this->productCollectionFactory->create()
                    ->addAttributeToFilter('parent_sku', $this->getSku());
                foreach ($parents as $testParent) {
                    // Check the color of the first child
                    $collection = $this->productCollectionFactory->create()
                        ->addFieldToSelect('color')
                        ->addFieldToFilter('type_id', 'simple')
                        ->addFieldToFilter('parent_sku', $testParent->getSku());
                    $firstChild = $collection->getFirstItem();
                    if ($firstChild->hasData() && $firstChild->getColor() == $colorAttributeId) {
                        $parent = $testParent;
                        break;
                    }
                }
            }
        }
        else if (!empty($this->getParentSku()) && $this->getTypeId() == 'simple') {
            // $this is a child.
            $collection = $this->productCollectionFactory->create()
                ->addAttributeToFilter('sku', $this->getParentSku());
            $parent = $collection->getFirstItem();
        }
        else {
            // $this is a parent already
            return $this;
        }
        
        if ($parent != null && empty($parent->getData())) {
            // The parent wasn't found in the preceding logic.
            // Probably a misconfigured parent_sku.
            $parent = null;
            $this->_logger->error(sprintf('Cannot get parent product using the following child: ID - %s, SKU - %s', $this->getId(), $this->getSku()));
        }
        
        return $parent;
    }
    
    /**
     * Helper function that returns the grand parent configurable.
     * 
     * @return ProductExtended|\Magento\Framework\DataObject|null
     */
    public function getGrandParent()
    {
        /** @var \Kurufootwear\Catalog\Model\ProductExtended */
        $grandParent = null;
    
        if (!empty($this->getParentSku()) && $this->getTypeId() == 'simple') {
            // $this is a child product
            $collection = $this->productCollectionFactory->create()
                ->addFieldToSelect('parent_sku')
                ->addFieldToFilter('type_id', 'configurable')
                ->addFieldToFilter('sku', $this->getParentSku());
            $grandParent = $collection->getFirstItem();
    
            if (!empty($grandParent->getParentSku()) && $grandParent->getTypeId() == 'configurable') {
                $collection = $this->productCollectionFactory->create()
                    ->addFieldToSelect('parent_sku')
                    ->addFieldToFilter('type_id', 'configurable')
                    ->addFieldToFilter('sku', $grandParent->getParentSku());
                $grandParent = $collection->getFirstItem();
            }
        }
    
        if (!empty($this->getParentSku()) && $this->getTypeId() == 'configurable') {
            // $this is a parent
            $collection = $this->productCollectionFactory->create()
                ->addFieldToSelect('parent_sku')
                ->addFieldToFilter('type_id', 'configurable')
                ->addFieldToFilter('sku', $this->getParentSku());
            $grandParent = $collection->getFirstItem();
        }
    
        if (empty($this->getParentSku()) && $this->getTypeId() == 'configurable') {
            // $this is already a grandparent
            $grandParent = $this;
        }
        
        return $grandParent;
    }

    /**
     * Get id for product, which is used in Yotpo reviews (grandparent)
     */
    public function getYotpoId()
    {
        $grandparent = $this->getGrandParent();

        return $grandparent ? $grandparent->getId() : null;
    }
}
