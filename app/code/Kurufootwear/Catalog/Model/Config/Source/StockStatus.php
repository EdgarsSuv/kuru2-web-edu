<?php
/**
 * @category    Kurufootwear
 * @package     Kurufootwear\Catalog
 * @author      Aleksejs Baranovs <info@scandiweb.com>
 * @copyright   Copyright (c) 2018 Scandiweb, Inc (http://scandiweb.com)
 * @license     http://opensource.org/licenses/OSL-3.0 The Open Software License 3.0 (OSL-3.0)
 */

namespace Kurufootwear\Catalog\Model\Config\Source;

use Magento\Framework\Data\OptionSourceInterface;

/**
 * Class StockStatus
 * @package Kurufootwear\Catalog\Model\Config\Source
 */
class StockStatus implements OptionSourceInterface
{
    /**
     * Get stock status options
     *
     * @return array
     */
    public function toOptionArray()
    {
        return array(
            ['value' => 0, 'label' => __('Out of Stock')],
            ['value' => 1, 'label' => __('In Stock')],
        );
    }
}
