<?php
/**
 * @category    Kurufootwear
 * @package     Kurufootwear\Catalog
 * @author      Krisjanis Veinbahs <info@scandiweb.com>
 * @copyright   Copyright (c) 2017 Scandiweb, Inc (http://scandiweb.com)
 * @license     http://opensource.org/licenses/OSL-3.0 The Open Software License 3.0 (OSL-3.0)
 */
namespace Kurufootwear\Catalog\Model;

use Magento\Framework\App\Request\Http;
use Magento\Checkout\Model\ConfigProviderInterface;
use Magento\Framework\View\DesignInterface;

class CheckoutConfigProvider implements ConfigProviderInterface
{
    /**
     * @var Http
     */
    protected $request;

    /**
     * CheckoutConfigProvider constructor.
     * @param Http $request
     */
    public function __construct(
        Http $request,
        DesignInterface $design
    )
    {
        $this->request = $request;
        $this->design = $design;
    }

    /**
     * {@inheritdoc}
     */
    public function getConfig()
    {
        if ($this->showDecimalsInUrl()) {
            $config['requiredPricePrecision'] = 2;
        } else {
            $config['requiredPricePrecision'] = 0;
        }

        return $config;
    }

    /**
     * Return whether prices in current url should be shown
     * with decimals.
     */
    public function showDecimalsInUrl()
    {
        $route = $this->request->getRouteName();
        $action = $this->request->getActionName();

        $isBraintree = $route == 'braintree';
        $isCheckout = $route == 'checkout';
        $isPaypal = $route == 'paypal';
        $isProductEdit = $action == 'configure';

        return $isBraintree || $isPaypal || ($isCheckout && !$isProductEdit) || $this->isAdmin();
    }

    public function isAdmin()
    {
        return $this->design->getArea() == 'adminhtml';
    }
}
