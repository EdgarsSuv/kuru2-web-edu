<?php
/**
 * @category     Kurufootwear
 * @package      Kurufootwear/Catalog
 * @author       Krisjanis Veinbahs <krisjanisv@kurufootwear.com>
 * @copyright    Copyright (c) 2018 KURU Footwear. All rights reserved.
 */
namespace Kurufootwear\Catalog\Helper\Product\Url;

use Exception;
use Magento\Catalog\Api\Data\ProductInterface;
use Magento\Framework\Exception\AlreadyExistsException;
use Magento\Framework\Exception\LocalizedException;
use Psr\Log\LoggerInterface;
use Magento\UrlRewrite\Model\UrlPersistInterface;
use Magento\CatalogUrlRewrite\Model\ProductUrlRewriteGenerator;
use Magento\UrlRewrite\Service\V1\Data\UrlRewrite;
use Kurufootwear\Catalog\Helper\Product\TypeHelper as ProductTypeHelper;

/**
 * Class Regeneration
 *
 * @package Kurufootwear\Catalog\Helper\Product\Url
 */
class Regeneration
{
    /**
     * @var LoggerInterface
     */
    protected $logger;

    /**
     * @var ProductUrlRewriteGenerator
     */
    protected $productUrlRewriteGenerator;

    /**
     * @var UrlPersistInterface
     */
    protected $urlPersist;

    /**
     * @var ProductTypeHelper
     */
    protected $productTypeHelper;

    /**
     * @var ConsistencyInfo
     */
    protected $consistencyInfo;

    /**
     * Message prefix used in logs
     */
    const MIGRATION_LOG_PREFIX = "Url key migration fix";

    /**
     * Force url key incrementation even if it seems optional
     * @var bool
     */
    protected $forceIncrementation = false;

    /**
     * Regeneration constructor.
     *
     * @param LoggerInterface $logger
     * @param ProductUrlRewriteGenerator $productUrlRewriteGenerator
     * @param UrlPersistInterface $urlPersist
     * @param ProductTypeHelper $productTypeHelper
     * @param ConsistencyInfo $consistencyInfo
     */
    public function __construct(
        LoggerInterface $logger,
        ProductUrlRewriteGenerator $productUrlRewriteGenerator,
        UrlPersistInterface $urlPersist,
        ProductTypeHelper $productTypeHelper,
        ConsistencyInfo $consistencyInfo
    ) {
        $this->logger = $logger;
        $this->productUrlRewriteGenerator = $productUrlRewriteGenerator;
        $this->urlPersist = $urlPersist;
        $this->productTypeHelper = $productTypeHelper;
        $this->consistencyInfo = $consistencyInfo;
    }

    /**
     * Append a string to a product url key to make it unique
     * For masters its just an integer increment
     * For duplicates it's color (and integer increment)
     * For simples it's color and size (and integer increment)
     *
     * @param ProductInterface $product
     * @throws LocalizedException
     * @throws Exception
     */
    public function correctProductUrlKey(ProductInterface $product)
    {
        $this->urlKeyMigrationLog(
            'Trying to fix product.',
            'debug',
            'Product id - ' . $product->getId()
        );

        $kuruProductType = $this->productTypeHelper->getKuruProductType($product);

        try {
            switch ($kuruProductType) {
                case ProductTypeHelper::KURU_CHILD:
                    $this->fixChildUrl($product);
                    break;
                case ProductTypeHelper::KURU_DUPLICATE:
                    $this->fixDuplicateUrl($product);
                    break;
                case ProductTypeHelper::KURU_MASTER:
                    $this->fixMasterUrl($product);
                    break;
            }
        } catch (\Exception $exception) {
            $this->urlKeyMigrationLog(
                'Uncaught exception',
                'debug',
                $exception->getMessage()
            );

            throw $exception;
        }
    }

    /**
     * Attempt fixing master product url issues
     *
     * @param ProductInterface $product
     * @throws Exception
     * @throws LocalizedException
     */
    public function fixMasterUrl(ProductInterface $product)
    {
        $baseUrlKey = $product->getUrlKey();
        $newUrlKey = $this->findAndIncrementUrlKey($product, $baseUrlKey);

        $this->saveProductUrlKeyFix($product, $newUrlKey);
    }

    /**
     * Attempt fixing duplicate url key with color appendage
     * and optionally (if namespace still colliding) with increments.
     *
     * @param $product
     * @throws Exception
     * @throws LocalizedException
     * @throws \Zend_Db_Statement_Exception
     */
    public function fixDuplicateUrl($product)
    {
        try {
            $master = $this->productTypeHelper->getMasterFromDuplicate($product);
            $baseUrlKey = $master->getUrlKey();
        } catch (\Exception $exception) {
            $this->urlKeyMigrationLog(
                'Duplicate-master link is broken, likely old product, falling back to increments.',
                'debug'
            );
            $baseUrlKey = $product->getUrlKey();
        }

        $extendedUrlKey = $baseUrlKey;

        /**
         * Formatting to lowercase letters and dashes only
         */
        $colorAppendage = preg_replace(
            "/[^\w-]+/", "",
            strtolower($product->getData('product_color_data'))
        );
        if ($colorAppendage && strpos($baseUrlKey, $colorAppendage) === false) {
            $extendedUrlKey .= '-' . $colorAppendage;
        }

        /**
         * If size or color was appended, don't force url key incrementation
         */
        $incrementOptional = $baseUrlKey !== $extendedUrlKey;
        $newUrlKey = $this->findAndIncrementUrlKey($product, $extendedUrlKey, $incrementOptional);

        $this->saveProductUrlKeyFix($product, $newUrlKey);
    }

    /**
     * Attempt fixing child product by recreating url key from master with color and size appendages
     * and optionally (if namespace still colliding) with increments.
     *
     * @param $product
     * @throws Exception
     * @throws LocalizedException
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     * @throws \Zend_Db_Statement_Exception
     */
    public function fixChildUrl($product)
    {
        try {
            $master = $this->productTypeHelper->getMasterFromSimple($product);
            $baseUrlKey = $master->getUrlKey();
        } catch (\Exception $exception) {
            $this->urlKeyMigrationLog(
                'Simple-master link is broken, likely old product, falling back to increments only.',
                'debug'
            );
            $baseUrlKey = $product->getUrlKey();
        }

        $extendedUrlKey = $baseUrlKey;

        /**
         * Formatting to lowercase letters and dashes only
         */
        $colorAppendage = preg_replace(
            "/[^\w-]+/", "",
            strtolower($product->getAttributeText('color'))
        );
        if ($colorAppendage && strpos($baseUrlKey, $colorAppendage) === false) {
            $extendedUrlKey .= '-' . $colorAppendage;
        }

        /**
         * Formatting to lowercase letters and dashes only
         */
        $sizeAppendage = $product->getAttributeText('size') ?:
            $product->getAttributeText('womens_size') ?:
            $product->getAttributeText('sock_size');
        $sizeAppendage = preg_replace(
            "/[^\w-]+/", "",
            strtolower($sizeAppendage)
        );
        if ($sizeAppendage && strpos($baseUrlKey, $sizeAppendage) === false) {
            $extendedUrlKey .= '-' . $sizeAppendage;
        }

        /**
         * If size or color was appended, don't force url key incrementation
         */
        $incrementOptional = $baseUrlKey !== $extendedUrlKey;
        $newUrlKey = $this->findAndIncrementUrlKey($product, $extendedUrlKey, $incrementOptional);

        $this->saveProductUrlKeyFix($product, $newUrlKey);
    }

    /**
     * Add increments to a url key so that it doesn't collide with existing url keys
     *
     * @param $product
     * @param $baseUrlKey
     * @param bool $incrementOptional - Don't add increment if url key given is already unique
     * @throws Exception
     * @throws \Zend_Db_Statement_Exception
     *
     * @return string
     */
    public function findAndIncrementUrlKey($product, $baseUrlKey, $incrementOptional = true)
    {
        $incrementId = 1;
        $productStoreId = $product->getStoreId();
        $reservedUrlKeys = $this->consistencyInfo->queryUrlKeys(
            $baseUrlKey . '%'
        );

        $incrementOptional = $this->forceIncrementation ? false : $incrementOptional;
        $baseUrlKeyReserved = array_key_exists($baseUrlKey, $reservedUrlKeys) &&
            array_search($productStoreId, $reservedUrlKeys[$baseUrlKey]) !== false;
        if ($incrementOptional && !$baseUrlKeyReserved) {
            return $baseUrlKey;
        }

        $newUrlKey = $baseUrlKey . '-' . $incrementId;
        $isUrlKeyReserved = array_key_exists($newUrlKey, $reservedUrlKeys) &&
            array_search($productStoreId, $reservedUrlKeys[$newUrlKey]) !== false;

        while ($isUrlKeyReserved) {
            $incrementId++;
            $newUrlKey = $baseUrlKey . '-' . $incrementId;
            $isUrlKeyReserved = array_key_exists($newUrlKey, $reservedUrlKeys) &&
                array_search($productStoreId, $reservedUrlKeys[$newUrlKey]) !== false;

            if ($incrementId > 10000) {
                throw new Exception(__("Product url key has too many increments"));
            }
        }

        return $newUrlKey;
    }

    /**
     * Generic wrapper for saving product with new url key.
     *
     * Saves url key in the specific store product was loaded from
     * and recreates url rewrites
     *
     * @param ProductInterface $product
     * @param $urlKey
     *
     * @throws Exception
     */
    public function saveProductUrlKeyFix(ProductInterface $product, $urlKey)
    {
        try {
            /**
             * Don't try creating perma-redirect from old url key to the new one,
             * ('save_rewrites_history' => false)
             *
             * Remove faulty url rewrites only if AlreadyExistsException is thrown.
             */
            try {
                $product->setUrlKey($urlKey)
                    ->setData('save_rewrites_history', false)
                    ->save();

                $this->setUrlRewrites($product, false);
            } catch (AlreadyExistsException $alreadyExistsException) {
                $product->setUrlKey($urlKey)
                    ->setUrlPath($urlKey)
                    ->setData('save_rewrites_history', false)
                    ->save();

                $this->setUrlRewrites($product, true);
            }

            $this->urlKeyMigrationLog(
                'Successfully fixed product url key',
                'debug'
            );
        } catch (Exception $exception) {
            $this->urlKeyMigrationLog(
                'Couldn\'t save product with new url key',
                'debug',
                $exception->getMessage()
            );

            throw $exception;
        }
    }

    /**
     * Change state of internal variable $forceIncrementation
     *
     * @param $bool
     */
    public function setForceIncrementation($bool)
    {
        $this->forceIncrementation = $bool;
    }

    /**
     * Recreates Url Rewrites
     *
     * @param ProductInterface $product
     * @param bool $forceDeleteOldRewrites
     * @throws AlreadyExistsException
     */
    private function setUrlRewrites(ProductInterface $product, $forceDeleteOldRewrites = false)
    {
        if ($forceDeleteOldRewrites) {
            $this->urlPersist->deleteByData([
                UrlRewrite::ENTITY_ID => $product->getId(),
                UrlRewrite::ENTITY_TYPE => ProductUrlRewriteGenerator::ENTITY_TYPE,
                UrlRewrite::REDIRECT_TYPE => 0,
                UrlRewrite::STORE_ID => [0, 1, 2]
            ]);
        }

        $this->urlPersist->replace(
            $this->productUrlRewriteGenerator->generate($product)
        );
    }

    /**
     * Helper function for logging
     *
     * @param $comment
     * @param $logLevel
     * @param $exceptionLog
     */
    private function urlKeyMigrationLog($comment, $logLevel, $exceptionLog = null)
    {
        $this->logger->{$logLevel}(
            sprintf(
                "%s: %s.%s",
                static::MIGRATION_LOG_PREFIX,
                $comment,
                is_null($exceptionLog) ? "" : "\nInfo: " . $exceptionLog
            )
        );
    }
}