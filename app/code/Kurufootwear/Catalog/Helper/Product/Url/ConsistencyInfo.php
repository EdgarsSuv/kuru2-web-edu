<?php
/**
 * @category     Kurufootwear
 * @package      Kurufootwear/Catalog
 * @author       Krisjanis Veinbahs <krisjanisv@kurufootwear.com>
 * @copyright    Copyright (c) 2018 KURU Footwear. All rights reserved.
 */
namespace Kurufootwear\Catalog\Helper\Product\Url;

use Magento\Eav\Model\ResourceModel\Entity\Attribute as EavAttribute;
use Magento\Framework\App\ResourceConnection;
use Zend_Db_Statement_Exception;

/**
 * Class ConsistencyInfo
 * @package Kurufootwear\Catalog\Helper\Product
 */
class ConsistencyInfo
{
    /**
     * @var EavAttribute
     */
    protected $eavAttribute;

    /**
     * @var ResourceConnection
     */
    protected $resourceConnection;

    /**
     * UrlConsistency constructor.
     *
     * @param EavAttribute $eavAttribute
     * @param ResourceConnection $resourceConnection
     */
    public function __construct(
        EavAttribute $eavAttribute,
        ResourceConnection $resourceConnection
    ) {
        $this->eavAttribute = $eavAttribute;
        $this->resourceConnection = $resourceConnection;
    }

    /**
     * Query all product ids with broken url rewrites or url keys
     *
     * @throws Zend_Db_Statement_Exception
     * @return array
     */
    public function queryProductsWithUrlProblems()
    {
        $brokenRewriteIds = $this->queryInconsistentUrlRewriteProducts();
        $brokenKeyProducts = $this->queryDuplicateUrlKeyProducts();

        $brokenProducts = [];

        foreach ($brokenRewriteIds as $brokenProductId => $storeIds) {
            $brokenProductStores = array_key_exists($brokenProductId, $brokenProducts) ?
                $brokenProducts[$brokenProductId] : [];

            $brokenProducts[$brokenProductId] = array_merge($brokenProductStores, $storeIds);
        }
        foreach ($brokenKeyProducts as $brokenProductId => $storeIds) {
            $brokenProductStores = array_key_exists($brokenProductId, $brokenProducts) ?
                $brokenProducts[$brokenProductId] : [];

            $brokenProducts[$brokenProductId] = array_merge($brokenProductStores, $storeIds);
        }

        return $brokenProducts;
    }

    /**
     * Query all products ids with inconsistencies in url rewrites.
     *
     * Matches the following criteria:
     * - Has a url rewrite with its url key (url key - chicane -> url rewrite - chicane.html)
     * - The matching url rewrite and url key entity_id's are different
     *
     * @throws Zend_Db_Statement_Exception
     *
     * @return array
     */
    public function queryInconsistentUrlRewriteProducts()
    {
        $connection = $this->resourceConnection->getConnection();
        $urlKeyAttributeId = $this->eavAttribute->getIdByCode('catalog_product', 'url_key');
        $urlRewriteInconsistencyQuery = sprintf("
            SELECT
                rewrites.request_path as rewrite,
                rewrites.entity_id as rewrite_product_id,
                urlkeys.entity_id as urlkeys_product_id, 
                rewrites.store_id
            FROM url_rewrite rewrites
            INNER JOIN
                (
                    SELECT
                        entity_id,
                        store_id,
                        CONCAT(value, '.html') as value
                    FROM catalog_product_entity_varchar
                    WHERE attribute_id = %s
                ) urlkeys
            ON rewrites.request_path = urlkeys.value 
            WHERE
                rewrites.entity_type = \"product\" AND
                rewrites.entity_id <> urlkeys.entity_id AND
                rewrites.store_id = urlkeys.store_id
	    ", $urlKeyAttributeId);
        $queryResult = $connection->query($urlRewriteInconsistencyQuery)->fetchAll();

        $brokenProducts = [];
        foreach ($queryResult as $queryRow) {
            $productIdOne = $queryRow['rewrite_product_id'];
            $productIdTwo = $queryRow['urlkeys_product_id'];
            $storeId = $queryRow['store_id'];

            $brokenProductStores = array_key_exists($productIdOne, $brokenProducts) ?
                $brokenProducts[$productIdOne] : [];
            if (!array_key_exists($storeId, $brokenProductStores)) {
                $brokenProductStores[] = $storeId;
            }
            $brokenProducts[$productIdOne] = $brokenProductStores;

            $brokenProductStores = array_key_exists($productIdTwo, $brokenProducts) ?
                $brokenProducts[$productIdTwo] : [];
            if (!array_key_exists($storeId, $brokenProductStores)) {
                $brokenProductStores[] = $storeId;
            }
            $brokenProducts[$productIdTwo] = $brokenProductStores;
        }

        return $brokenProducts;
    }

    /**
     * Get all products ids with duplicate url keys
     *
     * @throws Zend_Db_Statement_Exception
     *
     * @return array
     */
    public function queryDuplicateUrlKeyProducts()
    {
        $connection = $this->resourceConnection->getConnection();
        $productVarcharTable = $this->resourceConnection->getTableName('catalog_product_entity_varchar');
        $duplicateUrlKeys = $this->queryDuplicateUrlKeys();

        if (empty($duplicateUrlKeys)) {
            return [];
        }

        $select = $connection->select();
        $select->from(
            $productVarcharTable,
            ['entity_id', 'store_id', 'value']
        )->where(
            'value IN (?)',
            [array_keys($duplicateUrlKeys)]
        )->where(
            'attribute_id = (?)',
            [$this->eavAttribute->getIdByCode('catalog_product', 'url_key')]
        );

        $queryResult = $connection->query($select)->fetchAll();

        $duplicateUrlKeyProducts = [];
        foreach ($queryResult as $queryRow) {
            $productId = $queryRow['entity_id'];
            $storeId = $queryRow['store_id'];
            $brokenProductStores = array_key_exists($productId, $duplicateUrlKeyProducts) ?
                $duplicateUrlKeyProducts[$productId] : [];

            if (!array_key_exists($storeId, $brokenProductStores)) {
                $brokenProductStores[] = $storeId;
            }

            $duplicateUrlKeyProducts[$productId] = $brokenProductStores;
        }

        return $duplicateUrlKeyProducts;
    }

    /**
     * Gets all duplicate url keys
     *
     * @param bool $rawQuery
     * @throws Zend_Db_Statement_Exception
     *
     * @return array
     */
    public function queryDuplicateUrlKeys($rawQuery = false)
    {
        $connection = $this->resourceConnection->getConnection();
        $productVarcharTable = $this->resourceConnection->getTableName('catalog_product_entity_varchar');

        $select = $connection->select();
        $select->from(
            $productVarcharTable,
            ['store_id', 'value', 'COUNT(value)']
        )->where(
            'attribute_id = (?)',
            [$this->eavAttribute->getIdByCode('catalog_product', 'url_key')]
        )->group(
            ['store_id', 'value']
        )->having(
            'COUNT(value) > 1'
        );

        $queryResult = $connection->query($select)->fetchAll();

        if ($rawQuery) {
            return $queryResult;
        }

        $formattedResult = [];
        foreach ($queryResult as $queryRow) {
            $formattedResult[$queryRow['value']] = $queryRow['store_id'];
        }

        return $formattedResult;
    }

    /**
     * Query url keys with a LIKE statement
     *
     * @param $urlKeyLike
     * @param bool $rawQuery
     * @throws Zend_Db_Statement_Exception
     *
     * @return array
     */
    public function queryUrlKeys($urlKeyLike, $rawQuery = false)
    {
        $connection = $this->resourceConnection->getConnection();
        $productVarcharTable = $this->resourceConnection->getTableName('catalog_product_entity_varchar');

        $select = $connection->select();
        $select->from(
            $productVarcharTable,
            ['store_id', 'value']
        )->where(
            'attribute_id = (?)',
            [$this->eavAttribute->getIdByCode('catalog_product', 'url_key')]
        )->where(
            'value LIKE (?)',
            $urlKeyLike
        );

        $queryResult = $connection->query($select)->fetchAll();

        if ($rawQuery) {
            return $queryResult;
        }

        $urlKeys = [];
        foreach ($queryResult as $queryRow) {
            $urlKey = $queryRow['value'];
            $storeId = $queryRow['store_id'];
            $urlKeyStores = array_key_exists($urlKey, $urlKeys) ?
                $urlKeys[$urlKey] : [];

            if (!array_key_exists($storeId, $urlKeyStores)) {
                $urlKeyStores[] = $storeId;
            }

            $urlKeys[$urlKey] = $urlKeyStores;
        }

        return $urlKeys;
    }
}