<?php
/**
 * @category     Kurufootwear
 * @package      Kurufootwear/Catalog
 * @author       Krisjanis Veinbahs <krisjanisv@kurufootwear.com>
 * @copyright    Copyright (c) 2018 KURU Footwear. All rights reserved.
 */
namespace Kurufootwear\Catalog\Helper\Product;

use Magento\Catalog\Api\Data\ProductInterface;
use Magento\Catalog\Model\Product\Attribute\Repository as AttributeRepository;
use Magento\Catalog\Model\Product\Attribute\Source\Status;
use Magento\Catalog\Model\Product\Type as DefaultProductType;
use Magento\Catalog\Model\ResourceModel\Product\CollectionFactory as ProductCollectionFactory;
use Magento\ConfigurableProduct\Model\Product\Type\Configurable as ConfigurableProductType;
use Magento\Catalog\Api\ProductRepositoryInterface;
use Magento\ConfigurableProduct\Api\LinkManagementInterface;
use Magento\ConfigurableProduct\Model\ResourceModel\Product\Type\ConfigurableFactory as ConfigurableProductFactory;
use Magento\Catalog\Model\ProductFactory;
use Magento\Framework\App\RequestInterface as RequestInterface;
use Magento\Framework\Exception\LocalizedException;
use Magento\Framework\Registry;

/**
 * Class TypeHelper
 *
 * Helper class for managing the Kurufootwear product structure
 *
 * @package Kurufootwear\Catalog\Helper\Product
 */
class TypeHelper
{
    /**
     * @var AttributeRepository
     */
    protected $productAttributeRepository;

    /**
     * @var ProductRepositoryInterface
     */
    protected $productRepository;

    /**
     * @var LinkManagementInterface
     */
    protected $linkManagement;

    /**
     * @var ConfigurableProductFactory
     */
    protected $configurableProductFactory;

    /**
     * @var ProductFactory
     */
    protected $productFactory;

    /**
     * @var ProductCollectionFactory
     */
    protected $productCollectionFactory;

    /**
     * @var Registry
     */
    protected $registry;

    /**
     * @var RequestInterface
     */
    protected $request;

    /**
     * Product types inherited from M1
     * Not an actual attribute, but used all throughout code
     */
    const KURU_MASTER = 'kuru_master';
    const KURU_DUPLICATE = 'kuru_duplicate';
    const KURU_CHILD = 'kuru_child';

    /**
     * TypeHelper constructor.
     *
     * @param AttributeRepository $productAttributeRepository
     * @param ProductRepositoryInterface $productRepository
     * @param LinkManagementInterface $linkManagement
     * @param ConfigurableProductFactory $configurableProductFactory
     * @param ProductFactory $productFactory
     * @param Registry $registry
     * @param RequestInterface $request
     */
    public function __construct(
        AttributeRepository $productAttributeRepository,
        ProductRepositoryInterface $productRepository,
        LinkManagementInterface $linkManagement,
        ConfigurableProductFactory $configurableProductFactory,
        ProductFactory $productFactory,
        ProductCollectionFactory $productCollectionFactory,
        Registry $registry,
        RequestInterface $request
    ) {
        $this->productAttributeRepository = $productAttributeRepository;
        $this->productRepository = $productRepository;
        $this->linkManagement = $linkManagement;
        $this->configurableProductFactory = $configurableProductFactory;
        $this->productFactory = $productFactory;
        $this->productCollectionFactory = $productCollectionFactory;
        $this->registry = $registry;
        $this->request = $request;
    }

    /**
     * A helper function for detecting a product type from M1:
     *
     * - Master products
     * - Duplicate products
     * - Simple products
     *
     * @param ProductInterface $product
     *
     * @return null|string
     */
    public function getKuruProductType(ProductInterface $product)
    {
        if ($product->getTypeId() === DefaultProductType::TYPE_SIMPLE) {
            return static::KURU_CHILD;
        }

        if ($product->getTypeId() === ConfigurableProductType::TYPE_CODE) {
            if ($product->getData('product_color_data') ||
                $product->getData('product_duplicate')) {
                return static::KURU_DUPLICATE;
            } else {
                return static::KURU_MASTER;
            }
        }

        return null;
    }

    /**
     * Get the master product from a duplicate product
     * by fetching a child and then his parent
     *
     * @param ProductInterface $duplicate
     * @param bool $idOnly
     * @return ProductInterface
     * @throws LocalizedException
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     */
    public function getMasterFromDuplicate(ProductInterface $duplicate, $idOnly = false)
    {
        $children = $this->linkManagement->getChildren($duplicate->getSku());
        if (count($children) == 0) {
            throw new LocalizedException(__("Duplicate product has no children linked."));
        }

        $childrenParentIds = $this->getParents($children[0]);
        if (count($childrenParentIds) > 2) {
            throw new LocalizedException(
                __("Simple product with more than two (master, duplicate) parents linked.")
            );
        }

        // Unset duplicate id from parent ids
        unset($childrenParentIds[array_search($duplicate->getId(), $childrenParentIds)]);

        // Pick the non-duplicate parent
        $masterId = $childrenParentIds[0];

        if ($idOnly) {
            return $masterId;
        }

        return $this->productRepository->getById($masterId);
    }

    /**
     * Get the master product from a simple product
     *
     * @param ProductInterface $product
     * @param bool $idOnly
     * @throws LocalizedException
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     *
     * @return int|ProductInterface|null
     */
    public function getMasterFromSimple(ProductInterface $product, $idOnly = false)
    {
        $childrenParentIds = $this->getParents($product);
        if (count($childrenParentIds) > 2) {
            throw new LocalizedException(
                __("Simple product with more than two (master, duplicate) parents linked.")
            );
        }

        foreach ($childrenParentIds as $childrenParentId) {
            $childrenParent = $this->productRepository->getById($childrenParentId);

            if ($this->getKuruProductType($childrenParent) === static::KURU_MASTER) {
                if ($idOnly) {
                    return $childrenParent->getId();
                } else {
                    return $childrenParent;
                }
            }
        }

        throw new LocalizedException(
            __("Simple product has no linked master product.")
        );
    }

    /**
     * Get parents of a child product
     *
     * @param ProductInterface $product
     * @param bool $idOnly
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     *
     * @return int[]|ProductInterface[]
     */
    public function getParents(ProductInterface $product, $idOnly = false)
    {
        $childrenParentIds = $this->configurableProductFactory->create()
            ->getParentIdsByChild($product);

        if ($idOnly) {
            return $childrenParentIds;
        }

        $parents = [];
        foreach ($childrenParentIds as $childrenParentId) {
            $parents[$childrenParentId] = $this->productRepository->getById($childrenParentId);
        }

        return $parents;
    }

    /**
     * Parses URL and uses PDP registry to determine
     * the current preselected configuration
     */
    public function getPreselectedConfigurationProduct()
    {
        /**
         * Cache this function
         */
        $preselectedProduct = $this->registry->registry('pdp_preselected_product');
        if ($preselectedProduct && $preselectedProduct->getId()) {
            return $preselectedProduct;
        }

        /**
         * Sanity checks
         */
        // Is product page
        if (!($this->request->getRouteName() === 'catalog' &&
            $this->request->getControllerName() === 'product')) {
            return null;
        }

        // Product in registry
        $currentProduct = $this->registry->registry('current_product');
        if (!($currentProduct && $currentProduct->getId())) {
            return null;
        }

        // Product is master
        if ($this->getKuruProductType($currentProduct) !== static::KURU_MASTER) {
            return null;
        }

        /**
         * Actual detection
         */
        // TODO: AbstractType::getChildrenIds is deprecated, replace w/ linkManagement when available
        $configurationIds = $currentProduct->getTypeInstance()->getChildrenIds($currentProduct->getId())[0];

        $productCollection = $this->productCollectionFactory->create()->addAttributeToSelect(
            'sku'
        )->addAttributeToSelect(
            'special_price'
        )->addAttributeToSelect(
            'price'
        )->addFieldToFilter(
            'entity_id',
            ['in' => $configurationIds]
        );

        $preselectParams = [
            'color'
        ];

        foreach ($preselectParams as $preselectParam) {
            $preselectedOptionId = $this->getOptionId(
                $preselectParam,
                $this->request->getParam($preselectParam)
            );

            if ($preselectedOptionId) {
                /**
                 * FYI, productCollection escapes forbidden chars and all in
                 * \Magento\Framework\DB\Adapter\Pdo\Mysql::prepareSqlCondition
                 */
                $productCollection->addAttributeToFilter(
                    $preselectParam,
                    ['eq', $preselectedOptionId]
                );
            } else {
                $productCollection->addAttributeToSelect(
                    $preselectParam
                );
            }
        }

        /**
         * Query and sort first by color, then by size
         *
         * 1. Can't sort immediately, because the size option code isn't known immediately)
         * (e.g. size, womens_size, socks_size)
         *
         * 2. Sorting by '*_value', because the entity_ids don't have the proper order always
         */
        $configurations = $productCollection->getItems();
        if (count($configurations) === 0) {
            return null;
        }

        $sizeCode = $this->getSizeCodeFromProduct(reset($configurations)) ?: 'size';
        $sortArray = [];
        foreach ($configurations as $configuration) {
            if ($configuration->getStatus() != Status::STATUS_ENABLED) {
                continue;
            }

            $color = $configuration->getData('color_value') ?: 'None';
            $size = $configuration->getData($sizeCode . '_value') ?: 'None';
            $sortArray[$color][$size] = $configuration;
        }

        /**
         * Sort, cache and return the smallest color -> smallest size -> configuration product
         */
        $firstColorProducts = reset($sortArray);
        if ($firstColorProducts === false) {
            return null;
        }

        $firstSizeProduct = reset($firstColorProducts);
        if ($firstSizeProduct === false) {
            return null;
        }

        $this->registry->register('pdp_preselected_product', $firstSizeProduct);

        return $firstSizeProduct;
    }

    /**
     * Return one of "size", "socks_size" or "womens_size"
     *
     * @param $product
     *
     * @return null|string
     */
    private function getSizeCodeFromProduct($product)
    {
        if ($product->getData('size')) {
            return 'size';
        }

        if ($product->getData('womens_size')) {
            return 'womens_size';
        }

        if ($product->getData('socks_size')) {
            return 'socks_size';
        }

        return null;
    }

    /**
     * Get id of option label
     *
     * @param $attributeCode
     * @param $optionLabel
     *
     * @return null|string
     */
    private function getOptionId($attributeCode, $optionLabel)
    {
        $options = $this->productAttributeRepository->get($attributeCode)->getOptions();
        foreach ($options as $option) {
            if ($optionLabel === $option->getLabel()) {
                return $option->getValue();
            }
        }

        return null;
    }
}