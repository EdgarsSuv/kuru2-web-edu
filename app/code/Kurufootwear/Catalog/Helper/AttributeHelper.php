<?php
/**
 * @vendor Kurufootwear
 * @module Kurufootwear_Catalog
 * @author Kristaps Stalidzāns <info@scandiweb.com>
 * @copyright Copyright (c) 2017 Scandiweb, Ltd (http://scandiweb.com)
 * @license http://opensource.org/licenses/afl-3.0.php Academic Free License (AFL 3.0)
 */

namespace Kurufootwear\Catalog\Helper;

use Magento\Catalog\Model\Product;
use Magento\Framework\App\Helper\Context;
use Magento\Catalog\Model\ProductFactory;
use Magento\Store\Model\StoreManagerInterface;
use Magento\Framework\App\Helper\AbstractHelper;
use Magento\Catalog\Api\CategoryRepositoryInterface;
use Magento\Catalog\Model\ResourceModel\Category\CollectionFactory as CategoryFactory;

class AttributeHelper extends AbstractHelper
{
    /**
     * @var ProductFactory
     */
    protected $attributeLoading;

    /**
     * @var CategoryRepositoryInterface
     */
    protected $categoryRepository;

    /**
     * @var StoreManagerInterface
     */
    protected $storeManager;

    /**
     * @var CategoryFactory
     */
    protected $collectionFactory;

    /**
     * AttributeHelper constructor.
     * @param Context $context
     * @param ProductFactory $attributeLoading
     * @param StoreManagerInterface $storeManager
     * @param CategoryRepositoryInterface $categoryRepository
     * @param CategoryFactory $collectionFactory
     */
    public function __construct(
        Context $context,
        ProductFactory $attributeLoading,
        StoreManagerInterface $storeManager,
        CategoryRepositoryInterface $categoryRepository,
        CategoryFactory $collectionFactory
    ){
        $this->storeManager = $storeManager;
        $this->attributeLoading = $attributeLoading;
        $this->collectionFactory = $collectionFactory;
        $this->categoryRepository = $categoryRepository;

        parent::__construct($context);
    }

    /**
     * @return Product
     */
    public function getProductFactory()
    {
        return $this->attributeLoading->create();
    }

    /**
     * Returns Option Label based on its id and attribute
     *
     * @param $optionId
     * @param $attribute
     * @throws \Magento\Framework\Exception\LocalizedException
     *
     * @return bool|string
     */
    public function getAttributeLabel($optionId , $attribute)
    {
        $productResource = $this->getProductFactory();
        $label = $productResource->getResource()->getAttribute($attribute);
        $attributeLabel = null;

        if ($label->usesSource()) {
            $attributeLabel = $label->getSource()->getOptionText($optionId);
        }

        return $attributeLabel;
    }


    /**
     * Returns Option Id based on its label and attribute
     *
     * @param string $optionLabel
     * @param $attribute
     * @throws \Magento\Framework\Exception\LocalizedException
     *
     * @return bool|string
     */
    public function getAttributeId(string $optionLabel, $attribute)
    {
        $productResource = $this->getProductFactory();
        $label = $productResource->getResource()->getAttribute($attribute);
        $attributeId = null;

        if ($label->usesSource()) {
            $attributeId = $label->getSource()->getOptionId($optionLabel);
        }

        return $attributeId;
    }

    /**
     * Returns category name by id
     *
     * @param $categoryId
     * @return string
     */
    public function getCategoryById($categoryId)
    {
        try {
            $category = $this->categoryRepository->get($categoryId, $this->storeManager->getStore()->getId());
            $categoryName = $category->getName();
        } catch (\Exception $e) {
            $categoryName = null;
        }

        return $categoryName;
    }

    /**
     * Returns attribute Options
     *
     * @param $attribute
     * @return \Magento\Eav\Api\Data\AttributeOptionInterface[]|null
     */
    public function getAllAttributeOptions($attribute)
    {
        $productResource = $this->getProductFactory();
        $attributeData = $productResource->getResource()->getAttribute($attribute);

        return $attributeData->getOptions();
    }

    /**
     * Returns category name based on parent cat id and name
     *
     * @param $categoryName
     * @param $parentId
     * @return null|string
     */
    public function getCategoryByName($categoryName, $parentId)
    {
        $categoryCollection = $this->collectionFactory->create();
        $categoryCollection->addFieldToFilter('name', $categoryName);

        if ($parentId) {
            $categoryCollection->addFieldToFilter('parent_id', $parentId);
        }

        $categoryCollection->getFirstItem();
        $categoryId = $categoryCollection->getAllIds();

        return $categoryId[0];
    }
}
