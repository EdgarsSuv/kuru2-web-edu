<?php
/**
 * @category    Kurufootwear
 * @package     Kurufootwear\Catalog
 * @author      Krisjanis Veinbahs <info@scandiweb.com>
 * @copyright   Copyright (c) 2017 Scandiweb, Inc (http://scandiweb.com)
 * @license     http://opensource.org/licenses/OSL-3.0 The Open Software License 3.0 (OSL-3.0)
 */

namespace Kurufootwear\Catalog\Helper;

use Magento\Framework\App\Request\Http as HttpRequest;
use Magento\Framework\View\Result\Page;
use Magento\Framework\Controller\Result\JsonFactory;
use Magento\Framework\Controller\Result\Json;
use Kurufootwear\Catalog\Block\Product\ProductList\Toolbar;
use Kurufootwear\Catalog\Block\Product\ListProduct;

class LoadMoreHelper
{
    const LOADMORE_KEY = 'loadmore';

    /**
     * @var HttpRequest
     */
    private $request;

    /**
     * @var JsonFactory
     */
    private $jsonFactory;

    /**
     * LoadMoreHelper constructor.
     *
     * @param HttpRequest $request
     * @param JsonFactory $jsonFactory
     */
    public function __construct(HttpRequest $request, JsonFactory $jsonFactory)
    {
        $this->request = $request;
        $this->jsonFactory = $jsonFactory;
    }

    /**
     * Checks if current request is ajax load more request
     *
     * @return bool
     */
    public function isLoadMoreRequest()
    {
        if ($this->request->getMethod() !== 'POST') {
            return false;
        }

        if ((int)$this->request->getParam(self::LOADMORE_KEY, 0) === 1) {
            return true;
        }

        return false;
    }

    /**
     * Converts page returned by controller action to JSON for Load More functionallity
     *
     * @param Page $page
     * @param string $productListBlockName
     * @return Json
     */
    public function convertPageToJsonResponse(Page $page, $productListBlockName)
    {
        /** @var ListProduct $productListBlock */
        $productListBlock = $page->getLayout()->getBlock($productListBlockName);
        /** @var Toolbar $productToolbarBlock */
        $productToolbarBlock = $page->getLayout()->getBlock('product_list_toolbar');

        $result = $this->jsonFactory->create();
        $result->setData([
            'response' => 'ok',
            'page' => $productToolbarBlock->getCurrentPage(),
            'toolbar' => '', // $productToolbarBlock->toHtml(),
            'limit' => (int)$productToolbarBlock->getLimit(),
            'products' => $productListBlock->toHtml(),
        ]);

        return $result;
    }
}
