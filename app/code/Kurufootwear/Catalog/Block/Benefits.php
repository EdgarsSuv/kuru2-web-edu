<?php
/**
 * @vendor Kurufootwear
 * @module Kurufootwear_Catalog
 * @author Krisjanis Veinbahs <krisjanisv@scandiweb.com>
 * @copyright Copyright (c) 2018 Scandiweb. All rights reserved.
 * @license http://opensource.org/licenses/afl-3.0.php Academic Free License (AFL 3.0)
 */

namespace Kurufootwear\Catalog\Block;

use Amasty\Shopby\Model\OptionSetting;
use Magento\Framework\Registry;
use Magento\Catalog\Model\Product;
use Magento\Framework\UrlInterface;
use Magento\Framework\View\Element\Template;
use Magento\Framework\View\Element\Template\Context;
use Amasty\Shopby\Api\Data\OptionSettingInterface;
use Amasty\Shopby\Model\ResourceModel\OptionSetting\CollectionFactory as OptionCollectionFactory;
use Magento\Framework\DataObjectFactory;

class Benefits extends Template
{
    /**
     * Benefits attribute code
     */
    const BENEFITS_CODE = 'benefits';

    /**
     * @var Registry
     */
    private $registry;

    /**
     * @var OptionCollectionFactory
     */
    private $optionCollectionFactory;

    /**
     * @var DataObjectFactory
     */
    private $dataObjectFactory;

    /**
     * @var Product
     */
    private $product = null;

    /**
     * @var null
     */
    private $store = null;

    /**
     * Benefits constructor.
     * @param Context $context
     * @param Registry $registry
     * @param OptionCollectionFactory $optionCollectionFactory
     * @param DataObjectFactory $dataObjectFactory
     * @param array $data
     */
    public function __construct(
        Context $context,
        Registry $registry,
        OptionCollectionFactory $optionCollectionFactory,
        DataObjectFactory $dataObjectFactory,
        array $data = []
    ) {
        $this->optionCollectionFactory = $optionCollectionFactory;
        $this->registry = $registry;
        $this->dataObjectFactory = $dataObjectFactory;

        parent::__construct(
            $context,
            $data
        );
    }

    /**
     * Get array of assigned products' benefits labels
     * and optionally settings if available
     * Structured as hashed array with option value ids as keys
     *
     * @return array
     */
    public function getBenefits()
    {
        $benefitsLabels = $this->getBenefitsLabels();
        $benefitsSettings = $this->getBenefitsSettings($benefitsLabels);

        $benefits = [];
        foreach ($benefitsLabels as $benefitsId => $benefitsLabel) {
            $benefitsLabelData = [ 'label' => $benefitsLabel ];

            if (key_exists($benefitsId, $benefitsSettings)) {
                $benefitsSettingData = $benefitsSettings[$benefitsId];
                $benefitsData = array_merge($benefitsLabelData, $benefitsSettingData);
            } else {
                $benefitsData = $benefitsLabelData;
            }

            $benefits[$benefitsId] = $this->dataObjectFactory->create()
                ->setData($benefitsData);
        }

        return $benefits;
    }

    /**
     * Get benefits values for current product and current store
     * Structured as hashed array 'option value id' => 'option value'.
     * (Received from product model)
     * @return array|\Magento\Framework\Api\AttributeInterface|null
     */
    public function getBenefitsLabels()
    {
        $product = $this->getProduct();
        $productBenefitsIds = explode(",", $product->getData(self::BENEFITS_CODE));
        $productBenefitsLabels = [];
        $allBenefitsData = $product->getResource()->getAttribute(self::BENEFITS_CODE)->getSource()->getAllOptions();

        if (!$productBenefitsIds ||
            !is_array($productBenefitsIds)) {
            return $productBenefitsLabels;
        }

        foreach ($allBenefitsData as $allBenefitsDatum) {
            if ($allBenefitsDatum['value'] !== "" &&
                in_array($allBenefitsDatum['value'], $productBenefitsIds)) {
                $productBenefitsLabels[$allBenefitsDatum['value']] = $allBenefitsDatum['label'];
            }
        }

        return $productBenefitsLabels;
    }

    /**
     * Get benefits settings array for current product and current store
     * or for admin store if current store doesn't have values
     * Structured as hashed array with option values as keys.
     * (Received from option settings collection)
     * @return array
     */
    public function getBenefitsSettings($benefitsLabels)
    {
        $store = $this->getStore();

        if (count($benefitsLabels) == 0) {
            return [];
        }

        $benefitsSettingsCollection = $this->optionCollectionFactory->create()
            ->addFieldToFilter(OptionSettingInterface::FILTER_CODE, 'attr_' . self::BENEFITS_CODE)
            ->addFieldToFilter(OptionSettingInterface::STORE_ID, ['in', [0, $store->getId()]])
            ->addFieldToFilter(OptionSettingInterface::VALUE, ['in', array_keys($benefitsLabels)]);

        $benefitsSettings = [];
        foreach ($benefitsSettingsCollection as $benefitsSettingsCollectionItem) {
            // Amasty has bad naming, benefits settings 'value' is actually benefits option id
            $benefitId = $benefitsSettingsCollectionItem->getValue();
            $benefitsSetting = $benefitsSettingsCollectionItem->getData();

            // Get absolute link of benefits image
            $imageFilename = $benefitsSettingsCollectionItem->getImage();
            $benefitsSetting['image_link'] = $this->getBenefitImageLink($imageFilename);

            $benefitsSettings[$benefitId] = $benefitsSetting;
        }

        return $benefitsSettings;
    }

    /**
     * Generate absolute benefits image link from just it's filename
     *
     * @param $imageFilename
     * @return string
     */
    public function getBenefitImageLink($imageFilename)
    {
        if (!$imageFilename) {
            return null;
        }

        $benefitImageAbsolutePath = $this->getBenefitImageAbsolutePath();

        return $benefitImageAbsolutePath . $imageFilename;
    }


    /**
     * Generate path to benefits images
     * (Supposing Amasty internal structure)
     *
     * @return string
     */
    public function getBenefitImageAbsolutePath()
    {
        $baseUrl = $this->_storeManager->getStore()->getBaseUrl();
        $relativePath = $this->getBenefitImageRelativePath();

        return $baseUrl . $relativePath;
    }

    /**
     * Generate path to benefits images
     * (Supposing Amasty internal structure)
     *
     * @return string
     */
    public function getBenefitImageRelativePath()
    {
        $mediaUrl = UrlInterface::URL_TYPE_MEDIA;
        $amastyPath = OptionSetting::IMAGES_DIR;

        return $mediaUrl . $amastyPath;
    }

    /**
     * Get current store
     *
     * @return \Magento\Store\Api\Data\StoreInterface|null
     */
    public function getStore()
    {
        if (!$this->store) {
            $this->store = $this->_storeManager->getStore();
        }

        return $this->store;
    }

    /**
     * Get current product
     *
     * @return Product
     */
    public function getProduct()
    {
        if (!$this->product) {
            $this->product = $this->registry->registry('product');
        }

        return $this->product;
    }
}