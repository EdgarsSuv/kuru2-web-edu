<?php
/**
 * @vendor Kurufootwear
 * @module Kurufootwear_Catalog
 * @author Kristaps Stalidzans <info@scandiweb.com>
 * @copyright Copyright (c) 2017 Scandiweb. All rights reserved.
 * @license http://opensource.org/licenses/afl-3.0.php Academic Free License (AFL 3.0)
 */
namespace Kurufootwear\Catalog\Block\Product\ProductList;

use Magento\Catalog\Api\ProductRepositoryInterface;
use Magento\Catalog\Block\Product\Context;
use Magento\Catalog\Model\Product\Visibility;
use Magento\Catalog\Model\ResourceModel\Product as ProductResource;
use Magento\Catalog\Model\Product;
use Magento\Catalog\Model\ResourceModel\Product\Collection;
use Magento\Checkout\Model\ResourceModel\Cart;
use Magento\Checkout\Model\Session;
use Magento\Framework\Module\Manager;
use Magento\Catalog\Model\ResourceModel\Product\CollectionFactory as ProductCollection;
use Magento\ConfigurableProduct\Model\Product\Type\Configurable;
use Kurufootwear\Catalog\Helper\AttributeHelper;
use Zend_Db_Expr;

class Related extends \Magento\Catalog\Block\Product\ProductList\Related
{
    /**
     * @var ProductRepositoryInterface
     */
    private $productRepository;

    /**
     * @var ProductCollection
     */
    private $productCollection;

    /**
     * @var AttributeHelper
     */
    private $attributeHelper;

    /**
     * @var ProductResource
     */
    private $productResource;

    /**
     * @var Collection
     */
    private $randomItemsCollection;

    /**
     * Related constructor.
     * @param Context $context
     * @param Cart $checkoutCart
     * @param Visibility $catalogProductVisibility
     * @param Session $checkoutSession
     * @param Manager $moduleManager
     * @param ProductRepositoryInterface $productRepository
     * @param ProductCollection $productCollection
     * @param AttributeHelper $attributeHelper
     * @param ProductResource $productResource
     * @param array $data
     */
    public function __construct(
        Context $context,
        Cart $checkoutCart,
        Visibility $catalogProductVisibility,
        Session $checkoutSession,
        Manager $moduleManager,
        ProductRepositoryInterface $productRepository,
        ProductCollection $productCollection,
        AttributeHelper $attributeHelper,
        ProductResource $productResource,
        array $data = []
    )
    {
        $this->productRepository = $productRepository;
        $this->productCollection = $productCollection;
        $this->attributeHelper = $attributeHelper;
        $this->productResource = $productResource;

        parent::__construct(
            $context,
            $checkoutCart,
            $catalogProductVisibility,
            $checkoutSession,
            $moduleManager,
            $data
        );
    }

    /**
     * Gets current product
     *
     * @return mixed
     */
    public function getCurrentProduct()
    {
        return $this->_coreRegistry->registry('product');
    }

    /**
     * Returns related product collection that is random
     *
     * @return Collection
     */
    public function getRandomRelatedProducts()
    {
        if ($this->randomItemsCollection) {
            return $this->randomItemsCollection;
        }

        $benefitsIds = $this->excludeBenefitsAttributes();

        $this->randomItemsCollection = $this->productCollection->create()
            ->addAttributeToSelect('*')
            ->addAttributeToFilter('type_id', ['eq' => Configurable::TYPE_CODE])
            ->addAttributeToFilter('parent_sku', ['neq' => $this->getCurrentProduct()->getSku()])
            ->addAttributeToFilter('sku', ['neq' => $this->getCurrentProduct()->getSku()])
            ->addAttributeToFilter('benefits', [$benefitsIds])
            ->addAttributeToFilter('product_duplicate', ['eq' => 1])
            ->addAttributeToFilter('status', ['eq' => 1])
            ->setOrder(new Zend_Db_Expr('RAND()'))
            ->setPageSize(7)
            ->setCurPage(1);

        $this->randomItemsCollection->addCategoriesFilter(['in' => [$this->getCurrentProduct()->getCategoryIds()[0]]]);
        $this->randomItemsCollection->load();

        return $this->randomItemsCollection;
    }

    /**
     * Returns needed Items ids for collection
     *
     * @return array
     */
    public function excludeBenefitsAttributes()
    {
        $attributeData = $this->attributeHelper->getAllAttributeOptions('benefits');
        $excludedAttributes = [
            'Plantar fasciitis',
            'Arch support',
            'Nursing/healthcare'
        ];
        $ids = [];

        foreach ($attributeData as $attributeItem) {
            $key = array_search($attributeItem->getLabel(), $excludedAttributes);
            if ($key === false && $attributeItem->getValue()) {
                array_push($ids, ['finset' => $attributeItem->getValue()]);
            }
        }

        return $ids;
    }

    /**
     * Returns modified collection
     *
     * @return Collection
     */
    public function getItems()
    {
        return $this->_itemCollection->count() ? $this->_itemCollection : $this->getRandomRelatedProducts();
    }

    /**
     * Returns product id if prent sku is present then gets its id for yotpo
     *
     * @param Product $item
     *
     * @return int
     */
    public function getProductId(Product $item)
    {
        $productId = $item->getId();
        $productParentSku = $item->getData('parent_sku');

        if (is_null($productParentSku)) {
            return $productId;
        }

        return $this->productResource->getIdBySku($productParentSku) ?: $productId;
    }

    /**
     * Returns product color
     *
     * @param Product $item
     *
     * @return mixed
     */
    public function getProductColor(Product $item)
    {
        return $item->getData('fecolor') ?: $item->getData('product_color_data') ?: null;
    }

    /**
     * @param \Magento\Catalog\Model\Product $product
     * @param array $additional
     * @return string
     */
    public function getProductUrl($product, $additional = [])
    {
        if ($this->hasProductUrl($product)) {
            if (!isset($additional['_escape'])) {
                $additional['_escape'] = true;
            }

            $parentSku = $product->getData('parent_sku');
            $productDuplicate = $product->getData('product_duplicate');
            $productColorData = $product->getData('product_color_data');
            $url = $product->getUrlModel()->getUrl($product, $additional);

            /**
             * Checks if parent product is present with color data un duplicate check
             */
            if ($parentSku && $productColorData && !is_null($productDuplicate)) {
                try {
                    $product = $this->productRepository->get($parentSku);
                    $url = $product->getUrlModel()->getUrl($product, $additional) . '?color=' . $productColorData;
                } catch (\Exception $e) {}
            }

            return $url;
        }

        return '#';
    }

    /**
     * This is needed to take the correct templates
     *
     * @return string
     */
    protected function _toHtml()
    {
        $this->setModuleName($this->extractModuleName('Magento\Catalog\Block\Product\ProductList\Related'));

        return parent::_toHtml();
    }
}
