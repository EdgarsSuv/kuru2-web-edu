<?php
/**
 * @vendor Kurufootwear
 * @module Kurufootwear_Catalog
 * @author Krisjanis Veinbahs <info@scandiweb.com>
 * @copyright Copyright (c) 2018 Scandiweb. All rights reserved.
 * @license http://opensource.org/licenses/afl-3.0.php Academic Free License (AFL 3.0)
 */
namespace Kurufootwear\Catalog\Block\Product\ProductList;

use DateInterval;
use DateTime;
use Kurufootwear\ConfigurableProduct\Model\ResourceModel\Product\Type\ConfigurableFactory as ProductTypeConfigurableFactory;
use Kurufootwear\ConfigurableProduct\Model\ResourceModel\Product\Type\Configurable as ProductTypeConfigurable;
use Kurufootwear\Reports\Model\ResourceModel\Product\Sold\CollectionFactory as ReportCollectionFactory;
use Magento\Catalog\Model\Product\ProductList\Toolbar as ToolbarModel;
use Magento\Catalog\Helper\Product\ProductList;
use Magento\Catalog\Model\Config;
use Magento\Catalog\Model\Session;
use Magento\ConfigurableProduct\Model\Product\Type\Configurable;
use Magento\Framework\Data\Collection;
use Magento\Framework\Data\Helper\PostHelper;
use Magento\Framework\Url\EncoderInterface;
use Magento\Framework\View\Element\Template\Context;

class Toolbar extends \Magento\Catalog\Block\Product\ProductList\Toolbar
{
    /**
     * Toolbar-specific constants
     */
    const ASC = 'asc';
    const DESC = 'desc';
    const PRICE = 'price';
    const CREATED_AT = 'created_at';
    const POSITION = 'position';
    const NAME = 'name';

    /**
     * Bestseller filter constants
     */
    const BESTSELLER_PERIOD_CONFIG_PATH = 'catalog/frontend/bestseller_period';
    const BESTSELLER_FILTER_NAME = 'bestsellers';

    /**
     * @var ReportCollectionFactory
     */
    protected $reportCollectionFactory;

    /**
     * @var ProductTypeConfigurableFactory
     */
    protected $productTypeConfigurableFactory;

    /**
     * Toolbar constructor.
     *
     * @param Collection $collection
     * @param Context $context
     * @param Session $catalogSession
     * @param Config $catalogConfig
     * @param ToolbarModel $toolbarModel
     * @param EncoderInterface $urlEncoder
     * @param ProductList $productListHelper
     * @param PostHelper $postDataHelper
     * @param ReportCollectionFactory $reportCollectionFactory
     * @param array $data
     */
    public function __construct(
        Collection $collection,
        Context $context,
        Session $catalogSession,
        Config $catalogConfig,
        ToolbarModel $toolbarModel,
        EncoderInterface $urlEncoder,
        ProductList $productListHelper,
        PostHelper $postDataHelper,
        ReportCollectionFactory $reportCollectionFactory,
        ProductTypeConfigurableFactory $productTypeConfigurableFactory,
        array $data = []
    )
    {
        $this->reportCollectionFactory = $reportCollectionFactory;
        $this->productTypeConfigurableFactory = $productTypeConfigurableFactory;

        parent::__construct(
            $context,
            $catalogSession,
            $catalogConfig,
            $toolbarModel,
            $urlEncoder,
            $productListHelper,
            $postDataHelper,
            $data
        );
    }

    /**
     * Set collection to pager
     *
     * @param Collection $collection
     * @return $this
     */
    public function setCollection($collection)
    {
        $this->_collection = $collection;

        $this->_collection->setCurPage($this->getCurrentPage());

        $limit = (int)$this->getLimit();

        if ($limit) {
            $this->_collection->setPageSize($limit);
        }

        if ($currentOrder = $this->getCurrentOrder()) {
            switch ($currentOrder) {
                case 'price-high':
                    $this->_collection
                        ->setOrder(self::PRICE, self::DESC);
                    break;
                case 'price-low':
                    $this->_collection
                        ->setOrder(self::PRICE, self::ASC);
                    break;
                case 'newest':
                    $this->_collection
                        ->setOrder(self::CREATED_AT, self::DESC);
                    break;
                case self::BESTSELLER_FILTER_NAME:
                    $this->_collection = $this->sortByOrderedQty($this->_collection);
                    break;
                default:
                    $this->_collection
                        ->setOrder(self::POSITION, self::ASC);
                    break;
            }
        }

        return $this;
    }

    /**
     * Sort collection of products by ordered quantity
     *
     * @param int|null $orderPeriod
     * @param Collection $productCollection
     *
     * @return Collection|array
     */
    public function sortByOrderedQty($productCollection, $orderPeriod = null)
    {
        /**
         * Generate time period for bestsellers
         */
        if (is_null($orderPeriod)) {
            $orderPeriod = $this->_scopeConfig->getValue(self::BESTSELLER_PERIOD_CONFIG_PATH);
        }

        $dateFormat = 'Y-m-d H:i:s';
        $orderPeriod = new DateInterval('P' . (string)$orderPeriod . 'D');
        $dayPeriod = new DateInterval('P1D');
        $today = new DateTime();
        $orderPeriodEnd = $today->add($dayPeriod)->format($dateFormat); // Now
        $orderPeriodStart = $today->sub($orderPeriod)->format($dateFormat); // Now minus the period time

        /**
         * Get a list of simple product ids and duplicate children ids
         *
         * Duplicate products will never show up in order reports, only masters and simples
         */
        $productCollectionIds = $productCollection->getAllIds();
        $parentChildIdsMap = $this->productTypeConfigurableFactory->create()->mapParentChildrenIds($productCollectionIds);
        $simpleConfigurationProductIds = array_values($parentChildIdsMap[ProductTypeConfigurable::PARENT_KEYS]);
        $simpleProductIds = array_merge($productCollectionIds, ...$simpleConfigurationProductIds);

        /**
         * Get report of ordered quantities
         */
        $productReportCollection = $this->reportCollectionFactory->create()
            ->addOrderedQty($orderPeriodStart, $orderPeriodEnd)
            ->addAttributeToFilter('product_id', ['in' => $simpleProductIds]);

        /**
         * Load report collection & set product ordered quantity as position attribute
         */
        $productCollection->setDataToAll('position', 0);
        foreach ($productReportCollection as $productReport) {
            /**
             * Get simple or match simple configuration to duplicate
             */
            $orderedProductId = $productReport->getOrderItemsProductId();
            $childrenIds = $parentChildIdsMap[ProductTypeConfigurable::CHILD_KEYS];
            if (key_exists($orderedProductId, $childrenIds)) {
                // Simple configuration of a duplicate, reassign to duplicate
                $duplicateProductId = $childrenIds[$orderedProductId][0];
                $reportedProduct = $productCollection->getItemById($duplicateProductId);
            } else {
                // Simple product order, assign to simple
                $reportedProduct = $productCollection->getItemById($orderedProductId);
            }

            if (!$reportedProduct) {
                continue;
            }

            /**
             * Set position as ordered quantity
             *
             * Add incrementally, because simple configurations of duplicates
             * will show up in multiple reports, count them all to the one duplicate
             */
            $currentOrderQuantity = $reportedProduct->getPosition();
            $addedOrderQuantity = (int) $productReport->getOrderedQty();
            $reportedProduct->setPosition($currentOrderQuantity + $addedOrderQuantity);
        }
        
        /**
         * Sort collection products by position attribute (ordered quantities)
         *
         * Can't do it with a method on the collection, because collection
         * methods just append an "ORDER BY" SQL clause to their internal
         * select SQL clause, which works on load.
         * But our sorting method has already loaded the products to get reports.
         */
        $productCollectionReflection = new \ReflectionObject($productCollection);
        $productsProperty = $productCollectionReflection->getProperty('_items');
        $productsProperty->setAccessible(true);
        $products = $productsProperty->getValue($productCollection);
        uasort($products, function($productA, $productB) {
            $ascending = true;
            $productPositionA = $productA->getPosition();
            $productPositionB = $productB->getPosition();
            $productAConfigurable = $productA->getTypeId() === Configurable::TYPE_CODE;
            $productBConfigurable = $productB->getTypeId() === Configurable::TYPE_CODE;

            // Simple products should always be at the bottom
            if (!$productAConfigurable && $productBConfigurable) {
                return 1;
            } else if ($productAConfigurable && !$productBConfigurable) {
                return -1;
            }

            if ($productPositionA == $productPositionB) {
                return 0;
            } else if ($productPositionA < $productPositionB) {
                return $ascending ? 1 : -1;
            } else {
                return $ascending ? -1 : 1;
            }
        });
        $productsProperty->setValue($productCollection, $products);

        return $productCollection;
    }

    /**
     * Return if toolbar product collection is generated
     *
     * @return bool
     */
    public function collectionAvailable()
    {
        return !is_null($this->getCollection());
    }


    /**
     * This is needed to take the correct templates
     *
     * @return string
     */
    protected function _toHtml()
    {
        $this->setModuleName($this->extractModuleName('Magento\Catalog\Block\Product\ProductList\Toolbar'));

        return parent::_toHtml();
    }
}
