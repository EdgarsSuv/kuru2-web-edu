<?php
/**
 * @vendor Kurufootwear
 * @module Kurufootwear_Catalog
 * @author Kristaps Stalidzans <info@scandiweb.com>
 * @copyright Copyright (c) 2017 Scandiweb. All rights reserved.
 * @license http://opensource.org/licenses/afl-3.0.php Academic Free License (AFL 3.0)
 */
namespace Kurufootwear\Catalog\Block\Product\View;

use Magento\Catalog\Api\Data\ProductInterface;
use Magento\Catalog\Model\Product;
use Magento\Catalog\Model\Product\Attribute\Source\Status;
use Magento\Framework\Exception\NoSuchEntityException;
use Magento\Framework\Registry;
use Magento\Framework\View\Element\Template;
use Magento\Catalog\Api\ProductRepositoryInterface;

/**
 *  Hax for product width
 *
 * @TODO Remove HAX when products ir merged
 */
class Width extends Template
{
    const WIDE_ATTRIBUTE = 'width_wide_sku';
    const MEDIUM_ATTRIBUTE = 'width_medium_sku';

    /**
     * @var Product
     */
    protected $_product = null;

    /**
     * Core registry
     *
     * @var Registry
     */
    protected $_coreRegistry = null;

    /**
     * @var ProductRepositoryInterface
     */
    private $productRepository;

    /**
     * @param Template\Context $context
     * @param Registry $registry
     * @param ProductRepositoryInterface $productRepository
     * @param array $data
     */
    public function __construct(
        Template\Context $context,
        Registry $registry,
        ProductRepositoryInterface $productRepository,
        array $data = []
    ) {
        $this->_coreRegistry = $registry;
        $this->productRepository = $productRepository;
        parent::__construct($context, $data);
    }

    /**
     * @return Product
     */
    public function getProduct()
    {
        if (!$this->_product) {
            $this->_product = $this->_coreRegistry->registry('product');
        }

        return $this->_product;
    }

    /**
     * Checks if wide or normal product is present
     *
     * @return ProductInterface|null
     */
    public function getParentProduct()
    {
        $productWideSku = $this->getProduct()->getResource()
            ->getAttribute(self::WIDE_ATTRIBUTE)
            ->getFrontend()
            ->getValue($this->getProduct());

        $productMediumSku = $this->getProduct()->getResource()
            ->getAttribute(self::MEDIUM_ATTRIBUTE)
            ->getFrontend()
            ->getValue($this->getProduct());

        try {
            if ($productWideSku) {
                $product = $this->productRepository->get($productWideSku);
            } else {
                $product = $this->productRepository->get($productMediumSku);
            }

            $product = $product->getStatus() == Status::STATUS_DISABLED ? null : $product;
        } catch (NoSuchEntityException $e) {
            $product = null;
        }

        return $product;
    }

    /**
     * Returns current product width
     *
     * @return mixed
     */
    public function getCurrentProductWidth()
    {
        $width = $this->getProduct()->getResource()
            ->getAttribute('width')
            ->getFrontend()
            ->getValue($this->getProduct());

        return $width;
    }

    /**
     * Gets Parent product width
     *
     * @return mixed
     */
    public function getParentProductWidth()
    {
        $product = $this->getParentProduct();

        if ($product) {
            $width = $product->getResource()
                ->getAttribute('width')
                ->getFrontend()
                ->getValue($product);

            return $width;
        }
    }

    /**
     * Returns option array for select
     *
     * @return array
     */
    public function getOptionArray()
    {
        $data = [
            $this->getProduct()->getProductUrl() => $this->getCurrentProductWidth(),
            $this->getParentProduct() ? $this->getParentProduct()->getProductUrl() : '' =>
                $this->getParentProduct() ? $this->getParentProductWidth() : 'Wide not available'
        ];
        asort($data);

        return $data;
    }

    /**
     * Check if there is no parent product URL
     *
     * @param $optionArray
     *
     * @return bool
     */
    public function checkForNoUrl($optionArray)
    {
        if (array_key_exists('', $optionArray)) {
            return true;
        }

        return false;
    }

    /**
     * Check if the parent product is saleable
     *
     * @return bool
     */
    public function checkIfIsSaleable()
    {
        $product = $this->getParentProduct();

        if (!isset($product)) {
            return false;
        }

        return $product->isSaleable();
    }
}
