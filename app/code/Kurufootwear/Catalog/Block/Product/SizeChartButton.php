<?php
/**
 * @category    Kurufootwear
 * @package     Kurufootwear\Catalog
 * @author      Aleksejs Baranovs <info@scandiweb.com>
 * @copyright   Copyright (c) 2018 Scandiweb, Inc (http://scandiweb.com)
 * @license     http://opensource.org/licenses/OSL-3.0 The Open Software License 3.0 (OSL-3.0)
 */

namespace Kurufootwear\Catalog\Block\Product;

use Scandiweb\SizeChart\Block\SizeChartButton as BaseSizeChartButton;

/**
 * Class SizeChartButton
 * @package Kurufootwear\Catalog\Block\Product
 */
class SizeChartButton extends BaseSizeChartButton
{
    /**
     * Prepare layout
     * Override to add product color data to the title
     *
     * @return $this
     */
    protected function _prepareLayout()
    {
        $this->getLayout()->createBlock('Magento\Catalog\Block\Breadcrumbs');
        $product = $this->getProduct();
        if (!$product) {
            return $this;
        }

        $title = $product->getMetaTitle();

        /* Add product color data to the title */
        $productColorData = $this->_request->getParam('color');

        if (isset($productColorData) && $title) {
            $this->pageConfig->getTitle()->set(sprintf('%s %s', $title, $productColorData));
        } elseif ($title) {
            $this->pageConfig->getTitle()->set($title);
        }

        $keyword = $product->getMetaKeyword();
        $currentCategory = $this->_coreRegistry->registry('current_category');
        if ($keyword) {
            $this->pageConfig->setKeywords($keyword);
        } elseif ($currentCategory) {
            $this->pageConfig->setKeywords($product->getName());
        }
        $description = $product->getMetaDescription();
        if ($description) {
            $this->pageConfig->setDescription($description);
        } else {
            $this->pageConfig->setDescription($this->string->substr($product->getDescription(), 0, 255));
        }
        if ($this->_productHelper->canUseCanonicalTag()) {
            $this->pageConfig->addRemotePageAsset(
                $product->getUrlModel()->getUrl($product, ['_ignore_category' => true]),
                'canonical',
                ['attributes' => ['rel' => 'canonical']]
            );
        }

        $pageMainTitle = $this->getLayout()->getBlock('page.main.title');
        if ($pageMainTitle) {
            $pageMainTitle->setPageTitle($product->getName());
        }

        return $this;
    }
}
