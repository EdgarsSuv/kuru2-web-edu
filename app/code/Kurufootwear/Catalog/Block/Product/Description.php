<?php
/**
 * @vendor Kurufootwear
 * @module Kurufootwear_Catalog
 * @author Kristaps Stalidzans <info@scandiweb.com>
 * @copyright Copyright (c) 2017 Scandiweb. All rights reserved.
 * @license http://opensource.org/licenses/afl-3.0.php Academic Free License (AFL 3.0)
 */
namespace Kurufootwear\Catalog\Block\Product;

use Magento\Catalog\Model\Product;
use Magento\Framework\Exception\LocalizedException;
use Magento\Framework\Registry;
use Magento\Framework\View\Element\Template;

class Description extends Template
{
    /**
     * Constatns for description block
     */
    const DESCRIPTION = 0;
    const BENIFIT = 1;
    const IDEALFOR = 3;

    /**
     * @var Registry
     */
    protected $registry;

    /**
     * @var Product
     */
    private $product;

    /**
     * Information constructor.
     * @param Template\Context $context
     * @param Registry $registry
     * @param array $data
     */
    public function __construct
    (
        Template\Context $context,
        Registry $registry,
        array $data
    ){
        $this->registry = $registry;

        parent::__construct($context, $data);
    }

    /**
     * Gets product
     *
     * @return Product|mixed
     * @throws LocalizedException
     */
    public function getProduct()
    {
        if (is_null($this->product)) {
            $this->product = $this->registry->registry('product');

            if (!$this->product->getId()) {
                throw new LocalizedException(__('Failed to initialize product'));
            }
        }

        return $this->product;
    }

    public function getDescription()
    {
        $description = $this->getProduct()->getDescription();

        return explode('<h2>', $description);
    }

    /**
     * Gets Product Description or Benifit on
     *
     * @param int $infoType
     * @return mixed
     */
    public function getProductData(int $infoType)
    {
        $productDescription = $this->getDescription();
        $result = null;

        try {
            if ($productDescription[$infoType]) {
                $result = $productDescription[$infoType];
            }
        } catch (\Exception $e) {}

        /**
         * Adds h2 mising tag on concat
         */
        if (!is_null($result) && ($infoType == self::BENIFIT || $infoType == self::IDEALFOR)) {
            $result = '<h2>' . $result;
        }

        return $result;
    }

    /**
     * Gets Product Name
     *
     * @return mixed
     */
    public function getProductName()
    {
        return $this->getProduct()->getName();
    }
}
