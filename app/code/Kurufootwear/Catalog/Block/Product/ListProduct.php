<?php
/**
 * @vendor Kurufootwear
 * @module Kurufootwear_Catalog
 * @author Krisjanis Veinbahs <krisjanisv@scandiweb.com>
 * @copyright Copyright (c) 2017 Scandiweb. All rights reserved.
 * @license http://opensource.org/licenses/afl-3.0.php Academic Free License (AFL 3.0)
 */

namespace Kurufootwear\Catalog\Block\Product;

use Kurufootwear\Catalog\Model\Product\Url;
use Magento\Catalog\Api\CategoryRepositoryInterface;
use Magento\Catalog\Model\Indexer\Category\Flat\State;
use Magento\Catalog\Model\Layer\Resolver;
use Magento\Catalog\Model\Product;
use Magento\Catalog\Pricing\Price\FinalPrice;
use Magento\Framework\Data\Helper\PostHelper;
use Magento\Catalog\Block\Product\Context;
use Magento\Framework\Pricing\Render;
use Magento\Framework\Url\Helper\Data;
use Magento\Catalog\Api\ProductRepositoryInterface;
use Kurufootwear\Revo\Block\Slider\Items;
use Magento\Eav\Model\Entity\Collection\AbstractCollection;
use Magento\Framework\Exception\NoSuchEntityException;
use Kurufootwear\Catalog\Helper\AttributeHelper;
use RuntimeException;

class ListProduct extends \Magento\Catalog\Block\Product\ListProduct
{
    const WIDE = 'Wide';
    const MEDIUM = 'Medium';

    /**
     * @var ProductRepositoryInterface
     */
    private $productRepository;

    /**
     * @var Items
     */
    public $items;

    /**
     * @var AttributeHelper
     */
    public $attributeHelper;

    /**
     * @var State
     */
    private $flatStatus;

    /**
     * @var Url
     */
    private $url;

    /**
     * ListProduct constructor.
     *
     * @param Context $context
     * @param PostHelper $postDataHelper
     * @param Resolver $layerResolver
     * @param CategoryRepositoryInterface $categoryRepository
     * @param Data $urlHelper
     * @param ProductRepositoryInterface $productRepository
     * @param Items $items
     * @param AttributeHelper $attributeHelper
     * @param State $flatStatus
     * @param Url $url
     * @param array $data
     */
    public function __construct(
        Context $context,
        PostHelper $postDataHelper,
        Resolver $layerResolver,
        CategoryRepositoryInterface $categoryRepository,
        Data $urlHelper,
        ProductRepositoryInterface $productRepository,
        Items $items,
        AttributeHelper $attributeHelper,
        State $flatStatus,
        Url $url,
        array $data = []
    )
    {
        $this->productRepository = $productRepository;
        $this->items = $items;
        $this->attributeHelper = $attributeHelper;
        $this->flatStatus = $flatStatus;
        $this->url = $url;

        parent::__construct(
            $context,
            $postDataHelper,
            $layerResolver,
            $categoryRepository,
            $urlHelper,
            $data
        );
    }

    /**
     * Retrieve loaded category collection
     *
     * @return AbstractCollection
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    protected function _getProductCollection()
    {

        if ($this->_productCollection === null) {
            $layer = $this->getLayer();
            /* @var $layer \Magento\Catalog\Model\Layer */
            if ($this->getShowRootCategory()) {
                $this->setCategoryId($this->_storeManager->getStore()->getRootCategoryId());
            }

            // if this is a product view page
            if ($this->_coreRegistry->registry('product')) {
                // get collection of categories this product is associated with
                $categories = $this->_coreRegistry->registry('product')
                    ->getCategoryCollection()->setPage(1, 1)
                    ->load();
                // if the product is associated with any category
                if ($categories->count()) {
                    // show products from this category
                    $this->setCategoryId(current($categories->getIterator()));
                }
            }

            $origCategory = null;
            if ($this->getCategoryId()) {
                try {
                    $category = $this->categoryRepository->get($this->getCategoryId());
                } catch (NoSuchEntityException $e) {
                    $category = null;
                }

                if ($category) {
                    $origCategory = $layer->getCurrentCategory();
                    $layer->setCurrentCategory($category);
                }
            }
            $this->_productCollection = $layer->getProductCollection();

            /**
             * @HAX https://youtu.be/fVz1VVWcF5U
             * @bug https://github.com/magento/magento2/issues/9676
             * Temp workaround for magento 2 "feature" that shows child category products
             * @TODO Fix later stage
             */
            if ($this->_productCollection && $layer->getCurrentCategory()->getLevel() == 2) {
                $optionId = $this->attributeHelper->getAttributeId(self::WIDE,'filterable_width');
                $this->_productCollection->addAttributeToFilter(
                    'filterable_width',
                    [
                        ['neq' => $optionId],
                        ['null' => true]
                    ],
                    'left'
                );
            }

            /**
             * Workaround fix for parent category product to be shown parent category.
             * This issue appears when catalog flat tables are enabled and by generating an
             * index it will generate product position with value 10001 for products in its parent category.
             * For example we have product A added in category Mens Wide shoes and not selected for parent category Mens Shoes
             * it will appear also in Mens shoes category with postion value 10001.
             * This query fix is a workaround for Magento 2 feature.
             */
            if ($this->flatStatus->isFlatEnabled() && $layer->getCurrentCategory()->getLevel() == 2) {
                $this->_productCollection->getSelect()
                    ->where('`cat_index`.`position` != ?', 10001);
            }

            $this->prepareSortableFieldsByCategory($layer->getCurrentCategory());

            if ($origCategory) {
                $layer->setCurrentCategory($origCategory);
            }
        }

        return $this->_productCollection;
    }

    /**
     * @param $item
     * @return mixed
     */
    public function getItemColor($item)
    {
        return $this->items->getProductColor($item);
    }

    /**
     * Returns product id if prent sku is present then gets its id for yotpo
     *
     * @param $item
     * @return int|null
     */
    public function getProductId($item)
    {
        $productId = $item->getId();
        $productParentSku = $item->getData('parent_sku');

        if ($productParentSku != null) {
            try {
                $productId = $this->productRepository->get($productParentSku)->getId();
            } catch (\Exception $e) {

            }
        }

        return $productId;
    }

    /**
     * Overridden to not show old price in category
     * Only display the new and/or special price
     *
     * @param Product $product
     *
     * @throws RuntimeException
     * @return string
     */
    public function getProductPrice(Product $product)
    {
        $priceRender = $this->getPriceRender();

        $price = '';
        if ($priceRender) {
            $price = $priceRender->render(
                FinalPrice::PRICE_CODE,
                $product,
                [
                    'label' => null,
                    'include_container' => true,
                    'display_minimal_price' => true,
                    'zone' => Render::ZONE_ITEM_LIST,
                    'list_category_page' => true,
                    'only_new_price' => true
                ]
            );
        }

        return $price;
    }

    /**
     * This is needed to take the correct templates
     *
     * @return string
     */
    protected function _toHtml()
    {
        $this->setModuleName($this->extractModuleName('Magento\Catalog\Block\Product\ListProduct'));

        return parent::_toHtml();
    }

    /**
     * Get product size from URL (used for filters)
     *
     * @return array|bool
     */
    public function getProductSizeFromUrl()
    {
        $womensSize = (float)$this->getRequest()->getParam('womens_size');
        $mensSize = (float)$this->getRequest()->getParam('size');

        $sizeValue = $womensSize ?: $mensSize ?: null;

        if (!$sizeValue) {
            return false;
        }

        $sizeLabel = $womensSize ? 'womens_size' : ($mensSize ? 'size' : null);

        if ($sizeLabel) {
            return [
                $sizeLabel => $sizeValue
            ];
        }

        return false;
    }

    /**
     * Get product url
     *
     * @param Product $product
     * @param array $additional
     *
     * @return string
     */
    public function getProductUrl($product, $additional = [])
    {
        $productSize = $this->getProductSizeFromUrl();
        $returnUrl = $productSize ? $this->url->getProductUrl($product, null, $productSize) : $this->url->getProductUrl($product);

        if ($returnUrl) {
            return $returnUrl;
        }

        return '#';
    }
}
