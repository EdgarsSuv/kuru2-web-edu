<?php
/**
 * @vendor Kurufootwear
 * @module Kurufootwear_Catalog
 * @author Kristaps Stalidzans <info@scandiweb.com>
 * @copyright Copyright (c) 2017 Scandiweb. All rights reserved.
 * @license http://opensource.org/licenses/afl-3.0.php Academic Free License (AFL 3.0)
 */
namespace Kurufootwear\Catalog\Block;

use Magento\Catalog\Api\CategoryRepositoryInterface;
use Magento\Catalog\Api\Data\CategoryInterface;
use Magento\Catalog\Helper\Category;
use Magento\Catalog\Model\Indexer\Category\Flat\State;
use Magento\Catalog\Model\ResourceModel\Category\CollectionFactory;
use Magento\Framework\Registry;
use Magento\Framework\View\Element\Template;
use Magento\Framework\View\Element\Template\Context;
use Magento\Store\Model\StoreManagerInterface;

class CategoryList extends Template
{
    const INCLUDED_IN_MENU = 'include_in_menu';
    const ASC = 'asc';
    const POSITION = 'position';

    /**
     * @var Registry
     */
    private $registry;

    /**
     * @var State
     */
    private $categoryFlatState;

    /**
     * @var Category
     */
    private $categoryHelper;

    /**
     * @var CategoryRepositoryInterface
     */
    private $categoryRepository;

    /**
     * @var null
     */
    private $currentCategoryId = null;

    /**
     * Sets Prent category
     * @var null
     */
    private $parentCategory = null;

    /**
     * @var CollectionFactory
     */
    private $categoryCollection;
    /**
     * @var StoreManagerInterface
     */
    private $storeManager;

    /**
     * CategoryList constructor.
     * @param Context $context
     * @param Registry $registry
     * @param Category $categoryHelper
     * @param State $categoryFlatState
     * @param CollectionFactory $categoryCollection
     * @param CategoryRepositoryInterface $categoryRepository
     * @param array $data
     */
    public function __construct(
        Template\Context $context,
        Registry $registry,
        Category $categoryHelper,
        State $categoryFlatState,
        CollectionFactory $categoryCollection,
        CategoryRepositoryInterface $categoryRepository,
        array $data = []
    )
    {
        $this->registry = $registry;
        $this->categoryHelper = $categoryHelper;
        $this->categoryFlatState = $categoryFlatState;
        $this->categoryCollection = $categoryCollection;
        $this->categoryRepository = $categoryRepository;
        $this->storeManager = $context->getStoreManager();
        $this->currentCategoryId = $this->getCurrentCategoryId();
        $this->parentCategory = false;

        parent::__construct(
            $context,
            $data
        );
    }

    /**
     * Return categories helper
     */
    public function getCategoryHelper()
    {
        return $this->categoryHelper;
    }

    /**
     * Gets current product
     *
     * @return mixed
     */
    public function getCurrentCategory()
    {
        return $this->registry->registry('current_category');
    }

    /**
     * Gets current product
     *
     * @return mixed
     */
    public function getCurrentCategoryId()
    {
        return $this->getCurrentCategory()->getId();
    }

    /**
     * Returns selected category
     *
     * @param $id
     * @return string
     */
    public function getSelectedCategory($id)
    {
        if ($id == $this->getCurrentCategoryId()) {
            return 'am_shopby_link_selected';
        } else {
            return null;
        }
    }

    /**
     *  Adds check if level cat
     */
    public function getParentCategory()
    {
        try {
            $currentCategory = $this->categoryRepository->get($this->currentCategoryId);

            if ($currentCategory->getLevel() == 3) {
                $this->parentCategory = true;
                $this->currentCategoryId = $currentCategory->getParentId();
            }
        } catch (\Exception $e) {}
    }

    /**
     * Returns parent category Check
     * @return null
     */
    public function getParentCategoryCheck()
    {
        return $this->parentCategory;
    }

    /**
     * Returns Shop All category
     *
     * @return CategoryInterface
     */
    public function getShopAllLink()
    {
        try {
            $storeId = $this->storeManager->getStore()->getId();
            $parentId = $this->getCurrentCategory()->getParentId();
            $parentCategory = $this->categoryRepository->get($parentId, $storeId);
            $url = $parentCategory->getUrl();

            return $url;
        } catch (\Exception $e) {}

        return null;
    }

    /**
     * Returns categories included in sidebar
     *
     * @return CategoryInterface|mixed
     */
    public function getCurrentCategoriesCollection()
    {
        $this->getParentCategory();

        $currentCategories = $this->categoryCollection->create()
            ->addAttributeToSelect('*')
            ->addAttributeToFilter(self::INCLUDED_IN_MENU, '1')
            ->addAttributeToFilter('parent_id', $this->currentCategoryId)
            ->setOrder(self::POSITION, self::ASC);
        $currentCategories = $currentCategories->getItems();

        return $currentCategories;
    }
}
