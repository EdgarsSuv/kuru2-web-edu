<?php
/**
 * @category  Kurufootwear
 * @package   Kurufootwear\Catalog
 * @author    Viesturs Ruzans <viestursr@scandiweb.com>
 * @copyright Copyright (c) 2017 Scandiweb, Inc (http://scandiweb.com)
 * @license   http://opensource.org/licenses/OSL-3.0 The Open Software License 3.0 (OSL-3.0)
 */

namespace Kurufootwear\Catalog\Plugin;

use Magento\Catalog\Model\Product;
use Magento\CatalogUrlRewrite\Model\ProductUrlPathGenerator;

class ProductUrlPathGeneratorPlugin
{
    /**
     * Appends product SKU to the url path.
     *
     * @param ProductUrlPathGenerator $urlPathGenerator
     * @param callable $proceed
     * @param Product $product
     * @param null $category
     * @return string
     */
    public function aroundGetUrlPath(
        ProductUrlPathGenerator $urlPathGenerator,
        callable $proceed,
        Product $product,
        $category = null
    ) {
        return sprintf('%s-%s', $proceed($product, $category), $product->formatUrlKey($product->getSku()));
    }
}
