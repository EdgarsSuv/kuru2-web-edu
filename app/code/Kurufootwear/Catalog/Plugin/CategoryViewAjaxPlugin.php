<?php
/**
 * @category    Kurufootwear
 * @package     Kurufootwear\Catalog
 * @author      Krisjanis Veinbahs <info@scandiweb.com>
 * @copyright   Copyright (c) 2017 Scandiweb, Inc (http://scandiweb.com)
 * @license     http://opensource.org/licenses/OSL-3.0 The Open Software License 3.0 (OSL-3.0)
 */

namespace Kurufootwear\Catalog\Plugin;

use Magento\Catalog\Controller\Category\View as CategoryView;
use Magento\Framework\View\Result\Page;
use Kurufootwear\Catalog\Helper\LoadMoreHelper;

class CategoryViewAjaxPlugin
{
    /**
     * @var LoadMoreHelper
     */
    private $loadMoreHelper;

    /**
     * CategoryViewAjaxPlugin constructor.
     *
     * @param LoadMoreHelper $loadMoreHelper
     */
    public function __construct(LoadMoreHelper $loadMoreHelper)
    {
        $this->loadMoreHelper = $loadMoreHelper;
    }

    /**
     * Plugin converts and returns only product block html if it's load more button request
     *
     * @param CategoryView $subject
     * @param Page $result
     * @return mixed
     */
    public function afterExecute(CategoryView $subject, $result)
    {
        if (!$this->loadMoreHelper->isLoadMoreRequest()) {
            return $result;
        }

        return $this->loadMoreHelper->convertPageToJsonResponse($result, 'category.products.list');
    }
}
