<?php
/**
 * @vendor Kurufootwear
 * @module Kurufootwea_Catalog
 * @author Krisjanis Veinbahs <krisjanisv@scandiweb.com>
 * @copyright Copyright (c) 2017 Scandiweb, Ltd (http://scandiweb.com)
 * @license http://opensource.org/licenses/afl-3.0.php Academic Free License (AFL 3.0)
 */

namespace Kurufootwear\Catalog\Plugin\Framework\Locale;

class Format
{
    public function afterGetPriceFormat($subject, $result) {
        $result['precision'] = 0;
        $result['requiredPrecision'] = 0;

        return $result;
    }
}
