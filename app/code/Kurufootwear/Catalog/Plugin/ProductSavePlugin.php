<?php
/**
 * @vendor      Kurufootwear
 * @module      Kurufootwear_Catalog
 * @author      Krisjanis Veinbahs <krisjanisv@scandiweb.com>
 * @copyright   Copyright (c) 2018 Scandiweb, Inc (http://scandiweb.com)
 * @license     http://opensource.org/licenses/OSL-3.0 The Open Software License 3.0 (OSL-3.0)
 */
namespace Kurufootwear\Catalog\Plugin;

use Exception;
use Magento\Catalog\Api\ProductRepositoryInterface;
use Magento\Catalog\Model\Product;
use Magento\Framework\Event\ManagerInterface as EventManagerInterface;
use Magento\Framework\Exception\AlreadyExistsException;
use Magento\Framework\Message\ManagerInterface;
use Psr\Log\LoggerInterface;

/**
 * Class ProductSavePlugin
 * @package Kurufootwear\Catalog\Plugin
 */
class ProductSavePlugin
{
    /**
     * @var ManagerInterface
     */
    protected $messageManager;

    /**
     * @var ProductRepositoryInterface
     */
    protected $productRepository;

    /**
     * @var LoggerInterface
     */
    protected $logger;

    /**
     * @var EventManagerInterface
     */
    protected $eventManager;

    /**
     * ProductSavePlugin constructor.
     *
     * @param ManagerInterface $messageManager
     * @param ProductRepositoryInterface $productRepository
     * @param LoggerInterface $logger
     * @param EventManagerInterface $eventManager
     */
    public function __construct(
        ManagerInterface $messageManager,
        ProductRepositoryInterface $productRepository,
        LoggerInterface $logger,
        EventManagerInterface $eventManager
    ) {
        $this->messageManager = $messageManager;
        $this->productRepository = $productRepository;
        $this->logger = $logger;
        $this->eventManager = $eventManager;
    }

    /**
     * Before saving product, check if it's a simple,
     * with quantity changing from 0 to non-zero.
     * If that's the case, set "In Stock".
     *
     * @param Product $subject
     * @param $result
     * @return Product
     */
    public function afterBeforeSave(Product $subject, $result)
    {
        try {
            // Manage stock only for simple products
            if ($subject->getTypeId() !== 'simple') {
                return $subject;
            }

            $oldStockData = $subject->getOrigData('quantity_and_stock_status');
            $newStockData = $subject->getData('quantity_and_stock_status');

            // For new products stock isn't populated yet and no changes must be done
            if (!$this->isValidStockData($oldStockData) ||
                $this->isValidStockData($newStockData)) {
                return $subject;
            }

            $oldQuantity = (int)$oldStockData['qty'];
            $oldStockStatus = (bool)$oldStockData['is_in_stock'];
            $newQuantity = (int)$newStockData['qty'];

            if (
                $oldQuantity == 0 &&
                $newQuantity > 0 &&
                $oldStockStatus == false
            ) {
                $newStockData['is_in_stock'] = true;
                $subject->setData('quantity_and_stock_status', $newStockData);
            }

            return $subject;
        } catch (Exception $exception) {
            /**
             * Return the subject as is even if the "In stock logic fails"
             * It only fails when it shouldn't even be running.
             *
             * E.g. When a simple product gets configurations added (becomes configurable)
             */
            
            return $subject;
        }
    }

    /**
     * See if keys necessary for stock logic are present in stock data array
     * @param $data
     * @return bool
     */
    public function isValidStockData($data)
    {
        return is_array($data) &&
            key_exists('qty', $data) &&
            key_exists('quantity_and_stock_status', $data);
    }

    /**
     * Old corrupted URL patch
     *
     * @param Product $subject
     * @param callable $proceed
     */
    public function aroundSave(Product $subject, callable $proceed)
    {
        try {
            /**
             * Always synchronize url_key and url_path
             *
             * They don't seem to be synchronised by default, but if url_path
             * causes collision errors, you can't fix it, because it's an internal property
             */
            if ($subject->dataHasChangedFor('url_key')) {
                $subject->setData('url_path', $subject->getData('url_key'));
            }

            return $proceed();
        } catch (AlreadyExistsException $alreadyExistsException) {
            // Create unique urlkey
            if (!$subject->dataHasChangedFor('url_key')) {
                $urlkey = $subject->getData('url_key');
                $appendage = substr(uniqid(true), 0, 4);

                $subject->setData('url_key', $urlkey . '-' . $appendage);
            }

            // Synchronize internal url_path variable
            $subject->setData('url_path', $subject->getData('url_key'));

            // Avoid creating redirects for old urls
            // Because url_path or url_key is colliding and the rewrites for old urls will also collide
            $subject->setData('save_rewrites_history', false);

            $result = $proceed();

            $this->messageManager->addSuccessMessage("Url data was corrupted, attempted fixing.");

            return $result;

        }
    }

    /**
     * A plugin for invalidating parent product cache if (this) child was changed
     *
     * @param Product $subject
     * @param $result
     */
    public function afterSave(Product $subject, $result)
    {
        if ($parentSku = $subject->getData('parent_sku')) {
            try {
                $parentProduct = $this->productRepository->get($parentSku);

                if ($parentProduct instanceof Product) {
                    $parentProduct->cleanModelCache();
                } else {
                    $this->logger->warning(sprintf(
                        __("Product with SKU '%s' is not an instance of the default Magento Product Model")
                    ));
                }

                $this->eventManager->dispatch('clean_cache_by_tags', ['object' => $parentProduct]);
            } catch (Exception $exception) {
                $this->messageManager->addExceptionMessage($exception, sprintf(
                    __("Product from parent_sku - '%s' is non-existent"),
                    $parentSku
                ));
            }
        }
    }
}
