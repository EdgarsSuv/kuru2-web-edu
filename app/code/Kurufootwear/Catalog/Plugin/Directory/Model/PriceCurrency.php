<?php
/**
 * @vendor Kurufootwear
 * @module Kurufootwea_Catalog
 * @author Krisjanis Veinbahs <krisjanisv@scandiweb.com>
 * @copyright Copyright (c) 2017 Scandiweb, Ltd (http://scandiweb.com)
 * @license http://opensource.org/licenses/afl-3.0.php Academic Free License (AFL 3.0)
 */

namespace Kurufootwear\Catalog\Plugin\Directory\Model;

use Kurufootwear\Catalog\Model\CheckoutConfigProvider;

/**
 * Class PriceCurrency
 * (Plugin for rounding all prices)
 * @package Kurufootwear\Catalog\Plugin\Directory\Model
 */
class PriceCurrency
{
    /**
     * @var CheckoutConfigProvider
     */
    protected $checkoutConfigProvider;

    /**
     * PriceCurrency constructor.
     * @param CheckoutConfigProvider $checkoutConfigProvider
     */
    public function __construct(
        CheckoutConfigProvider $checkoutConfigProvider
    )
    {
        $this->checkoutConfigProvider = $checkoutConfigProvider;
    }

    /**
     * @param $subject
     * @param $amount
     * @param null $scope
     * @param null $currency
     * @param $precision
     * @return array
     */
    public function beforeConvertAndRound(
        $subject,
        $amount,
        $scope = null,
        $currency = null,
        $precision = \Magento\Directory\Model\PriceCurrency::DEFAULT_PRECISION
    ) {
        if ($this->checkoutConfigProvider->showDecimalsInUrl()) {
            return [$amount, $scope, $currency, $precision];
        } else {
            return [$amount, $scope, $currency, 0];
        }
    }

    /**
     * @param $subject
     * @param $amount
     * @param bool $includeContainer
     * @param $precision
     * @param null $scope
     * @param null $currency
     * @return array
     */
    public function beforeFormat(
        $subject,
        $amount,
        $includeContainer = true,
        $precision = \Magento\Directory\Model\PriceCurrency::DEFAULT_PRECISION,
        $scope = null,
        $currency = null
    ) {
        if ($this->checkoutConfigProvider->showDecimalsInUrl()) {
            return [$amount, $includeContainer, $precision, $scope, $currency];
        } else {
            return [$amount, $includeContainer, 0, $scope, $currency];
        }
    }
}