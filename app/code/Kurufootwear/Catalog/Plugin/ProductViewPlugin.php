<?php
/**
 * @vendor Kurufootwear
 * @module Kurufootwear_Catalog
 * @author Aleksejs Baranovs <info@scandiweb.com>
 * @copyright Copyright (c) 2018 Scandiweb, Ltd (http://scandiweb.com)
 * @license http://opensource.org/licenses/afl-3.0.php Academic Free License (AFL 3.0)
 */

namespace Kurufootwear\Catalog\Plugin;

use Magento\Catalog\Helper\Product\View;
use Magento\Framework\Exception\NoSuchEntityException;
use Magento\Framework\View\Result\Page as ResultPage;
use Magento\Catalog\Model\Product;
use Magento\Eav\Model\AttributeSetRepository;

/**
 * Class ProductViewPlugin
 * @package Kurufootwear\Catalog\Plugin
 */
class ProductViewPlugin
{
    /**
     * @var AttributeSetRepository
     */
    private $attributeSetRepository;

    /**
     * ProductViewPlugin constructor.
     *
     * @param AttributeSetRepository $attributeSetRepository
     */
    public function __construct(
        AttributeSetRepository $attributeSetRepository
    ) {
        $this->attributeSetRepository = $attributeSetRepository;
    }

    /**
     * Add new layout handle for PDP
     *
     * @param View $subject
     * @param ResultPage $resultPage
     * @param Product $product
     * @param null $params
     *
     * @return array
     *
     * @throws NoSuchEntityException
     */
    public function beforeInitProductLayout(View $subject, ResultPage $resultPage, Product $product, $params = null)
    {
        if ($attributeSetName = $this->attributeSetRepository->get($product->getAttributeSetId())->getAttributeSetName()) {
            $resultPage->addPageLayoutHandles(
                ['attribute_set' => $this->formatAttributeSetName($attributeSetName)]
            );
        }

        // Add new layout handle according to product custom layout
        if ($customLayout = $product->getCustomLayout()) {
            $resultPage->addPageLayoutHandles(
                ['layout' => $customLayout]
            );
        }

        return [$resultPage, $product, $params];
    }

    /**
     * Change dashes and spaces from attribute set name to underscore
     *
     * @param $attributeSetName
     *
     * @return string
     */
    private function formatAttributeSetName($attributeSetName)
    {
        return strtolower(preg_replace('/[\s-]+/', '_', $attributeSetName));
    }
}
