<?php
/**
 * @category    Kurufootwear
 * @package     Kurufootwear\Catalog
 * @author      Aleksejs Baranovs <info@scandiweb.com>
 * @copyright   Copyright (c) 2018 Scandiweb, Inc (http://scandiweb.com)
 * @license     http://opensource.org/licenses/OSL-3.0 The Open Software License 3.0 (OSL-3.0)
 */

namespace Kurufootwear\Catalog\Plugin;

use Magento\Catalog\Block\Adminhtml\Category\Tree;

/**
 * Class TreePlugin
 *
 * @package Kurufootwear\Catalog\Plugin
 */
class TreePlugin
{
    /**
     * Html special chars decode for category tree
     *
     * @param Tree $subject
     * @param $result
     *
     * @return string
     */
    public function afterBuildNodeName(Tree $subject, $result)
    {
        return htmlspecialchars_decode($result, ENT_QUOTES | ENT_SUBSTITUTE);
    }
}
