<?php
/**
 * @vendor Kurufootwear
 * @module Kurufootwear_Migration
 * @author Jim McGowen <jim@kurufootwear.com>
 * @copyright Copyright (c) 2017 KURU Footwear. All rights reserved.
 */
namespace Kurufootwear\Catalog\Plugin;

use Magento\Catalog\Block\Product\Context;
use Magento\Catalog\Helper\Image;
use Magento\Catalog\Model\ResourceModel\Product\CollectionFactory as ProductCollectionFactory;
use Magento\Catalog\Api\ProductRepositoryInterface;
use Magento\Framework\App\RequestInterface;

class GalleryPlugin
{
    /**
     * @var  ProductCollectionFactory
     */
    protected $productCollectionFactory;
    
    /**
     * @var ProductRepositoryInterface
     * */
    protected $productRepository;
    
    /**
     * @var Image
     */
    protected $_imageHelper;

    /**
     * @var RequestInterface
     */
    protected $request;

    /**
     * GalleryPlugin constructor.
     *
     * @param ProductCollectionFactory $productCollectionFactory
     * @param ProductRepositoryInterface $productRepository
     * @param Context $context
     */
    public function __construct(
        ProductCollectionFactory $productCollectionFactory,
        ProductRepositoryInterface $productRepository,
        Context $context
    ) {
        $this->productCollectionFactory = $productCollectionFactory;
        $this->productRepository = $productRepository;
        $this->request =  $context->getRequest();
        $this->_imageHelper = $context->getImageHelper();
    }
    
    /**
     * Wrapped to load images from duplicate product instead of parent.
     * 
     * @param \Magento\Catalog\Block\Product\View\Gallery $subject
     * @param callable $proceed
     *
     * @return \Magento\Framework\Data\Collection
     */
    public function aroundGetGalleryImages(
        \Magento\Catalog\Block\Product\View\Gallery $subject,
        callable $proceed
    )
    {
        // This is the parent product
        $product = $subject->getProduct();
        $images = $product->getMediaGalleryImages();
    
        // Get the duplicate product based on the color
        $color = $this->request->getParam('color');
        if (!empty($color)) {
            /** @var \Magento\Catalog\Model\Product $duplicateProduct */
            $duplicateProduct = $this->productCollectionFactory
                ->create()
                ->addAttributeToSelect('product_color_data')
                ->addAttributeToSelect('parent_sku')
                ->addAttributeToFilter('product_color_data', $color)
                ->addAttributeToFilter('parent_sku', $product->getSku())
                ->addFieldToFilter('type_id', 'configurable')
                ->getFirstItem();
            if ($duplicateProduct->hasData()) {
                // We have a duplicate so we will load the gallery from it
                $product = $this->productRepository->get($duplicateProduct->getSku());
                $images = $product->getMediaGalleryImages();
            } else {
                $firstColor = current($product->getTypeInstance()->getSalableUsedProducts($product, null))->getAttributeText('color');

                if ($firstColor) {
                    $duplicateProduct = $this->productCollectionFactory
                        ->create()
                        ->addFieldToFilter('parent_sku', $product->getSku())
                        ->addFieldToFilter('type_id', 'configurable')
                        ->addAttributeToFilter('product_color_data', $firstColor)
                        ->getFirstItem();

                    if ($duplicateProduct->hasData()) {
                        // We have a duplicate so we will load the gallery from it
                        $product = $this->productRepository->get($duplicateProduct->getSku());
                        $images = $product->getMediaGalleryImages();
                    }
                }
            }
        }
        
        // Sort the images by position
        $items = $images->toArray();
        usort($items['items'], function($a, $b) {
            return ($a['position'] <=> $b['position']);
        });
        $images->removeAllItems();
        foreach ($items['items'] as $item) {
            $images->addItem(new \Magento\Framework\DataObject($item));
        }
        
        if ($images instanceof \Magento\Framework\Data\Collection) {
            foreach ($images as $image) {
                /* @var \Magento\Framework\DataObject $image */
                $image->setData(
                    'small_image_url',
                    $this->_imageHelper->init($product, 'product_page_image_small')
                        ->setImageFile($image->getFile())
                        ->getUrl()
                );
                $image->setData(
                    'medium_image_url',
                    $this->_imageHelper->init($product, 'product_page_image_medium')
                        ->constrainOnly(true)->keepAspectRatio(true)->keepFrame(false)
                        ->setImageFile($image->getFile())
                        ->getUrl()
                );
                $image->setData(
                    'large_image_url',
                    $this->_imageHelper->init($product, 'product_page_image_large')
                        ->constrainOnly(true)->keepAspectRatio(true)->keepFrame(false)
                        ->setImageFile($image->getFile())
                        ->getUrl()
                );
            }
        }
    
        return $images;
    }
}