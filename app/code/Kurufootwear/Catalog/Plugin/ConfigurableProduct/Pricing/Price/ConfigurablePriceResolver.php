<?php
/**
 * @vendor Kurufootwear
 * @module Kurufootwear_Migration
 * @author Jim McGowen <jim@kurufootwear.com>
 * @copyright Copyright (c) 2017 KURU Footwear. All rights reserved.
 */

namespace Kurufootwear\Catalog\Plugin\ConfigurableProduct\Pricing\Price;

use Closure;
use Kurufootwear\Catalog\Helper\Product\TypeHelper;
use Magento\Catalog\Model\Product;
use Magento\ConfigurableProduct\Model\Product\Type\Configurable;
use Magento\ConfigurableProduct\Pricing\Price\ConfigurablePriceResolver as MagentoConfigurablePriceResolver;
use Magento\Framework\Exception\LocalizedException;
use Magento\Framework\Pricing\SaleableInterface;
use Magento\Framework\App\ObjectManager;
use Magento\Framework\Pricing\PriceCurrencyInterface;
use Magento\ConfigurableProduct\Pricing\Price\PriceResolverInterface;
use Magento\ConfigurableProduct\Pricing\Price\LowestPriceOptionsProviderInterface;

/**
 * Class ConfigurablePriceResolver
 * Resolve configurable class to largest of child product prices
 *
 * @package Kurufootwear\Catalog\Plugin\ConfigurableProduct\Pricing\Price
 */
class ConfigurablePriceResolver implements PriceResolverInterface {
    /**
     * @var TypeHelper
     */
    protected $kuruTypeHelper;

    /**
     * @var PriceResolverInterface
     */
    protected $priceResolver;

    /**
     * @var PriceCurrencyInterface
     * @deprecated
     */
    protected $priceCurrency;

    /**
     * @var Configurable
     * @deprecated
     */
    protected $configurable;

    /**
     * @var LowestPriceOptionsProviderInterface
     */
    private $lowestPriceOptionsProvider;

    /**
     * @param TypeHelper $kuruTypeHelper
     * @param PriceResolverInterface $priceResolver
     * @param Configurable $configurable
     * @param PriceCurrencyInterface $priceCurrency
     * @param LowestPriceOptionsProviderInterface $lowestPriceOptionsProvider
     *
     * @throws \RuntimeException
     */
    public function __construct(
        TypeHelper $kuruTypeHelper,
        PriceResolverInterface $priceResolver,
        Configurable $configurable,
        PriceCurrencyInterface $priceCurrency,
        LowestPriceOptionsProviderInterface $lowestPriceOptionsProvider = null
    ) {
        $this->kuruTypeHelper = $kuruTypeHelper;
        $this->priceResolver = $priceResolver;
        $this->configurable = $configurable;
        $this->priceCurrency = $priceCurrency;
        $this->lowestPriceOptionsProvider = $lowestPriceOptionsProvider ?:
            ObjectManager::getInstance()->get(LowestPriceOptionsProviderInterface::class);
    }

    /**
     * Wrap original price resolver method and use only modified version
     *
     * @param SaleableInterface|Product $product
     * @return float
     * @throws LocalizedException
     */
    public function aroundResolvePrice(MagentoConfigurablePriceResolver $subject, Closure $proceed, SaleableInterface $product)
    {
        return $this->priceResolver->resolvePrice($product);
    }

    /**
     * Get maximal product price
     *
     * @param SaleableInterface $product
     * @return float|mixed|null
     */
    public function resolvePrice(\Magento\Framework\Pricing\SaleableInterface $product)
    {
        $price = null;

        if ($preselectedProduct = $this->kuruTypeHelper->getPreselectedConfigurationProduct()) {
            $productPrice = $this->priceResolver->resolvePrice($preselectedProduct);
            $price = $price ? max($price, $productPrice) : $productPrice;
        } else {
            foreach ($this->lowestPriceOptionsProvider->getProducts($product) as $subProduct) {
                $productPrice = $this->priceResolver->resolvePrice($subProduct);
                $price = $price ? max($price, $productPrice) : $productPrice;
            }
        }

        return $price;
    }

}