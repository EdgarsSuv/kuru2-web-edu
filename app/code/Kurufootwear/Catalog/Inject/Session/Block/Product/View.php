<?php
/**
 * @vendor Kurufootwear
 * @module Kurufootwear_Catalog
 * @author Kristaps Stalidzans <info@scandiweb.com>
 * @copyright Copyright (c) 2017 Scandiweb. All rights reserved.
 * @license http://opensource.org/licenses/afl-3.0.php Academic Free License (AFL 3.0)
 */
namespace Kurufootwear\Catalog\Inject\Session\Block\Product;

class View extends \Magento\Catalog\Block\Product\View
{
    /**
     * Retrieve current product model
     *
     * @return \Magento\Catalog\Model\Product
     */
    public function getProduct()
    {
        if (!$this->_coreRegistry->registry('product') && $this->getProductId()) {
            $product = $this->productRepository->getById($this->getProductId());
            $this->_coreRegistry->register('product', $product);
        }

        $currentProduct = $this->_coreRegistry->registry('product');

        if ($currentProduct) {
            $parentSku = $currentProduct->getData('parent_sku');
            $duplicateCheck = $currentProduct->getData('product_duplicate');

            if ($parentSku && !is_null($duplicateCheck)) {
                try {
                    $currentProduct = $this->productRepository->get($parentSku);
                } catch (\Exception $e) {}
            }
        }

        return $currentProduct;
    }

    /**
     * This is needed to take the correct templates
     *
     * @return string
     */
    protected function _toHtml()
    {
        $this->setModuleName($this->extractModuleName('Magento\Catalog\Block\Product\View'));

        return parent::_toHtml();
    }
}
