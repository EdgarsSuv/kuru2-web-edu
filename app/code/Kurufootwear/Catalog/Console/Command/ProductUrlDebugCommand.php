<?php
/**
 * @category     Kurufootwear
 * @package      Kurufootwear/Catalog
 * @author       Krisjanis Veinbahs <krisjanisv@kurufootwear.com>
 * @copyright    Copyright (c) 2018 KURU Footwear. All rights reserved.
 */
namespace Kurufootwear\Catalog\Console\Command;

use Kurufootwear\Catalog\Helper\Product\Url\ConsistencyInfo;
use Magento\Catalog\Api\ProductRepositoryInterface;
use Magento\Framework\App\State;
use Magento\Framework\Exception\LocalizedException;
use Magento\Framework\Exception\NoSuchEntityException;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

/**
 * Class ProductUrlDebugCommand
 * @package Kurufootwear\Catalog\Console\Command
 */
class ProductUrlDebugCommand extends Command
{
    /**
     * @var ConsistencyInfo
     */
    protected $urlConsistencyInfo;

    /**
     * @var ProductRepositoryInterface
     */
    protected $productRepository;

    /**
     * @var State
     */
    protected $appState;

    /**
     * ProductUrlDebug constructor.
     *
     * @param ConsistencyInfo $urlConsistencyInfo
     * @param ProductRepositoryInterface $productRepository
     */
    public function __construct(
        State $appState,
        ConsistencyInfo $urlConsistencyInfo,
        ProductRepositoryInterface $productRepository
    ) {
        $this->appState = $appState;
        $this->urlConsistencyInfo = $urlConsistencyInfo;
        $this->productRepository = $productRepository;

        parent::__construct();
    }

    /**
     * {@inheritdoc}
     */
    protected function configure()
    {
        $this->setName('product:url:debug');
        $this->setDescription(
            'Log all products with url rewrite inconsistencies and url key duplicates. (has --urlprint)'
        );
        $this->addOption(
            'urlprint',
            'up',
            InputOption::VALUE_NONE,
            __('Log faulty product url keys')
        );
    }

    /**
     * {@inheritdoc}
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        /**
         * Function as if it were the backend
         */
        try {
            $this->appState->getAreaCode();
        } catch (LocalizedException $e) {
            $this->appState->setAreaCode('adminhtml');
        }

        /**
         * Debug additional info
         */
        $verbose = $input->getOption('urlprint');

        $output->writeln('Querying database for product url problems...');
        $brokenProductIds = $this->urlConsistencyInfo->queryProductsWithUrlProblems();

        if (empty($brokenProductIds)) {
            $output->writeln('No products with url key or rewrite issues detected.');
            return;
        }

        $output->writeln('Here\'s a list of products with url issues:');
        foreach ($brokenProductIds as $brokenProductId => $brokenProductStoreIds) {
            try {
                if ($verbose) {
                    $product = $this->productRepository->getById($brokenProductId);
                    $output->writeln(sprintf(
                        'Product id #%s has issues in store(s) %s (url key - %s)',
                        $brokenProductId,
                        implode(', ', $brokenProductStoreIds),
                        $product->getUrlKey() ? '\'' . $product->getUrlKey() . '\'' : '*not available*'
                    ));
                } else {
                    $output->writeln(sprintf(
                        'Product id #%s has issues in store(s) %s',
                        $brokenProductId,
                        implode(', ', $brokenProductStoreIds)
                    ));
                }
            } catch (NoSuchEntityException $noSuchEntityException) {
                $output->writeln(sprintf(
                    'Product id #%s has url info, but doesn\'t exist',
                    $brokenProductId,
                    implode(', ', $brokenProductStoreIds)
                ));
            }
        }
    }
}
