<?php
/**
 * @category     Kurufootwear
 * @package      Kurufootwear/Catalog
 * @author       Krisjanis Veinbahs <krisjanisv@kurufootwear.com>
 * @copyright    Copyright (c) 2018 KURU Footwear. All rights reserved.
 */
namespace Kurufootwear\Catalog\Console\Command;

use Exception;
use Kurufootwear\Catalog\Helper\Product\TypeHelper;
use Kurufootwear\Catalog\Helper\Product\Url\ConsistencyInfo;
use Kurufootwear\Catalog\Helper\Product\Url\Regeneration;
use Kurufootwear\Rma\Model\Source\CustomField\Type;
use League\CLImate\Util\Output;
use Magento\Catalog\Api\Data\ProductInterface;
use Magento\Catalog\Api\ProductRepositoryInterface;
use Magento\Framework\App\State;
use Magento\Framework\Exception\AlreadyExistsException;
use Magento\Framework\Exception\LocalizedException;
use Magento\Framework\Exception\NoSuchEntityException;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

/**
 * Class ProductUrlFixCommand
 * @package Kurufootwear\Catalog\Console\Command
 */
class ProductUrlFixCommand extends Command
{
    /**
     * @var State
     */
    protected $appState;

    /**
     * @var ConsistencyInfo
     */
    protected $urlConsistencyInfo;

    /**
     * @var ProductRepositoryInterface
     */
    protected $productRepository;

    /**
     * @var Regeneration
     */
    protected $urlRegenerationHelper;

    /**
     * @var TypeHelper
     */
    protected $productTypeHelper;

    /**
     * ProductUrlFixCommand constructor.
     *
     * @param State $appState
     * @param ConsistencyInfo $urlConsistencyInfo
     * @param ProductRepositoryInterface $productRepository
     * @param Regeneration $urlRegenerationHelper
     * @param TypeHelper $productTypeHelper
     */
    public function __construct(
        State $appState,
        ConsistencyInfo $urlConsistencyInfo,
        ProductRepositoryInterface $productRepository,
        Regeneration $urlRegenerationHelper,
        TypeHelper $productTypeHelper
    ) {
        $this->appState = $appState;
        $this->urlConsistencyInfo = $urlConsistencyInfo;
        $this->productRepository = $productRepository;
        $this->urlRegenerationHelper = $urlRegenerationHelper;
        $this->productTypeHelper = $productTypeHelper;

        parent::__construct();
    }

    /**
     * {@inheritdoc}
     */
    protected function configure()
    {
        $this->setName('product:url:fix');
        $this->setDescription(
            'Attempt fixing products with url rewrite inconsistencies and url key duplicates. (has --forceincrements)'
        );
        $this->addOption(
            'forceincrements',
            'fi',
            InputOption::VALUE_NONE,
            __('Force incrementation for duplicate url keys even if optional')
        );
        $this->addOption(
            'productid',
            'pi',
            InputOption::VALUE_OPTIONAL,
            __('Fix only specific product id. Store id also required then.')
        );
        $this->addOption(
            'storeid',
            'si',
            InputOption::VALUE_OPTIONAL,
            __('Store id, where product should be fixed. Only required with product id option.')
        );
    }

    /**
     * {@inheritdoc}
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        /**
         * Function as if it were the backend
         */
        try {
            $this->appState->getAreaCode();
        } catch (LocalizedException $e) {
            $this->appState->setAreaCode('adminhtml');
        }

        /**
         * Force url key incrementation
         */
        if ($input->getOption('forceincrements')) {
            $output->writeln('Force increments flag is enabled.');
            $this->urlRegenerationHelper->setForceIncrementation(true);
        }

        /**
         * Optionally fix only a specific product
         */
        $productId = $input->getOption('productid');
        $storeId = $input->getOption('storeid');

        if (!is_null($storeId) || !is_null($productId)) {
            if (ctype_digit($productId) && ctype_digit($storeId)) {
                $output->writeln(
                    sprintf(
                        'Requested fix for specific product #%s in store #%s',
                        $productId,
                        $storeId
                    )
                );

                $this->fixProductUrl($output, (int)$productId, [(int)$storeId]);
            } else {
                $output->writeln(
                    sprintf(
                        'Faulty input data, please recheck the inputs. ' .
                        'Product id and store id must be declared together. ' .
                        'Both must be integers',
                        $productId
                    )
                );

                return 1; // Error exit code
            }
        } else {
            $this->queryAndFixAllProducts($output);
        }

        return 0;
    }

    /**
     * @param OutputInterface $output
     * @throws \Zend_Db_Statement_Exception
     */
    public function queryAndFixAllProducts(OutputInterface $output)
    {
        $output->writeln('Querying database for product url problems...');
        $brokenProducts = $this->urlConsistencyInfo->queryProductsWithUrlProblems();

        if (empty($brokenProducts)) {
            $output->writeln('No products with url key or rewrite issues detected.');
            return;
        }

        $output->writeln('Attempting to fix broken product urls...');

        /**
         * Master product fixing
         */
        $output->writeln('First fixing master products (they\'ll be a source for fixing children):');
        foreach ($brokenProducts as $brokenProductId => $storeIds) {
            $this->fixProductUrl($output, $brokenProductId, $storeIds, true);
        }

        /**
         * Duplicate and simple product fixing
         */
        $output->writeln('Fixing simple and duplicate products:');
        foreach ($brokenProducts as $brokenProductId => $storeIds) {
            $this->fixProductUrl($output, $brokenProductId, $storeIds, false);
        }
    }

    /**
     * Attempt loading and fixing product with url issues and log exceptions
     *
     * @param OutputInterface $output
     * @param $productId
     * @param $storeIds
     * @param $fixOnlyMaster
     * @return void
     */
    public function fixProductUrl(OutputInterface $output, $productId, $storeIds, $fixOnlyMaster = false)
    {
        try {
            $output->writeln(sprintf('Checking product id #%s', $productId));

            foreach ($storeIds as $storeId) {
                $product = $this->productRepository->getById($productId, false, $storeId);
                $isMaster = $this->productTypeHelper->getKuruProductType($product) === TypeHelper::KURU_MASTER;

                if (!$isMaster && $fixOnlyMaster) {
                    $output->writeln(sprintf('Not a master product (#%s), skipping', $productId));
                    return;
                }

                $oldUrlKey = $product->getUrlKey() ?: '*not available*';
                $this->urlRegenerationHelper->correctProductUrlKey($product);
                $newUrlKey = $product->getUrlKey() ?: '*not available*';

                $output->writeln(
                    sprintf(
                        'Fixed product #%s in store #%s (urlkey \'%s\' => \'%s\')',
                        $productId,
                        $storeId,
                        $oldUrlKey,
                        $newUrlKey
                    )
                );
            }
        } catch (NoSuchEntityException $exception) {
            $output->writeln(
                sprintf(
                    'Product #%s doesn\'t exist, old url data must be in database.',
                    $productId
                )
            );
        } catch (AlreadyExistsException $exception) {
            $output->writeln(
                sprintf(
                    'AlreadyExistsException. Failed to generate and/or save unique url for product #%s.',
                    $productId
                )
            );
        } catch (Exception $exception) {
            $output->writeln(
                sprintf(
                    'An error occurred while trying to fix product url, see logs, product #%s.',
                    $productId
                )
            );
        }
    }
}
