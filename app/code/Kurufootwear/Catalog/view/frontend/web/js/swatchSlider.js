/**
 * @category  Kurufootwear
 * @package   Kurufootwear\Catalog
 * @author    Krisjanis Veinbahs <krisjanisv@scandiweb.com>
 * @copyright Copyright (c) 2017 Scandiweb, Inc (http://scandiweb.com)
 * @license   http://opensource.org/licenses/OSL-3.0 The Open Software License 3.0 (OSL-3.0)
 */

define([
    'jquery',
    'jquery/ui',
    'scandiweb/slider'
], function($) {
    'use strict';

    /**
     * Create widget for swatch sliders/updates
     */
    var SwatchSlider = {
        options: {
            infinite: true,
            slidesToShow: (window.innerWidth < 768) ? 2 : 3,
            slidesToScroll: 1,
            unslickTimeout: 2000 // How long until 'unslick' after unhover
        },

        initializing: true,

        bindItems: function (swatchSelector) {
            var self = this;
            var $swatchLists = $(swatchSelector).filter('[data-swatch!="1"]');

            $swatchLists.on('product-hover-on', function() {
                var $element = $(this);
                var $firstList = $element.find('.swatch-attribute:first-of-type .swatch-attribute-options');

                // Clear unslick callback timeout
                if (typeof $firstList.data('unslickTimeoutId') !== 'undefined') {
                    clearTimeout($firstList.data('unslickTimeoutId'));
                }

                // Add loader (should be done in PHP level)
                // And bind listeners on swatch clicks
                if ($firstList.siblings().filter('.loader-wrapper').length === 0) {
                    var loader = "<div class='loader-wrapper'><span class='loader'></span></div>";
                    $(loader).insertAfter($firstList);
                    self.bindUpdateOnClick($firstList);
                }

                // Start timeout with callback until swatch instantiation
                self.callStoppableCallback(function() {
                    if (!$firstList.hasClass('slick-initialized')) {
                        var slider = self.addSlider($firstList);

                        slider.addClass('visible');
                    }
                }, 300);
            });

            $swatchLists.on('product-hover-off', function() {
                var $element = $(this);
                var $firstList = $element.find('.swatch-attribute:first-of-type .swatch-attribute-options');

                // Stop timeout with callback until swatch instantiation
                // Because hover was too short
                self.stopCallback();

                // Start unslick callback timeout
                // In case slick is instantiated
                var unslickTimeoutId = setTimeout(function() {
                    var $product = $element.closest('.product-item-info-wrapper');
                    if ($firstList.hasClass('slick-initialized') && !$product.hasClass('hover')) {
                        $firstList.removeClass('visible');
                        $firstList.slick('unslick');
                    }
                }, self.options.unslickTimeout);

                // Store unslick timeout id
                $firstList.data('unslickTimeoutId', unslickTimeoutId);
            });

            $swatchLists.attr('data-swatch', '1');
        },

        /**
         * Run callback after some timeout
         * that can be stopped mid-timeout
         *
         * @param callback
         * @param timeout
         */
        callStoppableCallback: function(callback, timeout) {
            var self = this;
            this.initializing = true;

            setTimeout(function() {
                if (self.initializing) {
                    callback();
                }
            }, timeout);
        },

        stopCallback: function() {
            this.initializing = false;
        },

        /**
         * Initialize slider on swatches
         * @param $swatchSlider
         */
        addSlider: function($swatchSlider) {
            return $swatchSlider
                .slick(this.options);
        },

        /**
         * On swatch click swap new url/title
         * @param $swatchSlider
         */
        bindUpdateOnClick: function($swatchSlider) {
            $swatchSlider
                .find('.swatch-option')
                .on('click', function(e) {
                var $swatch = $(e.target);
                var $product = $swatch
                    .closest('.product-item-info');

                var $productTitle = $product
                    .find('.product-item-link');
                var $productSwatchTitle = $product
                    .find('.swatch-attribute-label');
                var $productPhoto = $product
                    .find('.product-item-photo');

                var label = $swatch.attr('option-label');
                var productHref = $productTitle
                    .attr('href');
                const colorAttribute = new RegExp('\\bcolor\\b');
                const regExp = new RegExp('color' + '(.+?)(&|$)', 'g');
                var newUrl = productHref.replace(regExp, 'color=' + label + '$2');

                if (!colorAttribute.test(newUrl)) {
                    newUrl += '?color=' + label;
                }

                $productSwatchTitle.text(label);
                $productTitle.attr('href', newUrl);
                $productPhoto.attr('href', newUrl);
            });
        }
    };

    return SwatchSlider;
});

