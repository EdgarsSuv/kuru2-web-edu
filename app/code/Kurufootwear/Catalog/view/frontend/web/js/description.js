/**
 * @vendor Kurufootwear
 * @module Kurufootwear_Catalog
 * @author Kristaps Stalidzans <info@scandiweb.com>
 * @copyright Copyright (c) 2017 Scandiweb. All rights reserved.
 * @license http://opensource.org/licenses/afl-3.0.php Academic Free License (AFL 3.0)
 */

define([
    'jquery',
    'jquery/ui'
], function($) {

    'use strict';

    $.widget('description.js', {
        _create: function (e) {
            $('.product-description-content, .product-benefits-content').click(function(e) {
                if ($(e.target).hasClass('expand')) {
                    $('.product-description-content, .product-benefits-content, .expand-items').toggleClass('expand');

                    if ($('.expand-items').hasClass('expand')) {
                        $('.expand-items').text('Expand').fadeIn();
                    } else {
                        $('.expand-items').text('Collapse').fadeIn();
                    }

                    e.preventDefault();
                }
            });

            $('.expand-items').click(function(e) {
                $('.product-description-content, .product-benefits-content, .expand-items').toggleClass('expand');

                if ($('.expand-items').hasClass('expand')) {
                    $('.expand-items').text('Expand').fadeIn();
                } else {
                    $('.expand-items').text('Collapse').fadeIn();
                }

                e.preventDefault();
            });
        }
    });

    return $.description.js;
});
