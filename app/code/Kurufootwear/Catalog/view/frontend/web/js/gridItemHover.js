/**
 * @category  Kurufootwear
 * @package   Kurufootwear\Catalog
 * @author    Krišjānis Veinbahs <krisjanisv@scandiweb.com>
 * @copyright Copyright (c) 2017 Scandiweb, Inc (http://scandiweb.com)
 * @license   http://opensource.org/licenses/OSL-3.0 The Open Software License 3.0 (OSL-3.0)
 */

define([
    'jquery',
    'jquery/ui'
], function($) {
    'use strict';

    var GridItemHover = {
        /**
         * Initialize hover effects
         * and link toggling
         * @private
         */
        bindItems: function (itemSelector) {
            var self = this;
            var $items = $(itemSelector).filter('[data-hover!="1"]');

            /**
             * Bind hover listeners (Desktop device support)
             */
            $items.hover(function() {
                var $element = $(this);
                self.triggerHoverOn($element);
                self.enableHover($element);
                self.enableLinks($element);
            }, function() {
                var $element = $(this);
                self.triggerHoverOff($element);
                self.disableHover($element);
                self.disableLinks($element);
                self.disableMobile($element);
                self.disableMobileLinks($element);
            });

            /**
             * Bind mobile touch listeners (Touch device support)
             * ! Script supposes that touchstart triggers only on
             *   touch devices
             */
            $items.on('touchstart', function() {
                var $element = $(this);
                /**
                 * Enable touch device flag (class)
                 */
                if (!$element.hasClass('mobile')) {
                    self.enableMobile($element);
                }
                self.triggerHoverOn($element);
            });
            $items.on('touchend', function() {
                var $element = $(this);
                /**
                 * After finger lifted off screen enable links
                 * unless hover didn't actually happen
                 */
                setTimeout(function() {
                    /**
                     * If scrolled and neither mouseenter/mouseleave triggered
                     * turn off links, because hover didn't happen
                     */
                    if (!($element.hasClass('hover'))) {
                        self.disableLinks($element);
                        self.disableMobile($element);
                        self.disableMobileLinks($element);
                        self.triggerHoverOff($element);
                    } else {
                        self.enableMobileLinks($element);
                    }
                /**
                 * Links have to be enabled after finger is lifted
                 */
                }, 300);
            });

            /**
             * On scroll suppose unhover
             */
            $(document).on('scroll', function() {
                var $element = $(this);
                self.triggerHoverOff($element);
                self.disableHover($element);
                self.disableLinks($element);
                self.disableMobile($element);
                self.disableMobileLinks($element);
            })

            $items.attr('data-hover', '1');
        },

        /**
         * Trigger hover events
         */
        triggerHoverOn: function($element) {
            var e = 'product-hover-on';
            $element.find('.swatches').trigger(e);
        },

        triggerHoverOff: function($element) {
            var e = 'product-hover-off';
            $element.find('.swatches').trigger(e);
        },

        /**
         * Handle parent elements height
         * and current elements hover class
         * @param $element
         */
        enableHover: function($element) {
            $element.addClass('hover');
        },
        disableHover: function($element) {
            $element.removeClass('hover');
        },

        /**
         * Enable/disable links
         * @param $element
         */
        enableLinks: function($element) {
            $element.addClass('enabled');
        },
        disableLinks: function($element) {
            $element.removeClass('enabled');
        },

        /**
         * Enable/disable mobile flag
         */
        enableMobile: function($element) {
            $element.addClass('mobile');
        },
        disableMobile: function($element) {
            $element.removeClass('mobile');
        },

        /**
         * Mobile link enable/disable
         * @param $element
         */
        enableMobileLinks: function($element) {
            $element.addClass('enabled-mobile');
        },
        disableMobileLinks: function($element) {
            $element.removeClass('enabled-mobile');
        }
    };

    return GridItemHover;
});
