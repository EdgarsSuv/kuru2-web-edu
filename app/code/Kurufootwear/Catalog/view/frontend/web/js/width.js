/**
 * @category  Kurufootwear
 * @package   Kurufootwear\Catalog
 * @author    Kristaps Stalidzans <info@scandiweb.com>
 * @copyright Copyright (c) 2017 Scandiweb, Inc (http://scandiweb.com)
 * @license   http://opensource.org/licenses/OSL-3.0 The Open Software License 3.0 (OSL-3.0)
 */

define([
    'jquery',
    'jquery/ui'
], function($) {

    'use strict';

    $.widget('width.js', {
        classes: {
            swatchOpt: '.swatch-opt',
            optionsWrapper: '.product-options-wrapper'
        },

        _create: function () {
            $('.width .button').on('click', function(e) {
               var url = $(this).attr('href'),
                   url_string = new URL(window.location.href),
                   color = url_string.searchParams.get('color');

               if (!$(e.target).hasClass('available') || $(e.target).hasClass('checked')) {
                   e.stopImmediatePropagation();
                   e.preventDefault();

                   return false;
               }

               if (url && color) {
                   window.location.href = url + '?color=' + color;
                   e.stopPropagation();
                   e.preventDefault();
               } else {
                   window.location.href = url;
                   e.stopPropagation();
                   e.preventDefault();
               }
            });

            this.removeInitClass();
        },

        /**
         * Remove initialize class from product-options-wrapper if no swatch-opt wrapper
         */
        removeInitClass: function () {
            var self = this;

            if ($(self.classes.swatchOpt).length < 1) {
                $(self.classes.optionsWrapper).removeClass('initializing');
            }
        }
    });

    return $.width.js;
});
