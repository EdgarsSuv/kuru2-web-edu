<?php
/**
 * Copyright © 2017 Mageside. All rights reserved.
 * See MS-LICENSE.txt for license details.
 */
namespace Kurufootwear\SubscribeAtCheckout\Model\Plugin\Checkout;

use Magento\Quote\Model\QuoteRepository;
use Magento\Checkout\Model\ShippingInformationManagement as ShippingManagement;
use Magento\Checkout\Api\Data\ShippingInformationInterface;
use Mageside\SubscribeAtCheckout\Helper\Config as Helper;

/**
 * Overridden to handle express checkouts which do not have an 
 * input for "subscribe to newsletter" thus do not provide the 
 * extended attribute for it in the shipping information.
 * 
 * If the extended attribute is missing this will check the 
 * config for the default setting instead.
 * 
 * Class ShippingInformationManagement
 * @package Kurufootwear\SubscribeAtCheckout\Model\Plugin\Checkout
 */
class ShippingInformationManagement
{
    /**
     * @var Helper
     */
    protected $helper;

    /**
     * @var QuoteRepository
     */
    protected $quoteRepository;

    /**
     * @param QuoteRepository $quoteRepository
     * @param Helper $helper
     */
    public function __construct(
        QuoteRepository $quoteRepository,
        Helper $helper
    ) {
        $this->quoteRepository = $quoteRepository;
        $this->helper = $helper;
    }

    /**
     * @param ShippingManagement $subject
     * @param $cartId
     * @param ShippingInformationInterface $addressInformation
     */
    public function beforeSaveAddressInformation(
        ShippingManagement $subject,
        $cartId,
        ShippingInformationInterface $addressInformation
    ) {
        if ($this->helper->getConfigModule('enabled')) {
            if ($this->helper->getConfigModule('checkout_subscribe') == 3 ||
                $this->helper->getConfigModule('checkout_subscribe') == 4
            ) {
                $newsletterSubscribe = 1;
            } else {
                $extAttributes = $addressInformation->getExtensionAttributes();
                if (!empty($extAttributes)) {
                    $newsletterSubscribe = $extAttributes->getNewsletterSubscribe() ? 1 : 0;
                }
                else {
                    // 1 = checked by default (see Mageside\SubscribeAtCheckout\Model\Config\Source\CheckoutSubscribe)
                    if ($this->helper->getConfigModule('checkout_subscribe') == 1) {
                        $newsletterSubscribe = 1;
                    }
                    else {
                        $newsletterSubscribe = 0;
                    }
                }
            }
            $quote = $this->quoteRepository->getActive($cartId);
            $quote->setNewsletterSubscribe($newsletterSubscribe);
        }
    }
}
