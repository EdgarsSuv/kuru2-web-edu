<?php
/**
 * @vendor Kurufootwear
 * @module Kurufootwear_SubscribeAtCheckout
 * @author Kristaps Stalidzans <info@scandiweb.com>
 * @copyright Copyright (c) 2017 Scandiweb, Ltd (http://scandiweb.com)
 * @license http://opensource.org/licenses/afl-3.0.php Academic Free License (AFL 3.0)
 */
namespace Kurufootwear\SubscribeAtCheckout\Model\Config\Source;

class SubscribeLayoutProcessor extends \Mageside\SubscribeAtCheckout\Model\Config\Source\SubscribeLayoutProcessor
{
    /**
     * {@inheritdoc}
     */
    public function process($jsLayout)
    {
        $checkbox = $this->_helper->getConfigModule('checkout_subscribe');

        $checked = $checkbox == 2 ? 0 : 1;
        $visible = $checkbox == 3 ? 0 : 1;
        $changeable = $checkbox == 4 ? 0 : 1;

        if (!$this->_helper->getConfigModule('enabled')) {
            $visible = 0;
        }

        $jsLayoutSubscribe = [
            'components' => [
                'checkout' => [
                    'children' => [
                        'steps' => [
                            'children' => [
                                'billing-step' => [
                                    'children' => [
                                        'payment' => [
                                            'children' => [
                                                'afterMethods' => [
                                                    'children' => [
                                                        'newsletter-subscribe' => [
                                                            'config' => [
                                                                'checkoutLabel' =>
                                                                    $this->_helper->getConfigModule('checkout_label'),
                                                                'checked' => $checked,
                                                                'visible' => $visible,
                                                                'changeable' => $changeable
                                                            ]
                                                        ]
                                                    ]
                                                ]
                                            ]
                                        ]
                                    ]
                                ]
                            ]
                        ]
                    ]
                ]
            ]
        ];

        $jsLayout = array_merge_recursive($jsLayout, $jsLayoutSubscribe);

        return $jsLayout;
    }
}
