<?php
/**
 * @category     Kurufootwear
 * @package      Kurufootwear\Menumanager
 * @copyright    Copyright (c) 2017 Scandiweb, Inc (http://scandiweb.com)
 * @license      http://opensource.org/licenses/afl-3.0.php Academic Free License (AFL 3.0)
 */
namespace Kurufootwear\Menumanager\Helper\Adminhtml;

use Magento\Backend\App\ConfigInterface;
use Magento\Captcha\Model\CaptchaFactory;
use Magento\Framework\App\Helper\Context;
use Magento\Framework\Filesystem;
use Magento\Framework\Registry;
use Magento\Store\Model\StoreManager;
use Scandiweb\Menumanager\Model\ResourceModel\Item\Collection;

class Data extends \Scandiweb\Menumanager\Helper\Adminhtml\Data
{
    /**
     * @var Collection
     */
    private $itemCollection;

    /**
     * Data constructor.
     * @param Context $context
     * @param StoreManager $storeManager
     * @param Filesystem $filesystem
     * @param CaptchaFactory $factory
     * @param ConfigInterface $backendConfig
     * @param Registry $registry
     * @param Collection $itemCollection
     */
    public function __construct(
        Context $context,
        StoreManager $storeManager,
        Filesystem $filesystem,
        CaptchaFactory $factory,
        ConfigInterface $backendConfig,
        Registry $registry,
        Collection $itemCollection
    )
    {
        $this->itemCollection = $itemCollection;

        parent::__construct(
            $context,
            $storeManager,
            $filesystem,
            $factory,
            $backendConfig,
            $registry
        );
    }


    /**
     * Item's URL types
     */
    const TYPE_NO_URL = 3;

    /**
     * Prepare available item url types
     *
     * @return array
     */
    public function getUrlTypes()
    {
        return [
            self::TYPE_CUSTOM_URL => __('Custom URL'),
            self::TYPE_CMS_PAGE => __('CMS Page'),
            self::TYPE_CATEGORY => __('Category'),
            self::TYPE_NO_URL => __('No url')
        ];
    }

    /**
     * @return array
     */
    public function getMenusAsOptionsFilter()
    {
        $menus[''] = __('Please select items');
        $itemOptions = $this->itemCollection
            ->toItemOptionArray();
        $menus = array_merge($menus, $itemOptions);

        return $menus;
    }

    /**
     * @return array
     */
    public function getCategoriesAsOptionsFilter()
    {
        $collection = $this->_getCategoryCollection();
        $categories = [];

        $menus[''] = __('-- Please Select --');

        foreach ($collection as $category) {
            $name = $category->getName();
            $urlPath = $category->getUrlPath();

            if (isset($name) && isset($urlPath)) {
                $prefix = '';
                for ($i = 2; $i < $category->getLevel(); $i++) {
                    $prefix = $prefix . "-";
                }

                $categories[$category->getId()] = $prefix . ' ' . __($category->getName());
            }
        }

        return $categories;
    }
}
