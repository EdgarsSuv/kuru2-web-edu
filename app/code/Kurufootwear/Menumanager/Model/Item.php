<?php
/**
 * @category     Kurufootwear
 * @package      Kurufootwear\Menumanager
 * @copyright    Copyright (c) 2017 Scandiweb, Inc (http://scandiweb.com)
 * @license      http://opensource.org/licenses/afl-3.0.php Academic Free License (AFL 3.0)
 */

namespace Kurufootwear\Menumanager\Model;

use Kurufootwear\Menumanager\Api\Data\ItemInterface;

class Item extends \Scandiweb\Menumanager\Model\Item implements ItemInterface
{
    /**
     * @return string
     */
    public function getCssClass()
    {
        return $this->getData(self::CSS_CLASS);
    }
}
