<?php
/**
 * @category     Kurufootwear
 * @package      Kurufootwear\Menumanager
 * @copyright    Copyright (c) 2017 Scandiweb, Inc (http://scandiweb.com)
 * @license      http://opensource.org/licenses/afl-3.0.php Academic Free License (AFL 3.0)
 */

namespace Kurufootwear\Menumanager\Api\Data;

interface ItemInterface
{
    /**
     * Constants for keys of data array. Identical to the name of the getter in snake case
     */
    const CSS_CLASS = 'css_class';

    /**
     * @return mixed
     */
    public function getCssClass();
}
