<?php
/**
 * @category     Kurufootwear
 * @package      Kurufootwear\Menumanager
 * @copyright    Copyright (c) 2017 Scandiweb, Inc (http://scandiweb.com)
 * @license      http://opensource.org/licenses/afl-3.0.php Academic Free License (AFL 3.0)
 */

namespace Kurufootwear\Menumanager\Block\Adminhtml\Item\Edit;

class Form extends \Scandiweb\Menumanager\Block\Adminhtml\Item\Edit\Form
{
    /**
     *  Adds new field
     */
    protected function _prepareForm()
    {
        parent::_prepareForm();

        /** @var \Scandiweb\Menumanager\Model\Item $item */
        $item = $this->_menumanagerHelper->initItem();
        $menuId = $this->getRequest()->getParam('menu_id', null);

        /** @var \Magento\Framework\Data\Form $form */
        $form = $this->getForm();

        $fieldset = $form->getElement('base_fieldset');

        $fieldset->addField(
            'css_class',
            'text',
            [
                'name' => 'css_class',
                'label' => __('Title css class'),
                'title' => __('Title css class'),
                'required' => false
            ]
        );

        if (!$item->getId()) {
            $item->setData('menu_id', $menuId);
            $item->setData('is_active', \Scandiweb\Menumanager\Helper\Adminhtml\Data::STATUS_ENABLED);
        }

        $form->setValues($item->getData());
        $form->setUseContainer(true);
        $this->setForm($form);
    }
}
