<?php
/**
 * @category     Kurufootwear
 * @package      Kurufootwear\Menumanager
 * @copyright    Copyright (c) 2018 Scandiweb, Inc (http://scandiweb.com)
 * @license      http://opensource.org/licenses/afl-3.0.php Academic Free License (AFL 3.0)
 */
namespace Kurufootwear\Menumanager\Block\Adminhtml\Item;

use Kurufootwear\Menumanager\Helper\Adminhtml\Data as KuruHelperData;
use Magento\Backend\Block\Template\Context;
use Magento\Backend\Helper\Data;
use Magento\Framework\Registry;
use Scandiweb\Menumanager\Helper\Adminhtml\Data as MenuManagerData;
use Scandiweb\Menumanager\Model\ResourceModel\Item\Collection;

class Grid extends \Scandiweb\Menumanager\Block\Adminhtml\Item\Grid
{
    /**
     * @var KuruHelperData
     */
    private $kuruHelperData;

    /**
     * Grid constructor.
     * @param Context $context
     * @param Data $backendHelper
     * @param Collection $itemCollection
     * @param MenuManagerData $menumanagerHelper
     * @param Registry $registry
     * @param KuruHelperData $kuruHelperData
     * @param array $data
     */
    public function __construct(
        Context $context,
        Data $backendHelper,
        Collection $itemCollection,
        MenuManagerData $menumanagerHelper,
        Registry $registry,
        KuruHelperData $kuruHelperData,
        array $data = []
    )
    {
        $this->kuruHelperData = $kuruHelperData;

        parent::__construct(
            $context,
            $backendHelper,
            $itemCollection,
            $menumanagerHelper,
            $registry,
            $data
        );
    }

    /**
     * Create grid columns
     *
     * @return $this
     * @throws \Exception
     */
    protected function _prepareColumns()
    {
        parent::_prepareColumns();

        $this->removeColumn('title');
        $this->removeColumn('parent_title');
        $this->removeColumn('url');
        $this->removeColumn('position');
        $this->removeColumn('is_active');
        $this->removeColumn('edit');
        $this->removeColumn('delete');

        $this->addColumn(
            'title',
            [
                'header' => __('Item Title'),
                'type' => 'text',
                'index' => 'title',
                'escape' => true,
                'filter_index' => 'main_table.title'
            ]
        );

        $this->addColumn(
            'parent_title',
            [
                'header' => __('Parent Item'),
                'type' => 'options',
                'index' => 'parent_id',
                'escape' => true,
                'renderer' => '\Scandiweb\Menumanager\Block\Adminhtml\Item\Renderer\Parentitem',
                'options' => $this->kuruHelperData->getMenusAsOptionsFilter(),
                'filter_index' => 'main_table.parent_id'
            ]
        );

        $this->addColumn(
            'url',
            [
                'header' => __('URL'),
                'type' => 'text',
                'index' => 'url',
                'sortable' => false,
                'filter_index' => 'main_table.url'
            ]
        );

        $this->addColumn(
            'position',
            [
                'header' => __('Position'),
                'type' => 'number',
                'index' => 'position',
            ]
        );

        $this->addColumn(
            'is_active',
            [
                'header' => __('Status'),
                'index' => 'is_active',
                'type' => 'options',
                'options' => $this->_menumanagerHelper->getAvailableStatuses()
            ]
        );

        $this->addColumn(
            'edit',
            [
                'header' => __('#'),
                'type' => 'action',
                'getter' => 'getId',
                'actions' => [
                    [
                        'caption' => __('Edit'),
                        'url' => [
                            'base' => '*/item/edit',
                            'params' => ['menu_id' => $this->getRequest()->getParam('menu_id')]
                        ],
                        'field' => 'item_id',
                        'class' => 'edit-menu-item',
                    ]
                ],
                'filter' => false,
                'sortable' => false,
                'index' => 'item_id',
                'header_css_class' => 'col-action',
                'column_css_class' => 'col-action'
            ]
        );

        $this->addColumn(
            'delete',
            [
                'header' => __('#'),
                'type' => 'action',
                'getter' => 'getId',
                'actions' => [
                    [
                        'caption' => __('Delete'),
                        'url' => [
                            'base' => '*/item/delete',
                            'params' => ['menu_id' => $this->getRequest()->getParam('menu_id')]
                        ],
                        'field' => 'item_id',
                        'confirm' => __('Do you really want to delete this item?'),
                    ]
                ],
                'filter' => false,
                'sortable' => false,
                'index' => 'item_id',
                'header_css_class' => 'col-action',
                'column_css_class' => 'col-action'
            ]
        );

        return $this;
    }
}
