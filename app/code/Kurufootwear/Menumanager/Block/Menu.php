<?php
/**
 * @category     Kurufootwear
 * @package      Kurufootwear\Menumanager
 * @copyright    Copyright (c) 2017 Scandiweb, Inc (http://scandiweb.com)
 * @license      http://opensource.org/licenses/afl-3.0.php Academic Free License (AFL 3.0)
 */

namespace Kurufootwear\Menumanager\Block;

use Magento\Framework\Data\Tree\Node;

class Menu extends \Scandiweb\Menumanager\Block\Menu
{
    /**
     * Recursively generates top menu html from data that is specified in $menuTree
     *
     * @param \Magento\Framework\Data\Tree\Node $menuTree
     * @param string $childrenWrapClass
     * @param int $limit
     * @param array $colBrakes
     *
     * @return string
     *
     * @SuppressWarnings(PHPMD.CyclomaticComplexity)
     * @SuppressWarnings(PHPMD.NPathComplexity)
     */
    protected function _getHtml(Node $menuTree, $childrenWrapClass, $limit, $colBrakes = [])
    {
        $html = '';

        $children = $menuTree->getChildren();
        $parentLevel = $menuTree->getLevel();
        $childLevel = $parentLevel === null ? 0 : $parentLevel + 1;

        $counter = 1;
        $itemPosition = 1;
        $childrenCount = $children->count();

        $parentPositionClass = $menuTree->getPositionClass();
        $itemPositionClassPrefix = $parentPositionClass ? $parentPositionClass . '-' : 'nav-';

        foreach ($children as $child) {
            if (!$this->_isMenuItemActive($child)) {
                continue;
            }

            $this->_generateFinalUrl($child);

            $child->setLevel($childLevel);
            $child->setIsFirst($counter == 1);
            $child->setIsLast($counter == $childrenCount);
            $child->setPositionClass($itemPositionClassPrefix . $counter);
            $child->setIsActiveUrl($this->_hasCurrentUrl($child));
            $child->setType($child->getOpenType() === 0 ? 'target="_blank"' : '');

            $outermostClassCode = '';
            $outermostClass = $menuTree->getOutermostClass() . ' count' . $counter;

            if ($childLevel == 0 && $outermostClass) {
                $outermostClassCode = ' class="' . $outermostClass . '" ';
                $child->setClass($outermostClass);
            }

            if (count($colBrakes) && $colBrakes[$counter]['colbrake']) {
                $html .= '</ul></li><li class="column"><ul>';
            }

            $html .= '<li ' . $this->_getRenderedMenuItemAttributes($child) . '>';

            if ($child->getUrlType() === '3') {
                $html .= '<span class="' . $child->getCssClass() . '">' . $this->escapeHtml(
                        $child->getTitle()
                    ) . '</span>';
            } else {
                $html .= '<a href="' . $child->getFullUrl() . '" ' . $outermostClassCode . '><span class="' . $child->getCssClass() . '">' . $this->escapeHtml(
                        $child->getTitle()
                    ) . '</span></a>';
            }

            $html .= $this->_addSubMenu(
                $child,
                $childLevel,
                $childrenWrapClass,
                $limit
            ) . '</li>';

            $itemPosition++;
            $counter++;
        }

        if (count($colBrakes) && $limit) {
            $html = '<li class="column"><ul>' . $html . '</ul></li>';
        }

        return $html;
    }
}
