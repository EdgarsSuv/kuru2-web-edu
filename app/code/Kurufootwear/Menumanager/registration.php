<?php
/**
 * @category    Kurufootwear
 * @package     Kurufootwear\Menumanager
 * @copyright   Copyright (c) 2017 Magento. All rights reserved.
 * @license     http://opensource.org/licenses/afl-3.0.php Academic Free License (AFL 3.0)
 */

\Magento\Framework\Component\ComponentRegistrar::register(
    \Magento\Framework\Component\ComponentRegistrar::MODULE,
    'Kurufootwear_Menumanager',
    __DIR__
);
