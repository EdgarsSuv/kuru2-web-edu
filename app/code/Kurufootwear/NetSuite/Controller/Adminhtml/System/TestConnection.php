<?php
/**
 * @category    Kurufootwear
 * @package     Kurufootwear\NetSuite
 * @author      Jim McGowen <jim@kurufootwear.com>
 * @copyright   Copyright (c) 2018 KURU Footwear. All rights reserved.
 */

namespace Kurufootwear\NetSuite\Controller\Adminhtml\System;

use Magento\Backend\App\Action;
use Magento\Backend\App\Action\Context;
use Magento\Framework\Controller\Result\JsonFactory;
use Kurufootwear\NetSuite\Helper\Inventory;
use Magento\Framework\Exception\IntegrationException;

/**
 * Class TestConnection
 * 
 * Tests the connection to NetSuite.
 * 
 * @package Kurufootwear\NetSuite\Controller\Adminhtml\System
 */
class TestConnection extends Action
{
    /** @var JsonFactory  */
    protected $resultJsonFactory;
    
    /** @var Inventory  */
    protected $inventory;
    
    /**
     * @param Context $context
     * @param JsonFactory $resultJsonFactory
     * @param Inventory $inventory
     */
    public function __construct(
        Context $context,
        JsonFactory $resultJsonFactory,
        Inventory $inventory
    ) {
        $this->inventory = $inventory;
        $this->resultJsonFactory = $resultJsonFactory;
        parent::__construct($context);
    }
    
    /**
     * @return \Magento\Framework\Controller\Result\Json
     */
    public function execute()
    {
        $result = $this->resultJsonFactory->create();
        
        try {
            // As long as there is no exception then we are good
            $this->inventory->searchInventoryItem('200620-100');
        }
        catch (\Exception $e) {
            $msg = $e->getMessage();
            if ($e->getPrevious()) {
                $msg .= ': ' . $e->getPrevious()->getMessage();
            }
            return $result->setData(['success' => false, 'message' => $msg]);
        }
        
        return $result->setData(['success' => true, 'message' => 'Connection successful']);
    }
}
