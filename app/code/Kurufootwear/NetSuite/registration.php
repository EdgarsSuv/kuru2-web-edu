<?php
/**
 * @vendor      Kurufootwear
 * @module      Kurufootwear_NetSuite
 * @author      Jim McGowen <jim@kurufootwear.com>
 * @copyright   Copyright (c) 2017 KURU Footwear. All rights reserved.
 */
\Magento\Framework\Component\ComponentRegistrar::register(
    \Magento\Framework\Component\ComponentRegistrar::MODULE,
    'Kurufootwear_NetSuite',
    __DIR__
);
