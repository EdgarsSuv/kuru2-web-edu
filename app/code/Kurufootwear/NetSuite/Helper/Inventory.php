<?php
/**
 * @category    Kurufootwear
 * @package     Kurufootwear\NetSuite
 * @author      Jim McGowen <jim@kurufootwear.com>
 * @copyright   Copyright (c) 2018 KURU Footwear. All rights reserved.
 */

namespace Kurufootwear\NetSuite\Helper;

use Magento\Framework\App\Helper\Context;
use NetSuite\NetSuiteService;
use NetSuite\Classes\SearchStringFieldFactory;
use NetSuite\Classes\ItemSearchBasicFactory;
use NetSuite\Classes\InventoryAdjustmentFactory;
use NetSuite\Classes\InventoryAdjustmentInventoryFactory;
use NetSuite\Classes\InventoryAdjustmentInventoryListFactory;
use NetSuite\Classes\SearchRequestFactory;
use NetSuite\Classes\AddRequestFactory;
use NetSuite\Classes\RecordRefFactory;
use Magento\Framework\Exception\IntegrationException;

/**
 * Class Inventory
 * @package Kurufootwear\NetSuite\Helper
 */
class Inventory extends \Magento\Framework\App\Helper\AbstractHelper
{
    // NetSuite accounts for inventory adjustments (internal ids from NetSuite)
    const INV_ACCOUNT_TCG_MEN = 220;
    const INV_ACCOUNT_TCG_WOMEN = 221;
    const INV_ACCOUNT_CARE_PRODUCTS = 223;
    
    // NetSuite inventory locations (internal ids)
    const INV_LOCATION_COMBINED_WAREHOUSE = 1;
    const INV_LOCATION_COMBINED_QUARANTINE = 5;
    
    /** @var NetSuiteService */
    protected $netSuiteService;
    
    /** @var SearchStringFieldFactory */
    protected $searchStringFieldFactory;
    
    /** @var ItemSearchBasicFactory */
    protected $itemSearchBasicFactory;
    
    /** @var SearchRequestFactory */
    protected $searchRequestFactory;
    
    /** @var RecordRefFactory */
    protected $recordRefFactory;
    
    /** @var InventoryAdjustmentFactory */
    protected $inventoryAdjustmentFactory;
    
    /** @var InventoryAdjustmentInventoryFactory */
    protected $inventoryAdjustmentInventoryFactory;
    
    /** @var InventoryAdjustmentInventoryListFactory */
    protected $inventoryAdjustmentInventoryListFactory;
    
    /** @var AddRequestFactory */
    protected $addRequestFactory;
    
    /**
     * Inventory constructor.
     *
     * @param Context $context
     * @param SearchStringFieldFactory $searchStringFieldFactory
     * @param ItemSearchBasicFactory $itemSearchBasicFactory
     * @param SearchRequestFactory $searchRequestFactory
     * @param RecordRefFactory $recordRefFactory
     * @param InventoryAdjustmentFactory $inventoryAdjustmentFactory
     * @param InventoryAdjustmentInventoryFactory $inventoryAdjustmentInventoryFactory
     * @param InventoryAdjustmentInventoryListFactory $inventoryAdjustmentInventoryListFactory
     * @param AddRequestFactory $addRequestFactory
     */
    public function __construct(
        Context $context,
        SearchStringFieldFactory $searchStringFieldFactory,
        ItemSearchBasicFactory $itemSearchBasicFactory,
        SearchRequestFactory $searchRequestFactory,
        RecordRefFactory $recordRefFactory,
        InventoryAdjustmentFactory $inventoryAdjustmentFactory,
        InventoryAdjustmentInventoryFactory $inventoryAdjustmentInventoryFactory,
        InventoryAdjustmentInventoryListFactory $inventoryAdjustmentInventoryListFactory,
        AddRequestFactory $addRequestFactory
    )
    {
        $this->searchStringFieldFactory = $searchStringFieldFactory;
        $this->itemSearchBasicFactory = $itemSearchBasicFactory;
        $this->searchRequestFactory = $searchRequestFactory;
        $this->recordRefFactory = $recordRefFactory;
        $this->inventoryAdjustmentFactory = $inventoryAdjustmentFactory;
        $this->inventoryAdjustmentInventoryFactory = $inventoryAdjustmentInventoryFactory;
        $this->inventoryAdjustmentInventoryListFactory = $inventoryAdjustmentInventoryListFactory;
        $this->addRequestFactory = $addRequestFactory;
    
        parent::__construct($context);
    
        $nsConfig = array(
            "endpoint" => $this->scopeConfig->getValue('kurufootwear_netsuite/connector/endpoint'),
            "host" => $this->scopeConfig->getValue('kurufootwear_netsuite/connector/host'),
            "account" => $this->scopeConfig->getValue('kurufootwear_netsuite/connector/account'),
            "app_id" => $this->scopeConfig->getValue('kurufootwear_netsuite/connector/app_id'),
            "logging" => $this->scopeConfig->getValue('kurufootwear_netsuite/connector/logging') == '0' ? false : true,
            "log_path" => $this->scopeConfig->getValue('kurufootwear_netsuite/connector/log_path'),
            "consumerKey" => $this->scopeConfig->getValue('kurufootwear_netsuite/connector/consumer_key'),
            "consumerSecret" => $this->scopeConfig->getValue('kurufootwear_netsuite/connector/consumer_secret'),
            "token" => $this->scopeConfig->getValue('kurufootwear_netsuite/connector/token'),
            "tokenSecret" => $this->scopeConfig->getValue('kurufootwear_netsuite/connector/secret'),
        );
        
        // TODO: There's a bug in M@ that causes dependency injection of NetSuiteService to fail when the di is compiled.
        // TODO: Would be nice to figure this out sometime. Meanwhile we'll instantiate it ourselves.
        $this->netSuiteService = new NetSuiteService($nsConfig);
    }
    
    /**
     * Searches NetSuite for an inventory item with the given SKU.
     * Returns the NS internal ID for the item.
     * 
     * @param $sku
     *
     * @return null|int
     * @throws IntegrationException
     */
    public function searchInventoryItem($sku)
    {
        if (!$this->enabled()) {
            return null;
        }
        
        try {
            // Get the NS internal ID from the SKU
            $id = null;
            $field = $this->searchStringFieldFactory->create();
            $field->operator = 'is';
            $field->searchValue = $sku;
            $search = $this->itemSearchBasicFactory->create();
            $search->itemId = $field;
            $request = $this->searchRequestFactory->create();
            $request->searchRecord = $search;
            $response = $this->netSuiteService->search($request);
        
            if ($response->searchResult->status->isSuccess) {
                if (empty($response->searchResult->recordList->record)) {
                    // No result
                    return null;
                }
                // Return the first result
                return $response->searchResult->recordList->record[0]->internalId;
            }
            else {
                throw new IntegrationException($response->searchResult->status->statusDetail);
            }
        }
        catch(\Exception $e) {
            throw new IntegrationException(__('Error communicating with NetSuite'), $e);
        }
    }
    
    /**
     * Adds an inventory adjustment to NetSuite.
     * 
     * @param $account
     * @param $itemInternalId
     * @param $qtyToAdj
     * @param $bin
     * @param $location
     * @param string $memo
     *
     * @return bool
     * @throws IntegrationException
     */
    public function addInventoryAdjustment($account, $itemInternalId, $qtyToAdj, $bin, $location, $memo = '')
    {
        if (!$this->enabled()) {
            return false;
        }
        
        $invAdj = $this->inventoryAdjustmentFactory->create();
    
        $invAdj->memo = $memo;
        $invAdj->account = $this->recordRefFactory->create();
        $invAdj->account->internalId = $account;
        $invAdj->adjLocation = $this->recordRefFactory->create();
        $invAdj->adjLocation->internalId = 1;
    
        $invAdjInv = $this->inventoryAdjustmentInventoryFactory->create();
        $invAdjInv->item = $this->recordRefFactory->create();
        $invAdjInv->item->internalId = $itemInternalId;
        $invAdjInv->adjustQtyBy = $qtyToAdj;
        $invAdjInv->binNumbers = $bin;
        $invAdjInv->location = $this->recordRefFactory->create();
        $invAdjInv->location->internalId = $location;
    
        $invAdj->inventoryList = $this->inventoryAdjustmentInventoryListFactory->create();
        $invAdj->inventoryList->inventory[] = $invAdjInv;
    
        $request = $this->addRequestFactory->create();
        $request->record = $invAdj;
    
        $response = $this->netSuiteService->add($request);
        if ($response->writeResponse->status->isSuccess) {
            return true;
        }
        else {
            throw new IntegrationException($response->writeResponse->status->statusDetail);
        }
    }
    
    /**
     * @return bool
     */
    public function enabled()
    {
        return $this->scopeConfig->getValue('kurufootwear_netsuite/connector/enabled') == '1' ? true: false;
    }
}
