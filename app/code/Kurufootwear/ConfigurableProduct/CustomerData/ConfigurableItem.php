<?php
/**
 * @category    Kurufootwear
 * @package     Kurufootwear\ConfigurableProduct
 * @author      Aleksejs Baranovs <info@scandiweb.com>
 * @copyright   Copyright (c) 2018 Scandiweb, Inc (http://scandiweb.com)
 * @license     http://opensource.org/licenses/OSL-3.0 The Open Software License 3.0 (OSL-3.0)
 */

namespace Kurufootwear\ConfigurableProduct\CustomerData;

use Magento\ConfigurableProduct\CustomerData\ConfigurableItem as BaseConfigurableItem;
use Magento\Catalog\Model\Config\Source\Product\Thumbnail as ThumbnailSource;
use Magento\ConfigurableProduct\Block\Cart\Item\Renderer\Configurable;
use Magento\Store\Model\ScopeInterface;

/**
 * Class ConfigurableItem
 *
 * @package Kurufootwear\ConfigurableProduct\CustomerData
 */
class ConfigurableItem extends BaseConfigurableItem
{
    /**
     * Retrieve URL to item Product
     *
     * @return string
     */
    protected function getProductUrl()
    {
        if ($this->item->getRedirectUrl()) {
            return $this->item->getRedirectUrl();
        }

        $product = $this->item->getProduct();
        $option = $this->item->getOptionByCode('product_type');
        if ($option) {
            $product = $option->getProduct();
        }

        $params = [];

        if ($options = $this->getOptionList()) {
            foreach ($options as $option) {
                $attribute = $product->getResource()->getAttribute($option['option_id']);

                if (isset($attribute)) {
                    $params[$attribute->getAttributeCode()] = $option['value'];
                }
            }
        }

        return $product->getUrlModel()->getUrl($product, ['_query' => $params]);
    }

    /**
     * {@inheritdoc}
     */
    protected function getProductForThumbnail()
    {
        /**
         * Show parent product thumbnail if it must be always shown according to the related setting in system config
         * or if child thumbnail is not available
         *
         * Added check for product based on issue MAGETWO-71446
         * https://github.com/magento/magento2/pull/10483
         * Looks like it is merged in 2.3 version
         */
        $config = $this->_scopeConfig->getValue(
            Configurable::CONFIG_THUMBNAIL_SOURCE,
            ScopeInterface::SCOPE_STORE
        );

        $product = $config == ThumbnailSource::OPTION_USE_PARENT_IMAGE
        || !($this->getChildProduct()
            && $this->getChildProduct()->getThumbnail() && $this->getChildProduct()->getThumbnail() != 'no_selection')
            ? $this->getProduct()
            : $this->getChildProduct();

        return $product;
    }

    /**
     * Patch to get original product name from master product
     * instead of default name in simple product at checkout.
     *
     * @return array
     */
    protected function doGetItemData()
    {
        $imageHelper = $this->imageHelper->init($this->getProductForThumbnail(), 'mini_cart_product_thumbnail');

        return [
            'options' => $this->getOptionList(),
            'qty' => $this->item->getQty() * 1,
            'item_id' => $this->item->getId(),
            'configure_url' => $this->getConfigureUrl(),
            'is_visible_in_site_visibility' => $this->item->getProduct()->isVisibleInSiteVisibility(),
            'product_id' => $this->item->getProduct()->getId(),
            'product_name' => $this->getOriginalName(),
            'product_sku' => $this->item->getProduct()->getSku(),
            'product_url' => $this->getProductUrl(),
            'product_has_url' => $this->hasProductUrl(),
            'product_price' => $this->checkoutHelper->formatPrice($this->item->getCalculationPrice()),
            'product_price_value' => $this->item->getCalculationPrice(),
            'product_image' => [
                'src' => $imageHelper->getUrl(),
                'alt' => $imageHelper->getLabel(),
                'width' => $imageHelper->getWidth(),
                'height' => $imageHelper->getHeight(),
            ],
            'canApplyMsrp' => $this->msrpHelper->isShowBeforeOrderConfirm($this->item->getProduct())
                && $this->msrpHelper->isMinimalPriceLessMsrp($this->item->getProduct()),
        ];
    }

    /**
     * Get master product original name
     *
     * @return string
     */
    public function getOriginalName()
    {
        $product = $this->item->getProduct();

        if ($origName = $product->getOrigName()) {
            return $origName;
        } else {
            return $this->item->getName();
        }
    }
}
