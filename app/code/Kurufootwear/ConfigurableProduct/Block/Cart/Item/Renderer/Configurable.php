<?php
/**
 * @category     Kurufootwear
 * @package      Kurufootwear/ConfigurableProduct
 * @copyright    Copyright (c) 2018 Scandiweb, Inc (http://scandiweb.com)
 * @license      http://opensource.org/licenses/OSL-3.0 The Open Software License 3.0 (OSL-3.0)
 */
namespace Kurufootwear\ConfigurableProduct\Block\Cart\Item\Renderer;

use Magento\ConfigurableProduct\Block\Cart\Item\Renderer\Configurable as BaseConfigurable;
use Magento\Catalog\Model\Config\Source\Product\Thumbnail as ThumbnailSource;
use Magento\Store\Model\ScopeInterface;

/**
 * Class Configurable
 * @package Kurufootwear\ConfigurableProduct\Block\Cart\Item\Renderer
 */
class Configurable extends BaseConfigurable
{
    /**
     * Retrieve URL to item Product with params
     *
     * @return string
     */
    public function getProductUrl()
    {
        if ($this->_productUrl !== null) {
            return $this->_productUrl;
        }
        if ($this->getItem()->getRedirectUrl()) {
            return $this->getItem()->getRedirectUrl();
        }

        $product = $this->getProduct();
        $option = $this->getItem()->getOptionByCode('product_type');
        if ($option) {
            $product = $option->getProduct();
        }

        $params = [];

        if ($options = $this->getOptionList()) {
            foreach ($options as $option) {
                $attribute = $product->getResource()->getAttribute($option['option_id']);

                if (isset($attribute)) {
                    $params[$attribute->getAttributeCode()] = $option['value'];
                }
            }
        }

        return $product->getUrlModel()->getUrl($product, ['_query' => $params]);
    }

    /**
     * Get item product name
     *
     * @return string
     */
    public function getProductName()
    {
        if ($origName = $this->getProduct()->getOrigName()) {
            return $origName;
        }

        return $this->getProduct()->getName();
    }

    /**
     * {@inheritdoc}
     */
    public function getProductForThumbnail()
    {
        /**
         * Show parent product thumbnail if it must be always shown according to the related setting in system config
         * or if child thumbnail is not available
         *
         * Added check for product based on issue MAGETWO-71446
         * https://github.com/magento/magento2/pull/10483
         * Looks like it is merged in 2.3 version
         */
        if ($this->_scopeConfig->getValue(
                self::CONFIG_THUMBNAIL_SOURCE,
                ScopeInterface::SCOPE_STORE
            ) == ThumbnailSource::OPTION_USE_PARENT_IMAGE
            || !($this->getChildProduct()
                && $this->getChildProduct()->getThumbnail() && $this->getChildProduct()->getThumbnail() != 'no_selection')
        ) {
            $product = $this->getProduct();
        } else {
            $product = $this->getChildProduct();
        }
        return $product;
    }

    /**
     * This is needed to take the correct templates
     *
     * @return string
     */
    protected function _toHtml()
    {
        $this->setModuleName($this->extractModuleName('Magento\ConfigurableProduct\Block\Cart\Item\Renderer\Configurable'));

        return parent::_toHtml();
    }
}
