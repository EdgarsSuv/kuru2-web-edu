<?php
/**
 * @vendor Kurufootwear
 * @module Kurufootwear_ConfigurableProduct
 * @author Aleksejs Baranovs <info@scandiweb.com>
 * @copyright Copyright (c) 2018 KURU Footwear. All rights reserved.
 */

namespace Kurufootwear\ConfigurableProduct\Model\ResourceModel\Indexer\Stock;

use Magento\Catalog\Api\Data\ProductInterface;
use Magento\CatalogInventory\Model\ResourceModel\Indexer\Stock\DefaultStock;
use Magento\Catalog\Model\Product\Attribute\Source\Status as ProductStatus;
use Magento\Framework\DB\Select;

/**
 * Class Configurable
 * Override to reindex only enabled product (do not include disabled)
 * @TODO Should be removed after update to M2.2
 *
 * @package Kurufootwear\ConfigurableProduct\Model\ResourceModel\Indexer\Stock
 */
class Configurable extends DefaultStock
{
    /**
     * Get the select object for get stock status by configurable product ids
     *
     * @param int|array $entityIds
     * @param bool $usePrimaryTable use primary or temporary index table
     *
     * @return Select
     */
    protected function _getStockStatusSelect($entityIds = null, $usePrimaryTable = false)
    {
        $metadata = $this->getMetadataPool()->getMetadata(ProductInterface::class);
        $connection = $this->getConnection();
        $idxTable = $usePrimaryTable ? $this->getMainTable() : $this->getIdxTable();
        $select = parent::_getStockStatusSelect($entityIds, $usePrimaryTable);

        $select->reset(
            Select::COLUMNS
        )->columns(
            ['e.entity_id', 'cis.website_id', 'cis.stock_id']
        )->joinLeft(
            ['l' => $this->getTable('catalog_product_super_link')],
            'l.parent_id = e.' . $metadata->getLinkField(),
            []
        )->join(
            ['le' => $this->getTable('catalog_product_entity')],
            'le.entity_id = l.product_id',
            []
        // Override to reindex only enabled product (do not include disabled)
        )->joinInner(
            ['cei' => $this->getTable('catalog_product_entity_int')],
            'le.' . $metadata->getLinkField() . ' = cei.' . $metadata->getLinkField()
            . ' AND cei.attribute_id = ' . $this->_getAttribute('status')->getId()
            . ' AND cei.value = ' . ProductStatus::STATUS_ENABLED,
            []
        )->joinLeft(
            ['i' => $idxTable],
            'i.product_id = l.product_id AND cis.website_id = i.website_id AND cis.stock_id = i.stock_id',
            []
        )->columns(
            ['qty' => new \Zend_Db_Expr('0')]
        );
        
        $statusExpr = $this->getStatusExpression($connection);

        $optExpr = $connection->getCheckSql("le.required_options = 0", 'i.stock_status', 0);
        $stockStatusExpr = $connection->getLeastSql(["MAX({$optExpr})", "MIN({$statusExpr})"]);

        $select->columns(['status' => $stockStatusExpr]);

        if ($entityIds !== null) {
            $select->where('e.entity_id IN(?)', $entityIds);
        }

        return $select;
    }
}
