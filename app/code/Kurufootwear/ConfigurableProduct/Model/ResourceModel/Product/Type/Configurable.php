<?php
/**
 * @category    Kurufootwear
 * @package     Kurufootwear\ConfigurableProduct
 * @author      Krisjanis Veinbahs <info@scandiweb.com>
 * @copyright   Copyright (c) 2018 Scandiweb, Inc (http://scandiweb.com)
 * @license     http://opensource.org/licenses/OSL-3.0 The Open Software License 3.0 (OSL-3.0)
 */
namespace Kurufootwear\ConfigurableProduct\Model\ResourceModel\Product\Type;

use Magento\Catalog\Api\Data\ProductInterface;
use Magento\Catalog\Model\ResourceModel\Product\Relation;
use Magento\ConfigurableProduct\Model\ResourceModel\Product\Type\Configurable as MagentoTypeConfigurable;
use Magento\Framework\App\ObjectManager;
use Magento\Framework\EntityManager\MetadataPool;
use Magento\Framework\Model\ResourceModel\Db\Context;

/**
 * Class Configurable
 *
 * Override to improve getChildrenIds return structure
 * to reduce array manipulations to only one place
 * https://github.com/magento/magento2/issues/14061
 */
class Configurable extends MagentoTypeConfigurable
{
    /**
     * Catalog product relation
     *
     * @var Relation
     */
    protected $catalogProductRelation;

    /**
     * Product metadata pool
     *
     * @var MetadataPool
     */
    private $metadataPool;

    /**
     * Product entity link field
     *
     * @var string
     */
    private $productEntityLinkField;

    /**
     * Constants used for structure in
     * children/parent id mapping
     */
    const PARENT_KEYS = 'parent';
    const CHILD_KEYS = 'child';

    /**
     * Configurable constructor.
     *
     * @param Context $context
     * @param Relation $catalogProductRelation
     * @param null $connectionName
     */
    public function __construct(
        Context $context,
        Relation $catalogProductRelation,
        $connectionName = null
    ) {
        parent::__construct(
            $context,
            $catalogProductRelation,
            $connectionName
        );
    }

    /**
     * Retrieve children ids by parent id(s)
     *
     * @param int|array $parentId
     *
     * @return array
     */
    private function getChildrenFromParentRows($parentId)
    {
        $select = $this->getConnection()->select()->from(
            ['l' => $this->getMainTable()],
            ['product_id', 'parent_id']
        )->join(
            ['p' => $this->getTable('catalog_product_entity')],
            'p.' . $this->getProductEntityLinkField() . ' = l.parent_id',
            []
        )->join(
            ['e' => $this->getTable('catalog_product_entity')],
            'e.entity_id = l.product_id AND e.required_options = 0',
            []
        )->where(
            'p.entity_id IN (?)',
            $parentId
        );

        return $this->getConnection()->fetchAll($select);
    }

    /**
     * Retrieve two arrays of links from configurable to children
     * and vice versa. Two arrays have constant access keys.
     *
     * @param int|array $parentId
     *
     * @return array
     */
    public function mapParentChildrenIds($parentId)
    {
        $childrenParentRows = $this->getChildrenFromParentRows($parentId);

        $parentMap = [];
        $childrenMap = [];

        foreach ($childrenParentRows as $row) {
            $parentMap[$row['parent_id']][] = $row['product_id'];
            $childrenMap[$row['product_id']][] = $row['parent_id'];
        }

        return [
            self::CHILD_KEYS => $childrenMap,
            self::PARENT_KEYS => $parentMap
        ];
    }

    /**
     * Get product metadata pool
     *
     * @return MetadataPool
     */
    private function getMetadataPool()
    {
        if (!$this->metadataPool) {
            $this->metadataPool = ObjectManager::getInstance()
                ->get(MetadataPool::class);
        }
        return $this->metadataPool;
    }

    /**
     * Get product entity link field
     *
     * @return string
     */
    private function getProductEntityLinkField()
    {
        if (!$this->productEntityLinkField) {
            $this->productEntityLinkField = $this->getMetadataPool()
                ->getMetadata(ProductInterface::class)
                ->getLinkField();
        }
        return $this->productEntityLinkField;
    }
}
