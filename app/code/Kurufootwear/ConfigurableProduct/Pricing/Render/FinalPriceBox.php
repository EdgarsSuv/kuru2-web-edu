<?php
/**
 * @category     Kurufootwear
 * @package      Kurufootwear/ConfigurableProduct
 * @copyright    Copyright (c) 2018 Scandiweb, Inc (http://scandiweb.com)
 * @license      http://opensource.org/licenses/OSL-3.0 The Open Software License 3.0 (OSL-3.0)
 */
namespace Kurufootwear\ConfigurableProduct\Pricing\Render;

use Kurufootwear\Catalog\Helper\Product\TypeHelper;
use Magento\Catalog\Pricing\Price\FinalPrice;
use Magento\Catalog\Pricing\Price\RegularPrice;
use Magento\ConfigurableProduct\Pricing\Price\ConfigurableOptionsProviderInterface;
use Magento\ConfigurableProduct\Pricing\Price\LowestPriceOptionsProviderInterface;
use Magento\ConfigurableProduct\Pricing\Render\FinalPriceBox as MagentoFinalPriceBox;
use Magento\Framework\App\ObjectManager;
use Magento\Framework\Pricing\Price\PriceInterface;
use Magento\Framework\Pricing\Render\RendererPool;
use Magento\Framework\Pricing\SaleableInterface;
use Magento\Framework\View\Element\Template\Context;

class FinalPriceBox extends MagentoFinalPriceBox
{
    /**
     * @var TypeHelper
     */
    protected $kuruTypeHelper;

    /**
     * @var LowestPriceOptionsProviderInterface
     */
    private $lowestPriceOptionsProvider;

    /**
     * FinalPriceBox constructor.
     *
     * @param Context $context
     * @param TypeHelper $kuruTypeHelper
     * @param SaleableInterface $saleableItem
     * @param PriceInterface $price
     * @param RendererPool $rendererPool
     * @param ConfigurableOptionsProviderInterface $configurableOptionsProvider
     * @param array $data
     * @param LowestPriceOptionsProviderInterface|null $lowestPriceOptionsProvider
     *
     * @throws \RuntimeException
     */
    public function __construct(
        Context $context,
        TypeHelper $kuruTypeHelper,
        SaleableInterface $saleableItem,
        PriceInterface $price,
        RendererPool $rendererPool,
        ConfigurableOptionsProviderInterface $configurableOptionsProvider,
        array $data = [],
        LowestPriceOptionsProviderInterface $lowestPriceOptionsProvider = null
    ) {
        parent::__construct(
            $context,
            $saleableItem,
            $price,
            $rendererPool,
            $configurableOptionsProvider,
            $data,
            $lowestPriceOptionsProvider
        );

        $this->kuruTypeHelper = $kuruTypeHelper;
        $this->lowestPriceOptionsProvider = $lowestPriceOptionsProvider ?:
            ObjectManager::getInstance()->get(LowestPriceOptionsProviderInterface::class);
    }

    /**
     * Define if the special price should be shown
     *
     * @return bool
     */
    public function hasSpecialPrice()
    {
        if ($configurationProduct = $this->kuruTypeHelper->getPreselectedConfigurationProduct()
            && $this->getRequest()->getFullActionName() !== 'catalog_product_view') {
            $regularPrice = $configurationProduct->getPriceInfo()->getPrice(RegularPrice::PRICE_CODE)->getValue();
            $finalPrice = $configurationProduct->getPriceInfo()->getPrice(FinalPrice::PRICE_CODE)->getValue();

            if ($finalPrice < $regularPrice) {
                return true;
            }
        } else {
            return parent::hasSpecialPrice();
        }

        return false;
    }

    /**
     * Retrieve price object of given type and quantity
     *
     * @param string $priceCode
     *
     * @return PriceInterface
     */
    public function getPriceType($priceCode)
    {
        if ($configurationProduct = $this->kuruTypeHelper->getPreselectedConfigurationProduct()) {
            return $configurationProduct->getPriceInfo()->getPrice($priceCode);
        } else {
            return parent::getPriceType($priceCode);
        }
    }
}
