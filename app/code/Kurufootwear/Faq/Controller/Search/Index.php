<?php
/**
 * @category     Kurufootwear
 * @package      Kurufootwear/Faq
 * @copyright    Copyright (c) 2017 Scandiweb, Inc (http://scandiweb.com)
 * @license      http://opensource.org/licenses/OSL-3.0 The Open Software License 3.0 (OSL-3.0)
 */
namespace Kurufootwear\Faq\Controller\Search;

use Magento\Framework\App\Action\Context;
use Magento\Backend\Model\View\Result\Page;
use Magento\Framework\View\Result\PageFactory;
use Magento\Framework\Controller\Result\Redirect;
use Aheadworks\Faq\Model\Url;
use Aheadworks\Faq\Model\Config;
use Aheadworks\Faq\Api\SearchManagementInterface;
use Aheadworks\Faq\Api\ArticleRepositoryInterface;
use Magento\Store\Model\StoreManagerInterface;
use Magento\Framework\Controller\Result\ForwardFactory;
use Magento\Framework\Controller\Result\Forward;

class Index extends \Aheadworks\Faq\Controller\Search\Index
{
    /**
     * @var PageFactory
     */
    private $resultPageFactory;

    /**
     * @var ArticleRepositoryInterface
     */
    private $articleRepository;

    /**
     * @var SearchManagementInterface
     */
    private $searchManagementInterface;

    /**
     * @var Url
     */
    private $url;

    /**
     * @var ForwardFactory
     */
    private $resultForwardFactory;

    /**
     * Index constructor.
     * @param Url $url
     * @param Config $config
     * @param Context $context
     * @param PageFactory $resultPageFactory
     * @param ArticleRepositoryInterface $articleRepository
     * @param SearchManagementInterface $searchManagementInterface
     * @param ForwardFactory $resultForwardFactory
     * @param StoreManagerInterface $storeManager
     */
    public function __construct(
        Url $url,
        Config $config,
        Context $context,
        PageFactory $resultPageFactory,
        ArticleRepositoryInterface $articleRepository,
        SearchManagementInterface $searchManagementInterface,
        ForwardFactory $resultForwardFactory,
        StoreManagerInterface $storeManager
    ){
        $this->url = $url;
        $this->resultPageFactory = $resultPageFactory;
        $this->resultForwardFactory = $resultForwardFactory;
        $this->searchManagementInterface = $searchManagementInterface;

        parent::__construct(
            $url,
            $config,
            $context,
            $resultPageFactory,
            $articleRepository,
            $searchManagementInterface,
            $resultForwardFactory,
            $storeManager
        );
    }

    /**
     * View FAQ search results action
     *
     * @return Page|Redirect|Forward
     */
    public function _execute()
    {
        if (!$this->config->isFaqSearchEnabled()) {
            /** @var Forward $forward */
            $forward = $this->resultForwardFactory->create();
            return $forward->setModule('cms')->setController('noroute')->forward('index');
        }

        $searchQuery = $this->getRequest()->getParam(Url::FAQ_QUERY_PARAM);
        $prepareQuery = $this->prepareSearchQuery($searchQuery);

        if (!$prepareQuery || !$searchResults = $this->getArticles($prepareQuery)) {
            $routeName = $this->getRequest()->getParam('routeName');
            $this->messageManager->addErrorMessage(__('No articles found'));
            if ($routeName == $this->url->getSearchResultsPageRoute()) {
                /** @var Page $resultPage */
                $resultPage = $this->resultPageFactory->create();
                $resultPage->getLayout()->getBlock('aw_faq.search')->setData('data', $searchQuery);
                return $resultPage;
            }
            /** Redirect $resultRedirect */
            $resultRedirect = $this->resultRedirectFactory->create();
            return $resultRedirect->setPath($routeName);
        }

        /** @var Page $resultPage */
        $resultPage = $this->resultPageFactory->create();
        $resultPage->getLayout()->getBlock('breadcrumbs')
            ->addCrumb(
                'home',
                [
                    'label' => 'Home',
                    'title'=>__('Go to store homepage'),
                    'link'=> $this->url->getBaseUrl()
                ]
            )->addCrumb(
                'faq',
                [
                    'label' => __('FAQ'),
                    'title'=> __('FAQ'),
                    'link'=> $this->url->getFaqHomeUrl()
                ]
            )->addCrumb(
                'search',
                [
                    'label' => __('Search results for: "%1"', $searchQuery)
                ]
            );

        $resultPage->getLayout()->getBlock('aw_faq.search_results')->setData('data', $searchResults);
        $pageConfig = $resultPage->getConfig();
        $pageConfig->getTitle()->set(__('Search results for: "%1"', $searchQuery));
        $pageConfig->setMetadata('robots', 'noindex');

        return $resultPage;
    }


    /**
     * Retrieve all articles
     *
     * @param string $searchQuery
     * @return array
     */
    private function getArticles($searchQuery)
    {
        return $this->searchManagementInterface->searchArticles($searchQuery, $this->getCurrentStore())->getItems();
    }

    /**
     * Prepare Search Query for
     * safe using in database select
     *
     * @param string $searchQuery
     * @return string
     */
    private function prepareSearchQuery($searchQuery)
    {
        $query = preg_split('/[\s*\W*]/', strip_tags($searchQuery));
        $searchWords = [];

        foreach ($query as $word) {
            if (mb_strlen($word) > 2) {
                $searchWords[] = trim($word) . '*';
            }
        }

        return implode(' ', $searchWords);
    }
}
