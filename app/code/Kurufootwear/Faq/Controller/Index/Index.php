<?php
/**
 * @category     Kurufootwear
 * @package      Kurufootwear/Faq
 * @copyright    Copyright (c) 2017 Scandiweb, Inc (http://scandiweb.com)
 * @license      http://opensource.org/licenses/OSL-3.0 The Open Software License 3.0 (OSL-3.0)
 */
namespace Kurufootwear\Faq\Controller\Index;

use Magento\Framework\Controller\Result\ForwardFactory;
use Magento\Framework\Controller\Result\Forward;
use Magento\Framework\View\Result\PageFactory;
use Magento\Framework\App\Action\Context;
use Magento\Backend\Model\View\Result\Page;
use Aheadworks\Faq\Model\Config;
use Aheadworks\Faq\Model\Url;
use Magento\Store\Model\StoreManagerInterface;

class Index extends \Aheadworks\Faq\Controller\Index\Index
{
    /**
     * @var Url
     */
    private $url;

    /**
     * @var PageFactory
     */
    private $resultPageFactory;

    /**
     * @var ForwardFactory
     */
    private $resultForwardFactory;

    /**
     * Index constructor.
     * @param Url $url
     * @param Config $config
     * @param Context $context
     * @param PageFactory $resultPageFactory
     * @param StoreManagerInterface $storeManager
     * @param ForwardFactory $resultForwardFactory
     */
    public function __construct(
        Url $url,
        Config $config,
        Context $context,
        PageFactory $resultPageFactory,
        StoreManagerInterface $storeManager,
        ForwardFactory $resultForwardFactory
    ){
        $this->url = $url;
        $this->resultPageFactory = $resultPageFactory;
        $this->resultForwardFactory = $resultForwardFactory;

        parent::__construct(
            $url,
            $config,
            $context,
            $resultPageFactory,
            $storeManager,
            $resultForwardFactory
        );
    }

    /**
     * View FAQ homepage action
     *
     * @return Page|\Magento\Framework\Controller\Result\Redirect
     */
    public function _execute()
    {
        $path = explode('/', trim($this->getRequest()->getPathInfo(), '/'));
        if ($this->config->getFaqRoute() !== $path[0]) {
            /** @var Forward $forward */
            $forward = $this->resultForwardFactory->create();
            return $forward->setModule('cms')->setController('noroute')->forward('index');
        }

        /** @var Page $resultPage */
        $resultPage = $this->resultPageFactory->create();
        $pageConfig = $resultPage->getConfig();
        $pageConfig->getTitle()->set($this->config->getFaqMetaTitle());
        $pageConfig->setDescription($this->config->getFaqMetaDescription());
        $pageConfig->addBodyClass('aw-columns-' . $this->config->getDefaultNumberOfColumnsToDisplay());
        $resultPage->getLayout()->getBlock('breadcrumbs')
            ->addCrumb(
                'home',
                [
                    'label' => 'Home',
                    'title'=>__('Go to store homepage'),
                    'link'=> $this->url->getBaseUrl()
                ]
            )->addCrumb(
                'faq',
                [
                    'label' => __('FAQ')
                ]
            );

        return $resultPage;
    }
}
