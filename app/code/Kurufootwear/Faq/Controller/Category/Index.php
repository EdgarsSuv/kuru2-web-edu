<?php
/**
 * @category     Kurufootwear
 * @package      Kurufootwear/Faq
 * @copyright    Copyright (c) 2017 Scandiweb, Inc (http://scandiweb.com)
 * @license      http://opensource.org/licenses/OSL-3.0 The Open Software License 3.0 (OSL-3.0)
 */
namespace Kurufootwear\Faq\Controller\Category;

use Aheadworks\Faq\Model\Config;
use Magento\Framework\App\Action\Context;
use Magento\Framework\Controller\Result\Redirect;
use Magento\Framework\View\Result\PageFactory;
use Magento\Backend\Model\View\Result\Page;
use Aheadworks\Faq\Model\Url;
use Aheadworks\Faq\Model\Category;
use Aheadworks\Faq\Api\CategoryRepositoryInterface;
use Magento\Store\Model\StoreManagerInterface;
use Magento\Framework\Controller\Result\ForwardFactory;
use Magento\Framework\Controller\Result\Forward;

class Index extends \Aheadworks\Faq\Controller\Category\Index
{
    /**
     * @var PageFactory
     */
    private $resultPageFactory;

    /**
     * @var CategoryRepositoryInterface
     */
    private $categoryRepository;

    /**
     * @var Url
     */
    private $url;

    /**
     * @var ForwardFactory
     */
    private $resultForwardFactory;

    /**
     * Index constructor.
     * @param Url $url
     * @param Config $config
     * @param Context $context
     * @param PageFactory $resultPageFactory
     * @param CategoryRepositoryInterface $categoryRepository
     * @param ForwardFactory $resultForwardFactory
     * @param StoreManagerInterface $storeManager
     */
    public function __construct(
        Url $url,
        Config $config,
        Context $context,
        PageFactory $resultPageFactory,
        CategoryRepositoryInterface $categoryRepository,
        ForwardFactory $resultForwardFactory,
        StoreManagerInterface $storeManager
    ){
        $this->url = $url;
        $this->categoryRepository = $categoryRepository;
        $this->resultPageFactory = $resultPageFactory;
        $this->resultForwardFactory = $resultForwardFactory;

        parent::__construct(
            $url,
            $config,
            $context,
            $resultPageFactory,
            $categoryRepository,
            $resultForwardFactory,
            $storeManager
        );
    }

    /**
     * View FAQ category page action
     *
     * @return Page|Redirect|Forward
     */
    public function _execute()
    {
        $categoryId = $this->getRequest()->getParam('id');

        /** @var Category $category */
        $category = $this->categoryRepository->getById($categoryId);

        if (!$category->getIsEnable()) {
            /** @var Forward $forward */
            $forward = $this->resultForwardFactory->create();
            return $forward->setModule('cms')->setController('noroute')->forward('index');
        }

        if (!array_intersect($category->getStoreIds(), $this->getCurrentStores())) {
            return $this->redirectWithErrorMessage();
        }

        $metaTitle = $category->getMetaTitle() ? $category->getMetaTitle() : $category->getName();

        /** @var Page $resultPage */
        $resultPage = $this->resultPageFactory->create();
        $resultPage->getConfig()->getTitle()->set($metaTitle);
        $resultPage->getConfig()->setDescription($category->getMetaDescription());
        $resultPage->getLayout()->getBlock('breadcrumbs')
            ->addCrumb(
                'home',
                [
                    'label' => 'Home',
                    'title'=>__('Go to store homepage'),
                    'link'=> $this->url->getBaseUrl()
                ]
            )
            ->addCrumb(
                'faq',
                [
                    'label' => __('FAQ'),
                    'title'=>__('FAQ'),
                    'link'=> $this->url->getFaqHomeUrl()
                ]
            )->addCrumb(
                'category',
                [
                    'label' => $category->getName(),
                ]
            );

        return $resultPage;
    }
}
