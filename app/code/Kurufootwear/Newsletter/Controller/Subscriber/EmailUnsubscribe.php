<?php
/**
 * @vendor Kurufootwear
 * @module Kurufootwear_Newsletter
 * @author Aleksejs Baranovs <info@scandiweb.com>
 * @copyright Copyright (c) 2018 Scandiweb, Ltd (http://scandiweb.com)
 * @license http://opensource.org/licenses/afl-3.0.php Academic Free License (AFL 3.0)
 */

namespace Kurufootwear\Newsletter\Controller\Subscriber;

use Exception;
use Magento\Framework\Exception\LocalizedException;
use Magento\Framework\Exception\MailException;
use Magento\Newsletter\Controller\Subscriber;

/**
 * Class EmailUnsubscribe
 * @package Kurufootwear\Newsletter\Controller\Subscriber
 */
class EmailUnsubscribe extends Subscriber
{
    /**
     * Execute customer unsubscribe
     */
    public function execute()
    {
        if ($this->getRequest()->isPost() && $this->getRequest()->getPost('email')) {
            $email = (string) $this->getRequest()->getPost('email');

            try {
                $subscriber = $this->_subscriberFactory->create()->loadByEmail($email);

                if (!$subscriber->getId()) {
                    throw new LocalizedException(__('You are not subscribed to our newsletter.'));
                }

                $subscriber->unsubscribe();
                $this->messageManager->addSuccessMessage(__('You have been unsubscribed from our newsletter.'));
            } catch (LocalizedException $e) {
                $this->messageManager->addExceptionMessage($e, $e->getMessage());
            } catch (Exception $e) {
                $this->messageManager->addExceptionMessage($e, __('Something went wrong while unsubscribing you. Please contact support.'));
            } catch (MailException $e) {
                $this->messageManager->addExceptionMessage($e, __('Something went wrong while unsubscribing you. Please contact support.'));
            }
        }

        $this->getResponse()->setRedirect($this->_redirect->getRedirectUrl());
    }
}
