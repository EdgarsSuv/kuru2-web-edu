<?php
/**
 * @category    Kurufootwear
 * @package     Kurufootwear\Promo
 * @author      Jim McGowen <jim@kurufootwear.com>
 * @copyright   Copyright (c) 2018 KURU Footwear. All rights reserved.
 */

\Magento\Framework\Component\ComponentRegistrar::register(
    \Magento\Framework\Component\ComponentRegistrar::MODULE,
    'Kurufootwear_Promo',
    __DIR__
);