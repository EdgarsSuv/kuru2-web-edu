/**
 * @category    Kurufootwear
 * @package     Kurufootwear\Promo
 * @author      Jim McGowen <jim@kurufootwear.com>
 * @copyright   Copyright (c) 2018 KURU Footwear. All rights reserved.
 */

var config = {
    map: {
        "*": {
            'Amasty_Promo/js/popup': 'Kurufootwear_Promo/js/popup'
        }
    }
};
