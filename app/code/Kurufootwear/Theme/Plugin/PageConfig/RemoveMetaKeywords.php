<?php
/**
 * @vendor Kurufootwear
 * @module Kurufootwear_Theme
 * @author Krisjanis Veinbahs <krisjanisv@scandiweb.com>
 * @copyright Copyright (c) 2017 KURU Footwear. All rights reserved.
 * @license http://opensource.org/licenses/afl-3.0.php Academic Free License (AFL 3.0)
 */
namespace Kurufootwear\Theme\Plugin\PageConfig;

class RemoveMetaKeywords
{
    /**
     * Never return meta keywords (SEO optimization)
     *
     * @param $subject
     * @param string $return
     * @return string
     */
    public function afterGetKeywords($subject, $return)
    {
        return (string)null;
    }
}