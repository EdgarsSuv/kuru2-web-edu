<?php
/**
 * @vendor Kurufootwear
 * @module Kurufootwear_CatalogUrlRewrite
 * @author Kristaps Stalidzāns <info@scandiweb.com>
 * @copyright Copyright (c) 2017 Scandiweb, Ltd (http://scandiweb.com)
 * @license http://opensource.org/licenses/afl-3.0.php Academic Free License (AFL 3.0)
 */
namespace Kurufootwear\CatalogUrlRewrite\Model;

class ProductUrlPathGenerator extends \Magento\CatalogUrlRewrite\Model\ProductUrlPathGenerator
{
    /**
     * Retrieve Product Url path with suffix
     *
     * @param \Magento\Catalog\Model\Product $product
     * @param int $storeId
     * @param \Magento\Catalog\Model\Category $category
     * @return string
     */
    public function getUrlPathWithSuffix($product, $storeId, $category = null)
    {
        $string = explode('-' . strtolower($product->getSku()), $this->getUrlPath($product, $category));

        if (is_array($string)) {
            return $string[0] . $this->getProductUrlSuffix($storeId);
        } else {
            return $this->getUrlPath($product, $category) . $this->getProductUrlSuffix($storeId);
        }
    }
}
