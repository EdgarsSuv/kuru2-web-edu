<?php
/**
 * @category     Kurufootwear
 * @package      Kurufootwear/Label
 * @author       Jim McGowen <jim@kurufootwear.com>
 * @copyright    Copyright (c)2018 KURU Footwear
 */

namespace Kurufootwear\Label\Plugin\Catalog\Product\View;

/**
 * Class Label
 * 
 * @package Kurufootwear\Label\Plugin\Catalog\Product\View
 */
class Label extends \Amasty\Label\Plugin\Catalog\Product\View\Label
{
    /**
     * Overridden to not do anything. The labels will be drawn in the callback 
     * for the media ajax calls.
     * 
     * See Kurufootwear\Swatches\Controller\Ajax\Media and
     * swatch-renderer.js::_ProductMediaCallback
     * 
     * @param \Magento\Catalog\Block\Product\View\Gallery $subject
     * @param $result
     *
     * @return string
     */
    public function afterToHtml(
        \Magento\Catalog\Block\Product\View\Gallery $subject,
        $result
    ) {
        return $result;
    }
}
