<?php
/**
 * @category     Kurufootwear
 * @package      Kurufootwear/Label
 * @author       Jim McGowen <jim@kurufootwear.com>
 * @copyright    Copyright (c)2018 KURU Footwear
 */

namespace Kurufootwear\Label\Block;

/**
 * Overridden to override the template for this block.
 * 
 * Class Label
 * @package Kurufootwear\Label\Block
 */
class Label extends \Amasty\Label\Block\Label
{
    /**
     * Label constructor.
     *
     * @param \Magento\Framework\View\Element\Template\Context $context
     * @param \Amasty\Label\Helper\Data $helper
     * @param array $data
     */
    public function __construct(
        \Magento\Framework\View\Element\Template\Context $context,
        \Amasty\Label\Helper\Data $helper,
        array $data = []
    )
    {
        parent::__construct($context, $helper, $data);
        $this->setTemplate('Kurufootwear_Label::label.phtml');
    }
}
