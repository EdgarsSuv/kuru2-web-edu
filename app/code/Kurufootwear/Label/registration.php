<?php
/**
 * @category     Kurufootwear
 * @package      Kurufootwear/Label
 * @author       Jim McGowen <jim@kurufootwear.com>
 * @copyright    Copyright (c) 201KURU Footwear
 */

\Magento\Framework\Component\ComponentRegistrar::register(
    \Magento\Framework\Component\ComponentRegistrar::MODULE,
    'Kurufootwear_Label',
    __DIR__
);
