<?php
/**
 * @vendor      Kurufootwear
 * @module      Kurufootwear_Contact
 * @author      Kristaps Stalidzans <info@scandiweb.com>
 * @copyright   Copyright (c) 2017 Scandiweb. All rights reserved.
 * @license     http://opensource.org/licenses/afl-3.0.php Academic Free License (AFL 3.0)
 */
namespace Kurufootwear\Contact\Block;

use Magento\Store\Model\ScopeInterface;
use Magento\Framework\View\Element\Template;
use Magento\Framework\App\Config\ScopeConfigInterface;

/**
 * Main contact form block
 */
class ContactForm extends Template
{
    /**
     * @var ScopeConfigInterface
     */
    protected $scopeConfig;

    /**
     * @param Template\Context $context
     * @param array $data
     */
    public function __construct(
        Template\Context $context,
        array $data = []
    ) {
        parent::__construct($context, $data);
        $this->_isScopePrivate = true;
        $this->scopeConfig = $context->getScopeConfig();
    }

    /**
     * Returns store phone number set in Stores->System->General->Store Information->Store Phone Number
     *
     * @return string
     */
    public function getPhone()
    {
        return $this->_scopeConfig->getValue(
            'general/store_information/phone',
            ScopeInterface::SCOPE_STORE
        );
    }

    /**
     * Returns store phone number set in Stores->System->General->Store Information->Store Hours of Operation
     *
     * @return string
     */
    public function getHours()
    {
        return $this->_scopeConfig->getValue(
            'general/store_information/hours',
            ScopeInterface::SCOPE_STORE
        );
    }

    /**
     * Returns action url for contact form
     *
     * @return string
     */
    public function getFormAction()
    {
        return $this->getUrl('contactKuru/index/post', ['_secure' => true]);
    }
}
