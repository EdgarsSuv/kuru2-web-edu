<?php
/**
 * @vendor      Kurufootwear
 * @module      Kurufootwear_Contact
 * @author      Jelena Sorohova <info@scandiweb.com>
 * @copyright   Copyright (c) 2017 Scandiweb. All rights reserved.
 * @license     http://opensource.org/licenses/afl-3.0.php Academic Free License (AFL 3.0)
 */
namespace Kurufootwear\Contact\Block;

use Kurufootwear\Zopim\Helper\Data;
use Magento\Framework\UrlInterface;
use Magento\Framework\View\Element\Template;
use Magento\Framework\App\Config\ScopeConfigInterface;
use Magento\Store\Model\ScopeInterface;
use Magento\Directory\Model\RegionFactory;
use Magento\Framework\App\Http\Context as HttpContext;
use Magento\Customer\Model\Context as CustomerContext;

class ContactInfo extends Template
{
    /**
     * @var ScopeConfigInterface
     */
    protected $scopeConfig;

    /**
     * @var UrlInterface
     */
    protected $urlInterface;

    /**
     * @var RegionFactory
     */
    protected $regionFactory;

    /**
     * @var HttpContext
     */
    protected $httpContext;

    /**
     * @var Data
     */
    protected $zopimHelper;

    /**
     * ContactInfo constructor.
     *
     * @param Template\Context $context
     * @param HttpContext $httpContext
     * @param RegionFactory $regionFactory
     * @param Data $zopimHelper
     * @param array $data
     */
    public function __construct(
        Template\Context $context,
        HttpContext $httpContext,
        RegionFactory $regionFactory,
        Data $zopimHelper,
        array $data = []
    ){
        parent::__construct($context, $data);

        $this->httpContext = $httpContext;
        $this->_scopeConfig = $context->getScopeConfig();
        $this->urlInterface = $context->getUrlBuilder();
        $this->regionFactory = $regionFactory;
        $this->zopimHelper = $zopimHelper;
    }

    /**
     * Encodes passed email to protect it from bots
     *
     * @param $email
     * @return string
     */
    protected function encodeEmail($email)
    {
        $output = '';

        for ($i = 0; $i < strlen($email); $i++)
        {
            $output .= '&#'.ord($email[$i]).';';
        }

        return $output;
    }

    /**
     * Returns store phone number set in Stores->System->General->Store Information->Store Phone Number
     *
     * @return string
     */
    public function getPhone()
    {
        return $this->_scopeConfig->getValue(
            'general/store_information/phone',
            ScopeInterface::SCOPE_STORE
        );
    }

    /**
     * Returns store email set in Stores->System->General->Store Information->E-mail
     *
     * @return string
     */
    public function getEmail()
    {
        return $this->encodeEmail($this->_scopeConfig->getValue(
            'general/store_information/email',
            ScopeInterface::SCOPE_STORE
        ));
    }

    /**
     * Returns store plain phone number set in Stores->System->General->Store Information->Plain Phone Number
     *
     * @return string
     */
    public function getPlainPhone()
    {
        return $this->_scopeConfig->getValue(
            'general/store_information/plain_phone',
            ScopeInterface::SCOPE_STORE
        );
    }

    /**
     * Returns store phone number set in Stores->System->General->Store Information->Store Hours of Operation
     *
     * @return string
     */
    public function getHours()
    {
        return $this->_scopeConfig->getValue(
            'general/store_information/hours',
            ScopeInterface::SCOPE_STORE
        );
    }

    /**
     * Returns store phone number set in Stores->System->General->Store Information->Store Address Coordinates
     *
     * @return string
     */
    public function getCoordinates()
    {
        return $this->_scopeConfig->getValue(
            'general/store_information/coordinates',
            ScopeInterface::SCOPE_STORE
        );
    }

    /**
     * Returns store phone number set in Stores->System->General->Store Information->Street Address
     *
     * @return string
     */
    public function getAddress()
    {
        return $this->_scopeConfig->getValue(
            'general/store_information/street_line1',
            ScopeInterface::SCOPE_STORE
        );
    }

    /**
     * Returns store phone number set in Stores->System->General->Store Information->City
     *
     * @return string
     */
    public function getCity()
    {
        return $this->_scopeConfig->getValue(
            'general/store_information/city',
            ScopeInterface::SCOPE_STORE
        );
    }

    /**
     * Returns store phone number set in Stores->System->General->Store Information->Region/State
     *
     * @return string
     */
    public function getState()
    {
        $regionId = $this->_scopeConfig->getValue(
            'general/store_information/region_id',
            ScopeInterface::SCOPE_STORE
        );

        return $this->regionFactory->create()->load($regionId)->getName();
    }

    /**
     * Returns Url
     *
     * @param $url
     * @return string
     */
    public function getMedia($url)
    {
        return $this->urlInterface->getBaseUrl(['_type' => UrlInterface::URL_TYPE_MEDIA]) . $url;
    }

    /**
     * Returns google api key
     *
     * @return string
     */
    public function getApiKey()
    {
        return $this->_scopeConfig->getValue(
            'general/store_information/googleapikey',
            ScopeInterface::SCOPE_STORE
        );
    }

    /**
     * Check if customer is logged in
     *
     * @return bool
     */
    public function isLoggedIn()
    {
        return $this->httpContext->getValue(CustomerContext::CONTEXT_AUTH);
    }

    /**
     * Get help bar offline text
     *
     * @return string
     */
    public function getHelpBarOfflineText()
    {
        return $this->zopimHelper->getHelpBarOfflineText();
    }
}
