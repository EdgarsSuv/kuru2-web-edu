<?php
/**
 * @category    Kurufootwear
 * @package     Kurufootwear\DiscountCode
 * @author      Jim McGowen <jim@kurufootwear.com>
 * @copyright   Copyright (c) 2018 KURU Footwear. All rights reserved.
 */

namespace Kurufootwear\DiscountCode\Controller\Cart;

use Magento\Checkout\Controller\Cart\Index as MagentoCheckoutIndex;
use Magento\Framework\App\Action\Context;
use Magento\Framework\App\Config\ScopeConfigInterface;
use Magento\Checkout\Model\Session;
use Magento\Store\Model\StoreManagerInterface;
use Magento\Framework\Data\Form\FormKey\Validator;
use Magento\Checkout\Model\Cart;
use Magento\Framework\View\Result\PageFactory;
use Magento\Framework\Escaper;
use Kurufootwear\DiscountCode\Helper\CodeHelper;
use Magento\Framework\View\Result\Page;

/**
 * Class Index
 * @package Kurufootwear\DiscountCode\Controller\Cart
 */
class Index extends MagentoCheckoutIndex
{
    /** @var \Magento\Framework\Escaper */
    private $escaper;
    
    /** @var CodeHelper */
    private $codeHelper;
    
    /**
     * Index constructor.
     *
     * @param Context $context
     * @param ScopeConfigInterface $scopeConfig
     * @param Session $checkoutSession
     * @param StoreManagerInterface $storeManager
     * @param Validator $formKeyValidator
     * @param Cart $cart
     * @param PageFactory $resultPageFactory
     * @param Escaper $escaper
     * @param CodeHelper $codeHelper
     */
    public function __construct(
        Context $context,
        ScopeConfigInterface $scopeConfig,
        Session $checkoutSession,
        StoreManagerInterface $storeManager,
        Validator $formKeyValidator,
        Cart $cart,
        PageFactory $resultPageFactory,
        Escaper $escaper,
        CodeHelper $codeHelper
    ) {
        $this->escaper = $escaper;
        $this->codeHelper = $codeHelper;
        parent::__construct($context, $scopeConfig, $checkoutSession, $storeManager, $formKeyValidator, $cart, $resultPageFactory);
    }
    
    /**
     * Overridden to validate the coupon code and show a message.
     *
     * @return Page
     */
    public function execute()
    {
        $resultPage = parent::execute();
        
        // Check the session for a coupon code
        // TODO: No longer storing the coupon code in the session
        $itemCount = $this->_checkoutSession->getQuote()->getItemsCount();
        $code = $this->_checkoutSession->getCouponCode();
        if (!empty($code) && $itemCount > 0) {
            $valid = $this->codeHelper->codeIsValid($code);
            if ($valid) {
                $this->messageManager->addSuccessMessage(__('The discount code "%1" has been applied.', $this->escaper->escapeHtml($code)));
            }
            else {
                $this->messageManager->addErrorMessage(__('The discount code "%1" is not valid.', $this->escaper->escapeHtml($code)));
            }
        }
        
        return $resultPage;
    }
}
