<?php
/**
 * @category    Kurufootwear
 * @package     Kurufootwear\DiscountCode
 * @author      Jim McGowen <jim@kurufootwear.com>
 * @copyright   Copyright (c) 2018 KURU Footwear. All rights reserved.
 */

namespace Kurufootwear\DiscountCode\Controller\Cart;

use Magento\Checkout\Controller\Cart\CouponPost as MagentoCheckoutCouponPost;
use Magento\Framework\Controller\Result\Redirect;

/**
 * Class CouponPost
 * @package Kurufootwear\DiscountCode\Controller\Cart
 */
class CouponPost extends MagentoCheckoutCouponPost
{
    /**
     * Overridden to handle the coupon code remove button.
     * Removes the code from the session and the quote.
     *
     * @return Redirect
     */
    public function execute()
    {
        // Was the cancel button clicked?
        if ($this->getRequest()->getParam('remove') == 1) {
            // Remove the code from the session
            $this->_checkoutSession->setCouponCode(null);
        }
        
        // Parent removes from quote
        parent::execute();
        
        // Redirect in case the code is in the current url so it doesn't get re-added
        $resultRedirect = $this->resultRedirectFactory->create();
        $resultRedirect->setPath('*/*', ['_current' => false]);

        return $resultRedirect;
    }
}
