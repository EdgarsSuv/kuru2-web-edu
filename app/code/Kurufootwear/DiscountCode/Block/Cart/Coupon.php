<?php
/**
 * @category    Kurufootwear
 * @package     Kurufootwear\DiscountCode
 * @author      Jim McGowen <jim@kurufootwear.com>
 * @copyright   Copyright (c) 2018 KURU Footwear. All rights reserved.
 */

namespace Kurufootwear\DiscountCode\Block\Cart;

use Magento\Checkout\Block\Cart\Coupon as MagentoCheckoutCoupon;

/**
 * Class Coupon
 * @package Kurufootwear\DiscountCode\Block\Cart
 */
class Coupon extends MagentoCheckoutCoupon
{
    /**
     * Returns the coupon code in the quote or the session.
     * 
     * @return string
     */
    public function getCouponCode()
    {
        $couponCode = $this->getQuote()->getCouponCode();
        if (empty($couponCode)) {
            $couponCode = $this->_checkoutSession->getCouponCode();
            if (empty($couponCode)) {
                $couponCode = '';
            }
        }
        return $couponCode;
    }
    
    /**
     * Whether or not to hide the coupon code block.
     * 
     * @return bool
     */
    public function hideCouponCode()
    {
        return empty($this->getCouponCode());
    }
    
    /**
     * Workaround for a bug in Magento: https://magento.stackexchange.com/questions/115988/are-we-forced-to-rewrite-a-template-in-magento2-when-rewritting-a-block
     * @return string
     */
    protected function _toHtml()
    {
        if (!$this->hideCouponCode()) {
            $this->setModuleName($this->extractModuleName('Magento\Checkout\Block\Cart\Coupon'));
    
            return parent::_toHtml();
        }
        
        return '';
    }
}
