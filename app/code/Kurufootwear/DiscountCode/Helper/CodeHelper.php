<?php
/**
 * @category    Kurufootwear
 * @package     Kurufootwear\DiscountCode
 * @author      Jim McGowen <jim@kurufootwear.com>
 * @copyright   Copyright (c) 2018 KURU Footwear. All rights reserved.
 */

namespace Kurufootwear\DiscountCode\Helper;

use Magento\Checkout\Model\Cart;
use Magento\SalesRule\Model\Coupon;

/**
 * Class CodeHelper
 * @package Kurufootwear\DiscountCode\Helper
 */
class CodeHelper
{
    /** @var Cart */
    protected $cart;
    
    /** @var Coupon */
    protected $coupon;
    
    /**
     * CodeHelper constructor.
     *
     * @param Cart $cart
     * @param Coupon $coupon
     */
    public function __construct(
        Cart $cart,
        Coupon $coupon
    ) {
        $this->cart = $cart;
        $this->coupon = $coupon;
    }
    
    /**
     * Check that the coupon code is valid (exists and is not expired).
     * 
     * @param $couponCode
     *
     * @return bool
     */
    public function codeIsValid($couponCode)
    {
        $rule = $this->coupon->loadByCode($couponCode);
        if (!$rule->hasData()) {
            // TODO: check expiration etc.
            return false;
        }
        
        return true;
    }
    
    /**
     * Adds the coupon code to the current quote. If the quote does not 
     * exist yet, it will be created here.
     * 
     * Note that if the code is not valid it will not 
     * be saved in the quote so $quote->getCouponCode() will return 
     * null in that case.
     * 
     * @param $couponCode
     */
    public function addCouponCodeToQuote($couponCode)
    {
        $quote = $this->cart->getQuote();
        if ($quote->getCouponCode() !== $couponCode) {
            $quote->setCouponCode($couponCode);
            $quote->collectTotals();
            $this->cart->saveQuote();
        }
    }
    
    /**
     * Returns the coupon code that is saved in the current quote.
     * 
     * @return string
     */
    public function getCouponCodeFromQuote()
    {
        $quote = $this->cart->getQuote();
        
        return $quote->getCouponCode();
    }
}
