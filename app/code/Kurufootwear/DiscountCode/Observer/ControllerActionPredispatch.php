<?php
/**
 * @category    Kurufootwear
 * @package     Kurufootwear\DiscountCode
 * @author      Jim McGowen <jim@kurufootwear.com>
 * @copyright   Copyright (c) 2018 KURU Footwear. All rights reserved.
 */

namespace Kurufootwear\DiscountCode\Observer;

use Magento\Framework\Event\Observer;
use Magento\Framework\Event\ObserverInterface;
use Magento\Framework\App\RequestInterface;
use Magento\Checkout\Model\Session;
use Kurufootwear\DiscountCode\Helper\CodeHelper;

/**
 * Class ControllerActionPredispatch
 * @package Kurufootwear\DiscountCode\Observer
 */
class ControllerActionPredispatch implements ObserverInterface
{
    /** @var RequestInterface */
    private $request;
    
    /** @var Session */
    private $session;
    
    /** @var CodeHelper */
    private $codeHelper;
    
    /**
     * ControllerActionPredispatch constructor.
     *
     * @param RequestInterface $request
     * @param Session $session
     * @param CodeHelper $codeHelper
     */
    public function __construct(
        RequestInterface $request,
        Session $session,
        CodeHelper $codeHelper
    ) {
        $this->request = $request;
        $this->session = $session;
        $this->codeHelper = $codeHelper;
    }
    
    /**
     * Checks the URL for a coupon code and adds it to the session 
     * and quote if there is one.
     * 
     * @param Observer $observer
     *
     * @return void
     */
    public function execute(\Magento\Framework\Event\Observer $observer)
    {
        // Check the URI for a coupon code
        $code = $this->request->getParam('code');
        if (!empty($code) && $this->codeHelper->codeIsValid($code)) {
            $this->codeHelper->addCouponCodeToQuote($code);
            
            // Also store it in the session so we can check it's validity 
            // on the cart page. If it's not valid it's not saved in the quote.
            $this->session->setCouponCode($code);
        }
        else {
            // Check the session for a code
            $code = $this->session->getCouponCode();
            if (!empty($code) &&
                $this->codeHelper->getCouponCodeFromQuote() !== $code &&
                $this->codeHelper->codeIsValid($code)
            ) {
                $this->codeHelper->addCouponCodeToQuote($code);
            }
        }
    }
}
