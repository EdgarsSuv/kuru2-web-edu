<?xml version="1.0"?>
<config xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:noNamespaceSchemaLocation="urn:magento:module:Magento_Config:etc/system_file.xsd">
    <system>
        <section id="weltpixel_maxmind_config" translate="label" type="text" sortOrder="200" showInDefault="1" showInWebsite="1" showInStore="1">
            <label>Maxmind</label>
            <tab>weltpixel</tab>
            <resource>WeltPixel_Maxmind::maxmind_settings</resource>

            <group id="general" translate="label" type="text" sortOrder="10" showInDefault="1" showInWebsite="1" showInStore="1">
                <comment><![CDATA[
                    <div style='border: 2px solid #DDDDDD; padding:4px; margin:4px; background-color:white'>

                      <ul style="padding:5px">
                        <p>The minFraud service reduces chargebacks by identifying risky orders to be held for further review. The minFraud service is used to identify fraud in online e-commerce transactions.</p>
                        <p>The riskScore, returned by the minFraud service, represents the likelihood that a given transaction is fraudulent. Merchants use the riskScore to determine whether to accept, reject, manually review, or submit transactions to complementary services for further screening.</p>
                        <p>The riskScore is given as a percentage, and as such it ranges from 0.01 to 100.00. For example, an order with a riskScore of 20.00 has a 20% chance of being fraudulent, while an order with a riskScore of 0.10 has a 0.1% chance of being fraudulent.</p>
                        <p>The riskScore is based on a statistical analysis of the following: reputations and real-time monitoring (IP addresses, Devices, Email addresses), Geolocation checks, Proxy detection, Bank Identification Number checks, The minFraud Network.</p>
                        <p>There is no single recommended set of riskScore values to use for deciding whether to accept, reject, manually review, or submit transactions to complementary services for analysis. In determining what thresholds to set, you should consider the costs of chargebacks and lost goods, the cost of manual review, the cost of complementary services, and the cost of potentially rejecting good orders</p>
                        <p><strong>A recommended strategy is to at first only automatically accept orders under a low riskScore (e.g., 3.00), only automatically reject orders above a high riskScore (e.g., 70.00), and manually review all other transactions. After monitoring the riskScores received for the manually reviewed transactions, you can adjust the thresholds appropriately to reduce the amount of manual review required.</strong></p>
                        <p>Below is the distribution of riskScores returned by the minFraud service across all users. You can use this data to estimate the number of orders that will be approved, rejected, or held back for review given the thresholds you set. Please note that the distribution of riskScores you observe may differ.</p>
                      </ul>

                      <table class="table table-bordered table-striped" style="-moz-border-bottom-colors: none;-moz-border-left-colors: none;-moz-border-right-colors: none;-moz-border-top-colors: none;border-collapse: separate;border-color: #DDDDDD #DDDDDD #DDDDDD #DDDDDD;border-image: none;border-radius: 4px 4px 4px 4px;border-style: solid solid solid solid;border-width: 1px 1px 1px 1px;line-height: 20px;border-spacing: 0;width: 480px;">
                        <thead>
                            <tr>
                                <th style="border-top-left-radius: 4px; padding: 8px;text-align: left;">riskScore range</th>
                                <th style="border-left: 1px solid #DDDDDD; padding: 8px;text-align: left;">Percent of orders in range</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr >
                                <td style="padding: 8px;text-align: left;border-top: 1px solid #DDDDDD;background-color: #D9DDD2;">0.10 - 4.99</td>
                                <td style="border-left: 1px solid #DDDDDD; padding: 8px;text-align: left;border-top: 1px solid #DDDDDD;background-color: #D9DDD2;">90%</td>
                            </tr>
                            <tr>
                                <td style="padding: 8px;text-align: left;border-top: 1px solid #DDDDDD;">5.00 - 9.99</td>
                                <td style="border-left: 1px solid #DDDDDD; padding: 8px;text-align: left;border-top: 1px solid #DDDDDD;">5%</td>
                            </tr>
                            <tr>
                                <td style="padding: 8px;text-align: left;border-top: 1px solid #DDDDDD;background-color: #D9DDD2;">10.00 - 29.99</td>
                                <td style="border-left: 1px solid #DDDDDD; padding: 8px;text-align: left;border-top: 1px solid #DDDDDD;background-color: #D9DDD2;">3%</td>
                            </tr>
                            <tr>
                                <td style="padding: 8px;text-align: left;border-top: 1px solid #DDDDDD;">30.00 - 99.99</td>
                                <td style="border-left: 1px solid #DDDDDD; padding: 8px;text-align: left;border-top: 1px solid #DDDDDD;">2%</td>
                            </tr>
                        </tbody>
                      </table>
                    </div> ]]>
                </comment>

                <label>General Configuration</label>
                <field id="enable" translate="label" type="select" sortOrder="10" showInDefault="1" showInWebsite="1" showInStore="1">
                    <label>Enable</label>
                    <source_model>Magento\Config\Model\Config\Source\Yesno</source_model>
                </field>
                <field id="score_threshold" translate="label" type="text" sortOrder="20" showInDefault="1" showInWebsite="1" showInStore="1">
                    <label>Score Threshold</label>
                </field>
                <field id="hold_order" translate="label" type="select" sortOrder="30" showInDefault="1" showInWebsite="1" showInStore="1">
                    <label>Hold Order</label>
                    <source_model>Magento\Config\Model\Config\Source\Yesno</source_model>
                    <comment>Hold Order when over threshold.</comment>
                </field>
                <field id="force_ip" translate="label" type="text" sortOrder="40" showInDefault="1" showInWebsite="1" showInStore="1">
                    <label>Force IP</label>
                    <comment>For debug only - This IP will overwrite the order IP address.</comment>
                </field>
                <field id="ip_exceptions" translate="label" type="textarea" sortOrder="40" showInDefault="1" showInWebsite="1" showInStore="1">
                    <label>IP Exceptions</label>
                    <comment>Add IP's separated by comma - Orders placed from this IP's will not be checked for possible fraud.</comment>
                </field>
                <field id="min_order_amount" translate="label" type="text" sortOrder="50" showInDefault="1" showInWebsite="1" showInStore="1">
                    <label>Minimum Order Amount</label>
                    <comment>Minimum amount for order to be checked for possible fraud.</comment>
                </field>
            </group>

            <group id="connection" translate="label" type="text" sortOrder="20" showInDefault="1" showInWebsite="1" showInStore="1">
                <label>Connection Configuration</label>
                <field id="api_server" translate="label" type="select" sortOrder="10" showInDefault="1" showInWebsite="1" showInStore="1">
                    <label>Api Hostname </label>
                    <source_model>WeltPixel\Maxmind\Model\Config\Source\Api</source_model>
                    <comment>Maxmind automatically picks the data center geographically closest to you. In some cases, this data center may not be the one that provides you with the best service. You can explicitly try the following hostnames to see which one provides the best performance for you.</comment>
                </field>
                <field id="license_key" translate="label" type="text" sortOrder="20" showInDefault="1" showInWebsite="1" showInStore="1">
                    <label>MaxMind License Key</label>
                </field>
                <field id="disable_certificate_check" translate="label" type="select" sortOrder="30" showInDefault="1" showInWebsite="1" showInStore="1">
                    <label>Disable cURL Server Certificate Check</label>
                    <source_model>Magento\Config\Model\Config\Source\Yesno</source_model>
                    <comment>For temporary server certificate issue - Set cURL to accept any server(peer) certificate.</comment>
                </field>
            </group>

        </section>
    </system>
</config>
