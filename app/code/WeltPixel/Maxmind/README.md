# m2-weltpixel-maxmind

### Installation

Dependencies:
 - m2-weltpixel-backend

With composer:

```sh
$ composer config repositories.welpixel-m2-weltpixel-maxmind git git@github.com:rusdragos/m2-weltpixel-maxmind.git
$ composer require weltpixel/m2-weltpixel-maxmind:dev-master
```

Manually:

Copy the zip into app/code/WeltPixel/Maxmind directory


#### After installation by either means, enable the extension by running following commands:

```sh
$ php bin/magento module:enable WeltPixel_Maxmind --clear-static-content
$ php bin/magento setup:upgrade
```
