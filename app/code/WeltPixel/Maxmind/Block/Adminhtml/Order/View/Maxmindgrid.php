<?php
namespace WeltPixel\Maxmind\Block\Adminhtml\Order\View;

use Magento\Sales\Model\Order;

class Maxmindgrid extends \Magento\Backend\Block\Template
{
    /**
     * Core registry
     *
     * @var \Magento\Framework\Registry
     */
    protected $_coreRegistry = null;

    protected $_helper = null;

    /**
     * @param \Magento\Backend\Block\Template\Context  $context
     * @param \Magento\Framework\Registry              $registry
     * @param \Magento\Sales\Helper\Admin              $adminHelper
     * @param \WeltPixel\Maxmind\Helper\Data           $helper
     * @param array $data
     */
    public function __construct(
        \Magento\Backend\Block\Template\Context $context,
        \Magento\Framework\Registry $registry,
        \Magento\Sales\Helper\Admin $adminHelper,
        \WeltPixel\Maxmind\Helper\Data $helper,
        array $data = []
    ) {
        parent::__construct(
            $context,
            $data
        );

        $this->_adminHelper  = $adminHelper;
        $this->_coreRegistry = $registry;
        $this->_helper       = $helper;
    }

    /**
     * Return the Maxmind fraud score
     *
     * @return mixed
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function getMaxmindFraudScore()
    {
        return $this->getOrder()->getData('weltpixel_fraud_score');
    }

    /**
     * Return the sys config score threshold
     *
     * @return mixed
     */
    public function getScoreThreshold()
    {
        return $this->_helper->getConfigValue('general/score_threshold');
    }

    /**
     * Return the Maxmind remaining credit
     *
     * @return mixed
     */
    public function getRemainingCredit()
    {
        return $this->_helper->getRemainingCredit();
    }

    /**
     * Retrieve available order
     *
     * @return Order
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function getOrder()
    {
        if ($this->hasOrder()) {
            return $this->getData('order');
        }
        if ($this->_coreRegistry->registry('current_order')) {
            return $this->_coreRegistry->registry('current_order');
        }
        if ($this->_coreRegistry->registry('order')) {
            return $this->_coreRegistry->registry('order');
        }
        throw new \Magento\Framework\Exception\LocalizedException(__('We can\'t get the order instance right now.'));
    }
}