<?php

namespace WeltPixel\Maxmind\Block\Adminhtml\Order\View\Tab;

class Maxmind extends \Magento\Backend\Block\Template implements \Magento\Backend\Block\Widget\Tab\TabInterface
{
    /**
     * Template
     *
     * @var string
     */
    protected $_template = 'order/view/tab/maxmind.phtml';

    /**
     * Core registry
     *
     * @var \Magento\Framework\Registry
     */
    protected $_coreRegistry = null;

    protected $_maxmindModel = null;

    protected $_maxmindData = null;

    /**
     * @var \Magento\Sales\Helper\Admin
     */
    protected $_helper;

    /**
     * @param \Magento\Backend\Block\Template\Context $context
     * @param \Magento\Framework\Registry $registry
     * @param \Magento\Sales\Helper\Admin $helper
     * @param array $data
     */
    public function __construct(
        \Magento\Backend\Block\Template\Context $context,
        \Magento\Framework\Registry $registry,
        \WeltPixel\Maxmind\Helper\Data $helper,
        \WeltPixel\Maxmind\Model\Maxmind $maxmind,
        array $data = []
    ) {
        parent::__construct(
            $context,
            $data
        );

        $this->_coreRegistry = $registry;
        $this->_maxmindModel = $maxmind;

        $this->_helper = $helper;
    }

    /**
     * Retrieve order model instance
     *
     * @return \Magento\Sales\Model\Order
     */
    public function getOrder()
    {
        return $this->_coreRegistry->registry('current_order');
    }

    /**
     * Retrieve maxmind fraud order data
     *
     * @return array
     */
    public function getMaxmindData()
    {
        if(empty($this->_maxmindData)) {
            $orderId = $this->getOrder()->getId();

            $model = $this->_maxmindModel;
            $model->loadMaxmindByOrderId($orderId);

            if ($model->getFraudData()) {
                $this->_maxmindData = unserialize($model->getFraudData());
            } else {
                $this->_maxmindData = [];
            }
        }

        return $this->_maxmindData;
    }

    /**
     * Return the sys config score threshold
     * @return mixed
     */
    public function getScoreThreshold()
    {
        return $this->_helper->getConfigValue('general/score_threshold');
    }

    /**
     * Get risk score percent
     *
     * @return string
     */
    public function getPercent()
    {
        $maxmindData    = $this->getMaxmindData();
        $scoreThreshold = $this->getScoreThreshold();
        $percent = '';

        if (array_key_exists('riskScore', $maxmindData) && !empty($maxmindData['riskScore'])) {
            $fraudScore = number_format($maxmindData['riskScore'], 2);
            if ($fraudScore >= 0.1 && $fraudScore <= 4.99) {
                $percent = "90%";
            } elseif ($fraudScore >= 5 && $fraudScore <= 9.99) {
                $percent =  "5%";
            } elseif ($fraudScore >= 10 && $fraudScore <= 29.99) {
                $percent =  "3%";
            } elseif ($fraudScore >= 30 && $fraudScore <= 99.99) {
                $percent =  "2%";
            } else {
                $percent = '';
            }
        }

        return $percent;
    }

    /**
     * Get risk score color
     *
     * @return string
     */
    public function getColor()
    {
        $maxmindData    = $this->getMaxmindData();
        $scoreThreshold = $this->getScoreThreshold();
        $fraudScore = 0;

        if (array_key_exists('riskScore', $maxmindData) && !empty($maxmindData['riskScore'])) {
            $fraudScore = number_format($maxmindData['riskScore'], 2);
        }

        return $fraudScore >= $scoreThreshold ? 'red' : 'auto';
    }

    /**
     * Get maxmind fraud score
     *
     * @return string
     */
    public function getFraudScore()
    {
        $maxmindData = $this->getMaxmindData();
        $fraudScore  = null;

        if (array_key_exists('riskScore', $maxmindData) && !empty($maxmindData['riskScore'])) {
            $fraudScore = number_format($maxmindData['riskScore'], 2);
        }

        return $fraudScore;
    }

    /**
     * {@inheritdoc}
     */
    public function getTabLabel()
    {
        return __('Maxmind Fraud Detection');
    }

    /**
     * {@inheritdoc}
     */
    public function getTabTitle()
    {
        return __('Order Maxmind');
    }

    /**
     * Get Tab Class
     *
     * @return string
     */
    public function getTabClass()
    {
        return 'ajax only';
    }

    /**
     * Get Class
     *
     * @return string
     */
    public function getClass()
    {
        return $this->getTabClass();
    }

    /**
     * Get Tab Url
     *
     * @return string
     */
    public function getTabUrl()
    {
        return $this->getUrl('maxmind/*/report', ['_current' => true]);
    }

    /**
     * {@inheritdoc}
     */
    public function canShowTab()
    {
//        return $this->_helper->moduleEnabled();
        return true;
    }

    /**
     * {@inheritdoc}
     */
    public function isHidden()
    {
        return false;
    }
}