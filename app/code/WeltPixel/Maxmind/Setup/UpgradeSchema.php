<?php

namespace WeltPixel\Maxmind\Setup;

use Magento\Framework\Setup\UpgradeSchemaInterface;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\SchemaSetupInterface;

/**
 * Upgrade schema
 * @category WeltPixel
 * @package  WeltPixel_Maxmind
 * @module   Maxmind
 * @author   WeltPixel Developer
 */
class UpgradeSchema implements UpgradeSchemaInterface
{
    public function upgrade(SchemaSetupInterface $setup, ModuleContextInterface $context)
    {
        $setup->startSetup();

        if (version_compare($context->getVersion(), '1.0.1', '<')) {

//        Add Fraud Score field to sales_order table
            $tableName = $setup->getTable('sales_order');

            if ($setup->getConnection()->isTableExists($tableName) == true) {
                $connection = $setup->getConnection();

                $connection->addColumn(
                    $tableName,
                    'fraud_score',
                    [
                        'type' => \Magento\Framework\DB\Ddl\Table::TYPE_DECIMAL,
                        'length' => '4,2',
                        'comment' => 'Fraud Score'
                    ]
                );
            }

//        Add Fraud Score field to sales_order_grid table
            $tableName = $setup->getTable('sales_order_grid');

            if ($setup->getConnection()->isTableExists($tableName) == true) {
                $connection = $setup->getConnection();

                $connection->addColumn(
                    $tableName,
                    'fraud_score',
                    [
                        'type' => \Magento\Framework\DB\Ddl\Table::TYPE_DECIMAL,
                        'length' => '4,2',
                        'comment' => 'Fraud Score'
                    ]
                );
            }
        }


        if (version_compare($context->getVersion(), '1.0.2', '<')) {

            $tableName = $setup->getTable('sales_order');
            $setup->getConnection()->changeColumn(
                $tableName,
                'fraud_score',
                'weltpixel_fraud_score',
                [
                    'type' => \Magento\Framework\DB\Ddl\Table::TYPE_DECIMAL,
                    'length' => '4,2',
                    'comment' => 'Fraud Score'
                ]
            );

            $tableName = $setup->getTable('sales_order_grid');
            $setup->getConnection()->changeColumn(
                $tableName,
                'fraud_score',
                'weltpixel_fraud_score',
                [
                    'type' => \Magento\Framework\DB\Ddl\Table::TYPE_DECIMAL,
                    'length' => '4,2',
                    'comment' => 'Fraud Score'
                ]
            );
        }

        $setup->endSetup();
    }
}
