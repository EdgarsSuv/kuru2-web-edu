<?php

namespace WeltPixel\Maxmind\Model;

/**
 * Maxmind Model
 * @category WeltPixel
 * @package  WeltPixel_Maxmind
 * @module   Maxmind
 * @author   WeltPixel Developer
 */
class Maxmind extends \Magento\Framework\Model\AbstractModel
{
    /**
     * logger.
     *
     * @var \Magento\Framework\Logger\Monolog
     */
    private $_monolog;

    private $_maxmindFactory;

    /**
     * [__construct description].
     *
     * @param \Magento\Framework\Model\Context                                     $context
     * @param \Magento\Framework\Registry                                          $registry
     * @param \WeltPixel\Maxmind\Model\ResourceModel\Maxmind                       $resource
     * @param \WeltPixel\Maxmind\Model\ResourceModel\Maxmind\Collection            $resourceCollection
     * @param \WeltPixel\Maxmind\Model\MaxmindFactory                              $maxmindFactory
     * @param \Magento\Store\Model\StoreManagerInterface                           $storeManager
     */
    public function __construct(
        \Magento\Framework\Model\Context $context,
        \Magento\Framework\Registry $registry,
        \WeltPixel\Maxmind\Model\ResourceModel\Maxmind $resource,
        \WeltPixel\Maxmind\Model\ResourceModel\Maxmind\Collection $resourceCollection,
        \WeltPixel\Maxmind\Model\MaxmindFactory $maxmindFactory,
        \Magento\Framework\Logger\Monolog $monolog,
        \Magento\Customer\Model\ResourceModel\Group\CollectionFactory $groupCollectionFactory
    ) {
        parent::__construct(
            $context,
            $registry,
            $resource,
            $resourceCollection
        );
        $this->_maxmindFactory = $maxmindFactory;
        $this->_monolog = $monolog;
    }

    /**
     * Load maxmind model by order id
     *
     * @param int $orderId
     * @return \WeltPixel\Maxmind\Model\Maxmind
     */
    public function loadMaxmindByOrderId($orderId)
    {
        $this->load($orderId, 'order_id');

        return $this;
    }
}
