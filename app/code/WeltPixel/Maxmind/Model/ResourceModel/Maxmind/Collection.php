<?php

namespace WeltPixel\Maxmind\Model\ResourceModel\Maxmind;

/**
 * Maxmind Collection
 * @category WeltPixel
 * @package  WeltPixel_Maxmind
 * @module   Maxmind
 * @author   WeltPixel Developer
 */
class Collection extends \Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection
{
    /**
     * _construct
     * @return void
     */
    protected function _construct()
    {
        $this->_init('WeltPixel\Maxmind\Model\Maxmind', 'WeltPixel\Maxmind\Model\ResourceModel\Maxmind');
    }
}
