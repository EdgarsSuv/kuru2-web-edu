<?php

namespace WeltPixel\Maxmind\Model\ResourceModel;

/**
 * Maxmind Resource Model
 * @category WeltPixel
 * @package  WeltPixel_Maxmind
 * @module   Maxmind
 * @author   WeltPixel Developer
 */
class Maxmind extends \Magento\Framework\Model\ResourceModel\Db\AbstractDb
{
    /**
     * construct
     * @return void
     */
    protected function _construct()
    {
        $this->_init('weltpixel_maxmind_data', 'id');
    }
}
