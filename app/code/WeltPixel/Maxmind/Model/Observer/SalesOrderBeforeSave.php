<?php

namespace WeltPixel\Maxmind\Model\Observer;

use Magento\Framework\Event\ObserverInterface;

class SalesOrderBeforeSave implements ObserverInterface
{
    public static $maxmindErrors = [
        'FATAL_ERR',
        'INVALID_LICENSE_KEY',
        'MAX_REQUESTS_PER_LICENSE',
        'IP_REQUIRED',
        'LICENSE_REQUIRED',
        'COUNTRY_REQUIRED',
        'MAX_REQUESTS_REACHED',
        'SYSTEM_ERROR',
        'IP_NOT_FOUND'
    ];

    private $_helper;

    private $_registry = null;

    private $_monolog;

    /**
     * [__construct description].
     *
     * @param \WeltPixel\Maxmind\Helper\Data    $helper
     * @param \Magento\Framework\Registry       $registry
     * @param \Magento\Framework\Logger\Monolog $monolog
     */
    public function __construct(
        \WeltPixel\Maxmind\Helper\Data $helper,
        \Magento\Framework\Registry $registry,
        \Magento\Framework\Logger\Monolog $monolog
    ) {
        $this->_helper   = $helper;
        $this->_registry = $registry;
        $this->_monolog  = $monolog;
    }

    /**
     * Set Maxmind data
     *
     * @param \Magento\Framework\Event\Observer $observer
     * @return $this
     */
    public function execute(\Magento\Framework\Event\Observer $observer)
    {
        if(!$this->_helper->moduleEnabled()) {
            return $this;
        }

        /** @var \Magento\Sales\Model\Order $order */
        $order = $observer->getEvent()->getOrder();

        $orderAmount = $order->getGrandTotal();
        $minOrderAmount = $this->_helper->getConfigValue('general/min_order_amount');

        if($minOrderAmount && ($orderAmount < $minOrderAmount)) {
            return $this;
        }

        $origdata = $order->getOrigData();
        $originalState = null;
        if($origdata) {
            $originalState = array_key_exists('state', $origdata) ? $origdata['state'] : null;
        }

        if ($order->isCanceled() || $order->getState() == \Magento\Sales\Model\Order::STATE_CLOSED 
            || ( $originalState && $originalState == \Magento\Sales\Model\Order::STATE_HOLDED)) {
            return $this;
        }

        $payment = $order->getPayment();

        try {
            $maxmindData = $this->_helper->getMaxmindData($payment);
            $maxmindData = $this->_helper->checkMaxmindData($maxmindData);


            if (!in_array($maxmindData['err'], self::$maxmindErrors) || empty($maxmindData['err'])) {

                $this->_helper->saveMaxmindData($maxmindData, $order);

                $fraudScore = array_key_exists('ourscore', $maxmindData) ? $maxmindData['ourscore'] : '';

                $order->setWeltpixelFraudScore($fraudScore);
                $order->setMaxmindData($maxmindData);

                $ourscore = array_key_exists('ourscore', $maxmindData) ? $maxmindData['ourscore'] : null;

                if(!$ourscore) {
                    return $this;
                }

                $canHold = $this->_helper->canHold($ourscore);

                if ($order->canHold() && $canHold) {
                    $order->hold();
                }
            } else {
                $this->_monolog->addError("Maxmind Error: " . $maxmindData['err']);
            }
        } catch (\Exception $e) {
            $this->_monolog->addError($e->getMessage());
        }

        return $this;
    }
}
