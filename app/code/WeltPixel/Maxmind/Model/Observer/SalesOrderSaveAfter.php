<?php

namespace WeltPixel\Maxmind\Model\Observer;

use \Magento\Framework\Event\Observer;
use \Magento\Framework\Event\ObserverInterface;

class SalesOrderSaveAfter implements ObserverInterface
{
    protected $_helper;

    protected $_registry = null;

    protected $_monolog;

    /**
     * [__construct description].
     *
     * @param \WeltPixel\Maxmind\Helper\Data    $helper
     * @param \Magento\Framework\Registry       $registry
     * @param \Magento\Framework\Logger\Monolog $monolog
     */

    public function __construct(
        \WeltPixel\Maxmind\Helper\Data $helper,
        \Magento\Framework\Registry $registry,
        \Magento\Framework\Logger\Monolog $monolog
    ) {
        $this->_helper   = $helper;
        $this->_registry = $registry;
        $this->_monolog  = $monolog;
    }

    /**
     * Set Maxmind data
     *
     * @param \Magento\Framework\Event\Observer $observer
     * @return $this
     */
    public function execute(Observer $observer)
    {
        if(!$this->_helper->moduleEnabled()) {
            return $this;
        }

        /** @var \Magento\Sales\Model\Order $order */
        $order = $observer->getEvent()->getOrder();

        $orderAmount = $order->getGrandTotal();
        $minOrderAmount = $this->_helper->getConfigValue('general/min_order_amount');

        if($minOrderAmount && ($orderAmount < $minOrderAmount)) {
            return $this;
        }

        try {
            if ($maxmindData = $order->getMaxmindData()) {
                if ($order->getId()) {
                    $this->_helper->saveMaxmindData($maxmindData, $order);
                    $order->unsMaxmindData();
                }
            }
        } catch (\Exception $e) {
            $this->_monolog->addError($e->getMessage());
        }
    }
}
