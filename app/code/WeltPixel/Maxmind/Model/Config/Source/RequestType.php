<?php

/**
 * Used in creating options for Maxmind request type config value
 *
 */
namespace WeltPixel\Maxmind\Model\Config\Source;

class RequestType implements \Magento\Framework\Option\ArrayInterface
{
    /**
     * Options getter
     *
     * @return array
     */
    public function toOptionArray()
    {
        return [
            [
                'label' => __('Standard'),
                'value' => 'standard',
            ],
            [
                'label' => __('Premium'),
                'value' => 'premium',
            ],
        ];
    }

    /**
     * Get options in "key-value" format
     *
     * @return array
     */
    public function toArray()
    {
        return [
            'standard' => __('Standard'),
            'premium'  => __('Premium'),
        ];
    }
}
