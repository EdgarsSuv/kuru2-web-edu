<?php

namespace WeltPixel\Maxmind\Helper;

/**
 * Helper Data
 * @category WeltPixel
 * @package  WeltPixel_Maxmind
 * @module   Maxmind
 * @author   WeltPixel Developer
 */

class Data extends \Magento\Framework\App\Helper\AbstractHelper
{
    const MAXMIND_VARIABLE_NAME = 'maxmind_remaining_credit';

    private $_requiredFields = ["i", "license_key", "country", "city", "postal"];

    private $_maxmindModel;

    private $_scopeConfig;

    private $_quote;

    private $_variableModel;

    /**
     * [__construct description].
     *
     * @param \Magento\Framework\App\Helper\Context              $context
     * @param \WeltPixel\Maxmind\Model\Maxmind                   $maxmindModel
     * @param \Magento\Quote\Model\Quote                         $quote
     * @param \Magento\Variable\Model\Variable                   $variableModel
     * @param \Magento\Directory\Model\Region                    $regionModel
     * @param \Magento\Framework\Logger\Monolog                  $monolog
     */
    public function __construct(
        \Magento\Framework\App\Helper\Context $context,
        \WeltPixel\Maxmind\Model\Maxmind $maxmindModel,
        \Magento\Framework\Stdlib\DateTime\DateTime $date,
        \Magento\Quote\Model\Quote $quote,
        \Magento\Variable\Model\Variable $variableModel,
        \Magento\Directory\Model\Region $regionModel,
        \Magento\Framework\Logger\Monolog $monolog
    ) {
        parent::__construct($context);

        $this->_maxmindModel  = $maxmindModel;
        $this->_date          = $date;
        $this->_scopeConfig   = $context->getScopeConfig();
        $this->_quote         = $quote;
        $this->_variableModel = $variableModel;
        $this->_regionModel   = $regionModel;
        $this->_monolog       = $monolog;
    }

    /**
     * Check maxmind data
     *
     * @param array $response
     * @return array
     */
    public function checkMaxmindData($response)
    {
        if (array_key_exists('riskScore', $response) && $response['riskScore'] != '') {
            $response['ourscore'] = floatval($response['riskScore']);
        } else {
            $response['err'] = (array_key_exists('err', $response) && !empty($res['err']))
                ? $response['err']
                : "FATAL_ERR";
            $response['errmsg'] = array_key_exists('errmsg', $response)
                ? $response['errmsg']
                : "No riskScore or server response.";
        }

        return $response;
    }

    /**
     * Get the Maxmind server response
     *
     * @param array $params
     * @return array
     */
    public function getMaxmindServerResponse($params)
    {
        $return = [];

        $url = $this->getConfigValue('connection/api_server');

        try {
            $curl_options = [
                CURLOPT_URL => $url,
                CURLOPT_POST => true,

                CURLOPT_POSTFIELDS => http_build_query($params),
                CURLOPT_HTTP_VERSION => 1.0,
                CURLOPT_RETURNTRANSFER => true,
                CURLOPT_HEADER => false
            ];

            $curl = curl_init();

            if ($this->getConfigValue('connection/disable_certificate_check')) {
                curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
            }

            curl_setopt_array($curl, $curl_options);
            $response = curl_exec($curl);

            curl_close($curl);

            if (!empty($response)) {
                foreach (explode(";", $response) as $keyval)
                {
                    $bits = explode("=", $keyval);
                    if (count($bits) > 1) {
                        list($key, $value) = $bits;
                        $return[ $key ] = $value;
                    }
                }
            }
        } catch (\Exception $e) {
            $this->_monolog->addError($e->getMessage());
        }

        if(!count($return)) {
            $return['errmsg'] = "Could not connect to MaxMind server.";
            $return['err'] = "FATAL_ERR";
        }

        return $return;
    }

    /**
     * Get maxmind data
     *
     * @param $payment
     * @return array
     */
    public function getMaxmindData($payment)
    {
        $order = $payment->getOrder();

        $params = $this->getMaxmindParams($payment);

        if (!isset($params['i']) || empty($params['i'])) {
            return [
                "err"    => "FATAL_ERR",
                "errmsg" => "Order IP address not found."
            ];
        }

        foreach ($this->_requiredFields as $field)
        {
            if (!array_key_exists($field, $params) || empty($params[$field])) {
                return [
                    "errmsg" => "Missing $field required field.",
                    "err"    => "FATAL_ERR"
                ];
            }
        }

        $ipExceptions = $this->getConfigValue('general/ip_exceptions');
        if ($ipExceptions) {
            $ipExceptions = explode(',', $ipExceptions);

            if (in_array($params['i'], $ipExceptions)) {
                return [
                    "errmsg" => "IP address " . $params['i'] . " is in IP exceptions list.",
                    "err"    => "FATAL_ERR"
                ];
            }
        }

        if ($order->getId()) {
            $this->_maxmindModel->loadMaxmindByOrderId($order->getId())
                ->addData([
                    'order_id'  => $order->getId(),
                    'sent_data' => utf8_encode(serialize($params))
                ])
            ->save();
        } else {
            $order->setMaxmindTempData(utf8_encode(serialize($params)));
        }

        $response = $this->getMaxmindServerResponse($params);

        // set remaining maxmind credit in custom variable
        if (!empty($response['queriesRemaining'])) {

            $model = $this->_variableModel->loadByCode(self::MAXMIND_VARIABLE_NAME);

            if(!$model->getCode()) {
                $model->setCode(self::MAXMIND_VARIABLE_NAME);
                $model->setName('Maxmind Remaining Credit ');
                $model->setPlainValue($response['queriesRemaining']);
            } else {
                $model->setPlainValue($response['queriesRemaining']);
            }

            $model->save();
        }

        return $response;
    }

    /**
     * Prepare maxmind params
     *
     * @param $payment
     * @return mixed
     */
    public function getMaxmindParams($payment)
    {
        $order = $payment->getOrder();

        $billAddress = $order->getBillingAddress();
        $shipAddress = $order->getShippingAddress();

        if (!($billAddress && $billAddress->getCountry()) || !($shipAddress && $shipAddress->getCountry())) {
            $shipAddress = $shipAddress && $shipAddress->getCountry() ? $shipAddress : $billAddress;

            $address = $billAddress && $billAddress->getCountry() ? $billAddress : $shipAddress;
        }
        if (!$billAddress && !$shipAddress) {
            return [
                "errmsg" => "Billing or Shipping address not found.",
                "err"    => "FATAL_ERR"
            ];
        }

        $licenseKey = trim($this->getConfigValue('connection/license_key'));

        if (empty($licenseKey)) {
            return [
                "errmsg" => "License key not set.",
                "err"    => "FATAL_ERR"
            ];
        }

        $params["requested_type"] = 'standard';
        $params["license_key"] = trim($licenseKey);
        $params["i"] = $order->getRemoteIp();

        if ($this->getConfigValue('general/force_ip')) {
            $params["i"] = $this->getConfigValue('general/force_ip');
        }

        $params["city"]    = $address->getCity();
        $params["region"]  = $address->getRegion();
        $params["postal"]  = $address->getPostcode();
        $params["country"] = $address->getCountryId();

        if ((strlen($params["postal"]) > 5) && (strpos($params["postal"], '-') !== false) &&
            $params["country"] == "US") {
            $params["postal"] = substr($address->getPostcode(), 0, 5);
        }

        $regionCode = '';
        if($address->getRegionId()) {
            $regionCode = $this->getRegionCode($address->getRegionId());
        }

        $params["region"] = $regionCode;

        if (preg_match("/[^@]+@(.*)/", $order->getCustomerEmail(), $matchEmail)) {
            $params["domain"] = $matchEmail[1];
        }

        if ($payment->getCcNumber()) {
            $params["bin"] = substr($payment->getCcNumber(), 0, 6);
        }

        $params["usernameMD5"] = md5(strtolower($order->getCustomerEmail()));
        $params["passwordMD5"] = $this->_quote->load($order->getQuoteId())->getPasswordHash();
        $params["emailMD5"]    = md5(strtolower($order->getCustomerEmail()));
        $params["custPhone"]   = $address->getTelephone();
        $params["shipAddr"]    = implode(',', $shipAddress->getStreet());
        $params["shipCity"]    = $shipAddress->getCity();

        $regionCode = '';
        if ($shipAddress->getRegion()) {
            $regionCode = $this->getRegionCode($shipAddress->getRegion());
        }
        $params["shipRegion"]  = $regionCode;
        $params["shipPostal"]  = $shipAddress->getPostcode();
        $params["shipCountry"] = $shipAddress->getCountryId();

        if ((strlen($params["shipPostal"]) > 5) && (strpos($params["shipPostal"], '-') !== false) &&
            $params["shipCountry"] == "US") {
            $params["shipPostal"] = substr($shipAddress->getPostcode(), 0, 5);
        }

        $params["txnID"] = $order->getIncrementId();
        $params["accept_language"] = substr($_SERVER['HTTP_ACCEPT_LANGUAGE'], 0, 2);
        $params["user_agent"] = $_SERVER['HTTP_USER_AGENT'];

        return $params;
    }

    /**
     * Return region code by region id
     *
     * @param $regionId
     * @return mixed
     */
    public function getRegionCode($regionId)
    {
        $region = $this->_regionModel->load($regionId);

        return $region->getCode();
    }

    /**
     * Save maxmind data
     *
     * @param array $data
     * @param $order
     */
    public function saveMaxmindData($data, $order)
    {
        if ($order->getMaxmindTempData()) {
            $this->_maxmindModel
                ->loadMaxmindByOrderId($order->getId())
                ->addData([
                    'order_id'    => $order->getId(),
                    'fraud_score' => $data['ourscore'],
                    'fraud_data'  => utf8_encode(serialize($data)),
                    'sent_data'   => $order->getMaxmindTempData(),
                ])
             ->save();
        } else {
            $this->_maxmindModel
                ->loadMaxmindByOrderId($order->getId())
                ->addData([
                    'order_id'    => $order->getId(),
                    'fraud_score' => $data['ourscore'],
                    'fraud_data'  => utf8_encode(serialize($data)),
                ])
             ->save();
        }
    }

    /**
     * Check if module is enabled
     *
     * @return mixed
     */
    public function moduleEnabled()
    {
        return $this->getConfigValue('general/enable');
    }

    /**
     * Check if order can be hold
     *
     * @param $ourscore
     * @return bool
     */
    public function canHold($ourscore)
    {
        $scoreThreshold = $this->getConfigValue('general/score_threshold');
        $paramsoldOrder = $this->getConfigValue('general/hold_order');

        return $ourscore >= $scoreThreshold && $paramsoldOrder;
    }

    /**
     * Get maxmind configuration field value
     *
     * @param string $configPath
     * @return mixed
     */
    public function getConfigValue($configPath)
    {
        $sysPath = 'weltpixel_maxmind_config/' . $configPath;

        return $this->_scopeConfig->getValue($sysPath, \Magento\Store\Model\ScopeInterface::SCOPE_STORE);
    }

    /**
     * Return Maxmind available credit
     *
     * @return mixed
     */
    public function getRemainingCredit()
    {
        $model = $this->_variableModel->loadByCode(self::MAXMIND_VARIABLE_NAME);

        $remainingCredit = $model->getPlainValue();

        return $remainingCredit;
    }
}
