<?php
/**
 * @vendor Scandiweb
 * @module Scandiweb_MagicZoom
 * @author Krisjanis Veinbahs <krisjanisv@scandiweb.com>
 * @copyright Copyright (c) 2017 KURU Footwear. All rights reserved.
 * @license http://opensource.org/licenses/afl-3.0.php Academic Free License (AFL 3.0)
 */
namespace Scandiweb\MagicZoom\Block;

use Scandi\MagicZoom\Block\Gallery as ScandiZoom;

class Gallery extends ScandiZoom
{
    /**
     * Retrieve product images in JSON format
     *
     * @return string
     */
    public function getGalleryImagesJson()
    {
        $imagesItems = [];
        foreach ($this->getGalleryImages() as $image) {
            $imagesItems[] = [
                'product' => $this->getProduct()->getName(),
                'thumb' => $image->getData('small_image_url'),
                'img' => $image->getData('medium_image_url'),
                'full' => $image->getData('large_image_url'),
                'caption' => $image->getLabel(),
                'position' => $image->getPosition(),
                'isMain' => $this->isMainImage($image),
                'mediaType' => $image->getMediaType(),
                'videoUrl' => $image->getVideoUrl()
            ];
        }
        if (empty($imagesItems)) {
            $imagesItems[] = [
                'product' => $this->getProduct()->getName(),
                'thumb' => $this->_imageHelper->getDefaultPlaceholderUrl('thumbnail'),
                'img' => $this->_imageHelper->getDefaultPlaceholderUrl('image'),
                'full' => $this->_imageHelper->getDefaultPlaceholderUrl('image'),
                'caption' => '',
                'position' => '0',
                'isMain' => true,
                'mediaType' => 'image',
                'videoUrl' => ''
            ];
        }
        return json_encode($imagesItems);
    }

    /**
     * Get first product image url
     *
     * @return bool|string
     */
    public function getFirstImageUrl()
    {
        if ($imageUrl = $this->getGalleryImages()->getFirstItem()->getData('medium_image_url')) {
            return $imageUrl;
        }

        return false;
    }
}