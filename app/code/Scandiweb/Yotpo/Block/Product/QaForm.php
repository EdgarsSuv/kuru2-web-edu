<?php
/**
 * @vendor Scandiweb
 * @module Scandiweb_Yotpo
 * @author Kristaps Stalidzāns <info@scandiweb.com>
 * @copyright Copyright (c) 2017 Scandiweb, Ltd (http://scandiweb.com)
 * @license http://opensource.org/licenses/afl-3.0.php Academic Free License (AFL 3.0)
 */
namespace Scandiweb\Yotpo\Block\Product;

use Magento\Catalog\Api\ProductRepositoryInterface;
use Magento\Customer\Model\Context;
use Magento\Customer\Model\Url;
use Magento\Customer\Model\Session;
use Magento\Framework\App\Http\Context as HttpContext;
use Magento\Framework\Message\ManagerInterface;
use Magento\Framework\Url\EncoderInterface;
use Magento\Framework\View\Element\Template;
use Magento\Review\Helper\Data;
use Magento\Review\Model\RatingFactory;
use Magento\Review\Model\ResourceModel\Rating\Collection as RatingCollection;

class QaForm extends Template
{
    /**
     * Review data
     *
     * @var Data
     */
    protected $_reviewData = null;

    /**
     * Catalog product model
     *
     * @var ProductRepositoryInterface
     */
    protected $productRepository;

    /**
     * Rating model
     *
     * @var RatingFactory
     */
    protected $_ratingFactory;

    /**
     * @var EncoderInterface
     */
    protected $urlEncoder;

    /**
     * Message manager interface
     *
     * @var ManagerInterface
     */
    protected $messageManager;

    /**
     * @var HttpContext
     */
    protected $httpContext;

    /**
     * @var Url
     */
    protected $customerUrl;

    /**
     * @var array
     */
    protected $jsLayout;
    /**
     * @var Session
     */
    private $customerSession;

    /**
     * @param Template\Context $context
     * @param EncoderInterface $urlEncoder
     * @param Data $reviewData
     * @param ProductRepositoryInterface $productRepository
     * @param RatingFactory $ratingFactory
     * @param ManagerInterface $messageManager
     * @param HttpContext $httpContext
     * @param Url $customerUrl
     * @param Session $customerSession
     * @param array $data
     */
    public function __construct(
        Template\Context $context,
        EncoderInterface $urlEncoder,
        Data $reviewData,
        ProductRepositoryInterface $productRepository,
        RatingFactory $ratingFactory,
        ManagerInterface $messageManager,
        HttpContext $httpContext,
        Url $customerUrl,
        Session $customerSession,
        array $data = []
    ) {
        $this->urlEncoder = $urlEncoder;
        $this->_reviewData = $reviewData;
        $this->productRepository = $productRepository;
        $this->_ratingFactory = $ratingFactory;
        $this->messageManager = $messageManager;
        $this->httpContext = $httpContext;
        $this->customerUrl = $customerUrl;
        parent::__construct($context, $data);
        $this->jsLayout = isset($data['jsLayout']) ? $data['jsLayout'] : [];
        $this->customerSession = $customerSession;
    }

    /**
     * Initialize review form
     *
     * @return void
     */
    protected function _construct()
    {
        parent::_construct();

        $this->setTemplate('qaform.phtml');
    }

    /**
     * @return \Magento\Catalog\Api\Data\ProductInterface
     */
    public function getProductInfo()
    {
        return $this->productRepository->getById(
            $this->getProductId(),
            false,
            $this->_storeManager->getStore()->getId()
        );
    }

    /**
     * Get review product post action
     *
     * @return string
     */
    public function getAction()
    {
        return $this->getUrl(
            'sw_yotpo/product/postquestion',
            [
                '_secure' => $this->getRequest()->isSecure(),
                'id' => $this->getProductId(),
            ]
        );
    }

    /**
     * Return register URL
     *
     * @return string
     */
    public function getRegisterUrl()
    {
        return $this->customerUrl->getRegisterUrl();
    }

    /**
     * Get review product id
     *
     * @return int
     */
    protected function getProductId()
    {
        return $this->getRequest()->getParam('id', false);
    }

    /**
     * Checks if user is logged in
     *
     * @return bool
     */
    public function isCustomerLoggedIn()
    {
        return $this->customerSession->isLoggedIn();
    }
}
