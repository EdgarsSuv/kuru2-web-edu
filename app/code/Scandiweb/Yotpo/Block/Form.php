<?php
/**
 * @vendor Scandiweb
 * @module Scandiweb_Yotpo
 * @author Kristaps Stalidzāns <info@scandiweb.com>
 * @copyright Copyright (c) 2017 Scandiweb, Ltd (http://scandiweb.com)
 * @license http://opensource.org/licenses/afl-3.0.php Academic Free License (AFL 3.0)
 */
namespace Scandiweb\Yotpo\Block;

use Magento\Catalog\Api\ProductRepositoryInterface;
use Magento\Customer\Model\Url;
use Magento\Framework\App\Http\Context as HttpContext;
use Magento\Framework\Message\ManagerInterface;
use Magento\Framework\Url\EncoderInterface;
use Magento\Framework\View\Element\Template\Context;
use Magento\Review\Helper\Data;
use Magento\Review\Model\RatingFactory;
use Magento\Customer\Model\Session;

class Form extends \Magento\Review\Block\Form
{
    /**
     * @var Session
     */
    protected $customerSession;

    /**
     * Form constructor.
     * @param Context $context
     * @param EncoderInterface $urlEncoder
     * @param Data $reviewData
     * @param ProductRepositoryInterface $productRepository
     * @param RatingFactory $ratingFactory
     * @param ManagerInterface $messageManager
     * @param HttpContext $httpContext
     * @param Url $customerUrl
     * @param Session $customerSession
     * @param array $data
     */
    public function __construct(
        Context $context,
        EncoderInterface $urlEncoder,
        Data $reviewData,
        ProductRepositoryInterface $productRepository,
        RatingFactory $ratingFactory,
        ManagerInterface $messageManager,
        HttpContext $httpContext,
        Url $customerUrl,
        Session $customerSession,
        array $data = []
    )
    {
        $this->customerSession = $customerSession;

        parent::__construct(
            $context,
            $urlEncoder,
            $reviewData,
            $productRepository,
            $ratingFactory,
            $messageManager,
            $httpContext,
            $customerUrl,
            $data
        );
    }

    /**
     * Initialize review form
     *
     * @return void
     */
    protected function _construct()
    {
        parent::_construct();

        $this->setTemplate('Scandiweb_Yotpo::review_form.phtml');
    }

    /**
     * @return bool
     */
    public function isCustomerLoggedIn()
    {
        return $this->customerSession->isLoggedIn();
    }

    /**
     * This is needed to take the correct templates
     *
     * @return string
     */
    protected function _toHtml()
    {
        $this->setModuleName($this->extractModuleName('Magento\Review\Block\Form'));

        return parent::_toHtml();
    }
}
