<?php
/**
 * @category  Scandiweb
 * @package   Scandiweb\Yotpo
 * @author    Krišjānis Veinbahs <krisjanisv@scandiweb.com>
 * @copyright Copyright (c) 2017 Scandiweb, Inc (http://scandiweb.com)
 * @license   http://opensource.org/licenses/OSL-3.0 The Open Software License 3.0 (OSL-3.0)
 */
namespace Scandiweb\Yotpo\Console;

use Symfony\Component\Console\Command\Command;
use Magento\Framework\App\State;
use Psr\Log\LoggerInterface;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Magento\Framework\Exception\LocalizedException;
use Scandiweb\Yotpo\Helper\ReviewsSynchronizer;

class MigrateReviews extends Command
{
    /**
     * @var LoggerInterface
     */
    protected $_logger;

    /**
     * @var ReviewsSynchronizer
     */
    protected $reviewsSynchronizer;

    /**
     * @var State
     */
    protected $state;

    /**
     * ImportReviews constructor.
     * @param LoggerInterface $logger
     * @param ReviewsSynchronizer $reviewsSynchronizer
     * @param State $state
     */
    public function __construct
    (
        LoggerInterface $logger,
        ReviewsSynchronizer $reviewsSynchronizer,
        State $state
    ) {
        $this->_logger = $logger;
        $this->reviewsSynchronizer = $reviewsSynchronizer;
        $this->state = $state;

        parent::__construct();
    }

    /**
     * Set command info
     */
    protected function configure()
    {
        $this->setName('scandiweb:migrate-yotpo-reviews');
        $this->setDescription('Migrates Yotpo product reviews');
        parent::configure();
    }

    /**
     * Migration execution
     * @param InputInterface $input
     * @param OutputInterface $output
     * @return bool
     * @throws LocalizedException
     */
    public function execute(InputInterface $input, OutputInterface $output)
    {
        try {
            $this->state->getAreaCode();
        } catch (LocalizedException $e) {
            $this->state->setAreaCode('adminhtml');
        }

        $this->reviewsSynchronizer->synchronize(false);

        return true;
    }
} 