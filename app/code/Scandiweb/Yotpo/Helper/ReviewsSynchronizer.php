<?php
/**
 * @vendor Scandiweb
 * @module Scandiweb_Yotpo
 * @author Krisjanis Veinbahs <krisjanisv@scandiweb.com>
 * @copyright Copyright (c) 2017 Scandiweb, Ltd (http://scandiweb.com)
 * @license http://opensource.org/licenses/afl-3.0.php Academic Free License (AFL 3.0)
 */
namespace Scandiweb\Yotpo\Helper;

use Magento\Framework\Exception\LocalizedException;
use Psr\Log\LoggerInterface;
use Scandiweb\Yotpo\Model\DataInterface;
use Scandiweb\Yotpo\Model\DataModel;

class ReviewsSynchronizer
{
    /**
     * @var LoggerInterface
     */
    protected $_logger;

    /**
     * @var Data
     */
    protected $helper;

    /**
     * @var DataModel
     */
    protected $dataModel;

    /**
     * ImportReviews constructor.
     * @param LoggerInterface $logger
     * @param Data $helper
     * @param DataModel $dataModel
     */
    public function __construct
    (
        LoggerInterface $logger,
        Data $helper,
        DataModel $dataModel
    ) {
        $this->_logger = $logger;
        $this->helper = $helper;
        $this->dataModel = $dataModel;
    }

    /**
     * Synchronize Yotpo product reviews
     * @param bool $dispensePoints
     */
    public function synchronize($dispensePoints = true)
    {
        $this->_logger->debug('Synchronizing yotpo reviews');
        $yotpoApiKey = $this->helper->getYotpoAppKey();
        $accessToken = $this->dataModel->getAccessToken();

        if ($yotpoApiKey && $accessToken) {
            $reviewsUrl = 'http://api.yotpo.com/v1/apps/' . $this->helper->getYotpoAppKey() . '/reviews?utoken=' . $accessToken;

            /**
             * Get first batch of reviews
             */
            $pageId = 1;
            $pageData = $this->getReviewsChunk($reviewsUrl, $pageId);

            while (count($pageData['reviews']) > 0) {
                /**
                 * Parse and process a batch of reviews
                 */
                try {
                    $this->dataModel->importReviewData($pageData['reviews'], $dispensePoints);
                } catch (LocalizedException $e) {
                    $this->_logger->debug('Review synchronization error: ' . $e->getMessage());
                }

                /**
                 * Get next batch of reviews
                 */
                $pageId++;
                $pageData = $this->getReviewsChunk($reviewsUrl, $pageId);
            }

            $this->_logger->debug('Review synchronization finished');
        } else {
            $this->_logger->debug('Review synchronization id not finished because API key and access Token is missing');
        }
    }

    /**
     * Return reviews page data
     * @param $path
     * @param int $page
     * @param int $count
     * @return array
     */
    public function getReviewsChunk($path, $page = 1, $count = 100) : array
    {
        $pathWithCount = $path . '&count=' . $count;
        $pathWithPage = $pathWithCount . '&page=' . $page;

        return $this->dataModel->sendJsonData($pathWithPage, DataInterface::GET);
    }
}
