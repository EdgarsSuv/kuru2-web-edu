<?php
/**
 * @vendor Scandiweb
 * @module Scandiweb_Yotpo
 * @author Kristaps Stalidzāns <info@scandiweb.com>
 * @copyright Copyright (c) 2017 Magento. All rights reserved.
 * @license http://opensource.org/licenses/afl-3.0.php Academic Free License (AFL 3.0)
 */
namespace Scandiweb\Yotpo\Helper;

use Magento\Store\Model\ScopeInterface;
use Magento\Framework\App\Helper\AbstractHelper;

class Data extends AbstractHelper
{
    /**
     * Yopto app secret
     */
    const YOTPO_APP_KEY = 'yotpo/settings/app_key';
    const YOTPO_APP_SECRET = 'yotpo/settings/secret';

    /**
     * Yopto Settings
     */
    const YOTPO_RENDER_WIDGET = 'scandiweb_yotpo/yotpo_settings/render';
    const YOTPO_REWARD_STATUS = 'scandiweb_yotpo/yotpo_settings/reward_status';
    const YOTPO_REWARD = 'scandiweb_yotpo/yotpo_settings/reward';
    const YOTPO_REWARD_MESSAGE = 'scandiweb_yotpo/yotpo_settings/reward_message';
    const YOTPO_AUTO_PUSH = 'scandiweb_yotpo/yotpo_settings/allow';

    /**
     * Path for rating for overall score
     */
    const YOTPO_RATING_PATH = 'scandiweb_yotpo/yotpo_settings/rating';

    /**
     * Returns Yotpo App key
     *
     * @return mixed
     */
    public function getYotpoAppKey()
    {
        $key = $this->scopeConfig->getValue(self::YOTPO_APP_KEY, ScopeInterface::SCOPE_STORE, 1);

        return $key;
    }

    /**
     * Returns Yotpo App secret
     *
     * @return mixed
     */
    public function getYotpoAppSecret()
    {
        $secret = $this->scopeConfig->getValue(self::YOTPO_APP_SECRET, ScopeInterface::SCOPE_STORE, 1);

        return $secret;
    }

    /**
     * This setting allows automatically push excellent score to yopto
     *
     * @return mixed
     */
    public function getAutoAllowStatus()
    {
        return $this->scopeConfig->getValue(self::YOTPO_AUTO_PUSH, ScopeInterface::SCOPE_STORE, 1);
    }

    /**
     * @param $path
     * @return mixed
     */
    public function getYotpoSettings($path)
    {
        return $this->scopeConfig->getValue($path, ScopeInterface::SCOPE_STORE, 1);
    }
}
