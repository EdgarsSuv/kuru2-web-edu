<?php
/**
 * @vendor Scandiweb
 * @module Scandiweb_Yotpo
 * @author Kristaps Stalidzāns <info@scandiweb.com>
 * @copyright Copyright (c) 2017 Scandiweb, Ltd (http://scandiweb.com)
 * @license http://opensource.org/licenses/afl-3.0.php Academic Free License (AFL 3.0)
 */
namespace Scandiweb\Yotpo\Setup;

use Magento\Framework\Setup\UpgradeSchemaInterface;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\SchemaSetupInterface;
use Magento\Framework\DB\Ddl\Table;
use Scandiweb\Yotpo\Setup\Setup as ParentSetup;

class UpgradeSchema extends ParentSetup implements UpgradeSchemaInterface
{
    /**
     * @param SchemaSetupInterface $setup
     * @param ModuleContextInterface $context
     */
    public function upgrade(
        SchemaSetupInterface $setup,
        ModuleContextInterface $context
    )
    {
        $this->_content = $context;
        $setup->startSetup();

        if ($this->getVersion('0.1.0')) {
            $this->addCustomerEmailColumn($setup);
        }

        if ($this->getVersion('0.1.1')) {
            $this->addRemoteIdColumn($setup);
        }

        $setup->endSetup();
    }

    /**
     * Adds email column to review table
     *
     * @param $setup
     */
    protected function addCustomerEmailColumn($setup)
    {
        // Get module table
        $tableName = $setup->getTable('review_detail');

        // Check if the table already exists
        if ($setup->getConnection()->isTableExists($tableName)) {

            $connection = $setup->getConnection();
            $connection->addColumn(
                $tableName,
                'email',
                [
                    'type' => Table::TYPE_TEXT,
                    'length' => 150,
                    'nullable' => true,
                    'comment' => 'Email field for non-registred users'
                ]
            );
        }
    }

    protected function addRemoteIdColumn($setup)
    {
        // Get module table
        $tableName = $setup->getTable('review');

        // Check if the table already exists
        if ($setup->getConnection()->isTableExists($tableName)) {

            $connection = $setup->getConnection();
            $connection->addColumn(
                $tableName,
                'remote_id',
                [
                    'type' => Table::TYPE_BIGINT,
                    'nullable' => true,
                    'comment' => 'Review id from remote database'
                ]
            );
        }
    }
}
