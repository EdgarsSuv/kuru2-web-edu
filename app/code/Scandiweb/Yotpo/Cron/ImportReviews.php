<?php
/**
 * @vendor Scandiweb
 * @module Scandiweb_Yotpo
 * @author Kristaps Stalidzāns <info@scandiweb.com>
 * @copyright Copyright (c) 2017 Scandiweb, Ltd (http://scandiweb.com)
 * @license http://opensource.org/licenses/afl-3.0.php Academic Free License (AFL 3.0)
 */
namespace Scandiweb\Yotpo\Cron;

use Magento\Framework\App\State;
use Psr\Log\LoggerInterface;
use Scandiweb\Yotpo\Helper\ReviewsSynchronizer;
use Magento\Framework\Exception\LocalizedException;

class ImportReviews
{
    /**
     * @var LoggerInterface
     */
    protected $_logger;

    /**
     * @var ReviewsSynchronizer
     */
    protected $reviewsSynchronizer;

    /**
     * @var State
     */
    protected $state;

    /**
     * ImportReviews constructor.
     * @param LoggerInterface $logger
     * @param ReviewsSynchronizer $reviewsSynchronizer
     * @param State $state
     */
    public function __construct
    (
        LoggerInterface $logger,
        ReviewsSynchronizer $reviewsSynchronizer,
        State $state
    ) {
        $this->_logger = $logger;
        $this->reviewsSynchronizer = $reviewsSynchronizer;
        $this->state = $state;
    }

    /**
     * Method executed when cron runs in server
     * @throws LocalizedException
     */
    public function execute()
    {
        try {
            $this->state->getAreaCode();
        } catch (LocalizedException $e) {
            $this->state->setAreaCode('adminhtml');
        }

        $this->reviewsSynchronizer->synchronize();
    }
}
