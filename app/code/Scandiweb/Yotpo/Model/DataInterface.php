<?php
/**
 * @vendor Scandiweb
 * @module Scandiweb_Yotpo
 * @author Kristaps Stalidzāns <info@scandiweb.com>
 * @copyright Copyright (c) 2017 Scandiweb, Ltd (http://scandiweb.com)
 * @license http://opensource.org/licenses/afl-3.0.php Academic Free License (AFL 3.0)
 */
namespace Scandiweb\Yotpo\Model;

use Exception;
use Zend_Http_Client_Exception;

interface DataInterface
{
    /**
     * Yotpo Auth URLs
     */
    const YOTPO_OAUTH_URL = 'https://api.yotpo.com/oauth/token';
    const YOTPO_API_URL = 'http://api.yotpo.com/v1/apps/';
    const YOTPO_WIDGET_REVIEWS_URL = 'https://api.yotpo.com/v1/widget/reviews/';
    const YOTPO_QESTION_CONFIRMATION_URL = 'https://api.yotpo.com/questions/send_confirmation_mail';

    /**
     * Response Codes
     */
    const YOTPO_RESPONSE_OK = 200;

    /**
     * Post Methods
     */
    const POST = 'POST';
    const GET = 'GET';

    /**
     * Approved review
     */
    const APPROVED = 1;
    const PENDING = 2;

    /**
     * Gets Yopto Access Token
     *
     * @return string
     */
    public function getAccessToken() : string;

    /**
     * Sends data with Zend Http client
     *
     * @param string $url
     * @param $method
     * @param array $data
     * @return mixed
     * @throws Zend_Http_Client_Exception
     * @throws Exception
     */
    public function sendJsonData(string $url, $method, array $data = null);

    /**
     * @param array $data
     */
    public function importReviewData(array $data);

    /**
     * @param array $data
     * @param $customerId
     * @param $reviewId
     */
    public function addRatings(array $data, $customerId, $reviewId);

    /**
     * @param array $data
     * @return string
     */
    public function jsonEncoder(array $data): string;

    /**
     * @param string $data
     * @return array
     */
    public function jsonDecoder(string $data): array;

    /**
     * @param $reviewId
     * @return float
     */
    public function calculateScore($reviewId) : float;
}
