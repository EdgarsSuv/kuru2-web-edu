<?php
/**
 * @vendor Scandiweb
 * @module Scandiweb_Yotpo
 * @author Kristaps Stalidzāns <info@scandiweb.com>
 * @copyright Copyright (c) 2017 Scandiweb, Ltd (http://scandiweb.com)
 * @license http://opensource.org/licenses/afl-3.0.php Academic Free License (AFL 3.0)
 */
namespace Scandiweb\Yotpo\Model\Rating;

use Magento\Eav\Model\Entity\Attribute\Source\AbstractSource;
use Magento\Review\Model\ResourceModel\Rating\CollectionFactory;

/**
 * Review top score select
 */
class RatingModel extends AbstractSource
{
    /**
     * Block collection factory
     *
     * @var CollectionFactory
     */
    protected $reviewRatingCollectionFactory;

    /**
     * Construct
     *
     * @param CollectionFactory $reviewRatingCollectionFactory
     */
    public function __construct
    (
        CollectionFactory $reviewRatingCollectionFactory
    )
    {
        $this->reviewRatingCollectionFactory = $reviewRatingCollectionFactory;
    }

    /**
     * {@inheritdoc}
     */
    public function getAllOptions()
    {
        if (!$this->_options) {
            $ratings = $this->reviewRatingCollectionFactory->create()->load();
            $options = [];

            foreach ($ratings->getItems() as $rating) {
                $options[] = [
                    'value' => $rating->getData('rating_id'),
                    'label' => $rating->getData('rating_code'),
                ];
            }

            $this->_options = $options;
            array_unshift($this->_options, ['value' => '', 'label' => __('Please select score mark')]);
        }

        return $this->_options;
    }
}
