<?php
/**
 * @vendor Scandiweb
 * @module Scandiweb_Yotpo
 * @author Kristaps Stalidzāns <info@scandiweb.com>
 * @copyright Copyright (c) 2017 Scandiweb, Ltd (http://scandiweb.com)
 * @license http://opensource.org/licenses/afl-3.0.php Academic Free License (AFL 3.0)
 */
namespace Scandiweb\Yotpo\Model;

use Braintree\Exception;
use Magento\Framework\Exception\NoSuchEntityException;
use Magento\Framework\Message\ManagerInterface;
use Magento\Review\Model\ReviewFactory;
use Scandiweb\Yotpo\Helper\Data;
use Magento\Framework\Json\Helper\Data as JsonHelper;
use Magento\Customer\Api\CustomerRepositoryInterface;
use Magento\Review\Model\Review;
use Magento\Framework\App\ResourceConnection;
use Magento\Review\Model\RatingFactory;
use Magento\Framework\UrlInterface;
use Mirasvit\Rewards\Helper\Balance;
use Magento\Catalog\Api\ProductRepositoryInterface;
use Magento\Customer\Api\Data\CustomerInterface;
use Magento\Catalog\Api\Data\ProductInterface;
use Magento\Store\Model\StoreManagerInterface;
use Zend_Http_Client;
use Zend_Http_Client_Exception;
use Psr\Log\LoggerInterface;

class DataModel implements DataInterface
{
    /**
     * @var Data
     */
    protected $helper;

    /**
     * @var JsonHelper
     */
    protected $jsonHelper;

    /**
     * @var CustomerRepositoryInterface
     */
    protected $customerRepositoryInterface;

    /**
     * Review model
     *
     * @var ReviewFactory
     */
    protected $reviewFactory;

    /**
     * @var LoggerInterface
     */
    protected $logger;

    /**
     * @var ResourceConnection
     */
    protected $resourceConnection;

    /**
     * @var RatingFactory
     */
    protected $ratingFactory;

    /**
     * @var Balance
     */
    protected $rewardsBalance;

    /**
     * @var ProductRepositoryInterface
     */
    protected $productRepository;

    /**
     * @var StoreManagerInterface
     */
    protected $storeManager;

    /**
     * @var ManagerInterface
     */
    protected $messageManager;

    /**
     * DataModel constructor.
     * @param Data $helper
     * @param JsonHelper $jsonHelper
     * @param CustomerRepositoryInterface $customerRepositoryInterface
     * @param ProductRepositoryInterface $productRepository
     * @param ManagerInterface $messageManager
     * @param StoreManagerInterface $storeManager
     * @param ReviewFactory $reviewFactory
     * @param ResourceConnection $resourceConnection
     * @param RatingFactory $ratingFactory
     * @param Balance $rewardsBalance
     * @param LoggerInterface $logger
     */
    public function __construct
    (
        Data $helper,
        JsonHelper $jsonHelper,
        CustomerRepositoryInterface $customerRepositoryInterface,
        ProductRepositoryInterface $productRepository,
        ManagerInterface $messageManager,
        StoreManagerInterface $storeManager,
        ReviewFactory $reviewFactory,
        ResourceConnection $resourceConnection,
        RatingFactory $ratingFactory,
        Balance $rewardsBalance,
        LoggerInterface $logger
    )
    {
        $this->helper = $helper;
        $this->jsonHelper = $jsonHelper;
        $this->customerRepositoryInterface = $customerRepositoryInterface;
        $this->logger = $logger;
        $this->reviewFactory = $reviewFactory;
        $this->resourceConnection = $resourceConnection;
        $this->ratingFactory = $ratingFactory;
        $this->rewardsBalance = $rewardsBalance;
        $this->productRepository = $productRepository;
        $this->storeManager = $storeManager;
        $this->messageManager = $messageManager;
    }

    /**
     * @return mixed
     */
    public function getAccessToken() : string
    {
        $postData = [
            'client_id' => $this->helper->getYotpoAppKey(),
            'client_secret' => $this->helper->getYotpoAppSecret(),
            'grant_type' => 'client_credentials'
        ];
        $data = $this->sendJsonData(self::YOTPO_OAUTH_URL, self::POST,$postData);

        return $data['access_token'];
    }

    /**
     * @param string $url
     * @param $method
     * @param array $data
     * @return mixed
     */
    public function sendJsonData(string $url, $method, array $data = null)
    {
        try {
            $client = new Zend_Http_Client($url);

            if ($method == DataInterface::POST) {
                $client->setRawData($this->jsonEncoder($data), 'application/json');
            }

            $response = $client->request($method);
            $responseStatus = $response->getStatus();

            if ($responseStatus === self::YOTPO_RESPONSE_OK) {
                return $this->jsonDecoder($response->getBody());
            } else {
                throw new Zend_Http_Client_Exception($response->getBody());
            }
        } catch (\Exception $e) {
            $this->logger->debug($e->getMessage());
        }
    }

    /**
     * @param ProductInterface $product
     * @param $customerData
     * @param array $review
     */
    protected function postReviews(ProductInterface $product, array $customerData, array $review)
    {
        $accessToken = $this->getAccessToken();
        $store = $this->storeManager->getStore();
        $imageUrl = $store->getBaseUrl(UrlInterface::URL_TYPE_MEDIA) . 'catalog/product' . $product->getImage();
        $postData = [
            'appkey' => $this->helper->getYotpoAppKey(),
            'domain' => $store->getBaseUrl(),
            'sku' => $product->getId(),
            'product_title' => $product->getName(),
            'product_description' => $product->getData('description'),
            'product_url' => $product->getProductUrl(),
            'product_image_url' => $imageUrl,
            'display_name' => $customerData['customer_name'],
            'email' => $customerData['email'],
            'review_content' => $review['content'],
            'review_title' => $review['title'],
            'review_score' => $review['score']
        ];

        if ($accessToken) {
            $this->sendJsonData(DataInterface::YOTPO_WIDGET_REVIEWS_URL, DataInterface::POST,$postData);
        }
    }

    /**
     * @param ProductInterface $product
     * @param array $customerData
     * @param array $question
     */
    protected function postQuestion(ProductInterface $product, array $customerData, array $question)
    {
        $accessToken = $this->getAccessToken();
        $store = $this->storeManager->getStore();
        $imageUrl = $store->getBaseUrl(UrlInterface::URL_TYPE_MEDIA) . 'catalog/product' . $product->getImage();
        $postData = [
            'review_content' => $question['question'],
            'display_name' => $customerData['customer_name'],
            'email' => $customerData['email'],
            'review_source' => 'widget_v2',
            'appkey' => $this->helper->getYotpoAppKey(),
            'sku' => $product->getId(),
            'product_id' => $product->getId(),
            'product_title' => $product->getName(),
            'product_description' => $product->getData('description'),
            'product_url' => $product->getProductUrl(),
            'product_image_url' => $imageUrl,
            'prevent_duplicate_review' => 'true',
        ];

        if ($accessToken) {
            $this->sendJsonData(DataInterface::YOTPO_QESTION_CONFIRMATION_URL, DataInterface::POST,$postData);
        }
    }

    /**
     * Processes question data that is made on Save
     *
     * @param $productId
     * @param $data
     * @throws NoSuchEntityException
     */
    public function processQuestionData($productId, $data)
    {
        $customer = $this->processCustomerData($data);
        $product = $this->productRepository->getById($productId);

        if ($customer) {
            $this->postQuestion($product, $customer, $data);
        }
    }

    /**
     * Processes review data that is made on Save
     *
     * @param $productId
     * @param $data
     * @param $reviewId
     * @throws NoSuchEntityException
     */
    public function processReviewData($productId, $data, $reviewId)
    {
        $reviewData = [
            'title' => $data['title'],
            'content' => $data['detail'],
            'score' => $this->calculateScore($reviewId),
        ];
        $customer = $this->processCustomerData($data);
        $product = $this->productRepository->getById($productId);

        if (!is_null($data['customer_id']) && $this->rewardStatus()) {
            $this->addCustomerRewardPoints($data['customer_id'], $reviewId);
            $this->messageManager->addSuccessMessage(__('Customer received reward points'));
        }

        if ($customer) {
            $this->postReviews($product, $customer, $reviewData);
        }
    }

    /**
     * Synchronize a batch of reviews and give points
     * to new review authors
     * @param array $data
     * @param bool $dispensePoints
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function importReviewData(array $data, $dispensePoints = true)
    {
        foreach ($data as $item) {
            /**
             * Create new or load old review by remote id
             * @var \Magento\Review\Model\Review $review
             */
            $remoteId = $item['id'];
            $review = $this->reviewFactory->create()
                ->load($remoteId, 'remote_id');

            /**
             * If product review is new, give reward points
             * then populate review data and save
             */
            if (is_null($review->getEntityId())) {
                /**
                 * Find existing, registered customer by email
                 */
                try {
                    $customerEmail = $item['email'];
                    $customer = $this->customerRepositoryInterface->get($customerEmail, 1);
                } catch (NoSuchEntityException $entityException) {
                    continue;
                }

                /**
                 * First create/save a local version of the review,
                 *
                 * if success - move on to dispensing points below
                 *              the review won't be looked at again in future cronjobs
                 *
                 * if fail - stop execution, don't dispense points either
                 *           essentially this will be like the request wasn't looked at
                 */
                try {
                    $itemSKU = $item['sku'];
                    $isProductReview = $itemSKU !== 'yotpo_site_reviews';

                    // As per task requirements, only product reviews get points
                    // And points can only be dispensed for registered customers (duh!)
                    if ($isProductReview && !is_null($customer)) {
                        $review->setEntityId($review->getEntityIdByCode(Review::ENTITY_PRODUCT_CODE))
                            ->setRemoteId($remoteId)
                            ->setEntityPkValue($itemSKU)
                            ->setStatusId(Review::STATUS_APPROVED)
                            ->setTitle($item['title'])
                            ->setDetail($item['content'])
                            ->setStoreId(1)
                            ->setNickname($item['name'])
                            ->setCustomerId($customer->getId())
                            ->save();

                        /**
                         * Dispense points for the review and add email for the review
                         */
                        if (!is_null($review->getId())) {
                            $this->addCustomerRewardPoints($customer->getId(), $remoteId);
                            $this->updateEmailField($review->getId(), $customerEmail);
                        }
                    }
                } catch (\Exception $exception) {
                    $this->logger->debug(
                        'Yotpo reviews synchronizer: Couldn\'t save review & dispense points. ' .
                        $exception->getMessage());
                }
            }
        }
    }

    /**
     * @param array $data
     * @param $customerId
     * @param $reviewId
     */
    public function addRatings(array $data, $customerId, $reviewId)
    {
        $ratingFactory = $this->ratingFactory->create();
        $ratingFactoryItems = $ratingFactory->getRatingCodes();

        foreach ($ratingFactoryItems as $item) {
            $rating = $this->ratingFactory->create()
                ->setRatingId($item)
                ->setReviewId($reviewId);

            if ($customerId) {
                $rating->setCustomerId($customerId);
            }

            $rating->addOptionVote($data['score'], $data['sku']);
        }
    }

    /**
     * @param array $data
     * @return string
     */
    public function jsonEncoder(array $data): string
    {
        return $this->jsonHelper->jsonEncode($data);
    }

    /**
     * @param string $data
     * @return array
     */
    public function jsonDecoder(string $data): array
    {
        return $this->jsonHelper->jsonDecode($data);
    }

    /**
     * @param int $customerId
     * @return CustomerInterface
     */
    public function getCustomerData(int $customerId) : CustomerInterface
    {
        return $this->customerRepositoryInterface->getById($customerId);
    }

    /**
     * Function makes sure that
     *
     * @param $data
     * @return array
     */
    public function processCustomerData($data) : array
    {
        $customerData = null;

        if ($data['customer_id']) {
            $customer = $this->getCustomerData($data['customer_id']);
            $customerData = [
                'customer_name' => $customer->getFirstname() . ' ' . $customer->getLastname(),
                'email' => $customer->getEmail()
            ];
        } else if ($data['nickname'] && $data['email']) {
            $customerData = [
                'customer_name' => $data['nickname'],
                'email' => $data['email']
            ];
        }

        return $customerData;
    }

    /**
     * Adds reward points to customer once the review is accepted
     *
     * @param $customerId
     */
    public function addCustomerRewardPoints($customerId, $remoteId)
    {
        $points = $this->helper->getYotpoSettings(Data::YOTPO_REWARD);
        $rawComment = $this->helper->getYotpoSettings(Data::YOTPO_REWARD_MESSAGE);
        $interpolations = array(
            '%remoteId' => $remoteId,
            '%dispensePoints' => $points
        );
        $comment = str_replace(array_keys($interpolations), array_values($interpolations), $rawComment);

        if ($this->helper->getYotpoSettings(Data::YOTPO_REWARD_STATUS)) {
            $this->rewardsBalance->changePointsBalance(
                $customerId,
                $points,
                $comment
            );
        }
    }

    /**
     * Temporary field update mechanisms because setEmail doesn't work :/
     *
     * @param $reviewId
     * @param $email
     */
    public function updateEmailField($reviewId, $email)
    {
        $connection = $this->resourceConnection->getConnection();
        $tableName = $this->resourceConnection->getTableName('review_detail');

        $sql = $connection->select()
            ->from($tableName)
            ->where('review_id = ?', $reviewId);

        if ($connection->fetchAll($sql)) {
            $connection->update(
                $tableName,
                [
                    'email' => $email,
                ],
                [
                    'review_id = ?' => $reviewId
                ]
            );
        }
    }

    /**
     * Converts score from 100 points scale to 5
     *
     * @param $reviewId
     * @return float
     */
    public function calculateScore($reviewId) : float
    {
        $reviewSummary = $this->ratingFactory->create();
        $reviewSummary->getReviewSummary($reviewId);
        $reviewScore = (round(($reviewSummary->getSum() / $reviewSummary->getCount())) / 100) * 5;

        return round($reviewScore, 2);
    }


    /**
     * Checks if rewards are enabled
     *
     * @return mixed
     */
    protected function rewardStatus()
    {
        return $this->helper->getYotpoSettings(Data::YOTPO_REWARD_STATUS);
    }

}
