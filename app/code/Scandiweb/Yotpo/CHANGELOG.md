Scandiweb_Yotpo changelog
========================

0.1.1:
- Added Review remote id column for synchronizing, mapping

0.1.0:
- Added Email field for post in when customer account isn't logged in
- Added Reward points for review approval
- Added Reward point BE configuration
- Added Approval save post to Yotpo
- Added auto approve review on post when overall score is 5
