/**
 * @vendor Scandiweb
 * @module Scandiweb_Yotpo
 * @author Kristaps Stalidzāns <info@scandiweb.com>
 * @copyright Copyright (c) 2017 Scandiweb, Ltd (http://scandiweb.com)
 * @license http://opensource.org/licenses/afl-3.0.php Academic Free License (AFL 3.0)
 */
define([
    'uiComponent',
    'Magento_Review/js/view/review'
], function (Component, customerData, customer) {
    'use strict';

    return Component.extend({
        initialize: function () {
            this._super();

            this.review = customerData.get('review').extend({disposableCustomerData: 'review'});
        },
        nickname: function() {
            return this.review().nickname || customerData.get('customer')().firstname;
        },
        email: function () {
            return this.review().email;
        }
    });
});
