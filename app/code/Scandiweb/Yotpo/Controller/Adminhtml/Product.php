<?php
/**
 * @vendor Scandiweb
 * @module Scandiweb_Yotpo
 * @author Kristaps Stalidzāns <info@scandiweb.com>
 * @copyright Copyright (c) 2017 Magento. All rights reserved.
 * @license http://opensource.org/licenses/afl-3.0.php Academic Free License (AFL 3.0)
 */
namespace Scandiweb\Yotpo\Controller\Adminhtml;

use Magento\Backend\App\Action\Context;
use Magento\Framework\Registry;
use Magento\Review\Model\RatingFactory;
use Magento\Review\Model\ReviewFactory;
use Scandiweb\Yotpo\Model\DataModel;
use Scandiweb\Yotpo\Helper\Data;
use Magento\Review\Model\Rating\Option\Vote;
use Magento\Review\Model\ResourceModel\Review\Collection as ReviewCollection;

abstract class Product extends \Magento\Review\Controller\Adminhtml\Product
{
    /**
     * @var DataModel
     */
    protected $dataModel;

    /**
     * @var Data
     */
    protected $helper;

    /**
     * @var Vote
     */
    protected $vote;

    /**
     * Product constructor.
     * @param Context $context
     * @param Registry $coreRegistry
     * @param ReviewFactory $reviewFactory
     * @param RatingFactory $ratingFactory
     * @param DataModel $dataModel
     * @param Vote $vote
     * @param ReviewCollection $reviewCollection
     * @param Data $helper
     */
    public function __construct
    (
        Context $context,
        Registry $coreRegistry,
        ReviewFactory $reviewFactory,
        RatingFactory $ratingFactory,
        DataModel $dataModel,
        Vote $vote,
        ReviewCollection $reviewCollection,
        Data $helper
    )
    {
        $this->dataModel = $dataModel;
        $this->helper = $helper;
        $this->vote = $vote;

        parent::__construct(
            $context,
            $coreRegistry,
            $reviewFactory,
            $ratingFactory
        );
    }
}
