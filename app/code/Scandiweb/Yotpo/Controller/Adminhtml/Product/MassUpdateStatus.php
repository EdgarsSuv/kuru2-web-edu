<?php
/**
 * @vendor Scandiweb
 * @module Scandiweb_Yotpo
 * @author Kristaps Stalidzāns <info@scandiweb.com>
 * @copyright Copyright (c) 2017 Magento. All rights reserved.
 * @license http://opensource.org/licenses/afl-3.0.php Academic Free License (AFL 3.0)
 */
namespace Scandiweb\Yotpo\Controller\Adminhtml\Product;

use Scandiweb\Yotpo\Controller\Adminhtml\Product;
use Magento\Framework\Exception\LocalizedException;
use Magento\Framework\Controller\ResultFactory;
use Scandiweb\Yotpo\Model\DataInterface;

class MassUpdateStatus extends Product
{
    /**
     * @return \Magento\Backend\Model\View\Result\Redirect
     */
    public function execute()
    {
        $reviewsIds = $this->getRequest()->getParam('reviews');

        if (!is_array($reviewsIds)) {
            $this->messageManager->addErrorMessage(__('Please select review(s).'));
        } else {
            try {
                $status = $this->getRequest()->getParam('status');

                foreach ($reviewsIds as $reviewId) {
                    $model = $this->reviewFactory->create()->load($reviewId);
                    $savedStatus = $model['status_id'];
                    $model->setStatusId($status)->save()->aggregate();

                    /**
                     * Makes sure that status is processed once
                     */
                    if ($status == DataInterface::APPROVED && $savedStatus == DataInterface::PENDING) {
                        $this->dataModel->processReviewData($model->getData('entity_pk_value'), $model->getData(), $reviewId);
                    }
                }

                $this->messageManager->addSuccessMessage(
                    __('A total of %1 record(s) have been updated.', count($reviewsIds))
                );

            } catch (LocalizedException $e) {
                $this->messageManager->addErrorMessage($e->getMessage());
            } catch (\Exception $e) {
                $this->messageManager->addExceptionMessage(
                    $e,
                    __('Something went wrong while updating these review(s).')
                );
            }
        }

        /** @var \Magento\Backend\Model\View\Result\Redirect $resultRedirect */
        $resultRedirect = $this->resultFactory->create(ResultFactory::TYPE_REDIRECT);
        $resultRedirect->setPath('review/*/' . $this->getRequest()->getParam('ret', 'index'));

        return $resultRedirect;
    }
}
