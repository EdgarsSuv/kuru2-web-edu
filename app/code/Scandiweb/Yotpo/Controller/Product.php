<?php
/**
 * @vendor Scandiweb
 * @module Scandiweb_Yotpo
 * @author Kristaps Stalidzāns <info@scandiweb.com>
 * @copyright Copyright (c) 2017 Magento. All rights reserved.
 * @license http://opensource.org/licenses/afl-3.0.php Academic Free License (AFL 3.0)
 */
namespace Scandiweb\Yotpo\Controller;

use Magento\Catalog\Api\CategoryRepositoryInterface;
use Magento\Catalog\Api\ProductRepositoryInterface;
use Magento\Catalog\Model\Design;
use Magento\Customer\Model\Session;
use Magento\Framework\App\Action\Context;
use Magento\Framework\Data\Form\FormKey\Validator;
use Magento\Framework\Registry;
use Magento\Framework\Session\Generic;
use Magento\Review\Model\RatingFactory;
use Magento\Review\Model\ReviewFactory;
use Magento\Store\Model\StoreManagerInterface;
use Psr\Log\LoggerInterface;
use Scandiweb\Yotpo\Model\DataModel;
use Scandiweb\Yotpo\Helper\Data;

abstract class Product extends \Magento\Review\Controller\Product
{
    /**
     * Overall Score
     */
    const PERFECT_OVERALL_SCORE = 5;

    /**
     * @var DataModel
     */
    protected $dataModel;

    /**
     * @var Data
     */
    protected $helper;

    /**
     * Product constructor.
     * @param Context $context
     * @param Registry $coreRegistry
     * @param Session $customerSession
     * @param CategoryRepositoryInterface $categoryRepository
     * @param LoggerInterface $logger
     * @param ProductRepositoryInterface $productRepository
     * @param ReviewFactory $reviewFactory
     * @param RatingFactory $ratingFactory
     * @param Design $catalogDesign
     * @param Generic $reviewSession
     * @param StoreManagerInterface $storeManager
     * @param Validator $formKeyValidator
     * @param DataModel $dataModel
     * @param Data $helper
     */
   public function __construct
   (
       Context $context,
       Registry $coreRegistry,
       Session $customerSession,
       CategoryRepositoryInterface $categoryRepository,
       LoggerInterface $logger,
       ProductRepositoryInterface $productRepository,
       ReviewFactory $reviewFactory,
       RatingFactory $ratingFactory,
       Design $catalogDesign,
       Generic $reviewSession,
       StoreManagerInterface $storeManager,
       Validator $formKeyValidator,
       DataModel $dataModel,
       Data $helper
   )
   {
       $this->dataModel = $dataModel;
       $this->helper = $helper;

       parent::__construct(
           $context,
           $coreRegistry,
           $customerSession,
           $categoryRepository,
           $logger,
           $productRepository,
           $reviewFactory,
           $ratingFactory,
           $catalogDesign,
           $reviewSession,
           $storeManager,
           $formKeyValidator
       );
   }
}
