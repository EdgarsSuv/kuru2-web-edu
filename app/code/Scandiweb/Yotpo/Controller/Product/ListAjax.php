<?php
/**
 * @vendor Scandiweb
 * @module Scandiweb_Yotpo
 * @author Kristaps Stalidzāns <info@scandiweb.com>
 * @copyright Copyright (c) 2017 Magento. All rights reserved.
 * @license http://opensource.org/licenses/afl-3.0.php Academic Free License (AFL 3.0)
 */
namespace Scandiweb\Yotpo\Controller\Product;

use Magento\Framework\Controller\ResultFactory;
use Magento\Framework\Exception\LocalizedException;
use Scandiweb\Yotpo\Controller\Product as ProductController;

class ListAjax extends ProductController
{
    /**
     * Show list of product's reviews
     *
     * @return \Magento\Framework\View\Result\Layout
     * @throws LocalizedException
     */
    public function execute()
    {
        if (!$this->initProduct()) {
            throw new LocalizedException(__('Cannot initialize product'));
        } else {
            /** @var \Magento\Framework\View\Result\Layout $resultLayout */
            $resultLayout = $this->resultFactory->create(ResultFactory::TYPE_LAYOUT);
        }

        return $resultLayout;
    }
}
