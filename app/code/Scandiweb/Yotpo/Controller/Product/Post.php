<?php
/**
 * @vendor Scandiweb
 * @module Scandiweb_Yotpo
 * @author Kristaps Stalidzāns <info@scandiweb.com>
 * @copyright Copyright (c) 2017 Magento. All rights reserved.
 * @license http://opensource.org/licenses/afl-3.0.php Academic Free License (AFL 3.0)
 */
namespace Scandiweb\Yotpo\Controller\Product;

use Scandiweb\Yotpo\Controller\Product;
use Magento\Framework\Controller\ResultFactory;
use Magento\Review\Model\Review;
use Scandiweb\Yotpo\Model\DataInterface;

class Post extends Product
{
    /**
     * PostController for reviews
     *
     * @return \Magento\Framework\Controller\Result\Redirect
     */
    public function execute()
    {
        /** @var \Magento\Framework\Controller\Result\Redirect $resultRedirect */
        $resultRedirect = $this->resultFactory->create(ResultFactory::TYPE_REDIRECT);

        if (!$this->formKeyValidator->validate($this->getRequest())) {
            $resultRedirect->setUrl($this->_redirect->getRefererUrl());

            return $resultRedirect;
        }

        $data = $this->reviewSession->getFormData(true);

        if ($data) {
            $rating = [];

            if (isset($data['ratings']) && is_array($data['ratings'])) {
                $rating = $data['ratings'];
            }
        } else {
            $data = $this->getRequest()->getPostValue();
            $rating = $this->getRequest()->getParam('ratings', []);
        }

        if (($product = $this->initProduct()) && !empty($data)) {
            /** @var \Magento\Review\Model\Review $review */
            $review = $this->reviewFactory->create()->setData($data);
            $review->unsetData('review_id');
            $validate = $review->validate();

            if ($validate === true) {
                try {
                    $review->setEntityId($review->getEntityIdByCode(Review::ENTITY_PRODUCT_CODE))
                        ->setEntityPkValue($product->getId())
                        ->setStatusId(Review::STATUS_PENDING)
                        ->setCustomerId($this->customerSession->getCustomerId())
                        ->setStoreId($this->storeManager->getStore()->getId())
                        ->setStores([$this->storeManager->getStore()->getId()])
                        ->save();
                    $reviewId = $review->getId();
                    $reviewMessage = 'You submitted your review for moderation.';

                    if (!$this->customerSession->isLoggedIn()) {
                        $this->dataModel->updateEmailField($review->getId(), $data['email']);
                    }

                    foreach ($rating as $ratingId => $optionId) {
                        $this->ratingFactory->create()
                            ->setRatingId($ratingId)
                            ->setReviewId($review->getId())
                            ->setCustomerId($this->customerSession->getCustomerId())
                            ->addOptionVote($optionId, $product->getId());
                    }


                    if ($this->helper->getAutoAllowStatus() && $this->dataModel->calculateScore($review->getId()) == self::PERFECT_OVERALL_SCORE) {
                        $model = $this->reviewFactory->create()->load($reviewId);
                        $model->setStatusId(DataInterface::APPROVED)->save()->aggregate();
                        $this->dataModel->processReviewData($review->getEntityPkValue(), $review->getData(), $reviewId);
                        $reviewMessage = 'You submitted your review is published';
                    }

                    $review->aggregate();
                    $this->messageManager->addSuccessMessage(__($reviewMessage));
                } catch (\Exception $e) {
                    $this->reviewSession->setFormData($data);
                    $this->messageManager->addErrorMessage(__('We can\'t post your review right now.'));
                }
            } else {
                $this->reviewSession->setFormData($data);

                if (is_array($validate)) {
                    foreach ($validate as $errorMessage) {
                        $this->messageManager->addErrorMessage($errorMessage);
                    }
                } else {
                    $this->messageManager->addErrorMessage(__('We can\'t post your review right now.'));
                }
            }
        }

        $redirectUrl = $this->reviewSession->getRedirectUrl(true);
        $resultRedirect->setUrl($redirectUrl ?: $this->_redirect->getRedirectUrl());

        return $resultRedirect;
    }
}
