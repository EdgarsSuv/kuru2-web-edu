<?php
/**
 * @vendor Scandiweb
 * @module Scandiweb_Yotpo
 * @author Kristaps Stalidzāns <info@scandiweb.com>
 * @copyright Copyright (c) 2017 Magento. All rights reserved.
 * @license http://opensource.org/licenses/afl-3.0.php Academic Free License (AFL 3.0)
 */
namespace Scandiweb\Yotpo\Controller\Product;

use Magento\Framework\Controller\ResultFactory;
use Magento\Framework\Controller\Result\Redirect;
use Scandiweb\Yotpo\Controller\Product as ProductController;

class PostQuestion extends ProductController
{
    /**
     * Submit new review action
     *
     * @return Redirect
     */
    public function execute()
    {
        /** @var Redirect $resultRedirect */
        $resultRedirect = $this->resultFactory->create(ResultFactory::TYPE_REDIRECT);

        if (!$this->formKeyValidator->validate($this->getRequest())) {
            $resultRedirect->setUrl($this->_redirect->getRefererUrl());

            return $resultRedirect;
        }

        $data = $this->getRequest()->getParams();

        if (($product = $this->initProduct()) && !empty($data)) {
            if ($this->customerSession->isLoggedIn()) {
                $data['customer_id'] = $this->customerSession->getId();
            } else {
                $data['customer_id'] = null;
            }

            $this->dataModel->processQuestionData($product->getId(), $data);
        }

        $redirectUrl = $this->reviewSession->getRedirectUrl(true);
        $resultRedirect->setUrl($redirectUrl ?: $this->_redirect->getRedirectUrl());

        return $resultRedirect;
    }
}
