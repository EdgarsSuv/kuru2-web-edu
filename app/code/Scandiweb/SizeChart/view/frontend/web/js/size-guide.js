/**
 * @vendor Scandiweb
 * @module Scandiweb_SizeChart
 * @author Kristaps Stalidzāns <info@scandiweb.com>
 * @copyright Copyright (c) 2017 Magento. All rights reserved.
 * @license http://opensource.org/licenses/afl-3.0.php Academic Free License (AFL 3.0)
 */
define([
    'jquery',
    'magnificPopup'
], function($, magnificPopup) {
    $(document).ready(function() {
        $('.size-guide').magnificPopup({
            items: {
                src:  $('.size-guide').attr('data-link') + '?isAjax=true'
            },
            type: 'iframe',
            preloader: true,
            fixedBgPos: 'auto'
        });
    });
});
