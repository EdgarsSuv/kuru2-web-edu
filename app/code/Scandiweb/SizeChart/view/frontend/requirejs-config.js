/**
 * @vendor Scandiweb
 * @module Scandiweb_SizeChart
 * @author Kristaps Stalidzāns <info@scandiweb.com>
 * @copyright Copyright (c) 2017 Magento. All rights reserved.
 * @license http://opensource.org/licenses/afl-3.0.php Academic Free License (AFL 3.0)
 */
var config = {
	map : {
		'*': {
			magnificPopup: 'Scandiweb_SizeChart/js/jquery.magnific-popup.min',
			sizeGuide: 'Scandiweb_SizeChart/js/size-guide'
		}
	},
   	shim: {
        'Scandiweb_SizeChart/js/jquery.magnific-popup.min': ['jquery'],
        'Scandiweb_SizeChart/js/size-guide': ['jquery', 'Scandiweb_SizeChart/js/jquery.magnific-popup.min']
   	}
};
