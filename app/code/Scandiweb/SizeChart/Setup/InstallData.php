<?php
/**
 * @vendor Scandiweb
 * @module Scandiweb_SizeChart
 * @author Kristaps Stalidzāns <info@scandiweb.com>
 * @copyright Copyright (c) 2017 Magento. All rights reserved.
 * @license http://opensource.org/licenses/afl-3.0.php Academic Free License (AFL 3.0)
 */
namespace Scandiweb\SizeChart\Setup;

use Magento\Catalog\Model\Product;
use Magento\Catalog\Model\ResourceModel\Eav\Attribute;
use Magento\Eav\Setup\EavSetupFactory;
use Magento\Framework\Setup\InstallDataInterface;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\ModuleDataSetupInterface;

class InstallData implements InstallDataInterface
{
    /**
     * @var EavSetupFactory
     */
    protected $_eavSetupFactory;

    /**
     * InstallData constructor.
     * @param EavSetupFactory $eavSetupFactory
     */
    public function __construct
    (
        EavSetupFactory $eavSetupFactory
    )
    {
        $this->_eavSetupFactory = $eavSetupFactory;
    }

    /**
     * @param ModuleDataSetupInterface $setup
     * @param ModuleContextInterface $context
     */
    public function install(ModuleDataSetupInterface $setup, ModuleContextInterface $context)
    {
        /** @var EavSetupFactory $eavSetupFactory */
        $eavSetup = $this->_eavSetupFactory->create(['setup' => $setup]);
        $entityType = Product::ENTITY;

        // Create text attributes
        if (!$eavSetup->getAttribute($entityType, 'size_guide_cms_block')) {
            $eavSetup->addAttribute(
                $entityType,
                'size_guide_cms_block',
                [
                    'global' => Attribute::SCOPE_GLOBAL,
                    'type' => 'int',
                    'label' => 'Size Guide CMS block',
                    'input' => 'select',
                    'required' => false,
                    'user_defined' => false,
                    'searchable' => false,
                    'filterable' => false,
                    'comparable' => false,
                    'visible_on_front' => true,
                    'visible_in_advanced_search' => false,
                    'apply_to' => '',
                    'is_visible_in_grid' => false,
                    'is_filterable_in_grid' => false,
                    'used_in_product_listing' => true,
                    'source' => 'Magento\Catalog\Model\Category\Attribute\Source\Page'
                ]
            );
        }

        $eavSetup->addAttributeToSet($entityType,
            $eavSetup->getAttributeSetId($entityType, 'Default'),
            'design',
            $eavSetup->getAttributeId($entityType, 'size_guide_cms_block')
        );
    }
}
