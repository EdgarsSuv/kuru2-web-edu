Scandiweb_SizeChart changelog
========================

0.1.0:
- Added SizeChart button
- Added SizeChart modal popup
- Added JS for SizeChart functionality
- Added Controller for rendering product size chart page
