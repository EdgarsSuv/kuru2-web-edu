<?php
/**
 * @vendor Scandiweb
 * @module Scandiweb_SizeChart
 * @author Kristaps Stalidzāns <info@scandiweb.com>
 * @copyright Copyright (c) 2017 Magento. All rights reserved.
 * @license http://opensource.org/licenses/afl-3.0.php Academic Free License (AFL 3.0)
 */
namespace Scandiweb\SizeChart\Block;

use Magento\Cms\Block\BlockFactory;
use Magento\Catalog\Model\ProductFactory;
use Magento\Framework\View\Element\Template;

class SizeChart extends Template
{
    /**
     * @var BlockFactory
     */
    protected $cmsBlockFactory;

    /**
     * @var ProductFactory
     */
    protected $productFactory;

    /**
     * Chart constructor.
     * @param Template\Context $context
     * @param BlockFactory $cmsBlockFactory
     * @param ProductFactory $productFactory
     * @internal param Block $cmsBlock
     */
    public function __construct
    (
        Template\Context $context,
        BlockFactory $cmsBlockFactory,
        ProductFactory $productFactory
    )
    {
        $this->cmsBlockFactory = $cmsBlockFactory;
        $this->productFactory = $productFactory;
        parent::__construct($context);
    }

    /**
     * Returns CMS Block
     *
     * @return mixed
     */
    public function getSizeChartBlock()
    {
        $params = $this->getRequest()->getParams();

        if (array_key_exists('chart', $params) & (strlen($params['chart']) > 0) ) {
            $cmsBlockId = $this->productFactory->create()
                ->load($params['chart'])
                ->getData('size_guide_cms_block');

            $cmsBlockData = $this->cmsBlockFactory->create()
                ->setBlockId($cmsBlockId)->toHtml();

            return $cmsBlockData;
        }
    }
}
