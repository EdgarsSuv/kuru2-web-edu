<?php
/**
 * @vendor Scandiweb
 * @module Scandiweb_SizeChart
 * @author Kristaps Stalidzāns <info@scandiweb.com>
 * @copyright Copyright (c) 2017 Magento. All rights reserved.
 * @license http://opensource.org/licenses/afl-3.0.php Academic Free License (AFL 3.0)
 */
namespace Scandiweb\SizeChart\Block;

use Magento\Catalog\Block\Product\View;

class SizeChartButton extends View
{
    /**
     * Returns Attribute
     *
     * @return mixed
     */
    public function getSizeChartAttribute()
    {
        return $this->getProduct()->getData('size_guide_cms_block');
    }

    /**
     * Returns Size Chart Cms block
     *
     * @return string
     */
    public function getSizeChartLink()
    {
        $productId = $this->getProduct()->getId();

        return $this->getUrl('sizechart/block/get', [ 'chart' => $productId ]);
    }

    /**
     * This is needed to take the correct templates
     *
     * @return string
     */
    protected function _toHtml()
    {
        $this->setModuleName($this->extractModuleName('Magento\Catalog\Block\Product\View'));

        return parent::_toHtml();
    }
}
