<?php
/**
 * @vendor Scandiweb
 * @module Scandiweb_UniversalCustomerPassword
 * @author Kristaps Stalidzāns <info@scandiweb.com>
 * @copyright Copyright (c) 2017 Magento. All rights reserved.
 * @license http://opensource.org/licenses/afl-3.0.php Academic Free License (AFL 3.0)
 */

namespace Scandiweb\UniversalCustomerPassword\Helper;

use Magento\Framework\App\Helper\AbstractHelper;
use Magento\Framework\App\Helper\Context;
use Magento\Store\Model\ScopeInterface;
use Magento\Backend\App\ConfigInterface;
use Scandiweb\UniversalCustomerPassword\Logger\Logger;

class Data extends AbstractHelper
{
    /**
     * @var Logger
     */
    private $logger;

    /**
     * Data constructor.
     * @param Context $context
     * @param Logger $logger
     */
    public function __construct(
        Context $context,
        Logger $logger
    )
    {
        parent::__construct($context);
        $this->logger = $logger;
    }

    /**
     * Check if the client remote address is allowed ip
     *
     * @param $allowedIps
     * @param $passwordHashTest
     * @return bool
     */
    public function isAllowed($allowedIps, $passwordHashTest)
    {
        $allow = false;

        $remoteAddr = $this->_remoteAddress->getRemoteAddress();

        /**
         * Checks if password is true then logs operator ip when action is done
         */
        if ($passwordHashTest) {
            $this->logger->addNotice(__('An operator with ip: ' . $remoteAddr . ' Tries to login with universal password'));
        }

        if (!empty($allowedIps) && !empty($remoteAddr)) {
            $allowedIps = preg_split('#\s*,\s*#', $allowedIps, null, PREG_SPLIT_NO_EMPTY);

            if ((array_search($remoteAddr, $allowedIps) !== false) && !array_search($this->_httpHeader->getHttpHost(), $allowedIps)) {
                $allow = true;
            }
        }

        /**
         * Checks if ip is in whitelist an password is correct. If so logs message that ip is white listed or not
         * Added these messages since IPv6 is hard to test locally and could be that AWS servers translate ips a different way with ip translation.
         */
        if ($allow && $passwordHashTest) {
            $this->logger->addNotice(__('An operator with ip: ' . $remoteAddr . ' is in whitelist'));
        } else if (!$allow && $passwordHashTest) {
            $this->logger->addNotice(__('An operator with ip: ' . $remoteAddr . ' is not whitelisted or not in whitlist'));
        }

        return $allow;
    }
}
