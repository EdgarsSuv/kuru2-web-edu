<?php
/**
 * @vendor Scandiweb
 * @module Scandiweb_UniversalCustomerPassword
 * @author Kristaps Stalidzāns <info@scandiweb.com>
 * @copyright Copyright (c) 2017 Magento. All rights reserved.
 * @license http://opensource.org/licenses/afl-3.0.php Academic Free License (AFL 3.0)
 */

namespace Scandiweb\UniversalCustomerPassword\Model\Config\Backend;

use Magento\Framework\App\Cache\TypeListInterface;
use Magento\Framework\App\Config\ScopeConfigInterface;
use Magento\Framework\App\Config\Value;
use Magento\Framework\Data\Collection\AbstractDb;
use Magento\Framework\Escaper;
use Magento\Framework\Message\ManagerInterface;
use Magento\Framework\Encryption\EncryptorInterface;
use Magento\Framework\Model\Context;
use Magento\Framework\Model\ResourceModel\AbstractResource;
use Magento\Framework\Registry;

/**
 * Backend model for validating ip addresses entered in Developer Client Restrictions
 *
 * Class AllowedIps
 */
class AllowedIps extends Value
{
    /**
     * @var ManagerInterface
     */
    private $messageManager;

    /**
     * Escaper
     *
     * @var Escaper
     */
    protected $escaper;

    /**
     * @var EncryptorInterface
     */
    protected $_encryptor;

    /**
     * Constructor
     *
     * @param Context $context
     * @param Registry $registry
     * @param ScopeConfigInterface $config
     * @param TypeListInterface $cacheTypeList
     * @param ManagerInterface $messageManager
     * @param Escaper $escaper
     * @param AbstractResource $resource
     * @param AbstractDb $resourceCollection
     * @param EncryptorInterface $encryptor
     * @param array $data
     */
    public function __construct(
        Context $context,
        Registry $registry,
        ScopeConfigInterface $config,
        TypeListInterface $cacheTypeList,
        ManagerInterface $messageManager,
        Escaper $escaper,
        AbstractResource $resource = null,
        AbstractDb $resourceCollection = null,
        EncryptorInterface $encryptor,
        array $data = []
    ) {
        $this->messageManager = $messageManager;
        $this->escaper = $escaper;
        $this->_encryptor = $encryptor;

        parent::__construct(
            $context,
            $registry,
            $config,
            $cacheTypeList,
            $resource,
            $resourceCollection,
            $data
        );
    }

    /**
     * Magic method called during class serialization
     *
     * @return string[]
     */
    public function __sleep()
    {
        $properties = parent::__sleep();
        return array_diff($properties, ['_encryptor']);
    }

    /**
     * Magic method called during class un-serialization
     *
     * @return void
     */
    public function __wakeup()
    {
        parent::__wakeup();
        $this->_encryptor = ObjectManager::getInstance()->get(
            'Magento\Framework\Encryption\EncryptorInterface'
        );
    }

    /**
     * Decrypt value after loading
     *
     * @return void
     */
    protected function _afterLoad()
    {
        $value = (string)$this->getValue();

        if (!empty($value) && ($decrypted = $this->_encryptor->decrypt($value))) {
            $this->setValue($decrypted);
        }
    }

    /**
     * Validate ip addresses before save
     *
     * @return $this
     */
    public function beforeSave()
    {
        $allowedIpsRaw = $this->escaper->escapeHtml($this->getValue());
        $noticeMsgArray = [];
        $allowedIpsArray = [];

        if (empty($allowedIpsRaw)) {
            return parent::beforeSave();
        }

        $dataArray = explode(',', $allowedIpsRaw);

        foreach ($dataArray as $data) {
            if (filter_var(trim($data), FILTER_VALIDATE_IP, FILTER_FLAG_IPV4) ||
                filter_var(trim($data), FILTER_VALIDATE_IP, FILTER_FLAG_IPV6)) {
                $allowedIpsArray[] = $data;
            } else {
                $noticeMsgArray[] = $data;
            }
        }

        $noticeMsg = implode(',', $noticeMsgArray);

        if (!empty($noticeMsgArray)) {
            $this->messageManager->addNoticeMessage(
                __(
                    __('The following invalid values cannot be saved: %values', ['values' => $noticeMsg])
                )
            );
        }

        $encrypted = $this->_encryptor->encrypt(implode(',', $allowedIpsArray));
        $this->setValue($encrypted);

        return parent::beforeSave();
    }

    /**
     * Process config value
     *
     * @param string $value
     * @return string
     */
    public function processValue($value)
    {
        return $this->_encryptor->decrypt($value);
    }
}
