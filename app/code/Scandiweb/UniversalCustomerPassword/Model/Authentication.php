<?php
/**
 * @vendor Scandiweb
 * @module Scandiweb_UniversalCustomerPassword
 * @author Kristaps Stalidzāns <info@scandiweb.com>
 * @copyright Copyright (c) 2017 Magento. All rights reserved.
 * @license http://opensource.org/licenses/afl-3.0.php Academic Free License (AFL 3.0)
 */
namespace Scandiweb\UniversalCustomerPassword\Model;

use Magento\Customer\Api\CustomerRepositoryInterface;
use Magento\Customer\Model\ResourceModel\CustomerRepository;
use Magento\Customer\Model\CustomerAuthUpdate;
use Magento\Backend\App\ConfigInterface;
use Magento\Framework\Encryption\EncryptorInterface;
use Magento\Framework\Exception\InvalidEmailOrPasswordException;
use Magento\Framework\Exception\State\UserLockedException;
use Magento\Framework\Stdlib\DateTime;
use Magento\Customer\Model\CustomerRegistry;
use Magento\Framework\App\Config\ScopeConfigInterface;
use Magento\Store\Model\ScopeInterface;
use Magento\Customer\Model\Authentication as CustomerAuthentication;
use Scandiweb\UniversalCustomerPassword\Logger\Logger;
use Scandiweb\UniversalCustomerPassword\Helper\Data;

class Authentication extends CustomerAuthentication
{
    /**
     * Config path to universal password module status
     */
    const UNIVERSAL_PASSWORD_MODULE_STATUS = 'scandiweb/universalcustomer/status';

    /**
     * Config path to universal password
     */
    const UNIVERSAL_HASH = 'scandiweb/universalcustomer/hash';

    /**
     * Config allow ips config path
     */
    const UNIVERSAL_ALLOWED_IPS = 'scandiweb/universalcustomer/allow_ips';

    /**
     * @var ScopeConfigInterface
     */
    protected $scopeConfig;

    /**
     * @var Logger
     */
    protected $logger;

    /**
     * @var Data
     */
    protected $ipHelper;

    /**
     * @param CustomerRepositoryInterface $customerRepository
     * @param CustomerRegistry $customerRegistry
     * @param ConfigInterface $backendConfig
     * @param DateTime $dateTime
     * @param EncryptorInterface $encryptor
     * @param ScopeConfigInterface $scopeConfig
     * @param Logger $logger
     * @param Data $ipHelper
     */
    public function __construct(
        CustomerRepositoryInterface $customerRepository,
        CustomerRegistry $customerRegistry,
        ConfigInterface $backendConfig,
        DateTime $dateTime,
        EncryptorInterface $encryptor,
        ScopeConfigInterface $scopeConfig,
        Logger $logger,
        Data $ipHelper
    ) {
        parent::__construct(
            $customerRepository,
            $customerRegistry,
            $backendConfig,
            $dateTime,
            $encryptor
        );
        $this->scopeConfig = $scopeConfig;
        $this->logger = $logger;
        $this->ipHelper = $ipHelper;
    }

    /**
     * Authenticate function that checks hashes from password or universal password
     *
     * {@inheritdoc}
     */
    public function authenticate($customerId, $password)
    {
        $customerSecure = $this->customerRegistry->retrieveSecureData($customerId);
        $hash = $customerSecure->getPasswordHash();
        $validateHash = $this->encryptor->validateHash($password, $hash);
        $validateUniversalHash = $this->validateUniversalHash($password);

        /**
         * User won't be authenticated when password is incorrect, module is disabled or universal
         * password hash won't match
         */
        if ($validateHash || $validateUniversalHash) {
            if ($validateUniversalHash && !$validateHash) {
                $this->logger->addNotice(__('Customer ' . $customerId . ' : Logged with universal password'));
            }

            return true;
        } else {
            $this->processAuthenticationFailure($customerId);

            if ($this->isLocked($customerId)) {
                $this->logger->addError(__('Customer ' . $customerId . ' : Accound locked'));
                throw new UserLockedException(__('The account is locked.'));
            }

            $this->logger->addError(__('Customer ' . $customerId . ' : Invalid login or password'));
            throw new InvalidEmailOrPasswordException(__('Invalid login or password.'));
        }
    }

    /**
     * Checks if passwords match
     *
     * @param $password
     * @return bool
     */
    protected function validateUniversalHash($password)
    {
        /**
         * Checks if module is enabled and password is set and if ip is allowed
         */
        if ($this->getModuleStatus()) {
            $hash = $this->getHash();
            $passwordHashTest = $this->encryptor->validateHash($password, $hash);

            if (!is_null($hash) && $this->validateUserIp($passwordHashTest)) {
                $status = $this->encryptor->validateHash($password, $hash);
            } else {
                $status = false;
            }
        } else {
            $status = false;
        }

        return $status;
    }

    /**
     * Check if module is enabled
     *
     * @return bool
     */
    protected function getModuleStatus()
    {
        return $this->scopeConfig->getValue(self::UNIVERSAL_PASSWORD_MODULE_STATUS, ScopeInterface::SCOPE_STORE);
    }

    /**
     * Get universal password hash
     *
     * @return string
     */
    protected function getHash()
    {
        return $this->backendConfig->getValue(self::UNIVERSAL_HASH, ScopeInterface::SCOPE_STORE);
    }

    /**
     * Gets set of IP that are allowed to login
     *
     * @param $passwordHashTest
     * @return string
     */
    protected function validateUserIp($passwordHashTest)
    {
        $value = $this->scopeConfig->getValue(self::UNIVERSAL_ALLOWED_IPS, ScopeInterface::SCOPE_STORE);

        return $this->ipHelper->isAllowed($this->encryptor->decrypt($value), $passwordHashTest);
    }
}
