# Universal customer login

Universal customer login is a extension that allows site admins with predefined password

## Installation

```
composer config repositories.module-universalcustomerpassword git git@bitbucket.org:scandiwebassets/universalcustomerpassword.git
composer require scandiweb/module-universalcustomerpassword:0.1.2
php -f bin/magento setup:upgrade
```

## Configuration

Stores -> Configuration -> Scandiweb -> Universal Customer Password
