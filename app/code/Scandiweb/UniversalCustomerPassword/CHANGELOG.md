Scandiweb_UniversalCustomerPassword changelog
========================

0.1.0:
- added admin config for module
- created a preference for authentication module
- added function to get module status and password
- extended authentication function with checks for BE password
- added hash model for password storing
- added hash validated function to compare universal password
- added option to hash and ecrypt password instead store it in plane text
- added admin validation for password

0.1.1:
- Added logger
- Name fixes

0.1.2:
- Added IP address checker for password usage
- Added last config check
