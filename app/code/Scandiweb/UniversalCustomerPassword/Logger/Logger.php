<?php
/**
 * @vendor Scandiweb
 * @module Scandiweb_UniversalCustomerPassword
 * @author Kristaps Stalidzāns <info@scandiweb.com>
 * @copyright Copyright (c) 2017 Magento. All rights reserved.
 * @license http://opensource.org/licenses/afl-3.0.php Academic Free License (AFL 3.0)
 */

namespace Scandiweb\UniversalCustomerPassword\Logger;

use Magento\Framework\Logger\Monolog;

class Logger extends Monolog
{
}