<?php
/**
 * Scandiweb_DraggableProducts
 *
 * @category    DraggableProducts
 * @package     Scandiweb_DraggableProducts
 * @author      Aleksandrs Vasilievs <aleksandrsv@scandiweb.com>
 * @copyright   Copyright (c) 2017 Scandiweb, Ltd (http://scandiweb.com)
 */
namespace Scandiweb\DraggableProducts\Cron;

class Positions
{
    /**
     * @var \Scandiweb\DraggableProducts\Helper\Position
     */
    protected $helper;

    /**
     * @var \Magento\Catalog\Model\ResourceModel\Category\CollectionFactory
     */
    protected $_categoryCollection;

    /**
     * Positions constructor.
     * @param \Magento\Catalog\Model\ResourceModel\Category\CollectionFactory $categoryCollection
     * @param \Scandiweb\DraggableProducts\Helper\Position $dataHelper
     */
    public function __construct(
        \Magento\Catalog\Model\ResourceModel\Category\CollectionFactory $categoryCollection,
        \Scandiweb\DraggableProducts\Helper\Position $dataHelper
    ) {
    
        $this->_categoryCollection = $categoryCollection;
        $this->helper = $dataHelper;
    }

    public function execute()
    {
        $collection = $this->_categoryCollection->create()
            ->addAttributeToSelect('*');

        foreach ($collection as $_category) {
            if (!$_category->getIsActive()) {
                continue;
            }

            if ($_category->getData('autosort') && $_category->getProductsPosition()) {
                $this->helper->changePositions($_category);
                $_category->save();
            }
        }

        return $this;
    }
}
