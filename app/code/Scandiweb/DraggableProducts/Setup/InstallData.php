<?php
/**
 * Scandiweb_DraggableProducts
 *
 * @category    DraggableProducts
 * @package     Scandiweb_DraggableProducts
 * @author      Aleksandrs Vasilievs <aleksandrsv@scandiweb.com>
 * @copyright   Copyright (c) 2017 Scandiweb, Ltd (http://scandiweb.com)
 */
namespace Scandiweb\DraggableProducts\Setup;

use Magento\Framework\Setup\InstallDataInterface;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\ModuleDataSetupInterface;
use Magento\Eav\Setup\EavSetup;
use Magento\Eav\Setup\EavSetupFactory;
use Magento\Framework\Module\Setup\Migration;
use Magento\Catalog\Model\Category;
use Magento\Eav\Model\Entity\Attribute\ScopedAttributeInterface;

/**
 * Scandiweb_DraggableProducts InstallData
 */
class InstallData implements InstallDataInterface
{
    /**
     * @var EavSetupFactory
     */
    private $eavSetupFactory;

    /**
     *
     * @param EavSetupFactory $eavSetupFactory
     */
    public function __construct(EavSetupFactory $eavSetupFactory)
    {
        $this->eavSetupFactory = $eavSetupFactory;
    }

    /**
     * Installs DB schema for a module Scandiweb_DraggableProducts
     *
     * @param SchemaSetupInterface $setup
     * @param ModuleContextInterface $context
     * @return void
     */
    public function install(ModuleDataSetupInterface $setup, ModuleContextInterface $context)
    {
        $installer = $setup->createMigrationSetup();
        $setup->startSetup();

        $installer->appendClassAliasReplace(
            'scandiweb_draggableproducts_rule',
            'conditions_serialized',
            Migration::ENTITY_TYPE_MODEL,
            Migration::FIELD_CONTENT_TYPE_SERIALIZED,
            ['rule_id']
        );

        $installer->doUpdateClassAliases();

        /** @var EavSetup $eavSetup */
        $eavSetup = $this->eavSetupFactory->create(['setup' => $setup]);
        $eavSetup->addAttribute(
            Category::ENTITY,
            'sw_hero_products',
            [
                'type' => 'varchar',
                'label' => 'Draggable Hero Products',
                'input' => 'text',
                'required' => false,
                'sort_order' => 100,
                'backend'   => 'Scandiweb\DraggableProducts\Model\Category\Attribute\Backend\Hero',
                'global' => ScopedAttributeInterface::SCOPE_STORE,
                'group' => 'Products in Category',
            ]
        );
        $eavSetup->addAttribute(
            Category::ENTITY,
            'autosort',
            [
                'type' => 'varchar',
                'label' => 'Draggable Autosort',
                'input' => 'text',
                'required' => false,
                'sort_order' => 5,
                'global' => ScopedAttributeInterface::SCOPE_STORE,
                'group' => 'Products in Category',
            ]
        );
        $eavSetup->addAttribute(
            Category::ENTITY,
            'is_smart',
            [
                'type' => 'int',
                'label' => 'Draggable Products Smart Category',
                'input' => 'select',
                'source' => 'Magento\Eav\Model\Entity\Attribute\Source\Boolean',
                'global' => ScopedAttributeInterface::SCOPE_GLOBAL,
                'required' => false,
                'sort_order' => 10,
                'default' => '0',
                'group' => 'Products in Category',
            ]
        );
        $setup->endSetup();
    }
}
