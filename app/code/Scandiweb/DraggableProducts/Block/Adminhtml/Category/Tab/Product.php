<?php
/**
 * Scandiweb_DraggableProducts
 *
 * @category    DraggableProducts
 * @package     Scandiweb_DraggableProducts
 * @author      Aleksandrs Vasilievs <aleksandrsv@scandiweb.com>
 * @copyright   Copyright (c) 2017 Scandiweb, Ltd (http://scandiweb.com)
 */
namespace Scandiweb\DraggableProducts\Block\Adminhtml\Category\Tab;

use Magento\Backend\Block\Widget\Grid;
use Magento\Backend\Block\Widget\Grid\Extended;
use Magento\Catalog\Model\Product\Visibility;
use Magento\Catalog\Model\Product\Type;
use Scandiweb\DraggableProducts\Block\Adminhtml\Category\Tab\Product\Grid\Renderer\Position;

class Product extends \Magento\Catalog\Block\Adminhtml\Category\Tab\Product
{
    /**
     * @param \Magento\Backend\Block\Template\Context $context
     * @param \Magento\Backend\Helper\Data $backendHelper
     * @param \Magento\Catalog\Model\ProductFactory $productFactory
     * @param \Magento\Framework\Registry $coreRegistry
     * @param array $data
     */
    public function __construct(
        \Magento\Backend\Block\Template\Context $context,
        \Magento\Backend\Helper\Data $backendHelper,
        \Magento\Catalog\Model\ProductFactory $productFactory,
        \Magento\Framework\Registry $coreRegistry,
        array $data = []
    ) {
        $this->_productFactory = $productFactory;
        $this->_coreRegistry = $coreRegistry;
        Extended::__construct($context, $backendHelper, $data);
    }

    /**
     * Default view for Merchandiser
     */
    const DEFAULT_VIEW = 'list';

    /**
     * @var string
     */
    protected $_template = 'Scandiweb_DraggableProducts::widget/grid.phtml';

    /**
     * Extend collection adding product thumbnail
     * @return Grid
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    protected function _prepareCollection()
    {
        if ($this->getCategory()->getId()) {
            $this->setDefaultFilter(['in_category' => 1]);
        }
        $collection = $this->_productFactory->create()->getCollection()
            ->addFieldToFilter([
                [
                    'attribute' => 'type_id',
                    'eq' => Type::TYPE_SIMPLE
                ],
                [
                    'attribute' => 'product_duplicate',
                    'eq' => 1
                ]
            ])->addAttributeToFilter(
                'visibility',
                [
                    'in' => [
                        Visibility::VISIBILITY_IN_CATALOG, Visibility::VISIBILITY_BOTH
                    ]
                ]
            )->addAttributeToSelect(
                'name'
            )->addAttributeToSelect(
                'sku'
            )->addAttributeToSelect(
                'status'
            )->addAttributeToSelect(
                'price'
            )->addAttributeToSelect(
                'type_id'
            )->addAttributeToSelect(
                'thumbnail'
            )->joinField(
                'position',
                'catalog_category_product',
                'position',
                'product_id=entity_id',
                'category_id=' . (int)$this->getRequest()->getParam('id', 0),
                'left'
            )->joinField(
                'is_in_stock',
                'cataloginventory_stock_item',
                'is_in_stock',
                'product_id=entity_id',
                '{{table}}.stock_id=1',
                'left'
            );
        $storeId = (int)$this->getRequest()->getParam('store', 0);
        if ($storeId > 0) {
            $collection->addStoreFilter($storeId);
        }

        $this->setCollection($collection);

        if ($this->getCategory()->getProductsReadonly()) {
            $productIds = $this->_getSelectedProducts();
            if (empty($productIds)) {
                $productIds = 0;
            }
            $this->getCollection()->addFieldToFilter('entity_id', ['in' => $productIds]);
        }

        return \Magento\Backend\Block\Widget\Grid\Extended::_prepareCollection();
    }

    /**
     * @return Extended
     * @throws \Exception
     */
    protected function _prepareColumns()
    {
        parent::_prepareColumns();

        if ($this->getDragValue()) {
            $this->setDefaultSort('position');
            $this->setDefaultDir('asc');
        }

        $this->setDefaultLimit(200);

        $this->addColumnAfter(
            'status',
            [
                'header' => __('Status'),
                'sortable' => true,
                'index' => 'status',
                'type' => 'options',
                'options' => [
                    '' => sprintf('%s & %s', __('Enabled'), __('Disabled')),
                    '1' => __('Enabled'),
                    '2' => __('Disabled')
                ],
                'header_css_class' => 'col-type',
                'column_css_class' => 'col-type'
            ],
            'price'
        );

        $this->addColumnAfter(
            'is_in_stock',
            [
                'header' => __('Stock Status'),
                'sortable' => true,
                'index' => 'is_in_stock',
                'type' => 'options',
                'options' => [
                    '1' => __('In Stock'),
                    '0' => __('Out of Stock')
                ],
                'header_css_class' => 'col-type',
                'column_css_class' => 'col-type'
            ],
            'status'
        );

        $this->addColumnAfter(
            'image',
            [
                'header' => __('Thumbnail'),
                'index' => 'image',
                'renderer' => '\Scandiweb\DraggableProducts\Block\Adminhtml\Category\Tab\Product\Grid\Renderer\Image',
                'filter' => false,
                'sortable' => false,
                'column_css_class' => 'data-grid-thumbnail-cell'
            ],
            'entity_id'
        );

        $this->addColumnAfter(
            'type_id',
            [
                'header' => __('Type'),
                'sortable' => true,
                'index' => 'type_id',
                'header_css_class' => 'col-type',
                'column_css_class' => 'col-type'
            ],
            'name'
        );

        $this->addColumn(
            'drag',
            [
                'header' => __('Drag'),
                'type' => 'number',
                'index' => 'position',
                'renderer' => Position::class,
            ]
        );

        $this->sortColumnsByOrder();

        return $this;
    }

    /**
     * Get Merchandiser view (list / grid)
     *
     * @return string
     */
    public function getDragValue()
    {
        if (!$this->_backendSession->getDragValue()) {
            return self::DEFAULT_VIEW;
        }

        return $this->_backendSession->getDragValue();
    }
}