<?php
/**
 * Scandiweb_DraggableProducts
 *
 * @category    DraggableProducts
 * @package     Scandiweb_DraggableProducts
 * @author      Aleksandrs Vasilievs <aleksandrsv@scandiweb.com>
 * @copyright   Copyright (c) 2017 Scandiweb, Ltd (http://scandiweb.com)
 */
namespace Scandiweb\DraggableProducts\Block\Widget\Grid;

use Magento\Framework\Exception\LocalizedException;
use Magento\Framework\View\Element\Template;

/**
 *
 *
 * @SuppressWarnings(PHPMD.CouplingBetweenObjects)
 */
class ColumnSet extends \Magento\Backend\Block\Widget\Grid\ColumnSet
{
    /**
     * @var \Magento\Backend\Model\Session
     */
    protected $adminSession;

    /**
     * @var \Magento\Framework\App\Request\Http
     */
    protected $request;

    /**
     * Category repository object
     */
    protected $_categoryRepository;

    /**
     * ColumnSet constructor.
     * @param Template\Context $context
     * @param \Magento\Backend\Model\Session $adminSession
     * @param \Magento\Framework\App\Request\Http $request
     * @param \Magento\Catalog\Api\CategoryRepositoryInterface $categoryRepository
     * @param array $data
     */
    public function __construct(
        Template\Context $context,
        \Magento\Backend\Model\Session $adminSession,
        \Magento\Framework\App\Request\Http $request,
        \Magento\Catalog\Api\CategoryRepositoryInterface $categoryRepository,
        \Magento\Backend\Model\Widget\Grid\Row\UrlGeneratorFactory $generatorFactory,
        \Magento\Backend\Model\Widget\Grid\SubTotals $subtotals,
        \Magento\Backend\Model\Widget\Grid\Totals $totals,
        array $data = []
    ) {

        $this->validator = $context->getValidator();
        $this->resolver = $context->getResolver();
        $this->_filesystem = $context->getFilesystem();
        $this->templateEnginePool = $context->getEnginePool();
        $this->_storeManager = $context->getStoreManager();
        $this->_appState = $context->getAppState();
        $this->templateContext = $this;
        $this->pageConfig = $context->getPageConfig();
        $this->adminSession = $adminSession;
        $this->_categoryRepository = $categoryRepository;
        $this->request = $request;

        $moduleName = $request->getModuleName();
        $controllerName = $request->getControllerName();
        $actionName = $request->getActionName();

        if ($moduleName == 'catalog' && $controllerName == 'category' && $actionName == 'edit') {
            Template::__construct($context, $data);
        } else {
            parent::__construct($context, $generatorFactory, $subtotals, $totals, $data);
        }
    }

    /**
     * Retrieve the list of columns
     *
     * @return array
     */
    public function getColumns()
    {
        $layout = $this->getLayout();
        if (!$layout) {
            return false;
        }
        $parentName = $layout->getParentName($this->getNameInLayout());
        if ($parentName == 'category.product.grid') {
            $this->setTemplate('Scandiweb_DraggableProducts::widget/grid/column_set.phtml');
        }


        $columns = $this->getLayout()->getChildBlocks($this->getNameInLayout());
        foreach ($columns as $key => $column) {
            if (!$column->isDisplayed()) {
                unset($columns[$key]);
            }
        }
        return $columns;
    }

    /**
     * Check is it hero product
     */
    public function isHeroProduct($id)
    {
        $categoryId = $this->request->getParam('id');
        if ($categoryId === null) {
            throw new LocalizedException(__("Can't detect hero product, because category is unknown"));
        }

        $category = $this->_categoryRepository->get($categoryId);
        if ($category->getData('sw_hero_products')) {
            foreach (explode(",", $category->getData('sw_hero_products')) as $product) {
                if ((int)$product == $id) {
                    return 1;
                }
            }
        }

        return false;
    }

    /**
     * Is the currently seen category available in query params
     *
     * @return bool
     */
    public function isExistingCategory()
    {
        return $this->request->getParam('id') !== null;
    }

    /**
     * Should be returned list or grid value, if user choosed...
     *
     * @return array
     */
    public function getDragValue()
    {

        return $this->adminSession->getDragValue();
    }

    /**
     * This is needed to take the correct templates
     *
     * @return string
     */
    protected function _toHtml()
    {
        $this->setModuleName($this->extractModuleName('Magento\Backend\Block\Widget\Grid\ColumnSet'));

        return parent::_toHtml();
    }
}
