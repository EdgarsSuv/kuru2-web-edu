# Scandiweb_DraggableProducts
Module allows store admin to quickly create and organise categories via drag and drop enabled interface.

## Installation

Run the following:

```
composer config repositories.module-draggableproducts git git@bitbucket.org:scandiwebassets/draggableproducts.git
composer require scandiweb/module-draggableproducts:1.0.0
php -f bin/magento setup:upgrade
```