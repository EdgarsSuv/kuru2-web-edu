<?php
/**
 * Scandiweb_DraggableProducts
 *
 * @category    DraggableProducts
 * @package     Scandiweb_DraggableProducts
 * @author      Aleksandrs Vasilievs <aleksandrsv@scandiweb.com>
 * @copyright   Copyright (c) 2017 Scandiweb, Ltd (http://scandiweb.com)
 */
namespace Scandiweb\DraggableProducts\Helper;

use Magento\Framework\App\Helper\AbstractHelper;

/**
 * Class Position
 * @package Scandiweb\DraggableProducts\Helper
 */
class Position extends AbstractHelper
{
    /**
     * @var \Magento\CatalogInventory\Model\Stock\StockItemRepository
     */
    protected $_stockItemRepository;

    /**
     * @var \Magento\Catalog\Api\ProductRepositoryInterface
     */
    protected $_productRepository;

    /**
     * Position constructor.
     * @param \Magento\CatalogInventory\Model\Stock\StockItemRepository $stockItemRepository
     * @param \Magento\Catalog\Api\ProductRepositoryInterface $productRepository
     */
    public function __construct(
        \Magento\CatalogInventory\Model\Stock\StockItemRepository $stockItemRepository,
        \Magento\Catalog\Api\ProductRepositoryInterface $productRepository
    ) {
        $this->_stockItemRepository = $stockItemRepository;
        $this->_productRepository = $productRepository;
    }

    /**
     * Change Product Positions of the Category
     *
     * @param $category
     * @return mixed
     */
    public function changePositions($category)
    {
        $new_positions = [];
        $productPositions = $category->getProductsPosition();
        asort($productPositions);

        if ($category->getData('autosort') == 1 || $category->getData('autosort') == 3) {
            $start_position = max($productPositions) + 1;
        } else {
            $start_position = min($productPositions) - 1;
        }

        foreach ($productPositions as $id => $position) {
            if ($category->getData('autosort') == 1) {
                if (!$this->getStockItem($id)) {
                    unset($productPositions[$id]);
                    $new_positions[$id] = $start_position++;
                }
            } else {
                if ($this->productHasDiscount($id)) {
                    unset($productPositions[$id]);
                    if ($category->getData('autosort') == 3) {
                        $new_positions[$id] = $start_position++;
                    } else {
                        $new_positions[$id] = $start_position--;
                    }
                }
            }
        }

        $productPositions = $productPositions + $new_positions;
        $category->setPostedProducts($productPositions);
    }

    private function productHasDiscount($id)
    {
        $product = $this->_productRepository->getById($id);

        return $product->getSpecialPrice();
    }


    private function getStockItem($productId)
    {
        return $this->_stockItemRepository->get($productId)->getIsInStock();
    }
}
