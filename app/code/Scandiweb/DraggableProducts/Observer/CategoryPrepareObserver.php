<?php
/**
 * Scandiweb_DraggableProducts
 *
 * @category    DraggableProducts
 * @package     Scandiweb_DraggableProducts
 * @author      Aleksandrs Vasilievs <aleksandrsv@scandiweb.com>
 * @copyright   Copyright (c) 2017 Scandiweb, Ltd (http://scandiweb.com)
 */
namespace Scandiweb\DraggableProducts\Observer;

use Magento\Framework\Event\Observer;
use Magento\Framework\Event\ObserverInterface;
use Magento\Framework\ObjectManagerInterface;
use Magento\Framework\DataObject;

/**
 * Category prepare observer
 */
class CategoryPrepareObserver implements ObserverInterface
{
    /**
     * Object Manager instance
     *
     * @var \Magento\Framework\ObjectManagerInterface
     */
    protected $_objectManager;

    /**
     * @var \Magento\Catalog\Api\Data\CategoryProductLinkInterfaceFactory
     */
    protected $productLinkFactory;

    /**
     * @var \Magento\Catalog\Api\CategoryLinkRepositoryInterface
     */
    protected $categoryLinkRepository;

    /**
     * Category repository object
     */
    protected $_categoryRepository;

    /**
     * @var \Magento\Framework\Message\ManagerInterface
     */
    protected $_messageManager;

    /**
     * @var \Scandiweb\DraggableProducts\Helper\Position
     */
    protected $helper;

    /**
     * Factory constructor
     *
     * CategoryPrepareObserver constructor.
     * @param ObjectManagerInterface $objectManager
     * @param \Magento\Catalog\Api\CategoryRepositoryInterface $categoryRepository
     * @param \Magento\Catalog\Api\Data\CategoryProductLinkInterfaceFactory $productLinkFactory
     * @param \Magento\Framework\View\Element\Template\Context $context
     * @param \Magento\Framework\Message\ManagerInterface $messageManager
     * @param \Scandiweb\DraggableProducts\Helper\Position $dataHelper
     */
    public function __construct(
        ObjectManagerInterface $objectManager,
        \Magento\Catalog\Api\CategoryRepositoryInterface $categoryRepository,
        \Magento\Catalog\Api\Data\CategoryProductLinkInterfaceFactory $productLinkFactory,
        \Magento\Framework\View\Element\Template\Context $context,
        \Magento\Framework\Message\ManagerInterface $messageManager,
        \Scandiweb\DraggableProducts\Helper\Position $dataHelper
    ) {
    
        $this->_objectManager = $objectManager;
        $this->_categoryRepository = $categoryRepository;
        $this->productLinkFactory = $productLinkFactory;
        $this->_messageManager = $messageManager;
        $this->helper = $dataHelper;
    }

    /**
     * Handler for category prepare event
     *
     * @param Observer $observer
     * @return void
     */
    public function execute(Observer $observer)
    {
        $request = $observer->getEvent()->getRequest();
        $category = $observer->getEvent()->getCategory();
        $data = $request->getPostValue();

        $rule = $this->_objectManager->create('Scandiweb\DraggableProducts\Model\Rule');
        if ($category->getId()) {
            $rule->load($category->getId());
        }

        if ($data && $category->getIsSmart()) {
            if (isset($data['rule'])) {
                $data['conditions'] = $data['rule']['conditions'];
                unset($data['rule']);
            }

            $validateResult = $rule->validateData(new DataObject($data));
            if ($validateResult !== true) {
                $category->setSmartRuleError($validateResult);
                return $this;
            }

            $rule->loadPost(['conditions' => $data['conditions']]);
            $rule->setCategory($category);
            // apply rule
            $matchingProducts = $rule->getMatchingProductIds();
            // update position
            $postedProducts = array_intersect_key($category->getPostedProducts() ?: [], $matchingProducts);
            $postedProducts = array_replace($matchingProducts, $postedProducts);

            $category->setPostedProducts($postedProducts);
            $category->setSmartRule($rule);
        } else {
            $rule->delete();
        }

        if ($category->getData('autosort') && $category->getProductsPosition()) {
            $this->helper->changePositions($category);
        }

        if ($category->getData('sw_hero_products') ||
            ($category->getId() && $this->_categoryRepository->get($category->getId())->getData('sw_hero_products'))) {
            $hero_products = array_reverse(explode(',', $category->getData('sw_hero_products')));
            $productPositions = $category->getProductsPosition();

            if ($category->getData('sw_hero_products') != $this->_categoryRepository->get($category->getId())->getData('sw_hero_products')) {
                // remove product from category if has been removed some hero product
                $category_data = array_reverse(explode(',', $this->_categoryRepository->get($category->getId())->getData('sw_hero_products')));

                foreach (array_diff($category_data, $hero_products) as $removeId) {
                    $removeId = preg_replace('/\s+/', '', $removeId);

                    unset($productPositions[$removeId]);
                }
            }

            if ($category->getData('sw_hero_products')) {
                // prepend hero products
                $min_data_position = min($productPositions) - 1;
                foreach ($hero_products as $key => $item) {
                    $item = preg_replace('/\s+/', '', $item);
                    $productPositions[$item] = $min_data_position - $key;
                }
            }

            $category->setPostedProducts($productPositions);
        }

        return $this;
    }
}
