<?php
/**
 * Scandiweb_DraggableProducts
 *
 * @category    DraggableProducts
 * @package     Scandiweb_DraggableProducts
 * @author      Aleksandrs Vasilievs <aleksandrsv@scandiweb.com>
 * @copyright   Copyright (c) 2017 Scandiweb, Ltd (http://scandiweb.com)
 */
namespace Scandiweb\DraggableProducts\Observer;

use Magento\Framework\Event\Observer;
use Magento\Framework\Event\ObserverInterface;
use Scandiweb\DraggableProducts\Model\Indexer\Product\ProductRuleProcessor;

/**
 * Product save observer
 */
class ProductSaveObserver implements ObserverInterface
{
    /**
     * @var ProductRuleProcessor
     */
    protected $_productRuleProcessor;

    /**
     * ProductSaveObserver constructor.
     * @param ProductRuleProcessor $productRuleProcessor
     */
    public function __construct(
        ProductRuleProcessor $productRuleProcessor
    ) {
        $this->_productRuleProcessor = $productRuleProcessor;
    }
        
    /**
     * Apply smart category rules after product model save
     *
     * @param Observer $observer
     * @return void
     */
    public function execute(Observer $observer)
    {
        $product = $observer->getEvent()->getProduct();
        if (!$product->getIsMassupdate()) {
            $this->_productRuleProcessor->reindexRow($product->getId());
        }
        return $this;
    }
}
