<?php
/**
 * Scandiweb_DraggableProducts
 *
 * @category    DraggableProducts
 * @package     Scandiweb_DraggableProducts
 * @author      Aleksandrs Vasilievs <aleksandrsv@scandiweb.com>
 * @copyright   Copyright (c) 2017 Scandiweb, Ltd (http://scandiweb.com)
 */
namespace Scandiweb\DraggableProducts\Observer;

use Magento\Framework\Event\Observer;
use Magento\Framework\Event\ObserverInterface;
use Magento\Framework\Exception\LocalizedException;

/**
 * Category save observer
 */
class CategorySaveObserver implements ObserverInterface
{
    /**
     * Handler for category save event
     *
     * @param Observer $observer
     * @return void
     */
    public function execute(Observer $observer)
    {
        $category = $observer->getEvent()->getCategory();
        if ($category->getIsSmart()) {
            if ($category->getSmartRuleError()) {
                throw new LocalizedException(
                    $category->getSmartRuleError()
                );
            } else {
                $rule = $category->getSmartRule();
                if ($rule) {
                    $rule->setId($category->getId());
                    $rule->save();
                }
            }
        }
        return $this;
    }
}
