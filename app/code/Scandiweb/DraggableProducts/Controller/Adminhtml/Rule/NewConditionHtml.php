<?php
/**
 * Scandiweb_DraggableProducts
 *
 * @category    DraggableProducts
 * @package     Scandiweb_DraggableProducts
 * @author      Aleksandrs Vasilievs <aleksandrsv@scandiweb.com>
 * @copyright   Copyright (c) 2017 Scandiweb, Ltd (http://scandiweb.com)
 */
namespace Scandiweb\DraggableProducts\Controller\Adminhtml\Rule;

use Magento\Rule\Model\Condition\AbstractCondition;
use Scandiweb\DraggableProducts\Controller\Adminhtml\Rule;

/**
 * DraggableProducts NewConditionHtml controller
 */
class NewConditionHtml extends Rule
{
    /**
     * New Condition Html
     *
     * @return void
     */
    public function execute()
    {
        $id = $this->getRequest()->getParam('id');
        $formName = $this->getRequest()->getParam('form_namespace');
        $typeArr = explode('|', str_replace('-', '/', $this->getRequest()->getParam('type')));
        $type = $typeArr[0];

        $model = $this->_objectManager->create($type)
            ->setId($id)
            ->setType($type)
            ->setRule($this->_objectManager->create('Scandiweb\DraggableProducts\Model\Rule'))
            ->setPrefix('conditions');

        if (!empty($typeArr[1])) {
            $model->setAttribute($typeArr[1]);
        }

        if ($model instanceof AbstractCondition) {
            $model->setJsFormObject($this->getRequest()->getParam('form'));
            $model->setFormName($formName);
            $html = $model->asHtmlRecursive();
        } else {
            $html = '';
        }
        $this->getResponse()->setBody($html);
    }
}
