<?php
/**
 * Scandiweb_DraggableProducts
 *
 * @category    DraggableProducts
 * @package     Scandiweb_DraggableProducts
 * @author      Aleksandrs Vasilievs <aleksandrsv@scandiweb.com>
 * @copyright   Copyright (c) 2017 Scandiweb, Ltd (http://scandiweb.com)
 */
namespace Scandiweb\DraggableProducts\Controller\Adminhtml\Grid;

/**
 * Class Index
 * @package Scandiweb\DraggableProducts\Controller\Adminhtml\Grid
 */
class Index extends \Magento\Backend\App\Action
{

    public function execute()
    {
        if ($this->getRequest()->getParam('type')) {
            $categorySession = $this->_getSession();
            $categorySession->setDragValue($this->getRequest()->getParam('type'));
        }
    }
}
