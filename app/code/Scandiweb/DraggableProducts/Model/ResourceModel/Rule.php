<?php
/**
 * Scandiweb_DraggableProducts
 *
 * @category    DraggableProducts
 * @package     Scandiweb_DraggableProducts
 * @author      Aleksandrs Vasilievs <aleksandrsv@scandiweb.com>
 * @copyright   Copyright (c) 2017 Scandiweb, Ltd (http://scandiweb.com)
 */
namespace Scandiweb\DraggableProducts\Model\ResourceModel;

use Magento\Rule\Model\ResourceModel\AbstractResource;

/**
 * DraggableProducts Rule resource
 */
class Rule extends AbstractResource
{
    /**
     * Primary key auto increment flag
     *
     * @var bool
     */
    protected $_isPkAutoIncrement = false;

    /**
     * Initialize main table and table id field
     *
     * @return void
     * @codeCoverageIgnore
     */
    protected function _construct()
    {
        $this->_init('scandiweb_draggableproducts_rule', 'rule_id');
    }
}
