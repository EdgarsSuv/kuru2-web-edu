<?php
/**
 * Scandiweb_DraggableProducts
 *
 * @category    DraggableProducts
 * @package     Scandiweb_DraggableProducts
 * @author      Aleksandrs Vasilievs <aleksandrsv@scandiweb.com>
 * @copyright   Copyright (c) 2017 Scandiweb, Ltd (http://scandiweb.com)
 */
namespace Scandiweb\DraggableProducts\Model\Category\Attribute\Backend;

class Hero extends \Magento\Eav\Model\Entity\Attribute\Backend\AbstractBackend
{
    /**
     * Validate
     *
     * @param \Magento\Catalog\Model\Product $object
     * @throws \Magento\Framework\Exception\LocalizedException
     * @return bool
     */
    public function validate($object)
    {
        if ($object->getData('sw_hero_products')) {
            $hero_products = array_reverse(explode(',', $object->getData('sw_hero_products')));
            $objectManager = \Magento\Framework\App\ObjectManager::getInstance();

            foreach ($hero_products as $item) {
                $product = $objectManager->create('Magento\Catalog\Model\Product')->load($item);
                if (!$product->getId()) {
                    throw new \Magento\Framework\Exception\NoSuchEntityException(
                        __(
                            'Product with ID "%1" does not exist',
                            $item
                        )
                    );
                }
            }
        }
    }
}
