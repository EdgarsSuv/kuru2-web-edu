<?php
/**
 * Scandiweb_DraggableProducts
 *
 * @category    DraggableProducts
 * @package     Scandiweb_DraggableProducts
 * @author      Aleksandrs Vasilievs <aleksandrsv@scandiweb.com>
 * @copyright   Copyright (c) 2017 Scandiweb, Ltd (http://scandiweb.com)
 */
namespace Scandiweb\DraggableProducts\Model\Indexer\Product;

use Magento\Catalog\Model\Product;
use Scandiweb\DraggableProducts\Model\Indexer\AbstractIndexer;

/**
 * DraggableProducts ProductRuleIndexer model
 */
class ProductRuleIndexer extends AbstractIndexer
{
    /**
     * Execute partial indexation by ID list
     *
     * @param int[] $ids
     * @return void
     */
    protected function doExecuteList($ids)
    {
        $this->_indexBuilder->reindexByIds(array_unique($ids));
        $this->getCacheContext()->registerEntities(Product::CACHE_TAG, $ids);
    }

    /**
     * Execute partial indexation by ID
     *
     * @param int $id
     * @throws \Magento\Framework\Exception\LocalizedException
     * @return void
     */
    protected function doExecuteRow($id)
    {
        $this->_indexBuilder->reindexById($id);
    }
}
