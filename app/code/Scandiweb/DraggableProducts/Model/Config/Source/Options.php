<?php
/**
 * Scandiweb_DraggableProducts
 *
 * @category    DraggableProducts
 * @package     Scandiweb_DraggableProducts
 * @author      Aleksandrs Vasilievs <aleksandrsv@scandiweb.com>
 * @copyright   Copyright (c) 2017 Scandiweb, Ltd (http://scandiweb.com)
 */
namespace Scandiweb\DraggableProducts\Model\Config\Source;

use Magento\Framework\Option\ArrayInterface;

class Options implements ArrayInterface
{
    /**
     * @return array
     */
    public function toOptionArray()
    {
        $options = [
            0 => [
                'label' => 'None',
                'value' => 0
            ],
            1 => [
                'label' => 'Move out of stock to bottom',
                'value' => 1
            ],
            2  => [
                'label' => 'Special price to top',
                'value' => 2
            ],
            3 => [
                'label' => 'Special price to bottom',
                'value' => 3
            ],
        ];

        return $options;
    }
}
