<?php
/**
 * @vendor Scandiweb
 * @module Scandiweb_Reward
 * @author Kristaps Stalidzāns <info@scandiweb.com>
 * @copyright Copyright (c) 2017 Magento. All rights reserved.
 * @license http://opensource.org/licenses/afl-3.0.php Academic Free License (AFL 3.0)
 */
namespace Scandiweb\Reward\Helper;

use Magento\Store\Model\ScopeInterface;
use Magento\Framework\App\Helper\AbstractHelper;

class Data extends AbstractHelper
{
    /**
     * Reward messages
     */
    const REWARD_MESSAGES = 'rewards/general/rewards_message';

    /**
     * Returns Reward Messages serialized
     *
     * @return mixed
     */
    public function getRewardMessages()
    {
        $messages = $this->scopeConfig->getValue(
            self::REWARD_MESSAGES,
            ScopeInterface::SCOPE_STORE,
            1
        );

        return unserialize($messages);
    }
}
