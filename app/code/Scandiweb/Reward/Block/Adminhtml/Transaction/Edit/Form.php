<?php
/**
 * @vendor Scandiweb
 * @module Scandiweb_Reward
 * @author Kristaps Stalidzāns <info@scandiweb.com>
 * @copyright Copyright (c) 2017 Magento. All rights reserved.
 * @license http://opensource.org/licenses/afl-3.0.php Academic Free License (AFL 3.0)
 */
namespace Scandiweb\Reward\Block\Adminhtml\Transaction\Edit;

use Magento\Framework\Registry;
use Mirasvit\Rewards\Helper\Message;
use Magento\Framework\Data\FormFactory;
use Magento\Backend\Block\Widget\Context;
use Scandiweb\Reward\Model\Messages\Options;
use Magento\Framework\Exception\LocalizedException;

class Form extends \Mirasvit\Rewards\Block\Adminhtml\Transaction\Edit\Form
{
    /**
     * @var Options
     */
    protected $options;

    /**
     * Form constructor.
     * @param Message $rewardsMessage
     * @param FormFactory $formFactory
     * @param Registry $registry
     * @param Context $context
     * @param Options $options
     * @param array $data
     */
    public function __construct
    (
        Message $rewardsMessage,
        FormFactory $formFactory,
        Registry $registry,
        Context $context,
        Options $options,
        array $data = []
    )
    {
        $this->options = $options;

        parent::__construct(
            $rewardsMessage,
            $formFactory,
            $registry,
            $context,
            $data
        );
    }

    /**
     * @return $this
     * @throws LocalizedException
     */
    protected function _prepareForm()
    {
        $form = $this->formFactory->create()->setData(
            [
                'id' => 'edit_form',
                'action' => $this->getUrl('*/*/save', ['id' => $this->getRequest()->getParam('id')]),
                'method' => 'post',
                'enctype' => 'multipart/form-data',
            ]
        );

        /** @var \Mirasvit\Rewards\Model\Transaction $transaction */
        $transaction = $this->registry->registry('current_transaction');
        $fieldset = $form->addFieldset('edit_fieldset', ['legend' => __('General Information')]);

        if ($transaction->getId()) {
            $fieldset->addField('transaction_id', 'hidden', [
                'name' => 'transaction_id',
                'value' => $transaction->getId(),
            ]);
        }

        $fieldset->addField('amount', 'text', [
            'label' => __('Points Balance Change'),
            'required' => true,
            'name' => 'amount',
        ]);

        $selectField = $fieldset->addField('history_message', 'select', [
            'label' => __('Message in the rewards history'),
            'required' => true,
            'name' => 'history_message',
            'values' => $this->options->toOptionArray(),
            'note' => __('Customer will see this in his accounts'),
        ]);

        $hideField = $fieldset->addField('field_to_hide', 'text', array(
            'label'  => __('Other Message in the rewards history'),
            'required' => true,
            'name' => 'history_message_other',
        ));

        $dependence = $this->getLayout()->createBlock(
            'Magento\Backend\Block\Widget\Form\Element\Dependence')
            ->addFieldMap($selectField->getHtmlId(), $selectField->getName())
            ->addFieldMap($hideField->getHtmlId(), $hideField->getName())
            ->addFieldDependence(
                $hideField->getName(),
                $selectField->getName(),
                'other'
            );

        $this->setChild('form_after', $dependence);

        $fieldset->addField('email_message', 'editor', [
            'label' => __('Message for customer notification email'),
            'name' => 'email_message',
            'wysiwyg' => true,
            'note' => $this->rewardsMessage->getNoteWithVariables(),
            'style' => 'width: 600px; height: 300px;',
        ]);

        $fieldset->addField('in_transaction_user', 'hidden',
            [
                'name' => 'in_transaction_user',
                'id' => 'in_transaction_user',
                'required' => true,
            ]
        );

        $fieldset->addField('in_transaction_user_old', 'hidden', ['name' => 'in_transaction_user_old']);

        $form->setUseContainer(true);
        $this->setForm($form);

        return $this;
    }
}
