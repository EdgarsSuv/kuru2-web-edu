<?php
/**
 * @vendor Scandiweb
 * @module Scandiweb_Reward
 * @author Kristaps Stalidzāns <info@scandiweb.com>
 * @copyright Copyright (c) 2017 Magento. All rights reserved.
 * @license http://opensource.org/licenses/afl-3.0.php Academic Free License (AFL 3.0)
 */
namespace Scandiweb\Reward\Controller\Adminhtml\Transaction;

use Magento\Framework\Exception\LocalizedException;

class Save extends \Mirasvit\Rewards\Controller\Adminhtml\Transaction\Save
{
    /**
     * @return void
     */
    public function execute()
    {
        if ($data = $this->getRequest()->getParams()) {
            $customers = $data['in_transaction_user'];
            parse_str($customers, $customers);
            $customers = array_keys($customers);

            try {
                foreach ($customers as $customerId) {
                    if ((int) $customerId > 0) {
                        $emailMessage = '';
                        $historyMessage = $data['history_message'];

                        if (isset($data['history_message_other'])) {
                            $historyMessage = $data['history_message_other'];
                        }

                        if (isset($data['email_message'])) {
                            $emailMessage = $data['email_message'];
                        }

                        $this->rewardsBalance->changePointsBalance(
                            $customerId,
                            $data['amount'],
                            $historyMessage,
                            false,
                            true,
                            $emailMessage
                        );
                    }
                }

                $this->messageManager->addSuccessMessage(__('Transaction was successfully saved'));
                $this->backendSession->setFormData(false);
                $this->_redirect('*/*/index');
            } catch (LocalizedException $e) {
                $this->messageManager->addErrorMessage(__('Unable to find Transaction to save'));
                $this->messageManager->addErrorMessage($e->getMessage());
                $this->backendSession->setFormData($data);
                $this->_redirect('*/*/index');
            }
        }

        $this->_redirect('*/*/');
    }
}
