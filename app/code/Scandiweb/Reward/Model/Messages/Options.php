<?php
/**
 * @vendor Scandiweb
 * @module Scandiweb_Reward
 * @author Kristaps Stalidzāns <info@scandiweb.com>
 * @copyright Copyright (c) 2017 Magento. All rights reserved.
 * @license http://opensource.org/licenses/afl-3.0.php Academic Free License (AFL 3.0)
 */
namespace Scandiweb\Reward\Model\Messages;

use Magento\Framework\Data\OptionSourceInterface;
use Scandiweb\Reward\Helper\Data;

class Options implements OptionSourceInterface
{
    /**
     * @var null|array
     */
    protected $options;

    /**
     * @var Data
     */
    protected $helper;

    /**
     * Options constructor.
     * @param Data $helper
     */
    public function __construct
    (
        Data $helper
    ) {
        $this->helper = $helper;
    }

    /**
     * @return array|null
     */
    public function toOptionArray()
    {
        if (null == $this->options) {
            $options = [];

            if ($this->helper->getRewardMessages()) {
                foreach ($this->helper->getRewardMessages() as $messageItem) {
                    $options[] = [
                        'value' => $messageItem['message'],
                        'label' => $messageItem['message'],
                    ];
                }
            }

            $this->options = $options;
            array_unshift($this->options, ['value' => '', 'label' => __('Please select a transaction message')]);
            array_push($this->options, ['value' => 'other', 'label' => __('Other')]);
        }

        return $this->options;
    }
}
