<?php
/**
 * @vendor Scandiweb
 * @module Scandiweb_Reward
 * @author Kristaps Stalidzāns <info@scandiweb.com>
 * @copyright Copyright (c) 2017 Magento. All rights reserved.
 * @license http://opensource.org/licenses/afl-3.0.php Academic Free License (AFL 3.0)
 */
namespace Scandiweb\Reward\Model\System\Config\Form\Field;

use Magento\Backend\Block\Template\Context;
use Magento\Framework\Data\Form\Element\Factory;
use Magento\Framework\View\Design\Theme\LabelFactory;
use Magento\Config\Block\System\Config\Form\Field\FieldArray\AbstractFieldArray;

/**
 * Backend system config array field renderer
 */
class Message extends AbstractFieldArray
{
    /**
     * @var Factory
     */
    protected $_elementFactory;

    /**
     * @var LabelFactory
     */
    protected $_labelFactory;

    /**
     * @param Context $context
     * @param Factory $elementFactory
     * @param LabelFactory $labelFactory
     * @param array $data
     */
    public function __construct
    (
        Context $context,
        Factory $elementFactory,
        LabelFactory $labelFactory,
        array $data = []
    ) {
        $this->_elementFactory = $elementFactory;
        $this->_labelFactory = $labelFactory;

        parent::__construct($context, $data);
    }

    /**
     * Initialise form fields
     *
     * @return void
     */
    protected function _construct()
    {
        $this->addColumn('message', ['label' => __('Message')]);
        $this->_addAfter = false;
        $this->_addButtonLabel = __('Add Message');

        parent::_construct();
    }
}
