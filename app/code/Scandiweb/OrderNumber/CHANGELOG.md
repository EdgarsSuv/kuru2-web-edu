Scandiweb_OrderNumber changelog
========================

0.1.0:
- Added random function that generates fixed length numbers
- Added checks for possible duplication to make numbers unique
