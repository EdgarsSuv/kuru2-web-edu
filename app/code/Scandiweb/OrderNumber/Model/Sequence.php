<?php
/**
 * @vendor Scandiweb
 * @module Scandiweb_OrderNumber
 * @author Kristaps Stalidzāns <info@scandiweb.com>
 * @copyright Copyright (c) 2017 Magento. All rights reserved.
 * @license http://opensource.org/licenses/afl-3.0.php Academic Free License (AFL 3.0)
 */
namespace Scandiweb\OrderNumber\Model;

use Magento\Framework\App\ResourceConnection as AppResource;
use Magento\Framework\DB\Adapter\AdapterInterface;
use Magento\Framework\DB\Sequence\SequenceInterface;
use Magento\SalesSequence\Model\Meta as Meta;

/**
 * Class Sequence
 * @package Scandiweb\OrderNumber\Model
 */
class Sequence extends \Magento\SalesSequence\Model\Sequence
{
    /**
     * Default pattern for Sequence
     */
    const DEFAULT_PATTERN  = "%s%'.09d%s";

    /**
     * @var string
     */
    private $lastIncrementId;

    /**
     * @var Meta
     */
    private $meta;

    /**
     * @var false|AdapterInterface
     */
    private $connection;

    /**
     * @var string
     */
    private $pattern;

    /**
     * @var int
     */
    private $randomNumber;

    /**
     * @param Meta $meta
     * @param AppResource $resource
     * @param string $pattern
     */
    public function __construct(
        Meta $meta,
        AppResource $resource,
        $pattern = self::DEFAULT_PATTERN
    ) {
        $this->meta = $meta;
        $this->connection = $resource->getConnection('sales');
        $this->pattern = $pattern;
    }

    /**
     * Retrieve current value
     *
     * @return string
     */
    public function getCurrentValue()
    {
        if (!isset($this->lastIncrementId)) {
            return null;
        }

        return sprintf(
            $this->pattern,
            $this->meta->getActiveProfile()->getPrefix(),
            $this->calculateCurrentValue(),
            $this->meta->getActiveProfile()->getSuffix()
        );
    }

    /**
     * Calculate current value depends on start value
     *
     * @return string
     */
    private function calculateCurrentValue()
    {
        return ($this->lastIncrementId - $this->meta->getActiveProfile()->getStartValue())
            * $this->meta->getActiveProfile()->getStep() + $this->meta->getActiveProfile()->getStartValue();
    }

    /**
     * Retrieve next value that is random
     *
     * @return string
     */
    public function getNextValue()
    {
        $this->randomNumber = $this->getRandomNumber();

        /**
         * Checks if random number exists then generates another number if it exists
         */
        do {
            $query = $this->connection->select('sequence_value')
                ->from($this->meta->getSequenceTable())
                ->where('sequence_value = ?', $this->randomNumber)
                ->limit(1);

            $data = $this->connection->fetchAll($query);

            if ($data) {
                $this->randomNumber = $this->getRandomNumber();
            }
        } while (!empty($data));

        $this->connection->insert($this->meta->getSequenceTable(),
            ['sequence_value' => $this->randomNumber]
        );

        $this->lastIncrementId = $this->connection->lastInsertId(
            $this->meta->getSequenceTable()
        );

        return $this->getCurrentValue();
    }

    /**
     * Generates fixed length random number
     *
     * @param int $length
     * @return int
     */
    public function getRandomNumber($length = 9) : Int
    {
        $result = '';

        for ($i = 0; $i < $length; $i++) {
            $result .= mt_rand(0, 9);
        }

        return (int)$result;
    }
}
