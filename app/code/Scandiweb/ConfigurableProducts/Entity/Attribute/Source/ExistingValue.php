<?php
/**
 * @category     Scandiweb
 * @package      Scandiweb/ConfigurableProducts
 * @author       Krisjanis Veinbahs <krisjanisv@kurufootwear.com>
 * @copyright    Copyright (c) 2018 KURU Footwear. All rights reserved.
 */
namespace Scandiweb\ConfigurableProducts\Entity\Attribute\Source;

use Magento\Eav\Model\Entity\Attribute\Source\AbstractSource;
use Magento\Eav\Model\Entity\Attribute\Source\SourceInterface;
use Magento\Framework\Data\OptionSourceInterface;

/**
 * Null checking text field functionality model
 */
class ExistingValue implements OptionSourceInterface
{
    /**
     * Retrieve option array
     *
     * @return string[]
     */
    public function toOptionArray()
    {
        return [
            [
                'value' => 0,
                'label' => __('Not set')
            ],
            [
                'value' => 1,
                'label' => __('Set')
            ]
        ];
    }
}
