<?php
/**
 * @vendor Scandiweb
 * @module Scandiweb_ConfigurableProducts
 * @author Kristaps Stalidzāns <info@scandiweb.com>
 * @copyright Copyright (c) 2017 Scandiweb, Ltd (http://scandiweb.com)
 * @license http://opensource.org/licenses/afl-3.0.php Academic Free License (AFL 3.0)
 */
namespace Scandiweb\ConfigurableProducts\Helper;

use Magento\Store\Model\ScopeInterface;
use Magento\Framework\App\Helper\AbstractHelper;

class Data extends AbstractHelper
{
    /**
     * Configurable product status
     */
    const CONFIGURABLE_STATUS = 'scandiweb_duplicate/configurableproduct/status';

    /**
     * Checks if filering is enabled
     *
     * @return mixed
     */
    public function getConfigurableProductStatus()
    {
        return $this->scopeConfig->getValue(self::CONFIGURABLE_STATUS, ScopeInterface::SCOPE_STORE, 1);
    }
}
