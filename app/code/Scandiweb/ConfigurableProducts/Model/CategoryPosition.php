<?php
/**
 * @vendor Scandiweb
 * @module Scandiweb_ConfigurableProducts
 * @author Kristaps Stalidzāns <info@scandiweb.com>
 * @copyright Copyright (c) 2017 Scandiweb, Ltd (http://scandiweb.com)
 * @license http://opensource.org/licenses/afl-3.0.php Academic Free License (AFL 3.0)
 */
namespace Scandiweb\ConfigurableProducts\Model;

use Magento\Catalog\Model\Product;
use Magento\Framework\App\ResourceConnection;
use Magento\Catalog\Api\ProductRepositoryInterface;

class CategoryPosition
{
    /**
     * @var ResourceConnection
     */
    protected $resourceConnection;

    /**
     * @var ProductRepositoryInterface
     */
    protected $productRepository;

    /**
     * CategoryPosition constructor.
     * @param ResourceConnection $resourceConnection
     * @param ProductRepositoryInterface $productRepository
     */
    public function __construct
    (
        ResourceConnection $resourceConnection,
        ProductRepositoryInterface $productRepository
    ){
        $this->resourceConnection = $resourceConnection;
        $this->productRepository = $productRepository;
    }

    /**
     * Gets product position in categories
     *
     * @param Product $product
     * @return array
     */
    public function getProductCategoryPosition(Product $product): array
    {
        $data = [];
        $connection = $this->resourceConnection->getConnection();
        $tableName = $this->resourceConnection->getTableName('catalog_category_product');

        try {
            $productData = $this->productRepository->get($product->getSku());

            if ($productData) {
                $sql = $connection->select()
                    ->from($tableName)
                    ->where('product_id = ?', $productData->getId());
                $result = $connection->fetchAll($sql);

                if ($result) {
                    foreach ($result as $item) {
                        array_push($data,
                            [
                                'category_id' => $item['category_id'],
                                'position' => $item['position']
                            ]);
                    }
                }
            }
        } catch (\Exception $e) {

        }

        return $data;
    }

    /**
     * Updates product position
     *
     * @param Product $product
     * @param array $position
     */
    public function updateProductPosition(Product $product, array $position)
    {
        $connection = $this->resourceConnection->getConnection();
        $tableName = $this->resourceConnection->getTableName('catalog_category_product');
        $tableNameIndex = $this->resourceConnection->getTableName('catalog_category_product_index');

        try {
            $productId = $this->productRepository->get($product->getSku())->getId();

            if (!empty($position)) {
                foreach ($position as $positionItem) {
                    $connection->update(
                        $tableName,
                        [
                            'position' => $positionItem['position']
                        ],
                        [
                            'category_id = ?' => $positionItem['category_id'],
                            'product_id = ?' => $productId
                        ]
                    );
                    $connection->update(
                        $tableNameIndex,
                        [
                            'position' => $positionItem['position']
                        ],
                        [
                            'category_id = ?' => $positionItem['category_id'],
                            'product_id = ?' => $productId
                        ]
                    );
                }
            }
        } catch (\Exception $e) {}
    }

    /**
     * Updates product position
     *
     * @param Product $product
     * @param array $position
     */
    public function updateProductPositionMigration(Product $product, array $position)
    {
        $connection = $this->resourceConnection->getConnection();
        $tableName = $this->resourceConnection->getTableName('catalog_category_product');
        $tableNameIndex = $this->resourceConnection->getTableName('catalog_category_product_index');

        try {
            $productId = $this->productRepository->get($product->getSku())->getId();

            if (!empty($position)) {
                foreach ($position['category_ids'] as $positionItemId) {
                    $connection->update(
                        $tableName,
                        [
                            'position' => $position['position']
                        ],
                        [
                            'category_id = ?' => $positionItemId,
                            'product_id = ?' => $productId
                        ]
                    );
                    $connection->update(
                        $tableNameIndex,
                        [
                            'position' => $position['position']
                        ],
                        [
                            'category_id = ?' => $positionItemId,
                            'product_id = ?' => $productId
                        ]
                    );
                }
            }
        } catch (\Exception $e) {
            echo $e->getMessage();
        }
    }
}
