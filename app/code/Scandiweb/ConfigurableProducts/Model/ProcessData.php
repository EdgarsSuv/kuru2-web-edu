<?php
/**
 * @vendor Scandiweb
 * @module Scandiweb_ConfigurableProducts
 * @author Kristaps Stalidzāns <info@scandiweb.com>
 * @copyright Copyright (c) 2017 Scandiweb, Ltd (http://scandiweb.com)
 * @license http://opensource.org/licenses/afl-3.0.php Academic Free License (AFL 3.0)
 */
namespace Scandiweb\ConfigurableProducts\Model;

use Magento\Catalog\Api\CategoryLinkManagementInterface;
use Magento\Catalog\Model\Product;
use Magento\Catalog\Model\Product\Action;
use Magento\Catalog\Model\Product\Visibility;
use Magento\Framework\App\ResourceConnection;
use Magento\Framework\Api\SearchCriteriaBuilder;
use Magento\Framework\DB\Adapter\AdapterInterface;
use Magento\Catalog\Api\ProductRepositoryInterface;
use Magento\Catalog\Api\CategoryRepositoryInterface;
use Scandiweb\ConfigurableProducts\Helper\AttributeHelper;
use Scandiweb\ConfigurableProducts\Model\Product\Copier;
use Magento\ConfigurableProduct\Api\LinkManagementInterface;
use Magento\UrlRewrite\Service\V1\Data\UrlRewrite;
use Magento\UrlRewrite\Model\UrlPersistInterface;
use Magento\CatalogUrlRewrite\Model\ProductUrlRewriteGenerator;

class ProcessData
{

    const PRODUCT_PATH = 'catalog/product/view/id/';
    const MEDIUM = 'Medium';
    const WIDE = 'Wide';

    /**
     * @var Copier
     */
    protected $productCopier;

    /**
     * @var ResourceConnection
     */
    protected $resourceConnection;

    /**
     * @var SearchCriteriaBuilder
     */
    protected $searchCriteriaBuilder;

    /**
     * @var LinkManagementInterface
     */
    protected $linkManagement;

    /**
     * @var CategoryRepositoryInterface
     */
    protected $categoryRepositoryInterface;

    /**
     * @var CategoryPosition
     */
    protected $categoryPosition;

    /**
     * @var ProductRepositoryInterface
     */
    protected $productRepository;

    /**
     * @var AttributeHelper
     */
    private $attributeHelper;

    /**
     * @var Action
     */
    private $productAction;

    /**
     * @var ProductUrlRewriteGenerator
     */
    protected $productUrlRewriteGenerator;

    /**
     * @var UrlPersistInterface
     */
    protected $urlPersist;
    /**
     * @var CategoryLinkManagementInterface
     */
    private $categoryLinkManagement;

    /**
     * ProcessData constructor.
     * @param Copier $productCopier
     * @param ResourceConnection $resourceConnection
     * @param SearchCriteriaBuilder $searchCriteriaBuilder
     * @param LinkManagementInterface $linkManagement
     * @param CategoryRepositoryInterface $categoryRepositoryInterface
     * @param CategoryPosition $categoryPosition
     * @param ProductRepositoryInterface $productRepository
     * @param AttributeHelper $attributeHelper
     * @param Action $productAction
     * @param ProductUrlRewriteGenerator $productUrlRewriteGenerator
     * @param UrlPersistInterface $urlPersist
     * @param CategoryLinkManagementInterface $categoryLinkManagement
     */
    public function __construct
    (
        Copier $productCopier,
        ResourceConnection $resourceConnection,
        SearchCriteriaBuilder $searchCriteriaBuilder,
        LinkManagementInterface $linkManagement,
        CategoryRepositoryInterface $categoryRepositoryInterface,
        CategoryPosition $categoryPosition,
        ProductRepositoryInterface $productRepository,
        AttributeHelper $attributeHelper,
        Action $productAction,
        ProductUrlRewriteGenerator $productUrlRewriteGenerator,
        UrlPersistInterface $urlPersist,
        CategoryLinkManagementInterface $categoryLinkManagement
    ){
        $this->productCopier = $productCopier;
        $this->resourceConnection = $resourceConnection;
        $this->searchCriteriaBuilder = $searchCriteriaBuilder;
        $this->linkManagement = $linkManagement;
        $this->categoryRepositoryInterface = $categoryRepositoryInterface;
        $this->categoryPosition = $categoryPosition;
        $this->productRepository = $productRepository;
        $this->attributeHelper = $attributeHelper;
        $this->productAction = $productAction;
        $this->productUrlRewriteGenerator = $productUrlRewriteGenerator;
        $this->urlPersist = $urlPersist;
        $this->categoryLinkManagement = $categoryLinkManagement;
    }

    /**
     * This function clones existing configurable product and gets its child products.
     * Then with correct color data it changes configurable product data and duplicates it.
     * After duplicate it deletes it
     *
     * @param Product $product
     * @param $postData
     * @return array
     */
    public function processConfigurableProduct(Product $product, $postData)
    {
        $originalProduct = clone $product;
        $this->setUrlRewrites($product);
        $childProduct = $this->linkManagement->getChildren($product->getSku());
        $catPositionData = [];

        foreach ($postData['product_color_ids'] as $key => $value) {
            $childProductData = $this->getChildProductsData($childProduct, $value);

            /**
             * Check if there is child products and if there is color data
             */
            if ($childProductData && !($value == '0')) {
                /**
                 * Checks if filterable ids is selected
                 */
                if (array_key_exists(strtolower($key) . '_filtrable_color', $postData) && $postData[strtolower($key) . '_filtrable_color']) {
                    $filterableIds = $postData[strtolower($key) . '_filtrable_color'];
                } else {
                    $filterableIds  = null;
                }

                if (strpos($product->getSku(), 'E') !== false) {
                    $size = self::WIDE;
                } else {
                    $size = self::MEDIUM;
                }

                $optionWidthId = $this->attributeHelper->getAttributeId($size, 'filterable_width');
                $newProductData =  [
                    'price' => $childProductData['price'],
                    'name' => $originalProduct->getData('orig_name'),
                    'sku' => $childProductData['sku'],
                    'color' => (int)$value,
                    'parent_sku' => $originalProduct->getSku(),
                    'description' => $originalProduct->getData('description'),
                    'short_description' => $originalProduct->getData('short_description'),
                    'pressawards_info' => $originalProduct->getData('pressawards_info'),
                    'technology_info' => $originalProduct->getData('technology_info'),
                    'product_duplicate' => 1,
                    'filterable_width' =>  $optionWidthId,
                    'category_ids'=> $postData[strtolower($key) . '_category_ids'],
                    'filterable_color'=> $filterableIds,
                    'visibility' => 4,
                    'product_color_data' => $key,
                    'url_key' => $this->generateDuplicateUrlKey($product, $key)
                ];
                /**
                 * Sets new product data to be duplicated saving all other data
                 */
                $product->addData($newProductData);
                $positionData = $this->categoryPosition->getProductCategoryPosition($product);

                /**
                 * Checks if configurable is available then updates its attributes
                 */
                try {
                    $productDuplicate = $this->productRepository->get($childProductData['sku']);

                    if ($productDuplicate) {
                        unset($newProductData['sku']); // Removing array data because updateAttributes function doesn't like sku attribute
                        $this->categoryLinkManagement->assignProductToCategories($childProductData['sku'], $newProductData['category_ids']);
                        unset($newProductData['category_ids']); // Removing array data because updateAttributes function doesn't like category_ids attribute
                        $this->productAction->updateAttributes([$productDuplicate->getId()], [$newProductData], 0);
                        $this->categoryPosition->updateProductPosition($productDuplicate, $positionData);
                        $catPositionData[$productDuplicate->getSku()] = $this->categoryPosition->getProductCategoryPosition($productDuplicate);
                    }
                } catch (\Exception $e) {
                    if ($e->getMessage() == 'Requested product doesn\'t exist') {
                        /**
                         * If no duplicate product is present create a copy of it
                         */
                        $duplicatedProduct = $this->productCopier->copy($product);
                        $this->updateUrlRewrite($originalProduct, $duplicatedProduct, $key);
                        $this->categoryPosition->updateProductPosition($duplicatedProduct, $positionData);
                        $catPositionData[$duplicatedProduct->getSku()] = $this->categoryPosition->getProductCategoryPosition($duplicatedProduct);
                    }
                }
            } else {
                $childProductData = $this->getChildProductsByColorName($childProduct, $key);

                /**
                 * Changes product visibility when checkbox is updated
                 */
                try {
                    $productDuplicate = $this->productRepository->get($childProductData['sku']);

                    if ($productDuplicate) {
                        $this->productAction->updateAttributes([$productDuplicate->getId()],
                            [
                                'visibility' => Visibility::VISIBILITY_NOT_VISIBLE,
                            ], 0);

                        $this->updateUrlRewrite($originalProduct, $productDuplicate, $key);
                    }
                } catch (\Exception $e) {

                }
            }
        }

        return $catPositionData;
    }

    /**
     * Generate a unique url key for duplicate from master.
     *
     * @param $product
     * @param $color
     *
     * @return mixed
     */
    public function generateDuplicateUrlKey($product, $color) {
        $masterUrlKey = $product->getUrlKey();

        if (empty($color)) {
            return $masterUrlKey;
        }

        return sprintf(
            "%s-%s",
            $masterUrlKey,
            strtolower($color)
        );
    }

    /**
     * Function gets configurable child product price and images
     *
     * @param $products
     * @param $colorCode
     * @return array
     */
    public function getChildProductsData($products, $colorCode)
    {
        $productData = [];

        foreach ($products as $item) {
            if ($item->getColor() == $colorCode) {
                $productData['price'] = $item['price'];
                $productData['filterable_color'] = $item['filterable_color'];
                $sku = explode('-', $item->getSku());
                $productData['sku'] = $sku[0];
            }
        }

        return $productData;
    }


    /**
     * Function updates new product URL paths
     *
     * @param Product $product
     * @param Product $copiedProduct
     * @param $attribute
     */
    public function updateUrlRewrite(Product $product, Product $copiedProduct, $attribute)
    {
        $connection = $this->resourceConnection->getConnection();
        $tableName = $this->resourceConnection->getTableName('url_rewrite');
        $productId = $product->getId();
        $copiedProductId = $copiedProduct->getId();
        $results = $this->pathQuery(
            $connection,
            $tableName,
            $product,
            self::PRODUCT_PATH . $productId
        );

        /**
         * Results for plain product
         */
        if ($results) {
            $connection->delete(
                $tableName,
                [
                    'request_path = ?' => $results[0]['request_path'] . '?color=' . $attribute,
                    'entity_id != ?' => $copiedProduct->getId()

                ]
            );

            $this->updatePath(
                $connection,
                $tableName,
                $results[0],
                $attribute,
                $copiedProduct,
                self::PRODUCT_PATH . $copiedProductId
            );
        }

        /**
         * Updates category Urls
         */
        foreach ($product->getCategoryIds() as $items) {
            $results = $this->pathQuery(
                $connection,
                $tableName,
                $product,
                self::PRODUCT_PATH . $productId . '/category/' . $items
            );

            if ($results) {
                $this->updatePathCategory(
                    $connection,
                    $tableName,
                    $results[0],
                    $attribute,
                    $copiedProduct,
                    self::PRODUCT_PATH . $copiedProductId . '/category/' . $items
                );
            }
        }
    }

    /**
     * Universal function to get table information based on product for URL rewrite
     *
     * @param AdapterInterface $connection
     * @param $tableName
     * @param $path
     * @return array
     */
    public function existingPathQuery(AdapterInterface $connection, $tableName, $path)
    {
        $sql = $connection->select()
            ->from($tableName)
            ->where('request_path = ?', $path);

        return $connection->fetchAll($sql);
    }

    /**
     * @param AdapterInterface $connection
     * @param $tableName
     * @param $path
     */
    public function deletePath(AdapterInterface $connection, $tableName, $path)
    {
        $connection->delete($tableName, ['request_path = ?' => $path]);
    }

    /**
     * Function gets configurable child product price and images
     *
     * @param $products
     * @param $colorLabel
     * @return array
     */
    public function getChildProductsByColorName($products, $colorLabel)
    {
        $productData = [];
        $colorCode = $this->attributeHelper->getAttributeId($colorLabel ,'color');

        foreach ($products as $item) {
            if ($item->getColor() == $colorCode) {
                $productData['price'] = $item['price'];
                $productData['filterable_color'] = $item['filterable_color'];
                $sku = explode('-', $item->getSku());
                $productData['sku'] = $sku[0];
            }
        }

        return $productData;
    }


    /**
     * Universal function to get table information based on product for URL rewrite
     *
     * @param AdapterInterface $connection
     * @param $tableName
     * @param Product $product
     * @param $path
     * @return array
     */
    public function pathQuery(AdapterInterface $connection, $tableName, Product $product, $path)
    {
        $sql = $connection->select()
            ->from($tableName)
            ->where('entity_id = ?', $product->getId())
            ->where('target_path = ?', $path);

        return $connection->fetchAll($sql);
    }

    /**
     * Updates product path in database
     *
     * @param AdapterInterface $connection
     * @param $tableName
     * @param $result
     * @param $attribute
     * @param Product $product
     * @param $path
     */
    public function updatePath(AdapterInterface $connection, $tableName, $result, $attribute, Product $product, $path)
    {
        $connection->update(
            $tableName,
            [
                'request_path' => $result['request_path'] . '?color=' . $attribute
            ],
            [
                'target_path = ?' => $path,
                'entity_id = ?' => $product->getId()
            ]
        );
    }

    /**
     * Updates product path in database for ca
     *
     * @param AdapterInterface $connection
     * @param $tableName
     * @param $result
     * @param $attribute
     * @param Product $product
     * @param $path
     */
    public function updatePathCategory(AdapterInterface $connection, $tableName, $result, $attribute, Product $product, $path)
    {
        $sql = $connection->select()
            ->from($tableName)
            ->where('request_path = ?', $result['request_path'] . '?color=' . $attribute)
            ->where('entity_id <> ?', $product->getId());

        $resultSql = $connection->fetchAll($sql);

        /**
         * Deletes existing category url rewrites for deleted products
         */
        if ($resultSql) {
            $connection->delete(
                $tableName,
                [
                    'request_path = ?' => $result['request_path'] . '?color=' . $attribute,
                    'entity_id != ?' => $product->getId()
                ]
            );
        }

        $connection->update(
            $tableName,
            [
                'request_path' => $result['request_path'] . '?color=' . $attribute
            ],
            [
                'target_path = ?' => $path,
                'entity_id = ?' => $product->getId()
            ]
        );
    }

    /**
     * Fixes Url Rewrites
     *
     * @param Product $product
     */
    public function setUrlRewrites(Product $product)
    {
        $this->urlPersist->deleteByData([
            UrlRewrite::ENTITY_ID => $product->getId(),
            UrlRewrite::ENTITY_TYPE => ProductUrlRewriteGenerator::ENTITY_TYPE,
            UrlRewrite::REDIRECT_TYPE => 0,
            UrlRewrite::STORE_ID => 0
        ]);
        $this->urlPersist->replace(
            $this->productUrlRewriteGenerator->generate($product)
        );
    }

    /**
     * This function deletes old product
     *
     * @param Product $product
     */
    public function deleteOldProduct(Product $product)
    {
        try {
            $oldProduct = $this->productRepository->get($product->getSku());

            if ($oldProduct) {
                $this->productRepository->delete($oldProduct);
            }
        } catch (\Exception $e) {}
    }
}
