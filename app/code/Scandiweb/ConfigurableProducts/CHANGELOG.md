Scandiweb_ConfigurableProducts changelog
========================

0.1.0:
- Added auto swatch select for label visual swatch
- Added product duplicate

0.2.0:
- Added duplicate product attributes
- Fixed issue on new product creation
- Fixed possible swatch issue 

0.2.1:
- Updated product attribute to be filtered
- Added filter for duplicated product in product grid

0.2.2:
- Moved Product attribute update to ConfigurableProducts module

0.2.3:
- Added confgurable product color attribute for storing information
