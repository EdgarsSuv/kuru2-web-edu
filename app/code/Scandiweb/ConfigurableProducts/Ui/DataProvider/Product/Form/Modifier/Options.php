<?php
/**
 * @vendor Scandiweb
 * @module Scandiweb_ConfigurableProducts
 * @author Kristaps Stalidzāns <info@scandiweb.com>
 * @copyright Copyright (c) 2017 Scandiweb, Ltd (http://scandiweb.com)
 * @license http://opensource.org/licenses/afl-3.0.php Academic Free License (AFL 3.0)
 */

namespace Scandiweb\ConfigurableProducts\Ui\DataProvider\Product\Form\Modifier;

use Magento\Catalog\Api\Data\ProductSearchResultsInterface;
use Magento\Framework\Api\SearchCriteriaBuilder;
use Magento\Framework\App\Cache\Type\Block;
use Magento\Ui\Component\Form;
use Magento\Framework\App\ObjectManager;
use Magento\Framework\App\CacheInterface;
use Magento\Framework\DB\Helper as DbHelper;
use Magento\Store\Model\StoreManagerInterface;
use Magento\Framework\Exception\NoSuchEntityException;
use Magento\Catalog\Api\Data\ProductInterface;
use Magento\Framework\Stdlib\ArrayManager;
use Magento\Catalog\Model\Product\Visibility;
use Magento\Catalog\Model\Locator\LocatorInterface;
use Magento\Catalog\Api\ProductRepositoryInterface;
use Magento\Catalog\Model\Category as CategoryModel;
use Magento\ConfigurableProduct\Model\Product\Type\Configurable;
use Magento\Catalog\Ui\DataProvider\Product\Form\Modifier\AbstractModifier;
use Magento\Catalog\Model\ResourceModel\Product\Attribute\CollectionFactory;
use Magento\ConfigurableProduct\Api\LinkManagementInterface;
use Magento\Catalog\Model\ResourceModel\Category\CollectionFactory as CategoryCollectionFactory;

/**
 * Class Websites customizes websites panel
 */
class Options extends AbstractModifier
{
    const SORT_ORDER = 2;
    /**#@+
     * Category tree cache id
     */
    const CATEGORY_TREE_ID = 'CATALOG_PRODUCT_CATEGORY_TREE';
    /**#@-*/

    /**
     * @var LocatorInterface
     */
    protected $locator;

    /**
     * @var array
     */
    protected $productList;

    /**
     * @var ProductRepositoryInterface
     */
    protected $productRepository;

    /**
     * @var StoreManagerInterface
     */
    protected $storeManager;

    /**
     * @var CategoryCollectionFactory
     */
    private $categoryCollectionFactory;

    /**
     * @var CacheInterface
     */
    private $cacheManager;
    
    /**
     * @var DbHelper
     */
    private $dbHelper;

    /**
     * @var ArrayManager
     */
    private $arrayManager;

    /**
     * @var CollectionFactory
     */
    private $attributeCollectionFactory;

    /**
     * @var SearchCriteriaBuilder
     */
    private $searchCriteriaBuilder;

    /**
     * @var LinkManagementInterface
     */
    private $linkManagement;

    /**
     * @param LocatorInterface $locator
     * @param ProductRepositoryInterface $productRepository
     * @param StoreManagerInterface $storeManager
     * @param CategoryCollectionFactory $categoryCollectionFactory
     * @param DbHelper $dbHelper
     * @param ArrayManager $arrayManager
     * @param CollectionFactory $attributeCollectionFactory
     * @param SearchCriteriaBuilder $searchCriteriaBuilder
     * @param LinkManagementInterface $linkManagement
     */
    public function __construct
    (
        LocatorInterface $locator,
        ProductRepositoryInterface $productRepository,
        StoreManagerInterface $storeManager,
        CategoryCollectionFactory $categoryCollectionFactory,
        DbHelper $dbHelper,
        ArrayManager $arrayManager,
        CollectionFactory $attributeCollectionFactory,
        SearchCriteriaBuilder $searchCriteriaBuilder,
        LinkManagementInterface $linkManagement
    ){
        $this->locator = $locator;
        $this->productRepository = $productRepository;
        $this->storeManager = $storeManager;
        $this->categoryCollectionFactory = $categoryCollectionFactory;
        $this->dbHelper = $dbHelper;
        $this->arrayManager = $arrayManager;
        $this->attributeCollectionFactory = $attributeCollectionFactory;
        $this->searchCriteriaBuilder = $searchCriteriaBuilder;
        $this->linkManagement = $linkManagement;
    }

    /**
     * Must be empty :/
     *
     * {@inheritdoc}
     */
    public function modifyData(array $data)
    {
        return [];
    }

    /**
     * Adds checkbox field and categories if product is configurable
     *
     * {@inheritdoc}
     */
    public function modifyMeta(array $meta)
    {
        $product = $this->locator->getProduct();
        $productDuplicate = $product->getData('product_duplicate');

        if (!$this->storeManager->isSingleStoreMode() && $product->getData('type_id') == Configurable::TYPE_CODE && !$productDuplicate) {
            $meta = array_replace_recursive(
                $meta,
                [
                    'options' => [
                        'arguments' => [
                            'data' => [
                                'config' => [
                                    'additionalClasses' => 'admin__fieldset-product-options',
                                    'label' => __('Product Options'),
                                    'collapsible' => true,
                                    'componentType' => Form\Fieldset::NAME,
                                    'dataScope' => self::DATA_SCOPE_PRODUCT,
                                    'disabled' => false,
                                    'sortOrder' => $this->getNextGroupSortOrder(
                                        $meta,
                                        'search-engine-optimizations',
                                        self::SORT_ORDER
                                    )
                                ],
                            ],
                        ],
                        'children' => $this->getFieldsForFieldset($product),
                    ],
                ]
            );
        }

        return $meta;
    }

    /**
     * Prepares children for the parent fieldset with color checkbox and its categories
     *
     * @param ProductInterface $product
     * @return array
     */
    protected function getFieldsForFieldset(ProductInterface $product)
    {
        $children = [];
        $sortOrder = 0;
        $fieldCode = 'category_ids';
        $config = $product->getTypeInstance(true);
        $attributesArray = $config->getConfigurableAttributesAsArray($product);
        $colorData = $this->checkProductVisibility($product, $attributesArray);

        foreach ($attributesArray as $attributeItem) {
            if ($attributeItem['attribute_code'] == 'color') {
                foreach ($attributeItem['values'] as $valueItem) {
                    $children[$valueItem['label']] = [
                        'arguments' => [
                            'data' => [
                                'config' => [
                                    'dataType' => Form\Element\DataType\Number::NAME,
                                    'componentType' => Form\Field::NAME,
                                    'formElement' => Form\Element\Checkbox::NAME,
                                    'description' => __($valueItem['label']),
                                    'sortOrder' => $sortOrder,
                                    'dataScope' => 'product_color_ids.' . $valueItem['label'],
                                    'label' => 'Product color attribute',
                                    'valueMap' => [
                                        'true' => (string)$valueItem['value_index'],
                                        'false' => '0',
                                    ],
                                    'value' => $colorData ? (string)$colorData[$valueItem['label']] : '0',
                                ],
                            ],
                        ],
                    ];


                    $children[strtolower($valueItem['label']) . '_filtrable_color'] = [
                        'arguments' => [
                            'data' => [
                                'config' => [
                                    'formElement' => 'multiselect',
                                    'componentType' => 'field',
                                    'component' => 'Magento_Ui/js/form/element/multiselect',
                                    'filterOptions' => true,
                                    'chipsEnabled' => true,
                                    'disableLabel' => true,
                                    'levelsVisibility' => '1',
                                    'elementTmpl' => 'ui/form/element/multiselect',
                                    'options' => $this->getAttributeData('filterable_color'),
                                    'listens' => [
                                        'index=create_category:responseData' => 'setParsed',
                                        'newOption' => 'toggleOptionSelected'
                                    ],
                                    'label' => 'Filtrable color attribute',
                                    'config' => [
                                        'dataScope' => $fieldCode,
                                        'sortOrder' => $sortOrder++,
                                    ],
                                    'value' => $this->getAttributeSelectData($valueItem['label'],'filterable_color')
                                ],
                            ],
                        ],
                    ];

                    $children[strtolower($valueItem['label']) . '_category_ids'] = [
                        'arguments' => [
                            'data' => [
                                'config' => [
                                    'formElement' => 'select',
                                    'componentType' => 'field',
                                    'component' => 'Magento_Catalog/js/components/new-category',
                                    'filterOptions' => true,
                                    'chipsEnabled' => true,
                                    'disableLabel' => true,
                                    'levelsVisibility' => '1',
                                    'elementTmpl' => 'ui/grid/filters/elements/ui-select',
                                    'options' => $this->getCategoriesTree(),
                                    'listens' => [
                                        'index=create_category:responseData' => 'setParsed',
                                        'newOption' => 'toggleOptionSelected'
                                    ],
                                    'label' => 'Product categories',
                                    'config' => [
                                        'dataScope' => $fieldCode,
                                        'sortOrder' => $sortOrder++,
                                    ],
                                    'value' => $this->getProductCategoriesIds($valueItem['label'])
                                ],
                            ],
                        ],
                    ];

                    $sortOrder++;
                }
            }
        }

        return $children;
    }

    /**
     * @param ProductInterface $product
     * @param $colorLabel
     * @return ProductSearchResultsInterface|null
     */
    public function getProductDuplicates(ProductInterface $product, $colorLabel)
    {
        $searchCriteria = $this->searchCriteriaBuilder
            ->addFilter('parent_sku', $product->getSku(), 'eq')
            ->addFilter('product_color_data', $colorLabel, 'eq')
            ->addFilter('product_duplicate', true, 'eq')
            ->create();

        try {
            $childConfigurable = $this->productRepository->getList($searchCriteria);
        } catch (\Exception $e) {
            $childConfigurable = null;
        }

        return $childConfigurable;
    }

    /**
     * Gets information about selected product data
     *
     * @param ProductInterface $product
     * @param $colorData
     * @return array
     */
    public function getSelectedData(ProductInterface $product, $colorData) : array
    {
        $selectedData = [];

        foreach ($colorData as $attributeItem) {
            if ($attributeItem['attribute_code'] == 'color') {
                foreach ($attributeItem['values'] as $valueItem) {
                    $color = '0';

                    try {
                        $childConfigurable = $this->getProductDuplicates($product, $valueItem['label']);

                        if ($childConfigurable->getTotalCount() > 0) {
                            foreach ($childConfigurable->getItems() as $item) {
                                if ($item->getVisibility() == Visibility::VISIBILITY_BOTH || $item->getVisibility() == Visibility::VISIBILITY_IN_CATALOG) {
                                    $color = $valueItem['value_index'];
                                }
                            }
                        }
                    } catch (\Exception $e) {}

                    $selectedData[$valueItem['label']] = $color;
                }
            }
        }

        return $selectedData;
    }

    /**
     * Checks Product Visibility
     *
     * @param ProductInterface $product
     * @param $colorData
     * @return array
     */
    public function checkProductVisibility(ProductInterface $product, $colorData)
    {
        $selectedData = [];

        foreach ($colorData as $attributeItem) {
            if ($attributeItem['attribute_code'] == 'color') {
                foreach ($attributeItem['values'] as $valueItem) {
                    $color = '0';

                    try {
                        $childConfigurable = $this->getProductDuplicates($product, $valueItem['label']);

                        if ($childConfigurable->getTotalCount() == 1) {
                            foreach ($childConfigurable->getItems() as $item) {
                                if ($item->getVisibility() == Visibility::VISIBILITY_BOTH || $item->getVisibility() == Visibility::VISIBILITY_IN_CATALOG) {
                                    $selectedData[$valueItem['label']] = $valueItem['value_index'];
                                } else {
                                    $selectedData[$valueItem['label']] = $color;
                                }
                            }
                        } else {
                            $selectedData[$valueItem['label']] = $color;
                        }
                    } catch (\Exception $e) {}
                }
            }
        }

        return $selectedData;
    }

    /**
     * Get attribute options from default store view
     *
     * @param string $code
     * @return array
     */
    public function getAttributeData(string $code) : array
    {
        $attribute = $this->attributeCollectionFactory->create();
        $attribute->addFieldToFilter('attribute_code', ['eq' => $code]);
        $attribute->addIsFilterableFilter();
        $layeredNavAttributes = $attribute->load()->getItems();
        $storeId = $this->storeManager->getStore('default')->getId();
        $options = '';

        foreach ($layeredNavAttributes AS $attr) {
            $attr->setStoreId($storeId);
            $options = $attr->getSource()->getAllOptions();
        }

        return $options;
    }

    /**
     * @param string $label
     * @param string $code
     * @return string
     */
    public function getAttributeSelectData(string $label, string $code)
    {
        $product = $this->locator->getProduct();
        $dataIds = '';

        $childConfigurable = $this->getProductDuplicates($product, $label);

        if ($childConfigurable->getTotalCount() == 1) {
            foreach ($childConfigurable->getItems() as $item) {
                $dataIds = $item->getData($code);
            }
        } else {
            $childConfigurableProduct = $this->productRepository->get($product->getSku());

            if ($childConfigurableProduct) {
                $dataIds = $childConfigurableProduct->getData($code);
            }

            if (is_null($dataIds)) {
                $dataIds = '';
            }
        }

        return $dataIds;
    }

    /**
     * Gets child product categories ids if child product doesn't exist it gets master product category ids
     *
     * @param string $label
     * @return array
     */
    private function getProductCategoriesIds(string $label) : array
    {
        $product = $this->locator->getProduct();
        $categoryIds = [];

        $childConfigurable = $this->getProductDuplicates($product, $label);

        if ($childConfigurable->getTotalCount() == 1) {
            foreach ($childConfigurable->getItems() as $item) {
                $categoryIds = $item->getCategoryIds();
            }
        } else {
            $childConfigurableProduct = $this->productRepository->get($product->getSku());

            if ($childConfigurableProduct) {
                $categoryIds = $childConfigurableProduct->getCategoryIds();
            }
        }

        return $categoryIds;
    }

    /**
     * Retrieve cache interface
     *
     * @return CacheInterface
     * @deprecated
     */
    private function getCacheManager()
    {
        if (!$this->cacheManager) {
            $this->cacheManager = ObjectManager::getInstance()
                ->get(CacheInterface::class);
        }
        return $this->cacheManager;
    }

    /**
     * Retrieve categories tree
     *
     * @param string|null $filter
     * @return array
     */
    protected function getCategoriesTree($filter = null)
    {
        $categoryTree = $this->getCacheManager()->load(self::CATEGORY_TREE_ID . '_' . $filter);
        if ($categoryTree) {
            return unserialize($categoryTree);
        }

        $storeId = $this->locator->getStore()->getId();
        /* @var $matchingNamesCollection \Magento\Catalog\Model\ResourceModel\Category\Collection */
        $matchingNamesCollection = $this->categoryCollectionFactory->create();

        if ($filter !== null) {
            $matchingNamesCollection->addAttributeToFilter(
                'name',
                ['like' => $this->dbHelper->addLikeEscape($filter, ['position' => 'any'])]
            );
        }

        $matchingNamesCollection->addAttributeToSelect('path')
            ->addAttributeToFilter('entity_id', ['neq' => CategoryModel::TREE_ROOT_ID])
            ->setStoreId($storeId);

        $shownCategoriesIds = [];

        /** @var CategoryModel $category */
        foreach ($matchingNamesCollection as $category) {
            foreach (explode('/', $category->getPath()) as $parentId) {
                $shownCategoriesIds[$parentId] = 1;
            }
        }

        /* @var $collection \Magento\Catalog\Model\ResourceModel\Category\Collection */
        $collection = $this->categoryCollectionFactory->create();

        $collection->addAttributeToFilter('entity_id', ['in' => array_keys($shownCategoriesIds)])
            ->addAttributeToSelect(['name', 'is_active', 'parent_id'])
            ->setStoreId($storeId);

        $categoryById = [
            CategoryModel::TREE_ROOT_ID => [
                'value' => CategoryModel::TREE_ROOT_ID,
                'optgroup' => null,
            ],
        ];

        foreach ($collection as $category) {
            foreach ([$category->getId(), $category->getParentId()] as $categoryId) {
                if (!isset($categoryById[$categoryId])) {
                    $categoryById[$categoryId] = ['value' => $categoryId];
                }
            }

            $categoryById[$category->getId()]['is_active'] = $category->getIsActive();
            $categoryById[$category->getId()]['label'] = $category->getName();
            $categoryById[$category->getParentId()]['optgroup'][] = &$categoryById[$category->getId()];
        }

        $this->getCacheManager()->save(
            serialize($categoryById[CategoryModel::TREE_ROOT_ID]['optgroup']),
            self::CATEGORY_TREE_ID . '_' . $filter,
            [
                CategoryModel::CACHE_TAG,
                Block::CACHE_TAG
            ]
        );

        return $categoryById[CategoryModel::TREE_ROOT_ID]['optgroup'];
    }
}
