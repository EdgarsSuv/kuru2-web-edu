<?php
/**
 * @category     Scandiweb
 * @package      Scandiweb/ConfigurableProducts
 * @author       Krisjanis Veinbahs <krisjanisv@kurufootwear.com>
 * @copyright    Copyright (c) 2018 KURU Footwear. All rights reserved.
 */
namespace Scandiweb\ConfigurableProducts\Ui\Component\Listing\Column;

use \Magento\Ui\Component\Listing\Columns\Column;

class ExistingValue extends Column
{
    /**
     * Check if g_link attribute is set or not
     *
     * @param array $dataSource
     * @return array
     */
    public function prepareDataSource(array $dataSource)
    {
        if (isset($dataSource['data']['items'])) {
            foreach ($dataSource['data']['items'] as & $item) {
                $item[$this->getData('name')] = empty($item['g_link']) ? "0" : "1";
            }
        }

        return $dataSource;
    }
}
