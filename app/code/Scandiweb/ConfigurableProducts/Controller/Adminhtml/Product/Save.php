<?php
/**
 * @vendor Scandiweb
 * @module Scandiweb_ConfigurableProducts
 * @author Kristaps Stalidzāns <info@scandiweb.com>
 * @copyright Copyright (c) 2017 Scandiweb, Ltd (http://scandiweb.com)
 * @license http://opensource.org/licenses/afl-3.0.php Academic Free License (AFL 3.0)
 */
namespace Scandiweb\ConfigurableProducts\Controller\Adminhtml\Product;

use Psr\Log\LoggerInterface;
use Magento\Framework\Escaper;
use Magento\Backend\App\Action;
use Magento\Catalog\Model\Product;
use Magento\Catalog\Model\Product\Copier;
use Magento\Store\Model\StoreManagerInterface;
use Magento\Catalog\Api\ProductRepositoryInterface;
use Magento\Framework\Exception\LocalizedException;
use Scandiweb\ConfigurableProducts\Model\ProcessData;
use Magento\Catalog\Model\Product\TypeTransitionManager;
use Magento\Catalog\Api\CategoryLinkManagementInterface;
use Magento\Framework\App\Request\DataPersistorInterface;
use Magento\Catalog\Controller\Adminhtml\Product\Builder;
use Magento\Catalog\Model\Product\Action as ProductAction;
use Magento\ConfigurableProduct\Model\Product\Type\Configurable;
use Magento\Catalog\Controller\Adminhtml\Product\Initialization\Helper;

class Save extends \Magento\Catalog\Controller\Adminhtml\Product\Save
{
    /**
     * @var Copier
     */
    protected $productCopier;

    /**
     * @var TypeTransitionManager
     */
    protected $productTypeManager;

    /**
     * @var CategoryLinkManagementInterface
     */
    protected $categoryLinkManagement;

    /**
     * @var ProductRepositoryInterface
     */
    protected $productRepository;

    /**
     * @var DataPersistorInterface
     */
    protected $dataPersistor;

    /**
     * @var StoreManagerInterface
     */
    protected $storeManager;

    /**
     * @var ProcessData
     */
    protected $processData;

    /**
     * @var ProductAction
     */
    private $productAction;

    /**
     * @var LoggerInterface
     */
    private $logger;

    /**
     * @var Escaper
     */
    private $escaper;

    /**
     * @var Product
     */
    private $product;

    /**
     * Save constructor.
     * @param Action\Context $context
     * @param Builder $productBuilder
     * @param Helper $initializationHelper
     * @param Copier $productCopier
     * @param TypeTransitionManager $productTypeManager
     * @param ProductRepositoryInterface $productRepository
     * @param ProcessData $processData
     * @param ProductAction $productAction
     * @param LoggerInterface $logger
     * @param Escaper $escaper
     * @param Product $product
     */
    public function __construct(
        Action\Context $context,
        Builder $productBuilder,
        Helper $initializationHelper,
        Copier $productCopier,
        TypeTransitionManager $productTypeManager,
        ProductRepositoryInterface $productRepository,
        ProcessData $processData,
        ProductAction $productAction,
        LoggerInterface $logger,
        Escaper $escaper,
        Product $product
    ){
        $this->logger = $logger;
        $this->escaper = $escaper;
        $this->product = $product;
        $this->processData = $processData;
        $this->productAction = $productAction;

        parent::__construct(
            $context,
            $productBuilder,
            $initializationHelper,
            $productCopier,
            $productTypeManager,
            $productRepository
        );
    }

    /**
     * Save product action
     *
     * @return \Magento\Backend\Model\View\Result\Redirect
     * @SuppressWarnings(PHPMD.CyclomaticComplexity)
     * @SuppressWarnings(PHPMD.NPathComplexity)
     */
    public function execute()
    {
        $storeId = $this->getRequest()->getParam('store', 0);
        $store = $this->getStoreManager()->getStore($storeId);
        $this->getStoreManager()->setCurrentStore($store->getCode());
        $redirectBack = $this->getRequest()->getParam('back', false);
        $productId = $this->getRequest()->getParam('id');
        $resultRedirect = $this->resultRedirectFactory->create();
        $data = $this->getRequest()->getPostValue();
        $productAttributeSetId = $this->getRequest()->getParam('set');
        $productTypeId = $this->getRequest()->getParam('type');

        if ($data) {
            try {
                $product = $this->initializationHelper->initialize(
                    $this->productBuilder->build($this->getRequest())
                );
                $this->productTypeManager->processProduct($product);

                if (isset($data['product'][$product->getIdFieldName()])) {
                    throw new LocalizedException(__('Unable to save product'));
                }

                $originalSku = $product->getSku();
                $product->save();
                $this->handleImageRemoveError($data, $product->getId());
                $this->getCategoryLinkManagement()->assignProductToCategories(
                    $product->getSku(),
                    $product->getCategoryIds()
                );
                $productId = $product->getEntityId();
                $productAttributeSetId = $product->getAttributeSetId();
                $productTypeId = $product->getTypeId();
                $this->copyToStores($data, $productId);
                $this->messageManager->addSuccessMessage(__('You saved the product.'));
                $this->getDataPersistor()->clear('catalog_product');

                if ($product->getSku() != $originalSku) {
                    $this->messageManager->addNoticeMessage(
                        __(
                            'SKU for product %1 has been changed to %2.',
                            $this->escaper->escapeHtml($product->getName()),
                            $this->escaper->escapeHtml($product->getSku())
                        )
                    );
                }
                $this->_eventManager->dispatch(
                    'controller_action_catalog_product_save_entity_after',
                    ['controller' => $this, 'product' => $product]
                );

                if ($product->getData('type_id') === Configurable::TYPE_CODE && $redirectBack !== 'duplicate') {
                    $this->processColorData($data['product'], $product);
                }

                if ($redirectBack === 'duplicate') {
                    $newProduct = $this->productCopier->copy($product);
                    $this->messageManager->addSuccessMessage(__('You duplicated the product.'));
                }

            } catch (LocalizedException $e) {
                $this->messageManager->addErrorMessage($e->getMessage());
                $this->getDataPersistor()->set('catalog_product', $data);
                $redirectBack = $productId ? true : 'new';
            } catch (\Exception $e) {
                $this->logger->critical($e);
                $this->messageManager->addErrorMessage($e->getMessage());
                $this->getDataPersistor()->set('catalog_product', $data);
                $redirectBack = $productId ? true : 'new';
            }

        } else {
            $resultRedirect->setPath('catalog/*/', ['store' => $storeId]);
            $this->messageManager->addErrorMessage('No data to save');

            return $resultRedirect;
        }

        if ($redirectBack === 'new') {
            $resultRedirect->setPath(
                'catalog/*/new',
                ['set' => $productAttributeSetId, 'type' => $productTypeId]
            );
        } elseif ($redirectBack === 'duplicate' && isset($newProduct)) {
            $resultRedirect->setPath(
                'catalog/*/edit',
                ['id' => $newProduct->getEntityId(), 'back' => null, '_current' => true]
            );
        } elseif ($redirectBack) {
            $resultRedirect->setPath(
                'catalog/*/edit',
                ['id' => $productId, '_current' => true, 'set' => $productAttributeSetId]
            );
        } else {
            $resultRedirect->setPath('catalog/*/', ['store' => $storeId]);
        }

        return $resultRedirect;
    }

    /**
     * Gets configurable product color attribute information
     *
     * @param array $productPostData
     * @param Product $product
     */
    public function processColorData(array $productPostData, Product $product)
    {
        $duplicateAndColorChk = ($product->getData('product_duplicate') == 0) && array_key_exists('product_color_ids', $productPostData) && $productPostData['product_color_ids'];

        if (($product->getData('type_id') == Configurable::TYPE_CODE) && $duplicateAndColorChk) {
            $categoryData = $this->processData->processConfigurableProduct($product, $productPostData);
            $this->updateProductData($product, $categoryData);
        }
    }

    /**
     * Updates product attribute information in master configurable product
     *
     * @param Product $product
     * @param $categoryData
     */
    public function updateProductData(Product $product, $categoryData)
    {
        $this->productAction->updateAttributes([$product->getId()], ['product_data' => serialize($categoryData)], 0);
    }

    /**
     * Notify customer when image was not deleted in specific case.
     * TODO: temporary workaround must be eliminated in MAGETWO-45306
     *
     * @param array $postData
     * @param int $productId
     * @return void
     */
    private function handleImageRemoveError($postData, $productId)
    {
        if (isset($postData['product']['media_gallery']['images'])) {
            $removedImagesAmount = 0;
            foreach ($postData['product']['media_gallery']['images'] as $image) {
                if (!empty($image['removed'])) {
                    $removedImagesAmount++;
                }
            }
            if ($removedImagesAmount) {
                $expectedImagesAmount = count($postData['product']['media_gallery']['images']) - $removedImagesAmount;
                $product = $this->productRepository->getById($productId);
                if ($expectedImagesAmount != count($product->getMediaGallery('images'))) {
                    $this->messageManager->addNoticeMessage(
                        __('The image cannot be removed as it has been assigned to the other image role')
                    );
                }
            }
        }
    }

    /**
     * Do copying data to stores
     *
     * @param array $data
     * @param int $productId
     * @return void
     */
    protected function copyToStores($data, $productId)
    {
        if (!empty($data['product']['copy_to_stores'])) {
            foreach ($data['product']['copy_to_stores'] as $websiteId => $group) {
                if (isset($data['product']['website_ids'][$websiteId])
                    && (bool)$data['product']['website_ids'][$websiteId]) {
                    foreach ($group as $store) {
                        $copyFrom = (isset($store['copy_from'])) ? $store['copy_from'] : 0;
                        $copyTo = (isset($store['copy_to'])) ? $store['copy_to'] : 0;
                        if ($copyTo) {
                            $this->product->setStoreId($copyFrom)
                                ->load($productId)
                                ->setStoreId($copyTo)
                                ->setCopyFromView(true)
                                ->save();
                        }
                    }
                }
            }
        }
    }

    /**
     * @return CategoryLinkManagementInterface
     */
    private function getCategoryLinkManagement()
    {
        if (null === $this->categoryLinkManagement) {
            $this->categoryLinkManagement = \Magento\Framework\App\ObjectManager::getInstance()
                ->get('Magento\Catalog\Api\CategoryLinkManagementInterface');
        }
        return $this->categoryLinkManagement;
    }

    /**
     * @return StoreManagerInterface
     * @deprecated
     */
    private function getStoreManager()
    {
        if (null === $this->storeManager) {
            $this->storeManager = \Magento\Framework\App\ObjectManager::getInstance()
                ->get('Magento\Store\Model\StoreManagerInterface');
        }
        return $this->storeManager;
    }

    /**
     * Retrieve data persistor
     *
     * @return DataPersistorInterface|mixed
     * @deprecated
     */
    protected function getDataPersistor()
    {
        if (null === $this->dataPersistor) {
            $this->dataPersistor = $this->_objectManager->get(DataPersistorInterface::class);
        }

        return $this->dataPersistor;
    }
}
