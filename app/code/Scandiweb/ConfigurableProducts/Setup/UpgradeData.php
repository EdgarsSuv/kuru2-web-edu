<?php
/**
 * @vendor Scandiweb
 * @module Scandiweb_ConfigurableProducts
 * @author Kristaps Stalidzāns <info@scandiweb.com>
 * @copyright Copyright (c) 2017 Scandiweb, Ltd (http://scandiweb.com)
 * @license http://opensource.org/licenses/afl-3.0.php Academic Free License (AFL 3.0)
 */
namespace Scandiweb\ConfigurableProducts\Setup;

use Psr\Log\LoggerInterface;
use Magento\Catalog\Model\Product;
use Magento\Eav\Setup\EavSetupFactory;
use Magento\Catalog\Model\Product\Action;
use Magento\Catalog\Model\ProductFactory;
use Magento\Framework\Api\SearchCriteriaBuilder;
use Magento\Framework\Setup\UpgradeDataInterface;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Catalog\Api\ProductRepositoryInterface;
use Magento\Framework\Setup\ModuleDataSetupInterface;
use Magento\Catalog\Model\ResourceModel\Product as ProductResource;

/**
 * Upgrade Data script
 * @codeCoverageIgnore
 */
class UpgradeData implements UpgradeDataInterface
{

    /**
     * EAV setup factory
     *
     * @var EavSetupFactory
     */
    private $eavSetupFactory;

    /**
     * @var SearchCriteriaBuilder
     */
    private $searchCriteriaBuilder;

    /**
     * @var ProductRepositoryInterface
     */
    private $productRepository;

    /**
     * @var ProductResource
     */
    private $product;

    /**
     * @var ProductFactory
     */
    private $productFactory;

    /**
     * @var Action
     */
    private $productAction;

    /**
     * @var LoggerInterface
     */
    private $logger;

    /**
     * Init
     *
     * @param EavSetupFactory $eavSetupFactory
     * @param SearchCriteriaBuilder $searchCriteriaBuilder
     * @param ProductRepositoryInterface $productRepository
     * @param ProductResource $product
     * @param ProductFactory $productFactory
     * @param Action $productAction
     * @param LoggerInterface $logger
     */
    public function __construct
    (
        EavSetupFactory $eavSetupFactory,
        SearchCriteriaBuilder $searchCriteriaBuilder,
        ProductRepositoryInterface $productRepository,
        ProductResource $product,
        ProductFactory $productFactory,
        Action $productAction,
        LoggerInterface $logger
    ){
        $this->eavSetupFactory = $eavSetupFactory;
        $this->searchCriteriaBuilder = $searchCriteriaBuilder;
        $this->productRepository = $productRepository;
        $this->product = $product;
        $this->productFactory = $productFactory;
        $this->productAction = $productAction;
        $this->logger = $logger;
    }

    /**
     * {@inheritdoc}
     * @SuppressWarnings(PHPMD.ExcessiveMethodLength)
     */
    public function upgrade(ModuleDataSetupInterface $setup, ModuleContextInterface $context)
    {
        $setup->startSetup();

        if ($context->getVersion()
            && version_compare($context->getVersion(), '0.2.0') < 0
        ) {
            $this->addAttributes($setup);
        }
        if ($context->getVersion()
            && version_compare($context->getVersion(), '0.2.1') < 0
        ) {
            $this->addAttributes($setup);
            $this->updateAttribute($setup);
        }
        if ($context->getVersion()
            && version_compare($context->getVersion(), '0.2.2') < 0
        ) {
            $this->updateProductAttributes();
        }
        if ($context->getVersion()
            && version_compare($context->getVersion(), '0.2.3') < 0
        ) {
            $this->addFeColorAttribute($setup);
        }

        $setup->endSetup();
    }

    /**
     * @param $setup
     */
    public function addAttributes($setup)
    {
        /** @var EavSetupFactory $eavSetupFactory */
        $eavSetup = $this->eavSetupFactory->create(['setup' => $setup]);
        $entityType = Product::ENTITY;

        // Create text attributes
        if (!$eavSetup->getAttribute($entityType, 'parent_sku')) {
            $eavSetup->addAttribute(
                $entityType,
                'parent_sku',
                [
                    'type' => 'varchar',
                    'label' => 'Parent SKU',
                    'input' => 'text',
                    'required' => false,
                    'user_defined' => false,
                    'searchable' => false,
                    'filterable' => false,
                    'comparable' => false,
                    'visible_on_front' => false,
                    'visible_in_advanced_search' => false,
                    'apply_to' => '',
                    'is_visible_in_grid' => false,
                    'is_filterable_in_grid' => false,
                    'used_in_product_listing' => false,
                ]
            );
        }

        $eavSetup->addAttributeToSet($entityType,
            $eavSetup->getAttributeSetId($entityType, 'Default'),
            'product_details',
            $eavSetup->getAttributeId($entityType, 'parent_sku')
        );

        // Create text attributes
        if (!$eavSetup->getAttribute($entityType, 'product_data')) {
            $eavSetup->addAttribute(
                $entityType,
                'product_data',
                [
                    'type' => 'varchar',
                    'label' => 'Product Data',
                    'input' => 'text',
                    'required' => false,
                    'user_defined' => false,
                    'searchable' => false,
                    'filterable' => false,
                    'comparable' => false,
                    'visible_on_front' => false,
                    'visible_in_advanced_search' => false,
                    'apply_to' => '',
                    'is_visible_in_grid' => false,
                    'is_filterable_in_grid' => false,
                    'used_in_product_listing' => false,
                ]
            );
        }

        $eavSetup->addAttributeToSet($entityType,
            $eavSetup->getAttributeSetId($entityType, 'Default'),
            'product_details',
            $eavSetup->getAttributeId($entityType, 'product_data')
        );

        // Create text attributes
        if (!$eavSetup->getAttribute($entityType, 'product_duplicate')) {
            $eavSetup->addAttribute(
                $entityType,
                'product_duplicate',
                [
                    'type' => 'int',
                    'backend' => 'Magento\Catalog\Model\Product\Attribute\Backend\Boolean',
                    'frontend' => '',
                    'label' => 'Product Duplicate',
                    'source' => 'Magento\Eav\Model\Entity\Attribute\Source\Boolean',
                    'input' => 'boolean',
                    'class' => '',
                    'visible' => true,
                    'required' => false,
                    'user_defined' => false,
                    'default' => 0,
                    'searchable' => false,
                    'filterable' => true,
                    'comparable' => false,
                    'visible_on_front' => false,
                    'used_in_product_listing' => true,
                    'unique' => false,
                    'apply_to' => ''
                ]
            );
        }

        $eavSetup->addAttributeToSet($entityType,
            $eavSetup->getAttributeSetId($entityType, 'Default'),
            'product_details',
            $eavSetup->getAttributeId($entityType, 'product_duplicate')
        );
    }

    /**
     * Updates product attributes missing data
     *
     * @param $setup
     */
    public function updateAttribute($setup)
    {
        $eavSetup = $this->eavSetupFactory->create(['setup' => $setup]);
        $entityType = Product::ENTITY;

        if ($eavSetup->getAttribute($entityType, 'product_duplicate')) {
            $eavSetup->updateAttribute($entityType, 'product_duplicate', 'is_visible_in_grid', '1');
            $eavSetup->updateAttribute($entityType, 'product_duplicate', 'is_filterable_in_grid', '1');
            $eavSetup->updateAttribute($entityType, 'product_duplicate', 'backend_model', 'Magento\Catalog\Model\Product\Attribute\Backend\Boolean');
            $eavSetup->updateAttribute($entityType, 'product_duplicate', 'source_model', 'Magento\Eav\Model\Entity\Attribute\Source\Boolean');
        }
    }

    /**
     * Updates Product attributes
     */
    public function updateProductAttributes()
    {
        $searchCriteria = $this->searchCriteriaBuilder
            ->addFilter('sku', null, 'neq')
            ->addFilter('name', null, 'neq')
            ->create();
        $productIds = [];

        try {
            $repositoryList = $this->productRepository->getList($searchCriteria);

            foreach ($repositoryList->getItems() as $repositoryItem) {
                array_push($productIds, $repositoryItem->getId());
            }

            $this->productAction->updateAttributes($productIds, ['product_duplicate' => '0'], 0);
        } catch (\Exception $e) {
            $this->logger->error($e->getMessage());
        }
    }

    public function addFeColorAttribute($setup)
    {
        /** @var EavSetupFactory $eavSetupFactory */
        $eavSetup = $this->eavSetupFactory->create(['setup' => $setup]);
        $entityType = Product::ENTITY;

        // Create text attributes
        if (!$eavSetup->getAttribute($entityType, 'product_color_data')) {
            $eavSetup->addAttribute(
                $entityType,
                'product_color_data',
                [
                    'type' => 'varchar',
                    'label' => 'Product Color',
                    'input' => 'text',
                    'visible' => true,
                    'required' => false,
                    'user_defined' => false,
                    'searchable' => false,
                    'filterable' => false,
                    'comparable' => false,
                    'visible_on_front' => true,
                    'visible_in_advanced_search' => false,
                    'apply_to' => '',
                    'is_visible_in_grid' => false,
                    'is_filterable_in_grid' => false,
                    'used_in_product_listing' => true,
                ]
            );
        }

        $eavSetup->addAttributeToSet($entityType,
            $eavSetup->getAttributeSetId($entityType, 'Default'),
            'product_details',
            $eavSetup->getAttributeId($entityType, 'product_color_data')
        );
    }
}
