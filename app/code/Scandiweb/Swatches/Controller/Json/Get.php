<?php
/**
 * @vendor Scandiweb
 * @module Scandiweb_Swatches
 * @author Krisjanis Veinbahs <krisjanisv@scandiweb.com>
 * @copyright Copyright (c) 2017 Magento. All rights reserved.
 * @license http://opensource.org/licenses/afl-3.0.php Academic Free License (AFL 3.0)
 */
namespace Scandiweb\Swatches\Controller\Json;

use Magento\Framework\Controller\Result\JsonFactory;
use Magento\Framework\App\Action\Action;
use Magento\Framework\App\Action\Context;
use Scandiweb\Swatches\Block\Product\Renderer\Configurable;
use Magento\Catalog\Api\ProductRepositoryInterface;

class Get extends Action
{
    /**
     * @var JsonFactory
     */
    protected $resultJsonFactory;

    /**
     * @var Configurable
     */
    protected $swatchBlock;

    /**
     * @var ProductRepositoryInterface
     */
    protected $productRepository;

    /**
     * Get constructor.
     * @param Configurable $swatchBlock
     * @param JsonFactory $resultJsonFactory
     * @param Context $context
     * @param ProductRepositoryInterface $productRepository
     */
    public function __construct(
        Configurable $swatchBlock,
        JsonFactory $resultJsonFactory,
        Context $context,
        ProductRepositoryInterface $productRepository
    )
    {
        $this->swatchBlock = $swatchBlock;
        $this->resultJsonFactory = $resultJsonFactory;
        $this->productRepository = $productRepository;

        parent::__construct($context);
    }

    /**
     * Return swatch JSON config
     * @return mixed
     */
    public function execute()
    {
        try {
            $productId = $this->_request->getParam('id');
            $product = $this->productRepository->getById($productId);
            $productSwatchBlock = $this->swatchBlock->setProduct($product);
            $swatchConfig = $productSwatchBlock->getConfig();

            $payload = [
                'success' => 1,
                'jsonConfig' => $swatchConfig
            ];
        } catch (\Exception $exception) {
            $payload = [
                'success' => 0
            ];
        }

        return $this->resultJsonFactory->create()->setData($payload);
    }
}