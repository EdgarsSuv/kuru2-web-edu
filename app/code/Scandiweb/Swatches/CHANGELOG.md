Scandiweb_Swatches changelog
========================

0.1.0:
- Added swatch image fallback for color swatch in product category page
- Added visual gradient as swatch option
