<?php
/**
 * @vendor Scandiweb
 * @module Scandiweb_Swatches
 * @author Kristaps Stalidzāns <info@scandiweb.com>
 * @copyright Copyright (c) 2017 Magento. All rights reserved.
 * @license http://opensource.org/licenses/afl-3.0.php Academic Free License (AFL 3.0)
 */
namespace Scandiweb\Swatches\Block\Product\Renderer;

use Magento\Framework\App\ObjectManager;
use Magento\Framework\Locale\Format;
use Magento\Catalog\Api\Data\ProductInterface;
use Magento\Catalog\Api\ProductRepositoryInterface;
use Magento\Catalog\Block\Product\Context;
use Magento\Catalog\Helper\Product as CatalogProduct;
use Magento\ConfigurableProduct\Helper\Data;
use Magento\ConfigurableProduct\Model\ConfigurableAttributeData;
use Magento\Customer\Helper\Session\CurrentCustomer;
use Magento\Eav\Model\Config;
use Magento\Framework\App\RequestInterface;
use Magento\Framework\Json\EncoderInterface;
use Magento\Framework\Pricing\PriceCurrencyInterface;
use Magento\Framework\Stdlib\ArrayUtils;
use Magento\Swatches\Helper\Data as SwatchData;
use Magento\Swatches\Helper\Media;
use Magento\Swatches\Model\SwatchAttributesProvider;
use Scandiweb\Swatches\Helper\Data as ScandiData;
use Magento\Catalog\Model\Product;
use Magento\Framework\Exception\LocalizedException;


class Configurable extends \Magento\Swatches\Block\Product\Renderer\Configurable
{
    const CONTROLLER = 'category_view';
    const EDIT_CONTROLLER = 'cart_configure';

    /**
     * Title suffix
     */
    const TITLE_SUFFIX = 'design/head/title_suffix';

    /**
     * New pdp layout handle
     */
    const NEW_PDP_LAYOUT_HANDLE = 'catalog_product_view_layout_new_pdp_page';
    const NEW_EDIT_PDP_LAYOUT_HANDLE = 'checkout_cart_configure_layout_new_pdp_page';

    /**
     * @var ScandiData
     */
    protected $scandiHelper;

    /**
     * @var null|string
     */
    protected $controller = null;

    /**
     * @var RequestInterface
     */
    protected $request;

    /**
     * @var Config
     */
    private $eavConfig;

    /**
     * @var ProductRepositoryInterface
     */
    private $productRepositoryInterface;

    /**
     * @var Format
     */
    private $localeFormat;

    /**
     * Configurable constructor.
     * @param Context $context
     * @param Format $localeFormat
     * @param ArrayUtils $arrayUtils
     * @param EncoderInterface $jsonEncoder
     * @param Data $helper
     * @param CatalogProduct $catalogProduct
     * @param CurrentCustomer $currentCustomer
     * @param PriceCurrencyInterface $priceCurrency
     * @param ConfigurableAttributeData $configurableAttributeData
     * @param SwatchData $swatchHelper
     * @param Media $swatchMediaHelper
     * @param ScandiData $scandiHelper
     * @param Config $eavConfig
     * @param ProductRepositoryInterface $productRepositoryInterface
     * @param array $data
     * @param SwatchAttributesProvider|null $swatchAttributesProvider
     */
    public function __construct(
        Context $context,
        Format $localeFormat,
        ArrayUtils $arrayUtils,
        EncoderInterface $jsonEncoder,
        Data $helper,
        CatalogProduct $catalogProduct,
        CurrentCustomer $currentCustomer,
        PriceCurrencyInterface $priceCurrency,
        ConfigurableAttributeData $configurableAttributeData,
        SwatchData $swatchHelper,
        Media $swatchMediaHelper,
        ScandiData $scandiHelper,
        Config $eavConfig,
        ProductRepositoryInterface $productRepositoryInterface,
        array $data = [],
        SwatchAttributesProvider $swatchAttributesProvider = null
    )
    {
        $this->scandiHelper = $scandiHelper;
        $this->eavConfig = $eavConfig;
        $this->request = $context->getRequest();
        $this->productRepositoryInterface = $productRepositoryInterface;
        $this->localeFormat = $localeFormat ?: ObjectManager::getInstance()->get(Format::class);

        parent::__construct(
            $context,
            $arrayUtils,
            $jsonEncoder,
            $helper,
            $catalogProduct,
            $currentCustomer,
            $priceCurrency,
            $configurableAttributeData,
            $swatchHelper,
            $swatchMediaHelper,
            $data,
            $swatchAttributesProvider
        );
    }

    /**
     * Returns parent product if possible
     *
     * @param Product $product
     * @return ProductInterface|null
     */
    public function getParentSkuProduct(Product $product)
    {
        $parentSku = $product->getData('parent_sku');
        $parentProduct = null;

        if ($parentSku) {
            try {
                $parentProduct = $this->productRepositoryInterface->get($parentSku);
            } catch (\Exception $e) {}
        }

        return $parentProduct;
    }

    /**
     * @return array
     */
    protected function getSwatchAttributesData()
    {
        $swatchAttributes = [];
        $controller = $this->request->getControllerName() . '_' .$this->request->getActionName();

        try {
            /**
             * Made sure that controller path is not stored in memory while loading page
             */
            $isNewPDP = $controller === self::CONTROLLER || in_array(self::NEW_PDP_LAYOUT_HANDLE, $this->_layout->getUpdate()->getHandles());
            $isEditNewPDP = $controller === self::EDIT_CONTROLLER || in_array(self::NEW_EDIT_PDP_LAYOUT_HANDLE, $this->_layout->getUpdate()->getHandles());
            if ($isNewPDP || $isEditNewPDP) {
                $swatchAttributes = $this->scandiHelper->getSwatchAttributesAsArray($this->getProduct(), false);
            } else {
                $swatchAttributes = $this->scandiHelper->getSwatchAttributesAsArray($this->getProduct(), true);
            }
        } catch (LocalizedException $e) {
            $this->_logger->critical('Cannot get swatch attributes data' . $e->getMessage());
        }

        return $swatchAttributes;
    }

    /**
     * Override parent function
     *
     * @return Product
     */
    public function getProduct()
    {
        if (!$this->product) {
            $this->product = parent::getProduct();
        }

        if ($this->getParentSkuProduct($this->product)) {
            $this->product = $this->getParentSkuProduct($this->product);
        }

        return $this->product;
    }


    /**
     * Get Swatch config data
     *
     * @return string
     */
    public function getJsonSwatchConfig()
    {
        $attributesData = $this->getSwatchAttributesData();
        $allOptionIds = $this->getConfigurableOptionsIds($attributesData);
        $swatchesData = $this->swatchHelper->getSwatchesByOptionsId($allOptionIds);
        $attributeColorId = $this->eavConfig->getAttribute('catalog_product', 'color')->getId();

        $config = [];
        foreach ($attributesData as $attributeId => $attributeDataArray) {
            if (isset($attributeDataArray['options'])) {
                $config[$attributeId] = $this->addSwatchDataForAttribute(
                    $attributeDataArray['options'],
                    $swatchesData,
                    $attributeDataArray
                );
            }
        }

        /**
         * Added check for controller and attribute size
         * If one attribute option for color and category page then return empty array
         */
        if (array_key_exists($attributeColorId, $config) && (count($config[$attributeColorId]) == 1) && ($this->controller == self::CONTROLLER)) {
            $config = [];
        }

        return $this->jsonEncoder->encode($config);
    }

    /**
     * Added function to get simple product skus
     * @return array
     */
    protected function getOptionPrices()
    {
        $prices = [];

        foreach ($this->getAllowProducts() as $product) {
            $priceInfo = $product->getPriceInfo();

            $prices[$product->getId()] =
                [
                    'oldPrice' => [
                        'amount' => $this->_registerJsPrice(
                            $priceInfo->getPrice('regular_price')->getAmount()->getValue()
                        ),
                    ],
                    'basePrice' => [
                        'amount' => $this->_registerJsPrice(
                            $priceInfo->getPrice('final_price')->getAmount()->getBaseAmount()
                        ),
                    ],
                    'finalPrice' => [
                        'amount' => $this->_registerJsPrice(
                            $priceInfo->getPrice('final_price')->getAmount()->getValue()
                        ),
                    ],
                    'productSku' => $product->getSku()
                ];
        }

        return $prices;
    }

    /**
     * This is needed to take the correct templates
     *
     * @return string
     */
    protected function _toHtml()
    {
        $this->setModuleName($this->extractModuleName('Magento\Swatches\Block\Product\Renderer\Configurable'));

        return parent::_toHtml();
    }

    /**
     * Composes configuration for js
     *
     * @return string
     */
    public function getConfig()
    {
        $store = $this->getCurrentStore();
        $currentProduct = $this->getProduct();

        $regularPrice = $currentProduct->getPriceInfo()->getPrice('regular_price');
        $finalPrice = $currentProduct->getPriceInfo()->getPrice('final_price');

        $options = $this->helper->getOptions($currentProduct, $this->getAllowProducts());
        $attributesData = $this->configurableAttributeData->getAttributesData($currentProduct, $options);

        $config = [
            'attributes' => $attributesData['attributes'],
            'template' => str_replace('%s', '<%- data.price %>', $store->getCurrentCurrency()->getOutputFormat()),
            'currencyFormat' => $store->getCurrentCurrency()->getOutputFormat(),
            'optionPrices' => $this->getOptionPrices(),
            'optionStock' => $this->getOptionStocksMessages(),
            'priceFormat' => $this->localeFormat->getPriceFormat(),
            'prices' => [
                'oldPrice' => [
                    'amount' => $this->localeFormat->getNumber($regularPrice->getAmount()->getValue()),
                ],
                'basePrice' => [
                    'amount' => $this->localeFormat->getNumber($finalPrice->getAmount()->getBaseAmount()),
                ],
                'finalPrice' => [
                    'amount' => $this->localeFormat->getNumber($finalPrice->getAmount()->getValue()),
                ],
            ],
            'productId' => $currentProduct->getId(),
            'chooseText' => __('Choose an Option...'),
            'images' => $this->getOptionImages(),
            'index' => isset($options['index']) ? $options['index'] : [],
        ];

        if ($currentProduct->hasPreconfiguredValues() && !empty($attributesData['defaultValues'])) {
            $config['defaultValues'] = $attributesData['defaultValues'];
        }

        $config = array_merge($config, $this->_getAdditionalConfig());

        return $config;
    }

    /**
     * Get option stocks messages
     *
     * @return array
     */
    private function getOptionStocksMessages()
    {
        $stocksMessages = [];

        foreach ($this->getAllowProducts() as $product) {
            $productQty = (int)$this->stockRegistry->getStockItem($product->getId())->getQty();
            $stockMessage = $productQty === 1 ? sprintf('%s!', __('Hurry, only 1 left')) :
                ($productQty < 10 ? sprintf('%s!', __('Only a few more left')) : '');
            $stocksMessages[$product->getId()] = $stockMessage;
        }

        return $stocksMessages;
    }

    /**
     * Get title suffix
     *
     * @return string
     */
    public function getTitleSuffix()
    {
        return $this->_scopeConfig->getValue(self::TITLE_SUFFIX);
    }
}
