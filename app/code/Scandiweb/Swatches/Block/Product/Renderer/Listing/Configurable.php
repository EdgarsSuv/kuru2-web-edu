<?php
/**
 * @vendor Scandiweb
 * @module Scandiweb_Swatches
 * @author Kristaps Stalidzāns <info@scandiweb.com>
 * @copyright Copyright (c) 2017 Magento. All rights reserved.
 * @license http://opensource.org/licenses/afl-3.0.php Academic Free License (AFL 3.0)
 */
namespace Scandiweb\Swatches\Block\Product\Renderer\Listing;

class Configurable extends \Scandiweb\Swatches\Block\Product\Renderer\Configurable
{
    /**
     * @return string
     */
    protected function getRendererTemplate()
    {
        return $this->_template;
    }

    /**
     * Produce and return block's html output.
     *
     * @return string
     */
    protected function _toHtml()
    {
        $output = '';
        if ($this->isProductHasSwatchAttribute()) {
            $output = parent::_toHtml();
        }

        return $output;
    }

    /**
     * @deprecated
     * @return string
     */
    protected function getHtmlOutput()
    {
        $output = '';
        if ($this->isProductHasSwatchAttribute()) {
            $output = parent::getHtmlOutput();
        }

        return $output;
    }
    
    /**
     * @return array
     */
    protected function getSwatchAttributesData()
    {
        $result = [];
        $swatchAttributeData = parent::getSwatchAttributesData();

        foreach ($swatchAttributeData as $attributeId => $item) {
            if (!empty($item['used_in_product_listing'])) {
                $result[$attributeId] = $item;
            }
        }

        return $result;
    }

    /**
    * Composes configuration for js
    *
    * @return string
    */
   public function getJsonConfig()
   {
       $this->unsetData('allow_products');

       return parent::getJsonConfig();
   }
}
