<?php
/**
 * @vendor Scandiweb
 * @module Scandiweb_Swatches
 * @author Kristaps Stalidzāns <info@scandiweb.com>
 * @copyright Copyright (c) 2017 Magento. All rights reserved.
 * @license http://opensource.org/licenses/afl-3.0.php Academic Free License (AFL 3.0)
 */
namespace Scandiweb\Swatches\Block\Adminhtml\Attribute\Edit\Options;

class Visual extends \Magento\Swatches\Block\Adminhtml\Attribute\Edit\Options\Visual
{
    /**
     * @var string
     */
    protected $_template = 'Scandiweb_Swatches::catalog/product/attribute/visual.phtml';

    /**
     * Parse swatch labels for template
     *
     * @codeCoverageIgnore
     * @param null $swatchStoreValue
     * @return string
     */
    protected function reformatSwatchLabels($swatchStoreValue = null)
    {
        if ($swatchStoreValue === null) {
            return;
        }

        $newSwatch = '';

        foreach ($swatchStoreValue as $key => $value) {
            if ($value[0] == '#') {
                $newSwatch[$key] = 'background: '.$value;
            } elseif ($value[0] == '-' || $value[0] == 'l') {
                $newSwatch[$key] = 'background-image: '.$value;
            } elseif ($value[0] == '/') {
                $mediaUrl = $this->swatchHelper->getSwatchMediaUrl();
                $newSwatch[$key] = 'background: url('.$mediaUrl.$value.'); background-size: cover;';
            }
        }

        return $newSwatch;
    }
}
