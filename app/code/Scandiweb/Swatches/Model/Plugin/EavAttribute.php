<?php
/**
 * @vendor Scandiweb
 * @module Scandiweb_Swatches
 * @author Kristaps Stalidzāns <info@scandiweb.com>
 * @copyright Copyright (c) 2017 Magento. All rights reserved.
 * @license http://opensource.org/licenses/afl-3.0.php Academic Free License (AFL 3.0)
 */
namespace Scandiweb\Swatches\Model\Plugin;

use Scandiweb\Swatches\Model\Swatch;
use Magento\Catalog\Model\ResourceModel\Eav\Attribute;

class EavAttribute extends \Magento\Swatches\Model\Plugin\EavAttribute
{
    /**
     * Save Visual Swatch data
     *
     * @param Attribute $attribute
     * @return void
     */
    protected function processVisualSwatch(Attribute $attribute)
    {
        $swatchArray = $attribute->getData('swatch/value');

        if (isset($swatchArray) && is_array($swatchArray)) {
            foreach ($swatchArray as $optionId => $value) {
                $optionId = $this->getAttributeOptionId($optionId);
                $isOptionForDelete = $this->isOptionForDelete($attribute, $optionId);

                if ($optionId === null || $isOptionForDelete) {
                    //option was deleted by button with basket
                    continue;
                }

                $swatch = $this->loadSwatchIfExists($optionId, self::DEFAULT_STORE_ID);
                $swatchType = $this->determineSwatchType($value);
                $this->saveSwatchData($swatch, $optionId, self::DEFAULT_STORE_ID, $swatchType, $value);
                $this->isSwatchExists = null;
            }
        }
    }

    /**
     * @param string $value
     * @return int
     */
    private function determineSwatchType($value)
    {
        $swatchType = Swatch::SWATCH_TYPE_EMPTY;

        if (!empty($value) && $value[0] == '#') {
            $swatchType = Swatch::SWATCH_TYPE_VISUAL_COLOR;
        } elseif (!empty($value) && $value[0] == '/') {
            $swatchType = Swatch::SWATCH_TYPE_VISUAL_IMAGE;
        } elseif (!empty($value) && ($value[0] == '-' || $value[0] == 'l')) {
            $swatchType = Swatch::SWATCH_TYPE_GRADIENT;
        }

        return $swatchType;
    }
}
