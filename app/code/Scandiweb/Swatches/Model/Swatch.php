<?php
/**
 * @vendor Scandiweb
 * @module Scandiweb_Swatches
 * @author Kristaps Stalidzāns <info@scandiweb.com>
 * @copyright Copyright (c) 2017 Magento. All rights reserved.
 * @license http://opensource.org/licenses/afl-3.0.php Academic Free License (AFL 3.0)
 */
namespace Scandiweb\Swatches\Model;

class Swatch extends \Magento\Swatches\Model\Swatch
{
    /** Constant for identifying visual swatch type with color number value */
    const SWATCH_TYPE_GRADIENT = 4;
}
