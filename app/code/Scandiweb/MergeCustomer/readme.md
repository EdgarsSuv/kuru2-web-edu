# Merge Customer

This extension allows to merge customer data in BE
Can be used if cusomter has 2 accounts with oder/wishlist/vault... data that need to be merged to one account.
Main account after merge is deleted.
These merge action can trigger observers.

## Installation

```
composer config repositories.module-mergecustomer git git@bitbucket.org:scandiwebassets/mergecustomer.git
composer require scandiweb/module-mergecustomer:1.0.0
php -f bin/magento setup:upgrade
```
