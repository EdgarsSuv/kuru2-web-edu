<?php
/**
 * @vendor Scandiweb
 * @module Scandiweb_MergeCustomer
 * @author Kristaps Stalidzāns <info@scandiweb.com>
 * @copyright Copyright (c) 2017 Scandiweb, Ltd (http://scandiweb.com)
 * @license http://opensource.org/licenses/afl-3.0.php Academic Free License (AFL 3.0)
 */
namespace Scandiweb\MergeCustomer\Controller\Adminhtml\Customer;

use Magento\Backend\App\Action;
use Magento\Backend\App\Action\Context;
use Magento\Framework\View\Result\PageFactory;
use Magento\Framework\View\LayoutFactory;
use Magento\Framework\Controller\ResultFactory;

class Merge extends Action
{
    
    const ADMIN_RESOURCE = 'Scandiweb_MergeCustomer::top';

    /**
     * @var PageFactory
     */
    protected $resultPageFactory;

    /**
     * @var LayoutFactory
     */
    protected $layoutFactory;

    /**
     * Merge constructor.
     * @param Context $context
     * @param PageFactory $resultPageFactory
     * @param LayoutFactory $layoutFactory
     */
    public function __construct
    (
        Context $context,
        PageFactory $resultPageFactory,
        LayoutFactory $layoutFactory
    )
    {
        $this->resultPageFactory = $resultPageFactory;
        $this->layoutFactory = $layoutFactory;

        return parent::__construct($context);
    }

    /**
     * Returns grid
     *
     * @return \Magento\Framework\App\ResponseInterface|\Magento\Framework\Controller\Result\Raw
     */
    public function execute()
    {
        if (!$this->getRequest()->isAjax()) {
            return $this->_redirect('*/*/index');
        }

        $layout = $this->layoutFactory->create();
        /** @var \Magento\Framework\Controller\Result\Raw $resultRaw */
        $resultRaw = $this->resultFactory->create(ResultFactory::TYPE_RAW);
        $resultRaw->setContents($layout->createBlock('Scandiweb\MergeCustomer\Block\Adminhtml\Grid')->toHtml());

        return $resultRaw;
    }
}
