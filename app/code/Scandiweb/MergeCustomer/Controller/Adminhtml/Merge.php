<?php
/**
 * @vendor Scandiweb
 * @module Scandiweb_MergeCustomer
 * @author Kristaps Stalidzāns <info@scandiweb.com>
 * @copyright Copyright (c) 2017 Scandiweb, Ltd (http://scandiweb.com)
 * @license http://opensource.org/licenses/afl-3.0.php Academic Free License (AFL 3.0)
 */
namespace Scandiweb\MergeCustomer\Controller\Adminhtml;

use Magento\Backend\App\Action;
use Magento\Backend\App\Action\Context;
use Magento\Framework\Api\SearchCriteriaBuilder;
use Magento\Sales\Model\ResourceModel\Order\CollectionFactory;
use Magento\Sales\Api\OrderRepositoryInterface;
use Magento\Customer\Api\AddressRepositoryInterface;
use Magento\Vault\Api\PaymentTokenRepositoryInterface;
use Magento\Customer\Api\CustomerRepositoryInterface;
use Magento\Wishlist\Model\WishlistFactory;
use Scandiweb\MergeCustomer\Logger\Logger;
use Magento\Framework\App\ResourceConnection;
use Magento\Framework\Controller\ResultFactory;
use Magento\Newsletter\Model\SubscriberFactory;

abstract class Merge extends Action
{
    /**
     * Authorization level of a basic admin session
     *
     * @see _isAllowed()
     */
    const ADMIN_RESOURCE = 'Scandiweb_MergeCustomer::top';

    /**
     * Module constants
     */
    const MERGE_IN = 'merge_to';
    const MERGE_FROM = 'merge_from';

    /**
     * @var ResultFactory
     */
    protected $resultRedirect;

    /**
     * @var Logger
     */
    protected $logger;

    /**
     * @var CollectionFactory
     */
    protected $orderCollectionFactory;

    /**
     * @var OrderRepositoryInterface
     */
    protected $orderRepository;

    /**
     * @var AddressRepositoryInterface
     */
    protected $addressRepository;

    /**
     * @var SearchCriteriaBuilder
     */
    protected $searchCriteriaBuilder;

    /**
     * @var WishlistFactory
     */
    protected $wishlistFactory;

    /**
     * @var PaymentTokenRepositoryInterface
     */
    protected $paymentTokenRepository;

    /**
     * @var CustomerRepositoryInterface
     */
    protected $customerRepository;

    /**
     * @var ResourceConnection
     */
    protected $resourceConnection;

    /**
     * @var SubscriberFactory
     */
    protected $subscriberFactory;

    /**
     * Merge constructor.
     * @param Context $context
     * @param CollectionFactory $orderCollectionFactory
     * @param SearchCriteriaBuilder $searchCriteriaBuilder
     * @param OrderRepositoryInterface $orderRepository
     * @param AddressRepositoryInterface $addressRepository
     * @param WishlistFactory $wishlistFactory
     * @param PaymentTokenRepositoryInterface $paymentTokenRepository
     * @param CustomerRepositoryInterface $customerRepository
     * @param ResourceConnection $resourceConnection
     * @param SubscriberFactory $subscriberFactory
     * @param Logger $logger
     */
    public function __construct
    (
        Context $context,
        CollectionFactory $orderCollectionFactory,
        SearchCriteriaBuilder $searchCriteriaBuilder,
        OrderRepositoryInterface $orderRepository,
        AddressRepositoryInterface $addressRepository,
        WishlistFactory $wishlistFactory,
        PaymentTokenRepositoryInterface $paymentTokenRepository,
        CustomerRepositoryInterface $customerRepository,
        ResourceConnection $resourceConnection,
        SubscriberFactory $subscriberFactory,
        Logger $logger
    )
    {
        $this->resultRedirect = $context->getResultFactory();
        $this->logger = $logger;
        $this->searchCriteriaBuilder = $searchCriteriaBuilder;
        $this->orderCollectionFactory = $orderCollectionFactory;
        $this->orderRepository = $orderRepository;
        $this->addressRepository = $addressRepository;
        $this->wishlistFactory = $wishlistFactory;
        $this->paymentTokenRepository = $paymentTokenRepository;
        $this->customerRepository = $customerRepository;
        $this->resourceConnection = $resourceConnection;
        $this->subscriberFactory = $subscriberFactory;

        return parent::__construct($context);
    }

    /**
     * Sets customer id using Magento 2 built Persistent entities
     * For more information please check here
     * @link http://devdocs.magento.com/guides/v2.1/extension-dev-guide/persistent-entities.html#persisting-with-resource-models
     * @param $repository
     * @param $masterUserId
     * @param $selectedUserId
     * @param string $filter
     */
    protected function saveRepositoryItem($repository, $masterUserId, $selectedUserId, $filter = 'customer_id')
    {
        $searchCriteria = $this->searchCriteriaBuilder
            ->addFilter($filter, $masterUserId, 'eq')
            ->create();
        $repositoryList = $repository->getList($searchCriteria);

        foreach ($repositoryList as $repositoryItem) {
            $repositoryItem->setCustomerId($selectedUserId);
            $repository->save($repositoryItem);
        }
    }

    /**
     * @param $selectedUserId
     * @param $masterUserId
     */
    protected function mergeWishlistItems($selectedUserId, $masterUserId)
    {
        $wishlist = $this->wishlistFactory->create();
        $wishlist->loadByCustomerId($selectedUserId, true);
        $wishlistToId = $wishlist->getId();
        $wishlist->loadByCustomerId($masterUserId, true);

        foreach ($wishlist->getItemCollection() as $item) {
            $item->setWishlistId($wishlistToId);
            $item->save();
        }
    }

    /**
     * Temporary Workaround since resource model save method is deprecated for collection
     *
     * @param $selectedUserId
     * @param $masterUserId
     */
    protected function mergeReviews($selectedUserId, $masterUserId)
    {
        $connection = $this->resourceConnection->getConnection();
        $tableName = $this->resourceConnection->getTableName('review_detail');

        $sql = $connection->select()
            ->from($tableName)
            ->where('customer_id = ?', $masterUserId);

        if ($connection->fetchAll($sql)) {
            $connection->update(
                $tableName,
                [
                    'customer_id' => $selectedUserId,
                ],
                [
                    'customer_id = ?' => $masterUserId
                ]
            );
        }
    }

    /**
     * @param $selectedUserId
     * @param $masterUserId
     */
    protected function mergeSubscriptions($selectedUserId, $masterUserId)
    {
        $subscriberFactory = $this->subscriberFactory->create();

        if ($subscriberFactory->loadByCustomerId($masterUserId)) {
            $subscriberFactory->unsubscribeCustomerById($masterUserId);
            $subscriberFactory->subscribeCustomerById($selectedUserId);
        }
    }

    /**
     * @param $masterUserId
     */
    public function deleteCustomer($masterUserId)
    {
        $customer = $this->customerRepository->getById($masterUserId);
        $this->customerRepository->delete($customer);
    }
}
