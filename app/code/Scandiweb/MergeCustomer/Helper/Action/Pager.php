<?php
/**
 * @vendor Scandiweb
 * @module Scandiweb_MergeCustomer
 * @author Kristaps Stalidzāns <info@scandiweb.com>
 * @copyright Copyright (c) 2017 Scandiweb, Ltd (http://scandiweb.com)
 * @license http://opensource.org/licenses/afl-3.0.php Academic Free License (AFL 3.0)
 */
namespace Scandiweb\MergeCustomer\Helper\Action;

use Magento\Backend\Model\Session;
use Magento\Framework\App\Helper\AbstractHelper;
use Magento\Framework\App\Helper\Context;
use Magento\Framework\Exception\LocalizedException;

class Pager extends AbstractHelper
{
    const STORAGE_PREFIX = 'search_result_ids';

    /**
     * Storage id
     *
     * @var int
     */
    protected $_storageId = null;

    /**
     * Array of items
     *
     * @var array
     */
    protected $_items = null;

    /**
     * Backend session model
     *
     * @var Session
     */
    protected $_backendSession;

    /**
     * @param Context $context
     * @param Session $backendSession
     */
    public function __construct
    (
        Context $context,
        Session $backendSession
    )
    {
        $this->_backendSession = $backendSession;

        parent::__construct($context);
    }

    /**
     * Set storage id
     *
     * @param int $storageId
     * @return void
     */
    public function setStorageId($storageId)
    {
        $this->_storageId = $storageId;
    }

    /**
     * Set items to storage
     *
     * @param array $items
     * @return $this
     */
    public function setItems(array $items)
    {
        $this->_items = $items;
        $this->_backendSession->setData($this->_getStorageKey(), $this->_items);

        return $this;
    }

    /**
     * Load stored items
     *
     * @return void
     */
    protected function _loadItems()
    {
        if (is_null($this->_items)) {
            $this->_items = (array)$this->_backendSession->getData($this->_getStorageKey());
        }
    }

    /**
     * Get next item id
     *
     * @param int $id
     * @return int|bool
     */
    public function getNextItemId($id)
    {
        $position = $this->_findItemPositionByValue($id);
        if ($position === false || $position == count($this->_items) - 1) {
            return false;
        }

        return $this->_items[$position + 1];
    }

    /**
     * Get previous item id
     *
     * @param int $id
     * @return int|bool
     */
    public function getPreviousItemId($id)
    {
        $position = $this->_findItemPositionByValue($id);
        if ($position === false || $position == 0) {
            return false;
        }

        return $this->_items[$position - 1];
    }

    /**
     * Return item position based on passed in value
     *
     * @param mixed $value
     * @return int|bool
     */
    protected function _findItemPositionByValue($value)
    {
        $this->_loadItems();

        return array_search($value, $this->_items);
    }

    /**
     * Get storage key
     *
     * @return string
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    protected function _getStorageKey()
    {
        if (!$this->_storageId) {
            throw new LocalizedException(__('Storage key was not set'));
        }

        return self::STORAGE_PREFIX . $this->_storageId;
    }
}
