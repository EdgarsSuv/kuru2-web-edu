<?php
/**
 * @vendor Scandiweb
 * @module Scandiweb_MergeCustomer
 * @author Kristaps Stalidzāns <info@scandiweb.com>
 * @copyright Copyright (c) 2017 Scandiweb, Ltd (http://scandiweb.com)
 * @license http://opensource.org/licenses/afl-3.0.php Academic Free License (AFL 3.0)
 */
namespace Scandiweb\MergeCustomer\Block\Adminhtml;

use Magento\Backend\Block\Template\Context;
use Magento\Backend\Block\Widget\Grid\Extended;
use Magento\Backend\Helper\Data;
use Magento\Customer\Model\ResourceModel\CustomerFactory;
use Magento\Framework\Registry;
use Magento\Customer\Model\ResourceModel\Grid\CollectionFactory;
use Scandiweb\MergeCustomer\Helper\Action\Pager;

class Grid extends Extended
{
    /**
     * Review data
     *
     * @var \Magento\Review\Helper\Data
     */
    protected $_reviewData = null;

    /**
     * Core registry
     *
     * @var Registry
     */
    protected $_coreRegistry = null;

    /**
     * Review collection model factory
     *
     * @var CustomerFactory
     */
    protected $_customerFactory;

    /**
     * @var CollectionFactory
     */
    protected $_customerCollectionFactory;

    /**
     * @var Pager
     */
    protected $_customerActionPager;

    /**
     * Grid constructor.
     * @param Context $context
     * @param Data $backendHelper
     * @param CustomerFactory $customerFactory
     * @param Registry $coreRegistry
     * @param CollectionFactory $customerCollectionFactory
     * @param Pager $customerActionPager
     * @param array $data
     */
    public function __construct(
        Context $context,
        Data $backendHelper,
        CustomerFactory $customerFactory,
        Registry $coreRegistry,
        CollectionFactory $customerCollectionFactory,
        Pager $customerActionPager,
        array $data = []
    ) {
        $this->_customerFactory = $customerFactory;
        $this->_coreRegistry = $coreRegistry;
        $this->_customerCollectionFactory = $customerCollectionFactory;
        $this->_customerActionPager = $customerActionPager;

        parent::__construct($context, $backendHelper, $data);
    }

    /**
     * Initialize grid
     *
     * @return void
     */
    protected function _construct()
    {
        parent::_construct();
        $this->setId('customerGrid');
        $this->setDefaultSort('entity_id');
        $this->setUseAjax(true);
    }

    /**
     * @return \Magento\Customer\Model\ResourceModel\Grid\Collection
     */
    protected function getCustomerCollection()
    {
        $collection = $this->_customerCollectionFactory->create();

        /**
         * Filter out your self
         */
        if ($this->getPostCustomer()) {
            $collection->addFieldToFilter('entity_id', ['neq' => $this->getPostCustomer()]);
        }

        return $collection;
    }

    /**
     * Save search results
     *
     * @return \Magento\Backend\Block\Widget\Grid
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    protected function _afterLoadCollection()
    {
        /** @var $actionPager \Scandiweb\MergeCustomer\Helper\Action\Pager */
        $actionPager = $this->_customerActionPager;
        $actionPager->setStorageId('customer');
        $actionPager->setItems($this->getCustomerCollection()->getAllIds());

        return parent::_afterLoadCollection();
    }

    /**
     * Prepare collection
     *
     * @return Extended
     */
    protected function _prepareCollection()
    {
        $this->setCollection($this->getCustomerCollection());

        return parent::_prepareCollection();
    }

    /**
     * Prepare grid columns
     *
     * @return \Magento\Backend\Block\Widget\Grid
     * @throws \Exception
     * @throws \Magento\Framework\Exception\LocalizedException
     * @SuppressWarnings(PHPMD.ExcessiveMethodLength)
     */
    protected function _prepareColumns()
    {
        $this->addColumn(
            'entity_id',
            [
                'header' => __('ID'),
                'filter_index' => 'entity_id',
                'index' => 'entity_id',
                'header_css_class' => 'col-id',
                'column_css_class' => 'col-id'
            ]
        );

        $this->addColumn(
            'name',
            [
                'header' => __('Name'),
                'filter_index' => 'name',
                'index' => 'name',
                'header_css_class' => 'col-id',
                'column_css_class' => 'col-id'
            ]
        );

        $this->addColumn(
            'email',
            [
                'header' => __('E-mail'),
                'filter_index' => 'email',
                'index' => 'email',
                'header_css_class' => 'col-id',
                'column_css_class' => 'col-id'
            ]
        );

        $this->addColumn(
            'created_at',
            [
                'header' => __('Created At'),
                'type' => 'datetime',
                'filter_index' => 'created_at',
                'index' => 'created_at',
                'header_css_class' => 'col-date',
                'column_css_class' => 'col-date'
            ]
        );

        $this->addColumn(
            'created_in',
            [
                'header' => __('Created In'),
                'type' => 'datetime',
                'filter_index' => 'created_in',
                'index' => 'created_at',
                'header_css_class' => 'col-date',
                'column_css_class' => 'col-date'
            ]
        );

        $this->addColumn(
            'action',
            [
                'header' => __('Action'),
                'type' => 'action',
                'getter' => 'getEntityId',
                'actions' => [
                    [
                        'caption' => __('Merge To'),
                        'url' => [
                            'base' => 'customermerge/customer/mergeaction',
                            'params' => [
                                'action' => 'merge_to',
                                'id' => $this->getPostCustomer(),
                                'entity_id' => $this->getEntityId()
                            ],
                        ],
                        'field' => 'entity_id',
                        'confirm' => __('Are you sure wanna merge this customer?')
                    ],
                    [
                        'caption' => __('Merge From'),
                        'url' => [
                            'base' => 'customermerge/customer/mergeaction',
                            'params' => [
                                'action' => 'merge_from',
                                'id' => $this->getPostCustomer(),
                                'entity_id' => $this->getEntityId()
                            ],
                        ],
                        'field' => 'entity_id',
                        'confirm' => __('Are you sure wanna merge this customer?')
                    ],
                ],
                'filter' => false,
                'sortable' => false
            ]
        );

        $block = $this->getLayout()->getBlock('grid.bottom.links');

        if ($block) {
            $this->setChild('grid.bottom.links', $block);
        }

        return parent::_prepareColumns();
    }

    /**
     * Return Post Customer Id
     *
     * @return mixed
     */
    protected function getPostCustomer()
    {
        if ($this->getRequest()->getParam('id')) {
            $customerId = $this->getRequest()->getParam('id');

            return $customerId;
        }
    }

    /**
     * {@inheritdoc}
     */
    public function getRowUrl($row)
    {
        return $this->getUrl('customer/index/edit', ['id' => $row->getEntityId()]);
    }
}
