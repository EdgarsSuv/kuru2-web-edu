<?php
/**
 * @vendor Scandiweb
 * @module Scandiweb_Promo
 * @license http://opensource.org/licenses/OSL-3.0 The Open Software License 3.0 (OSL-3.0)
 */
\Magento\Framework\Component\ComponentRegistrar::register(
    \Magento\Framework\Component\ComponentRegistrar::MODULE,
    'Scandiweb_Promo',
    __DIR__
);
