<?php
/**
 * @vendor      Scandiweb
 * @module      Scandiweb_Promo
 * @license     http://opensource.org/licenses/OSL-3.0 The Open Software License 3.0 (OSL-3.0)
 */
namespace Scandiweb\Promo\Setup;

use Magento\Config\Block\System\Config\Form;
use Magento\Framework\Setup\UpgradeDataInterface;
use Magento\Framework\Setup\ModuleDataSetupInterface;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Config\Model\ResourceModel\Config;

/**
 * Class UpgradeData
 * @package Scandiweb\Promo\Setup
 */
class UpgradeData implements UpgradeDataInterface
{
    /**
     * @var Config
     */
    protected $_resourceConfig;

    /**
     * UpgradeData constructor.
     *
     * @param Config $resourceConfig
     */
    public function __construct(Config $resourceConfig)
    {
        $this->_resourceConfig = $resourceConfig;
    }

    /**
     * @param ModuleDataSetupInterface $setup
     * @param ModuleContextInterface $context
     */
    public function upgrade(ModuleDataSetupInterface $setup, ModuleContextInterface $context)
    {
        $setup->startSetup();

        if (version_compare($context->getVersion(), '1.0.1', '<')) {
            $this->updateConfig();
        }

        $setup->endSetup();
    }

    public function updateConfig()
    {
        $this->_resourceConfig->saveConfig('design/promo_configurations/header_promo_text',
            'FREE Shipping + FREE Exchanges + <a href="/customer-service/returns-and-exchanges.html" class="blue">FREE Return</a>',
            Form::SCOPE_DEFAULT,
            0
        );
    }
}
