<?php
/**
 * @vendor      Scandiweb
 * @module      Scandiweb_Promo
 * @license     http://opensource.org/licenses/OSL-3.0 The Open Software License 3.0 (OSL-3.0)
 */
namespace Scandiweb\Promo\Setup;

use Magento\Config\Block\System\Config\Form;
use Magento\Framework\Setup\InstallDataInterface;
use Magento\Framework\Setup\ModuleDataSetupInterface;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Config\Model\ResourceModel\Config;

/**
 * Class InstallData
 * @package Scandiweb\Promo\Setup
 */
class InstallData implements InstallDataInterface
{
    /**
     * @var Config
     */
    protected $_resourceConfig;

    /**
     * InstallData constructor.
     *
     * @param Config $resourceConfig
     */
    public function __construct(Config $resourceConfig)
    {
        $this->_resourceConfig = $resourceConfig;
    }

    /**
     * @param ModuleDataSetupInterface $setup
     * @param ModuleContextInterface $context
     */
    public function install(ModuleDataSetupInterface $setup, ModuleContextInterface $context)
    {
        $setup->startSetup();

        $this->_resourceConfig->saveConfig('design/promo_configurations/header_promo_text',
            'Free Shipping. Free Return. Free Exchanges.',
            Form::SCOPE_DEFAULT,
            0
        );

        $setup->endSetup();
    }
}
