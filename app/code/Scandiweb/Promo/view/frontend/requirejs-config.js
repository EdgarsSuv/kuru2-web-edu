/**
 * @vendor Scandiweb
 * @module Scandiweb_Promo
 * @license http://opensource.org/licenses/OSL-3.0 The Open Software License 3.0 (OSL-3.0)
 */
var config = {
    map: {
        '*': {
            showPromojs: 'Scandiweb_Promo/js/showPromo'
        }
    }
};
