/**
 * @vendor Scandiweb
 * @module Scandiweb_Promo
 * @license http://opensource.org/licenses/OSL-3.0 The Open Software License 3.0 (OSL-3.0)
 */
/**
 * Show promo banner after load
 */
require([
    'jquery'
], function ($) {
    var Offset = function() {
        this.oldTop = 0;
        this.headerTop = function() {
            var $header = $('.header.content');
            var headerTop = parseInt($header[0].getBoundingClientRect().top);
            return headerTop;
        };
        this.headerBottom = function() {
            var $header = $('.header.content');
            var headerHeight = 68;
            var headerBottom = this.headerTop() + headerHeight;
            // On some browsers the jQuery bottom method is imprecise by half a pixel;
            return headerBottom;
        };
        this.mobileMenuReset = function() {
            var $mobileMenu = $('.header-main');

            if (window.innerWidth < 1024) {
                $mobileMenu.css('top', this.headerBottom());
            } else {
                $mobileMenu.css('top', 0);
            }
        };
        this.minicartTitleReset = function () {
            var $minicartTitle = $('.header .minicart-title');
            var defaultTopPadding = $minicartTitle.data('default_top_padding');
            if (typeof(defaultTopPadding) === 'undefined') {
                defaultTopPadding = parseInt($minicartTitle.css('padding-top'));
                $minicartTitle.data('default_top_padding', defaultTopPadding);
            }

            if (window.innerWidth >= 1024) {
                $minicartTitle.css('padding-top', defaultTopPadding + this.headerTop());
            } else {
                $minicartTitle.css('padding-top', defaultTopPadding);
            }
        };
        this.minicartContentReset = function() {
            var $minicart = $('.header .block-minicart');
            var $minicartContent = $('.header .minicart-content');
            var defaultTopPadding = $minicartContent.data('default_top_padding');
            if (typeof(defaultTopPadding) === 'undefined') {
                // Set default Desktop padding
                if (window.innerWidth >= 1024) {
                    defaultTopPadding = parseInt($minicartContent.css('padding-top'));
                } else {
                    defaultTopPadding = $('.header-top').outerHeight();
                }
                $minicartContent.data('default_top_padding', defaultTopPadding);
            }

            if (window.innerWidth >= 1024) {
                $minicartContent.css('padding-top', defaultTopPadding + this.headerTop());
                $minicart.css('top', 0);
            } else {
                $minicartContent.css('padding-top', 0);
                $minicart.css('top', this.headerBottom());
            }

            $('.header #minicart-content-wrapper')
                .off('DOMNodeInserted', '.minicart-title')
            $('.header #minicart-content-wrapper')
                .off('DOMNodeInserted', '.block-content')
        };
        this.minicartMenuReset = function() {
            this.minicartTitleReset();
            $('.header #minicart-content-wrapper')
                .on('DOMNodeInserted', '.minicart-title', this.minicartTitleReset.bind(this));

            this.minicartContentReset();
            $('.header #minicart-content-wrapper')
                .on('DOMNodeInserted', '#class-buddy', this.minicartContentReset.bind(this));
        };
        this.menuDropdownReset = function() {
            // Change top offset relative to header bottom
            var headerBottom = this.headerBottom();
            var $dropdowns = [$('.header .navigation .ui-menu .level0 > .submenu'), $('.header .account-links .help-submenu')];
            $.each($dropdowns, function(index, element) {
                var $dropdown = $(element);
                if (window.innerWidth >= 1024) {
                    $dropdown.css('top', headerBottom);
                } else {
                    $dropdown.css('top', 0);
                }
            });
        };
        this.headerReset = function() {
            var $header = $('.page-header');
            if ($header) {
                $header.sticky('update');
            }
        };
        this.reset = function() {
            this.headerReset();
            this.mobileMenuReset();
            this.minicartMenuReset();
            this.menuDropdownReset();
        };
        this.resetAndCache = function() {
            var oldTop = this.oldTop;
            var newTop = this.headerTop();
            if (oldTop !== newTop) {
                this.reset();
            }
            this.oldTop = newTop;
        };
        this.resetLong = function() {
            var id = setInterval(this.reset.bind(this), 1000 / 60);
            setTimeout(function() {
                clearInterval(id);
            }, 2000);
        }
    };

    $(document).ready(function () {
        if (!($('body').hasClass('cms-no-route'))) {
            offset = new Offset();
            $promoblock = $('.header-block-promo');
            if ($.cookie('CloseButtonPressed') === 'True') {
                $promoblock.slideUp('fast');
            } else if (($.cookie('CloseButtonPressed') === 'False') || ($.cookie('CloseButtonPressed') === null)) {
                $promoblock.slideDown('fast');
            }
            offset.resetLong();

            $(window).scroll(function () {
                offset.reset.bind(offset)();
            });

            $(window).resize(function () {
                offset.reset.bind(offset)();
            });

            $(document).on('dropdowndialogclose dropdowndialogopen', function () {
                offset.reset.bind(offset)();
            });

            $('.mobile-menu').click(function () {
                offset.reset.bind(offset)();
            });

            $('.header-block-x').click(function (event) {
                if (event) {
                    offset.resetLong();
                    $promoblock = $('.header-block-promo');
                    $promoblock.slideUp('fast');
                    $.cookie('CloseButtonPressed', true, {expires: 7});
                }
            });
        }
    });
});
