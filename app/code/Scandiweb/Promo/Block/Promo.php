<?php
/**
 * @vendor      Scandiweb
 * @module      Scandiweb_Promo
 * @license     http://opensource.org/licenses/OSL-3.0 The Open Software License 3.0 (OSL-3.0)
 */
namespace Scandiweb\Promo\Block;

use Magento\Framework\View\Element\Template;
use Magento\Framework\Stdlib\CookieManagerInterface;
use Magento\Framework\View\Element\Template\Context;

/**
 * Class Promo
 */
class Promo extends Template
{
    /**
     * @var CookieManagerInterface
     */
    protected $_cookieManager;

    /**
     * Promo constructor.
     * @param CookieManagerInterface $cookieManager
     * @param Context $context
     * @param array $data
     */
    public function __construct(
        CookieManagerInterface $cookieManager,
        Context $context,
        array $data
    )
    {
        $this->_cookieManager = $cookieManager;
        parent::__construct($context, $data);
    }

    /**
     * Get config value from backend
     *
     * @param $path
     * @return mixed
     */
    public function getConfig($path)
    {
       return $this->_scopeConfig->getValue($path);
    }

    /**
     * Get merchendise alerts
     * @return mixed
     */
    public function getMessage()
    {
        return $this->getConfig('design/promo_configurations/header_promo_text');
    }

    /**
     * Returns CMS page path URL rewrite
     * @return mixed
     */
    public function getMoreUrl()
    {
        return $this->getConfig('design/promo_configurations/more_link');
    }

    /**
     * @return CookieManagerInterface
     */
    public function getCookieManager()
    {
        return $this->_cookieManager;
    }

    /**
     * Check if header promo is enabled
     *
     * @return string|null
     */
    public function isEnabled()
    {
        return $this->getConfig('design/promo_configurations/header_promo_status');
    }
}
