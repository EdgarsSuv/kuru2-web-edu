'use strict';

var gulp = require('gulp');
var sass = require('gulp-sass');
var sourcemaps = require('gulp-sourcemaps');
var spritesmith = require('gulp.spritesmith');
var through = require('through2');
var browserSync = require('browser-sync').create();
var csscomb = require('gulp-csscomb');
var scsslint = require('gulp-scss-lint');
var autoprefixer = require('gulp-autoprefixer');
var cleanCss = require('gulp-clean-css');

var kurufootwearWebDir = './app/design/frontend/Kurufootwear/default/web/',
    kurufootwearStaticDir = './pub/static/frontend/Kurufootwear/default/',
    kurufootwearMaintenanceDir = './pub/errors/maintenancemode/';

/**
 * Compile css and run watch tasks
 */
gulp.task('default', ['sass', 'sass-emails', 'sass-maintenance', 'scss-lint', 'browser-sync', 'watch']);



gulp.task('sass', function () {
    var wm = gulp.src(kurufootwearWebDir + 'scss/**/*.scss')
        .pipe(sass({outputStyle: 'compressed'}).on('error', sass.logError))
        .pipe(autoprefixer('last 2 versions'))
        .pipe(cleanCss({compatibility: 'ie10'}))
        .pipe(gulp.dest(kurufootwearStaticDir + 'en_US' + '/css'))
        .pipe(gulp.dest(kurufootwearWebDir + 'css'))
        .pipe(browserSync.stream());

    return [wm];
});


/**
 * Sass-dev task
 *
 * Compiles minified css directly to pub/static with source maps
 * and reloads stylesheets with browser-sync
 */
gulp.task('sass-dev', function () {
    var wm = gulp.src(kurufootwearWebDir + 'scss/**/*.scss')
        .pipe(sourcemaps.init())
        .pipe(sass({outputStyle: 'compressed'}).on('error', sass.logError))
        .pipe(autoprefixer('last 2 versions'))
        .pipe(cleanCss({compatibility: 'ie10'}, (function() {
            var totaldiff = 0;
            return function(details) {
                var diff = details.stats.originalSize - details.stats.minifiedSize;
                console.log(details.name + ' optimized: ' + (diff / 1024).toFixed(2) + 'kB');
                totaldiff = totaldiff + diff;
                console.log('Total: ' + +(totaldiff / 1024).toFixed(2) + 'kB');
            }
        })()))
        .pipe(sourcemaps.write())
        .pipe(gulp.dest(kurufootwearStaticDir + 'en_US' + '/css'))
        .pipe(gulp.dest(kurufootwearWebDir + 'css'))
        .pipe(browserSync.stream());

    return [wm];
});

/**
 * Email Sass task
 *
 * Compiles minified css directly to pub/static with source maps
 */
gulp.task('sass-emails', function () {
    return gulp.src(kurufootwearWebDir + 'scss-email/**/*.scss')
        .pipe(sass({outputStyle: 'compressed'}).on('error', sass.logError))
        .pipe(gulp.dest(kurufootwearWebDir + 'css'));
});

/**
 * Maintenance page Sass task
 *
 * Compiles minified css directly to pub/static with source maps
 */
gulp.task('sass-maintenance', function () {
    return gulp.src(kurufootwearWebDir + 'scss-maintenance/styles.scss')
        .pipe(sass({outputStyle: 'compressed'}).on('error', sass.logError))
        .pipe(gulp.dest(kurufootwearMaintenanceDir + 'css'));
});

/**
 * Sass linter
 */
gulp.task('scss-lint', function () {
    return gulp.src([kurufootwearWebDir + 'scss/**/*.scss', '!' + kurufootwearWebDir + 'scss/vendor/**/*.scss', '!' + kurufootwearWebDir + 'scss/common/_grid.scss', '!' + kurufootwearWebDir + 'scss/utils/*.scss'])
        .pipe(scsslint({
            'config': './app/design/frontend/Kurufootwear/default/web/lint-config.yml',
            'maxBuffer': 1024000
        }));
});

/**
 * watches files for changes and triggers various tasks
 */
gulp.task('watch', function () {
    gulp.watch([kurufootwearWebDir + 'scss/**/*.scss'], ['sass', 'scss-lint']);
    gulp.watch([kurufootwearWebDir + 'scss-email/**/*.scss'], ['sass-emails', 'scss-lint']);
});

/**
 * initialise browser-sync
 */
gulp.task('browser-sync', function () {
    browserSync.init({
        proxy: "kuru.dev"
    });
});

/**
 * Sprite task
 *
 * Generates retina sprites from 2 source images
 */
gulp.task('sprite', function () {
    var spriteData = gulp.src(kurufootwearWebDir + 'sprites/*.png')
        .pipe(spritesmith(
            {
                imgName: 'sprites.png',
                retinaImgName: 'sprites@2x.png',
                imgPath: '../spritesheets/sprites.png',
                retinaImgPath: '../spritesheets/sprites@2x.png',
                cssName: '_sprites.scss',
                retinaSrcFilter: kurufootwearWebDir + 'sprites/*@2x.png',
                cssVarMap: function (sprite) {
                    // prefix all sprite variables
                    sprite.name = 'sprite_' + sprite.name;
                }
            })
        );

    var rewrites = [];

    spriteData.img
    // grab filename rewrites
        .pipe(through.obj(function (file, enc, callback) {
            if (file.path && file.revOrigPath) {
                var r = {
                    from: relPath(file.revOrigBase, file.revOrigPath),
                    to: relPath(file.base, file.path)
                };
                rewrites.push(r);
            }

            this.push(file);

            callback();
        }))

        // move to release directory
        .pipe(gulp.dest('./app/design/frontend/Kurufootwear/default/web/spritesheets/'))

        .on('end', function () {
            var cssPipe = spriteData.css;

            for (var i = 0; i < rewrites.length; ++i) {
                var r = rewrites[i];

                cssPipe = cssPipe
                // rename references to fingerprinted spritesheet
                    .pipe(replace(r.from, r.to));
            }

            cssPipe
            // move to assets directory
                .pipe(gulp.dest(kurufootwearWebDir + 'scss/utils'));
        });
});
