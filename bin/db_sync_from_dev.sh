#!/bin/bash

# 
# *** Updated local database with development server database ***
# 
# Requires a .my.cnf file in the user's home directory on 
# both the remote machine and the local machine with a [client] 
# section like so:
# 
# [client]
# user = "mysql_user"
# password = "password"
#
# TODO: Add some error handling and aborting.
# 

SSH_USER="jim"
SSH_REMOTE_HOST="dev.kurufootwear.com"
REMOTE_DB_NAME="kuru_staging"
REMOTE_WEB_ROOT="/var/www/staging"
LOCAL_DB_NAME="kuru_local"
LOCAL_BASE_URL_SECURE="https://kuru.dev"
LOCAL_BASE_URL_UNSECURE="http://kuru.dev"

if [ ! -d "app" ]; then
	echo "app directory not found. This script needs to be executed in the magento root directory."
	exit
fi

dt=$(date +'%m-%d-%Y_%H-%M-%S')

# Dump database on dev
echo "Dumping remote database..."
ssh ${SSH_USER}@${SSH_REMOTE_HOST} "mysqldump --single-transaction $REMOTE_DB_NAME | gzip > ${REMOTE_WEB_ROOT}/db_backups/${REMOTE_DB_NAME}-${dt}.sql.gz"

# Copy to local
echo "Downloading remote dump file..."
scp ${SSH_USER}@${SSH_REMOTE_HOST}:${REMOTE_WEB_ROOT}/db_backups/${REMOTE_DB_NAME}-${dt}.sql.gz db_backups

# Drop local database
echo "Dropping local database..."
mysql -e "drop database ${LOCAL_DB_NAME};"

# Re-create the local database
echo "Creating local database..."
mysql -e "CREATE SCHEMA ${LOCAL_DB_NAME} DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci;"

# Import from local file
echo "Importing data..."
gunzip -c db_backups/${REMOTE_DB_NAME}-${dt}.sql.gz | mysql --force ${LOCAL_DB_NAME}

# Modify DB for local use
echo "Modifying data for local use..."
mysql -e "USE ${LOCAL_DB_NAME}; UPDATE core_config_data SET value = '${LOCAL_BASE_URL_UNSECURE}' WHERE path = 'web/unsecure/base_url';"
mysql -e "USE ${LOCAL_DB_NAME}; UPDATE core_config_data SET value = '${LOCAL_BASE_URL_SECURE}' WHERE path = 'web/secure/base_url';"

### Clean up ###

# Delete remote file
echo "Deleting remote dump file..."
ssh ${SSH_USER}@${SSH_REMOTE_HOST} "rm ${REMOTE_WEB_ROOT}/db_backups/${REMOTE_DB_NAME}-${dt}.sql.gz"

# Delete local file?
echo "Deleting local dump file..."
rm db_backups/${REMOTE_DB_NAME}-${dt}.sql.gz

# Clean Magento cache
bin/magento cache:clean

# Gulp
gulp sass
gulp sprite

echo "Done!"
