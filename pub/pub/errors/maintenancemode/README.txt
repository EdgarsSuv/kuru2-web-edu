Magento 2.1.7 has core bug reported and fixed here https://github.com/magento/magento2/issues/4305
Fix will be available only in 2.2 version.
As a permanent workaround we have created empty pub/errors/maintenancemode folder.